const path = require( 'path' );
const HtmlWebpackPlugin = require( 'html-webpack-plugin' );
const CopyWebpackPlugin = require( 'copy-webpack-plugin' );
const ExtractTextPlugin = require( 'extract-text-webpack-plugin' );
const TimeStampPlugin = require( './webpack-plugins/timestamp' );
const {
  AureliaPlugin
} = require( 'aurelia-webpack-plugin' );
const {
  ProvidePlugin
} = require( 'webpack' );

// config helpers:
const ensureArray = ( config ) => config && ( Array.isArray( config ) ? config : [ config ] ) || []
const when = ( condition, config, negativeConfig ) =>
  condition ? ensureArray( config ) : ensureArray( negativeConfig )

// primary config:
const title = 'Snowatch';
const outDir = path.resolve( __dirname, 'dist' );
const srcDir = path.resolve( __dirname, 'src' );
const nodeModulesDir = path.resolve( __dirname, 'node_modules' );
const baseUrl = '';

const cssRules = [
  {
    loader: 'css-loader'
  },
  {
    loader: 'sass-loader'
  },
  {
    loader: 'postcss-loader',
    options: { sourceMap: true }
  },
]

/**
 * @return {webpack.Configuration}
 */
module.exports = ( {
  production,
  server,
  extractCss,
  coverage
} = {} ) => ( {
  context: srcDir,
  resolve: {
    extensions: [ ".ts", ".tsx", ".js" ],
    modules: [ srcDir, 'node_modules' ],
  },
  devtool: 'source-map',
  entry: {
    app: [ 'aurelia-bootstrapper' ],
    vendor: [ 'jquery', 'bootstrap' ],
  },
  output: {
    path: outDir,
    publicPath: baseUrl,
    filename: production ? '[name].[chunkhash].bundle.js' : '[name].[hash].bundle.js',
    sourceMapFilename: production ? '[name].[chunkhash].bundle.map' : '[name].[hash].bundle.map',
    chunkFilename: production ? '[name].[chunkhash].chunk.js' : '[name].[hash].chunk.js',
  },
  devServer: {
    hot: true,
    inline: true,
    contentBase: outDir,
    // serve index.html for all 404 (required for push-state)
    historyApiFallback: true,
  },
  module: {
    rules: [
      // CSS required in JS/TS files should use the style-loader that auto-injects it into the website
      // only when the issuer is a .js/.ts file, so the loaders are not applied inside html templates
      {
        test: /\.css$/i,
        issuer: [ {
          not: [ {
            test: /\.html$/i
          } ]
        } ],
        use: extractCss ? ExtractTextPlugin.extract( {
          fallback: 'style-loader',
          use: cssRules,
        } ) : [ 'style-loader', ...cssRules ],
      },
      { // sass / scss loader for webpack
        test: /\.(sass|scss)$/,
        issuer: [ {
          not: [ {
            test: /\.html$/i
          } ]
        } ],
        use: extractCss ? ExtractTextPlugin.extract( {
          fallback: 'style-loader',
          use: cssRules,
        } ) : [ 'style-loader', ...cssRules ]
      },
      {
        test: /\.css$/i,
        issuer: [ {
          test: /\.html$/i
        } ],
        // CSS required in templates cannot be extracted safely
        // because Aurelia would try to require it again in runtime
        use: cssRules,
      },
      {
        test: /\.(sass|scss)$/,
        issuer: [ {
          test: /\.html$/i
        } ],
        // CSS required in templates cannot be extracted safely
        // because Aurelia would try to require it again in runtime
        use: [ ...cssRules, 'sass-loader' ]
      },
      {
        test: /\.html$/i,
        loader: 'html-loader'
      },
      {
        test: /\.tsx?$/i,
        loader: "ts-loader"
      },
      {
        test: /\.json$/i,
        loader: 'json-loader'
      },
      {
        test: require.resolve( 'jquery' ),
        loader: 'expose-loader',
        options: {
          exposes: [ '$', 'jQuery' ],
        },
      },
      // embed small images and fonts as Data Urls and larger ones as files:
      {
        test: /\.(png|gif|jpg|cur)$/i,
        loader: 'url-loader',
        options: {
          limit: 8192
        }
      },
      {
        test: /\.woff2(\?v=[0-9]\.[0-9]\.[0-9])?$/i,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimetype: 'application/font-woff2'
        }
      },
      {
        test: /\.woff(\?v=[0-9]\.[0-9]\.[0-9])?$/i,
        loader: 'url-loader',
        options: {
          limit: 10000,
          mimetype: 'application/font-woff'
        }
      },
      // load these fonts normally, as files:
      {
        test: /\.(ttf|eot|svg|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/i,
        loader: 'file-loader'
      },
    ],
  },
  optimization: {
    splitChunks: {
      name: 'common',
    },
    noEmitOnErrors: true, // NoEmitOnErrorsPlugin
    concatenateModules: true //ModuleConcatenationPlugin
  },
  plugins: [
    new AureliaPlugin( {
      aureliaApp: 'snowatch-game-loader'
    } ),
    new ProvidePlugin( {
      '$': 'jquery',
      'jQuery': 'jquery',
      'window.jQuery': 'jquery',
      'Popper': [ 'popper.js', 'default' ],
    } ),
    new HtmlWebpackPlugin( {
      template: 'index.ejs',
      minify: production ? {
        removeComments: true,
        collapseWhitespace: true,
      } : undefined,
      metadata: {
        // available in index.ejs //
        title,
        server,
        baseUrl
      },
    } ),
    new CopyWebpackPlugin( {
      patterns: [ {
        from: 'static/favicon.ico',
        to: 'favicon.ico'
      }, ],
    } ),
    new TimeStampPlugin( {
      outDir: outDir
    } ),
    ...when( extractCss, new ExtractTextPlugin( {
      filename: production ? '[contenthash].css' : '[id].css',
      allChunks: true,
    } ) ),
  ],
} )
