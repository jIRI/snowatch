# snowatch

At some point this will be Typescript engine for making interactive fictions/text games.

# How to build

Install all dependencies:

```
npm install
npm install -g typings
typings install
```

Also install VSA 2017 with C++ compiler, as SASS requires that, or use combination of VS install and following:

```
npm install --global --production windows-build-tools
npm config set msvs_version 2017 --global
```

Development build watching for changes:
```
npm start
```

Production build:
```
npm start -- webpack.build.production
```