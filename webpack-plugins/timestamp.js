var fs = require('fs');

function TimeStampPlugin(options) {
  this.outDir = options.outDir;
}

TimeStampPlugin.prototype.apply = function(compiler) {
  let self = this;
  compiler.plugin('done', function() {
    if( !fs.existsSync(self.outDir) ) {
      fs.mkdirSync(self.outDir);
    }

    const timestampFile = self.outDir + '/buildstamp.json';
    fs.writeFileSync(timestampFile, '{ "timestamp": "' + new Date().toJSON() + '" }');
  });
};

module.exports = TimeStampPlugin;