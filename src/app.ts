import {Router, RouterConfiguration} from 'aurelia-router';
import { PLATFORM } from 'aurelia-pal';

export class App {
  router: Router;

  configureRouter(config: RouterConfiguration, router: Router) {
    config.title = 'Snowatch';
    config.map([
      { route: [''],  moduleId: PLATFORM.moduleName('snowatch/game-controller'), nav: true, title: 'Game'},
    ]);

    this.router = router;
  }
}
