import {GameData} from 'snowatch/game-data';
import {broadcast, Message} from 'snowatch/message';
import {Channel} from 'snowatch/bus';
import {Topics} from 'snowatch/message-topics/all';
import * as ent from 'snowatch/entity';
import * as result from 'snowatch/result';

export interface BehaviorProperties {
  behaviors: Map<string, BehaviorInit>;
}

export interface BehaviorPropertiesInit {
  behaviors: Array<ent.HasId & BehaviorInit>;
}

export interface BehaviorSetupFn {
  (target: ent.Entity, state: BehaviorState | null): void;
}

export interface BehaviorFilterFn {
  (target: ent.Entity, state: BehaviorState | null, message: Message): result.ResultObject;
}

export interface BehaviorProcessFn {
  (target: ent.Entity, state: BehaviorState | null, message: Message): void;
}

export interface BehaviorSubProcessFn {
  (target: ent.Entity, state: BehaviorState | null, message: Message): any;
}

export enum BehaviorStatus {
  Enabled = 'Enabled',
  Disabled = 'Disabled'
}

export interface BehaviorState {
  status?: BehaviorStatus;
}

export function createEmptyBehaviorState(): BehaviorState {
  return {};
}

export interface BehaviorStateInit<T extends BehaviorState> {
  initialState?: T;
  setup?: BehaviorSetupFn;
  activate?: ent.InitializerFn;
  filter?: BehaviorFilterFn;
  process?: BehaviorProcessFn;
  subProcess?: BehaviorSubProcessFn;
}

export interface BehaviorInit extends BehaviorStateInit<BehaviorState>, ent.HasId {
}

export class Behavior extends ent.StatelessEntity {
  static ensureIsInstanceOf(obj: Behavior | BehaviorInit): Behavior {
    if( obj instanceof Behavior ) {
      return obj;
    }
    return new Behavior(obj);
  }

  public static OBJECT_GROUP = 'behaviors';
  static registrator(game: GameData, o: Behavior | BehaviorInit): Behavior {
    const obj = Behavior.ensureIsInstanceOf(o);
    return obj.initialize(game);
  }

  static initializeBehaviors(target: ent.Entity) {
    target.logger.debug(`${target.id}: initializeBehaviors()`);
    target.state.behaviors = target.state.behaviors ?? new Map<string, BehaviorState>();
    // register all behaviors
    for(let [behaviorId, ] of target.state.behaviors) {
      Behavior.registerBusListener(target, behaviorId);
    }
  }

  static getState(target: ent.Entity, id: string): BehaviorState | null {
    if( target != null && target.state != null && target.state.behaviors != null ) {
      return target.state.behaviors.get(id);
    } else {
      return null;
    }
  }

  static registerBusListener(target: ent.Entity, behaviorId: string): result.ResultObject {
    const behavior = target.game.instances.behaviors.get(behaviorId);
    if( behavior == null ) {
      return result.make(result.fail, `No behavior with ID '${behaviorId}'`);
    }

    if( Behavior.getState(target, behaviorId) == null ) {
      return result.make(result.fail, `No state for behavior '${behaviorId}' exists on entity ${target.id}`);
    }

    const channel = behavior.busChannel ?? target.game.bus.notifications;
    const disposer = channel.subscribe({
      receiver: target,
      filter: (m) => {
        const subscription = ent.getProperties(target, Behavior.OBJECT_GROUP, behaviorId).disposer;
        const state = Behavior.getState(target, behaviorId);
        if( state == null ?? subscription == null ) {
          target.logger.debug(`Deregistered behavior '${behaviorId}' activated on entity ${target.id}`);
          return false;
        }

        if( state?.status === BehaviorStatus.Disabled ) {
          return false;
        }

        return result.isAccepted(behavior.filter(target, Behavior.getState(target, behaviorId), m));
      },
      handler: (m) =>  {
        behavior.process(target, Behavior.getState(target, behaviorId), m);
      },
      disposeOnReset: true,
      allowAsync: behavior.supportsAsync,
    });

    if( ent.getProperties(target, Behavior.OBJECT_GROUP, behaviorId).disposer != null ) {
      target.logger.warn(`Disposer for '${behaviorId}' already exists on entity ${target.id}`);
    }

    ent.getProperties(target, Behavior.OBJECT_GROUP, behaviorId).disposer = disposer;

    return result.make(result.ok);
  }

  static unregisterBusListener(target: ent.Entity, behaviorId: string) {
    const disposer = ent.getProperties(target, Behavior.OBJECT_GROUP, behaviorId).disposer;
    if( disposer != null ) {
      ent.getProperties(target, Behavior.OBJECT_GROUP, behaviorId).disposer = null;
      disposer.dispose();
    }
  }

  static handleSetup(target: ent.Entity, behaviorId: string): result.ResultObject {
    const behavior = target.game.instances.behaviors.get(behaviorId);
    if( behavior == null ) {
      return result.make(result.fail, `No behavior with ID '${behaviorId}'`);
    }

    if( behavior.setup != null ) {
      behavior.setup(target, Behavior.getState(target, behaviorId));
    }

    return result.make(result.ok);
  }

  static registerForActivation(target: ent.Entity, behaviorId: string, initialState: BehaviorState): result.ResultObject {
    const behavior = target.game.instances.behaviors.get(behaviorId);
    if( behavior == null ) {
      return result.make(result.fail, `No behavior with ID '${behaviorId}'`);
    }

    if( behavior.activate != null ) {
      target.registerOnStartInitializer(() => behavior.activate(target, initialState));
    }

    return result.make(result.ok);
  }

  static handleActivate(target: ent.Entity, behaviorId: string, initialState: BehaviorState): result.ResultObject {
    const behavior = target.game.instances.behaviors.get(behaviorId);
    if( behavior == null ) {
      return result.make(result.fail, `No behavior with ID '${behaviorId}'`);
    }

    if( behavior.activate != null ) {
      behavior.activate(target, initialState);
    }

    return result.make(result.ok);
  }

  static add(target: ent.Entity, behaviorId: string, behaviorInit?: BehaviorStateInit<BehaviorState>) {
    if( !target.state.behaviors.has(behaviorId) ) {
      target.logger.debug(`Adding behavior ${behaviorId} (${behaviorInit})`);
      const initialState = (behaviorInit != null) ? (behaviorInit.initialState ?? {}) : {};
      target.state.behaviors.set(behaviorId, initialState);
      Behavior.registerSubProcessFunction(behaviorInit, target, behaviorId);
      Behavior.registerBusListener(target, behaviorId);
      Behavior.handleSetup(target, behaviorId);
      target.game.bus.notifications.publish(broadcast(Topics.BEHAVIOR_ADDED, { entityId: target.id, behaviorId: behaviorId }, undefined, target.id));
    }
  }

  static addFromProperties(target: ent.Entity, properties: BehaviorProperties) {
    if( properties != null && properties.behaviors != null ) {
      for(let [behaviorId, behavior] of properties.behaviors) {
        Behavior.add(target, behaviorId, behavior);
      }
    }
  }

  static remove(target: ent.Entity, behaviorId: string) {
    target.logger.debug(`Removing behavior ${behaviorId}`);
    Behavior.unregisterBusListener(target, behaviorId);
    target.game.bus.notifications.publish(broadcast(Topics.BEHAVIOR_REMOVED, { entityId: target.id, behaviorId: behaviorId }, undefined, target.id));
    target.state.behaviors.delete(behaviorId);
  }

  static removeAll(target: ent.Entity) {
    target.logger.debug(`Clearing all behaviors`);
    const behaviors = Array.from<string>(target.state.behaviors.keys());
    for(let behaviorId of behaviors) {
      Behavior.remove(target, behaviorId);
    }
  }

  static enable(target: ent.Entity, behaviorId: string) {
    target.logger.debug(`Enabling behavior ${behaviorId}`);
    const state = Behavior.getState(target, behaviorId);
    if( state == null ) {
      target.logger.console.warn(`Behavior '${behaviorId}' has no state on entity ${target.id}`);
      return;
    }

    state.status = BehaviorStatus.Enabled;
    target.game.bus.notifications.publish(broadcast(Topics.BEHAVIOR_ENABLED, { entityId: target.id, behaviorId: behaviorId }, undefined, target.id));
  }

  static disable(target: ent.Entity, behaviorId: string) {
    target.logger.debug(`Disabling behavior ${behaviorId}`);
    const state = Behavior.getState(target, behaviorId);
    if( state == null ) {
      target.logger.console.warn(`Behavior '${behaviorId}' has no state on entity ${target.id}`);
      return;
    }

    state.status = BehaviorStatus.Disabled;
    target.game.bus.notifications.publish(broadcast(Topics.BEHAVIOR_DISABLED, { entityId: target.id, behaviorId: behaviorId }, undefined, target.id));
  }

  static enableAll(target: ent.Entity) {
    target.logger.debug(`Enabling all behaviors`);
    const behaviors = Array.from<string>(target.state.behaviors.keys());
    for(let behaviorId of behaviors) {
      Behavior.enable(target, behaviorId);
    }
  }

  static disableAll(target: ent.Entity) {
    target.logger.debug(`Disabling all behaviors`);
    const behaviors = Array.from<string>(target.state.behaviors.keys());
    for(let behaviorId of behaviors) {
      Behavior.disable(target, behaviorId);
    }
  }

  static has(target: ent.Entity, behaviorId: string) {
    return target.state.behaviors.has(behaviorId);
  }

  init: BehaviorInit;

  constructor(init: BehaviorInit | null = null) {
    super(init, Behavior.OBJECT_GROUP);
  }

  initialize(game: GameData, init?: BehaviorInit) {
    super.initialize(game, init);
    return this;
  }

  /**
   * Bus channel to subscribe to.
   *
   * Defaults to notifications if null is returned.
   *
   * @readonly
   * @memberof Behavior
   */
  get busChannel(): Channel | null {
    return null;
  }

  get supportsAsync(): boolean {
    return true;
  }

  setup(target: ent.Entity, state: BehaviorState | null) {
    // handle behavior setup
  }

  activate(target: ent.Entity, state: BehaviorState | null) {
    if( this.init.activate != null ) {
      return this.init.activate(target, state);
    }
  }

  filter(target: ent.Entity, state: BehaviorState | null, message: Message): result.ResultObject {
    if( this.init.filter != null ) {
      return this.init.filter(target, state, message);
    }
    return result.make(result.accepted);
  }

  process(target: ent.Entity, state: BehaviorState | null, message: Message) {
    if( this.init.process != null ) {
      this.init.process(target, state, message);
    }
  }

  /**
   * This method allows to perform instance specific operations on subclassed behaviors.
   *
   * Let's say we have have behavior which does some things, and we need particular instance of the behavior e.g. report different string or something,
   * so we use subProcess() to handle that. That way we can have one behavior and customize its instances.
   *
   * @param {ent.Entity} target entity
   * @param {*} state storage
   * @param {Message} message object
   * @returns {*}
   * @memberof Behavior
   */
  subProcess(target: ent.Entity, state: BehaviorState | null, message: Message): any {
    if( this.init.subProcess != null ) {
      return this.init.subProcess(target, state, message);
    }
    return null;
  }

  private static registerSubProcessFunction(behaviorInit: BehaviorStateInit<any> | undefined, target: ent.Entity, behaviorId: string) {
    if( behaviorInit != null ) {
      const behavior = target.game.instances.behaviors.get(behaviorId);
      if( behavior != null ) {
        behavior.init.subProcess = behaviorInit.subProcess;
      }
    }
  }
}
