import {GameData} from 'snowatch/game-data';
import * as ent from 'snowatch/entity';

export interface PrepositionNounCase {
  preposition: Preposition | null;
  adjective: Adjective | null;
  adjectiveCase: string | null;
  noun: Noun | null;
  nounCase: string | null;
}

export interface AdverbNounCase {
  adverb: Adverb | null;
  adjective: Adjective | null;
  adjectiveCase: string | null;
  noun: Noun | null;
  nounCase: string | null;
}

export interface PronounNounCase {
  pronoun: Pronoun | null;
  pronounCase: string | null;
  adjective: Adjective | null;
  adjectiveCase: string | null;
  noun: Noun | null;
  nounCase: string | null;
}

export enum CapitalizationOptions {
  keep = 'keep',
  capitalize = 'capitalize',
}

export interface EntityTitleData {
  noun: Noun | null;
  adjective: Adjective | null;
  title: string;
}

export function compose(template: string, templateBag: any = null) {
  if (templateBag == null) {
    return template;
  }

  let regex = /\${([^}]+)}/g;
  let result = template;
  let matches: RegExpMatchArray | null;
  while ((matches = regex.exec(template)) != null) {
    result = result.replace(matches[0], `${templateBag[matches[1]]}`);
  }

  // trim the double spaces in case there was empty value in template bag
  return result.trim().replace('  ', ' ');
}

export function capitalFirst(text: string) {
  return text && text[0].toUpperCase() + text.slice(1);
}

export function mergeList(items: Array<string>, separator: string, conjunction: string): string {
    let merged = '';
    if( items.length === 1 ) {
      merged = items[0];
    } else {
      merged = items.slice(0, -1).join(separator);
      merged = merged.concat(conjunction).concat(items[items.length - 1]);
    }
    return merged;
}

export function getPrepositionNounCase(game: GameData, prepositionId: string | null, adjectiveId: string | null, nounId: string | null): PrepositionNounCase {
  const preposition: Preposition | null = prepositionId != null ? Preposition.get(game, prepositionId) : null;
  const requiredCase = (preposition != null) ? preposition.requiredCase() : null;
  const requiredCaseId = (requiredCase != null) ? requiredCase : Case.nominative;
  const adjective: Adjective | null = adjectiveId != null ? Adjective.get(game, adjectiveId) : null;
  const noun: Noun | null = nounId != null ? Noun.get(game, nounId) : null;
  const nounCase: string | null = noun != null ? noun.noun(requiredCaseId) : null;
  const adjectiveCase: string | null = (adjective != null && noun != null)
    ? adjective.adjective(noun.number, requiredCaseId, noun.gender, noun.genderSubtype )
    : null;
  return { preposition, adjective, adjectiveCase, noun, nounCase };
}

export function getAdverbNounCase(game: GameData, adverbId: string | null, adjectiveId: string | null, nounId: string | null): AdverbNounCase {
  const adverb: Adverb | null = adverbId != null ? Adverb.get(game, adverbId) : null;
  const requiredCase = (adverb != null) ? adverb.requiredCase() : null;
  const requiredCaseId = (requiredCase != null) ? requiredCase : Case.accusative;
  const adjective: Adjective | null = adjectiveId != null ? Adjective.get(game, adjectiveId) : null;
  const noun: Noun | null = nounId != null ? Noun.get(game, nounId) : null;
  const nounCase = noun != null ? noun.noun(requiredCaseId) : null;
  const adjectiveCase: string | null = (adjective != null && noun != null)
    ? adjective.adjective(noun.number, requiredCaseId, noun.gender, noun.genderSubtype )
    : null;
  return { adverb, adjective, adjectiveCase, noun, nounCase };
}

export function getPronounNounCase(game: GameData, pronounId: string | null, adjectiveId: string | null, nounId: string | null, requiredCase: Case = Case.accusative): PronounNounCase {
  const adjective: Adjective | null = adjectiveId != null ? Adjective.get(game, adjectiveId) : null;
  const noun: Noun | null = nounId != null ? Noun.get(game, nounId) : null;
  const pronoun: Pronoun | null = pronounId != null ? Pronoun.get(game, pronounId) : null;
  const nounCase = noun != null ? noun.noun(requiredCase) : null;
  const adjectiveCase: string | null = (adjective != null && noun != null)
  ? adjective.adjective(noun.number, requiredCase, noun.gender, noun.genderSubtype)
  : null;
  const pronounCase: string | null = pronoun != null && noun != null ? pronoun.pronoun(noun, requiredCase) : null;
  return { pronoun, pronounCase, adjective, adjectiveCase, noun, nounCase };
}

export function getTitleData(game: GameData, titleTemplate: string, nounId: string, adjectiveId: string, caseId: Case = Case.nominative, capitalizationOptions: CapitalizationOptions = CapitalizationOptions.capitalize): EntityTitleData {
  const adjective: Adjective | null = adjectiveId != null ? Adjective.get(game, adjectiveId) : null;
  const noun: Noun | null = nounId != null ? Noun.get(game, nounId) : null;
  let title = compose(titleTemplate, {
    adjective: (adjective != null && noun != null) ? adjective.adjective(noun.number, caseId, noun.gender, noun.genderSubtype) : '',
    noun: (noun != null) ? noun.noun(caseId) : '',
  });
  switch( capitalizationOptions ) {
    case CapitalizationOptions.capitalize:
      title = capitalFirst(title);
      break;
    case CapitalizationOptions.keep:
    default:
      break;
  }

  return {
    noun: noun,
    adjective: adjective,
    title: title
   };
}


export class LanguageStructure {
  public static OBJECT_GROUP = 'languageStructures';
  static registrator(game: GameData, o: LanguageStructure | NounInit | VerbInit | AdjectiveInit | ConjunctionInit): LanguageStructure | null {
    let objId: string | null = null;
    let langObj: LanguageStructure | null = null;
    if (o instanceof LanguageStructure) {
      objId = o.id;
      langObj = o;
    } else if (o.hasOwnProperty(Noun.ID)) {
      objId = (<NounInit>o)[Noun.ID];
      langObj = new Noun(<NounInit>o);
    } else if (o.hasOwnProperty(Verb.ID)) {
      objId = (<VerbInit>o)[Verb.ID];
      langObj = new Verb(<VerbInit>o);
    } else if (o.hasOwnProperty(Adjective.ID)) {
      objId = (<AdjectiveInit>o)[Adjective.ID];
      langObj = new Adjective(<AdjectiveInit>o);
    } else if (o.hasOwnProperty(Conjunction.ID)) {
      objId = (<ConjunctionInit>o)[Conjunction.ID];
      langObj = new Conjunction(<ConjunctionInit>o);
    }
    if (objId != null && langObj != null) {
      game.languageStructures.set(objId, langObj);
    }
    return langObj;
  }

  static find(game: GameData, key: string) {
    return game.languageStructures.get(key);
  }

  get id(): string {
    return (<any>this.constructor).ID;
  }
}

export enum Number {
  plural = 'plural',
  singular = 'singular',
}

export enum Case {
  nominative = 'nominative',
  genitive = 'genitive',
  dative = 'dative',
  accusative = 'accusative',
  vocative = 'vocative',
  locative = 'locative',
  instrumental = 'instrumental',
}

export enum Gender {
  masculine = 'masculine',
  feminine = 'feminine',
  neutral = 'neutral',
}

export enum GenderSubtype {
  animate = 'animate',
  inanimate = 'inanimate',
}

export enum Tense {
  present = 'present',
  past = 'past',
  future = 'future',
  passive = 'passive',
  imperative = 'imperative',
  participle = 'participle',
}

export enum Person {
  first = 'first',
  second = 'second',
  third = 'third',
}

export interface NounInit {
  [index: string]: any;
  noun: string;
  gender?: Gender;
  genderSubtype?: GenderSubtype;
  number?: Number;
  nominative?: string;
  genitive?: string;
  dative?: string;
  accusative?: string;
  vocative?: string;
  locative?: string;
  instrumental?: string;
}

export class Noun extends LanguageStructure {
  public static readonly ID: string = 'noun';

  static get(game: GameData, nounId: string): Noun {
    let noun = LanguageStructure.find(game, nounId);
    if (noun == null) {
      noun = ent.registerEntities(game, LanguageStructure.OBJECT_GROUP, { noun: nounId, singular: nounId }, LanguageStructure.registrator);
    }
    return <Noun>noun;
  }

  private _init: NounInit;

  constructor(init: NounInit) {
    super();
    this._init = init;
  }

  noun(nounCase?: string): string {
    const num = this._init;
    if (nounCase != null && num.hasOwnProperty(nounCase)) {
      return num[nounCase];
    } else {
      return num.noun;
    }
  }

  get number(): Number {
    return this._init.number ?? Number.singular;
  }

  get gender(): Gender {
    return this._init.gender ?? Gender.masculine;
  }

  get genderSubtype(): GenderSubtype {
    // this is kind of czech specific, as only masculine nouns have subtypes, but for most purposes we need to return inanimate for feminine/neutral type to match rules properly
    return this._init.genderSubtype ?? (this.gender === Gender.masculine ? GenderSubtype.animate : GenderSubtype.inanimate);
  }
}

export interface AdjectiveGenderSubTypeInit {
  [index: string]: any;
  animate?: string;
  inanimate?: string;
}

export interface AdjectiveCasesInit {
  [index: string]: any;
  nominative: AdjectiveGenderSubTypeInit | string;
  genitive: AdjectiveGenderSubTypeInit | string;
  dative: AdjectiveGenderSubTypeInit | string;
  accusative: AdjectiveGenderSubTypeInit | string;
  vocative: AdjectiveGenderSubTypeInit | string;
  locative: AdjectiveGenderSubTypeInit | string;
  instrumental: AdjectiveGenderSubTypeInit | string;
}

export interface AdjectiveGenderInit {
  [index: string]: any;
  masculine: AdjectiveCasesInit | string;
  feminine: AdjectiveCasesInit | string;
  neutral: AdjectiveCasesInit | string;
}

export interface AdjectiveInit {
  [index: string]: any;
  adjective: string;
  singular: AdjectiveGenderInit | AdjectiveCasesInit | string;
  plural: AdjectiveGenderInit | AdjectiveCasesInit | string;
}

export class Adjective extends LanguageStructure {
  public static readonly ID: string = 'adjective';

  static get(game: GameData, adjectiveId: string): Adjective {
    let adjective = LanguageStructure.find(game, adjectiveId);
    if (adjective == null) {
      adjective = ent.registerEntities(game, LanguageStructure.OBJECT_GROUP, { adjective: adjectiveId }, LanguageStructure.registrator);
    }
    return <Adjective>adjective;
  }

  private _init: AdjectiveInit;

  constructor(init: AdjectiveInit) {
    super();
    this._init = init;
  }

  adjective(adjectiveNumber: Number, adjectiveCase: Case, adjectiveGender?: Gender, adjectiveGenderSubType?: GenderSubtype): string {
    if (this._init.hasOwnProperty(adjectiveNumber)) {
      let adj: any = this._init[adjectiveNumber];
      // handle adjectives with cases
      if (adj != null && adjectiveGender != null && adj.hasOwnProperty(adjectiveGender)) {
        adj = adj[adjectiveGender];
      }
      // now we either have case or number
      if (adj != null && adjectiveCase != null && adj.hasOwnProperty(adjectiveCase)) {
        adj = adj[adjectiveCase];
        if (adj != null && adjectiveGenderSubType != null && adj.hasOwnProperty(adjectiveGenderSubType)) {
          adj = adj[adjectiveGenderSubType];
          return adj;
        } else {
          return adj;
        }
      } else {
        return adj;
      }
    } else {
      return this._init.adjective;
    }
  }
}

export interface VerbTenseInit {
  // some verbs in czech do not have present tense, there is alternative form for that
  present?: VerbPersonInit | string;
  past?: VerbPersonInit | string;
  future?: VerbPersonInit | string;
  passive?: VerbPersonInit | string;
  imperative?: VerbPersonInit | string;
  participle?: VerbGenderInit | string;
}

export interface VerbPersonInit {
  first?: VerbGenderInit | string;
  second?: VerbGenderInit | string;
  third?: VerbGenderInit | string;
}

export interface VerbGenderInit {
  masculine: VerbGenderSubTypeInit | string;
  feminine: VerbGenderSubTypeInit | string;
  neutral: VerbGenderSubTypeInit | string;
}

export interface VerbGenderSubTypeInit {
  animate: string;
  inanimate: string;
}

export interface VerbInit {
  [index: string]: any;

  verb: string;
  infinitive: string;
  singular?: VerbTenseInit | string;
  plural?: VerbTenseInit | string;
}

export class Verb extends LanguageStructure {
  public static readonly ID: string = 'verb';

  static get(game: GameData, verbId: string): Verb {
    let verb = LanguageStructure.find(game, verbId);
    if (verb == null) {
      verb = ent.registerEntities(game, LanguageStructure.OBJECT_GROUP, { verb: verbId, infinitive: verbId }, LanguageStructure.registrator);
    }
    return <Verb>verb;
  }

  private _init: VerbInit;

  constructor(init: VerbInit) {
    super();
    this._init = init;
  }

  verb(verbNumber?: Number, verbTense?: Tense, verbPerson?: Person, verbGender?: Gender, verbGenderSubType?: GenderSubtype): string {
    if (verbNumber != null && this._init.hasOwnProperty(verbNumber)) {
      let verb: any  = this._init[verbNumber];
      if (verb != null && verbTense != null && verb.hasOwnProperty(verbTense)) {
        verb = verb[verbTense];
        if (verb != null && verbPerson != null && verb.hasOwnProperty(verbPerson)) {
          verb = verb[verbPerson];
          if (verb != null && verbGender != null && verb.hasOwnProperty(verbGender)) {
            verb = verb[verbGender];
            if (verb != null && verbGenderSubType != null && verb.hasOwnProperty(verbGenderSubType)) {
              return verb[verbGenderSubType];
            } else {
              return verb;
            }
          } else {
            return verb;
          }
        } else {
          return verb;
        }
      } else {
        return verb;
      }
    } else {
      return this._init.infinitive;
    }
  }

  get infinitive(): string {
    return this._init.infinitive;
  }
}

export interface ConjunctionInit {
  [index: string]: any;
  conjunction: string;
}

export class Conjunction extends LanguageStructure {
  public static readonly ID: string = 'conjunction';

  static get(game: GameData, conjunctionId: string): Conjunction {
    let conjunction = LanguageStructure.find(game, conjunctionId);
    if (conjunction == null) {
      conjunction = ent.registerEntities(game, LanguageStructure.OBJECT_GROUP, { conjunction: conjunctionId }, LanguageStructure.registrator);
    }
    return <Conjunction>conjunction;
  }

  private _init: ConjunctionInit;

  constructor(init: ConjunctionInit) {
    super();
    this._init = init;
  }

  conjunction(): string {
    return this._init.conjunction;
  }
}

export interface PronounCasesInit {
  nominative: PronounPersonInit | PronounGenderInit | string;
  genitive: PronounPersonInit | PronounGenderInit | string;
  dative: PronounPersonInit | PronounGenderInit | string;
  accusative: PronounPersonInit | PronounGenderInit | string;
  vocative: PronounPersonInit | PronounGenderInit | string;
  locative: PronounPersonInit | PronounGenderInit | string;
  instrumental: PronounPersonInit | PronounGenderInit | string;
}

export interface PronounPersonInit {
  first: PronounGenderInit | string;
  second: PronounGenderInit | string;
  third: PronounGenderInit | string;
}

export interface PronounGenderInit {
  masculine: PronounGenderSubTypeInit | string;
  feminine: PronounGenderSubTypeInit | string;
  neutral: PronounGenderSubTypeInit | string;
}

export interface PronounGenderSubTypeInit {
  animate?: string;
  inanimate?: string;
}

export interface PronounInit {
  [index: string]: any;
  pronoun: string;
  singular: PronounCasesInit | string;
  plural: PronounCasesInit | string;
}

export class Pronoun extends LanguageStructure {
  public static readonly ID: string = 'pronoun';

  static get(game: GameData, pronounId: string): Pronoun {
    let pronoun = LanguageStructure.find(game, pronounId);
    if (pronoun == null) {
      pronoun = ent.registerEntities(game, LanguageStructure.OBJECT_GROUP, { pronoun: pronounId, singular: pronounId }, LanguageStructure.registrator);
    }
    return <Pronoun>pronoun;
  }


  private _init: PronounInit;

  constructor(init: PronounInit) {
    super();
    this._init = init;
  }

  pronoun(noun: Noun, pronounCase: Case, pronounPerson?: Person): string {
    if (this._init.hasOwnProperty(noun.number)) {
      let pronoun: any = this._init[noun.number];
      if (pronoun != null && pronounCase != null && pronoun.hasOwnProperty(pronounCase)) {
        pronoun = pronoun[pronounCase];
        if (pronoun != null && pronounPerson != null && pronoun.hasOwnProperty(pronounPerson)) {
          pronoun = pronoun[pronounCase];
          if (noun.gender != null && this._init.hasOwnProperty(noun.gender)) {
            pronoun = pronoun[noun.gender];
            if (pronoun != null && noun.genderSubtype != null && pronoun.hasOwnProperty(noun.genderSubtype)) {
              return pronoun[noun.genderSubtype];
            } else {
              return pronoun;
            }
          } else {
            return pronoun;
          }
        } else if (noun.gender != null && pronoun.hasOwnProperty(noun.gender)) {
          pronoun = pronoun[noun.gender];
          if (pronoun != null && noun.genderSubtype != null && pronoun.hasOwnProperty(noun.genderSubtype)) {
            return pronoun[noun.genderSubtype];
          } else {
            return pronoun;
          }
        } else {
          return pronoun;
        }
      } else {
        return pronoun;
      }
    } else {
      return this._init.pronoun;
    }
  }
}

export interface PrepositionInit {
  [index: string]: any;
  preposition: string;
  requiredCase?: Case;
}

export class Preposition extends LanguageStructure {
  public static readonly ID: string = 'preposition';

  static get(game: GameData, prepositionId: string): Preposition {
    let preposition = LanguageStructure.find(game, prepositionId);
    if (preposition == null) {
      preposition = ent.registerEntities(game, LanguageStructure.OBJECT_GROUP, { preposition: prepositionId }, LanguageStructure.registrator);
    }
    return <Preposition>preposition;
  }

  private _init: PrepositionInit;

  constructor(init: PrepositionInit) {
    super();
    this._init = init;
  }

  preposition(): string {
    return this._init.preposition;
  }

  requiredCase(): Case | null | undefined {
    return this._init.requiredCase;
  }
}

export enum AdverbPosition {
 begin = 'begin',
 middle = 'middle',
 end = 'end',
}

export interface AdverbInit {
  [index: string]: any;
  adverb: string;
  requiredCase?: Case;
  position?: AdverbPosition;
}

export class Adverb extends LanguageStructure {
  public static readonly ID: string = 'adverb';

  static get(game: GameData, adverbId: string): Adverb {
    let adverb = LanguageStructure.find(game, adverbId);
    if (adverb == null) {
      adverb = ent.registerEntities(game, LanguageStructure.OBJECT_GROUP, { preposition: adverbId }, LanguageStructure.registrator);
    }
    return <Adverb>adverb;
  }

  private _init: AdverbInit;

  constructor(init: AdverbInit) {
    super();
    this._init = init;
  }

  adverb(): string {
    return this._init.adverb;
  }

  requiredCase(): Case | null | undefined {
    return this._init.requiredCase;
  }

  position(): AdverbPosition | null | undefined {
    return this._init.position;
  }
}

