
export interface ComparerFn<T> {
  (first:T, second:T): number;
}

export class PriorityQueue<T> {
  private _array: Array<T>;
  private _comparer: ComparerFn<T>;

  constructor(comparer?: ComparerFn<T>) {
    if( comparer != null ) {
      this._comparer = comparer;
    } else {
      this._comparer = (f: T, s: T): number => {
        if( f > s ) {
          return 1;
        } else if( f < s ) {
          return -1;
        } else {
          return 0;
        }
      };
    }
    this.clear();
  }

  push(item: T) {
    if( this._array.length === 0 ) {
      this._array.push(item);
      return;
    }
    if( this._comparer(item, this._array[0]) <= 0 ) {
      this._array.unshift(item);
      return;
    }
    if( this._comparer(item, this._array[this._array.length - 1]) >= 0 ) {
      this._array.push(item);
      return;
    }

    let offset = (this._array.length) >>> 1;
    let step = (offset >>> 1) || 1;
    do {
      const comp = this._comparer(item, this._array[offset]);
      const compPrev = this._comparer(item, this._array[offset - 1]);
      if(  compPrev >= 0 && comp <= 0 ) {
        break;
      } else if( comp > 0 ) {
        offset += step;
      } else if( comp < 0 ) {
        offset -= step;
      }
      step = (step >>> 1) || 1;
    } while(true);

    this._array.splice(offset, 0, item);
  }

  pop(): T | undefined {
    return this._array.shift();
  }

  peek(): T {
    return this._array[0];
  }

  clear() {
    this._array = new Array<T>();
  }

  isEmpty() {
    return this._array.length === 0;
  }
}