import {GameData} from 'snowatch/game-data';
import {Message} from 'snowatch/message';
import {Realm} from 'snowatch/realm';
import {Disposable, StatelessEntity} from 'snowatch/entity';
import {TimeSource} from 'snowatch/time-source';
import {Subject, Subscription} from 'rxjs';
import {filter} from 'rxjs/operators';

import {LogManager} from 'aurelia-framework';
const busLogger = LogManager.getLogger('snowatch-bus-log');

export interface PublishOptions {
  allowAsync?: boolean;
  addTimestamp?: boolean;
  debugLog?: boolean;
  freeze?: boolean;
}

export interface SubscribeArgs {
  receiver?: StatelessEntity;
  handler: HandlerFn;
  filter?: FilterFn;
  disposeOnReset?: boolean;
  allowAsync?: boolean;
}

export enum AvailableChannels {
  commands = 'commands',
  system = 'system',
  notifications = 'notifications',
  input = 'input',
}

export class Bus {
  private _game: GameData;
  private _commands: Channel;
  private _system: Channel;
  private _notifications: Channel;
  private _input: Channel;

  constructor(game: GameData) {
    this._game = game;
    this._commands = new Channel(this._game, 'commands');
    this._system = new Channel(this._game, 'system');
    this._notifications = new Channel(this._game, 'notifications');
    this._input = new Channel(this._game, 'input');
  }

  get commands() {
    return this._commands;
  }

  get system() {
    return this._system;
  }

  get notifications() {
    return this._notifications;
  }

  get input() {
    return this._input;
  }

  public fromType(channel: AvailableChannels): Channel {
    switch( channel ) {
      case AvailableChannels.commands:
        return this._commands;
      case AvailableChannels.input:
        return this._input;
      case AvailableChannels.notifications:
        return this._notifications;
      case AvailableChannels.system:
        return this._system;
                }
  }
}

export interface HandlerFn {
  (message: Message): void;
}

export interface FilterFn {
  (message: Message): boolean;
}

enum SubscriptionDisposerBehavior {
  Manual,
  OnReset,
}

export class SubscriptionDisposer implements Disposable {
  private _subscription: Subscription;
  private _game: GameData;
  private _behavior: SubscriptionDisposerBehavior;
  constructor(game: GameData, subscription: Subscription, behavior: SubscriptionDisposerBehavior) {
    this._game = game;
    this._subscription = subscription;
    this._behavior = behavior;
    if( this._behavior === SubscriptionDisposerBehavior.OnReset ) {
      this._game.disposeOnReset(this);
    }
  }

  dispose() {
    if( this._behavior === SubscriptionDisposerBehavior.OnReset ) {
      this._game.removeDisposer(this);
    }
    this._subscription.unsubscribe();
  }
}

export class Channel {
  private _game: GameData;
  private _name: string;
  private _subject: Subject<Message>;

  constructor(game: GameData, name: string) {
    this._game = game;
    this._name = name;
    this._subject = new Subject<Message>();
      this._subject.subscribe(
        m => {
          if( m.shouldBeAddedToDebugLog ) { busLogger.debug(`[${this._name}] ${m.topicId}`, m); }
        },
        e => busLogger.debug(`[${this._name}.error]`, e),
        () => busLogger.debug(`[${this._name}.completed]`)
      );
  }

  subscribe(args: SubscribeArgs): Disposable {
    args.disposeOnReset = (args.disposeOnReset != null) ? args.disposeOnReset : true;
    args.allowAsync = (args.allowAsync != null) ? args.allowAsync : true;

    let subscription: Subscription | null = null;
    if( args.allowAsync ) {
      const handler = args.handler;
      args.handler = (m) => {
        if( m.allowAsync === true ) {
          setTimeout(handler, 0, m);
        } else {
          handler(m);
        }
      };
    }

    const realmFilter: FilterFn = (m) => {
      if( m.sourceId == null || args.receiver == null) {
        return true;
      }

      const sourceRealmId = Realm.getRealmIdForEntityId(this._game, m.sourceId);
      if( sourceRealmId == null ) {
        return true;
      }

      const receiverRealmId = Realm.getRealmIdForEntity(args.receiver);
      if( receiverRealmId == null ) {
        return true;
      }

      return sourceRealmId === receiverRealmId || (m.realmIds != null && m.realmIds.includes(receiverRealmId));
    };

    if( args.filter != null ) {
      subscription = this._subject.pipe(
          filter(realmFilter),
          filter(args.filter)
        ).subscribe(args.handler);
    } else {
      subscription = this._subject.pipe(
          filter(realmFilter)
        ).subscribe(args.handler);
    }

    return new SubscriptionDisposer(
      this._game,
      subscription,
      args.disposeOnReset ? SubscriptionDisposerBehavior.OnReset : SubscriptionDisposerBehavior.Manual
    );
  }

  publish(message: Message, args: PublishOptions = {}) {
    args.allowAsync = (args.allowAsync != null) ? args.allowAsync : true;
    args.debugLog = (args.debugLog != null) ? args.debugLog : true;
    args.addTimestamp = (args.addTimestamp != null) ? args.addTimestamp : false;

    message.allowAsync = args.allowAsync;
    message.shouldBeAddedToDebugLog = args.debugLog;
    if( args.addTimestamp ) {
      message.timestamp = TimeSource.getWallClockTicks(this._game);
    }

    message.publishTimestamp = Date.now();

    // freeze the message by default
    if( args.freeze == null || args.freeze === true ) {
      Object.freeze(message);
    }

    this._subject.next(message);
  }
}
