import {GameData} from 'snowatch/game-data';
import {Message, broadcast, message} from 'snowatch/message';
import {Topics} from 'snowatch/message-topics/all';
import {Item} from 'snowatch/item';
import {Actor} from 'snowatch/actor';
import {Substance} from 'snowatch/substance';
import {Behavior} from 'snowatch/behavior';
import {evaluate} from 'snowatch/utils';
import {Surface} from 'snowatch/surface';
import {Vector3, Size3} from 'snowatch/spatial-relations';
import {GraphNode, Graph, GraphLink, PathElement, LinkNeighborPair} from 'snowatch/pathfinding';
import {ResourceDictionary} from 'snowatch/resource-dictionary';
import {DefaultResDict} from 'snowatch/resources/default';
import {SpatialCapacityComputedAttribute} from 'snowatch/attributes/computed-spatial-capacity';
import { SpatialPrepositions } from 'snowatch/prepositions';
import * as ent from 'snowatch/entity';
import * as result from 'snowatch/result';
import * as attrs from 'snowatch/attributes-ids';
import * as lang from 'snowatch/language-structures';
import * as spatial from 'snowatch/spatial-relations';

export interface ExitAvailableFn {
  (exit: Exit, entity: ent.Entity): result.ResultObject;
}

export interface ExitVisibleFn {
  (exit: Exit, entity: ent.Entity): boolean;
}

export interface ExitSceneFn {
  (exit: Exit, entity: ent.Entity): string;
}

export interface ExitResultCallbackFn {
  (exit: Exit, entity: ent.Entity): void;
}

export interface ExitTitleGetterFn {
  (exit: Exit, entity: ent.Entity): string;
}

export interface ExitSoundAttenuationFn {
  (exit: Exit, entity: ent.Entity): number;
}

export interface ExitOpacityFn {
  (exit: Exit, entity: ent.Entity): number;
}

export interface Exit extends GraphLink {
  id: string;
  title: ExitTitleGetterFn | string;
  scene: ExitSceneFn | string;
  available?: ExitAvailableFn | result.ResultObject;
  visible?: ExitVisibleFn | boolean;
  success?: ExitResultCallbackFn;
  fail?: ExitResultCallbackFn;
  soundAttenuation?: ExitSoundAttenuationFn | number;
  opacity?: ExitOpacityFn | number;
}

export interface ScenePathElement {
  node: Scene;
  link: Exit;
}

export interface SceneLinkNeighborPair {
  link: Exit;
  neighbor: Scene;
}

export interface SceneInit extends ent.EntityInit {
  title?: string;
  description?: string;
  defaultSpot?: Vector3;
  exits: Array<Exit>;
  hasGround?: boolean;
  disablesInteractions?: boolean;
}

export function getCurrentSceneId(entity: ent.StatelessEntity): string | null {
  if( attrs.Attribute.applies(entity, attrs.CurrentSceneId) ) {
    return attrs.Attribute.value<string>(entity, attrs.CurrentSceneId);
  }

  return null;
}

export function getCurrentScene(entity: ent.StatelessEntity): Scene | null | undefined {
  if( attrs.Attribute.applies(entity, attrs.CurrentSceneId) ) {
    return entity.game.instances.scenes.get(attrs.Attribute.value<string>(entity, attrs.CurrentSceneId));
  }

  return null;
}

export function getStartSceneId(entity: ent.StatelessEntity): string | null {
  if( attrs.Attribute.applies(entity, attrs.StartSceneId) ) {
    return attrs.Attribute.value<string>(entity, attrs.StartSceneId);
  }

  return null;
}

export function setStartSceneId(entity: ent.Entity, value: string): string | null | undefined {
  if( attrs.Attribute.applies(entity, attrs.StartSceneId) ) {
    return attrs.Attribute.value<string>(entity, attrs.StartSceneId);
  }

  return null;
}

export function getStartScene(entity: ent.StatelessEntity): Scene | null | undefined {
  if( attrs.Attribute.applies(entity, attrs.StartSceneId) ) {
    return entity.game.instances.scenes.get(attrs.Attribute.value<string>(entity, attrs.StartSceneId));
  }

  return null;
}

export function setSceneId(entity: ent.Entity, sceneId: string) {
  if( !attrs.Attribute.applies(entity, attrs.CurrentSceneId) || getCurrentSceneId(entity) === sceneId) {
    return;
  }

  const oldSceneId = getCurrentSceneId(entity);
  attrs.Attribute.setValue<string>(entity, attrs.CurrentSceneId, sceneId);
  entity.game.bus.notifications.publish(broadcast(
    Topics.ENTITY_SCENE_CHANGED,
    {
      entityId: entity.id,
      oldSceneId: oldSceneId,
      newSceneId: sceneId
    }
  ));
}

export function getDefaultSpot(entity: Scene): Vector3 | null {
  if( attrs.Attribute.applies(entity, attrs.SceneDefaultSpot) ) {
    return attrs.Attribute.value<Vector3>(entity, attrs.SceneDefaultSpot);
  }

  return null;
}

export class Scene extends ent.Entity implements GraphNode {
   static ensureIsInstanceOf(obj: SceneInit | Scene): Scene {
     if( obj instanceof Scene ) {
       return obj;
     }
     return new Scene(obj);
   }

  public static readonly GROUND = 'ground';
  public static readonly EXIT_VISIBLE_PROP = 'visible';

  public static OBJECT_GROUP = 'scenes';
  static registrator(game: GameData, scene: SceneInit | Scene) {
    const sceneObj = Scene.ensureIsInstanceOf(scene);
    return sceneObj.initialize(game);
  }

  private _exits: Map<string, Exit>;
  init: SceneInit;

  constructor(init: SceneInit | null = null) {
    super(init, Scene.OBJECT_GROUP);
  }

  initialize(game: GameData, init?: SceneInit) {
    super.initialize(game, init);
    this.exits = this.init.exits;
    this.state.visitCounts = new Map<string, number>();

    this.registerInitializer(() => {
      attrs.Attribute.initializeAttributes(this);
      attrs.Attribute.addFromProperties(this, this.properties);
      attrs.Attribute.addConstFromProperties(this, this.properties);
      attrs.Attribute.addCompFromProperties(this, this.properties);

      attrs.Attribute.addComp(this, attrs.Title, { compState: {
        value: (entity, attributeId, actor?: ent.Entity) => {
          return ent.getTitle(this).title;
        }
      }});
      attrs.Attribute.add(this, attrs.Description, { initialState: {value: this.init.description ?? "..."}});
      if( this.init.defaultSpot != null ) {
        attrs.Attribute.add(this, attrs.SceneDefaultSpot, { initialState: {value: this.init.defaultSpot}});
      }
    });

    this.registerInitializer(() => {
      Behavior.initializeBehaviors(this);
      if( this.properties.behaviors != null ) {
        ent.registerEntities(this.game, Behavior.OBJECT_GROUP, Array.from(this.properties.behaviors.values()), Behavior.registrator);
        Behavior.addFromProperties(this, this.properties);
        for(let [behaviorId, behavior] of this.properties.behaviors) {
          Behavior.registerForActivation(this, behaviorId, behavior.initialState);
        }
      }
    });

    if( this.init.hasGround !== false ) {
      this.registerInitializer(() => {
        const spatialAttributes: Array<attrs.ConstAttributeInit> = [];
        if( attrs.Attribute.has(this, attrs.SpatialLocation) ) {
          spatialAttributes.push({ id: attrs.SpatialLocation, constState: { value: attrs.Attribute.value<Vector3>(this, attrs.SpatialLocation)}});
        }
        if( attrs.Attribute.has(this, attrs.SpatialSize) ) {
          const size = { id: attrs.SpatialSize, constState: {value: attrs.Attribute.value<Vector3>(this, attrs.SpatialSize)} };
          size.constState.value.z = 0;
          spatialAttributes.push(size);
        }

        const self = this;
        ent.registerEntities(this.game,
            ent.Entity.OBJECT_GROUP,
            {
              id: self.id,
              instanceId: Scene.GROUND,
              properties: {
                compAttributes: [
                  { id: attrs.AvailableSpatialCapacity, compState: SpatialCapacityComputedAttribute.default },
                ],
                constAttributes: [
                  { id: attrs.Title, constState: { value: lang.Noun.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.GROUND_NOUN)).noun(lang.Case.nominative) } },
                  { id: attrs.NounResourceId, constState: { value: DefaultResDict.GROUND_NOUN } },
                  { id: attrs.CanBeIndirect },
                  { id: attrs.HasSpatialRelations, constState: { value: true }, },
                  { id: attrs.Surfaces, constState: { value: [Surface.outerTop] }, },
                  ...spatialAttributes
                ],
                attributes: [
                  { id: attrs.CurrentSceneId, initialState: {value: self.id} },
                ],
              },
          },
          (game: GameData, o: ent.Entity) => {
            const ground = ent.Entity.registrator(game, o);
            attrs.Attribute.initializeAttributes(ground);
            attrs.Attribute.addFromProperties(ground, ground.properties);
            attrs.Attribute.addConstFromProperties(ground, ground.properties);
            attrs.Attribute.addCompFromProperties(ground, ground.properties);
            ground.state.currentSceneId = this.id;
          }
        );
      });
    }

    return this;
  }

  get items(): Array<Item> {
    return Array.from<Item>(this.game.instances.items.values()).filter(i => getCurrentSceneId(i) === this.id);
  }

  get actors(): Array<Actor> {
    return Array.from<Actor>(this.game.instances.actors.values()).filter(a => getCurrentSceneId(a) === this.id);
  }

  get substances(): Array<Substance> {
    return Array.from<Substance>(this.game.instances.substances.values()).filter(a => getCurrentSceneId(a) === this.id);
  }

  get entities(): Array<ent.Entity> {
    return Array.from<ent.Entity>(this.game.instances.entities.values()).filter(e => getCurrentSceneId(e) === this.id);
  }

  get visitCounts(): Map<string, number> {
    return this.state.visitCounts;
 }

  get exits(): Array<Exit> {
    return Array.from<Exit>(this._exits.values());
  }

  set exits(exits: Array<Exit>) {
    this._exits = new Map<string, Exit>();
    if( exits != null ) {
      exits.forEach((exit: Exit, index: number) => {
        exit.id = `${this.id}:${index}`;
        this._exits.set(exit.id, exit);
      });
    }
  }

  get ground(): ent.Entity | undefined {
    return this.game.instances.getEntityFromId(ent.getEntityId(this.id, Scene.GROUND));
  }

  onStart(message: Message) {
    super.onStart(message);
  }

  getExit(exitId: string) {
    return this._exits.get(exitId);
  }

  canEnter(entityId: string): result.ResultObject {
    this.logger.debug(`${this.id}: canEnter(${entityId})`);
    return result.make(result.accepted);
  }

  preEnter(entityId: string) {
    this.logger.debug(`${this.id}: preEnter(${entityId})`);
  }

  onEnter(entityId: string) {
    this.logger.debug(`${this.id}: onEnter(${entityId})`);
    let count = 1;
    if( this.state.visitCounts.has(entityId) ) {
      count = (this.visitCounts.get(entityId) ?? 0) + 1;
    }

    this.state.visitCounts.set(entityId, count);

    const entity = this.game.instances.getEntityFromId(entityId);
    if( entity != null ) {
      if( attrs.Attribute.applies(this, attrs.SceneDefaultSpot) ) {
        spatial.setSpatialLocation(entity, attrs.Attribute.value<Vector3>(this, attrs.SceneDefaultSpot));
      } else if( attrs.Attribute.applies(this, attrs.SpatialLocation) ) {
        spatial.setSpatialLocation(entity, attrs.Attribute.value<Vector3>(this, attrs.SpatialLocation));
      }
      if( this.init.disablesInteractions === true ) {
        attrs.Attribute.setValue<boolean>(entity, attrs.CanUseSkills, false);
        attrs.Attribute.setValue<boolean>(entity, attrs.CanSeeInventory, false);
      }
      this.game.bus.notifications.publish(broadcast(Topics.SCENE_ENTERED, { entityId: entityId, sceneId: this.id } ));
    }

    if( this.ground != null ) {
      // only for actors
      spatial.SpatialRelations.addRelation(this.game, SpatialPrepositions.isOn.id, entityId, this.ground.id);
    }
  }

  postEnter(entityId: string) {
    this.logger.debug(`${this.id}: postEnter(${entityId})`);
  }

  canLeave(entityId: string): result.ResultObject {
    this.logger.debug(`${this.id}: canLeave(${entityId})`);
    return result.make(result.accepted);
  }

  preLeave(entityId: string) {
    this.logger.debug(`${this.id}: preLeave(${entityId})`);
  }

  onLeave(entityId: string) {
    this.logger.debug(`${this.id}: onLeave(${entityId})`);
    const entity = this.game.instances.getEntityFromId(entityId);
    if( entity != null ) {
      if( this.init.disablesInteractions === true ) {
        attrs.Attribute.setValue<boolean>(entity, attrs.CanUseSkills, true);
        attrs.Attribute.setValue<boolean>(entity, attrs.CanSeeInventory, true);
      }
      this.game.bus.notifications.publish(broadcast(Topics.SCENE_LEFT, { entityId: entityId, sceneId: this.id } ));
    }
  }

  postLeave(entityId: string) {
    this.logger.debug(`${this.id}: postLeave(${entityId})`);
  }

  static handleTryExit(game: GameData, payload: any, requestId: number = -1) {
    const actor = game.instances.actors.get(payload.entityId);
    if( actor == null ) {
      return;
    }
    const currentScene = getCurrentScene(actor);
    if( currentScene == null ) {
      return;
    }

    const exit = currentScene.getExit(payload.exitId);
    if( exit == null ) {
      return;
    }

    const res = Scene.tryTakeExit(game, actor, exit);
    const messageData = {
      entityId: payload.entityId,
      exitId: payload.exitId,
      result: res.data,
      message: res.message,
    };

    if( !result.isAccepted(res) ) {
      game.bus.notifications.publish(broadcast(Topics.EXIT_REJECTED, messageData, requestId, actor.id));
    } else {
      game.bus.notifications.publish(broadcast(Topics.EXIT_ACCEPTED, messageData, requestId, actor.id));
    }
  }

  static tryTakeExit(game: GameData, actor: Actor, exit: Exit): result.ResultObject {
    if( null == exit ) {
      return result.make(result.fail, 'Null-exit specified.');
    }

    // check for exit availability
    if( exit.available != null ) {
      const availabilityResult = evaluate(exit.available, exit, actor);
      if( result.isRejected(availabilityResult) ) {
        Scene.callFailIfDefined(exit, actor);
        return availabilityResult;
      }
    }

    // get new scene id
    if( null == exit.scene ) {
      Scene.callFailIfDefined(exit, actor);
      return result.make(result.fail, 'No scene specified.');
    }

    const sceneName = evaluate(exit.scene, exit, actor);
    const changeResult = actor.tryChangeSceneTo(sceneName);
    if( result.isAccepted(changeResult) ) {
      Scene.callSuccessIfDefined(exit, actor);
    } else {
      Scene.callFailIfDefined(exit, actor);
    }

    return changeResult;
  }

  static callSuccessIfDefined(exit: Exit, entity: ent.Entity) {
    if( exit.success != null ) {
      exit.success(exit, entity);
    }
  }

  static callFailIfDefined(exit: Exit, entity: ent.Entity) {
    if( exit.fail != null ) {
      exit.fail(exit, entity);
    }
  }
}


export class ActorTraversabilitySceneGraph implements Graph {
  game: GameData;

  constructor(game: GameData) {
    this.game = game;
  }

  neighbors(actor: Actor, node: Scene): Iterable<LinkNeighborPair> {
    const availableNeighbors = new Array<SceneLinkNeighborPair>();

    if( node.exits ) {
      for(let exit of node.exits) {
        if( exit.available != null && result.isRejected(evaluate(exit.available, exit, actor)) ) {
          continue;
        }

        if( exit.scene != null ) {
          const neighborScene = this.game.instances.scenes.get(evaluate(exit.scene, exit, actor));
          if( neighborScene == null || !result.isAccepted(node.canLeave(actor.id)) || !result.isAccepted(neighborScene.canEnter(actor.id)) ) {
            continue;
          }

          availableNeighbors.push({ link: exit, neighbor: neighborScene });
        }
      }
    }

    return availableNeighbors;
  }

   cost(actor: Actor, start: Scene, target: Scene): number {
    const distance = spatial.getDirectDistance(start, target);
    return (distance != null && distance > 0) ? distance : Number.MAX_VALUE;
  }

  validatePath(actor: Actor, path: Array<ScenePathElement>): boolean {
    return path.length !== 0;
  }
}

export class AudibilitySceneGraph implements Graph {
  game: GameData;

  constructor(game: GameData) {
    this.game = game;
  }

  neighbors(actor: Actor, node: Scene): Iterable<LinkNeighborPair> {
    const availableNeighbors = new Array<SceneLinkNeighborPair>();

    if( node.exits ) {
      for(let exit of node.exits) {
        if( exit.soundAttenuation != null && evaluate(exit.soundAttenuation, exit, actor) === 1 ) {
          continue;
        }

        if( exit.scene != null ) {
          const neighborScene = this.game.instances.scenes.get(evaluate(exit.scene, exit, actor));
          if( neighborScene == null ) {
            continue;
          }

          availableNeighbors.push({ link: exit, neighbor: neighborScene });
        }
      }
    }

    return availableNeighbors;
  }

  cost(actor: Actor, start: Scene, target: Scene): number {
    const exit = start.exits.find(e => start.id === evaluate(e.scene, e, actor));
    if( exit == null ) {
      return Number.MAX_VALUE;
    }

    return evaluate(exit.soundAttenuation, exit, actor) ?? 0;
  }

  validatePath(actor: Actor, path: Array<ScenePathElement>): boolean {
    return path.length !== 0;
  }
}

export class VisibilitySceneGraph implements Graph {
  game: GameData;

  constructor(game: GameData) {
    this.game = game;
  }

  neighbors(actor: Actor, node: Scene): Iterable<LinkNeighborPair> {
    const availableNeighbors = new Array<SceneLinkNeighborPair>();

    if( node.exits ) {
      for(let exit of node.exits) {
        if( exit.opacity != null && evaluate(exit.opacity, exit, actor) === 1 ) {
          continue;
        }

        if( exit.scene != null ) {
          const neighborScene = this.game.instances.scenes.get(evaluate(exit.scene, exit, actor));
          if( neighborScene == null ) {
            continue;
          }

          availableNeighbors.push({ link: exit, neighbor: neighborScene });
        }
      }
    }

    return availableNeighbors;
  }

  cost(actor: Actor, start: Scene, target: Scene): number {
    const startLocation = spatial.getSpatialLocation(start);
    const targetLocation = spatial.getSpatialLocation(target);
    // centers of scenes must be aligned at one coord to have direct visibility
    if( startLocation == null
      || targetLocation == null
      || (startLocation.x !== targetLocation.x && startLocation.y !== targetLocation.y)
    ) {
      return Number.MAX_VALUE;
    }

    const exit = start.exits.find(e => start.id === evaluate(e.scene, e, actor));
    if( exit == null ) {
      return Number.MAX_VALUE;
    }

    return evaluate(exit.opacity, exit, actor) ?? 0;
  }

  validatePath(actor: Actor, path: Array<ScenePathElement>): boolean {
    if( path.length === 0 ) {
      return false;
    }

    const startLocation = spatial.getSpatialLocation(path[0].node);
    if( startLocation == null ) {
      return false;
    }

    // for each node on path check whether x and y coord differs from start location
    const locationDifferentFromStart: Array<{xDiff: boolean, yDiff: boolean}> = path.map(element => {
      const nodeLocation = spatial.getSpatialLocation(element.node);
      if( nodeLocation == null ) {
        return { xDiff: true, yDiff: true };
      }
      return { xDiff: nodeLocation.x !== startLocation.x, yDiff: nodeLocation.y !== startLocation.y };
    });

    // in case there is difference in both x and y, this is not direct visibility and the path is invalid
    return !(locationDifferentFromStart.find(pair => pair.xDiff) != null && locationDifferentFromStart.find(pair => pair.yDiff) != null);
  }
}

