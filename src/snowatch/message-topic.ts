import { Newable } from "snowatch/base-types";

class _private {
  public static readonly registredTopics = new Map<string, Function>();
}


export function registerTopics(topics: Array<Topic>) {
  _private.registredTopics.clear();
  topics.forEach(t => _private.registredTopics.set((<any>t.constructor).ID ?? t.constructor.name, t.constructor));
}

export class Topic {
  public static fromId(topicId: string): Topic | undefined {
    return _private.registredTopics.get(topicId);
  }

  public static id<T extends Topic>(topic: Newable<T>): string {
    return (<any>topic).ID ?? (<Function>topic).name;
  }
}