import {isString} from 'snowatch/utils';

export const ok = 'ok';
export const accepted = 'accepted';
export const rejected = 'rejected';
export const timeout = 'timeout';
export const fail = 'fail';

export interface ResultObject {
  result: string;
  data?: any;
  message?: string;
}

export function make(result: string, data?: any, message?: string): ResultObject {
  if( isString(data) && message == null ) {
    return { result, data, message: data};
  } else {
    return { result, data, message};
  }
}

export function isResult(result: ResultObject, value: string): boolean {
  return result.result === value;
}

export function isAccepted(result: ResultObject): boolean {
  return isResult(result, accepted);
}

export function isRejected(result: ResultObject): boolean {
  return isResult(result, rejected);
}

