import { GameData } from 'snowatch/game-data';
import { Actor } from 'snowatch/actor';
import { Container } from 'snowatch/container';
import { Inventory } from 'snowatch/inventory';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { Surface } from 'snowatch/surface';
import { DefaultResDict } from 'snowatch/resources/default';
import * as ent from 'snowatch/entity';
import * as attrs from 'snowatch/attributes/all';
import * as behaviors from 'snowatch/behaviors/all';
import * as lang from 'snowatch/language-structures';


export const HANDS_ID = 'Hands';

export function createHands(owningActor: Actor, startSceneId: string, actorId: string) {
  const hands = ent.registerEntities(owningActor.game,
    ent.Entity.OBJECT_GROUP,
    {
      id: owningActor.id,
      instanceId: HANDS_ID,
      startSceneId: startSceneId,
      properties: {
        compAttributes: [
          { id: attrs.AvailableSpatialCapacity, compState: attrs.SpatialCapacityComputedAttribute.default },
          { id: attrs.IsVisible, compState: {
              value: (entity: ent.StatelessEntity, attributeId: string, actor?: ent.Entity) => {
                return ent.getIsVisible(owningActor);
              }
            }
          },
        ],
        constAttributes: [
          { id: attrs.Title, constState: { value: lang.capitalFirst(
            lang.Noun.get(owningActor.game,
              ResourceDictionary.get(owningActor.game, DefaultResDict.HANDS_NOUN)).noun(lang.Case.nominative))
            }
          },
          { id: attrs.NounResourceId, constState: { value: DefaultResDict.HANDS_NOUN } },
          { id: attrs.OwnerId, constState: { value: actorId} },
          { id: attrs.HolderId, constState: { value: actorId} },
          { id: attrs.HasSpatialRelations, constState: { value: true }, },
          { id: attrs.Surfaces, constState: { value: [Surface.outerTop, Surface.inside] }, },
          { id: attrs.Elasticity, constState: { value: { x: 0.5, y: 0.5, z: 0.5 } }, },
          { id: attrs.SpatialSize, constState: { value: {x: 1, y: 1, z: 1}} },
          { id: attrs.CanBeIndirect },
          { id: attrs.CanBeInstrument },
          { id: attrs.Weight, constState: { value: <attrs.NumericValueAttribute>{ value: 5 } } },
          { id: attrs.Agility, constState: { value: <attrs.NumericValueAttribute>{ value: 5 } } },
          { id: attrs.Damage, constState: { value: <attrs.NumericValueAttribute>{ value: 5 } } },
      ],
        attributes: [
          { id: attrs.CurrentSceneId, initialState: {value: startSceneId }},
        ],
      },
  },
  (game: GameData, o: ent.Entity) => {
    const hands = ent.Entity.registrator(game, o);
    attrs.Attribute.initializeAttributes(hands);
    attrs.Attribute.addFromProperties(hands, hands.properties);
    attrs.Attribute.addConstFromProperties(hands, hands.properties);
    attrs.Attribute.addCompFromProperties(hands, hands.properties);
    return hands;
  });

  Container.initializeContainer(hands);
  behaviors.Behavior.initializeBehaviors(hands);
  behaviors.Behavior.add(hands, behaviors.SupportsContainer.ID, { initialState: behaviors.createEmptyBehaviorState() });
  behaviors.Behavior.add(hands, behaviors.SupportsInventory.ID, { initialState: behaviors.createEmptyBehaviorState() });
  Inventory.addItem(owningActor, hands.id);
}