import { StatelessEntity } from "snowatch/entity";

export enum AttackType {
  fast = 'fast',
  strong = 'strong',
}

export interface FightCalculator {
  pickInstrument(actor: StatelessEntity): StatelessEntity;

  damage(actor: StatelessEntity, instrument: StatelessEntity, attackType: AttackType): number;

  attackWindUpTime(actor: StatelessEntity, instrument: StatelessEntity, attackType: AttackType): number;
  attackCoolDownTime(actor: StatelessEntity, instrument: StatelessEntity, attackType: AttackType): number;
  attackStaminaConsumption(actor: StatelessEntity, instrument: StatelessEntity, attackType: AttackType): number;

  stunTime(target: StatelessEntity, damage: number): number;

  blockWindUpTime(actor: StatelessEntity, instrument: StatelessEntity): number;
  blockStaminaConsumption(actor: StatelessEntity, instrument: StatelessEntity): number;

  dodgeCoolDownTime(actor: StatelessEntity): number;
  dodgeStaminaConsumption(actor: StatelessEntity): number;
}
