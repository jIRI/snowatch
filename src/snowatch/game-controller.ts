import { inject, All } from 'aurelia-framework';
import { HttpClient } from 'aurelia-fetch-client';
import { AutoRefreshCollectionCache, evalProperty } from 'snowatch/utils';
import { Topic, registerTopics, broadcast, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { GameData, GameLogItem } from 'snowatch/game-data';
import { Actor } from 'snowatch/actor';
import { InputLogger, InputLogRecording, InputLogItem } from 'snowatch/input-logger';
import { TimeSource } from 'snowatch/time-source';
import { FightDialog } from 'snowatch/views/dialogs/fight-dialog';
import * as vm from 'snowatch/viewmodels';
import * as attrs from 'snowatch/attributes-ids';
import * as skill from 'snowatch/skill';
import * as ent from 'snowatch/entity';
import * as result from 'snowatch/result';
import * as storage from 'snowatch/state-storage';
import * as scene from 'snowatch/scene';

import { LogManager } from 'aurelia-framework';
const logger = LogManager.getLogger('snowatch-game-controller');

@inject(
  GameData,
  All.of(Topic),
  vm.ActorViewModel,
  FightDialog
)
export class GameController {
  public game: GameData;
  public timesource: { isRunning: boolean } = { isRunning: true };
  private _initialized = false;
  private _currentActorVM: vm.ActorViewModel;
  private _storage: Storage;
  private _cache: Map<string, any>;
  private _exitsCache: AutoRefreshCollectionCache | null = null;
  private _inputLogger: InputLogger;
  private _inputLoggerSelectedRecording: InputLogRecording;
  private _inputLoggerSelectedItem: InputLogItem;
  private _buildstamp: Date | null = null;
  private _uiRefreshLastMS = 0;
  private _uiRefreshDebounceTimer = -1;
  private _fightDialog: FightDialog;

  constructor(game: GameData, topics: Array<Topic>, actorViewModel: vm.ActorViewModel, fightDialog: FightDialog) {
    this._initialized = false;

    // do the controller setup
    this.game = game;
    registerTopics(topics);
    // first reset state and initialize components
    this.doResetState();
    this.doIntializeComponents();
    // now we can continue...
    this._currentActorVM = actorViewModel;
    this._storage = localStorage;
    this._cache = new Map<string, any>();
    this._inputLogger = new InputLogger(this.game);
    this.initBusListeners();
    this.readBuildStamp();
    this._fightDialog = fightDialog;
    this.timesource = { isRunning: true };
  }

  get initialized() {
    return this._initialized;
  }

  get title() {
    if (!this._initialized || this.actor == null) {
      return '';
    }

    const currentScene = scene.getCurrentScene(this.actor);
    return (currentScene != null && attrs.Attribute.has(currentScene, attrs.Title)) ? attrs.Attribute.value<string>(currentScene, attrs.Title, this.actor) : '';
  }

  get description() {
    if (!this._initialized || this.actor == null) {
      return '';
    }

    const currentScene = scene.getCurrentScene(this.actor);
    return (currentScene != null && attrs.Attribute.has(currentScene, attrs.Description)) ? attrs.Attribute.value<string>(currentScene, attrs.Description, this.actor) : '';
  }

  get exits() {
    if (!this._initialized || this.actor == null) {
      return [];
    }

    if (scene.getCurrentSceneId(this.actor) !== null) {
      if (this._exitsCache == null) {
        this._exitsCache = new AutoRefreshCollectionCache(
          this.game.bus.notifications,
          () => {
            const actor = this.actor;
            let result: Array<vm.ExitViewModel> = [];
            if (actor != null) {
              const currentScene = scene.getCurrentScene(actor);
              if (currentScene != null) {
                result = currentScene.exits
                  .filter(e => evalProperty(e, scene.Scene.EXIT_VISIBLE_PROP, e, actor.id))
                  .map(e => new vm.ExitViewModel().initialize(this.game, this._currentActorVM.actor, e));
              }
            }
            return result;
          }
        );
        this._exitsCache.refresh();
      }
      return this._exitsCache.array;
    } else {
      return [];
    }
  }

  get self(): Array<vm.ViewModelRelationsSkills> {
    if (!this._initialized) {
      return [];
    }

    const cacheLineName = `self`;
    if (!this._cache.has(cacheLineName)) {
      this._cache.set(cacheLineName, new AutoRefreshCollectionCache(
        this.game.bus.notifications,
        () => {
          let result: Array<vm.ViewModelRelationsSkills> = [];
          const actor = this.actor;
          if (actor != null) {
            // make single entry array with actor to allow simple mapping later...
            result = [actor].map(e => vm.getViewModelRelationsSkills(e, actor, this._cache, 'self'));
          }
          return result;
        }));
      this._cache.get(cacheLineName).refresh();
    }
    return this._cache.get(cacheLineName).array[0];
  }

  get sceneEntities(): Array<vm.ViewModelRelationsSkills> {
    if (!this._initialized) {
      return [];
    }

    const cacheLineName = `scene-entities`;
    if (!this._cache.has(cacheLineName)) {
      this._cache.set(cacheLineName, new AutoRefreshCollectionCache(
        this.game.bus.notifications,
        () => {
          let result: Array<vm.ViewModelRelationsSkills> = [];
          const actor = this.actor;
          if (actor != null) {
            const currentScene = scene.getCurrentScene(actor);
            if (currentScene != null) {
              const actorId = actor.id;
              const entities = Array.from<ent.Entity>(currentScene.items.filter(i => ent.getIsVisible(i, actor) && ent.getHolderId(i) == null))
                .concat(currentScene.actors.filter(a => ent.getIsVisible(a, actor) && ent.getHolderId(a) == null && a.id !== actorId))
                .concat(currentScene.entities.filter(e => ent.getIsVisible(e, actor) && ent.getHolderId(e) == null && e.id !== actorId))
                .concat(currentScene.substances.filter(e => ent.getIsVisible(e, actor) && ent.getHolderId(e) == null && e.id !== actorId));

              result = entities
                .filter(e => !vm.canEntityBeCollapsed(e))
                .map(e => vm.getViewModelRelationsSkills(e, actor, this._cache, 'scene.' + currentScene.id));
            }
          }
          return result;
        }));
      this._cache.get(cacheLineName).refresh();
    }
    return this._cache.get(cacheLineName).array;
  }

  get currentActorVM(): vm.ActorViewModel | null {
    if (!this._initialized) {
      return null;
    }

    return this._currentActorVM;
  }

  get actor(): Actor | null {
    if (!this._initialized) {
      return null;
    }

    return this._currentActorVM.actor;
  }

  get inventory() {
    if (!this._initialized) {
      return null;
    }

    return this._currentActorVM.inventory;
  }

  get gameLogItems(): Array<GameLogItem> {
    if (!this._initialized) {
      return [];
    }

    return this.game.state.gameLog;
  }

  get realTimeContent(): any {
    return {
      visible: false,
      contentTemplate: 'snowatch/views/draw-surface',
    };
  }

  tryExit(exit: scene.Exit) {
    return this._currentActorVM.tryExit(exit);
  }

  useSkill(skill: vm.SkillViewModel, object: ent.Entity) {
    skill.useSkill(object);
  }

  setSelectedInstrument(skill: vm.SkillViewModel, instrument: vm.PrepositionedEntityViewModel) {
    skill.setSelectedInstrument(instrument);
  }

  setSelectedIndirect(skill: vm.SkillViewModel, indirect: vm.PrepositionedEntityViewModel) {
    skill.setSelectedIndirect(indirect);
  }

  setSelectedModifier(skill: vm.SkillViewModel, modifier: skill.SkillModifier) {
    skill.setSelectedModifier(modifier);
  }

  activate() {
    this._initialized = false;
    this.doActivate();
  }

  scrollTheLogDown() {
    // put in the queue so log can be redrawn before it gets scrolled
    setTimeout(() => {
      const objDiv = document.getElementById("game-log-view");
      if (objDiv != null) {
        objDiv.scrollTop = objDiv.scrollHeight;
      }
    }, 0);
  }

  clearTheLog() {
    // put in the queue so log can be redrawn before it gets scrolled
    setTimeout(() => {
      let objDiv = document.getElementById("game-log-view");
      if (objDiv != null) {
        objDiv.scrollTop = objDiv.scrollHeight;
      }
    }, 0);
  }

  private initBusListeners() {
    // when something is added to log, scroll down to see it
    this.game.bus.notifications.subscribe({
      disposeOnReset: false,
      filter: (m) => {
        return m.topicId === Topic.id(Topics.GAME_LOG_UPDATED);
      },
      handler: (m) => {
        this.scrollTheLogDown();
      },
    });
    // add reset handling
    this.game.bus.system.subscribe({
      allowAsync: false,
      disposeOnReset: false,
      filter: (m) => {
        return m.topicId === Topic.id(Topics.RESET_GAME);
      },
      handler: (m) => {
        this.resetState();
      },
    });
    // add load handling
    this.game.bus.system.subscribe({
      allowAsync: false,
      disposeOnReset: false,
      filter: (m) => {
        return m.topicId === Topic.id(Topics.LOAD_STATE);
      },
      handler: (m) => {
        const messagePayload = payload<Topics.LOAD_STATE>(m);
        this.loadState(messagePayload.slotName);
      },
    });
    // add scene transitions handling
    this.game.bus.commands.subscribe({
      disposeOnReset: false,
      filter: (m) => {
        return m.topicId === Topic.id(Topics.TRY_EXIT);
      },
      handler: (m) => {
        scene.Scene.handleTryExit(this.game, m.payload, m.id);
      }
    });
    // add skill handling
    this.game.bus.commands.subscribe({
      disposeOnReset: false,
      filter: (m) => {
        return m.topicId === Topic.id(Topics.TRY_USE_SKILL);
      },
      handler: (m) => {
        skill.Skill.handleTryUseSkill(this.game, m.id, m.payload);
      }
    });

    // add ui refresh handling
    const maxRefreshDelay = 150;
    this.game.bus.notifications.subscribe({
      disposeOnReset: false,
      filter: (m) => {
        return m.topicId !== Topic.id(Topics.UI_REFRESH);
      },
      handler: (m) => {
        // if there is pending ui refresh, cancel it
        const now = Date.now();
        if (this._uiRefreshDebounceTimer !== -1 && (now - this._uiRefreshLastMS) < maxRefreshDelay) {
          clearTimeout(this._uiRefreshDebounceTimer);
        } else {
          this._uiRefreshLastMS = now;
        }

        this._uiRefreshDebounceTimer = window.setTimeout(() => {
            this._uiRefreshDebounceTimer = -1;
            this.game.bus.notifications.publish(broadcast(Topics.UI_REFRESH));
          },
          maxRefreshDelay
        );
      }
    });
  }

  private doResetState() {
    logger.debug('invoking state reset');
    this.clearTheLog();
    logger.debug('invoking reset');
    this.game.bus.system.publish(broadcast(Topics.RESET_STATE), { allowAsync: false });
  }

  private doIntializeComponents() {
    logger.debug('invoking components initializetion');
    this.game.bus.system.publish(broadcast(Topics.INITIALIZE_COMPONENTS), { allowAsync: false });
  }

  private doActivate() {
    logger.debug('activating');

    this.attachWindowHandlers();
    this.reloadStoredStateIfExists();

    logger.debug('setting start actor');
    const startActor = Array.from<Actor>(this.game.instances.actors.values()).find((a: Actor)  => attrs.Attribute.has(a, attrs.IsStartActor));
    if( startActor != null ) {
      this.game.startActor = startActor;
      this.game.currentActor = this.game.startActor;
      attrs.Attribute.setValue<boolean>(this.game.currentActor, attrs.IsCurrentActor, true);
    } else {
      logger.error('No start actor set!');
    }

    this._currentActorVM.initialize(this.game, this._cache, this.game.currentActor);
    this._initialized = true;
    this.game.bus.system.publish(broadcast(Topics.START, { game: this.game }), { allowAsync: false });
  }

  private attachWindowHandlers() {
    const self = this;
    window.onfocus = function () {
      logger.debug('Unpausing time source');
      TimeSource.unpause();
      self.timesource.isRunning = true;
    };

    window.onblur = function () {
      logger.debug('Pausing time source');
      TimeSource.pause();
      self.timesource.isRunning = false;
    };
  }

  resetState() {
    logger.debug('resetting state');
    this._initialized = false;
    this._exitsCache = null;
    this._cache.clear();
    this.doResetState();
    this.doIntializeComponents();
    this.doActivate();
  }

  saveState(slotName: string) {
    storage.addSlot(this._storage, slotName);
    logger.debug(`saving state to slot '${slotName}'`);
    storage.store(this._storage, slotName, this.game.state);
  }

  loadState(slotName: string) {
    logger.debug(`loading state from slot '${slotName}'`);

    const res = storage.restore(this._storage, slotName);
    if (result.isResult(res, result.ok)) {
      this._initialized = false;
      this.clearCaches();
      this.doResetState();
      this.doIntializeComponents();
      this.game.state = res.data;
      this.game.ensureAllStateEntitiesRegistered();
      this.scrollTheLogDown();
      this.doActivate();
    }
  }

  readBuildStamp() {
    let client = new HttpClient();
    client.fetch('buildstamp.json')
      .then((response) => response.json())
      .then((data: any) => {
        const date = new Date(data.timestamp);
        if (this._buildstamp != null && this._buildstamp < date) {
          this.saveState('@@autoreload');
          location.reload();
        } else {
          this._buildstamp = date;
        }
      })
      .catch(() => logger.debug("Unable to read build stamp, skipping reload."));
  }

  private clearCaches() {
    this._exitsCache = null;
    this._cache.clear();
  }

  //
  // Debug & develop stuff
  //

  dumpAll() {
    console.log(this.game);
  }

  dumpState() {
    console.log(this.game.state);
  }

  autosaveForReload() {
    const autoreloadSlotName = '@@autoreload';
    if( !storage.hasSlot(this._storage, autoreloadSlotName) ) {
      this.saveState(autoreloadSlotName);
    }
  }

  reload() {
    this.autosaveForReload();
    location.reload();
  }

  get inputLoggerItems() {
    return this._inputLogger.items;
  }

  get inputLoggerRecordings() {
    return this._inputLogger.recordings;
  }

  get selectedInputLoggerRecording(): InputLogRecording {
    return this._inputLoggerSelectedRecording;
  }

  get selectedInputLoggerItem(): InputLogItem {
    return this._inputLoggerSelectedItem;
  }

  setSelectedInputLoggerRecording(recording: InputLogRecording) {
    this._inputLoggerSelectedRecording = recording;
    this._inputLogger.activateRecording(recording.name);
    this.setSelectedInputLoggerItem(this.inputLoggerItems[this.inputLoggerItems.length - 1]);
  }

  addRecording(recordingName: string) {
    if( this._inputLoggerSelectedItem != null ) {
      this._inputLogger.addRecording(recordingName, this._inputLogger.items.indexOf(this._inputLoggerSelectedItem));
    } else {
      this._inputLogger.addRecording(recordingName);
    }
  }

  deleteSelectedRecording() {
    if( this._inputLoggerSelectedRecording != null ) {
      this._inputLogger.deleteRecording();
    }
  }

  setSelectedInputLoggerItem(item: InputLogItem) {
    this._inputLoggerSelectedItem = item;
  }

  replayInputLog(clockScale: number) {
    if( this._inputLoggerSelectedItem != null ) {
        // add to the end of event queue so we can release the thread
      setTimeout(() => this._inputLogger.replayTo(this._inputLogger.items.indexOf(this._inputLoggerSelectedItem), clockScale), 0);
    } else {
      // add to the end of event queue so we can release the thread
      setTimeout(() => this._inputLogger.replayTo(this._inputLogger.items.length - 1, clockScale), 0);
    }
  }

  restoreInputLogState() {
    if( this._inputLoggerSelectedItem != null ) {
      this._inputLogger.restoreAt(this._inputLogger.items.indexOf(this._inputLoggerSelectedItem));
    } else {
      this._inputLogger.restoreAt(this._inputLogger.items.length - 1);
    }
  }

  trimInputLog() {
    if( this._inputLoggerSelectedItem != null ) {
      this._inputLogger.trimFrom(this._inputLogger.items.indexOf(this._inputLoggerSelectedItem));
    }
  }

  clearInputLog() {
    this._inputLogger.clear();
  }

  startRecordingInput() {
    if (this._inputLoggerSelectedItem != null) {
      if( storage.hasSlot(this._storage, this._inputLoggerSelectedItem.slotName) ) {
        this.restoreInputLogState();
      }
      this._inputLogger.trimFrom(this._inputLogger.items.indexOf(this._inputLoggerSelectedItem) + 1);
    } else {
      this._inputLogger.clear();
    }
    this._inputLogger.startRecording();
  }

  stopRecordingInput() {
    this._inputLogger.stopRecording();
  }

  reloadStoredStateIfExists() {
    const res = storage.restore(this._storage, '@@autoreload');
    if (result.isResult(res, result.ok)) {
      storage.removeSlot(this._storage, '@@autoreload');
      this.clearCaches();
      this.doResetState();
      this.doIntializeComponents();
      this.game.state = res.data;
      this.doActivate();
    }
  }

  test() {
    const newActor = new Actor({id: 'testActor', instanceId: 'one'});
    attrs.Attribute.initializeAttributes(newActor);
    ent.registerEntities(this.game, newActor.entityGroup, newActor, Actor.registrator);
    ent.cloneGroup(attrs.Attribute.OBJECT_GROUP, this._currentActorVM.actor, newActor);
  }
}
