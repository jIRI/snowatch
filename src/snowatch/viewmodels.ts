import {Topic, Message, broadcast} from 'snowatch/message';
import {Topics} from 'snowatch/message-topics/all';
import {AutoRefreshCollectionCache, evaluate} from 'snowatch/utils';
import {BusRequest} from 'snowatch/utils';
import {Inventory} from 'snowatch/inventory';
import {GameData} from 'snowatch/game-data';
import {Actor} from 'snowatch/actor';
import {Say} from 'snowatch/skills/say';
import {DialogueLine} from 'snowatch/dialogue-line';
import {ResourceDictionary} from 'snowatch/resource-dictionary';
import {DefaultResDict} from 'snowatch/resources/default';
import {Skill, SkillModifier, PrepositionedEntity} from 'snowatch/skill';
import { SpatialRelations } from 'snowatch/spatial-relations';
import { SpatialPrepositions } from 'snowatch/prepositions';
import { HANDS_ID } from 'snowatch/entities/hands';
import * as attrs from 'snowatch/attributes-ids';
import * as scene from 'snowatch/scene';
import * as ent from 'snowatch/entity';
import * as result from 'snowatch/result';
import * as lang from 'snowatch/language-structures';

export const CollapsibleSpatialRelationsTypeIds = [
  SpatialPrepositions.isOn.id,
  SpatialPrepositions.isIn.id,
  SpatialPrepositions.isUnder.id,
];

export function canEntityBeCollapsed(entity: ent.Entity): boolean {
  const relations = SpatialRelations.relationsForDirect(entity);
  return relations.length > 0
    && relations.every(r => {
        const indirect = entity.game.instances.getSpatialFromId(r.indirectId);
        return indirect != null && indirect.instanceId !== scene.Scene.GROUND && CollapsibleSpatialRelationsTypeIds.includes(r.typeId);
      });
}

export function getViewModelRelationsSkills(entity: ent.Entity, actor: Actor, cache: Map<string, any>, cachePrefix: string): ViewModelRelationsSkills {
  const actorId = actor.id;
  const relations = SpatialRelations.relationsForIndirect(entity)
    .filter(r => CollapsibleSpatialRelationsTypeIds.includes(r.typeId))
    .map(r => entity.game.instances.getSpatialFromId(r.directId))
    .filter(e => e != null && ent.getIsVisible(e, actor) && e.id !== entity.id && e.id !== actorId) as ent.Entity[];

  return {
    vm: new EntityViewModel(entity, actor),
    relations: relations.map(e => getViewModelRelationsSkills(e, actor, cache, cachePrefix)),
    skills: SkillViewModel.getAvailableSkills(cachePrefix, entity.game, actor, entity, cache),
  };
}

export interface ViewModelRelationsSkills {
  vm: EntityViewModel;
  relations: Array<ViewModelRelationsSkills>;
  skills: Array<SkillViewModel>;
}

export interface PrepositionedEntityViewModel {
  prepositionId: string | null;
  vm: EntityViewModel;
  prepositionedEntityNoun: string;
}

export class EntityViewModel {
  public entity: ent.Entity | ent.StatelessEntity;
  public actor: Actor;
  public tag: string;
  constructor(entity: ent.Entity | ent.StatelessEntity, actor: Actor) {
    this.entity = entity;
    this.actor = actor;
    this.tag = this.entity.id.replace(/\./g, '-');
    if( (<ent.Entity>entity) != null && (<ent.Entity>entity).state != null && (<ent.Entity>entity).state.attributes != null ) {
      this.addAttributes((<ent.Entity>entity).state.attributes.keys());
    }
    if( entity != null && entity.computed != null && entity.computed.attributes != null ) {
      this.addAttributes(entity.computed.attributes.keys());
    }
    if( entity != null && entity.consts != null && entity.consts.attributes != null ) {
      this.addAttributes(entity.consts.attributes.keys());
    }
  }


  logEntityDescription() {
    this.entity.game.addToGameLog({
      source: this.actor != null ? this.actor.id : '',
      action: 'item-description',
      category: 'game-log-item-desc',
      description: (<any>this).Description
    });
  }


  private addAttributes(attribs: Array<string>) {
    for(const key of attribs) {
      if( key != null && !this.hasOwnProperty(key) ) {
        (<any>this)[key] = attrs.Attribute.value<any>(this.entity, key, this.actor);
      }
    }
  }
}

export class ExitViewModel {
  public game: GameData;
  private _actor: Actor;
  private _exit: scene.Exit;

  constructor() {
  }

  initialize(game: GameData, actor: Actor, exit: scene.Exit): ExitViewModel {
    this.game = game;
    this._actor = actor;
    this._exit = exit;
    return this;
  }

  get actor() {
    return this._actor;
  }

  get exit() {
    return this._exit;
  }

  get title() {
    return evaluate(this._exit.title, this._exit, this._actor);
  }
}

export class ActorViewModel {
  public game: GameData;
  private _actor: Actor;
  private _cache: Map<string, any>;
  private _skillVMMap: Map<string, SkillViewModel>;

  constructor() {
  }

  initialize(game: GameData, cache: Map<string, any>, actor: Actor) {
    this.game = game;
    this._actor = actor;
    this._cache = cache;
    this._skillVMMap = new Map<string, any>();
  }

  get actor() {
    return this._actor;
  }

  get inventory(): Array<ViewModelRelationsSkills> {
    if( this._actor == null
      || (attrs.Attribute.applies(this._actor, attrs.CanSeeInventory, this._actor)
        && !attrs.Attribute.value(this._actor, attrs.CanSeeInventory, this._actor))
      ) {
      return [];
    }

    const cacheLineName = `${this._actor.id}.inventory`;
    if( !this._cache.has(cacheLineName) ) {
      this._cache.set(cacheLineName, new AutoRefreshCollectionCache(
        this.game.bus.notifications,
        () => {
        const itemIds = Inventory.getItemsIds(this._actor);
        let results: Array<ViewModelRelationsSkills> = [];
        for(let itemId of itemIds) {
          const item = this.game.instances.getEntityFromId(itemId);
          if(item != null && !canEntityBeCollapsed(item)) {
            results.push(getViewModelRelationsSkills(item, this._actor, this._cache, 'inventory.'));
          }
        }
        return results;
      }));
      this._cache.get(cacheLineName).refresh();
    }
    return this._cache.get(cacheLineName).array;
  }

  tryExit(exit: scene.Exit) {
    this.game.bus.input.publish(broadcast(
      Topics.TRY_EXIT,
      { entityId: this._actor.id, exitId: exit.id }),
      { addTimestamp: true }
    );
  }
}

export class SkillViewModel {
  static getAvailableSkills(contextName: string, game: GameData, actor: Actor, object: ent.Entity, cache: Map<string, any>): Array<SkillViewModel> {
    const availableSkills: Array<SkillViewModel> = [];
    const canUseSkillApplicable = attrs.Attribute.applies(actor, attrs.CanUseSkills, actor);
    if( canUseSkillApplicable === false
        || (canUseSkillApplicable === true && attrs.Attribute.value<boolean>(actor, attrs.CanUseSkills, actor) === true)
      ) {
        for(let skillId of Skill.getIds(actor))  {
          const skill: Skill | undefined = game.instances.skills.get(skillId);
          if( skill != null ) {
            const skillCacheName = `${actor.id}.${contextName}.${skillId}.${object.id}`;
            if( !cache.has(skillCacheName) ) {
              const skillVM = new SkillViewModel().initialize(game, actor, skill);
              cache.set(skillCacheName, skillVM);
            }

            const skillVM = cache.get(skillCacheName);
            const previewResult = skillVM.previewSkill(object);
            if( result.isAccepted(previewResult)) {
              availableSkills.push(skillVM);
            }
          }
        }
    }

    return availableSkills;
  }

  private _skill: Skill;
  private _title: string;
  private _capitalTitle: string;
  private _actor: Actor;
  private _previewData: any;
  private _canUse: boolean;
  private _selectedModifier: SkillModifier | null;
  private _selectedInstrument: PrepositionedEntityViewModel | null;
  private _selectedIndirect: PrepositionedEntityViewModel | null;
  game: GameData;

  constructor() {
  }

  initialize(game: GameData, actor: Actor, skill: Skill) {
    this.game = game;
    this._actor = actor;
    this._skill = skill;
    this._title = this._skill.verb.infinitive;
    this._capitalTitle = lang.capitalFirst(this._title);
    return this;
  }

  get actor() {
    return this._actor;
  }

  get skill() {
    return this._skill;
  }

  get title() {
    return this._title;
  }

  get capitalTitle() {
    return this._capitalTitle;
  }

  get canUse() {
    return this._canUse;
  }

  get isRealTime() {
    return this._skill.isRealTime;
  }

  get config() {
    return this._previewData;
  }

  get selectedModifier() {
    return this._selectedModifier;
  }

  get selectedModifierId() {
    return this._selectedModifier != null ? this._selectedModifier.id : '';
  }

  get selectedInstrument() {
    return this._selectedInstrument;
  }

  get selectedIndirect() {
    return this._selectedIndirect;
  }

  get selectedInstrumentPrepositionId() {
    return this._selectedInstrument != null && this._selectedInstrument.prepositionId != null ? this._selectedInstrument.prepositionId : '';
  }

  get selectedInstrumentId() {
    return this._selectedInstrument != null && this._selectedInstrument.vm != null ? this._selectedInstrument.vm.entity.id : '';
  }

  get selectedIndirectPrepositionId() {
    return this._selectedIndirect != null && this._selectedIndirect.prepositionId != null ? this._selectedIndirect.prepositionId : '';
  }

  get selectedIndirectObjectId() {
    return this._selectedIndirect != null && this._selectedIndirect.vm != null ? this._selectedIndirect.vm.entity.id : '';
  }

  setSelectedModifier(modifier: SkillModifier | null) {
    this._selectedModifier = modifier;
  }

  setSelectedInstrument(instrument: PrepositionedEntityViewModel | null) {
    this._selectedInstrument = instrument;
  }

  setSelectedIndirect(indirect: PrepositionedEntityViewModel | null) {
    this._selectedIndirect = indirect;
  }

  getInstruments(skillId: string, object: ent.Entity): Array<ent.Entity> {
    let entities: Array<ent.Entity> = [];
    const currentScene = scene.getCurrentScene(this._actor);
    if( currentScene != null ) {
      entities = Array.from<ent.Entity>(currentScene.items.filter(o => ent.isHeldByOrFree(o, this._actor) && ent.getCanBeInstrument(o, this._actor)));
      entities = entities.concat(currentScene.entities.filter(o => ent.isHeldByOrFree(o, this._actor) && ent.getCanBeInstrument(o, this._actor)));
    }
    return entities;
  }

  getIndirects(skillId: string, object: ent.Entity): Array<ent.Entity> | Array<DialogueLine> {
    // do a bit of  distinguishing here, because there potentially might be lots of dialogue lines which would kill performance
    switch(skillId) {
      case Say.ID:
        // TODO: add some filtering only for active/available dialogue lines to reduce amount of items in the array...
        // also it would be cool to have access to values, not just keys here...
        const lines: Array<DialogueLine> = [];
        const lineIds = DialogueLine.getIds(this._actor);
        for(let lineId of lineIds) {
         const line = this.game.instances.dialogueLines.get(lineId);
         if( line != null ) {
           lines.push(line);
         }
        }
        return lines;
      default:
        let entities: Array<ent.Entity> = [];
        const currentScene = scene.getCurrentScene(this._actor);
        if( currentScene != null ) {
          entities = Array.from<ent.Entity>(currentScene.items.filter(o => ent.isHeldByOrFree(o, this._actor) && ent.getCanBeIndirect(o, this._actor)));
          entities = entities.concat(currentScene.actors.filter(o => ent.getCanBeIndirect(o, this._actor)));
          entities = entities.concat(currentScene.entities.filter(o => ent.isHeldByOrFree(o, this._actor) && ent.getCanBeIndirect(o, this._actor)));
        }
        return entities;
    }
  }

  previewSkill(object: ent.Entity) {
    const skillId = this._skill.id;
    const instruments = this.getInstruments(skillId, object);
    const indirects = this.getIndirects(skillId, object);

    const previewResult = Skill.preview(
      this.game,
      skillId,
      ent.getState(this._actor, Skill.OBJECT_GROUP, skillId),
      {
        actor: this._actor,
        direct: object,
        instruments: instruments,
        indirects: indirects,
      },
      this._actor.properties.skills.get(skillId).filterPreview
    );

    const previewData = previewResult.data;
    if( previewData != null ) {
      if( previewData.requiredInstrumentsCount > 0 ) {
        previewData.instruments = Array.from<PrepositionedEntity, PrepositionedEntityViewModel>(previewData.instruments, i => {
          const preposition: any | null  = (i.preposition != null) ? ResourceDictionary.get(this.game, i.preposition.prepositionResource) : null;
          const prepositionId: string | null = preposition != null ? (preposition.id != null ? preposition.id : preposition) : null;
          const adjectiveId: string | null = ResourceDictionary.get(this.game, attrs.Attribute.value<string>(i.entity, attrs.AdjectiveResourceId, this.actor));
          const nounId: string | null = ResourceDictionary.get(this.game, attrs.Attribute.value<string>(i.entity, attrs.NounResourceId, this.actor));
          const prepNounCase = lang.getPrepositionNounCase(this.game, prepositionId, adjectiveId, nounId);
          return {
            prepositionId: i.preposition != null ? i.preposition.id : null,
            vm: new EntityViewModel(i.entity, this._actor),
            prepositionedEntityNoun: lang.compose(
              ResourceDictionary.get(this.game, DefaultResDict.PREPOSITIONED_INDIRECT).template,
              {
                preposition: prepNounCase.preposition != null ? prepNounCase.preposition.preposition() : '',
                noun: prepNounCase.nounCase != null ? `${prepNounCase.adjectiveCase ?? ''} ${prepNounCase.nounCase}` : ''
              }
            ),
          };
        });
      } else {
        previewData.instruments = [];
      }

      // convert indirects into viewmodels
      previewData.indirects = Array.from<PrepositionedEntity, PrepositionedEntityViewModel>(previewData.indirects, i => {
        const preposition: any | null  = (i.preposition != null) ? ResourceDictionary.get(this.game, i.preposition.prepositionResource) : null;
        const prepositionId: string | null = preposition != null ? (preposition.id != null ? preposition.id : preposition) : null;
        const adjectiveId: string | null = ResourceDictionary.get(this.game, attrs.Attribute.value<string>(i.entity, attrs.AdjectiveResourceId, this.actor));
        const nounId: string | null = ResourceDictionary.get(this.game, attrs.Attribute.value<string>(i.entity, attrs.NounResourceId, this.actor));
        const prepNounCase = lang.getPrepositionNounCase(this.game, prepositionId, adjectiveId, nounId);
        return {
          prepositionId: i.preposition != null ? i.preposition.id : null,
          vm: new EntityViewModel(i.entity, this._actor),
          prepositionedEntityNoun: lang.compose(
            ResourceDictionary.get(this.game, DefaultResDict.PREPOSITIONED_INDIRECT).template,
            {
              preposition: prepNounCase.preposition != null ? prepNounCase.preposition.preposition() : '',
              noun: prepNounCase.nounCase != null ? `${prepNounCase.adjectiveCase ?? ''} ${prepNounCase.nounCase}` : ''
            }
          ),
        };
      });

      // get titles for modifiers
      const directNounResourceId = attrs.Attribute.value<string>(object, attrs.NounResourceId, this.actor);
      const directNounId = ResourceDictionary.get(object.game, directNounResourceId);
      previewData.modifiers = Array.from<SkillModifier, SkillModifier & { title: string }>(previewData.modifiers, m => {
        const modifierResource = ResourceDictionary.get(this.game, m.modifierResource);
        let modifierTitle: string;
        switch( modifierResource.kind ) {
          case lang.Pronoun.ID:
            modifierTitle = lang.Pronoun.get(this.game, modifierResource.id).pronoun(directNounId, lang.Case.accusative);
            break;
          case lang.Adverb.ID:
          default:
            modifierTitle = lang.Adverb.get(this.game, modifierResource.id).adverb();
            break;
        }
        return {
          ...m,
          title: modifierTitle,
        };
      });

      // in case we have some preview game, reset selected values in case the sizes do not match
      if( this._previewData != null ) {
        if( previewData.modifiers.length !== this._previewData.modifiers.length ) {
          this.setSelectedModifier(null);
        }
        if( this._previewData != null && previewData.indirects.length !== this._previewData.indirects.length ) {
          this.setSelectedIndirect(null);
        }
      }

      // first check whether the selected objects are still in the selection
      const currentSelectedIndirect = this.selectedIndirect;
      if( currentSelectedIndirect != null && previewData.indirects.find((indirect: any) => currentSelectedIndirect.prepositionId === indirect.prepositionId && currentSelectedIndirect.vm.entity === indirect.vm.entity) == null ) {
        this.setSelectedIndirect(null);
      }
      const currentSelectedModifier = this.selectedModifier;
      if( currentSelectedModifier != null && previewData.modifiers.find((modifier: any) => currentSelectedModifier.id === modifier.id) == null ) {
        this.setSelectedModifier(null);
      }

      // if we have something to select from (and nothing was selected yet), select it for...
      // ...instruments
      if( previewData.instruments.length > 0 && this._selectedInstrument == null ) {
        let preselectedInstrument = (previewData.instruments as Array<PrepositionedEntityViewModel>).find(instrument => instrument.vm.entity.instanceId === HANDS_ID);
        this.setSelectedInstrument(preselectedInstrument ?? previewData.instruments[0]);
      } else if( previewData.instruments.length === 0) {
        this.setSelectedInstrument(null);
      }

      // ...modifiers
      if( previewData.modifiers.length > 0 && this._selectedModifier == null) {
        this.setSelectedModifier(previewData.modifiers[0]);
      } else if( previewData.modifiers.length === 0 ) {
        this.setSelectedModifier(null);
      }

      // ...indirects
      if( previewData.indirects.length > 0 && this._selectedIndirect == null ) {
        this.setSelectedIndirect(previewData.indirects[0]);
      } else if( previewData.indirects.length === 0) {
        this.setSelectedIndirect(null);
      }
    } else {
      // no game, nothing to select
      this.setSelectedModifier(null);
      this.setSelectedIndirect(null);
    }

    this._previewData = previewData;
    this._canUse = result.isAccepted(previewResult);

    return previewResult;
  }

  useSkill(object: ent.Entity) {
    const skillId = this._skill.id;
    const request = new BusRequest({
      requestBus: this.game.bus.input,
      responseBus: this.game.bus.notifications,
      resolveFilter: (m: Message) => m.topicId === Topic.id(Topics.SKILL_ACCEPTED),
      rejectFilter: (m: Message) => m.topicId === Topic.id(Topics.SKILL_REJECTED)
    });

    return request.send(broadcast(
      Topics.TRY_USE_SKILL,
      {
        skillId: skillId,
        actorId: this._actor.id,
        directId: object.id,
        modifierId: this.selectedModifierId,
        instrumentPrepositionId: this.selectedInstrumentPrepositionId,
        instrumentId: this.selectedInstrumentId,
        indirectsPrepositionId: this.selectedIndirectPrepositionId,
        indirectsIds: [this.selectedIndirectObjectId],
      },
      undefined,
      object.id
    ), { addTimestamp: true });
  }
}
