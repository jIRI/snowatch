import { AttackType, FightCalculator } from 'snowatch/fight-calculator';
import { StatelessEntity, Entity } from 'snowatch/entity';
import { Inventory } from 'snowatch/inventory';
import * as attrs from 'snowatch/attributes-ids';

export class DefaultFightCalculator implements FightCalculator  {
  public pickInstrument(actor: Entity): StatelessEntity {
    const instruments = Inventory.filter(actor, item => attrs.Attribute.has(item, attrs.CanBeInstrument));
    const maxDamageInstrument = instruments.reduce((a, b) => {
      const damageA = attrs.Attribute.applies(a, attrs.Damage) ? attrs.Attribute.value<attrs.NumericValueAttribute>(a, attrs.Damage) : 0;
      const damageB = attrs.Attribute.applies(b, attrs.Damage) ? attrs.Attribute.value<attrs.NumericValueAttribute>(b, attrs.Damage) : 0;
      return damageA > damageB ? a : b;
    });
    return maxDamageInstrument;
  }

  public attackWindUpTime(actor: Entity, instrument: StatelessEntity, attackType: AttackType): number {
    const actorStrength = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Strength);
    const actorAgility = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Agility);
    const actorDexterity = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Dexterity);
    const instrumentWeight = attrs.Attribute.value<attrs.NumericValueAttribute>(instrument, attrs.Weight);
    const instrumentAgility = attrs.Attribute.value<attrs.NumericValueAttribute>(instrument, attrs.Agility);

    const windUpTime = this.timeDifficultyMultiplier() * 1000 * (instrumentWeight.value / instrumentAgility.value) / (actorStrength.value + actorAgility.value + actorDexterity.value);
    return windUpTime * this.timeMultiplier(attackType);
  }

  public attackCoolDownTime(actor: Entity, instrument: StatelessEntity, attackType: AttackType): number {
    const actorStrength = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Strength);
    const actorAgility = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Agility);
    const instrumentWeight = attrs.Attribute.value<attrs.NumericValueAttribute>(instrument, attrs.Weight);
    const instrumentAgility = attrs.Attribute.value<attrs.NumericValueAttribute>(instrument, attrs.Agility);

    const coolDownTime = 2 * 1000 * (instrumentWeight.value / instrumentAgility.value) / (actorStrength.value + actorAgility.value);
    return coolDownTime * this.timeMultiplier(attackType);
  }

  public stunTime(target: StatelessEntity, damage: number): number {
    const actorStrength = attrs.Attribute.value<attrs.NumericValueAttribute>(target, attrs.Strength);
    const actorConstitution = attrs.Attribute.value<attrs.NumericValueAttribute>(target, attrs.Constitution);

    const stunTime = 1 + 5 * Math.log10(1 + (damage / 10) / (actorStrength.value * actorConstitution.value));
    return 1000 * stunTime;
  }

  public damage(actor: Entity, instrument: StatelessEntity, attackType: AttackType): number {
    const actorWeight = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Weight);
    const actorStrength = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Strength);
    const actorAgility = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Agility);
    const actorDexterity = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Dexterity);
    const instrumentWeight = attrs.Attribute.value<attrs.NumericValueAttribute>(instrument, attrs.Weight);
    const instrumentDamage = attrs.Attribute.value<attrs.NumericValueAttribute>(instrument, attrs.Damage);
    const instrumentAgility = attrs.Attribute.value<attrs.NumericValueAttribute>(instrument, attrs.Agility);

    const damage = instrumentDamage.value + (actorWeight.value * (actorStrength.value * instrumentWeight.value * instrumentAgility.value) + actorAgility.value + actorDexterity.value) / (actorWeight.value + instrumentWeight.value);
    return damage * this.damageMultiplier(attackType);
  }

  public attackStaminaConsumption(actor: Entity, instrument: StatelessEntity, attackType: AttackType): number {
    const actorWeight = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Weight);
    const actorAgility = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Agility);
    const actorDexterity = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Dexterity);
    const instrumentWeight = attrs.Attribute.value<attrs.NumericValueAttribute>(instrument, attrs.Weight);
    const instrumentAgility = attrs.Attribute.value<attrs.NumericValueAttribute>(instrument, attrs.Agility);

    const staminaConsumptionAttack = (actorWeight.value / 100) + (instrumentWeight.value / instrumentAgility.value) / (actorAgility.value + actorDexterity.value);
    return staminaConsumptionAttack * this.staminaMultiplier(attackType);
  }

  public blockWindUpTime(actor: Entity, instrument: StatelessEntity): number {
    const actorStrength = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Strength);
    const actorAgility = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Agility);
    const actorDexterity = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Dexterity);
    const instrumentWeight = attrs.Attribute.value<attrs.NumericValueAttribute>(instrument, attrs.Weight);
    const instrumentAgility = attrs.Attribute.value<attrs.NumericValueAttribute>(instrument, attrs.Agility);

    const windUpTime = (1000 / this.timeDifficultyMultiplier()) * (instrumentWeight.value / instrumentAgility.value) / (actorStrength.value + actorAgility.value + actorDexterity.value);
    return windUpTime / 3;
  }

  public blockStaminaConsumption(actor: Entity, instrument: StatelessEntity): number {
    const actorWeight = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Weight);
    const actorStrength = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Strength);
    const actorDexterity = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Dexterity);
    const instrumentWeight = attrs.Attribute.value<attrs.NumericValueAttribute>(instrument, attrs.Weight);
    const instrumentAgility = attrs.Attribute.value<attrs.NumericValueAttribute>(instrument, attrs.Agility);

    const staminaConsumptionBlock = (actorWeight.value / 100) + (instrumentWeight.value / instrumentAgility.value) / (actorDexterity.value + actorStrength.value);
    return staminaConsumptionBlock / 2;
  }

  public dodgeStaminaConsumption(actor: Entity): number {
    const actorWeight = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Strength);
    const actorAgility = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Agility);
    const actorDexterity = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Dexterity);

    const staminaConsumptionDodge = (actorWeight.value / 100) + 1 / (actorDexterity.value + actorAgility.value);
    return 2 * staminaConsumptionDodge;
  }

  public dodgeCoolDownTime(actor: Entity): number {
    const actorStrength = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Strength);
    const actorAgility = attrs.Attribute.value<attrs.NumericValueAttribute>(actor, attrs.Agility);

    const coolDownTime = 0.5 * 1000 / (actorStrength.value + actorAgility.value);
    return coolDownTime;
  }

  private damageMultiplier(attackType: AttackType): number {
    switch( attackType ) {
      case AttackType.fast:
        return 0.6;
      case AttackType.strong:
      default:
        return 1;
    }
  }

  private timeMultiplier(attackType: AttackType): number {
    switch( attackType ) {
      case AttackType.fast:
        return 0.4;
      case AttackType.strong:
      default:
        return 1;
    }
  }

  private staminaMultiplier(attackType: AttackType): number {
    switch( attackType ) {
      case AttackType.fast:
        return 0.5;
      case AttackType.strong:
      default:
        return 1;
    }
  }

  private timeDifficultyMultiplier(): number {
    return 3;
  }
}