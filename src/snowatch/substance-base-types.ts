
export enum SubstancePhases {
  solid = 'solid',
  liquid = 'liquid',
  gas = 'gas',
}
