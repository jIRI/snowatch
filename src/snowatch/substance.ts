export * from 'snowatch/substance-base-types';

import {GameData} from 'snowatch/game-data';
import {Topic, Message, broadcast, message, payload} from 'snowatch/message';
import {Topics} from 'snowatch/message-topics/all';
import {Behavior} from 'snowatch/behavior';
import {Attribute} from 'snowatch/attribute';
import {Surface} from 'snowatch/surface';
import {clamp, getTypeId} from 'snowatch/utils';
import {SubstancePhases} from 'snowatch/substance-base-types';
import * as behaviors from 'snowatch/behaviors/all';
import * as ent from 'snowatch/entity';
import * as spatial from 'snowatch/spatial-relations';
import * as attrs from 'snowatch/attributes-ids';
import * as scene from 'snowatch/scene';

export interface SubstanceInit extends ent.EntityInit {
  substancePhase?: SubstancePhases;
  title?: string;
  startSceneId?: string;
  isVisible?: boolean;
  description?: string;
  nounId?: string;
  adjectiveId?: string;
  holderId?: string | null;
  ownerId?: string | null;
  surfaces?: Array<string>;
  volume?: number;
  spatialLocation?: spatial.Vector3;
  spatialSize?: spatial.Vector3;
  viscosity?: number;
  constRateOfEvaporation?: number;
  constRateOfDissipation?: number;
  containerId?: string;
}

export function getSubstancePhase(substance: ent.Entity, actor?: ent.Entity): SubstancePhases | undefined {
  if( attrs.Attribute.applies(substance, attrs.SubstancePhase, actor) ) {
    return attrs.Attribute.value<SubstancePhases>(substance, attrs.SubstancePhase, actor);
  }

  return undefined;
}

export class Substance extends ent.Entity {
  static ensureIsInstanceOf(obj: Substance | SubstanceInit): Substance {
   if( obj instanceof Substance ) {
     return obj;
   }
   return new Substance(obj);
  }

  public static OBJECT_GROUP = 'substances';
  static registrator(game: GameData, o: Substance | SubstanceInit): Substance {
    const obj = Substance.ensureIsInstanceOf(o);
    return obj.initialize(game);
  }

  static _instanceId: number = 0;
  static nextInstanceId(): string {
    return (++Substance._instanceId).toString();
  }

  public static decreaseVolumeRelative(substance: Substance, volumeToBeRemoved: number) {
    const oldVolume = attrs.Attribute.value<number>(substance, attrs.Volume);
    const newVolume = oldVolume - oldVolume * clamp(volumeToBeRemoved, 0.0, 1.0);
    attrs.Attribute.setValue<number>(substance, attrs.Volume, newVolume);
    substance.game.bus.notifications.publish(broadcast(Topics.SUBSTANCE_VOLUME_DECREASED, { substanceId: substance.id, oldVolume: oldVolume, newVolume: newVolume }, undefined, substance.id));
  }

  public static decreaseVolume(substance: Substance, volumeToBeRemoved: number) {
    const oldVolume = attrs.Attribute.value<number>(substance, attrs.Volume);
    const newVolume = oldVolume - volumeToBeRemoved;
    attrs.Attribute.setValue<number>(substance, attrs.Volume, newVolume);
    substance.game.bus.notifications.publish(broadcast(Topics.SUBSTANCE_VOLUME_DECREASED, { substanceId: substance.id, oldVolume: oldVolume, newVolume: newVolume }, undefined, substance.id));
  }

  public static increaseVolume(substance: Substance, volumeToBeAdded: number) {
    const oldVolume = attrs.Attribute.value<number>(substance, attrs.Volume);
    const newVolume = oldVolume + volumeToBeAdded;
    attrs.Attribute.setValue<number>(substance, attrs.Volume, newVolume);
    substance.game.bus.notifications.publish(broadcast(Topics.SUBSTANCE_VOLUME_INCREASED, { substanceId: substance.id, oldVolume: oldVolume, newVolume: newVolume }, undefined, substance.id));
  }

  public static setVolume(substance: Substance, volume: number) {
    const oldVolume = attrs.Attribute.value<number>(substance, attrs.Volume);
    const newVolume = volume;
    attrs.Attribute.setValue<number>(substance, attrs.Volume, newVolume);
    substance.game.bus.notifications.publish(broadcast(Topics.SUBSTANCE_VOLUME_INCREASED, { substanceId: substance.id, oldVolume: oldVolume, newVolume: newVolume }, undefined, substance.id));
  }

  public static addCompound(substance: Substance, compoundId: string) {
    const compounds = attrs.Attribute.value<Set<string>>(substance, attrs.SubstanceCompounds);
    compounds.add(compoundId);
    attrs.Attribute.setValue<Set<string>>(substance, attrs.SubstanceCompounds, compounds);
  }

  public static split(substance: Substance, volumeToBeUsed: number): Substance | null {
    const originalVolume = attrs.Attribute.value<number>(substance, attrs.Volume);
    let actualVolumeToBeUsed = clamp(volumeToBeUsed, 0, originalVolume);
    if( originalVolume === 0 ?? actualVolumeToBeUsed === 0 ) {
      return null;
    }

    // try create instance for subclassed substances
    let newInstance = substance.game.instances.create<ent.StatelessEntity>(getTypeId(substance));
    let newSubstance = null;
    if( newInstance != null ) {
      // there is subclass, register new instance
      // update instanceId
      newInstance.instanceId = Substance.nextInstanceId();
      newSubstance = ent.registerEntities(substance.game, Substance.OBJECT_GROUP, newInstance, Substance.registrator);
    } else {
      // no subclass, this is probably parametrically created instance, reproduce it by copying init
      const newInit = { ...substance.init };
      newInit.id = substance.baseId;
      newInit.instanceId = Substance.nextInstanceId();
      newSubstance = ent.registerEntities(substance.game, Substance.OBJECT_GROUP, newInit, Substance.registrator);
    }

    newSubstance.initializeComponents();

    // once the instance is initialized we need to redo some of the attributes
    spatial.SpatialRelations.removeRelationsOf(newSubstance.game, newSubstance.id);
    if( attrs.Attribute.value<string>(newSubstance, attrs.StartSceneId) != null ) {
      attrs.Attribute.setValue<string>(newSubstance, attrs.StartSceneId, null);
    }
    if( attrs.Attribute.value<string>(newSubstance, attrs.ContainerId) != null ) {
      attrs.Attribute.setValue<string>(newSubstance, attrs.ContainerId, null);
    }

    const sceneId = scene.getCurrentSceneId(substance);
    if( sceneId != null ) {
      scene.setSceneId(newSubstance, sceneId);
    }

    const compounds = attrs.Attribute.value<Set<string>>(substance, attrs.SubstanceCompounds);
    attrs.Attribute.setValue<Set<string>>(newSubstance, attrs.SubstanceCompounds, compounds);

    Substance.setVolume(newSubstance, actualVolumeToBeUsed);
    Substance.decreaseVolume(substance, actualVolumeToBeUsed);
    return newSubstance;
  }

  public static join(first: Substance, second: Substance): Substance {
    if( first === second ) {
      return first;
    }

    const secondVolume = attrs.Attribute.value<number>(second, attrs.Volume);
    Substance.increaseVolume(first, secondVolume);
    if( first.baseId !== second.baseId ) {
      Substance.addCompound(first, second.baseId);
    }
    attrs.Attribute.setValue<boolean>(second, attrs.IsVisible, false);
    first.game.addToGarbage(second);
    return first;
  }

  init: SubstanceInit;

  constructor(init: SubstanceInit | null = null) {
    super(init, Substance.OBJECT_GROUP);
  }

  initialize(game: GameData, init?: SubstanceInit): Substance {
    super.initialize(game, init);

    this.registerInitializer(() => {
      Attribute.initializeAttributes(this);
      Attribute.addFromProperties(this, this.properties);
      Attribute.addConstFromProperties(this, this.properties);
      Attribute.addCompFromProperties(this, this.properties);
      // add default attrs (is ignored if already defined in properties as state, const or computed attr)
      Attribute.add(this, attrs.NounResourceId, { initialState: {value: this.init.nounId ?? this.init.id}});
      Attribute.add(this, attrs.AdjectiveResourceId, { initialState: {value: this.init.adjectiveId ?? null}});
      Attribute.addComp(this, attrs.Title, { compState: {
        value: (entity, attributeId, actor?: ent.Entity) => {
          return ent.getTitle(this).title;
        }
      }});
      Attribute.add(this, attrs.SubstancePhase, { initialState: {value: this.init.substancePhase ?? SubstancePhases.solid}});
      Attribute.add(this, attrs.Description, { initialState: {value: this.init.description ?? "..."}});
      Attribute.add(this, attrs.IsVisible, { initialState: {value: this.init.isVisible != null ? this.init.isVisible : true}});
      Attribute.add(this, attrs.HolderId, { initialState: {value: this.init.holderId ?? null}});
      Attribute.add(this, attrs.OwnerId, { initialState: {value: this.init.ownerId ?? null}});
      Attribute.add(this, attrs.ContainerId, { initialState: {value: this.init.containerId ?? null}});
      Attribute.add(this, attrs.StartSceneId, { initialState: {value: this.init.startSceneId ?? null}});
      Attribute.add(this, attrs.CurrentSceneId, { initialState: {value: null}});
      Attribute.add(this, attrs.SpatialSize, { initialState: {value: this.init.spatialSize ?? {x: 0.1, y: 0.1, z: 0.1} /* 1 liter*/}});
      Attribute.add(this, attrs.SpatialLocation, { initialState: {value: this.init.spatialLocation ?? {x: 0, y: 0, z: 0}}});
      Attribute.add(this, attrs.Volume, { initialState: {value: this.init.volume ?? 1 }});
      Attribute.add(this, attrs.Surfaces, { initialState: {value: this.init.surfaces ?? Surface.none}});
      Attribute.add(this, attrs.ConstantRateOfEvaporation, { initialState: {value: this.init.constRateOfEvaporation ?? 0.1 }});
      Attribute.add(this, attrs.ConstantRateOfDissipation, { initialState: {value: this.init.constRateOfDissipation ?? 0.05 }});
      Attribute.add(this, attrs.Viscosity, { initialState: {value: this.init.viscosity ?? 0.00102 /* water */}});
      spatial.SpatialRelations.addFromProperties(this, this.properties);
    });

    this.registerInitializer(() => {
      Behavior.initializeBehaviors(this);
      if( this.properties.behaviors != null ) {
        ent.registerEntities(this.game, Behavior.OBJECT_GROUP, Array.from(this.properties.behaviors.values()), Behavior.registrator);
        Behavior.addFromProperties(this, this.properties);

        Behavior.add(this, behaviors.SupportsContainer.ID, { initialState: behaviors.createEmptyBehaviorState() });
        Behavior.add(this, behaviors.SupportsInventory.ID, { initialState: behaviors.createEmptyBehaviorState() });

        for(let [behaviorId, behavior] of this.properties.behaviors) {
          Behavior.registerForActivation(this, behaviorId, behavior.initialState);
        }
      }
    });

    return this;
  }

  onStart(message: Message) {
    super.onStart(message);
    const startSceneId = scene.getStartSceneId(this);
    // we check here for the case that current scene was changed along the way during initialization phase
    if( startSceneId != null && scene.getCurrentSceneId(this) == null ) {
      scene.setSceneId(this, startSceneId);
    }
  }
}
