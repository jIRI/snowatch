import { Entity, StatelessEntity } from 'snowatch/entity';
import { Surface } from 'snowatch/surface';
import { getSpatialSize, SpatialRelations, SpatialRelation, areaSurface } from 'snowatch/spatial-relations';
import { SpatialPrepositions } from 'snowatch/prepositions';
import { clamp } from 'snowatch/utils';
import * as attrs from 'snowatch/attributes-ids';

export class SpatialCapacityComputedAttribute implements attrs.ComputedAttribute {
  public static readonly ID = 'SpatialCapacityComputedAttribute';
  public static readonly default = new SpatialCapacityComputedAttribute();

  constructor() {
  }

  value(entity: StatelessEntity, attributeId: string, actor?: Entity): attrs.SpatialCapacityMap {
    const capacity = attrs.NewSpatialCapacityValue(SpatialCapacityComputedAttribute.getInitialCapacity(entity));

    const relations = SpatialRelations.relationsForIndirect(entity);
    relations.forEach(relation => SpatialCapacityComputedAttribute.updateCapacity(capacity, entity, relation));
    return capacity;
  }

  applicability(entity: StatelessEntity, attributeId: string, actor?: Entity): boolean {
    return true;
  }

  static getInitialCapacity(entity: StatelessEntity): Iterable<[string, attrs.NumericValueAttribute]> {
    const spatialSize = getSpatialSize(entity);
    if( spatialSize == null) {
      return new Array<[string, attrs.NumericValueAttribute]>();
    }

    return SpatialPrepositions.all().map<[string, attrs.NumericValueAttribute]>(p => {
      const capacity = areaSurface(p.targetSurfaceId, spatialSize);
      return [p.targetSurfaceId, {
        minValue: 0,
        maxValue: capacity,
        value: capacity,
        initialValue: capacity,
      }];
    });
  }

  static updateCapacity(capacity: attrs.SpatialCapacityMap, target: StatelessEntity, relation: SpatialRelation) {
    const relationDef = SpatialPrepositions.allById().get(relation.typeId);
    if( relationDef == null ) {
      return;
    }

    const value = capacity.get(relationDef.targetSurfaceId);
    if( value == null ) {
      return;
    }

    const oppositeSurface = Surface.getOpposite(relationDef.targetSurfaceId);
    if( oppositeSurface == null ) {
      return;
    }

    const directEntity = target.game.instances.getSpatialFromId(relation.directId);
    if( directEntity == null ) {
      return;
    }

    const directSize = getSpatialSize(directEntity);
    if( directSize == null ) {
      return;
    }

    const oppositeArea = areaSurface(oppositeSurface, directSize);
    value.value = clamp(value.value - oppositeArea, value.minValue, value.maxValue);

    return value;
  }
}