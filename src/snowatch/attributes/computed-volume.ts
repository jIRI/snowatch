import { Entity, StatelessEntity } from 'snowatch/entity';
import { Size3, Vector3 } from 'snowatch/spatial-relations';

import * as attrs from 'snowatch/attributes-ids';

export class VolumeComputedAttribute implements attrs.ComputedAttribute {
  public static readonly ID = 'VolumeComputedAttribute';
  public static readonly default = new VolumeComputedAttribute();

  constructor() {
  }

  value(entity: StatelessEntity, attributeId: string, actor?: Entity): number {
    const spatialSize = attrs.Attribute.value<Vector3>(entity, attrs.SpatialSize, actor);
    // we reduce the volume a bit to simulate thickness of the container's shell
    return 0.95 * Size3.volume(spatialSize);
  }

  applicability(entity: StatelessEntity, attributeId: string, actor?: Entity): boolean {
    return attrs.Attribute.applies(entity, attrs.SpatialSize, actor);
  }
}