import {FrameworkConfiguration} from 'aurelia-framework';

import * as attributes from 'snowatch/attributes/all';

export function configure(aurelia: FrameworkConfiguration) {
  aurelia.singleton(attributes.ComputedAttribute, attributes.SpatialCapacityComputedAttribute);
  aurelia.singleton(attributes.ComputedAttribute, attributes.VolumeComputedAttribute);
}
