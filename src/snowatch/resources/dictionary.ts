import * as lang from 'snowatch/language-structures';

export class ToBe extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.to-be`;
  constructor() {
    super({
      verb: 'to-be',
      infinitive: 'to be',
      singular: {
        present: { first: 'am', second: 'are', third: 'is', },
        future: 'will be',
        imperative: 'be',
        past: { first: 'was', second: 'were', third: 'was', },
        passive: { first: 'was', second: 'were', third: 'was', },
      },
      plural: {
        present: 'are',
        future: 'will be',
        imperative: 'be',
        past: 'were',
        passive: 'were',
      }
    });
  }
}

export class NotToBe extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.not-to-be`;
  constructor() {
    super({
      verb: 'not-to-be',
      infinitive: 'not to be',
      singular: {
        present: { first: 'am not', second: 'are not', third: 'is not', },
        future: 'will not be',
        imperative: 'not be',
        past: { first: 'was not', second: 'were not', third: 'was not', },
        passive: { first: 'was not', second: 'were not', third: 'was not', },
      },
      plural: {
        present: 'are not',
        future: 'will not be',
        imperative: 'not be',
        past: 'were not',
        passive: 'were not',
      }
    });
  }
}

export class Give extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.give`;
  constructor() {
    super({
      verb: 'give',
      infinitive: 'to give',
      singular: {
        present: { first: 'give', second: 'give', third: 'gives', },
        future: 'will give',
        imperative: 'give',
        past: 'gave',
        passive: 'given',
      },
      plural: {
        present: 'give',
        future: 'will give',
        imperative: 'give',
        past: 'gave',
        passive: 'given',
      }
    });
  }
}

export class Take extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.take`;
  constructor() {
    super({
      verb: 'take',
      infinitive: 'to take',
      singular: {
        present: { first: 'take', second: 'take', third: 'takes', },
        future: 'will take',
        imperative: 'take',
        past: 'took',
        passive: 'taken',
      },
      plural: {
        present: 'take',
        future: 'will take',
        imperative: 'take',
        past: 'took',
        passive: 'taken',
      }
    });
  }
}

export class Put extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.put`;
  constructor() {
    super({
      verb: 'put',
      infinitive: 'to put',
      singular: {
        present: { first: 'put', second: 'put', third: 'puts', },
        future: 'will put',
        imperative: 'put',
        past: 'put',
        passive: 'put',
      },
      plural: {
        present: 'put',
        future: 'will put',
        imperative: 'put',
        past: 'put',
        passive: 'put',
      }
    });
  }
}

export class Move extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.move`;
  constructor() {
    super({
      verb: 'move',
      infinitive: 'to move',
      singular: {
        present: { first: 'move', second: 'move', third: 'moves', },
        future: 'will move',
        imperative: 'move',
        past: 'moved',
        passive: 'moved',
      },
      plural: {
        present: 'move',
        future: 'will move',
        imperative: 'move',
        past: 'moved',
        passive: 'moved',
      }
    });
  }
}

export class Close extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.close`;
  constructor() {
    super({
      verb: 'close',
      infinitive: 'to close',
      singular: {
        present: { first: 'close', second: 'close', third: 'closes', },
        future: 'will close',
        imperative: 'close',
        past: 'closed',
        passive: 'closed',
      },
      plural: {
        present: 'close',
        future: 'will close',
        imperative: 'close',
        past: 'closed',
        passive: 'closed',
      }
    });
  }
}

export class Open extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.open`;
  constructor() {
    super({
      verb: 'open',
      infinitive: 'to open',
      singular: {
        present: { first: 'open', second: 'open', third: 'opens', },
        future: 'will open',
        imperative: 'open',
        past: 'opened',
        passive: 'opened',
      },
      plural: {
        present: 'open',
        future: 'will open',
        imperative: 'open',
        past: 'opened',
        passive: 'opened',
      }
    });
  }
}

export class Lock extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.lock`;
  constructor() {
    super({
      verb: 'lock',
      infinitive: 'to lock',
      singular: {
        present: { first: 'lock', second: 'lock', third: 'locks', },
        future: 'will lock',
        imperative: 'lock',
        past: 'locked',
        passive: 'locked',
      },
      plural: {
        present: 'lock',
        future: 'will lock',
        imperative: 'lock',
        past: 'locked',
        passive: 'locked',
      }
    });
  }
}

export class Say extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.say`;
  constructor() {
    super({
      verb: 'say',
      infinitive: 'to say',
      singular: {
        present: { first: 'say', second: 'say', third: 'says', },
        future: 'will say',
        imperative: 'say',
        past: 'said',
        passive: 'said',
      },
      plural: {
        present: 'say',
        future: 'will say',
        imperative: 'say',
        past: 'said',
        passive: 'said',
      }
    });
  }
}

export class Learn extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.learn`;
  constructor() {
    super({
      verb: 'learn',
      infinitive: 'to learn',
      singular: {
        present: { first: 'learn', second: 'learn', third: 'learns', },
        future: 'will learn',
        imperative: 'learn',
        past: 'learnt',
        passive: 'learnt',
      },
      plural: {
        present: 'learn',
        future: 'will learn',
        imperative: 'learn',
        past: 'learnt',
        passive: 'learnt',
      }
    });
  }
}

export class Teach extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.teach`;
  constructor() {
    super({
      verb: 'teach',
      infinitive: 'to teach',
      singular: {
        present: { first: 'teach', second: 'teach', third: 'teaches', },
        future: 'will teach',
        imperative: 'teach',
        past: 'taught',
        passive: 'taught',
      },
      plural: {
        present: 'teach',
        future: 'will teach',
        imperative: 'teach',
        past: 'taught',
        passive: 'taught',
      }
    });
  }
}

export class Unlock extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.unlock`;
  constructor() {
    super({
      verb: 'unlock',
      infinitive: 'to unlock',
      singular: {
        present: { first: 'unlock', second: 'unlock', third: 'unlocks', },
        future: 'will unlock',
        imperative: 'unlock',
        past: 'unlocked',
        passive: 'unlocked',
      },
      plural: {
        present: 'unlock',
        future: 'will unlock',
        imperative: 'unlock',
        past: 'unlocked',
        passive: 'unlocked',
      }
    });
  }
}

export class Press extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.press`;
  constructor() {
    super({
      verb: 'press',
      infinitive: 'to press',
      singular: {
        present: { first: 'press', second: 'press', third: 'presses', },
        future: 'will press',
        imperative: 'press',
        past: 'pressed',
        passive: 'pressed',
      },
      plural: {
        present: 'press',
        future: 'will press',
        imperative: 'press',
        past: 'pressed',
        passive: 'pressed',
      }
    });
  }
}

export class Drink extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.drink`;
  constructor() {
    super({
      verb: 'drink',
      infinitive: 'to drink',
      singular: {
        present: { first: 'drink', second: 'drink', third: 'drinks', },
        future: 'will drink',
        imperative: 'drink',
        past: 'drank',
        passive: 'drunk',
      },
      plural: {
        present: 'drink',
        future: 'will drink',
        imperative: 'drink',
        past: 'drank',
        passive: 'drunk',
      }
    });
  }
}

export class DrinkUp extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.drink-up`;
  constructor() {
    super({
      verb: 'drink',
      infinitive: 'to drink',
      singular: {
        present: { first: 'drink', second: 'drink', third: 'drinks', },
        future: 'will drink',
        imperative: 'drink',
        past: 'drank',
        passive: 'drunk',
      },
      plural: {
        present: 'drink',
        future: 'will drink',
        imperative: 'drink',
        past: 'drank',
        passive: 'drunk',
      }
    });
  }
}

export class LookAround extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.look-around`;
  constructor() {
    super({
      verb: 'look around',
      infinitive: 'to look around',
      singular: {
        present: { first: 'look around', second: 'look around', third: 'looks around', },
        future: 'will look around',
        imperative: 'look around',
        past: 'looked around',
        passive: 'looked around',
      },
      plural: {
        present: 'look around',
        future: 'will look around',
        imperative: 'look around',
        past: 'looked around',
        passive: 'looked around',
      }
    });
  }
}

export class Pour extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.pour`;
  constructor() {
    super({
      verb: 'pour',
      infinitive: 'to pour',
      singular: {
        present: { first: 'pour', second: 'pour', third: 'pours', },
        future: 'will pour',
        imperative: 'pour',
        past: 'poured',
        passive: 'poured',
      },
      plural: {
        present: 'pour',
        future: 'will pour',
        imperative: 'pour',
        past: 'poured',
        passive: 'poured',
      }
    });
  }
}

export class Try extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.try`;
  constructor() {
    super({
      verb: 'try',
      infinitive: 'to try',
      singular: {
        present: { first: 'try', second: 'try', third: 'tries', },
        future: 'will try',
        imperative: 'try',
        past: 'tried',
        passive: 'tried',
      },
      plural: {
        present: 'try',
        future: 'will try',
        imperative: 'try',
        past: 'tried',
        passive: 'tried',
      }
    });
  }
}

export class Evaporate extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.evaporate`;
  constructor() {
    super({
      verb: 'evaporate',
      infinitive: 'to evaporate',
      singular: {
        present: { first: 'evaporate', second: 'evaporate', third: 'evaporates', },
        future: 'will evaporate',
        imperative: 'evaporate',
        past: 'evaporated',
        passive: 'evaporated',
      },
      plural: {
        present: 'evaporate',
        future: 'will evaporate',
        imperative: 'evaporate',
        past: 'evaporated',
        passive: 'evaporated',
      }
    });
  }
}

export class SmellVerb extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.smell`;
  constructor() {
    super({
      verb: 'smell',
      infinitive: 'to smell',
      singular: {
        present: { first: 'smell', second: 'smell', third: 'smells', },
        future: 'will smell',
        imperative: 'smell',
        past: 'smelled',
        passive: 'smelled',
      },
      plural: {
        present: 'smell',
        future: 'will smell',
        imperative: 'smell',
        past: 'smelled',
        passive: 'smelled',
      }
    });
  }
}

export class Enter extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.enter`;
  constructor() {
    super({
      verb: 'enter',
      infinitive: 'to enter',
      singular: {
        present: { first: 'enter', second: 'enter', third: 'enters', },
        future: 'will enter',
        imperative: 'enter',
        past: 'entered',
        passive: 'entered',
      },
      plural: {
        present: 'enter',
        future: 'will enter',
        imperative: 'enter',
        past: 'entered',
        passive: 'entered',
      }
    });
  }
}

export class Leave extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.leave`;
  constructor() {
    super({
      verb: 'leave',
      infinitive: 'to leave',
      singular: {
        present: { first: 'leave', second: 'leave', third: 'leaves', },
        future: 'will leave',
        imperative: 'leave',
        past: 'left',
        passive: 'left',
      },
      plural: {
        present: 'leave',
        future: 'will leave',
        imperative: 'leave',
        past: 'left',
        passive: 'left',
      }
    });
  }
}

export class Engage extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.engage`;
  constructor() {
    super({
      verb: 'engage',
      infinitive: 'to engage',
      singular: {
        present: { first: 'engage', second: 'engage', third: 'engages', },
        future: 'will engage',
        imperative: 'engage',
        past: 'engaged',
        passive: 'engaged',
      },
      plural: {
        present: 'engage',
        future: 'will engage',
        imperative: 'engage',
        past: 'engaged',
        passive: 'engaged',
      }
    });
  }
}

export class Attack extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.attack`;
  constructor() {
    super({
      verb: 'attack',
      infinitive: 'to attack',
      singular: {
        present: { first: 'attack', second: 'attack', third: 'attacks', },
        future: 'will attack',
        imperative: 'attack',
        past: 'attacked',
        passive: 'attacked',
      },
      plural: {
        present: 'attack',
        future: 'will attack',
        imperative: 'attack',
        past: 'attacked',
        passive: 'attacked',
      }
    });
  }
}

export class Block extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.block`;
  constructor() {
    super({
      verb: 'block',
      infinitive: 'to block',
      singular: {
        present: { first: 'block', second: 'block', third: 'blocks', },
        future: 'will block',
        imperative: 'block',
        past: 'parried',
        passive: 'parried',
      },
      plural: {
        present: 'block',
        future: 'will block',
        imperative: 'block',
        past: 'parried',
        passive: 'parried',
      }
    });
  }
}

export class Dodge extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.dodge`;
  constructor() {
    super({
      verb: 'dodge',
      infinitive: 'to dodge',
      singular: {
        present: { first: 'dodge', second: 'dodge', third: 'dodges', },
        future: 'will dodge',
        imperative: 'dodge',
        past: 'dodged',
        passive: 'dodged',
      },
      plural: {
        present: 'dodge',
        future: 'will dodge',
        imperative: 'dodge',
        past: 'dodged',
        passive: 'dodged',
      }
    });
  }
}

export class Evade extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.evade`;
  constructor() {
    super({
      verb: 'evade',
      infinitive: 'to evade',
      singular: {
        present: { first: 'evade', second: 'evade', third: 'evades', },
        future: 'will evade',
        imperative: 'evade',
        past: 'evaded',
        passive: 'evaded',
      },
      plural: {
        present: 'evade',
        future: 'will evade',
        imperative: 'evade',
        past: 'evaded',
        passive: 'evaded',
      }
    });
  }
}

export class Hit extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.hit`;
  constructor() {
    super({
      verb: 'hit',
      infinitive: 'to hit',
      singular: {
        present: { first: 'hit', second: 'hit', third: 'hits', },
        future: 'will hit',
        imperative: 'hit',
        past: 'hit',
        passive: 'hit',
      },
      plural: {
        present: 'hit',
        future: 'will hit',
        imperative: 'hit',
        past: 'hit',
        passive: 'hit',
      }
    });
  }
}

export class Die extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.die`;
  constructor() {
    super({
      verb: 'die',
      infinitive: 'to die',
      singular: {
        present: { first: 'die', second: 'die', third: 'dies', },
        future: 'will die',
        imperative: 'die',
        past: 'died',
        passive: 'died',
      },
      plural: {
        present: 'die',
        future: 'will die',
        imperative: 'die',
        past: 'died',
        passive: 'died',
      }
    });
  }
}

export class SmellNoun extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.smell`;
  constructor() {
    super({
      noun: 'smell',
      number: lang.Number.singular,
    });
  }
}

export class Ground extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.ground`;
  constructor() {
    super({
      noun: 'ground',
      number: lang.Number.singular,
    });
  }
}

export class Hand extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.hand`;
  constructor() {
    super({
      noun: 'hand',
      number: lang.Number.singular,
    });
  }
}

export class Hands extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.hand`;
  constructor() {
    super({
      noun: 'hand',
      number: lang.Number.plural,
    });
  }
}

export class Neutrally extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.neutrally`;
  constructor() {
    super({
      adverb: '',
    });
  }
}

export class Strongly extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.strongly`;
  constructor() {
    super({
      adverb: 'strongly',
    });
  }
}

export class Weakly extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.weakly`;
  constructor() {
    super({
      adverb: 'weakly',
    });
  }
}

export class Quickly extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.quickly`;
  constructor() {
    super({
      adverb: 'quickly',
    });
  }
}

export class Quietly extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.quietly`;
  constructor() {
    super({
      adverb: 'quietly',
    });
  }
}

export class Whisperingly extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.whisperingly`;
  constructor() {
    super({
      adverb: 'whisperingly',
    });
  }
}

export class Slowly extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.slowly`;
  constructor() {
    super({
      adverb: 'slowly',
    });
  }
}

export class Hesitantly extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.hesitantly`;
  constructor() {
    super({
      adverb: 'hesitantly',
    });
  }
}

export class Cautiously extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.cautiously`;
  constructor() {
    super({
      adverb: 'cautiously',
    });
  }
}

export class Loudly extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.loudly`;
  constructor() {
    super({
      adverb: 'loudly',
    });
  }
}

export class Angrily extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.angrily`;
  constructor() {
    super({
      adverb: 'angrily',
    });
  }
}

export class Patiently extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.patiently`;
  constructor() {
    super({
      adverb: 'patiently',
    });
  }
}

export class To extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.to`;
  constructor() {
    super({
      preposition: 'to',
    });
  }
}

export class EngageSomeone extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.engage-someone`;
  constructor() {
    super({
      preposition: '',
      requiredCase: lang.Case.dative
    });
  }
}

export class EvadeSomeone extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.evade-someone`;
  constructor() {
    super({
      preposition: '',
      requiredCase: lang.Case.dative
    });
  }
}

export class DoActionWith extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.do-action-with`;
  constructor() {
    super({
      preposition: 'with',
    });
  }
}

export class AccusativeOn extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.accusative-on`;
  constructor() {
    super({
      preposition: '',
    });
  }
}

export class SpatialOn extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-on`;
  constructor() {
    super({
      preposition: 'on',
    });
  }
}

export class SpatialUnder extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-under`;
  constructor() {
    super({
      preposition: 'under',
    });
  }
}

export class SpatialBelow extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-below`;
  constructor() {
    super({
      preposition: 'below',
    });
  }
}

export class SpatialIn extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-in`;
  constructor() {
    super({
      preposition: 'in',
    });
  }
}

export class SpatialFront extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-front`;
  constructor() {
    super({
      preposition: 'in front of',
    });
  }
}

export class SpatialBehind extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-behind`;
  constructor() {
    super({
      preposition: 'behind',
    });
  }
}

export class SpatialBack extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-back`;
  constructor() {
    super({
      preposition: 'to the back',
    });
  }
}

export class SpatialLeft extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-left`;
  constructor() {
    super({
      preposition: 'to the left side of',
    });
  }
}

export class SpatialRight extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-right`;
  constructor() {
    super({
      preposition: 'to the right side of',
    });
  }
}

export class And extends lang.Conjunction {
  public static readonly ID = `${lang.Conjunction.ID}.and`;
  constructor() {
    super({
      conjunction: 'and',
    });
  }
}

export class Big extends lang.Adjective {
  public static readonly ID = `${lang.Adjective.ID}.big`;
  constructor() {
    super({
      adjective: 'big',
      singular: 'big',
      plural: 'big'
    });
  }
}

export class Stunned extends lang.Adjective {
  public static readonly ID = `${lang.Adjective.ID}.stunned`;
  constructor() {
    super({
      adjective: 'stunned',
      singular: 'stunned',
      plural: 'stunned'
    });
  }
}


export class BitOfIt extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.bit-of-it`;
  constructor() {
    super({
      adverb: 'a bit',
    });
  }
}

export class SomeOfIt extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.some-of-it`;
  constructor() {
    super({
      adverb: 'some',
    });
  }
}

export class HalfOfIt extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.half-of-it`;
  constructor() {
    super({
      adverb: 'half',
    });
  }
}

export class AllOfIt extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.all-of-it`;
  constructor() {
    super({
      adverb: 'all',
    });
  }
}

export class InstrumentWith extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.instrument-with`;
  constructor() {
    super({
      preposition: 'with',
      requiredCase: lang.Case.instrumental
    });
  }
}
