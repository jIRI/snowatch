import {ResourceDictionary} from 'snowatch/resource-dictionary';
import * as lang from 'snowatch/language-structures';
import * as dict from 'snowatch/resources/dictionary';

export class DefaultResDict extends ResourceDictionary {
  public static readonly GROUND_NOUN = 'ground-noun-id';

  public static readonly SPATIAL_ACTION_ON = 'spatial-action-on';
  public static readonly SPATIAL_ACTION_UNDER = 'spatial-action-under';
  public static readonly SPATIAL_ACTION_BELOW = 'spatial-action-below';
  public static readonly SPATIAL_ACTION_IN = 'spatial-action-in';
  public static readonly SPATIAL_ACTION_FRONT = 'spatial-action-front';
  public static readonly SPATIAL_ACTION_BEHIND = 'spatial-action-behind';
  public static readonly SPATIAL_ACTION_BACK = 'spatial-action-back';
  public static readonly SPATIAL_ACTION_LEFT = 'spatial-action-left';
  public static readonly SPATIAL_ACTION_RIGHT = 'spatial-action-right';

  public static readonly SPATIAL_DESCRIPTION_ON = 'spatial-description-on';
  public static readonly SPATIAL_DESCRIPTION_UNDER = 'spatial-description-under';
  public static readonly SPATIAL_DESCRIPTION_BELOW = 'spatial-description-below';
  public static readonly SPATIAL_DESCRIPTION_IN = 'spatial-description-in';
  public static readonly SPATIAL_DESCRIPTION_FRONT = 'spatial-description-front';
  public static readonly SPATIAL_DESCRIPTION_BEHIND = 'spatial-description-behind';
  public static readonly SPATIAL_DESCRIPTION_BACK = 'spatial-description-back';
  public static readonly SPATIAL_DESCRIPTION_LEFT = 'spatial-description-left';
  public static readonly SPATIAL_DESCRIPTION_RIGHT = 'spatial-description-right';
  public static readonly SPATIAL_RELATION_DESCRIPTION = 'spatial-relation-description';
  public static readonly SPATIAL_RELATION_ADDED_DESC = 'spatial-relation-added-description';
  public static readonly SPATIAL_RELATION_REMOVED_DESC = 'spatial-relation-removed-description';

  public static readonly EVAPORATED_SOME_DESC = 'evaporated-some-description';
  public static readonly EVAPORATED_DESC = 'evaporated-all-description';

  public static readonly SMELL_DESCRIPTION = 'smell-description';
  public static readonly ACTOR_SMELL_DESCRIPTION = 'actor-smells-description';
  public static readonly SMELLS_DESCRIPTION = 'smells-description';

  public static readonly INSTRUMENT_WITH_PREPOSITION = 'instrument-with-preposition';
  public static readonly HANDS_NOUN = 'hands-noun-id';

  public static readonly NEUTRALLY = 'neutrally';
  public static readonly STRONGLY = 'strongly';
  public static readonly WEAKLY = 'weakly';
  public static readonly QUICKLY = 'quickly';
  public static readonly QUIETLY = 'quietly';
  public static readonly WHISPERINGLY = 'whisperingly';
  public static readonly SLOWLY = 'slowly';
  public static readonly HESITANTLY = 'hesitantly';
  public static readonly CAUTIOUSLY = 'cautiously';
  public static readonly LOUDLY = 'loudly';
  public static readonly ANGRILY = 'angrily';
  public static readonly PATIENTLY = 'patiently';
  public static readonly GIVE_TO = 'to';
  public static readonly DO_ACTION_WITH = 'do-action-with';
  public static readonly ACCUSATIVE_ON = 'accusative-on';
  public static readonly ENGAGE_SOMEONE = 'engage-someone';
  public static readonly EVADE_SOMEONE = 'evade-someone';

  public static readonly BIT_OF_IT = 'bit-of-it';
  public static readonly HALF_OF_IT = 'half-of-it';
  public static readonly ALL_OF_IT = 'all-of-it';

  public static readonly GIVE = 'give';
  public static readonly TAKE = 'take';
  public static readonly PUT = 'put';
  public static readonly MOVE = 'move';
  public static readonly CLOSE = 'close';
  public static readonly OPEN = 'open';
  public static readonly LOCK = 'lock';
  public static readonly SAY = 'say';
  public static readonly LEARN = 'learn';
  public static readonly TEACH = 'teach';
  public static readonly UNLOCK = 'unlock';
  public static readonly PRESS = 'press';
  public static readonly DRINK = 'drink';
  public static readonly DRINK_UP = 'drink-up';
  public static readonly POUR = 'pour';
  public static readonly LOOK_AROUND = 'look-around';
  public static readonly ENGAGE = 'engage';
  public static readonly ATTACK = 'attack';
  public static readonly BLOCK = 'block';
  public static readonly DODGE = 'dodge';
  public static readonly EVADE = 'evade';

  public static readonly STUNNED = 'stunned';

  public static readonly TOOK_DAMAGE_DESCRIPTION = 'took-damage-description';
  public static readonly DIED_DESCRIPTION = 'died-description';

  public static readonly NOUN = 'noun';
  public static readonly PREPOSITIONED_INSTRUMENT = 'prepositioned-instrument';
  public static readonly PREPOSITIONED_INDIRECT = 'prepositioned-indirect';
  public static readonly MOVE_REJECTED_TOO_BIG = 'move-rejected-too-big';
  public static readonly TRYING_USE_SKILL = 'try-use-skill';
  public static readonly SKILL_ACCEPTED = 'skill-accepted';
  public static readonly SCENE_ENTERED = 'scene-entered';
  public static readonly SCENE_LEFT = 'scene-left';

  constructor() {
    super({
      culture: 'en',
      resources: [
        { id: DefaultResDict.GROUND_NOUN, value: dict.Ground.ID },

        { id: DefaultResDict.SPATIAL_ACTION_ON, value:  dict.SpatialOn.ID },
        { id: DefaultResDict.SPATIAL_ACTION_UNDER, value: dict.SpatialUnder.ID },
        { id: DefaultResDict.SPATIAL_ACTION_BELOW, value: dict.SpatialBelow.ID },
        { id: DefaultResDict.SPATIAL_ACTION_IN, value: dict.SpatialIn.ID },
        { id: DefaultResDict.SPATIAL_ACTION_FRONT, value: dict.SpatialFront.ID },
        { id: DefaultResDict.SPATIAL_ACTION_BEHIND, value: dict.SpatialBehind.ID },
        { id: DefaultResDict.SPATIAL_ACTION_BACK, value: dict.SpatialBack.ID },
        { id: DefaultResDict.SPATIAL_ACTION_LEFT, value: dict.SpatialLeft.ID },
        { id: DefaultResDict.SPATIAL_ACTION_RIGHT, value: dict.SpatialRight.ID },

        { id: DefaultResDict.SPATIAL_DESCRIPTION_ON, value: dict.SpatialOn.ID },
        { id: DefaultResDict.SPATIAL_DESCRIPTION_UNDER, value: dict.SpatialUnder.ID },
        { id: DefaultResDict.SPATIAL_DESCRIPTION_BELOW, value: dict.SpatialBelow.ID },
        { id: DefaultResDict.SPATIAL_DESCRIPTION_IN, value: dict.SpatialIn.ID },
        { id: DefaultResDict.SPATIAL_DESCRIPTION_FRONT, value: dict.SpatialFront.ID },
        { id: DefaultResDict.SPATIAL_DESCRIPTION_BEHIND, value: dict.SpatialBehind.ID },
        { id: DefaultResDict.SPATIAL_DESCRIPTION_BACK, value: dict.SpatialBack.ID },
        { id: DefaultResDict.SPATIAL_DESCRIPTION_LEFT, value: dict.SpatialLeft.ID },
        { id: DefaultResDict.SPATIAL_DESCRIPTION_RIGHT, value: dict.SpatialRight.ID },

        { id: DefaultResDict.INSTRUMENT_WITH_PREPOSITION, value: dict.InstrumentWith.ID },
        { id: DefaultResDict.HANDS_NOUN, value: dict.Hands.ID },

        { id: DefaultResDict.NEUTRALLY, value: { id: dict.Neutrally.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.STRONGLY, value: { id: dict.Strongly.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.WEAKLY, value: { id: dict.Weakly.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.QUICKLY, value: { id: dict.Quickly.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.QUIETLY, value: { id: dict.Quietly.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.WHISPERINGLY, value: { id: dict.Whisperingly.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.SLOWLY, value: { id: dict.Slowly.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.HESITANTLY, value: { id: dict.Hesitantly.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.CAUTIOUSLY, value: { id: dict.Cautiously.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.LOUDLY, value: { id: dict.Loudly.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.ANGRILY, value: { id: dict.Angrily.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.PATIENTLY, value: { id: dict.Patiently.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.GIVE_TO, value: { id: dict.To.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.DO_ACTION_WITH, value: dict.DoActionWith.ID },
        { id: DefaultResDict.ACCUSATIVE_ON, value: dict.AccusativeOn.ID },
        { id: DefaultResDict.ENGAGE_SOMEONE, value: dict.EngageSomeone.ID },
        { id: DefaultResDict.EVADE_SOMEONE, value: dict.EvadeSomeone.ID },

        { id: DefaultResDict.STUNNED, value: dict.Stunned.ID },

        { id: DefaultResDict.BIT_OF_IT, value: { id: dict.BitOfIt.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.HALF_OF_IT, value: { id: dict.HalfOfIt.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.ALL_OF_IT, value: { id: dict.AllOfIt.ID, kind: lang.Adverb.ID } },

        { id: DefaultResDict.GIVE, value: dict.Give.ID },
        { id: DefaultResDict.TAKE, value: dict.Take.ID },
        { id: DefaultResDict.PUT, value: dict.Put.ID },
        { id: DefaultResDict.MOVE, value: dict.Move.ID },
        { id: DefaultResDict.CLOSE, value: dict.Close.ID },
        { id: DefaultResDict.OPEN, value: dict.Open.ID },
        { id: DefaultResDict.LOCK, value: dict.Lock.ID },
        { id: DefaultResDict.SAY, value: dict.Say.ID },
        { id: DefaultResDict.LEARN, value: dict.Learn.ID },
        { id: DefaultResDict.TEACH, value: dict.Teach.ID },
        { id: DefaultResDict.UNLOCK, value: dict.Unlock.ID },
        { id: DefaultResDict.PRESS, value: dict.Press.ID },
        { id: DefaultResDict.DRINK, value: dict.Drink.ID },
        { id: DefaultResDict.DRINK_UP, value: dict.DrinkUp.ID },
        { id: DefaultResDict.LOOK_AROUND, value: dict.LookAround.ID },
        { id: DefaultResDict.POUR, value: dict.Pour.ID },
        { id: DefaultResDict.ENGAGE, value: dict.Engage.ID },
        { id: DefaultResDict.ATTACK, value: dict.Attack.ID },
        { id: DefaultResDict.BLOCK, value: dict.Block.ID },
        { id: DefaultResDict.DODGE, value: dict.Dodge.ID },
        { id: DefaultResDict.EVADE, value: dict.Evade.ID },

        { id: DefaultResDict.SPATIAL_RELATION_DESCRIPTION, value: {
          template: '${sources} ${be} ${preposition} the ${target}',
          beId: dict.ToBe.ID,
          sourcesSeparator: ', ',
          sourcesConjunction: ' and ',
        }},

        { id: DefaultResDict.NOUN, value: {
          template: '${adjective} ${noun}',
        }},

        { id: DefaultResDict.PREPOSITIONED_INSTRUMENT, value: {
          template: '${preposition} ${noun}',
        }},

        { id: DefaultResDict.PREPOSITIONED_INDIRECT, value: {
          template: '${preposition} ${noun}',
        }},

        { id: DefaultResDict.MOVE_REJECTED_TOO_BIG, value: {
          template: "${directNoun} can't move ${preposition} the ${indirectNoun} because it ${beVerb} too ${bigAdjective}.",
          beVerbId: dict.ToBe.ID,
          bigAdjectiveId: dict.Big.ID
        }},

        { id: DefaultResDict.SPATIAL_RELATION_ADDED_DESC, value: {
          template: '${direct} ${verb} ${preposition} ${indirect}.',
          verbId: dict.ToBe.ID,
        }},

        { id: DefaultResDict.SPATIAL_RELATION_REMOVED_DESC, value: {
          template: '${direct} ${verb} ${preposition} ${indirect} anymore.',
          verbId: dict.NotToBe.ID,
        }},

        { id: DefaultResDict.EVAPORATED_SOME_DESC, value: {
          template: 'Some of the ${direct} ${preposition} the ${indirect} ${evaporateVerb}.',
          evaporateVerbId: dict.Evaporate.ID,
          directCase: lang.Case.genitive,
        }},

        { id: DefaultResDict.EVAPORATED_DESC, value: {
          template: '${direct} ${preposition} the ${indirect} ${evaporateVerb}.',
          evaporateVerbId: dict.Evaporate.ID,
          directCase: lang.Case.nominative,
        }},

        { id: DefaultResDict.SMELL_DESCRIPTION, value: {
          template: 'There ${beVerb} ${smellNoun} of the ${direct} in the air.',
          beVerbId: dict.ToBe.ID,
          smellVerbId: null,
          smellNounId: dict.SmellNoun.ID,
          smellNounCase: lang.Case.accusative,
          directCase: lang.Case.nominative,
        }},
        { id: DefaultResDict.SMELLS_DESCRIPTION, value: {
          template: 'There ${beVerb} ${smellNoun} of the ${substances} in the air.',
          beVerbId: dict.ToBe.ID,
          smellVerbId: null,
          smellNounId: dict.SmellNoun.ID,
          smellNounCase: lang.Case.accusative,
          smellCase: lang.Case.nominative,
          substancesSeparator: ', ',
          substancesConjunction: ' and ',
        }},
        { id: DefaultResDict.ACTOR_SMELL_DESCRIPTION, value: {
          template: '${actorNoun} ${smellVerb} the ${direct}.',
          smellVerbId: dict.SmellVerb.ID,
          actorNounCase: lang.Case.nominative,
          directCase: lang.Case.accusative,
        }},

        { id: DefaultResDict.TOOK_DAMAGE_DESCRIPTION, value: {
          template: '${direct} ${be} ${hitVerb}.',
          beVerbId: dict.ToBe.ID,
          hitVerbId: dict.ToBe.ID,
          directCase: lang.Case.nominative,
        }},
        { id: DefaultResDict.DIED_DESCRIPTION, value: {
          template: '${direct} ${dieVerb}.',
          dieVerbId: dict.Die.ID,
          directCase: lang.Case.nominative,
        }},

        { id: DefaultResDict.TRYING_USE_SKILL, value: {
          templateSimple: '${adverbBegin} ${actorNoun} ${tryVerb} ${adverbMiddle} ${skillVerb} ${prepositionedInstrumentNoun} ${adjective} ${pronoun} ${directPreposition} ${adjective} ${directNoun} ${adverbEnd}.',
          templateWithIndirects: '${adverbBegin} ${actorNoun} ${tryVerb} ${adverbMiddle} ${skillVerb} ${prepositionedInstrumentNoun} ${adjective} ${pronoun} ${preposition} ${directPreposition} ${adjective} ${directNoun} ${adverbEnd} ${prepositionedIndirects}.',
          templateWithIndirectsActorIsDirect: '${adverbBegin} ${actorNoun} ${tryVerb} ${adverbMiddle} ${skillVerb} ${prepositionedInstrumentNoun} ${adverbEnd} ${pronoun} ${prepositionedIndirects}.',
          templatePrepositionedIndirect: '${preposition} ${adjective} ${indirect}',
          templatePrepositionedInstrument: '${preposition} ${indirect}',
          indirectsSeparator: ', ',
          indirectsConjunction: ' and ',
          tryVerbId: dict.Try.ID,
          pronounCase: lang.Case.accusative,
        }},
        { id: DefaultResDict.SKILL_ACCEPTED, value: {
          templateSimple: '${adverbBegin} ${actorNoun} ${adverbMiddle} ${skillVerb} ${prepositionedInstrumentNoun} ${adjective} ${pronoun} ${directPreposition} ${adjective} ${directNoun} ${adverbEnd}.',
          templateWithIndirects: '${adverbBegin} ${actorNoun} ${adverbMiddle} ${skillVerb} ${prepositionedInstrumentNoun} ${adjective} ${pronoun} ${directPreposition} ${adjective} ${directNoun} ${adverbEnd} ${prepositionedIndirects}.',
          templateWithIndirectsActorIsDirect: '${adverbBegin} ${actorNoun} ${adverbMiddle} ${skillVerb} ${prepositionedInstrumentNoun} ${adverbEnd} ${pronoun} ${prepositionedIndirects}.',
          templatePrepositionedIndirect: '${preposition} ${adjective} ${indirect}',
          templatePrepositionedInstrument: '${preposition} ${indirect}',
          indirectsSeparator: ', ',
          indirectsConjunction: ' and ',
          pronounCase: lang.Case.accusative,
        }},
        { id: DefaultResDict.SCENE_ENTERED, value: {
          template: '${noun} ${verb} the ${sceneNoun}.',
          verbId: dict.Enter.ID,
        }},
        { id: DefaultResDict.SCENE_LEFT, value: {
          template: '${noun} ${verb} the ${sceneNoun}.',
          verbId: dict.Leave.ID,
        }},
      ],
    });
  }
}
