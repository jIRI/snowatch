import {FrameworkConfiguration} from 'aurelia-framework';

import {ResourceDictionary} from 'snowatch/resource-dictionary';
import {DefaultResDict} from 'snowatch/resources/default';
import * as lang from 'snowatch/language-structures';
import * as dict from 'snowatch/resources/dictionary';

export function configure(aurelia: FrameworkConfiguration) {
  aurelia.singleton(lang.LanguageStructure, dict.ToBe);
  aurelia.singleton(lang.LanguageStructure, dict.NotToBe);
  aurelia.singleton(lang.LanguageStructure, dict.Give);
  aurelia.singleton(lang.LanguageStructure, dict.Take);
  aurelia.singleton(lang.LanguageStructure, dict.Put);
  aurelia.singleton(lang.LanguageStructure, dict.Move);
  aurelia.singleton(lang.LanguageStructure, dict.Close);
  aurelia.singleton(lang.LanguageStructure, dict.Open);
  aurelia.singleton(lang.LanguageStructure, dict.Lock);
  aurelia.singleton(lang.LanguageStructure, dict.Say);
  aurelia.singleton(lang.LanguageStructure, dict.Learn);
  aurelia.singleton(lang.LanguageStructure, dict.Teach);
  aurelia.singleton(lang.LanguageStructure, dict.Unlock);
  aurelia.singleton(lang.LanguageStructure, dict.Press);
  aurelia.singleton(lang.LanguageStructure, dict.Drink);
  aurelia.singleton(lang.LanguageStructure, dict.DrinkUp);
  aurelia.singleton(lang.LanguageStructure, dict.Pour);
  aurelia.singleton(lang.LanguageStructure, dict.Try);
  aurelia.singleton(lang.LanguageStructure, dict.SmellVerb);
  aurelia.singleton(lang.LanguageStructure, dict.Enter);
  aurelia.singleton(lang.LanguageStructure, dict.Leave);
  aurelia.singleton(lang.LanguageStructure, dict.Engage);
  aurelia.singleton(lang.LanguageStructure, dict.Attack);
  aurelia.singleton(lang.LanguageStructure, dict.Block);
  aurelia.singleton(lang.LanguageStructure, dict.Dodge);
  aurelia.singleton(lang.LanguageStructure, dict.Evade);

  aurelia.singleton(lang.LanguageStructure, dict.Ground);
  aurelia.singleton(lang.LanguageStructure, dict.SmellNoun);

  aurelia.singleton(lang.LanguageStructure, dict.Neutrally);
  aurelia.singleton(lang.LanguageStructure, dict.Quickly);
  aurelia.singleton(lang.LanguageStructure, dict.Slowly);
  aurelia.singleton(lang.LanguageStructure, dict.Quietly);
  aurelia.singleton(lang.LanguageStructure, dict.Whisperingly);
  aurelia.singleton(lang.LanguageStructure, dict.Hesitantly);
  aurelia.singleton(lang.LanguageStructure, dict.Cautiously);
  aurelia.singleton(lang.LanguageStructure, dict.Loudly);
  aurelia.singleton(lang.LanguageStructure, dict.Angrily);
  aurelia.singleton(lang.LanguageStructure, dict.Patiently);
  aurelia.singleton(lang.LanguageStructure, dict.To);
  aurelia.singleton(lang.LanguageStructure, dict.DoActionWith);
  aurelia.singleton(lang.LanguageStructure, dict.AccusativeOn);
  aurelia.singleton(lang.LanguageStructure, dict.EngageSomeone);
  aurelia.singleton(lang.LanguageStructure, dict.EvadeSomeone);

  aurelia.singleton(lang.LanguageStructure, dict.BitOfIt);
  aurelia.singleton(lang.LanguageStructure, dict.SomeOfIt);
  aurelia.singleton(lang.LanguageStructure, dict.HalfOfIt);
  aurelia.singleton(lang.LanguageStructure, dict.AllOfIt);

  aurelia.singleton(lang.LanguageStructure, dict.SpatialOn);
  aurelia.singleton(lang.LanguageStructure, dict.SpatialUnder);
  aurelia.singleton(lang.LanguageStructure, dict.SpatialBelow);
  aurelia.singleton(lang.LanguageStructure, dict.SpatialIn);
  aurelia.singleton(lang.LanguageStructure, dict.SpatialFront);
  aurelia.singleton(lang.LanguageStructure, dict.SpatialBehind);
  aurelia.singleton(lang.LanguageStructure, dict.SpatialBack);
  aurelia.singleton(lang.LanguageStructure, dict.SpatialLeft);
  aurelia.singleton(lang.LanguageStructure, dict.SpatialRight);
  aurelia.singleton(lang.LanguageStructure, dict.And);
  aurelia.singleton(lang.LanguageStructure, dict.Big);

  aurelia.singleton(lang.LanguageStructure, dict.Stunned);

  aurelia.singleton(lang.LanguageStructure, dict.InstrumentWith);

  aurelia.singleton(ResourceDictionary, DefaultResDict);
}
