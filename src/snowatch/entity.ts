import {GameData} from 'snowatch/game-data';
import {Message, Topic} from 'snowatch/message';
import {Topics} from 'snowatch/message-topics/all';
import {LogManager} from 'aurelia-framework';
import {BehaviorInit} from 'snowatch/behavior';
import {SpatialRelationInit} from 'snowatch/spatial-relations';
import {ResourceDictionary} from 'snowatch/resource-dictionary';
import {DefaultResDict} from 'snowatch/resources/default';
import { getTypeId } from 'snowatch/utils';
import * as lang from 'snowatch/language-structures';
import * as attrs from 'snowatch/attributes-ids';

export interface Disposable {
    dispose(): void;
}

export interface HasId {
  id: string;
  instanceId?: string;
}

export interface PropertiesInit {
  constAttributes?: Array<attrs.ConstAttributeInit>;
  compAttributes?: Array<attrs.ComputedAttributeInit>;
  attributes?: Array<attrs.AttributeInit>;
  behaviors?: Array<BehaviorInit>;
  spatialRelations?: Array<SpatialRelationInit>;
}

export interface EntityInit {
  [index: string]: any;
  id?: string;
  instanceId?: string;
  properties?: PropertiesInit;
  title?: string;
  holderId?: string | null;
  ownerId?: string | null;
}

export interface InitializerFn {
  (entity: Entity, ...args: any[]): void;
}

export function notNull(object: StatelessEntity | Entity | undefined): object is StatelessEntity | Entity {
  return object != null;
}

export function entityNotNull(object: Entity | undefined): object is Entity {
   return object != null;
}

export function statelessEntityNotNull(object: StatelessEntity | undefined): object is StatelessEntity {
   return object != null;
}

export function getEntityId(baseId: string, instanceId: string | undefined): string  {
  if( instanceId != null ) {
    return `${baseId}.${instanceId}`;
  } else {
    return baseId;
  }
}

export function getClassFromInstanceId(instanceId: string): string  {
  return instanceId.split('.')[0];
}

export function getIsVisible(entity: Entity, actor?: Entity): boolean {
  if( attrs.Attribute.applies(entity, attrs.IsVisible, actor) ) {
    return attrs.Attribute.value<boolean>(entity, attrs.IsVisible, actor);
  }

  // not visible by default
  return false;
}

export function getCanBeInstrument(entity: StatelessEntity, actor?: Entity): boolean {
  if( attrs.Attribute.applies(entity, attrs.CanBeInstrument, actor) ) {
    return attrs.Attribute.value<boolean>(entity, attrs.CanBeInstrument, actor);
  }

  // not visible by default
  return false;
}

export function getCanBeIndirect(entity: Entity, actor?: Entity): boolean {
  if( attrs.Attribute.applies(entity, attrs.CanBeIndirect, actor) ) {
    return attrs.Attribute.value<boolean>(entity, attrs.CanBeIndirect, actor);
  }

  // not visible by default
  return false;
}

export function getProperties(target: StatelessEntity, entityGroup: string, id: string): any {
  if( target.properties[entityGroup] == null ) {
    target.properties[entityGroup] = new Map<string, HasId>();
  }

  let props = target.properties[entityGroup].get(id);
  if( props == null ) {
    props = {};
    target.properties[entityGroup].set(id, props);
  }
  return props;
}

export function getState(target: Entity, entityGroup: string, id: string): any {
  return target != null && target.state != null ? target.state[entityGroup].get(id) : null;
}

export function getConst(target: StatelessEntity, entityGroup: string, id: string): any {
  return target != null && target.consts != null ? target.consts[entityGroup].get(id) : null;
}

export function getComputed(target: StatelessEntity, entityGroup: string, id: string): any {
  return target != null && target.computed != null ? target.computed[entityGroup].get(id) : null;
}

export function cloneGroup(objectGroup: string, source: Entity | StatelessEntity, target: Entity | StatelessEntity) {
  for(let [id, val] of source.consts[objectGroup]) {
    target.consts[objectGroup].set(id, val);
  }

  for(let [id, val] of source.computed[objectGroup]) {
    target.computed[objectGroup].set(id, val);
  }

  if(source instanceof Entity && target instanceof Entity) {
    for(let [id, val] of source.state[objectGroup]) {
      target.state[objectGroup].set(id, Object.assign({}, val));
    }
  }
}

export function updateInstanceIds<T extends HasId>(objects: T[], instanceId: string): Array<T> {
  objects.forEach(o => o.instanceId = instanceId);
  return objects;
}

export function getTitle(entity: StatelessEntity, caseId: lang.Case = lang.Case.nominative, capitalizationOptions: lang.CapitalizationOptions = lang.CapitalizationOptions.capitalize, actor?: Entity): lang.EntityTitleData {
  const resource = ResourceDictionary.get(entity.game, DefaultResDict.NOUN);
  const adjectiveId = (attrs.Attribute.applies(entity, attrs.AdjectiveResourceId)) ? ResourceDictionary.get(entity.game, attrs.Attribute.value<string>(entity, attrs.AdjectiveResourceId, actor)) : null;
  const nounId = (attrs.Attribute.applies(entity, attrs.NounResourceId)) ? ResourceDictionary.get(entity.game, attrs.Attribute.value<string>(entity, attrs.NounResourceId, actor)) : null;

  return lang.getTitleData(entity.game, resource.template, nounId, adjectiveId, caseId, capitalizationOptions);
}

export function isHeldBy(entity: Entity, actor: Entity): boolean {
  const holderId = getHolderId(entity);
  return holderId != null && holderId === actor.id;
}

export function isHeldByOrFree(entity: Entity, actor: Entity): boolean {
  const holderId = getHolderId(entity);
  return holderId == null || holderId === actor.id;
}

export function getHolderId(entity: Entity): string | null {
  if( attrs.Attribute.applies(entity, attrs.HolderId) ) {
    return attrs.Attribute.value<string>(entity, attrs.HolderId);
  } else {
    return null;
  }
}

export function setHolderId(entity: Entity, value: string | null | undefined) {
  if( attrs.Attribute.applies(entity, attrs.HolderId) ) {
    attrs.Attribute.setValue<string>(entity, attrs.HolderId, value !== undefined ? value : null);
  }
}

export function getOwnerId(entity: Entity): string | null {
  if( attrs.Attribute.applies(entity, attrs.OwnerId) ) {
    return attrs.Attribute.value<string>(entity, attrs.OwnerId);
  } else {
    return null;
  }
}

export function setOwnerId(entity: Entity, value: string | null | undefined) {
  if( attrs.Attribute.applies(entity, attrs.OwnerId) ) {
    attrs.Attribute.setValue<string>(entity, attrs.OwnerId, value !== undefined ? value : null);
  }
}

export function registerEntities(game: GameData, entityGroup: string, objects: StatelessEntity | EntityInit | Array<Entity | EntityInit>, registrator: Function) {
  if( null ==  objects) {
    return null;
  }

  if( Array.isArray(objects) ) {
    return objects
      .filter(o => o != null && !game.instances[entityGroup].has(o.id || o))
      .map(o => registrator(game, o));
  } else {
    return registrator(game, objects);
  }
}

export function deregisterEntities(game: GameData, entityGroup: string, objects: StatelessEntity | Array<Entity>) {
  if( null ==  objects) {
    return;
  }

  if( Array.isArray(objects) ) {
    const deleteList = new Array<any>();
    // colect deletable entities
    objects
      .filter(o => o != null && game.instances[entityGroup].has(o.id))
      .forEach(o => deleteList.push(o));

    deleteList.forEach(o => {
      deleteEntity(game, entityGroup, o);
    });
    return deleteList;
  } else {
    deleteEntity(game, entityGroup, objects);
  }
}

function deleteEntity(game: GameData, entityGroup: string, object: StatelessEntity) {
  const entityId = object.id;
  object.logger.debug(`Deleting entity ${entityId}`);
  game.instances[entityGroup].delete(entityId);
  game.state[entityGroup].delete(entityId);
  object.logger.debug(`Entity ${entityId} deleted`);
}

export function processProperties(properties: any) {
  // translate properties to maps (where applicable)
  const processedProperties: any = {};
  if( properties != null ) {
    for(let pname of Object.getOwnPropertyNames(properties)) {
      // arrays need special handling to convert attribs, skills etc. to maps
      if(Array.isArray(properties[pname])) {
        let arrayItemCnt = 0;
        processedProperties[pname] = new Map<string, HasId>(properties[pname].map((obj: HasId) => {
          const objId = obj.id ?? `id.${pname}.${arrayItemCnt++}`;
          return [/* key: */ objId, /* value: */ obj];
        }));
      } else {
        // plain attrib - keep as is
        processedProperties[pname] = properties[pname];
      }
    }
  }

  return processedProperties;
}

export class StatelessEntity implements Disposable {
  private _initializers: Array<InitializerFn> = [];
  private _onStartInitializers: Array<InitializerFn> = [];
  protected _baseId: string;
  entityGroup: string;
  instanceId: string | undefined;
  init: EntityInit;
  properties: any;
  consts: any = {};
  computed: any = {};
  game: GameData;
  logger: any;

  constructor(init: EntityInit | null, entityGroup: string = 'objects') {
    this.init = init ?? { };
    this.entityGroup = entityGroup;
  }

  initialize(game: GameData, init?: EntityInit | null, entityGroup?: string): StatelessEntity {
    this.game = game;
    if( init != null ) {
      this.init = init;
    }

    if( entityGroup != null ) {
      this.entityGroup = entityGroup;
    }

    this._baseId = this.init.id ?? getTypeId(this);
    this.instanceId = this.instanceId ?? this.init.instanceId;
    this.properties = processProperties(this.init.properties);
    this.logger = LogManager.getLogger(this.id);
    this.game.instances[this.entityGroup].set(this.id, this);

    this.game.bus.system.subscribe({
      filter: (m) =>  {
        return m.topicId === Topic.id(Topics.START);
      },
      handler: (m) =>  {
        return this.onStart(m);
      }
    });
    this.game.bus.system.subscribe({
      filter: (m) =>  {
        return m.topicId === Topic.id(Topics.INITIALIZE_COMPONENTS);
      },
      handler: (m) =>  {
        this.onInitializeComponents(m);
      }
    });

    return this;
  }

  dispose() {
    this.logger.debug(`${this.id}: dispose() - instance`);
    this.game.instances[this.entityGroup].delete(this.id);
  }

  get id(): string {
    return getEntityId(this._baseId, this.instanceId);
  }

  get baseId(): string {
    return this._baseId;
  }

  registerInitializer(initializer: InitializerFn) {
    this._initializers = this._initializers ?? [];
    this._initializers.push(initializer);
  }

  initializeComponents(...args: any[]) {
    this.logger.debug(`${this.id}: initializeComponents()`);

    this._initializers.forEach(i => {
      i.call(this.game, this, ...args);
    });
    this._initializers = [];
  }

  onInitializeComponents(...args: any[]) {
    this.logger.debug(`${this.id}: onInitializeComponents()`);
    this.initializeComponents(...args);
  }

  registerOnStartInitializer(initializer: InitializerFn) {
    this._onStartInitializers = this._onStartInitializers ?? [];
    this._onStartInitializers.push(initializer);
  }

  onStartInitialize(...args: any[]) {
    this.logger.debug(`${this.id}: onStartInitialize()`);

    this._onStartInitializers.forEach(i => {
      i.call(this, ...args);
    });
    this._onStartInitializers = [];
  }

  onStart(message: Message, ...args: any[]) {
    this.logger.debug(`${this.id}: onStart()`);
    this.onStartInitialize(message, ...args);
  }
}

export class Entity extends StatelessEntity {
  static ensureIsInstanceOf(obj: Entity | EntityInit): Entity {
   if( obj instanceof Entity ) {
     return obj;
   }
   return new Entity(obj);
  }

  public static OBJECT_GROUP = 'entities';
  static registrator(game: GameData, o: Entity | EntityInit) {
    const obj = Entity.ensureIsInstanceOf(o);
    return obj.initialize(game);
  }

  constructor(init: EntityInit | null = null, entityGroup = Entity.OBJECT_GROUP) {
    super(init, entityGroup);
  }

  initialize(game: GameData, init?: EntityInit, entityGroup?: string): Entity {
    super.initialize(game, init, entityGroup);
    this.game.state[this.entityGroup].set(this.id, this.properties.state ?? {});

    this.state.id = this._baseId;
    this.state.instanceId = this.instanceId;

    // copy all methods from init to this
    if( init != null ) {
      Object.keys(init).filter(k => init[k] instanceof Function).forEach(k => (<any>this)[k] = init[k]);
    }

    return this;
  }

  dispose() {
    this.logger.debug(`${this.id}: dispose() - state`);
    this.game.state[this.entityGroup].delete(this.id);
    super.dispose();
  }

  get state() {
    return this.game.state[this.entityGroup].get(this.id);
  }
}
