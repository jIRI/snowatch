import {GameData} from 'snowatch/game-data';
import {broadcast} from 'snowatch/message';
import {Topics} from 'snowatch/message-topics/all';
import {cloneIfObject} from 'snowatch/utils';
import * as result from 'snowatch/result';
import * as ent from 'snowatch/entity';

export interface AttributeProperties {
  attributes: Map<string, AttributeInit>;
}

export interface AttributeInit extends ent.HasId, AttributeStateInit {
}

export interface AttributeStateInit {
  initialState?: AttributeInitialStateInit;
}

export interface AttributeInitialStateInit {
  value?: any;
  applicability?: boolean;
}

export interface ConstAttributeProperties {
  constAttributes: Map<string, ConstAttributeInit>;
}

export interface ConstAttributeInit extends ent.HasId, ConstAttributeStateInit {
}

export interface ConstAttributeStateInit {
  constState?: ConstAttributeConstStateInit;
}

export interface ConstAttributeConstStateInit {
  value?: any;
  applicability?: boolean;
}

export interface ComputedAttributeProperties {
  compAttributes: Map<string, ComputedAttributeInit>;
}

export interface ComputedAttributeInit extends ent.HasId, ComputedAttributeStateInit {
}

export interface ComputedAttributeStateInit {
  compState?: ComputedAttributeComputedStateInit;
}

export abstract class ComputedAttribute {
  abstract value(entity: ent.StatelessEntity, attributeId: string, actor?: ent.Entity): any;
  abstract applicability(entity: ent.StatelessEntity, attributeId: string, actor?: ent.Entity): boolean;
}

export interface ComputedAttributeComputedStateInit {
  value?: (entity: ent.StatelessEntity, attributeId: string, actor?: ent.Entity) => any;
  applicability?: (entity: ent.StatelessEntity, attributeId: string, actor?: ent.Entity) => boolean;
}

export class Attribute {
  public static OBJECT_GROUP = 'attributes';

  static attrRegistry = new Map<string, any>();

  static register<T>(id: string, defaultValue: T | { value: T}) {
    if( !Attribute.attrRegistry.has(id) ) {
      Attribute.attrRegistry.set(id, defaultValue);
    } else {
      throw Error(`Registering existing attribute: '${id}'.`);
    }
  }

  static defaultValue(id: string): any {
    if( Attribute.attrRegistry.has(id) ) {
      return Attribute.attrRegistry.get(id).value;
    } else {
      return undefined;
    }
  }

  static initializeAttributes(target: ent.Entity | ent.StatelessEntity) {
    target.logger.debug(`${target.id}: initializeAttributes()`);
    if( (<ent.Entity>target).state != null ) {
      (<ent.Entity>target).state[Attribute.OBJECT_GROUP] = (<ent.Entity>target).state[Attribute.OBJECT_GROUP] || new Map<string, any>();
    }
    target.consts[Attribute.OBJECT_GROUP] = target.consts[Attribute.OBJECT_GROUP] || new Map<string, any>();
    target.computed[Attribute.OBJECT_GROUP] = target.computed[Attribute.OBJECT_GROUP] || new Map<string, any>();
  }

  static valueIfApplies<T>(entity: ent.Entity | ent.StatelessEntity, attributeId: string, defaultValue: T, actor?: ent.Entity): T {
    if( Attribute.applies(entity, attributeId, actor) !== true) {
      return defaultValue;
    }

    return Attribute.value(entity, attributeId, actor);
  }

  static value<T>(entity: ent.Entity | ent.StatelessEntity, attributeId: string, actor?: ent.Entity): T {
    let value: T | undefined;
    if( Attribute.hasVal(<ent.Entity>entity, attributeId) ) {
      value = cloneIfObject(ent.getState(<ent.Entity>entity, Attribute.OBJECT_GROUP, attributeId).value);
    } else if( Attribute.hasConst(entity, attributeId) ) {
      value = ent.getConst(entity, Attribute.OBJECT_GROUP, attributeId).value;
    } else if( Attribute.hasComp(entity, attributeId) ) {
      // NOTE: here we use plain JS function, we can't have this in any of the comp attrs getters
      const valueFunction = ent.getComputed(entity, Attribute.OBJECT_GROUP, attributeId).value;
      value = (valueFunction != null) ? valueFunction(entity, attributeId, actor) : undefined;
    }

    if(value !== undefined ) {
      return value;
    } else {
      return Attribute.defaultValue(attributeId);
    }
  }

  static setValue<T>(entity: ent.Entity, attributeId: string, value: T | null) {
    if( Attribute.hasVal(entity, attributeId) ) {
      const attrib = ent.getState(entity, Attribute.OBJECT_GROUP, attributeId);
      const oldValue = attrib.value;
      if( value != null ) {
        attrib.value = cloneIfObject(value);
      } else {
        attrib.value = Attribute.defaultValue(attributeId);
      }
      if( oldValue !== attrib.value ) {
        entity.game.bus.notifications.publish(
          broadcast(Topics.ATTRIBUTE_VALUE_CHANGED, {
              entityId: entity.id,
              attributeId: attributeId,
              oldValue: oldValue,
              newValue: attrib.value
            },
            undefined,
            entity.id
          )
        );
      }
    }
  }

  static ifAttributesThenSet<U, V>(checkAttrs: Array<{entity: ent.Entity, withAttributeId: string, valueMustBe?: U | null}>, setAttrs: Array<{entity: ent.Entity, withAttributeId: string, setValueTo: V | null, onlyIfApplies?: boolean}>, actor?: ent.Entity): boolean {
    if( checkAttrs.every(attr => Attribute.applies(attr.entity, attr.withAttributeId) && (attr.valueMustBe === undefined || Attribute.value(attr.entity, attr.withAttributeId, actor) === attr.valueMustBe)) ) {
      setAttrs.forEach(attr => {
        if( attr.onlyIfApplies == null || Attribute.applies(attr.entity, attr.withAttributeId) ) {
          Attribute.setValue(attr.entity, attr.withAttributeId, attr.setValueTo);
        }
      });
      return true;
    }

    return false;
  }

  static applies(entity: ent.Entity | ent.StatelessEntity, attributeId: string, actor?: ent.Entity): boolean {
    if( Attribute.hasVal(<ent.Entity>entity, attributeId) ) {
      const applicability = ent.getState(<ent.Entity>entity, Attribute.OBJECT_GROUP, attributeId).applicability;
      return (applicability !== undefined) ? applicability : true;
    } else if( Attribute.hasConst(entity, attributeId) ) {
      const applicability = ent.getConst(entity, Attribute.OBJECT_GROUP, attributeId).applicability;
      return (applicability !== undefined) ? applicability : true;
    } else if( Attribute.hasComp(entity, attributeId) ) {
      const applicability = ent.getComputed(entity, Attribute.OBJECT_GROUP, attributeId).applicability;
      return (applicability != null) ? applicability(entity.game, entity, attributeId, actor) : true;
    } else {
      return false;
    }
  }

  static setApplicability(entity: ent.Entity, attributeId: string, applicability: boolean) {
    if( Attribute.hasVal(entity, attributeId) ) {
      const attrib = ent.getState(entity, Attribute.OBJECT_GROUP, attributeId);
      const oldApplicability = attrib.applicability;
      attrib.applicability = applicability;
      if( oldApplicability !== attrib.applicability ) {
        entity.game.bus.notifications.publish(
          broadcast(Topics.ATTRIBUTE_APPLICABILITY_CHANGED, {
            entityId: entity.id,
            attributeId: attributeId,
            oldValue: oldApplicability,
            newValue: attrib.applicability
          },
          undefined,
          entity.id
        )
        );
      }
    }
  }

  static add(entity: ent.Entity, attributeId: string, attr?: AttributeStateInit) {
    if( !Attribute.has(entity, attributeId) ) {
      entity.logger.debug(`Adding attribute ${attributeId} (${attr})`);
      const initialState = (attr != null) ? (attr.initialState || {}) : {};
      entity.state[Attribute.OBJECT_GROUP].set(attributeId, initialState);
      entity.game.bus.notifications.publish(broadcast(Topics.ATTRIBUTE_ADDED, { entityId: entity.id, attributeId: attributeId, attributeValue: initialState.value }, undefined, entity.id));
    }
  }

  static addFromProperties(entity: ent.Entity, properties: AttributeProperties) {
    if( properties != null && properties.attributes != null ) {
      for(let [attributeId, attributeInit] of properties.attributes) {
        Attribute.add(entity, attributeId, attributeInit);
      }
    }
  }

  static addConst(entity: ent.StatelessEntity, attributeId: string, attr?: ConstAttributeStateInit) {
    if( !Attribute.hasConst(entity, attributeId) && !Attribute.hasComp(entity, attributeId) ) {
      entity.logger.debug(`Adding const attribute ${attributeId} (${attr})`);
      const constState = (attr != null) ? (attr.constState || {}) : {};
      entity.consts[Attribute.OBJECT_GROUP].set(attributeId, constState);
      entity.game.bus.notifications.publish(broadcast(Topics.ATTRIBUTE_ADDED, { entityId: entity.id, attributeId: attributeId, attributeValue: constState.value }, undefined, entity.id));
    }
  }

  static addConstFromProperties(entity: ent.StatelessEntity, properties: ConstAttributeProperties) {
    if( properties != null && properties.constAttributes != null ) {
      for(let [attributeId, attributeInit] of properties.constAttributes) {
        Attribute.addConst(entity, attributeId, attributeInit);
      }
    }
  }

  static addComp(entity: ent.StatelessEntity, attributeId: string, attr?: ComputedAttributeStateInit) {
    if( !Attribute.hasConst(entity, attributeId) && !Attribute.hasComp(entity, attributeId) ) {
      entity.logger.debug(`Adding computed attribute ${attributeId} (${attr})`);
      const compState = (attr != null) ? (attr.compState || {}) : {};
      entity.computed[Attribute.OBJECT_GROUP].set(attributeId, compState);
      entity.game.bus.notifications.publish(broadcast(Topics.ATTRIBUTE_ADDED, { entityId: entity.id, attributeId: attributeId, computed: true }, undefined, entity.id));
    }
  }

  static addCompFromProperties(entity: ent.StatelessEntity, properties: ComputedAttributeProperties) {
    if( properties != null && properties.compAttributes != null ) {
      for(let [attributeId, attributeInit] of properties.compAttributes) {
        Attribute.addComp(entity, attributeId, attributeInit);
      }
    }
  }

  static remove(entity: ent.Entity, attributeId: string) {
    entity.logger.debug(`Removing attribute ${attributeId}`);
    entity.state.attributes.delete(attributeId);
    entity.game.bus.notifications.publish(broadcast(Topics.ATTRIBUTE_REMOVED, { entityId: entity.id, attributeId: attributeId }, undefined, entity.id));
  }

  static removeAll(entity: ent.Entity) {
    entity.logger.debug(`Clearing all attributes`);
    const attrs = entity.state[Attribute.OBJECT_GROUP];
    if( attrs != null ) {
      for( const attrId of attrs.keys() ) {
        Attribute.remove(entity, attrId);
      }
    }
  }

  static has(entity: ent.Entity, attributeId: string) {
    return Attribute.hasVal(entity, attributeId)
          || Attribute.hasConst(entity, attributeId)
          || Attribute.hasComp(entity, attributeId);
  }

  static hasVal(entity: ent.Entity, attributeId: string) {
    if( entity == null || entity.state == null ) {
      return false;
    }

    const attrs = entity.state[Attribute.OBJECT_GROUP];
    return attrs != null && attrs.has(attributeId);
  }

  static hasConst(entity: ent.StatelessEntity, attributeId: string) {
    if( entity == null || entity.consts == null ) {
      return false;
    }

    const attrs = entity.consts[Attribute.OBJECT_GROUP];
    return attrs != null && attrs.has(attributeId);
  }

  static hasComp(entity: ent.StatelessEntity, attributeId: string) {
    if( entity == null || entity.computed == null ) {
      return false;
    }

    const attrs = entity.computed[Attribute.OBJECT_GROUP];
    return attrs != null && attrs.has(attributeId);
  }
}
