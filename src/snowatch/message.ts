import { Newable } from 'snowatch/base-types';
import * as topics from 'snowatch/message-topics/all';

export * from 'snowatch/message-topic';

class _private {
  public static messageId = 0;
}

export type PayloadType = topics.Topics.ANY | any;

export interface Message {
    readonly id: number;
    readonly sourceId: string | null;
    readonly topicId: string;
    readonly payload?: PayloadType;
    readonly targetId?: string;
    readonly responseTo?: number;
    readonly realmIds?: Array<string>;
    timestamp?: number;
    shouldBeAddedToDebugLog?: boolean;
    publishTimestamp?: number;
    allowAsync?: boolean;
}

export function payload<T>(message: Message): T {
  return message.payload as T;
}

export function preprocess(message: Message, handler: Function): Message {
  handler(message);
  return message;
}

export function broadcast<T>(ctor: Newable<T>, payload?: Readonly<T>, responseTo?: number, sourceId: string | null = null, realmIds?: Array<string>): Message {
  return {
    id: _private.messageId++,
    sourceId: sourceId,
    responseTo: responseTo,
    payload: new ctor(payload),
    topicId: (ctor as any).ID ?? ctor.name,
    realmIds: realmIds,
  };
}

export function message<T>(ctor: Newable<T>, target: string, payload?: Readonly<T>, responseTo?: number, sourceId: string | null = null): Message {
  return {
    id: _private.messageId++,
    sourceId: sourceId,
    responseTo: responseTo,
    targetId: target,
    topicId: (ctor as any).ID ?? ctor.name,
    payload: payload,
  };
}

export function broadcastId(id: string, payload?: PayloadType, responseTo?: number, sourceId: string | null = null): Message {
  return {
    id: _private.messageId++,
    sourceId: sourceId,
    responseTo: responseTo,
    payload: { ...payload },
    topicId: id,
  };
}

export function messageId(id: string, target: string, payload?: PayloadType, responseTo?: number, sourceId: string | null = null): Message {
  return {
    id: _private.messageId++,
    sourceId: sourceId,
    targetId: target,
    responseTo: responseTo,
    topicId: id,
    payload: { ...payload },
  };
}

export function checkPayloadTemplate(template: any, payload: PayloadType): boolean {
  if( template == null ) {
    return true;
  }

  for(let templateProp in Object.getOwnPropertyNames(template)) {
    if( template[templateProp] !== payload[templateProp]) {
      return false;
    }
  }

  return true;
}
