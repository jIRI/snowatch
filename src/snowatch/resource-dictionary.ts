import {GameData} from 'snowatch/game-data';
import {HasId} from 'snowatch/entity';

import {LogManager} from 'aurelia-framework';
const logger = LogManager.getLogger('snowatch-resources');

export interface ResourceDictionaryItemInit extends HasId {
  value: ResourceDisctionaryItemValue | string | any;
}

export interface ResourceDisctionaryItemValue {
  template?: string;
}

export interface ContentsDescriptionRange {
  from: number;
  to: number;
  description: string;
}

export interface HasContentsDescription {
  contentsDescriptions: Array<ContentsDescriptionRange>;
}

export interface ResourceDictionaryInit {
  culture: string;
  resources: Array<ResourceDictionaryItemInit>;
}

export class ResourceDictionary {
  public static OBJECT_GROUP = 'resources';
  public static readonly DEFAULT_CULTURE = 'en';

  static registrator(game: GameData, obj: ResourceDictionary | ResourceDictionaryInit): ResourceDictionary {
    let resDict: ResourceDictionary | null = null;
    if( obj instanceof ResourceDictionary ) {
      resDict = obj;
    } else {
      resDict = new ResourceDictionary(obj);
    }

    ResourceDictionary.load(game, resDict);

    return resDict;
  }

  public static load(game: GameData, resDict: ResourceDictionary) {
    logger.debug(`loading resource dictionary '${resDict.constructor.name}'`);
    for(let res of resDict.resources) {
      game.resources.set(ResourceDictionary.getFullResourceId(resDict.culture, res.id), res.value);
    }
  }

  public static unload(game: GameData, resDict: ResourceDictionary) {
    logger.debug(`unloading resource dictionary '${resDict.constructor.name}'`);
    for(let res of resDict.resources) {
      game.resources.delete(ResourceDictionary.getFullResourceId(resDict.culture, res.id));
    }
  }

  public static get(game: GameData, id: string, culture?: string): any {
    const currentCulture = culture ?? game.state.settings.currentCulture ?? ResourceDictionary.DEFAULT_CULTURE;
    let resource = game.resources.get(ResourceDictionary.getFullResourceId(currentCulture, id));
    if( resource == null ) {
      resource = game.resources.get(ResourceDictionary.getFullResourceId(ResourceDictionary.DEFAULT_CULTURE, id));
    }
    return resource;
  }

  public static getFullResourceId(culture: string, id: string): string {
    return `${culture}.${id}`;
  }

  public static getContentsDescription(game: GameData, id: string, value: number): string {
    const resource = ResourceDictionary.get(game, id);
    if( resource == null ) {
      logger.warn(`Unable to find resource [${id}]`);
      return '';
    }

    if( resource.contentsDescriptions == null ) {
      logger.warn(`Unable to find contentsDescriptions in resource [${id}]`);
      return '';
    }

    for(let contentsRange of (resource as HasContentsDescription).contentsDescriptions) {
      if( value >= contentsRange.from  && value <= contentsRange.to ) {
        return contentsRange.description;
      }
    }

    logger.warn(`Unable to find appropriate contents range for value [${value}] in resource [${id}]`);
    return '';
  }

  public culture: string;
  public resources: Array<ResourceDictionaryItemInit>;

  constructor(init: ResourceDictionaryInit) {
    this.culture = init.culture;
    this.resources = init.resources;
  }
}