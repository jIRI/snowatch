import {bindable, inject} from 'aurelia-framework';

import {GameController} from 'snowatch/game-controller';
import * as scene from 'snowatch/scene';
import * as vm from 'snowatch/viewmodels';

@inject(GameController)
export class ExitsCustomElement {
  @bindable exits: Array<vm.ExitViewModel>;

  private _controller: GameController;
  constructor(controller: GameController) {
    this._controller = controller;
  }

  tryExit(exit: scene.Exit) {
    this._controller.tryExit(exit);
  }
}
