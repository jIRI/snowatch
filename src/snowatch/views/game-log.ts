import {bindable} from 'aurelia-framework';

import {GameLogItem} from 'snowatch/game-data';

export class GameLogCustomElement {
  @bindable items: Array<GameLogItem>;
}
