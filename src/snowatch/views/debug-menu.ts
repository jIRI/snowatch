import {inject} from 'aurelia-framework';
import {InputLogItem, InputLogRecording} from 'snowatch/input-logger';
import {GameController} from 'snowatch/game-controller';
import { NewRecordingDialog } from 'snowatch/views/dialogs/new-recording-dialog';

@inject(
  GameController,
  NewRecordingDialog
)
export class DebugMenuCustomElement {
  private _controller: GameController;
  private _newRecordingDialog: NewRecordingDialog;

  constructor(
    controller: GameController,
    newRecordingDialog: NewRecordingDialog
  ) {
    this._controller = controller;
    this._newRecordingDialog = newRecordingDialog;
    window.onbeforeunload = () => {
      this._controller.autosaveForReload();
    };
  }

  get inputLoggerRecordings() {
    return this._controller.inputLoggerRecordings;
  }

  get selectedInputLoggerRecording() {
    return this._controller.selectedInputLoggerRecording;
  }

  get inputLoggerItems() {
    return this._controller.inputLoggerItems;
  }

  get selectedInputLoggerItem() {
    return this._controller.selectedInputLoggerItem;
  }

  dumpAll() {
    this._controller.dumpAll();
  }

  dumpState() {
    this._controller.dumpState();
  }

  resetState() {
    this._controller.resetState();
  }

  reload() {
    this._controller.reload();
  }

  saveState(slotName: string) {
    this._controller.saveState(slotName);
  }

  loadState(slotName: string) {
    this._controller.loadState(slotName);
  }

  break() {
    // well, we need this becauce for some reason vscode + chrome + webpack = no breakpoint in vscode until breakpoint in chrome, and i wasted enough time trying to figure that out...
    console.debug('Breaking...');
  }

  test() {
    this._controller.test();

  }

  clearTheLog() {
    this._controller.clearTheLog();
  }

  setSelectedInputLoggerItem(item: InputLogItem) {
    this._controller.setSelectedInputLoggerItem(item);
  }

  replayInputLog(clockScale: number) {
    this._controller.replayInputLog(clockScale);
  }

  setSelectedInputLoggerRecording(recording: InputLogRecording) {
    this._controller.setSelectedInputLoggerRecording(recording);
  }

  restoreInputLogState() {
    this._controller.restoreInputLogState();
  }

  trimInputLog() {
    this._controller.trimInputLog();
  }

  clearInputLog() {
    this._controller.clearInputLog();
  }

  startRecordingInput() {
    this._controller.startRecordingInput();
  }

  stopRecordingInput() {
    this._controller.stopRecordingInput();
  }

  newRecording() {
    this._newRecordingDialog.show(name => this._controller.addRecording(name));
  }

  deleteSelectedRecording() {
    this._controller.deleteSelectedRecording();
  }
}
