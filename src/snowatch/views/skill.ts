import {bindable} from 'aurelia-framework';

import * as ent from 'snowatch/entity';
import * as vm from 'snowatch/viewmodels';

export class SkillCustomElement {
  @bindable skill: vm.SkillViewModel;
  @bindable entity: ent.Entity;
}
