import {inject} from 'aurelia-framework';
import {DialogController} from 'aurelia-dialog';

export interface Recording {
  name: string;
}

@inject(DialogController)
export class NewRecordingViewModel {
  controller: DialogController;
  recording: Recording = { name: '' };

  constructor(controller: DialogController) {
    this.controller = controller;
  }

  activate(recording: Recording) {
    this.recording = recording;
  }
}
