import {inject} from 'aurelia-framework';
import {DialogService} from 'aurelia-dialog';
import { Recording, NewRecordingViewModel } from 'snowatch/views/dialogs/new-recording';


@inject(DialogService)
export class NewRecordingDialog {
  dialogService: DialogService;
  recording: Recording;

  constructor(dialogService: DialogService) {
    this.dialogService = dialogService;
  }

  async show(callback: (name: string) => void) {
    this.recording = { name: '' };
    const response = await this.dialogService.open({ viewModel: NewRecordingViewModel, model: this.recording, lock: true }).whenClosed(response => {
      if( !response.wasCancelled && response.output.name !== '') {
        callback(this.recording.name);
      }
    });
  }
}
