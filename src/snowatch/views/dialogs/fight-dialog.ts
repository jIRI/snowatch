import { inject } from 'aurelia-framework';
import { GameData } from 'snowatch/game-data';
import { DialogService } from 'aurelia-dialog';
import { Actor } from 'snowatch/actor';
import { Fight, FightTimer, FightViewModel, createFightTimer } from 'snowatch/views/dialogs/fight';
import { getViewModelRelationsSkills as entityToViewModelRelationsSkills } from 'snowatch/viewmodels';
import { Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { Entity, getTitle } from 'snowatch/entity';
import { TimeSource } from 'snowatch/time-source';
import { Attack, Block } from 'snowatch/skills/all';
import { capitalFirst } from 'snowatch/language-structures';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { DefaultResDict } from 'snowatch/resources/default';
import * as attrs from 'snowatch/attributes/all';
import * as lang from 'snowatch/language-structures';

import { LogManager } from 'aurelia-framework';
import { sleep } from 'snowatch/utils';

const logger = LogManager.getLogger('snowatch-game-controller');

@inject(
  GameData,
  DialogService
)
export class FightDialog {
  _dialogService: DialogService;
  _cache = new Map<string, any>();
  game: GameData;
  fight: Fight | null = null;

  constructor(game: GameData, dialogService: DialogService) {
    this.game = game;
    this._dialogService = dialogService;
    this.game.bus.notifications.subscribe({
      filter: (m) =>  {
        if( m.topicId !== Topic.id(Topics.ENGAGED) ) {
          return false;
        }
        return true;
      },
      handler: (m) =>  {
        const messagePayload = payload<Topics.ENGAGED>(m);
        let actor = this.game.instances.actors.get(messagePayload.actorId);
        let target = this.game.instances.actors.get(messagePayload.targetId);

        if( actor != null && this.fight?.targets.find(t => t.vm.entity.id === actor?.id) ) {
          return;
        }

        if( actor != null && !attrs.Attribute.value<boolean>(actor, attrs.IsCurrentActor) ) {
          const swap = actor;
          actor = target;
          target = swap;
        }

        if( actor != null && target != null ) {
          if( this.fight == null ) {
            this._cache.clear();
            this.show(actor, target);
          } else {
            this.fight.targets.push(entityToViewModelRelationsSkills(target, actor, this._cache, 'fight-cache'));
          }
        }
      },
      disposeOnReset: false,
    });
    this.game.bus.notifications.subscribe({
      filter: (m) =>  {
        if( this.fight == null || m.topicId !== Topic.id(Topics.EVADED) ) {
          return false;
        }
        return true;
      },
      handler: (m) =>  {
        if( this.fight != null ) {
          const messagePayload = payload<Topics.EVADED>(m);
          this.handleTargetRemoval(messagePayload.actorId);
        }
      },
      disposeOnReset: false,
    });
    this.game.bus.notifications.subscribe({
      filter: (m) =>  {
        if( this.fight == null || (
          m.topicId !== Topic.id(Topics.ATTACK_WINDING_UP)
          && m.topicId !== Topic.id(Topics.ATTACK_COOLING_DOWN)
          && m.topicId !== Topic.id(Topics.ATTACK_CANCELLING)
        )) {
          return false;
        }
        return true;
      },
      handler: async (m) =>  {
        if( this.fight != null ) {
          const messagePayload = payload<Topics.ATTACK_STAGE_CHANGING>(m);
          const timer = this.fight.timers.get(messagePayload.actorId);
          if( this.fight.timers.has(messagePayload.actorId) ) {
            timer!.actionDescription = capitalFirst(this.game.instances.skills.get(Attack.ID)?.verb.infinitive ?? 'Attack');
            timer!.target = attrs.Attribute.value<string>(this.game.instances.getAnyFromId(messagePayload.targetId), attrs.Title);
            timer!.instrumentDescription = attrs.Attribute.value<string>(this.game.instances.getAnyFromId(messagePayload.instrumentId), attrs.Title);
            timer!.minTime = TimeSource.getWallClockTicks(this.game);
            timer!.maxTime = TimeSource.getWallClockTicksFromNow(this.game, messagePayload.stageChangeTimeMillis);
            timer!.waiting = false;
            timer!.cancelled = false;
            switch( m.topicId ) {
              case Topic.id(Topics.ATTACK_WINDING_UP):
                timer!.currentPercent = 0;
                timer!.direction = 1;
                break;
              case Topic.id(Topics.ATTACK_COOLING_DOWN):
                timer!.currentPercent = 100;
                timer!.direction = -1;
                timer!.waiting = true;
                break;
              case Topic.id(Topics.ATTACK_CANCELLING):
                timer!.currentPercent = 100;
                timer!.direction = -1;
                timer!.waiting = true;
                timer!.cancelled = true;
                break;
            }
            await sleep(50);
            timer!.active = true;
          }
        }
      },
      disposeOnReset: false,
    });
    this.game.bus.notifications.subscribe({
      filter: (m) =>  {
        if( this.fight == null || m.topicId !== Topic.id(Topics.STUNNED)) {
          return false;
        }
        return true;
      },
      handler: async (m) =>  {
        if( this.fight != null ) {
          const messagePayload = payload<Topics.STUNNED>(m);
          const timer = this.fight.timers.get(messagePayload.targetId);
          if( this.fight.timers.has(messagePayload.targetId) ) {
            const target = this.game.instances.getAnyFromId(messagePayload.targetId);
            if( target != null ) {
              const targetTitle = getTitle(target);
              timer!.actionDescription = capitalFirst(lang.Adjective.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.STUNNED)).adjective(targetTitle.noun?.number ?? lang.Number.singular, lang.Case.nominative, targetTitle.noun?.gender));
            } else {
              timer!.actionDescription = capitalFirst('Stun');
            }
            timer!.target = undefined;
            timer!.instrumentDescription = undefined;
            timer!.minTime = TimeSource.getWallClockTicks(this.game);
            timer!.maxTime = TimeSource.getWallClockTicksFromNow(this.game, messagePayload.stunDurationMillis);
            timer!.active = true;
            timer!.waiting = true;
            timer!.cancelled = true;
            timer!.direction = -1;
          }
        }
      },
      disposeOnReset: false,
    });
    this.game.bus.notifications.subscribe({
      filter: (m) =>  {
        if( this.fight == null || (m.topicId !== Topic.id(Topics.BLOCK) && m.topicId !== Topic.id(Topics.BLOCKED)) ) {
          return false;
        }
        return true;
      },
      handler: async (m) =>  {
        if( this.fight != null ) {
          switch( m.topicId ) {
            case Topic.id(Topics.BLOCK): {
              const messagePayload = payload<Topics.BLOCK>(m);
              const timer = this.fight.timers.get(messagePayload.actorId);
              if( this.fight.timers.has(messagePayload.actorId) ) {
                const target = this.game.instances.getAnyFromId(messagePayload.actorId);
                if( target != null ) {
                  const targetTitle = getTitle(target);
                  timer!.actionDescription = capitalFirst(lang.Verb.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.BLOCK)).verb(targetTitle.noun?.number ?? lang.Number.singular, lang.Tense.present, lang.Person.third, targetTitle.noun?.gender, targetTitle.noun?.genderSubtype));
                } else {
                  timer!.actionDescription = capitalFirst('Blocking');
                }
                timer!.target = undefined;
                timer!.instrumentDescription = undefined;
                timer!.minTime = TimeSource.getWallClockTicks(this.game);
                timer!.maxTime = TimeSource.getWallClockTicksFromNow(this.game, messagePayload.blockWindupMillis);
                timer!.active = true;
                timer!.waiting = true;
                timer!.cancelled = false;
                timer!.direction = 1;
              }
              break;
            }
            case Topic.id(Topics.BLOCKED): {
              const messagePayload = payload<Topics.BLOCKED>(m);
              const timer = this.fight.timers.get(messagePayload.actorId);
              if( this.fight.timers.has(messagePayload.actorId) ) {
                timer!.currentPercent = 0;
              }
              break;
            }
          }
         }
      },
      disposeOnReset: false,
    });
    this.game.bus.notifications.subscribe({
      filter: (m) =>  {
        if( this.fight == null || (m.topicId !== Topic.id(Topics.DODGE) && m.topicId !== Topic.id(Topics.DODGED)) ) {
          return false;
        }
        return true;
      },
      handler: async (m) =>  {
        if( this.fight != null ) {
          switch( m.topicId ) {
            case Topic.id(Topics.DODGE): {
              const messagePayload = payload<Topics.DODGE>(m);
              const timer = this.fight.timers.get(messagePayload.actorId);
              if( this.fight.timers.has(messagePayload.actorId) ) {
                const target = this.game.instances.getAnyFromId(messagePayload.actorId);
                if( target != null ) {
                  const targetTitle = getTitle(target);
                  timer!.actionDescription = capitalFirst(lang.Verb.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.DODGE)).verb(targetTitle.noun?.number ?? lang.Number.singular, lang.Tense.present, lang.Person.third, targetTitle.noun?.gender, targetTitle.noun?.genderSubtype));
                } else {
                  timer!.actionDescription = capitalFirst('Dodging');
                }
                timer!.target = undefined;
                timer!.instrumentDescription = undefined;
                timer!.minTime = TimeSource.getWallClockTicks(this.game);
                timer!.maxTime = TimeSource.getWallClockTicksFromNow(this.game, messagePayload.dodgeCooldownMillis);
                timer!.active = true;
                timer!.waiting = true;
                timer!.cancelled = false;
                timer!.direction = -1;
              }
              break;
            }
            case Topic.id(Topics.DODGED): {
              const messagePayload = payload<Topics.DODGED>(m);
              const timer = this.fight.timers.get(messagePayload.actorId);
              if( this.fight.timers.has(messagePayload.actorId) ) {
                timer!.currentPercent = 0;
              }
              break;
            }
          }
         }
      },
      disposeOnReset: false,
    });
    this.game.bus.notifications.subscribe({
      filter: (m) =>  {
        if( this.fight == null || m.topicId !== Topic.id(Topics.DIED) ) {
          return false;
        }
        return true;
      },
      handler: (m) =>  {
        if( this.fight != null ) {
          const messagePayload = payload<Topics.DIED>(m);
          this.handleTargetRemoval(messagePayload.entityId);
        }
      },
      disposeOnReset: false,
    });
    this.game.bus.notifications.subscribe({
      filter: (m) =>  {
        if( this.fight == null ) {
          return false;
        }
        switch( m.topicId ) {
          case Topic.id(Topics.UI_REFRESH):
            return true;
          default:
            return false;
        }

      },
      handler: (m) =>  {
        const fight = this.fight;
        if( fight != null) {
          const actor = fight.actor;
          const targets = fight.targets;
          if( actor != null && targets != null ) {
            fight.actor = entityToViewModelRelationsSkills(actor.vm.entity as Entity, actor.vm.actor, this._cache, 'fight-cache');
            const newTargets = targets.map(t => entityToViewModelRelationsSkills(t.vm.entity as Entity, actor.vm.actor, this._cache, 'fight-cache'));
            fight.targets = targets.filter(t => newTargets.find(newTarget => newTarget.vm.entity.id === t.vm.entity.id) != null);
            newTargets.forEach(newTarget => {
              const existingTarget = targets.find(t => t.vm.entity.id === newTarget.vm.entity.id);
              if( existingTarget == null ) {
                fight.targets.push(newTarget);
              } else {
                // if target exists, we need to check skills
                existingTarget.vm = newTarget.vm;
                // if skill exists in original, but not is new, remove it
                existingTarget.skills = existingTarget.skills.filter(s => newTarget.skills.find(newTargetSkill => newTargetSkill.skill.id === s.skill.id) != null);
                // if skill is missing in original, add the skill
                newTarget.skills.forEach(newTargetSkill => {
                  if( existingTarget.skills.find(s => s.skill.id === newTargetSkill.skill.id) == null) {
                    existingTarget.skills.unshift(newTargetSkill);
                  }
                });
              }
            });
          }
        }
      },
      disposeOnReset: false,
    });
    this.game.bus.system.subscribe({
      filter: (m) =>  {
        if( this.fight == null || m.topicId !== Topic.id(Topics.CLOCK_TICK) ) {
          return false;
        }
        return true;
      },
      handler: async (m) =>  {
        if( this.fight != null ) {
          this.fight.isTimesourceRunning = true;
          this.fight.isTimeDilated = TimeSource.isTimeDilated();
          for(const timer of this.fight.timers.values()) {
            if( !timer.active ) {
              timer.currentPercent = 0;
              continue;
            }

            const currentTime = TimeSource.getWallClockTicks(this.game);
            let percentOfTimeElapsed = Math.min(100, Math.round(100 * (currentTime - timer.minTime) / (timer.maxTime - timer.minTime)));
            if( percentOfTimeElapsed < 100) {
              const shouldShortCircuit = percentOfTimeElapsed < 90;
              if( timer.direction > 0 ) {
                timer.currentPercent = shouldShortCircuit ? percentOfTimeElapsed : 100;
              } else {
                timer.currentPercent = shouldShortCircuit ? 100 - percentOfTimeElapsed : 0;
              }
              await sleep(50);
            } else {
              timer.active = false;
            }
          }
        }
      },
      disposeOnReset: false,
    });

  }

  private handleTargetRemoval(targetId: string) {
    if( this.fight == null ) {
      return;
    }

    if (this.fight.actor != null && targetId === this.fight.actor.vm.entity.id) {
      this._dialogService.closeAll();
      this.fight = null;
    } else {
      const targetIndex = this.fight.targets.findIndex(t => t.vm.entity.id === targetId);
      if (targetIndex !== -1) {
        this.fight.targets.splice(targetIndex, 1);
        if (this.fight.targets.length === 0) {
          this._dialogService.closeAll();
          this.fight = null;
        }
      }
    }
  }

  async show(actor: Actor, target: Entity) {
    this.fight = this.fight ?? {
      isTimesourceRunning: false,
      isTimeDilated: false,
      actor: entityToViewModelRelationsSkills(actor, actor, this._cache, 'fight-cache'),
      targets: [entityToViewModelRelationsSkills(target, actor, this._cache, 'fight-cache')],
      timers: new Map<string, FightTimer>(
        [
          [actor.id, createFightTimer()],
          [target.id, createFightTimer()],
        ]
      ),
    };

    if( this.fight.targets.find(t => t.vm.entity.id === target.id) == null ) {
      this.fight.targets.push(entityToViewModelRelationsSkills(target, actor, this._cache, 'fight-cache'));
      this.fight.timers.set(target.id, createFightTimer());
    }

    const response = await this._dialogService
      .open({ viewModel: FightViewModel, model: this.fight, lock: true })
      .whenClosed(response => {
        this.fight = null;
      });
  }
}
