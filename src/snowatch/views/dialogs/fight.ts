import { useView } from 'aurelia-framework';
import { ViewModelRelationsSkills } from 'snowatch/viewmodels';
import { NumericValueAttribute } from 'snowatch/attributes-ids';

import { LogManager } from 'aurelia-framework';
import { TimeSource } from 'snowatch/time-source';
const logger = LogManager.getLogger('snowatch-game-controller');

export interface FightTimer {
  actionDescription: string;
  instrumentDescription?: string;
  target?: string;
  direction: 1 | -1;
  minTime: number;
  maxTime: number;
  currentPercent: number;
  active: boolean;
  waiting: boolean;
  cancelled: boolean;
}

export function createFightTimer(): FightTimer {
  return {
    actionDescription: '',
    direction: 1,
    minTime: 0,
    maxTime: 0,
    currentPercent: 0,
    active: false,
    waiting: false,
    cancelled: false,
  };
}

export interface Fight {
  actor: ViewModelRelationsSkills | null;
  targets: Array<ViewModelRelationsSkills>;
  timers: Map<string, FightTimer>;
  isTimesourceRunning: boolean;
  isTimeDilated: boolean;
}

@useView('snowatch/views/dialogs/fight.html')
export class FightViewModel {
  fight: Fight = {
    actor: null,
    targets: [],
    timers: new Map(),
    isTimesourceRunning: false,
    isTimeDilated: false,
  };

  constructor() {
  }

  activate(fight: Fight) {
    this.fight = fight;
  }

  getActorAttributeBarWidth(attribute: NumericValueAttribute): number {
    return 3 * (attribute.maxValue ?? 100);
  }

  getActorAttributeBarPercent(attribute: NumericValueAttribute): number {
    return 100 * (attribute.value / (attribute.maxValue ?? 100));
  }

  getActionDescription(timerId: string): string {
    const timer = this.fight.timers.get(timerId);
    if( timer == null ) {
      return '<unknown>';
    }

    if( timer.instrumentDescription != null && timer.target != null) {
      return `${timer.actionDescription}: ${timer.instrumentDescription} \u21D2 ${timer.target}`;
    }

    return timer.actionDescription;
  }

  getActionDescpriptionVisibility(timerId: string) {
   return this.fight.timers.get(timerId)!.active ? 'visible' : 'hidden';
  }

  getProgressStyle(timerId: string) {
    const timer = this.fight.timers.get(timerId);
    const styles = new Array<string>();
    if( timer!.cancelled ) {
      styles.push('bg-danger');
    } else if( timer!.waiting && !timer!.cancelled ) {
      styles.push('bg-success');
    } else {
      styles.push('bg-info');
    }

    if( TimeSource.isTimeDilated() ) {
      styles.push('progress-bar-slow');
    } else {
     styles.push('progress-bar-fast');
    }

    return styles.join(' ');
   }
}
