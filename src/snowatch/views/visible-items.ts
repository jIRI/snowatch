import {bindable} from 'aurelia-framework';

import * as vm from 'snowatch/viewmodels';

export class VisibleItemsCustomElement {
  @bindable items: Array<vm.ViewModelRelationsSkills>;
}
