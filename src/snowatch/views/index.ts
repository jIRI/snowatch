import {FrameworkConfiguration} from 'aurelia-framework';
import { FightDialog } from 'snowatch/views/dialogs/fight-dialog';

export function configure(aurelia: FrameworkConfiguration) {
  aurelia.singleton(FightDialog, FightDialog);
}
