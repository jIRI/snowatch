import {bindable} from 'aurelia-framework';

export class DrawSurfaceCustomElement {
  public canvas: HTMLCanvasElement;

  attached() {
    const canvasElement = document.getElementById("canvas");
    if( canvasElement != null ) {
      canvasElement.addEventListener('click', (event: any): any => this.onClick(event));
    }

    const context = this.canvas.getContext('2d');
    if( context == null ) {
      console.log('oh no, no canvas...');
      return;
    }

    context.fillStyle = 'green';
    context.fillRect(10, 10, 100, 100);
  }

  onClick(event: any): any {
    const rect = this.canvas.getBoundingClientRect();
    const x = event.clientX - rect.left;
    const y = event.clientY - rect.top;
    console.log("x: " + x + " y: " + y);
  }
}
