import {bindable} from 'aurelia-framework';

import * as vm from 'snowatch/viewmodels';

export class InventoryCustomElement {
  @bindable items: Array<vm.ViewModelRelationsSkills>;
}
