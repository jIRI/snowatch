export class EmptyToNbspValueConverter {
  toView(value: string)  {
    return value == null || value === '' ? '&nbsp;' : value;
  }
}
