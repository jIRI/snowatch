import {bindable} from 'aurelia-framework';
import { NumericValueAttribute } from 'snowatch/attributes-ids';

import * as vm from 'snowatch/viewmodels';

export class ActorCustomElement {
  @bindable inventory: Array<vm.ViewModelRelationsSkills>;
  @bindable self: Array<vm.ViewModelRelationsSkills>;
  @bindable timesource: boolean;


  getActorAttributeBarWidth(attribute: NumericValueAttribute): number {
    return (attribute.maxValue ?? 100);
  }

  getActorAttributeBarPercent(attribute: NumericValueAttribute): number {
    return 100 * (attribute.value / (attribute.maxValue ?? 100));
  }

}
