import {bindable} from 'aurelia-framework';

export class RealTimePanelCustomElement {
  @bindable panel: any;
}
