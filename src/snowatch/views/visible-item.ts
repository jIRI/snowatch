import {bindable} from 'aurelia-framework';

import * as vm from 'snowatch/viewmodels';

export class VisibleItemCustomElement {
  @bindable item: vm.ViewModelRelationsSkills;
}
