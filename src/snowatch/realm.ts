import { GameData } from 'snowatch/game-data';
import { Message, broadcast, message } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { Scene, getCurrentScene } from 'snowatch/scene';
import { Attribute } from 'snowatch/attribute';
import { Behavior } from 'snowatch/behavior';
import * as ent from 'snowatch/entity';
import * as result from 'snowatch/result';

export interface RealmInit extends ent.EntityInit {
  sceneIds: Array<string>;
}

export class Realm extends ent.Entity {

  public static OBJECT_GROUP = 'realms';
  static registrator(game: GameData, realm: RealmInit | Realm) {
    const realmObj = Realm.ensureIsInstanceOf(realm);
    return realmObj.initialize(game);
  }

  public static getRealmForScene(scene: Scene): Realm | null {
    for(let [realmId, realm] of scene.game.instances.realms.entries()) {
      if(realm.sceneIds.includes(scene.id)) {
        return realm;
      }
    }

    return null;
  }

  public static getRealmIdForEntityId(game: GameData, entityId: string): string | null {
    const entity = game.instances.getAnyFromId(entityId);
    if( entity != null ) {
      return Realm.getRealmIdForEntity(entity);
    } else {
      return null;
    }
  }

  public static getRealmIdForEntity(entity: ent.StatelessEntity): string | null {
    const realm = Realm.getRealmForEntity(entity);
    return realm != null ? realm.id : null;
  }

  public static getRealmForEntity(entity: ent.StatelessEntity): Realm | null {
    const entityScene = getCurrentScene(entity);
    if( entityScene != null ) {
      return Realm.getRealmForScene(entityScene);
    } else {
      return null;
    }
  }

  public static addSceneId(target: Realm, id: string) {
    target.state.sceneIds.add(id);
  }

  public static removeSceneId(target: Realm, id: string) {
    target.state.sceneIds.delete(id);
  }

  public static ensureIsInstanceOf(obj: RealmInit | Realm): Realm {
    if (obj instanceof Realm) {
      return obj;
    }
    return new Realm(obj);
  }

  init: RealmInit;

  constructor(init: RealmInit | null = null) {
    super(init, Realm.OBJECT_GROUP);
  }

  initialize(game: GameData, init?: RealmInit) {
    super.initialize(game, init);
    this.state.sceneIds = this.state.sceneIds ?? new Set<string>(this.init.sceneIds);

    this.registerInitializer(() => {
      Attribute.initializeAttributes(this);
      Attribute.addFromProperties(this, this.properties);
      Attribute.addConstFromProperties(this, this.properties);
      Attribute.addCompFromProperties(this, this.properties);
      Behavior.initializeBehaviors(this);
      if( this.properties.behaviors != null ) {
        ent.registerEntities(this.game, Behavior.OBJECT_GROUP, Array.from(this.properties.behaviors.values()), Behavior.registrator);
        Behavior.addFromProperties(this, this.properties);
        for(let [behaviorId, behavior] of this.properties.behaviors) {
          Behavior.registerForActivation(this, behaviorId, behavior.initialState);
        }
      }
    });

    return this;
  }

  get sceneIds(): Array<string> {
    return Array.from<string>(this.state.sceneIds);
  }

  get scenes(): Array<Scene> {
    return Array.from<Scene>(this.state.sceneIds.map((id: string) => this.game.instances.scenes.get(id)));
  }

  onStart(message: Message) {
    super.onStart(message);
  }
}
