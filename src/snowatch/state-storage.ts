import * as result from 'snowatch/result';

import {LogManager} from 'aurelia-framework';
const logger = LogManager.getLogger('snowatch-state-storage');

const SLOT_LIST_ID = 'snowatch-state-slots';
const SLOT_PREFIX = 'snowatch-state-slot';

enum SupportedTypes {
  Map = 'Map',
  Object = 'Object',
  Set = 'Set',
  Array = 'Array',
}

export interface Storage {
  getItem(slot: string): any;
  setItem(slot: string, data: string): void;
  removeItem(slot: string): void;
}

export function getSlots(storage: Storage): Array<string> {
  const slots = storage.getItem(SLOT_LIST_ID);
  if( slots != null ) {
    return JSON.parse(slots);
  } else {
    storage.setItem(SLOT_LIST_ID, JSON.stringify([]));
    return [];
  }
}

export function hasSlot(storage: Storage, slot: string): boolean {
  const slots = storage.getItem(SLOT_LIST_ID);
  if( slots != null ) {
    const slotIds = JSON.parse(slots);
    return slotIds.includes(slot);
  }

  return false;
}

export function addSlot(storage: Storage, slot: string) {
  logger.debug(`adding new slot '${slot}'`);
  const slots = getSlots(storage);
  if( !slots.includes(slot) ) {
    slots.push(slot);
    storage.setItem(SLOT_LIST_ID, JSON.stringify(slots));
    storage.setItem(getFullSlotName(slot), '{}');
  }
}

export function removeSlot(storage: Storage, slot: string) {
  logger.debug(`removing slot '${slot}'`);
  const slots = getSlots(storage);
  const slotIndex = slots.indexOf(slot);
  if( slotIndex !== -1 ) {
    slots.splice(slotIndex, 1);
    storage.setItem(SLOT_LIST_ID, JSON.stringify(slots));
    storage.removeItem(getFullSlotName(slot));
  }
}

export function store(storage: Storage, slot: string, state: any): result.ResultObject {
  logger.debug(`storing state to slot '${slot}'`);
  const slots = getSlots(storage);
  if(!slots.includes(slot)) {
    return result.make(result.fail, `Storing failed: No slot '${slot}'`);
  }

  const status = prepareForSerialization(state);
  if( !result.isResult(status, result.ok) ) {
    return status;
  }

  storage.setItem(getFullSlotName(slot), JSON.stringify(status.data));
  return result.make(result.ok);
}

export function restore(storage: Storage, slot: string): result.ResultObject {
  logger.debug(`restoring state from slot '${slot}'`);
  const slots = getSlots(storage);
  if(!slots.includes(slot)) {
    return result.make(result.fail, `Restoring failed: No slot '${slot}'`);
  }

  const obj = JSON.parse(storage.getItem(getFullSlotName(slot)));
  const status = deserialize(obj);
  return status;
}


function getFullSlotName(slot: string): string {
  return `${SLOT_PREFIX}:${slot}`;
}

function prepareForSerialization(obj: any): result.ResultObject {
  if( null == obj ) {
    return result.make(result.ok, obj);
  }

  switch( obj.constructor.name ) {
    case SupportedTypes.Map:
      return prepareMapForSerialization(obj);
    case SupportedTypes.Object:
      return prepareObjectForSerialization(obj);
    case SupportedTypes.Set:
      return prepareSetForSerialization(obj);
    case SupportedTypes.Array:
      return prepareArrayForSerialization(obj);
    default:
      return result.make(result.ok, obj);
  }
}

function prepareMapForSerialization(obj: any): result.ResultObject {
  const serializableObject = {
    __type: SupportedTypes.Map,
  };

  for(let [key, val] of obj.entries()) {
    const status = prepareForSerialization(val);
    if( !result.isResult(status, result.ok) ) {
      return status;
    }
    (<any>serializableObject)[key] = status.data;
  }

  return result.make(result.ok, serializableObject);
}

function prepareObjectForSerialization(obj: any): result.ResultObject {
  const serializableObject = {
    __type: SupportedTypes.Object,
  };

  for(let key of Object.keys(obj)) {
    const status = prepareForSerialization(obj[key]);
    if( !result.isResult(status, result.ok) ) {
      return status;
    }
    (<any>serializableObject)[key] = status.data;
  }

  return result.make(result.ok, serializableObject);
}

function prepareSetForSerialization(obj: any): result.ResultObject {
  const serializableObject = {
    __type: SupportedTypes.Set,
    __items: new Array<any>(),
  };

  for(let val of obj.values()) {
    const status = prepareForSerialization(val);
    if( !result.isResult(status, result.ok) ) {
      return status;
    }
    serializableObject.__items.push(status.data);
  }

  return result.make(result.ok, serializableObject);
}

function prepareArrayForSerialization(obj: any): result.ResultObject {
  const serializableObject = {
    __type: SupportedTypes.Array,
    __items: new Array<any>(),
  };

  for(let val of obj.values()) {
    const status = prepareForSerialization(val);
    if( !result.isResult(status, result.ok) ) {
      return status;
    }
    serializableObject.__items.push(status.data);
  }

  return result.make(result.ok, serializableObject);
}

function deserialize(obj: any): result.ResultObject {
  if( null == obj ) {
    return result.make(result.ok, obj);
  }

  switch( obj.__type ) {
    case SupportedTypes.Map:
      return deserializeMap(obj);
    case SupportedTypes.Object:
      return deserializeObject(obj);
    case SupportedTypes.Set:
      return deserializeSet(obj);
    case SupportedTypes.Array:
      return deserializeArray(obj);
    default:
      return result.make(result.ok, obj);
  }
}

function deserializeMap(obj: any): result.ResultObject {
  const deserializedObject = new Map();

  for(let key of Object.keys(obj).filter(k => !k.startsWith('__'))) {
    const status = deserialize(obj[key]);
    if( !result.isResult(status, result.ok) ) {
      return status;
    }
    deserializedObject.set(key, status.data);
  }

  return result.make(result.ok, deserializedObject);
}

function deserializeObject(obj: any): result.ResultObject {
  const deserializedObject = {};

  for(let key of Object.keys(obj).filter(k => !k.startsWith('__'))) {
    const status = deserialize(obj[key]);
    if( !result.isResult(status, result.ok) ) {
      return status;
    }
    (<any>deserializedObject)[key] = status.data;
  }

  return result.make(result.ok, deserializedObject);
}

function deserializeSet(obj: any): result.ResultObject {
  const deserializedObject = new Set();

  for(let val of obj.__items) {
    const status = deserialize(val);
    if( !result.isResult(status, result.ok) ) {
      return status;
    }
    deserializedObject.add(status.data);
  }

  return result.make(result.ok, deserializedObject);
}

function deserializeArray(obj: any): result.ResultObject {
  const deserializedObject = new Array();

  for(let val of obj.__items) {
    const status = deserialize(val);
    if( !result.isResult(status, result.ok) ) {
      return status;
    }
    deserializedObject.push(status.data);
  }

  return result.make(result.ok, deserializedObject);
}