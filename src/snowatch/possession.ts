import {Entity, HasId} from 'snowatch/entity';
import {message, broadcast} from 'snowatch/message';
import {Topics} from 'snowatch/message-topics/all';

export interface PossesionProperties {
  possessions: Map<string, PossessionInit>;
}

export interface PossessionInit extends HasId {
}

export class Possession {
  static initializePossesions(target: Entity) {
    target.logger.debug(`${target.id}: initializePossessions()`);
    target.state.possessions = target.state.possessions ?? new Set<string>();
  }

  static add(target: Entity, itemId: string) {
    if( !target.state.possession.has(itemId) ) {
      target.logger.debug(`Adding item to possessions ${itemId}`);
      target.state.possession.add(itemId);
      target.game.bus.notifications.publish(broadcast(Topics.POSSESSION_ADDED, { entityId: target.id, itemId: itemId }));
    }
  }

  static addFromProperties(target: Entity, properties: PossesionProperties) {
    if( properties != null && properties.possessions != null ) {
      for(let [possessionId, possession] of properties.possessions) {
        Possession.add(target, possessionId);
      }
    }
  }

  static remove(target: Entity, itemId: string) {
    target.logger.debug(`Removing item from possessions ${itemId}`);
    target.state.possession.delete(itemId);
    target.game.bus.notifications.publish(broadcast(Topics.POSSESSION_REMOVED, { entityId: target.id, itemId: itemId }));
  }

  static removeAll(target: Entity) {
    target.logger.debug(`Clearing all possessions`);
    for(let itemId of target.state.possessions) {
      Possession.remove(target, itemId);
    }
  }

  static has(target: Entity, itemId: string) {
    return target.state.possesions != null && target.state.possesions.has(itemId);
  }
}
