import {GameData} from 'snowatch/game-data';
import {Message, broadcast} from 'snowatch/message';
import {Topics} from 'snowatch/message-topics/all';
import {Skill, SkillInit} from 'snowatch/skill';
import {Behavior} from 'snowatch/behavior';
import {Attribute} from 'snowatch/attribute';
import {Knowledge, KnowledgeInit} from 'snowatch/knowledge';
import {DialogueLine} from 'snowatch/dialogue-line';
import {Possession, PossessionInit} from 'snowatch/possession';
import {Inventory, InventoryInit, InventoriesInit} from 'snowatch/inventory';
import {Surface} from 'snowatch/surface';
import {SpatialRelations} from 'snowatch/spatial-relations';
import { Realm } from 'snowatch/realm';
import * as spatial from 'snowatch/spatial-relations';
import * as result from 'snowatch/result';
import * as ent from 'snowatch/entity';
import * as scene from 'snowatch/scene';
import * as attrs from 'snowatch/attributes/all';

export interface ActorPropertiesInit extends ent.PropertiesInit {
  inventory?: Array<InventoryInit>;
  inventories?: Array<InventoriesInit>;
  possessions?: Array<PossessionInit>;
  skills?: Array<SkillInit>;
  knowledge?: Array<KnowledgeInit>;
}

export interface ActorInit extends ent.EntityInit {
  name?: string;
  nounId?: string;
  startSceneId?: string;
  isVisible?: boolean;
  holderId?: string | null;
  ownerId?: string | null;
  surfaces?: Array<string>;
  spatialLocation?: spatial.Vector3;
  spatialSize?: spatial.Vector3;
  containerId?: string;
  properties?: ActorPropertiesInit;
}


export class Actor extends ent.Entity {
  static ensureIsInstanceOf(obj: Actor | ActorInit): Actor {
    if( obj instanceof Actor ) {
      return obj;
    }
    return new Actor(obj);
  }

  public static OBJECT_GROUP = 'actors';

  static registrator(game: GameData, o: Actor | ActorInit): Actor {
    const obj = Actor.ensureIsInstanceOf(o);
    return obj.initialize(game);
  }

  init: ActorInit;

  constructor(init: ActorInit | null = null) {
    super(init, Actor.OBJECT_GROUP);
  }

  initialize(game: GameData, init?: ActorInit) {
    super.initialize(game, init);

    this.registerInitializer(() => {
      Attribute.initializeAttributes(this);
      Attribute.addFromProperties(this, this.properties);
      Attribute.addConstFromProperties(this, this.properties);
      Attribute.addCompFromProperties(this, this.properties);
      // add default attrs (ignores what was defined in properties)
      Attribute.add(this, attrs.NounResourceId, { initialState: {value: this.init.nounId ?? this.init.id}});
      Attribute.addComp(this, attrs.Title, { compState: {
        value: (entity, attributeId, actor?: ent.Entity) => {
          return ent.getTitle(this).title;
        }
      }});
      Attribute.add(this, attrs.ActorMode, { initialState: { value: attrs.ActorModes.Normal }});
      Attribute.add(this, attrs.IsCurrentActor, { initialState: { value: false }});
      Attribute.add(this, attrs.IsVisible, { initialState: {value: this.init.isVisible != null ? this.init.isVisible : true}});
      Attribute.add(this, attrs.StartSceneId, { initialState: {value: this.init.startSceneId ?? null}});
      Attribute.add(this, attrs.CurrentSceneId, { initialState: {value: null}});
      Attribute.add(this, attrs.CanMove, { initialState: {value: true}});
      Attribute.add(this, attrs.HasSpatialRelations, { initialState: {value: true}});
      Attribute.add(this, attrs.SpatialSize, { initialState: {value: this.init.spatialSize ?? {x: 0.5, y: 0.4, z: 1.8}}});
      Attribute.add(this, attrs.SpatialLocation, { initialState: {value: this.init.spatialLocation ?? {x: 0, y: 0, z: 0}}});
      Attribute.add(this, attrs.Surfaces, { initialState: {value: this.init.surfaces ?? Surface.outsideAround}});
    });
    this.registerInitializer(() => {
      Behavior.initializeBehaviors(this);
      if( this.properties.behaviors != null ) {
        ent.registerEntities(this.game, Behavior.OBJECT_GROUP, Array.from(this.properties.behaviors.values()), Behavior.registrator);
        Behavior.addFromProperties(this, this.properties);
        for(let [behaviorId, behavior] of this.properties.behaviors) {
          Behavior.registerForActivation(this, behaviorId, behavior.initialState);
        }
      }
    });
    this.registerInitializer(() => {
      Skill.initializeSkills(this);
      Skill.removeAll(this);
      Skill.addFromProperties(this, this.properties);
    });
    this.registerInitializer(() => {
      Inventory.initializeInventory(this);
      Inventory.addFromProperties(this, this.properties);
    });
    this.registerInitializer(() => {
      Possession.initializePossesions(this);
      Possession.removeAll(this);
      Possession.addFromProperties(this, this.properties);
    });
    this.registerInitializer(() => {
      Knowledge.initializeKnowledge(this);
      Knowledge.removeAll(this);
      Knowledge.addFromProperties(this, this.properties);
    });
    this.registerInitializer(() => {
      DialogueLine.initializeDialogueLines(this);
      DialogueLine.removeAll(this);
      if( this.properties.dialogueLines != null ) {
        ent.registerEntities(this.game, DialogueLine.OBJECT_GROUP, Array.from(this.properties.dialogueLines.values()), DialogueLine.registrator);
        DialogueLine.addFromProperties(this, this.properties);
      }
    });

    return this;
  }

  onStart(message: Message, ...args: any[]) {
    super.onStart(message);
    const startSceneId = scene.getStartSceneId(this);
    if( startSceneId != null && scene.getCurrentSceneId(this) == null) {
      this.tryChangeSceneTo(startSceneId);
    }
  }

  tryChangeSceneTo(newSceneName: string): result.ResultObject {
    const current = scene.getCurrentScene(this);
    const oldSceneId = current != null ? current.id : null;
    const oldRealmId = Realm.getRealmIdForEntity(this);
    const newScene = this.game.instances.scenes.get(newSceneName);
    if( newScene == null ) {
      return result.make(result.fail, `No scene with ID '${newSceneName}'`);
    }

    if( current != null ) {
      const canLeaveResult = current.canLeave(this.id);
      if( !result.isAccepted(canLeaveResult) ) {
        return canLeaveResult;
      }
    }

    const canEnterResult = newScene.canEnter(this.id);
    if( !result.isAccepted(canEnterResult) ) {
      return canEnterResult;
    }

    if( current != null ) {
      current.preLeave(this.id);
    }
    newScene.preEnter(this.id);
    if( current != null ) {
      current.onLeave(this.id);
    }

    // remove all spatial relations of the actor when changing scenes
    SpatialRelations.removeRelationsOf(this.game, this.id);

    attrs.Attribute.setValue<string>(this, attrs.CurrentSceneId, newScene.id);
    newScene.onEnter(this.id);

    const newRealmId = Realm.getRealmIdForEntity(this);

    // notify subscribers scene changed
    this.game.bus.notifications.publish(broadcast(
      Topics.ENTITY_SCENE_CHANGED,
      { entityId: this.id,
        oldSceneId: oldSceneId,
        newSceneId: newScene.id
      },
      undefined,
      this.id,
      [oldRealmId, newRealmId].filter((realmId): realmId is string => realmId !== null)
    ));

    if( current != null ) {
      current.postLeave(this.id);
    }
    newScene.postEnter(this.id);

    return result.make(result.accepted);
  }
}
