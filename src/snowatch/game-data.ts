import {inject, All, Optional} from 'aurelia-framework';
import {Bus} from 'snowatch/bus';
import {Topic, broadcast, payload} from 'snowatch/message';
import {Topics} from 'snowatch/message-topics/all';
import {StatelessEntity, Entity, EntityInit, registerEntities, Disposable, deregisterEntities} from 'snowatch/entity';
import {Behavior} from 'snowatch/behavior';
import {Scene} from 'snowatch/scene';
import {Realm} from 'snowatch/realm';
import {Skill} from 'snowatch/skill';
import {Actor} from 'snowatch/actor';
import {Item} from 'snowatch/item';
import {Substance} from 'snowatch/substance';
import {Knowledge} from 'snowatch/knowledge';
import {DialogueLine} from 'snowatch/dialogue-line';
import {SpatialRelation, SpatialRelations} from 'snowatch/spatial-relations';
import {LanguageStructure} from 'snowatch/language-structures';
import {ResourceDictionary} from 'snowatch/resource-dictionary';
import {TimeSource, GameClock} from 'snowatch/time-source';
import { DebugExtension } from 'snowatch/debug-extensions/debug-extension';
import { getTypeId } from 'snowatch/utils';

import {LogManager} from 'aurelia-framework';

const logger = LogManager.getLogger('snowatch-game-data');
const busLogger = LogManager.getLogger('snowatch-bus-log');


export class GlobalGameState {
  // allow game to add any other global state
  [index: string]: any;
}

export class GameSettings  {
  [index: string]: any;
  currentCulture = ResourceDictionary.DEFAULT_CULTURE;
}

export interface DefaultDataModel {
  [index: string]: any;
  behaviors: Map<string, Behavior>;
  realms: Map<string, Realm>;
  scenes: Map<string, Scene>;
  skills: Map<string, Skill>;
  actors: Map<string, Actor>;
  items: Map<string, Item>;
  knowledge: Map<string, Knowledge>;
  dialogueLines: Map<string, DialogueLine>;
  entities: Map<string, Entity>;
  substances: Map<string, Substance>;
  spatialRelations: Map<string, SpatialRelation>;
}

export type SpatialEntities = Actor | Item | Entity | Substance;
export type EntitiesWithId = Actor | Item | Entity | Substance | Knowledge;
export type StatelessEntitiesWithId = DialogueLine;

export interface InstanceModel extends DefaultDataModel {
  registerInstance(instance: StatelessEntity): void;
  create<T>(typeName: string | null): T | null;
  getSceneFromId(id: string): Scene | undefined;
  getSubstanceFromId(id: string): Substance | undefined;
  getAllSpatials(): Array<SpatialEntities>;
  getSpatialFromId(id: string): SpatialEntities | undefined;
  getEntityFromId(id: string): EntitiesWithId | undefined;
  getStatelessEntityFromId(id: string): StatelessEntitiesWithId | undefined;
  getAnyFromId(id: string): any;
  getContainerFromId(id: string): any;
}

export interface StateModel extends DefaultDataModel {
  gameLog: Array<GameLogItem>;
  gameClock: GameClock;
  settings: GameSettings;
  global: GlobalGameState;
}

export interface GameLogItem {
  source: string;
  action: string;
  style?: string;
  category?: string;
  title?: string;
  description?: string;
  descriptionLines?: Array<string>;
}

interface EntityRegistrationMapEntry {
  group: string;
  objects: Array<Entity | EntityInit>;
  registrator: Function;
}

@inject(
  Optional.of(GameSettings),
  All.of(Behavior),
  All.of(Realm),
  All.of(Scene),
  All.of(Skill),
  All.of(Actor),
  All.of(Item),
  All.of(Knowledge),
  All.of(DialogueLine),
  All.of(Entity),
  All.of(Substance),
  All.of(LanguageStructure),
  All.of(ResourceDictionary),
  Optional.of(GlobalGameState),
  All.of(DebugExtension),
)
export class GameData {
  private _disposables: Array<Disposable>;
  private _entityRegistrationMap: Array<EntityRegistrationMapEntry>;
  private _garbage: Array<StatelessEntity>;

  public state: StateModel;
  public bus: Bus;
  public startActor: Actor;
  public currentActor: Actor;
  public instances: InstanceModel;
  public resources: Map<string, any>;
  public languageStructures: Map<string, LanguageStructure>;

  constructor(
      gameSettings: GameSettings,
      behaviors: Array<Behavior>,
      realms: Array<Realm>,
      scenes: Array<Scene>,
      skills: Array<Skill>,
      actors: Array<Actor>,
      items: Array<Item>,
      knowledge: Array<Knowledge>,
      dialogueLines: Array<DialogueLine>,
      entities: Array<Entity>,
      substances: Array<Substance>,
      langStructures: Array<LanguageStructure>,
      resourceDictionaries: Array<ResourceDictionary>,
      globalGameState: GlobalGameState,
      debugExtensions: Array<DebugExtension>
    ) {
    logger.debug('initializing story data');

    // first create disposable storage so other components can register...
    this._disposables = new Array<Disposable>();
    this.bus = new Bus(this);
    this.bus.system.subscribe({
      allowAsync: false,
      disposeOnReset: false,
      filter: (m) => {
        return m.topicId === Topic.id(Topics.RESET_STATE);
      },
      handler: (m)  => {
        logger.debug('resetting story data');
        logger.debug('disposing registered disposables');
        const disposables = this._disposables;
        while( disposables.length > 0 ) {
          const disposer = disposables.pop();
          if( disposer != null ) {
            disposer.dispose();
          }
        }
        this._disposables = new Array<Disposable>();
        logger.debug('resetting instances');
        this.instances = new InstancesObject();
        logger.debug('resetting resources');
        this.resources = new Map<string, any>();
        this.languageStructures = new Map<string, LanguageStructure>();
        logger.debug('resetting state');
        this.state = {
          behaviors: new Map<string, Behavior>(),
          realms: new Map<string, Realm>(),
          scenes: new Map<string, Scene>(),
          skills: new Map<string, Skill>(),
          actors: new Map<string, Actor>(),
          items: new Map<string, Item>(),
          knowledge: new Map<string, Knowledge>(),
          dialogueLines: new Map<string, DialogueLine>(),
          entities: new Map<string, Entity>(),
          substances: new Map<string, Substance>(),
          spatialRelations: new Map<string, SpatialRelation>(),
          gameLog: new Array<GameLogItem>(),
          gameClock: TimeSource.bot(0.01),
          settings: gameSettings ?? new GameSettings(),
          global: globalGameState ?? new GlobalGameState(),
        };

        logger.debug('initializing time source');
        TimeSource.initializeGameClock(this);
        // forward to command bus automatically
        // this needs to be here so controller can attach to input bus with logger to ensure it gets messages before the state is changed
        this.bus.input.subscribe({
          handler: m => {
            this.bus.commands.publish(Object.assign({}, m));
          },
        });

        logger.debug('registering entites');
        this._entityRegistrationMap = [
          { group: Entity.OBJECT_GROUP, objects: entities, registrator: Entity.registrator },
          { group: Behavior.OBJECT_GROUP, objects: behaviors, registrator: Behavior.registrator},
          { group: Knowledge.OBJECT_GROUP, objects: knowledge, registrator: Knowledge.registrator},
          { group: DialogueLine.OBJECT_GROUP, objects: dialogueLines, registrator: DialogueLine.registrator},
          { group: Skill.OBJECT_GROUP, objects: skills, registrator: Skill.registrator},
          { group: Realm.OBJECT_GROUP, objects: realms, registrator: Realm.registrator},
          { group: Scene.OBJECT_GROUP, objects: scenes, registrator: Scene.registrator},
          { group: Item.OBJECT_GROUP, objects: items, registrator: Item.registrator},
          { group: Actor.OBJECT_GROUP, objects: actors, registrator: Actor.registrator},
          { group: Substance.OBJECT_GROUP, objects: substances, registrator: Substance.registrator},
        ];
        this.performInitialEntityRegistration();
        langStructures.forEach(ls => LanguageStructure.registrator(this, ls));
        resourceDictionaries.forEach(resDict => ResourceDictionary.registrator(this, resDict));

        // if any debug extensions are registered, register them
        if( debugExtensions != null ) {
          debugExtensions.forEach(ext => ext.register(this));
        }
      }
    });

    // here we are collecting entities which are no longer needed.
    // we need to do this, because there might be behaviors handling entites shortly after they are marked for deletion by other behaviors
    this._garbage = new Array<StatelessEntity>();
    this.bus.system.subscribe({
      allowAsync: true,
      disposeOnReset: false,
      filter: (m) => {
        if( m.topicId !== Topic.id(Topics.CLOCK_TICK) ) {
          return false;
        }
        const messagePayload = payload<Topics.CLOCK_TICK>(m);
        return messagePayload.wallClockTicks % 10 === 0;
      },
      handler: (m)  => {
        while( this._garbage.length > 0 ) {
          const entity = this._garbage.shift();
          if( entity == null ) {
            continue;
          }

          logger.debug(`Collecting [${entity.id}]`);

          if( entity instanceof Entity ) {
            Behavior.removeAll(entity);
          }
          SpatialRelations.removeRelationsOf(entity.game, entity.id);
          deregisterEntities(entity.game, entity.entityGroup, entity);
        }
      }
    });
  }

  addToGarbage(entity: StatelessEntity) {
    logger.debug(`Entity [${entity.id}] added to garbage, will be collected soon`);
    this._garbage.push(entity);
  }

  disposeOnReset(disposable: Disposable) {
    this._disposables.push(disposable);
  }

  removeDisposer(disposable: Disposable) {
    const index = this._disposables.indexOf(disposable);
    if( index > -1) {
      this._disposables.splice(index, 1);
    }
  }

  get gameLog() {
    return this.state.gameLog;
  }

  addToGameLog(item: GameLogItem) {
    item.style = item.style ?? item.category;
    this.state.gameLog.push(item);
    this.bus.notifications.publish(broadcast(Topics.GAME_LOG_UPDATED));
  }

  performInitialEntityRegistration() {
    this._entityRegistrationMap.forEach(entry =>
      registerEntities(this, entry.group, entry.objects, entry.registrator)
        .forEach((entity: StatelessEntity) => this.instances.registerInstance(entity)));
  }

  ensureAllStateEntitiesRegistered() {
    this._entityRegistrationMap.forEach(entry => {
      const groupState: Map<string, any> = this.state[entry.group];
      for(let [entityId, entityState] of groupState) {
        if( this.instances.getAnyFromId(entityId) == null ) {
          // parse first part of the ID -- that is baseId
          const idParts = entityId.split('.');
          if( idParts.length <= 1 ) {
            return;
          }
          // look into instances, find the one with the baseId
          const templateEntity = this.instances[entry.group].get(idParts[0]);
          if( templateEntity == null) {
            return;
          }
          // take its init and add second part of the ID as instanceId
          const instanceInit = {
            ...templateEntity.init,
            instanceId: idParts.slice(1).join('.'),
          };
          // register entity with that init object
          registerEntities(this, entry.group, instanceInit, entry.registrator);
        }
      }
    });
  }
}

export class InstancesObject implements InstanceModel {
  private _registeredInstances: Map<string, StatelessEntity> = new Map<string, StatelessEntity>();
  public behaviors: Map<string, Behavior> = new Map<string, Behavior>();
  public realms: Map<string, Realm> = new Map<string, Realm>();
  public scenes: Map<string, Scene> = new Map<string, Scene>();
  public skills: Map<string, Skill> = new Map<string, Skill>();
  public actors: Map<string, Actor> = new Map<string, Actor>();
  public items: Map<string, Item> = new Map<string, Item>();
  public knowledge: Map<string, Knowledge> = new Map<string, Knowledge>();
  public dialogueLines: Map<string, DialogueLine> = new Map<string, DialogueLine>();
  public entities: Map<string, Entity> = new Map<string, Entity>();
  public substances: Map<string, Substance> = new Map<string, Substance>();
  public spatialRelations: Map<string, SpatialRelation> = new Map<string, SpatialRelation>();

  public registerInstance(instance: StatelessEntity): void {
    this._registeredInstances.set(getTypeId(instance), instance);
  }

  public create<T>(typeId: string): T | null {
    const instance = this._registeredInstances.get(typeId);
    if( instance == null ) {
      return null;
    }
    return new (<any>instance).constructor();
  }

  public getAllSpatials(): Array<SpatialEntities> {
    return [...this.actors.values(), ...this.items.values(), ...this.entities.values(), ...this.substances.values()];
  }

  public getSpatialFromId(id: string): EntitiesWithId | undefined {
    return this.actors.get(id)
          ?? this.items.get(id)
          ?? this.entities.get(id)
          ?? this.substances.get(id);
  }

  public getRealmFromId(id: string): Realm | undefined {
    return this.realms.get(id);
  }

  public getSceneFromId(id: string): Scene | undefined {
    return this.scenes.get(id);
  }

  public getSubstanceFromId(id: string): Substance | undefined {
    return this.substances.get(id);
  }

  public getEntityFromId(id: string):  EntitiesWithId | undefined {
    return this.actors.get(id)
          ?? this.items.get(id)
          ?? this.knowledge.get(id)
          ?? this.entities.get(id)
          ?? this.substances.get(id);
  }

  public getStatelessEntityFromId(id: string): StatelessEntitiesWithId | undefined {
    return this.dialogueLines.get(id);
  }

  public getAnyFromId(id: string): any {
    return this.behaviors.get(id)
          ?? this.realms.get(id)
          ?? this.scenes.get(id)
          ?? this.skills.get(id)
          ?? this.actors.get(id)
          ?? this.items.get(id)
          ?? this.knowledge.get(id)
          ?? this.dialogueLines.get(id)
          ?? this.entities.get(id)
          ?? this.substances.get(id)
          ?? this.spatialRelations.get(id);
  }

  public getContainerFromId(id: string): any {
    return this.items.get(id)
          ?? this.entities.get(id);
  }
}