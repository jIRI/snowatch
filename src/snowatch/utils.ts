import {Channel} from 'snowatch/bus';
import {Topic, Message, payload} from 'snowatch/message';
import {Topics} from 'snowatch/message-topics/all';
import {PublishOptions} from 'snowatch/bus';
import {Actor} from 'snowatch/actor';
import * as attrs from 'snowatch/attributes-ids';
import * as ent from 'snowatch/entity';
import * as result from 'snowatch/result';

export interface ArrayFilterFunction {
  (value: unknown, index: number, array: unknown[]): boolean;
}

export function isArrayNullOrEmpty<T>(array: Array<T> | null | undefined): boolean {
  return array == null || array.length === 0;
}

export function tryGetArrayElement<T>(array: Array<T> | null | undefined, index: number): T | null {
  if( array == null || array.length <= index ) {
    return null;
  }

  return array[index];
}

export function nextElementInCircularBuffer<T>(array: T[], previousIndex: number): [T, number] {
  let currentIndex = previousIndex + 1;
  if( currentIndex >= array.length ) {
    currentIndex = 0;
  }

  return [array[currentIndex], currentIndex];
}

export function getTypeId(instance: any): string {
  return (<any>instance.constructor).ID ?? instance.constructor.name;
}

export function cloneIfObject(source: any): any {
  if( source instanceof Function ) {
    return source;
  } else if( source instanceof Map ) {
    const clone = new Map();
    for(let [key, val] of source.entries()) {
      clone.set(key, cloneIfObject(val));
    }
    return clone;
  } else if( source instanceof Set ) {
    const clone = new Set();
    for(let [key, val] of source.values()) {
      clone.add(cloneIfObject(val));
    }
    return clone;
  } else if( source instanceof Array ) {
    return Array.from(source, val => cloneIfObject(val));
  } else if( source instanceof Object ) {
    const clone: any = {};
    for( const propName in source ) {
      clone[propName] = cloneIfObject(source[propName]);
    }
    return clone;
  } else {
    return source;
  }
}

export function arrayDeepClone<T>(source: Array<T>): Array<T> {
  return source.map(e => Object.assign({}, cloneIfObject(e)));
}

export function pickByGender<T extends any>(actor: Actor | ent.Entity | null | undefined, male: T, female: T, neutral?: T): T {
  if( actor == null ) {
    return male;
  }

  let gender: string = attrs.Attribute.value<attrs.GenderIdValues>(actor, attrs.GenderId, actor);
  if( gender === attrs.GenderIdValues.Female ) {
    return female;
  } else if( gender === attrs.GenderIdValues.Neutral && neutral != null ) {
    return neutral;
  }

  return male;
}

export function clamp(value: number, lower?: number, upper?: number): number {
  if( lower != null ) {
    value = Math.max(value, lower);
  }
  if( upper != null ) {
    value = Math.min(value, upper);
  }
  return value;
}

export function isString(s: any) {
    return typeof(s) === 'string' || (s instanceof String);
}

export function evaluate(arg: any, ...params: Array<any>) {
  return (typeof(arg) === 'function') ? arg(...params) : arg;
}

export function evalProperty(object: any, property: string, ...args: Array<any>) {
  if( null == object[property] ) {
    return true;
  }

  return evaluate(object[property], ...args);
}


export function sleep(ms: number) {
    return new Promise(function(res, rej) {
        setTimeout(res, ms);
    });
}

export class CollectionCache {
  private _sourceGetter: Function;
  private _filter: ArrayFilterFunction | null | undefined;
  private _array: Array<any>;
  private _isDirty: boolean;

  constructor(sourceGetter: Function, filter?: ArrayFilterFunction) {
    this._sourceGetter = sourceGetter;
    this._filter = filter;
    this._array = [];
    this._isDirty = true;
  }

  get array() {
    return this._array;
  }

  refresh() {
    const source = Array.from(this._sourceGetter());
    const filtered = this._filter ? source.filter(this._filter) : source;
    if( this._isDirty ) {
      this._array = filtered;
    } else if( this._array.length !== filtered.length ) {
      this._array = filtered;
    } else {
      for(let i = 0; i < filtered.length; i++) {
        if( this._array[i] !== filtered[i]) {
          this._array = filtered;
        }
      }
    }

    this._isDirty = false;
    return this;
  }

  markDirty() {
    this._isDirty = true;
  }
}

export class AutoRefreshCollectionCache {
  private _sourceGetter: Function;
  private _filter: ArrayFilterFunction | null | undefined;
  private _array: Array<any>;
  private _notificationChannel: Channel;

  constructor(notificationChannel: Channel, sourceGetter: Function, filter?: ArrayFilterFunction) {
    this._notificationChannel = notificationChannel;
    this._sourceGetter = sourceGetter;
    this._filter = filter;
    this._array = [];
    this._notificationChannel.subscribe({
      filter: (m) =>  m.topicId === Topic.id(Topics.UI_REFRESH),
      handler: (m) => this.refresh()
    });
  }

  get array() {
    return this._array;
  }

  refresh() {
    const source = Array.from(this._sourceGetter());
    const filtered = this._filter ? source.filter(this._filter) : source;
    this._array = filtered;
    return this;
  }

}

export enum RejectFilterBehaviors {
  response = "response",
  any = "any",
}

export class BusRequest {
  private _requestChannel: Channel;
  private _responseChannel: Channel;
  private _clockTickChannel: Channel | null;
  private _resolveFilter: Function;
  private _rejectFilter: Function;
  private _timeoutFilter: Function;
  private _result: result.ResultObject;
  private _rejectFilterBehavior: RejectFilterBehaviors;
  private _id: number;

  constructor(params: {
    requestBus: Channel,
    responseBus: Channel,
    clockTickBus?: Channel,
    resolveFilter?: Function,
    rejectFilter?: Function,
    timeoutFilter?: Function,
    timeoutWallClockTicks?: number,
    rejectFilterBehavior?: RejectFilterBehaviors,
  }) {
    this._requestChannel = params.requestBus;
    this._responseChannel = params.responseBus;
    this._clockTickChannel = params.clockTickBus ?? null;
    this._resolveFilter = params.resolveFilter ?? (() => false);
    this._rejectFilter = params.rejectFilter ?? (() => false);
    this._timeoutFilter = params.timeoutFilter ?? ((m: Message) => (m.topicId === Topic.id(Topics.CLOCK_TICK) && (params.timeoutWallClockTicks == null || params.timeoutWallClockTicks <= payload<Topics.CLOCK_TICK>(m).wallClockTicks)));
    this._rejectFilterBehavior = params.rejectFilterBehavior ?? RejectFilterBehaviors.response;
    this._id = 1;
  }

  get id() {
    return this._id;
  }

  get result() {
    return this._result;
  }

  async send(msg: Message, options?: PublishOptions) {
    this._id = msg.id;
    let result = await this.doSend(msg, options);
    return result;
  }

  private doSend(msg: Message, options?: PublishOptions) {
    return new Promise<result.ResultObject>((resolve, reject) => {
      const finisher = (fn: Function, subscriptions: Array<ent.Disposable>) => {
        subscriptions.forEach(s => s?.dispose());
        fn();
        resolve(this._result);
      };

      const subscriptions = [
        this._responseChannel.subscribe({
          filter: (m) => {
            return m.responseTo === this._id && this._resolveFilter(m);
          },
          handler: (m) => {
            finisher(() => this._result = result.make(result.accepted, m.payload), subscriptions);
          },
        }),
        this._responseChannel.subscribe({
          filter: (m) => {
            switch( this._rejectFilterBehavior ) {
              case RejectFilterBehaviors.any:
                return this._rejectFilter(m);
              default:
                return m.responseTo === this._id && this._rejectFilter(m);
            }
          },
          handler: (m) => {
            finisher(() => this._result = result.make(result.rejected, m.payload), subscriptions);
          }
        }),
        this._clockTickChannel?.subscribe({
          filter: (m) => {
            return this._timeoutFilter(m);
          },
          handler: (m) => {
            finisher(() => this._result = result.make(result.timeout, m.payload), subscriptions);
          }
        }) ?? { dispose: () => {} },
      ];

      this._requestChannel.publish(msg, options);
    });
  }
}

