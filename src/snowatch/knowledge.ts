import {GameData} from 'snowatch/game-data';
import {broadcast} from 'snowatch/message';
import {Topics} from 'snowatch/message-topics/all';
import {Attribute} from 'snowatch/attribute';
import * as ent from 'snowatch/entity';

export interface KnowledgeInit extends ent.EntityInit {
  initialState?: any;
}

export interface KnowledgeProperties {
  knowledge: Map<string, KnowledgeInit>;
}

export class Knowledge extends ent.Entity {
  static ensureIsInstanceOf(obj: Knowledge | KnowledgeInit) {
   if( obj instanceof Knowledge ) {
     return obj;
   }
   return new Knowledge(obj);
  }

  public static OBJECT_GROUP = 'knowledge';
  static registrator(game: GameData, o: KnowledgeInit) {
    const obj = Knowledge.ensureIsInstanceOf(o);
    return obj.initialize(game);
  }

  static initializeKnowledge(target: ent.Entity) {
    target.logger.debug(`${target.id}: initializeKnowledge()`);
    target.state.knowledge = target.state.knowledge ?? new Map<string, any>();
  }

  static getState(target: ent.Entity, id: string) {
    return target.state.knowledge.get(id);
  }

  static add(target: ent.Entity, knowledgeId: string, knowledge?: KnowledgeInit) {
    if( !target.state.knowledge.has(knowledgeId) ) {
      target.logger.debug(`Adding knowledge ${knowledgeId} (${knowledge})`);
      const initialState = (knowledge != null) ? (knowledge.initialState ?? {}) : {};
      target.state.knowledge.set(knowledgeId, initialState);
      target.game.bus.notifications.publish(broadcast(Topics.KNOWLEDGE_ADDED, { entityId: target.id, knowledgeId: knowledgeId }));
    }
  }

  static addFromProperties(target: ent.Entity, properties: KnowledgeProperties) {
    if( properties != null && properties.knowledge != null ) {
      for(let [knowledgeId, knowledge] of properties.knowledge) {
        Knowledge.add(target, knowledgeId, knowledge);
      }
    }
  }

  static remove(target: ent.Entity, knowledgeId: string) {
    target.logger.debug(`Removing knowledge ${knowledgeId}`);
    target.state.knowledge.delete(knowledgeId);
    target.game.bus.notifications.publish(broadcast(Topics.KNOWLEDGE_REMOVED, { entityId: target.id, knowledgeId: knowledgeId }));
  }

  static removeAll(target: ent.Entity) {
    target.logger.debug(`Clearing all knowledge`);
    const knowledge = Array.from<string>(target.state.knowledge.keys());
    for(let knowledgeId of knowledge) {
      Knowledge.remove(target, knowledgeId);
    }
  }

  static has(target: ent.Entity, knowledgeId: string) {
    if( target == null || target.state == null || target.state.knowledge == null ) {
      return false;
    }

    return target.state.knowledge.has(knowledgeId);
  }

  constructor(init: KnowledgeInit | null = null) {
    super(init, Knowledge.OBJECT_GROUP);
  }

  initialize(game: GameData, init?: KnowledgeInit) {
    super.initialize(game, init);

    this.registerInitializer(() => {
      // knowledge has some related attributes like teachable etc., so we need to add those here...
      Attribute.initializeAttributes(this);
      Attribute.addFromProperties(this, this.properties);
    });

    return this;
  }
}
