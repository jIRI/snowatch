
export class Surface {
  public static readonly inside = 'inside';
  public static readonly innerTop = 'inner-top';
  public static readonly outerTop = 'outer-top';
  public static readonly innerBottom = 'inner-bottom';
  public static readonly outerBottom = 'outer-bottom';
  public static readonly innerLeft = 'inner-left';
  public static readonly outerLeft = 'outer-left';
  public static readonly innerRight = 'inner-right';
  public static readonly outerRight = 'outer-right';
  public static readonly innerFront = 'inner-front';
  public static readonly outerFront = 'outer-front';
  public static readonly innerBack = 'inner-back';
  public static readonly outerBack = 'outer-back';
  public static readonly none = [];
  public static readonly outsideAll = [Surface.outerFront, Surface.outerBack, Surface.outerLeft, Surface.outerRight, Surface.outerTop, Surface.outerBottom];
  public static readonly outsideAround = [Surface.outerFront, Surface.outerBack, Surface.outerLeft, Surface.outerRight];
  public static readonly insideAll = [Surface.innerFront, Surface.innerBack, Surface.innerLeft, Surface.innerRight, Surface.innerTop, Surface.innerBottom];
  public static readonly insideNoTop = [Surface.innerFront, Surface.innerBack, Surface.innerLeft, Surface.innerRight, Surface.innerBottom];

  public static getOpposite(surfaceId: string): string | null {
    switch(surfaceId) {
      case Surface.innerTop:
        return Surface.outerTop;
      case Surface.outerTop:
        return Surface.outerBottom;
      case Surface.innerBottom:
        return Surface.outerBottom;
      case Surface.outerBottom:
        return Surface.outerTop;
      case Surface.innerLeft:
        return Surface.outerLeft;
      case Surface.outerLeft:
        return Surface.outerRight;
      case Surface.innerRight:
        return Surface.outerRight;
      case Surface.outerRight:
        return Surface.outerLeft;
      case Surface.innerFront:
        return Surface.outerFront;
      case Surface.outerFront:
        return Surface.outerBack;
      case Surface.innerBack:
        return Surface.outerBack;
      case Surface.outerBack:
        return Surface.outerFront;
    }
    return null;
  }
}