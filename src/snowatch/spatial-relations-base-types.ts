
export const Zero3: Vector3 = {x: 0, y: 0, z: 0};

export const BaseX: Vector3 = {x: 1, y: 0, z: 0};
export const BaseY: Vector3 = {x: 0, y: 1, z: 0};
export const BaseZ: Vector3 = {x: 0, y: 0, z: 1};
export const BaseNegX: Vector3 = {x: -1, y: 0, z: 0};
export const BaseNegY: Vector3 = {x: 0, y: -1, z: 0};
export const BaseNegZ: Vector3 = {x: 0, y: 0, z: -1};

export const Unit3: Vector3 = {x: 1, y: 1, z: 1};

export interface Vector3 {
  x: number;
  y: number;
  z: number;
}

export interface RectangleBounds {
  topLeft: Vector3;
  topRight: Vector3;
  bottomLeft: Vector3;
  bottomRight: Vector3;
}

export interface BoxBounds {
  anchor: Vector3;
  left: RectangleBounds;
  right: RectangleBounds;
  top: RectangleBounds;
  bottom: RectangleBounds;
  front: RectangleBounds;
  back: RectangleBounds;
}

export interface BoxNormals {
  left: Vector3;
  right: Vector3;
  top: Vector3;
  bottom: Vector3;
  front: Vector3;
  back: Vector3;
}

export interface SpatialRelationProperties {
  spatialRelations: Map<string, SpatialRelationInit>;
}

export interface SpatialRelationInit {
  targetId: string;
  typeId: string;
}

export interface SpatialRelationDef {
  id: string;
  targetSurfaceId: string;
  prepositionActionResource: string;
  prepositionDescriptionResource: string;
}

export interface SpatialRelation {
  id: string;
  directId: string;
  indirectId: string;
  typeId: string;
}

export interface AvailableVolume {
  volume: number;
}
