import {GameData} from 'snowatch/game-data';
import {Topic, Message, broadcast, message, payload} from 'snowatch/message';
import {Topics} from 'snowatch/message-topics/all';
import {TimeSource} from 'snowatch/time-source';
import * as result from 'snowatch/result';
import * as storage from 'snowatch/state-storage';

export interface InputLogRecording {
  name: string;
}

export interface InputLogItem {
  description: string;
  message: string;
  slotName: string;
}

export class InputLogger {
  game: GameData;
  private _enableRecording = false;
  private _storage: Storage;
  private _items: Array<InputLogItem>;
  private _recordingName: string = InputLogger.INPUT_LOGGER_STORAGE_DEFAULT_SLOT;
  private static readonly INPUT_LOGGER_STORAGE_PREFIX = '@@';
  private static readonly INPUT_LOGGER_STORAGE_DEFAULT_SLOT = 'default';
  private static readonly MESSAGE_LOG_SLOT_NAME_BASE = `${InputLogger.INPUT_LOGGER_STORAGE_PREFIX}#message-log`;

  constructor(game: GameData) {
    this.game = game;
    this._storage = localStorage;

    this.addRecording(this._recordingName);
    this.activateRecording(this._recordingName);

    this.game.bus.input.subscribe({
      disposeOnReset: false,
      filter: m => this.isRecording,
      handler: m => this.storeMessage(m)
    });
  }

  get recordings(): Array<InputLogRecording> {
    return storage
      .getSlots(this._storage)
      .filter(s => s.startsWith(`${InputLogger.MESSAGE_LOG_SLOT_NAME_BASE}:`))
      .map(r => r.split(':')[1])
      .filter(r => r.indexOf('#') === -1)
      .map(r => ({
        name: r
      }));
  }

  get items() {
    return this._items;
  }

  get isRecording() {
    return this._enableRecording;
  }

  get recordingName(): string {
    return this._recordingName;
  }

  addRecording(recordingName: string, currentRecordingItemsIndex?: number) {
    this._recordingName = recordingName;
    storage.addSlot(this._storage, this.slotName);
    if( currentRecordingItemsIndex != null ) {
      const inheritedItems = this._items.slice(0, currentRecordingItemsIndex + 1);
      this._items = inheritedItems;
      this.save();
    }
  }

  deleteRecording() {
    if( this.recordings.length > 1) {
      storage
        .getSlots(this._storage)
        .filter(s => s.startsWith(`${InputLogger.MESSAGE_LOG_SLOT_NAME_BASE}:${this._recordingName}`))
        .forEach(r => storage.removeSlot(this._storage, r));

      this.save();
      this.activateRecording(this.recordings[0].name);
    }
  }

  activateRecording(recordingName: string) {
    this._recordingName = recordingName;
    const res = storage.restore(this._storage, this.slotName);
    if( !result.isResult(res, result.ok) || res.data == null || !Array.isArray(res.data) ) {
      this._items = new Array<InputLogItem>();
    } else {
      this._items = res.data;
    }
  }

  replayTo(index: number, clockScale: number = 0) {
    this.stopRecording();

    this.game.bus.system.publish(broadcast(Topics.RESET_GAME), {allowAsync: false});

    if( clockScale !== 0 ) {
      TimeSource.pushTimeScaleDivider(this.game, clockScale);
    }

    const messageQueue = this._items.slice(0, index + 1);
    const serializedMessage = messageQueue.shift();
    if( serializedMessage == null ) {
      return;
    }

    let message = InputLogger.parseMessage(serializedMessage.message);

    const tickSubscription = this.game.bus.system.subscribe({
      filter: m => m.topicId === Topic.id(Topics.CLOCK_TICK),
      handler: m => {
        if( message.timestamp === payload<Topics.CLOCK_TICK>(m).wallClockTicks ) {
          this.game.bus.commands.publish(message);
          const logItem = messageQueue.shift();
          if( logItem == null ) {
            if( clockScale !== 0 ) {
              TimeSource.popTimeScaleDivider(this.game);
            }
            return;
          }

          message = InputLogger.parseMessage(logItem.message);
        }
      }
    });
  }

  restoreAt(index: number) {
    this.stopRecording();
    const logItem = this._items[index];
    this.game.bus.system.publish(broadcast(Topics.RESET_GAME), { allowAsync: false });
    this.game.bus.system.publish(broadcast(Topics.LOAD_STATE, { slotName: logItem.slotName }), { allowAsync: false });
    // based on expectation that Rx delivers messages in order subscribers subscribe,
    // the logger should always get the message before it is forwarded, and we can do following:
    this.game.bus.commands.publish(InputLogger.parseMessage(logItem.message));
  }

  trimFrom(index: number) {
    this._items.splice(index, this._items.length - index);
    this.save();
  }

  startRecording() {
    this._enableRecording = true;
  }

  stopRecording() {
    this._enableRecording = false;
  }

  clear() {
    this._items.forEach(i => {
      storage.removeSlot(this._storage, i.slotName);
    });
    this._items = new Array<InputLogItem>();
    this.save();
  }

  private get slotName(): string {
    return `${InputLogger.MESSAGE_LOG_SLOT_NAME_BASE}:${this._recordingName}`;
  }

  private storeMessage(message: Message) {
    const slotName = `${this.slotName}#${Date.now()}`;
    storage.addSlot(this._storage, slotName);
    storage.store(this._storage, slotName, this.game.state);

    this._items.push({
      description: `[${message.topicId}] ${JSON.stringify(message.payload, (key, val) => val == null || val.length === 0 ? undefined : val).replace(/,/g, ', ').replace(/\"/g, '')}`,
      message: JSON.stringify(message),
      slotName
    });
    this.save();
  }

  private save() {
    storage.store(this._storage, this.slotName, this._items);
  }

  private static parseMessage(json: string): Message {
    const message = JSON.parse(json);
    return message;
  }

}