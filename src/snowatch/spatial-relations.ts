export * from 'snowatch/spatial-relations-base-types';

import {getCurrentScene} from 'snowatch/scene';
import {Attribute} from 'snowatch/attribute';
import {GameData, SpatialEntities} from 'snowatch/game-data';
import {broadcast} from 'snowatch/message';
import {Topics} from 'snowatch/message-topics/all';
import {ResourceDictionary} from 'snowatch/resource-dictionary';
import {DefaultResDict} from 'snowatch/resources/default';
import {Vector3, Zero3, RectangleBounds, BoxBounds, BoxNormals, AvailableVolume, SpatialRelationDef, SpatialRelation, SpatialRelationProperties} from 'snowatch/spatial-relations-base-types';
import { Surface } from 'snowatch/surface';
import { SubstancePhases } from 'snowatch/substance-base-types';
import { SpatialPrepositions } from 'snowatch/prepositions';
import * as ent from 'snowatch/entity';
import * as attrs from 'snowatch/attributes-ids';
import * as lang from 'snowatch/language-structures';
import * as result from 'snowatch/result';

export function hasSpatialRelations(entity: ent.Entity) {
  if( attrs.Attribute.applies(entity, attrs.HasSpatialRelations) ) {
    return attrs.Attribute.value<boolean>(entity, attrs.HasSpatialRelations);
  }

  return false;
}

export function areaSurface(surfaceId: string, size: Vector3) {
  switch(surfaceId) {
    case Surface.innerTop:
    case Surface.outerTop:
    case Surface.innerBottom:
    case Surface.outerBottom:
      return size.x * size.y;
    case Surface.innerLeft:
    case Surface.outerLeft:
    case Surface.innerRight:
    case Surface.outerRight:
      return size.z * size.y;
    case Surface.innerFront:
    case Surface.outerFront:
    case Surface.innerBack:
    case Surface.outerBack:
      return size.x * size.x;
  }

  return 0;
}

export function rectWidth(bounds: RectangleBounds): number {
  return bounds.topRight.x - bounds.topLeft.x;
}

export function rectHeight(bounds: RectangleBounds): number {
  return bounds.topLeft.y - bounds.bottomLeft.x;
}

export function rectTop(bounds: RectangleBounds): number {
  return bounds.topLeft.y;
}

export function rectBottom(bounds: RectangleBounds): number {
  return bounds.bottomLeft.y;
}

export function rectLeft(bounds: RectangleBounds): number {
  return bounds.topLeft.x;
}

export function rectRight(bounds: RectangleBounds): number {
  return bounds.topRight.x;
}

export function magnitudeSquaredVect3(vector: Vector3): number {
  return vector.x * vector.x + vector.y * vector.y + vector.z * vector.z;
}

export function magnitudeVect3(vector: Vector3): number {
  return Math.sqrt(magnitudeSquaredVect3(vector));
}

export function dotVect3(first: Vector3, second: Vector3) {
  return first.x * second.x + first.y * second.y + first.z * second.z;
}

export function mulVect3(first: Vector3, second: Vector3): Vector3 {
  return {
    x: first.x * second.x,
    y: first.y * second.y,
    z: first.z * second.z,
  };
}

export function addVect3(...vectors: Vector3[]): Vector3 {
  const result: Vector3 = {x: 0, y: 0, z: 0};
  vectors.forEach(v => {
    result.x = result.x + v.x;
    result.y = result.y + v.y;
    result.z = result.z + v.z;
  });
  return result;
}

export function subVect3(...vectors: Vector3[]): Vector3 {
  const result: Vector3 = {x: vectors[0].x, y: vectors[0].y, z: vectors[0].z};
  vectors.forEach(v => {
    result.x = result.x - v.x;
    result.y = result.y - v.y;
    result.z = result.z - v.z;
  });
  return result;
}

export function normalizeVect3(vector: Vector3): Vector3 {
  const mag = magnitudeVect3(vector);
  return {
    x: vector.x / mag,
    y: vector.y / mag,
    z: vector.z / mag
  };
}

export function getSpatialLocation(entity: ent.StatelessEntity): Vector3 | null {
  if( attrs.Attribute.applies(entity, attrs.SpatialLocation) ) {
    return attrs.Attribute.value<Vector3>(entity, attrs.SpatialLocation);
  }

  return null;
}

export function setSpatialLocation(entity: ent.Entity, location: Vector3 | null) {
  if( location != null &&  attrs.Attribute.applies(entity, attrs.SpatialLocation) ) {
    attrs.Attribute.setValue<Vector3>(entity, attrs.SpatialLocation, location);
  }
}


export function getSpatialSize(entity: ent.StatelessEntity): Vector3 | null {
  if( attrs.Attribute.applies(entity, attrs.SpatialSize) ) {
    return attrs.Attribute.value<Vector3>(entity, attrs.SpatialSize);
  }

  return null;
}

export function getVolume(entity: ent.StatelessEntity): number | null {
  if( attrs.Attribute.applies(entity, attrs.Volume) ) {
    return attrs.Attribute.value<number>(entity, attrs.Volume);
  }

  return null;
}

export function getBoxBounds(location: Vector3, size: Vector3): BoxBounds {
  const delta: Vector3  = {
    x: size.x / 2,
    y: size.y / 2,
    z: size.z / 2
  };

  return {
    anchor: location,
    front: {
      topLeft:      { x: location.x - delta.x, y: location.y + delta.y, z: location.z + delta.z},
      topRight:     { x: location.x + delta.x, y: location.y + delta.y, z: location.z + delta.z},
      bottomLeft:   { x: location.x - delta.x, y: location.y + delta.y, z: location.z - delta.z},
      bottomRight:  { x: location.x + delta.x, y: location.y + delta.y, z: location.z - delta.z},
    },
    back: {
      topLeft:      { x: location.x - delta.x, y: location.y - delta.y, z: location.z + delta.z},
      topRight:     { x: location.x + delta.x, y: location.y - delta.y, z: location.z + delta.z},
      bottomLeft:   { x: location.x - delta.x, y: location.y - delta.y, z: location.z - delta.z},
      bottomRight:  { x: location.x + delta.x, y: location.y - delta.y, z: location.z - delta.z},
    },
    top: {
      topLeft:      { x: location.x - delta.x, y: location.y - delta.y, z: location.z + delta.z},
      topRight:     { x: location.x + delta.x, y: location.y - delta.y, z: location.z + delta.z},
      bottomLeft:   { x: location.x - delta.x, y: location.y + delta.y, z: location.z + delta.z},
      bottomRight:  { x: location.x + delta.x, y: location.y + delta.y, z: location.z + delta.z},
    },
    bottom: {
      topLeft:      { x: location.x - delta.x, y: location.y - delta.y, z: location.z - delta.z},
      topRight:     { x: location.x + delta.x, y: location.y - delta.y, z: location.z - delta.z},
      bottomLeft:   { x: location.x - delta.x, y: location.y + delta.y, z: location.z - delta.z},
      bottomRight:  { x: location.x + delta.x, y: location.y + delta.y, z: location.z - delta.z},
    },
    left: {
      topLeft:      { x: location.x - delta.x, y: location.y - delta.y, z: location.z + delta.z},
      topRight:     { x: location.x - delta.x, y: location.y + delta.y, z: location.z + delta.z},
      bottomLeft:   { x: location.x - delta.x, y: location.y - delta.y, z: location.z - delta.z},
      bottomRight:  { x: location.x - delta.x, y: location.y + delta.y, z: location.z - delta.z},
    },
    right: {
      topLeft:      { x: location.x + delta.x, y: location.y - delta.y, z: location.z + delta.z},
      topRight:     { x: location.x + delta.x, y: location.y + delta.y, z: location.z + delta.z},
      bottomLeft:   { x: location.x + delta.x, y: location.y - delta.y, z: location.z - delta.z},
      bottomRight:  { x: location.x + delta.x, y: location.y + delta.y, z: location.z - delta.z},
    }
  };
}

export function getBoxNormals(box: BoxBounds): BoxNormals {
  return {
    left: getRectangleNormal(box.anchor, box.left),
    right: getRectangleNormal(box.anchor, box.right),
    top: getRectangleNormal(box.anchor, box.top),
    bottom: getRectangleNormal(box.anchor, box.bottom),
    front: getRectangleNormal(box.anchor, box.front),
    back: getRectangleNormal(box.anchor, box.back),
  };
}

export function isPointInRectangle(rect: RectangleBounds, point: Vector3): boolean {
  if( point.z !== rect.topLeft.z
    || point.z !== rect.topRight.z
    || point.z !== rect.bottomLeft.z
    || point.z !== rect.bottomRight.z
  ) {
    return false;
  }

  if( point.x >= rect.topLeft.x && point.x <= rect.topRight.x
    && point.y <= rect.topLeft.y && point.x >= rect.bottomRight.y
  ) {
    return true;
  }

  return false;
}

export interface RectangleSplit {
  upLeft: RectangleBounds;
  upRight: RectangleBounds;
  downLeft: RectangleBounds;
  downRight: RectangleBounds;
}

export function divideRectangle(rect: RectangleBounds, cutPoint: Vector3): RectangleSplit {
  return {
    upLeft: {
      topLeft: rect.topLeft,
      topRight: { x: cutPoint.x, y: rect.topLeft.y, z: cutPoint.z},
      bottomLeft: { x: rect.topLeft.x, y: cutPoint.y, z: cutPoint.z},
      bottomRight: cutPoint
    },
    upRight: {
      topLeft: { x: cutPoint.x, y: rect.topLeft.y, z: cutPoint.z},
      topRight: rect.topRight,
      bottomLeft: cutPoint,
      bottomRight: { x: rect.bottomRight.x, y: cutPoint.y, z: cutPoint.z}
    },
    downLeft: {
      topLeft: { x: rect.topLeft.x, y: cutPoint.y, z: cutPoint.z},
      topRight: cutPoint,
      bottomLeft: rect.bottomLeft,
      bottomRight: { x: cutPoint.x, y: rect.bottomLeft.y, z: cutPoint.z}
    },
    downRight: {
      topLeft: cutPoint,
      topRight: { x: rect.bottomRight.x, y: cutPoint.y, z: cutPoint.z},
      bottomLeft: { x: cutPoint.x, y: rect.bottomLeft.y, z: cutPoint.z},
      bottomRight: rect.bottomRight
    }
  };
}

export function getRectangleNormal(objectAnchorLocation: Vector3, bounds: RectangleBounds): Vector3 {
  // because we always align to perpendicular grid, we can simplify things here...

  // rectangle is aligned along the y axis
  if( bounds.topLeft.x === bounds.topRight.x ) {
    if( objectAnchorLocation.x < bounds.topLeft.x ) {
      // anchor is on the left, this is right side, the normal is positive
      return {x: 1, y: 0, z: 0};
    } else {
      // anchor is on the right, this is left side, the normal is negative
      return {x: -1, y: 0, z: 0};
    }
  }

  // rectangle is aligned along x axis
  if( bounds.topLeft.y === bounds.topRight.y ) {
    if( objectAnchorLocation.y < bounds.topLeft.y ) {
      // anchor is in the front, this is front side, the normal is positive
      return {x: 0, y: 1, z: 0};
    } else {
      // anchor is in the back, this is back side, the normal is negative
      return {x: 0, y: -1, z: 0};
    }
  }

  // rectangle is aligned along z axis
  if( bounds.topLeft.z === bounds.bottomLeft.z ) {
    if( objectAnchorLocation.z < bounds.topLeft.z ) {
      // anchor is down, this is top side, the normal is positive
      return {x: 0, y: 0, z: 1};
    } else {
      // anchor is up, this is bottom side, the normal is negative
      return {x: 0, y: 0, z: -1};
    }
  }

  return Zero3;
}

export function scaleRect(rect: RectangleBounds, distance: number): RectangleBounds {
  const scaleFactor = 1 / (0.1 + distance);
  const offset = {
    x: scaleFactor * (rect.topRight.x - rect.topLeft.x),
    y: scaleFactor * (rect.topLeft.y - rect.bottomLeft.y),
    z: 0
  };
  return {
    topLeft: addVect3(rect.topLeft, offset),
    topRight: subVect3(rect.topRight, offset),
    bottomLeft: addVect3(rect.bottomLeft, offset),
    bottomRight: subVect3(rect.bottomRight, offset),
  };
}

export function boxProjectionRectangle(box: BoxBounds, viewPoint: Vector3, lineOfSight: Vector3): RectangleBounds {
  const scaledNear = scaleRect(box.front, magnitudeVect3(subVect3(box.front.topLeft, viewPoint)));
  const scaledFar = scaleRect(box.back, magnitudeVect3(subVect3(box.back.topLeft, viewPoint)));

  const projectX = dotVect3(getRectangleNormal(box.anchor, box.left), normalizeVect3(lineOfSight));
  const projectY = dotVect3(getRectangleNormal(box.anchor, box.top), normalizeVect3(lineOfSight));

  return {
    topLeft: { x: scaledFar.topLeft.x * (1 + projectX), y: scaledFar.topLeft.y * (1 + projectY), z: 0 },
    topRight: { x: scaledNear.topRight.x, y: scaledFar.topLeft.y, z: 0 },
    bottomLeft: { x: scaledFar.topLeft.x * (1 + projectX), y: scaledNear.bottomLeft.y * (1 + projectY), z: 0 },
    bottomRight: { x: scaledNear.topLeft.x, y: scaledNear.topLeft.y, z: 0 },
  };
}

export function isPointVisibleFromViewpoint(allSpatials: Array<ent.Entity>, viewPoint: Vector3, targetEntity: ent.Entity): boolean {
  const targetLocation = getSpatialLocation(targetEntity);
  const targetSize = getSpatialSize(targetEntity) ?? Zero3;

  if( targetLocation == null) {
    // no location -> ambient -> always visible
    return true;
  }

  const direction = subVect3(targetLocation, viewPoint);
  const targetBoxBounds = getBoxBounds(targetLocation, targetSize);

  // no size -> point source -> easy check
  // collect all objects which are between viewpoint and target point
  const spatialsInLineOfSight = new Array<{ boxBounds: BoxBounds, boxNormals: BoxNormals, entity: SpatialEntities}>();
  for(let entity of allSpatials) {
    const location = getSpatialLocation(entity);
    const size = getSpatialLocation(entity);
    if( location == null || size == null ) {
      continue;
    }

    // now we are going to determine which objects lie in direction between target entity and viewPoint
    // for that we will calculate normal vectors of each object and calculate dot product with vector going from target to viewPoint
    // in case dot product is positive, normals are approximatelly in direction of with might be visible by observer at viewpoint
    // negative dot products are out of sight and will be rejected from further evaluation
    const boxBounds = getBoxBounds(location, size);
    const boxNormals = getBoxNormals(boxBounds);
    if(dotVect3(direction, boxNormals.left) < 0
      && dotVect3(direction, boxNormals.right) < 0
      && dotVect3(direction, boxNormals.top) < 0
      && dotVect3(direction, boxNormals.bottom) < 0
      && dotVect3(direction, boxNormals.front) < 0
      && dotVect3(direction, boxNormals.back) < 0
    ) {
      continue;
    }

    spatialsInLineOfSight.push({ boxBounds, boxNormals, entity});
  }

  // if there are no entities in direction between viewPoint and target, target is visible
  if( spatialsInLineOfSight.length === 0 ) {
    return true;
  }

  // now check the occlusion
  // first we need to determine which side of the target if facing our directino by getting normals and calculating dot products
  // then:
  // -- create array of rectangles, put target rectangle in it
  const visibleRects = new Array<RectangleBounds>();
  visibleRects.push(boxProjectionRectangle(targetBoxBounds, viewPoint, direction));

  // -- build array of all rectangles in the line of sight
  const lineOfSightRects = new Array<RectangleBounds>();
  spatialsInLineOfSight.forEach(spatial => lineOfSightRects.push(boxProjectionRectangle(spatial.boxBounds, viewPoint, direction)));

  while( lineOfSightRects.length > 0 ) {
    // -- pop one rectangle out of the  line of sight rectangles and chech whether it intersects with current visible rectangle
    const lineOfSightRect = lineOfSightRects.pop();
    if( lineOfSightRect == null ) {
      break;
    }

    while( visibleRects.length > 0 ) {
      // -- pop one visible rectangle which we will work on in this iteration
      const visibleRect = visibleRects.pop();
      if( visibleRect == null ) {
        break;
      }

      // -- for all cases in which intersection can occur (there is 9 of them)
      //    split current visible rect and add parts which are not cover by line of sight rectangle back to visible rectangles array
      //    then break out of line of sight loop to evaluate
      if( isPointInRectangle(visibleRect, lineOfSightRect.topLeft)
        && isPointInRectangle(visibleRect, lineOfSightRect.topRight)
        && isPointInRectangle(visibleRect, lineOfSightRect.bottomLeft)
        && isPointInRectangle(visibleRect, lineOfSightRect.bottomRight)
      ) {
        const splitLeft = divideRectangle(visibleRect, lineOfSightRect.topLeft);
        visibleRects.push(splitLeft.upLeft);
        visibleRects.push(splitLeft.downLeft);

        const splitRight = divideRectangle(visibleRect, lineOfSightRect.bottomRight);
        visibleRects.push(splitRight.upLeft);
        visibleRects.push(splitRight.downLeft);

        visibleRects.push({
          topLeft: splitLeft.upLeft.topRight,
          topRight: splitRight.upLeft.topLeft,
          bottomLeft: splitLeft.upLeft.bottomRight,
          bottomRight: {x: splitRight.upLeft.topLeft.x, y: splitLeft.upLeft.bottomRight.y, z: lineOfSightRect.topLeft.z},
        });

        visibleRects.push({
          topLeft: {x: splitLeft.downLeft.topRight.x, y: splitRight.downLeft.topLeft.y, z: lineOfSightRect.topLeft.z},
          topRight: splitRight.downRight.topLeft,
          bottomLeft: splitLeft.downLeft.bottomRight,
          bottomRight: splitRight.downRight.bottomLeft,
        });
      }

      if( isPointInRectangle(visibleRect, lineOfSightRect.topLeft)
        && isPointInRectangle(visibleRect, lineOfSightRect.topRight)
      ) {
        const split = divideRectangle(visibleRect, lineOfSightRect.topLeft);
        visibleRects.push(split.upLeft);
        visibleRects.push(split.upRight);
        visibleRects.push(split.downLeft);
        split.downRight.topLeft.x = lineOfSightRect.topLeft.x;
        split.downRight.bottomLeft.x = lineOfSightRect.topLeft.x;
        visibleRects.push(split.downRight);
      }

      if( isPointInRectangle(visibleRect, lineOfSightRect.topLeft)
        && isPointInRectangle(visibleRect, lineOfSightRect.bottomLeft)
      ) {
        const split = divideRectangle(visibleRect, lineOfSightRect.topLeft);
        visibleRects.push(split.upLeft);
        visibleRects.push(split.upRight);
        visibleRects.push(split.downLeft);
        split.downRight.topLeft.y = lineOfSightRect.topLeft.y;
        split.downRight.topRight.y = lineOfSightRect.topLeft.y;
        visibleRects.push(split.downRight);
      }

      if( isPointInRectangle(visibleRect, lineOfSightRect.topRight)
        && isPointInRectangle(visibleRect, lineOfSightRect.bottomRight)
      ) {
        const split = divideRectangle(visibleRect, lineOfSightRect.topRight);
        visibleRects.push(split.upLeft);
        visibleRects.push(split.upRight);
        visibleRects.push(split.downRight);
        split.downLeft.topLeft.y = lineOfSightRect.topLeft.y;
        split.downLeft.topRight.y = lineOfSightRect.topLeft.y;
        visibleRects.push(split.downLeft);
      }

      if( isPointInRectangle(visibleRect, lineOfSightRect.bottomLeft)
        && isPointInRectangle(visibleRect, lineOfSightRect.bottomRight)
      ) {
        const split = divideRectangle(visibleRect, lineOfSightRect.bottomRight);
        visibleRects.push(split.upLeft);
        visibleRects.push(split.downLeft);
        visibleRects.push(split.downRight);
        split.upRight.bottomLeft.x = lineOfSightRect.topLeft.x;
        split.upRight.bottomRight.x = lineOfSightRect.topLeft.x;
        visibleRects.push(split.upRight);
      }

      if( isPointInRectangle(visibleRect, lineOfSightRect.topLeft) ) {
        const split = divideRectangle(visibleRect, lineOfSightRect.topLeft);
        visibleRects.push(split.upLeft);
        visibleRects.push(split.upRight);
        visibleRects.push(split.downLeft);
      }

      if( isPointInRectangle(visibleRect, lineOfSightRect.topRight) ) {
        const split = divideRectangle(visibleRect, lineOfSightRect.topRight);
        visibleRects.push(split.upLeft);
        visibleRects.push(split.upRight);
        visibleRects.push(split.downRight);
      }

      if( isPointInRectangle(visibleRect, lineOfSightRect.bottomRight) ) {
        const split = divideRectangle(visibleRect, lineOfSightRect.bottomRight);
        visibleRects.push(split.upRight);
        visibleRects.push(split.downLeft);
        visibleRects.push(split.downRight);
      }

      if( isPointInRectangle(visibleRect, lineOfSightRect.bottomLeft) ) {
        const split = divideRectangle(visibleRect, lineOfSightRect.bottomLeft);
        visibleRects.push(split.upLeft);
        visibleRects.push(split.downLeft);
        visibleRects.push(split.downRight);
      }

      const lineOfSightTop = rectTop(lineOfSightRect);
      const lineOfSightBottom = rectBottom(lineOfSightRect);
      const lineOfSightLeft = rectLeft(lineOfSightRect);
      const lineOfSightRight = rectRight(lineOfSightRect);
      const visibleTop = rectTop(visibleRect);
      const visibleBottom = rectBottom(visibleRect);
      const visibleLeft = rectLeft(visibleRect);
      const visibleRight = rectRight(visibleRect);
      if( visibleTop > lineOfSightTop && visibleBottom > lineOfSightTop ) {
        visibleRect.bottomLeft.y = lineOfSightTop;
        visibleRect.bottomRight.y = lineOfSightTop;
      }

      if( visibleBottom < lineOfSightBottom && visibleTop > lineOfSightBottom ) {
        visibleRect.topLeft.y = lineOfSightBottom;
        visibleRect.topRight.y = lineOfSightBottom;
      }

      if( visibleLeft < lineOfSightLeft && visibleRight > lineOfSightLeft ) {
        visibleRect.topRight.x = lineOfSightLeft;
        visibleRect.bottomRight.x = lineOfSightLeft;
      }

      if( visibleRight > lineOfSightRight && visibleLeft > lineOfSightRight ) {
        visibleRect.topLeft.x = lineOfSightRight;
        visibleRect.bottomLeft.x = lineOfSightRight;
      }
    }
  }

  // if there is any rectangle left in target array, we have direct visibility to at least part of the target rectangle.
  return visibleRects.length > 0;
}

export function sizeFromVolume(volume: number): Size3 {
  // 1 liter = 1 dm^3, thus divide by 10 to get the size
  return {
    x: volume / 10,
    y: volume / 10,
    z: volume / 10,
  };
}

export function getDirectDistance(first: ent.Entity, second: ent.Entity): number | undefined {
  if( !Attribute.applies(first, attrs.SpatialLocation) || !Attribute.applies(second, attrs.SpatialLocation)) {
    return -1;
  }

  const locationFirst = getSpatialLocation(first);
  const locationSecond = getSpatialLocation(second);
  if( locationFirst != null && locationSecond != null ) {
    return magnitudeVect3(subVect3(locationFirst, locationSecond));
  }

  return undefined;
}

export function canActorReachTopSurface(actor: ent.Entity, target: ent.StatelessEntity, reachCoef: number): result.ResultObject {
  // check if there is enough spatial space under the target
  if( attrs.Attribute.applies(actor, attrs.SpatialSize) && attrs.Attribute.applies(target, attrs.SpatialLocation)) {
    const sizeActor = Attribute.value<Vector3>(actor, attrs.SpatialSize);
    const locationTarget = Attribute.value<Vector3>(target, attrs.SpatialLocation);

    let targetSceneLocation = Zero3;
    const targetScene = getCurrentScene(target);
    if( targetScene != null ) {
      targetSceneLocation = Attribute.value<Vector3>(targetScene, attrs.SpatialLocation);
    }

    let sizeTarget = Zero3;
    if( attrs.Attribute.applies(target, attrs.SpatialSize) ) {
      sizeTarget = Attribute.value<Vector3>(target, attrs.SpatialSize);
    }
    const targetTopZ = (locationTarget.z - targetSceneLocation.z) + sizeTarget.z / 2;

    const actorReachZ = sizeActor.z * (1 + reachCoef);

    if( actorReachZ >= targetTopZ ) {
      return result.make(result.accepted);
    }
  }

  return result.make(result.rejected, `Actor ${actor.id} can't reach top of entity ${target.id}.`);
}

export function doesEntityFitUnder(candidate: ent.Entity, target: ent.StatelessEntity, tolerance: number): result.ResultObject {
  // check if there is enough spatial space under the target
  if( attrs.Attribute.applies(candidate, attrs.SpatialSize) && attrs.Attribute.applies(target, attrs.SpatialLocation)) {
    const sizeCandidate = Attribute.value<Vector3>(candidate, attrs.SpatialSize);
    const locationTarget = Attribute.value<Vector3>(target, attrs.SpatialLocation);

    let targetSceneLocation = Zero3;
    const targetScene = getCurrentScene(target);
    if( targetScene != null ) {
      targetSceneLocation = Attribute.value<Vector3>(targetScene, attrs.SpatialLocation);
    }

    let sizeTarget = Zero3;
    if( attrs.Attribute.applies(target, attrs.SpatialSize) ) {
      sizeTarget = Attribute.value<Vector3>(target, attrs.SpatialSize);
    }
    const targetBottomZ = (locationTarget.z - targetSceneLocation.z) - sizeTarget.z / 2;

    let objectElasticity: Vector3 = Zero3;
    let objectCompressibility: Vector3 = Zero3;
    if( attrs.Attribute.applies(candidate, attrs.Elasticity) ) {
      objectElasticity = attrs.Attribute.value<Vector3>(candidate, attrs.Elasticity);
    }
    if( attrs.Attribute.applies(candidate, attrs.Compressibility) ) {
      objectCompressibility = attrs.Attribute.value<Vector3>(candidate, attrs.Compressibility);
    }

    const candidateSizeZ = sizeCandidate.z * (1  - objectCompressibility.z) * (1 - objectElasticity.z);

    if( (1 + tolerance) * candidateSizeZ <= targetBottomZ ) {
      return result.make(result.accepted);
    }
  }

  return result.make(result.rejected, `Entity ${candidate.id} doesn't fit under entity ${target.id} because there is not enough space.`);
}

export function doesEntityFitIntoContainer(candidate: ent.Entity, target: ent.StatelessEntity, tolerance: number): result.ResultObject {
  if( !attrs.Attribute.applies(target, attrs.IsContainer) || !attrs.Attribute.value<boolean>(target, attrs.IsContainer)) {
    // no localized reason here, we reject the skill in preview
    return result.make(result.rejected, `Entity ${candidate.id} doesn't fit into entity ${target.id} because ${target.id} is not a container.`);
  }

  let doesFit = true;
  if( attrs.Attribute.applies(target, attrs.IsClosed) ) {
    doesFit = !attrs.Attribute.value(target, attrs.IsClosed);
  }

  if( doesFit
    && (attrs.Attribute.applies(candidate, attrs.SpatialSize) || attrs.Attribute.applies(candidate, attrs.Volume))
    && attrs.Attribute.applies(target, attrs.SpatialSize)
  ) {
    // TODO: at some point we should be able to add stuff to container with liquid including pushing liquid out of the container,
    // in which case we need way more complex rules here
    let sizeDirect: Vector3 = Zero3;
    if( attrs.Attribute.applies(candidate, attrs.Volume) ) {
      const volumeDirect = attrs.Attribute.value<attrs.NumericValueAttribute>(candidate, attrs.Volume);
      sizeDirect = sizeFromVolume(volumeDirect.value);
    } else if( attrs.Attribute.applies(candidate, attrs.SpatialSize) ) {
      sizeDirect = attrs.Attribute.value<Vector3>(candidate, attrs.SpatialSize);
    }
    const sizeIndirect: Vector3 = attrs.Attribute.value<Vector3>(target, attrs.SpatialSize);
    let containerElasticity: Vector3 | undefined = undefined;
    let containerExpandibility: Vector3 | undefined = undefined;
    let objectElasticity: Vector3 | undefined = undefined;
    let objectCompressibility: Vector3 | undefined = undefined;
    if( attrs.Attribute.applies(target, attrs.Elasticity) ) {
      containerElasticity = attrs.Attribute.value<Vector3>(target, attrs.Elasticity);
    }
    if( attrs.Attribute.applies(target, attrs.Expandability) ) {
      containerExpandibility = attrs.Attribute.value<Vector3>(target, attrs.Expandability);
    }
    if( attrs.Attribute.applies(candidate, attrs.Elasticity) ) {
      objectElasticity = attrs.Attribute.value<Vector3>(candidate, attrs.Elasticity);
    }
    if( attrs.Attribute.applies(candidate, attrs.Compressibility) ) {
      objectCompressibility = attrs.Attribute.value<Vector3>(candidate, attrs.Compressibility);
    }

    let volumeAlreadyUsed = 0;
    if( attrs.Attribute.applies(target, attrs.ContainerCapacityUsed) ) {
      volumeAlreadyUsed = attrs.Attribute.value(target, attrs.ContainerCapacityUsed);
    }
    doesFit = Size3.fits(sizeIndirect, sizeDirect, volumeAlreadyUsed, {containerElasticity, containerExpandibility, objectElasticity, objectCompressibility}, tolerance);
  }

  const reasonResource = ResourceDictionary.get(candidate.game, DefaultResDict.MOVE_REJECTED_TOO_BIG);
  const objectNoun = lang.Noun.get(candidate.game, ResourceDictionary.get(candidate.game, attrs.Attribute.value(candidate, attrs.NounResourceId, candidate)));
  return doesFit ? result.make(result.accepted) : result.make(result.rejected, lang.compose(reasonResource.template, {
    directNoun: objectNoun.noun(lang.Case.nominative),
    preposition: lang.Preposition.get(candidate.game, ResourceDictionary.get(candidate.game, SpatialPrepositions.isIn.prepositionActionResource)).preposition(),
    indirectNoun: lang.Noun.get(candidate.game, ResourceDictionary.get(candidate.game, attrs.Attribute.value(target, attrs.NounResourceId, candidate))).noun(lang.Case.nominative),
    beVerb: lang.Verb.get(candidate.game, reasonResource.beVerbId).verb(lang.Number.singular, lang.Tense.present, lang.Person.third, objectNoun.gender, objectNoun.genderSubtype),
    bigAdjective: lang.Adjective.get(candidate.game, reasonResource.bigAdjectiveId).adjective(lang.Number.singular, lang.Case.nominative, objectNoun.gender, objectNoun.genderSubtype),
  }));
}

export function volumeAvailableForSubstance(target: ent.StatelessEntity): result.ResultObject {
  if( !attrs.Attribute.applies(target, attrs.IsContainer) || !attrs.Attribute.value<boolean>(target, attrs.IsContainer)) {
    return result.make(result.rejected, `There is no volume available in entity ${target.id} because it is not a container.`);
  }

  if( attrs.Attribute.applies(target, attrs.IsClosed) && attrs.Attribute.value<boolean>(target, attrs.IsClosed) ) {
    return result.make(result.rejected, `There is no volume available in container ${target.id} because the it is closed.`);
  }

  let availableVolume = 0;
  if( attrs.Attribute.applies(target, attrs.SpatialSize) ) {
    const containerSize: Vector3 = attrs.Attribute.value<Vector3>(target, attrs.SpatialSize);
    let containerExpandibility: Vector3 = Zero3;
    if( attrs.Attribute.applies(target, attrs.Expandability) ) {
      containerExpandibility = attrs.Attribute.value<Vector3>(target, attrs.Expandability);
    }

    let volumeAlreadyUsed = 0;
    if( attrs.Attribute.applies(target, attrs.ContainerCapacityUsed) ) {
      volumeAlreadyUsed = attrs.Attribute.value(target, attrs.ContainerCapacityUsed);
    }

    availableVolume = Size3.volume({
      x: containerSize.x * (1  + containerExpandibility.x),
      y: containerSize.y * (1  + containerExpandibility.y),
      z: containerSize.z * (1  + containerExpandibility.z),
    }) - volumeAlreadyUsed;
  }

  return result.make(result.accepted, <AvailableVolume>{ volume: availableVolume });
}

export class Size3 implements Vector3 {
  x: number;
  y: number;
  z: number;

  constructor(x: number, y: number, z: number) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  public static volume(size: Vector3): number {
    return (size.x || 0) * (size.y || 0) * (size.z || 0);
  }

  public static compare(first: Vector3, second: Vector3): number {
    const xDiff = first.x - second.x;
    const yDiff = first.y - second.y;
    const zDiff = first.z - second.z;

    // same size reuturns 0
    if( xDiff === 0 && yDiff === 0 && zDiff === 0 ) {
      return 0;
    }
    // if first is smaller in any dimension, return -1
    if( xDiff < 0 || yDiff < 0 || zDiff < 0 ) {
      return -1;
    }
    // otherwise return 1 (it must be bigger in at least one dimension)
    return 1;
  }

  public static fits(
    containerSize: Vector3,
    objectSize: Vector3,
    volumeAlreadyUsed: number = 0,
    {containerElasticity = Zero3, containerExpandibility = Zero3, objectElasticity = Zero3, objectCompressibility = Zero3}: {containerElasticity?: Vector3, containerExpandibility?: Vector3, objectElasticity?: Vector3, objectCompressibility?: Vector3},
    tolerance: number = 0
  ): boolean {
    if( volumeAlreadyUsed === 0 && Size3.compare(containerSize, objectSize) >= 0 ) {
      // container is bigger than object, it fits
      return true;
    }

    // now consider elasticity and expandability/compressibility
    // compressibility means the size can be shrinked by the coeficient
    // expandability means the size can be enlarged by the coeficient
    // elasticity means that while keeping the same volume, the size can be moved to other dimension of the object
    // used volume is total volume of things already in the container. we do not consider dimensions, just the volume.

    // let's calculate expanded/compressed volumes
    const volumeContainer = Size3.volume({
      x: containerSize.x * (1  + containerExpandibility.x),
      y: containerSize.y * (1  + containerExpandibility.y),
      z: containerSize.z * (1  + containerExpandibility.z),
    }) - volumeAlreadyUsed;
    const volumeObject = Size3.volume({
      x: objectSize.x * (1  - objectCompressibility.x),
      y: objectSize.y * (1  - objectCompressibility.y),
      z: objectSize.z * (1  - objectCompressibility.z),
    });

    if( (1 + tolerance) * volumeContainer < volumeObject ) {
      // container is too small to fit the object
      return false;
    }

    // now check whether elasticity of dimensions allows to fit object in the container
    // elasticity 0 for object means it is absolutelly rigid, elasticity 1 means it can shrink/expand infinitely in that direction
    // container elasticity translates to expanding in given dimension, we limit it to 1000 times
    const elasticityLimit = 0.001;
    const containerElasticSize: Vector3 = {
      x: (1 + tolerance) * containerSize.x * (1 / ((1 - containerElasticity.x) ?? elasticityLimit)),
      y: (1 + tolerance) * containerSize.y * (1 / ((1 - containerElasticity.y) ?? elasticityLimit)),
      z: (1 + tolerance) * containerSize.z * (1 / ((1 - containerElasticity.z) ?? elasticityLimit)),
    };
    // object elasticity translates to shrinking in given dimension
    const objectElasticSize: Vector3 = {
      x: objectSize.x * ((1 - objectElasticity.x) ?? elasticityLimit),
      y: objectSize.y * ((1 - objectElasticity.y) ?? elasticityLimit),
      z: objectSize.z * ((1 - objectElasticity.z) ?? elasticityLimit),
    };
    // we do not simulate to which direction whe squeezed material will go, but we already know, that volumes are OK, so it can fit somehow
    if( Size3.compare(containerElasticSize, objectElasticSize) >= 0 ) {
      // max stretched container is bigger than max squeezed object, it fits
      return true;
    }

    return false;
  }
}

export class SpatialRelations {
  public static OBJECT_GROUP = 'spatialRelations';

  public static registrator(game: GameData, o: SpatialRelation) {
    game.state.spatialRelations.set(o.id, o);
    return o;
  }

  public static relationsForScene(game: GameData, sceneId: string): Array<SpatialRelation> {
    const relations: Array<SpatialRelation> = [];
    const scene = game.instances.scenes.get(sceneId);
    if( scene != null ) {
      const sceneItems = Array.from<ent.Entity, string>(
        scene.items.filter(i => ent.getIsVisible(i))
          .concat(scene.actors.filter(i => ent.getIsVisible(i)))
          .concat(scene.substances.filter(i => ent.getIsVisible(i))),
          i => i.id
      );

      const ground = scene.ground;
      if( ground != null ) {
        sceneItems.push(ground.id);
      }

      for(let relation of game.state.spatialRelations.values()) {
        if( sceneItems.includes(relation.directId) && sceneItems.includes(relation.indirectId) ) {
          relations.push(relation);
        }
      }
    }
    return relations;
  }

  public static relationsForDirect(entity: ent.StatelessEntity): Array<SpatialRelation> {
    const relations: Array<SpatialRelation> = [];
    for(let relation of entity.game.state.spatialRelations.values()) {
      if( relation.directId === entity.id ) {
        relations.push(relation);
      }
    }
    return relations;
  }

  public static relationsForIndirect(entity: ent.StatelessEntity): Array<SpatialRelation> {
    const relations: Array<SpatialRelation> = [];
    for(let relation of entity.game.state.spatialRelations.values()) {
      if( relation.indirectId === entity.id ) {
        relations.push(relation);
      }
    }
    return relations;
  }

  public static addRelation(game: GameData, typeId: string, directId: string, indirectId: string) {
    const spatialRelation = ent.registerEntities(game,
      SpatialRelations.OBJECT_GROUP,
      {
        id: ent.getEntityId('SpatialRelation', `(${directId}:${indirectId})`),
        typeId: typeId,
        directId: directId,
        indirectId: indirectId,
      },
      SpatialRelations.registrator
    );

    game.bus.notifications.publish(broadcast(Topics.SPATIAL_RELATION_ADDED, {
      spatialRelationId: spatialRelation.id,
      directId: directId,
      indirectId: indirectId,
      typeId: typeId,
    }, undefined, directId));
  }

  static addFromProperties(target: ent.Entity, properties: SpatialRelationProperties) {
    if( properties != null && properties.spatialRelations != null ) {
      for(let [spatialRelationId, spatialRelation] of properties.spatialRelations) {
        SpatialRelations.addRelation(target.game, spatialRelation.typeId, target.id, spatialRelation.targetId);
      }
    }
  }

  public static removeRelationsOf(game: GameData, directId: string) {
    for(let relation of game.state.spatialRelations.values()) {
      if( relation.directId !== directId ) {
        continue;
      }
      const messageData = {
        directId: relation.directId,
        indirectId: relation.indirectId,
        typeId: relation.typeId,
      };
      game.state.spatialRelations.delete(relation.id);
      game.bus.notifications.publish(broadcast(Topics.SPATIAL_RELATION_REMOVED, messageData, undefined, directId));
    }
  }

  public static find(game: GameData, directId: string, indirectId: string, typeId: string): SpatialRelation | undefined {
    const relations = Array.from(game.state.spatialRelations.values());
    return relations.find(r => r.directId === directId && r.indirectId === indirectId && r.typeId === typeId);
  }

  public static findAll(game: GameData, match: {directId?: string, indirectId?: string, typeId?: string}): ReadonlyArray<SpatialRelation> {
    const relations = Array.from(game.state.spatialRelations.values());
    return relations.filter(r =>
      (match.directId == null || r.directId === match.directId)
      && (match.indirectId == null || r.indirectId === match.indirectId)
      && (match.typeId == null || r.typeId === match.typeId)
    );
  }

}

export interface SpatialRendererOptions {
}

export class SpatialRelationsRenderer {
  options: SpatialRendererOptions | undefined;
  game: GameData;

  constructor(rendererOptions?: SpatialRendererOptions) {
    this.options = rendererOptions;
  }

  initialize(game: GameData, spatialRelations?: SpatialRelation) {
    this.game = game;
    return this;
  }

  render(sceneId: string) {
    const relationsDescriptions: Array<string> = [];
    this.renderSpatialRelations(relationsDescriptions, sceneId);
    this.renderSmells(relationsDescriptions, sceneId);
    return relationsDescriptions;
  }

  getRelationMap(sceneId: string) {
    const scenesRelations = SpatialRelations.relationsForScene(this.game, sceneId);
    let itemsRelationalMap = new Map<string, Map<lang.Preposition, Array<string>>>();

    for(let relation of scenesRelations) {
      const targetId = relation.indirectId;
      let prepositionMap = itemsRelationalMap.get(targetId);
      if( null == prepositionMap ) {
        prepositionMap = new Map<lang.Preposition, Array<string>>();
        itemsRelationalMap.set(targetId, prepositionMap);
      }
      const relationType = SpatialPrepositions.allById().get(relation.typeId);
      if( relationType != null ) {
        const preposition = lang.Preposition.get(this.game, ResourceDictionary.get(this.game, relationType.prepositionDescriptionResource));
        let sourceArray = prepositionMap.get(preposition);
        if( null == sourceArray ) {
          sourceArray = [];
          prepositionMap.set(preposition, sourceArray);
        }
        sourceArray.push(relation.directId);
      }
    }

    return itemsRelationalMap;
  }

  getNoun(sourceId: string): lang.Noun | null {
    const entity = this.game.instances.getSpatialFromId(sourceId);
    if( entity != null ) {
      // first try get the Noun attr which returns resource ID
      if (attrs.Attribute.applies(entity, attrs.NounResourceId)) {
        const nounResourceId = attrs.Attribute.value<string>(entity, attrs.NounResourceId);
        const noun = lang.Noun.get(entity.game, ResourceDictionary.get(this.game, nounResourceId));
        return noun;
      }

      // no Noun attr set, try use title as noun directly
      if (attrs.Attribute.applies(entity, attrs.Title)) {
        let noun = lang.Noun.get(entity.game, attrs.Attribute.value<string>(entity, attrs.Title));
        return noun;
      }

      // nothing was set at all, so default to the entity ID
      return lang.Noun.get(entity.game, entity.id);
    } else {
      // no entity of such ID, return null to indicate that and default to <undefined> string
      return null;
    }
  }

  private renderSpatialRelations(relationsDescriptions: Array<string>, sceneId: string) {
    const map = this.getRelationMap(sceneId);
    for(let [targetId, prepMap] of map.entries()) {
     for(let [preposition, sourceIds] of prepMap.entries()) {
      let toBeGender: lang.Gender | undefined = undefined;
      let toBeGenderSubtype: lang.GenderSubtype | undefined = undefined;
      const sources: Array<{entity: ent.StatelessEntity, noun: lang.Noun}> = [];
      for(let sourceId of sourceIds) {
        const source = this.game.instances.getSpatialFromId(sourceId);
        if( source == null ) {
          continue;
        }
        const noun = this.getNoun(sourceId);
        if( noun == null ) {
          continue;
        }
        if( toBeGender == null || noun.gender === lang.Gender.masculine ) {
          toBeGender = noun.gender;
        }
        if( toBeGenderSubtype == null || noun.genderSubtype === lang.GenderSubtype.animate ) {
          toBeGenderSubtype = noun.genderSubtype;
        }
        sources.push({ entity: source, noun: noun });
      }

      const descriptionResource = ResourceDictionary.get(this.game, DefaultResDict.SPATIAL_RELATION_DESCRIPTION);
      const target = this.game.instances.getAnyFromId(targetId);
      const sourcesText = lang.mergeList(sources.map(s =>
        ent.getTitle(s.entity, lang.Case.nominative, lang.CapitalizationOptions.keep).title),
        descriptionResource.sourcesSeparator,
        descriptionResource.sourcesConjunction
      );

      const requiredCase = preposition.requiredCase();
      relationsDescriptions.push(
        lang.compose(
          descriptionResource.template,
          {
            sources: lang.capitalFirst(sourcesText),
            be: lang.Verb.get(this.game, descriptionResource.beId)
                    .verb(
                      (sources.length > 1) || (sources[0].noun.number === lang.Number.plural) ? lang.Number.plural : lang.Number.singular,
                      lang.Tense.past,
                      lang.Person.third,
                      toBeGender,
                      toBeGenderSubtype
                    ),
            preposition: preposition.preposition(),
            target: ent.getTitle(target, requiredCase != null ? requiredCase : lang.Case.nominative, lang.CapitalizationOptions.keep).title,
          }
        )
      );
      }
    }
  }

  private renderSmells(relationsDescriptions: Array<string>, sceneId: string) {
    const renderedScene = this.game.instances.getSceneFromId(sceneId);
    if( renderedScene == null ) {
      return;
    }

    // get all substances in current scene which are in gas phase and are smellable and are not in container
    // TODO: we should handle substance compounds here as well...
    const substances = renderedScene.substances.filter(s =>
      attrs.Attribute.applies(s, attrs.SubstancePhase) && attrs.Attribute.value<SubstancePhases>(s, attrs.SubstancePhase) === SubstancePhases.gas
      && attrs.Attribute.applies(s, attrs.IsSmellable) && attrs.Attribute.value<boolean>(s, attrs.IsSmellable)
      && attrs.Attribute.applies(s, attrs.ContainerId) && attrs.Attribute.value<string>(s, attrs.ContainerId) == null
    );
    for(let substance of substances) {
      let toBeGender: lang.Gender | undefined = undefined;
      let toBeGenderSubtype: lang.GenderSubtype | undefined = undefined;
      const smellySubstances: Array<lang.Noun> = [];
      let noun = this.getNoun(substance.id);
      if( noun == null ) {
        continue;
      }
      if( toBeGender == null || noun.gender === lang.Gender.masculine ) {
        toBeGender = noun.gender;
      }
      if( toBeGenderSubtype == null || noun.genderSubtype === lang.GenderSubtype.animate ) {
        toBeGenderSubtype = noun.genderSubtype;
      }
      smellySubstances.push(noun);

      const descriptionResource = ResourceDictionary.get(this.game, DefaultResDict.SMELLS_DESCRIPTION);
      const smellCase = descriptionResource.smellCase;
      const smellsText = lang.mergeList(smellySubstances.map(s => s.noun(smellCase)), descriptionResource.smellsSeparator, descriptionResource.smellsConjunction);

      relationsDescriptions.push(
        lang.compose(
          descriptionResource.template,
          {
            substances: smellsText,
            beVerb: lang.Verb.get(this.game, descriptionResource.beVerbId)
                    .verb(
                      (smellySubstances.length > 1) || (smellySubstances[0].number === lang.Number.plural) ? lang.Number.plural : lang.Number.singular,
                      lang.Tense.past,
                      lang.Person.third,
                      toBeGender,
                      toBeGenderSubtype
                    ),
            smellNoun: descriptionResource.smellNounId != null ? lang.Noun.get(this.game, descriptionResource.smellNounId).noun(descriptionResource.smellNounCase) : null,
            smellVerb: descriptionResource.smellVerbId != null ? lang.Verb.get(this.game, descriptionResource.smellVerbId).verb() : null
          }
        )
      );
    }
  }
}