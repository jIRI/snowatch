import {Entity, HasId, getHolderId, StatelessEntity} from 'snowatch/entity';
import {broadcast} from 'snowatch/message';
import {Topics} from 'snowatch/message-topics/all';
import * as item from 'snowatch/item';

export interface InventoryProperties {
      inventory?: Map<string, InventoryInit>;
      inventories?: Array<InventoriesInit>;
}

export interface InventoriesInit {
  id: string;
  items: Map<string, InventoryInit>;
}

export interface InventoryInit extends HasId {
}

export class Inventory {
  static initializeInventory(target: Entity, inventoryId?: string) {
    inventoryId = inventoryId ?? `${target.id}-default`;
    target.logger.debug(`${target.id}: initializeInventory('${inventoryId}')`);
    target.state.inventories = target.state.inventories ?? new Map<string, Set<string>>();
    target.state.inventories.set(inventoryId, new Set<string>());
    target.state.defaultInventory = target.state.defaultInventory ?? inventoryId;
  }

  static getIds(target: Entity): Array<string> {
    return Array.from<string>(target.state.inventories.keys());
  }

  static getItemsIds(target: Entity, inventoryId?: string): Array<string> {
    inventoryId = inventoryId ?? target.state.defaultInventory;
    return Array.from<string>(target.state.inventories.get(inventoryId).values());
  }

  static filter(target: Entity, filterFn: (item: Entity) => boolean, inventoryId?: string): Array<StatelessEntity> {
    inventoryId = inventoryId ?? target.state.defaultInventory;
    return Array.from<string>(target.state.inventories.get(inventoryId).values())
      .map(id => target.game.instances.getAnyFromId(id))
      .filter(filterFn);
  }

  static addItem(target: Entity, itemId: string, inventoryId?: string) {
    inventoryId = inventoryId ?? target.state.defaultInventory;
    if( !target.state.inventories.get(inventoryId).has(itemId) ) {
      target.logger.debug(`Adding item ${itemId} to invetory ${inventoryId}`);
      target.state.inventories.get(inventoryId).add(itemId);
      target.game.bus.notifications.publish(broadcast(Topics.ENTITY_ADDED_TO_INVENTORY, { inventoryOwnerId: target.id, inventoryId: inventoryId, entityId: itemId }));
    }
  }

  static addFromProperties(target: Entity, properties: InventoryProperties, inventoryId?: string) {
    if( properties != null && properties.inventory != null ) {
      for(let [itemId, item] of properties.inventory) {
        Inventory.addItem(target, itemId);
      }
    }
    if( properties != null && properties.inventories != null ) {
      for(let inventory of properties.inventories) {
        for(let [itemId, item] of inventory.items) {
          Inventory.addItem(target, itemId, inventory.id);
        }
      }
    }
  }

  static removeItem(target: Entity, itemId: string, inventoryId?: string) {
    inventoryId = inventoryId ?? target.state.defaultInventory;
    target.logger.debug(`Removing ${itemId} from invetory ${inventoryId}`);
    target.state.inventories.get(inventoryId).delete(itemId);
    target.game.bus.notifications.publish(broadcast(Topics.ENTITY_REMOVED_FROM_INVENTORY, { inventoryOwnerId: target.id, inventoryId: inventoryId, entityId: itemId }));
  }

  static removeAllItems(target: Entity, inventoryId?: string) {
    inventoryId = inventoryId ?? target.state.defaultInventory;
    target.logger.debug(`Clearing inventory ${inventoryId}`);
    const items = Array.from<string>(target.state.inventories.get(inventoryId));
    for(let itemId of items) {
      Inventory.removeItem(target, itemId);
    }
  }

  static removeAllItemsFromAll(target: Entity) {
    for(let inventoryId of Inventory.getIds(target)) {
      Inventory.removeAllItems(target, inventoryId);
    }
  }

  static hasInAny(target: Entity, itemId: string) {
    for(let inventory of target.state.inventories.values()) {
      if( inventory.has(itemId) ) {
        return true;
      }
    }
    return false;
  }

  static isInAny(target: Entity) {
    const holderId = getHolderId(target);
    return holderId != null;
  }
}
