import { GameData, StateModel } from 'snowatch/game-data';
import { Actor } from 'snowatch/actor';
import { broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { DefaultResDict } from 'snowatch/resources/default';
import * as ent from 'snowatch/entity';
import * as result from 'snowatch/result';
import * as lang from 'snowatch/language-structures';
import { ResourceDictionary } from './snowatch';

export interface SkillProperties {
  skills: Map<string, SkillInit>;
}

export interface SkillInit extends ent.HasId {
  initialState?: any;
}

export interface SkillFilterFn {
  (game: GameData, state: any, config: SkillConfig, previewData: PreviewData): result.ResultObject;
}

export interface SkillModifier {
  id: string;
  modifierResource: string;
  details: any;
}

export interface EntityPreposition {
  id: string;
  prepositionResource: string;
}

export interface PrepositionedEntity {
  preposition: EntityPreposition | null | undefined;
  entity: ent.StatelessEntity;
}

export interface PreviewData {
  directPrepositionId?: string;
  modifiers: Array<SkillModifier>;
  requiredIndirectsCount: number;
  requiredInstrumentsCount: number;
  instruments: Array<PrepositionedEntity>;
  indirects: Array<PrepositionedEntity>;
}

export interface SkillConfig {
  actor: Actor;
  direct: ent.Entity;
  modifierId?: string;
  prepositionId?: string;
  instruments: Array<ent.StatelessEntity>;
  indirects: Array<ent.StatelessEntity>;
  details?: any;
}

export class SkillModifiers {
  public static readonly neutrally: SkillModifier = { id: 'neutrally', modifierResource: DefaultResDict.NEUTRALLY, details: {} };
}

export class Skill extends ent.StatelessEntity {
  static ensureIsInstanceOf(obj: Skill | SkillInit): Skill {
    if (obj instanceof Skill) {
      return obj;
    }
    return new Skill(obj);
  }

  public static OBJECT_GROUP = 'skills';

  static registrator(game: GameData, o: Skill | SkillInit): Skill {
    const obj = Skill.ensureIsInstanceOf(o);
    return obj.initialize(game);
  }

  static initializeSkills(target: ent.Entity) {
    target.logger.debug(`${target.id}: initializeSkills()`);
    target.state.skills = target.state.skills ?? new Map<string, any>();
  }

  static getIds(target: ent.Entity): Array<string> {
    return Array.from(target.state.skills.keys());
  }

  static add(target: ent.Entity, skillId: string, skill?: SkillInit) {
    if (!target.state.skills.has(skillId)) {
      target.logger.debug(`Adding skill ${skillId} (${skill})`);
      const initialState = (skill != null) ? (skill.initialState ?? {}) : {};
      target.state.skills.set(skillId, initialState ?? {});
      target.game.bus.notifications.publish(broadcast(Topics.SKILL_ADDED, { entityId: target.id, skillId: skillId }));
    }
  }

  static addFromProperties(target: ent.Entity, properties: SkillProperties) {
    if (properties != null && properties.skills != null) {
      for (let [skillId, skill] of properties.skills) {
        Skill.add(target, skillId, skill);
      }
    }
  }

  static remove(target: ent.Entity, skillId: string) {
    target.logger.debug(`Removing skill ${skillId}`);
    target.state.skills.delete(skillId);
    target.game.bus.notifications.publish(broadcast(Topics.SKILL_REMOVED, { entityId: target.id, skillId: skillId }));
  }

  static removeAll(target: ent.Entity) {
    target.logger.debug(`Clearing all skills`);
    const skills = Array.from<string>(target.state.skills.keys());
    for (let skillId of skills) {
      Skill.remove(target, skillId);
    }
  }

  static has(target: ent.Entity | ent.StatelessEntity, skillId: string) {
    if (!(target instanceof ent.Entity)) {
      return false;
    }
    return (<ent.Entity>target).state.skills.has(skillId);
  }


  protected _instrumentPreposition: EntityPreposition;
  protected _prepositionsMap: Map<string, EntityPreposition>;
  protected _allPrepositions: EntityPreposition[];
  protected _modifiersMap: Map<string, SkillModifier>;
  protected _allModifiers: SkillModifier[];

  constructor(init: SkillInit | null = null) {
    super(init, Skill.OBJECT_GROUP);
  }

  initialize(game: GameData, init?: SkillInit) {
    super.initialize(game, init);

    this._modifiersMap = new Map<string, SkillModifier>();
    this.getAllModifiers().forEach(m => this._modifiersMap.set(m.id, m));

    this._prepositionsMap = new Map<string, EntityPreposition>();
    this.getAllPrepositions().forEach(p => this._prepositionsMap.set(p.id, p));

    this._instrumentPreposition = {
      id: 'instrument-with',
      prepositionResource: DefaultResDict.INSTRUMENT_WITH_PREPOSITION
    };

    return this;
  }

  get verb(): lang.Verb {
    return lang.Verb.get(this.game, this.init.title ?? this.init.id ?? this.constructor.name);
  }

  get isRealTime(): boolean {
    return false;
  }

  getPreviewResult(state: any, config: SkillConfig, previewData: PreviewData, filter: SkillFilterFn): result.ResultObject {
    if ((previewData.requiredIndirectsCount === -1 && previewData.indirects.length === 0)
      || (previewData.requiredIndirectsCount > 0 && previewData.indirects.length === 0)) {
      return result.make(result.rejected, 'Indirect object is needed but none matches skill requirements.');
    }

    if ((previewData.requiredInstrumentsCount === -1 && previewData.instruments.length === 0)
      || (previewData.requiredInstrumentsCount > 0 && previewData.instruments.length === 0)) {
      return result.make(result.rejected, 'Instrument is needed but none matches skill requirements.');
    }

    return filter != null ? filter(this.game, state, config, previewData) : result.make(result.accepted);
  }

  getAllModifiers(): Array<SkillModifier> {
    this._allModifiers = this._allModifiers ?? [];
    return this._allModifiers;
  }

  getAllPrepositions(): Array<EntityPreposition> {
    this._allPrepositions = this._allPrepositions ?? [];
    return this._allPrepositions;
  }

  getRequiredIndirectsCount(config: SkillConfig): number {
    return 0;
  }

  getRequiredInstrumentsCount(config: SkillConfig): number {
    return 0;
  }

  getDirectPrepositionId(): string | null {
    return null;
  }

  getInstruments(config: SkillConfig): Array<PrepositionedEntity> {
    const instruments = (config.instruments ?? [])
      .filter((obj: ent.StatelessEntity, i, a) => obj !== config.direct)
      .map((obj: ent.StatelessEntity, i, a) =>  [{ preposition: this._instrumentPreposition, entity: obj }]);
    return [].concat.apply([], instruments);
  }

  getIndirects(config: SkillConfig): Array<PrepositionedEntity> {
    const indirects = (config.indirects ?? [])
      .filter((obj: ent.StatelessEntity, i, a) => obj !== config.direct)
      .map((obj: ent.StatelessEntity, i, a) => this.getPrepositionedEntities(obj, config));
    return [].concat.apply([], indirects);
  }

  getPrepositionedEntities(entity: ent.StatelessEntity, config: SkillConfig): Array<PrepositionedEntity> {
    return [{ preposition: null, entity: entity }];
  }

  getApplicableModifiers(config: SkillConfig): SkillModifier[] {
    return this.getAllModifiers();
  }

  preparePreviewData(result: string, config: SkillConfig): PreviewData {
    return {
      directPrepositionId: this.getDirectPrepositionId() ?? undefined,
      modifiers: this.getApplicableModifiers(config),
      requiredIndirectsCount: this.getRequiredIndirectsCount(config),
      requiredInstrumentsCount: this.getRequiredInstrumentsCount(config),
      instruments: this.getInstruments(config),
      indirects: this.getIndirects(config),
    };
  }

  preview(state: any, config: SkillConfig, filter: SkillFilterFn): result.ResultObject {
    const previewData = this.preparePreviewData(result.accepted, config);
    const previewResult = this.getPreviewResult(state, config, previewData, filter);
    return result.make(previewResult.result, previewData, previewResult.message);
  }

  async execute(state: any, config: SkillConfig, filter: SkillFilterFn): Promise<result.ResultObject> {
    return result.make(result.ok);
  }

  static preview(game: GameData, skillId: string, state: any, config: SkillConfig, filter: SkillFilterFn): result.ResultObject {
    if (!Skill.has(config.actor, skillId)) {
      return result.make(result.rejected, `Skill ${skillId} is not supported by actor ${config.actor.id}.`);
    }

    const skill = game.instances.skills.get(skillId);
    if (skill != null) {
      return skill.preview(state, config, filter);
    } else {
      return result.make(result.rejected, `Unknown skill ${skillId}.`);
    }
  }

  static async use(game: GameData, skillId: string, state: any, config: SkillConfig, filter: SkillFilterFn) {
    const skill = game.instances.skills.get(skillId);
    if (skill != null) {
      return await skill.execute(state, config, filter);
    } else {
      return result.make(result.rejected, `Unknown skill ${skillId}.`);
    }
  }

  static async handleTryUseSkill(game: GameData, requestId: number, payload: any) {
    // validate
    const actor = game.instances.actors.get(payload.actorId);
    if (actor == null) {
      game.bus.notifications.publish(broadcast(Topics.SKILL_REJECTED, payload, requestId));
      return;
    }
    const direct = game.instances.getEntityFromId(payload.directId);
    if (direct == null) {
      game.bus.notifications.publish(broadcast(Topics.SKILL_REJECTED, payload, requestId, actor.id));
      return;
    }
    const indirects: Array<ent.StatelessEntity> = [];
    for (let indirectId of payload.indirectsIds ?? []) {
      const indirect = game.instances.getEntityFromId(indirectId) ?? game.instances.getStatelessEntityFromId(indirectId);
      if (indirect != null) {
        indirects.push(indirect);
      }
    }
    const instruments: Array<ent.StatelessEntity> = [];
    if (payload.instrumentId != null) {
      const instrument = game.instances.getEntityFromId(payload.instrumentId) ?? game.instances.getStatelessEntityFromId(payload.instrumentId);
      if (instrument != null) {
        instruments.push(instrument);
      }
    }

    // execute
    const config: SkillConfig = {
      actor: actor,
      direct: direct,
      modifierId: payload.modifierId,
      prepositionId: payload.indirectsPrepositionId,
      indirects: indirects,
      instruments: instruments,
      details: payload.details,
    };

    const state = ent.getState(config.actor, Skill.OBJECT_GROUP, payload.skillId);
    const filterPreview = ent.getProperties(config.actor, Skill.OBJECT_GROUP, payload.skillId).filterPreview;
    const previewResult = Skill.preview(game, payload.skillId, state, config, filterPreview);
    const previewData = {
      ...payload,
      ...previewResult.data,
    };

    // do actual validation here
    if (!result.isAccepted(previewResult)
      || (payload.modifier != null && !previewData.modifiers.includes(payload.modifier))
      || (previewData.requiredIndirectsCount !== 0
        && (payload.indirectsIds == null || !previewData.indirects.some((i: PrepositionedEntity) => payload.indirectsIds.includes(i.entity.id))))
      || (previewData.requiredInstrumentsCount !== 0
        && (payload.instrumentId == null || !previewData.instruments.some((i: PrepositionedEntity) => payload.instrumentId === i.entity.id)))
    ) {
      payload.previewData = previewData;
      payload.message = `Invalid skill configuration: ${previewResult.message}`;
      game.bus.notifications.publish(broadcast(Topics.SKILL_REJECTED, payload, requestId, config.actor.id));
      return;
    }

    game.bus.notifications.publish(broadcast(Topics.TRYING_USE_SKILL, previewData));

    const filterExec = ent.getProperties(config.actor, Skill.OBJECT_GROUP, payload.skillId).filterExec;
    const useResult = await Skill.use(game, payload.skillId, state, config, filterExec);
    if (result.isAccepted(useResult)) {
      game.bus.notifications.publish(broadcast(Topics.SKILL_ACCEPTED, previewData, requestId, config.actor.id));
    } else {
      payload.message = useResult.message ?? useResult.data.message;
      game.bus.notifications.publish(broadcast(Topics.SKILL_REJECTED, payload, requestId, config.actor.id));
    }
  }
}
