import {Topic} from 'snowatch/message-topic';
import {GameData} from 'snowatch/game-data';
import {Vector3} from 'snowatch/spatial-relations-base-types';
import {SkillModifier, PrepositionedEntity} from 'snowatch/skill';
import { Preposition } from 'snowatch/language-structures';


export namespace Topics {
  export type ANY =
    | START
    | RESET_STATE
    | RESET_GAME
    | LOAD_STATE
    | INITIALIZE_COMPONENTS
    | CLOCK_TICK
    | TRY_EXIT
    | EXIT_ACCEPTED
    | EXIT_REJECTED
    | SCENE_ENTERED
    | SCENE_LEFT
    | SKILL_ADDED
    | SKILL_REMOVED
    | TRY_USE_SKILL
    | TRYING_USE_SKILL
    | SKILL_ACCEPTED
    | SKILL_REJECTED
    | KNOWLEDGE_ADDED
    | KNOWLEDGE_REMOVED
    | ATTRIBUTE_ADDED
    | ATTRIBUTE_REMOVED
    | ATTRIBUTE_VALUE_CHANGED
    | ATTRIBUTE_APPLICABILITY_CHANGED
    | BEHAVIOR_ADDED
    | BEHAVIOR_REMOVED
    | BEHAVIOR_ENABLED
    | BEHAVIOR_DISABLED
    | POSSESSION_ADDED
    | POSSESSION_REMOVED
    | DIALOG_LINE_ADDED
    | DIALOG_LINE_REMOVED
    | ENTITY_ADDED_TO_INVENTORY
    | ENTITY_REMOVED_FROM_INVENTORY
    | ENTITY_ADDED_TO_CONTAINER
    | ENTITY_REMOVED_FROM_CONTAINER
    | SPATIAL_RELATION_ADDED
    | SPATIAL_RELATION_REMOVED
    | ENTITY_SCENE_CHANGED
    | SUBSTANCE_VOLUME_DECREASED
    | SUBSTANCE_VOLUME_INCREASED
    | SUBSTANCE_VOLUME_RESTORED
    | DRUNK
    | POURED
    | SOLIDIFIED
    | MELTED
    | EVAPORATED
    | DISSIPATED
    | SOUND
    | SMELL
    | APPEARANCE_CHANGE
    | SPEECH
    | ENGAGING
    | ENGAGED
    | FIGHT_SEQUENCE_NEXT
    | PURSUE_STEP_NEXT
    | ATTACK_WINDING_UP
    | ATTACK_WINDUP
    | ATTACK_CANCELLING
    | ATTACK_CANCELLED
    | ATTACK_COOLING_DOWN
    | ATTACK_COOLDOWN
    | ATTACK_FINISHED
    | ATTACKED
    | STUNNED
    | STUN_RECOVERY
    | BLOCKING
    | BLOCKED
    | DODGING
    | DODGE
    | DODGED
    | EVADING
    | EVADED
    | TOOK_DAMAGE
    | DIED
    | RENDER_SCENE_DESCRIPTION
    | GAME_LOG_UPDATED
    | UI_REFRESH
  ;

  export type ATTACK_STAGE_CHANGES =
    | ATTACK_WINDING_UP
    | ATTACK_CANCELLING
    | ATTACK_COOLING_DOWN
    | ATTACK_LANDING
    | ATTACK_FINISHING
  ;


  export class START extends Topic {
    public static readonly ID = 'START';

    public game: GameData;

    public constructor(init?: Readonly<START>) {
      super();
      Object.assign(this, init);
    }
  }

  export class RESET_STATE extends Topic {
    public static readonly ID = 'RESET_STATE';

    public constructor(init?: Readonly<RESET_STATE>) {
      super();
      Object.assign(this, init);
    }
  }

  export class RESET_GAME extends Topic {
    public static readonly ID = 'RESET_GAME';

    public constructor(init?: Readonly<RESET_GAME>) {
      super();
      Object.assign(this, init);
    }
  }

  export class LOAD_STATE extends Topic {
    public static readonly ID = 'LOAD_STATE';

    public slotName: string;

    public constructor(init?: Readonly<LOAD_STATE>) {
      super();
      Object.assign(this, init);
    }
  }

  export class INITIALIZE_COMPONENTS extends Topic {
    public static readonly ID = 'INITIALIZE_COMPONENTS';

    public constructor(init?: Readonly<INITIALIZE_COMPONENTS>) {
      super();
      Object.assign(this, init);
    }
  }

  export class CLOCK_TICK extends Topic {
    public static readonly ID = 'CLOCK_TICK';

    public wallClockTicks: number;
    public clockScale: number;

    public constructor(init?: Readonly<CLOCK_TICK>) {
      super();
      Object.assign(this, init);
    }
  }

  export class TRY_EXIT extends Topic {
    public static readonly ID = 'TRY_EXIT';

    public entityId: string;
    public exitId: string;

    public constructor(init?: Readonly<TRY_EXIT>) {
      super();
      Object.assign(this, init);
    }
  }

  export class EXIT_ACCEPTED extends Topic {
    public static readonly ID = 'EXIT_ACCEPTED';

    public entityId: string;
    public exitId: string;
    public result?: any | null;
    public message?: string | null;
    public constructor(init?: Readonly<EXIT_ACCEPTED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class EXIT_REJECTED extends Topic {
    public static readonly ID = 'EXIT_REJECTED';

    public entityId: string;
    public exitId: string;
    public result?: any | null;
    public message?: string | null;

    public constructor(init?: Readonly<EXIT_REJECTED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class SCENE_ENTERED extends Topic {
    public static readonly ID = 'SCENE_ENTERED';

    public entityId: string;
    public sceneId: string;

    public constructor(init?: Readonly<SCENE_ENTERED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class SCENE_LEFT extends Topic {
    public static readonly ID = 'SCENE_LEFT';

    public entityId: string;
    public sceneId: string;

    public constructor(init?: Readonly<SCENE_LEFT>) {
      super();
      Object.assign(this, init);
    }
  }

  export class SKILL_ADDED extends Topic {
    public static readonly ID = 'SKILL_ADDED';

     public entityId: string;
     public skillId: string;

     public constructor(init?: Readonly<SKILL_ADDED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class SKILL_REMOVED extends Topic {
    public static readonly ID = 'SKILL_REMOVED';

    public entityId: string;
    public skillId: string;

    public constructor(init?: Readonly<SKILL_REMOVED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class TRY_USE_SKILL extends Topic {
    public static readonly ID = 'TRY_USE_SKILL';

    public skillId: string;
    public actorId: string;
    public directPrepositionId?: string;
    public directId: string;
    public modifierId: string | null;
    public details?: {
      responseTo: string;
      responseToModifier: string;
    };
    public instrumentPrepositionId?: string;
    public instrumentId?: string;
    public indirectsPrepositionId?: string;
    public indirectsIds: Array<string>;

    public constructor(init?: Readonly<TRY_USE_SKILL>) {
      super();
      Object.assign(this, init);
    }
  }

  export class TRYING_USE_SKILL extends Topic {
    public static readonly ID = 'TRYING_USE_SKILL';

    public skillId: string;
    public actorId: string;
    public directPrepositionId?: string;
    public directId: string;
    public modifierId?: string;
    public details?: {
      responseTo: string;
      responseToModifier: string;
    };
    public instrumentPrepositionId?: string;
    public instrumentId?: string;
    public indirectsPrepositionId?: string;
    public indirectsIds: Array<string>;

    public instruments?: Array<PrepositionedEntity>;
    public modifiers: Array<SkillModifier>;
    public requiredIndirectsCount: number;
    public requiredInstrumentsCount: number;
    public indirects: Array<PrepositionedEntity>;

    public constructor(init?: Readonly<SKILL_ACCEPTED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class SKILL_ACCEPTED extends Topic {
    public static readonly ID = 'SKILL_ACCEPTED';

    public skillId: string;
    public actorId: string;
    public directPrepositionId?: string;
    public directId: string;
    public modifierId?: string;
    public details?: {
      responseTo: string;
      responseToModifier: string;
    };
    public instrumentPrepositionId?: string;
    public instrumentId?: string;
    public indirectsPrepositionId?: string;
    public indirectsIds: Array<string>;

    public instruments?: Array<PrepositionedEntity>;
    public modifiers: Array<SkillModifier>;
    public requiredIndirectsCount: number;
    public indirects: Array<PrepositionedEntity>;

    public constructor(init?: Readonly<SKILL_ACCEPTED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class SKILL_REJECTED extends Topic {
    public static readonly ID = 'SKILL_REJECTED';

    public skillId: string;
    public entityId: string;
    public directPrepositionId?: string;
    public directId: string;
    public modifierId?: string;
    public details?: {
      responseTo: string;
      responseToModifier: string;
    };
    public instrumentPrepositionId?: string;
    public instrumentId?: string;
    public indirectsPrepositionId?: string;
    public indirectsIds: Array<string>;

    public constructor(init?: Readonly<SKILL_REJECTED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class KNOWLEDGE_ADDED extends Topic {
    public static readonly ID = 'KNOWLEDGE_ADDED';

    public entityId: string;
    public knowledgeId: string;

    public constructor(init?: Readonly<KNOWLEDGE_ADDED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class KNOWLEDGE_REMOVED extends Topic {
    public static readonly ID = 'KNOWLEDGE_REMOVED';

    public entityId: string;
    public knowledgeId: string;

    public constructor(init?: Readonly<KNOWLEDGE_REMOVED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class ATTRIBUTE_ADDED extends Topic {
    public static readonly ID = 'ATTRIBUTE_ADDED';

    public entityId: string;
    public attributeId: string;
    public attributeValue?: any;
    public computed?: boolean;

    public constructor(init?: Readonly<ATTRIBUTE_ADDED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class ATTRIBUTE_REMOVED extends Topic {
    public static readonly ID = 'ATTRIBUTE_REMOVED';

    public entityId: string;
    public attributeId: string;

    public constructor(init?: Readonly<ATTRIBUTE_REMOVED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class ATTRIBUTE_VALUE_CHANGED extends Topic {
    public static readonly ID = 'ATTRIBUTE_VALUE_CHANGED';

    public entityId: string;
    public attributeId: string;
    public oldValue: any;
    public newValue: any;

    public constructor(init?: Readonly<ATTRIBUTE_VALUE_CHANGED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class ATTRIBUTE_APPLICABILITY_CHANGED extends Topic {
    public static readonly ID = 'ATTRIBUTE_APPLICABILITY_CHANGED';

    public entityId: string;
    public attributeId: string;
    public oldValue: string;
    public newValue: string;

    public constructor(init?: Readonly<ATTRIBUTE_APPLICABILITY_CHANGED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class BEHAVIOR_ADDED extends Topic {
    public static readonly ID = 'BEHAVIOR_ADDED';

    public entityId: string;
    public behaviorId: string;

    public constructor(init?: Readonly<BEHAVIOR_ADDED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class BEHAVIOR_REMOVED extends Topic {
    public static readonly ID = 'BEHAVIOR_REMOVED';

    public entityId: string;
    public behaviorId: string;

    public constructor(init?: Readonly<BEHAVIOR_REMOVED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class BEHAVIOR_ENABLED extends Topic {
    public static readonly ID = 'BEHAVIOR_ENABLED';

    public entityId: string;
    public behaviorId: string;

    public constructor(init?: Readonly<BEHAVIOR_ENABLED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class BEHAVIOR_DISABLED extends Topic {
    public static readonly ID = 'BEHAVIOR_DISABLED';

    public entityId: string;
    public behaviorId: string;

    public constructor(init?: Readonly<BEHAVIOR_DISABLED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class POSSESSION_ADDED extends Topic {
    public static readonly ID = 'POSSESSION_ADDED';

    public entityId: string;
    public itemId: string;

    public constructor(init?: Readonly<POSSESSION_ADDED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class POSSESSION_REMOVED extends Topic {
    public static readonly ID = 'POSSESSION_REMOVED';

    public entityId: string;
    public itemId: string;

    public constructor(init?: Readonly<POSSESSION_REMOVED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class DIALOG_LINE_ADDED extends Topic {
    public static readonly ID = 'DIALOG_LINE_ADDED';

    public entityId: string;
    public dialogueLineId: string;

    public constructor(init?: Readonly<DIALOG_LINE_ADDED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class DIALOG_LINE_REMOVED extends Topic {
    public static readonly ID = 'DIALOG_LINE_REMOVED';

    public entityId: string;
    public dialogueLineId: string;

    public constructor(init?: Readonly<DIALOG_LINE_REMOVED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class ENTITY_ADDED_TO_INVENTORY extends Topic {
    public static readonly ID = 'ENTITY_ADDED_TO_INVENTORY';

    public inventoryOwnerId: string;
    public inventoryId?: string;
    public entityId: string;

    public constructor(init?: Readonly<ENTITY_ADDED_TO_INVENTORY>) {
      super();
      Object.assign(this, init);
    }
  }

  export class ENTITY_REMOVED_FROM_INVENTORY extends Topic {
    public static readonly ID = 'ENTITY_REMOVED_FROM_INVENTORY';

    public inventoryOwnerId: string;
    public inventoryId?: string;
    public entityId: string;

    public constructor(init?: Readonly<ENTITY_REMOVED_FROM_INVENTORY>) {
      super();
      Object.assign(this, init);
    }
  }

  export class ENTITY_ADDED_TO_CONTAINER extends Topic {
    public static readonly ID = 'ENTITY_ADDED_TO_CONTAINER';

    public containerId: string;
    public entityId: string;

    public constructor(init?: Readonly<ENTITY_ADDED_TO_CONTAINER>) {
      super();
      Object.assign(this, init);
    }
  }

  export class ENTITY_REMOVED_FROM_CONTAINER extends Topic {
    public static readonly ID = 'ENTITY_REMOVED_FROM_CONTAINER';

    public containerId: string;
    public entityId: string;

    public constructor(init?: Readonly<ENTITY_REMOVED_FROM_CONTAINER>) {
      super();
      Object.assign(this, init);
    }
  }

  export class SPATIAL_RELATION_ADDED extends Topic {
    public static readonly ID = 'SPATIAL_RELATION_ADDED';

    public spatialRelationId: string;
    public directId: string;
    public indirectId: string;
    public typeId: string;

    public constructor(init?: Readonly<SPATIAL_RELATION_ADDED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class SPATIAL_RELATION_REMOVED extends Topic {
    public static readonly ID = 'SPATIAL_RELATION_REMOVED';

    public directId: string;
    public indirectId: string;
    public typeId: string;

    public constructor(init?: Readonly<SPATIAL_RELATION_REMOVED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class ENTITY_SCENE_CHANGED extends Topic {
    public static readonly ID = 'ENTITY_SCENE_CHANGED';

    public entityId: string;
    public oldSceneId?: string | null;
    public newSceneId: string;

    public constructor(init?: Readonly<ENTITY_SCENE_CHANGED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class SUBSTANCE_VOLUME_DECREASED extends Topic {
    public static readonly ID = 'SUBSTANCE_VOLUME_DECREASED';

    public substanceId: string;
    public oldVolume: number;
    public newVolume: number;

    public constructor(init?: Readonly<SUBSTANCE_VOLUME_DECREASED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class SUBSTANCE_VOLUME_INCREASED extends Topic {
    public static readonly ID = 'SUBSTANCE_VOLUME_INCREASED';

    public substanceId: string;
    public oldVolume: number;
    public newVolume: number;

    public constructor(init?: Readonly<SUBSTANCE_VOLUME_INCREASED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class SUBSTANCE_VOLUME_RESTORED extends Topic {
    public static readonly ID = 'SUBSTANCE_VOLUME_RESTORED';

    public substanceId: string;
    public oldVolume: number;
    public newVolume: number;

    public constructor(init?: Readonly<SUBSTANCE_VOLUME_RESTORED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class DRUNK extends Topic {
    public static readonly ID = 'DRUNK';

    public actorId: string;
    public substanceId: string;
    public amount: number;

    public constructor(init?: Readonly<DRUNK>) {
      super();
      Object.assign(this, init);
    }
  }

  export class POURED extends Topic {
    public static readonly ID = 'POURED';

    public actorId: string;
    public substanceId: string;
    public targetId: string;
    public spatialRelationTypeId: string;
    public amount: number;

    public constructor(init?: Readonly<POURED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class SOLIDIFIED extends Topic {
    public static readonly ID = 'SOLIDIFIED';

    public substanceId: string;
    public substanceNounId: string;
    public spatialRelationTypeId: string | null;
    public indirectId: string | null;

    public constructor(init?: Readonly<SOLIDIFIED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class MELTED extends Topic {
    public static readonly ID = 'MELTED';

    public substanceId: string;
    public substanceNounId: string;
    public spatialRelationTypeId: string | null;
    public indirectId: string | null;
    public prevVolume: number;
    public newVolume: number;

    public constructor(init?: Readonly<MELTED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class EVAPORATED extends Topic {
    public static readonly ID = 'EVAPORATED';

    public substanceId: string;
    public substanceNounId: string;
    public spatialRelationTypeId: string | null;
    public indirectId: string | null;
    public prevVolume: number;
    public newVolume: number;

    public constructor(init?: Readonly<EVAPORATED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class DISSIPATED extends Topic {
    public static readonly ID = 'DISSIPATED';

    public substanceId: string;
    public substanceNounId: string;
    public prevVolume: number;
    public newVolume: number;

    public constructor(init?: Readonly<DISSIPATED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class SOUND extends Topic {
    public static readonly ID = 'SOUND';

    public entityId: string;
    public soundDescriptionResourceId?: string;
    public soundLevel: number;
    public duration: number;
    public sourceSceneId: string | null;
    public sourceLocation: Vector3 | null;

    public constructor(init?: Readonly<SOUND>) {
      super();
      Object.assign(this, init);
    }
  }

  export class SMELL extends Topic {
    public static readonly ID = 'SMELL';

    public substanceId: string;
    public substanceNounId: string;
    public smellDescriptionResourceId?: string;
    public smellLevel: number;
    public sourceSceneId: string | null;
    public sourceLocation: Vector3 | null;

    public constructor(init?: Readonly<SMELL>) {
      super();
      Object.assign(this, init);
    }
  }

  export class APPEARANCE_CHANGE extends Topic {
    public static readonly ID = 'APPEARANCE_CHANGE';

    public entityId: string;
    public appearanceDescriptionResourceId?: string;
    public appearanceDescriptionResourceTemplateBag?: any;
    public appearanceDescription?: string;
    public sourceSceneId: string | null;
    public sourceLocation: Vector3 | null;

    public constructor(init?: Readonly<APPEARANCE_CHANGE>) {
      super();
      Object.assign(this, init);
    }
  }

  export class SPEECH extends Topic {
    public static readonly ID = 'SPEECH';

    public entityId: string;
    public targetId: string;
    public dialogueLineId: string;
    public modifierId: string;

    public constructor(init?: Readonly<SPEECH>) {
      super();
      Object.assign(this, init);
    }
  }

  export class ENGAGING extends Topic {
    public static readonly ID = 'ENGAGING';

    public actorId: string;
    public targetId: string;

    public constructor(init?: Readonly<ENGAGING>) {
      super();
      Object.assign(this, init);
    }
  }

  export class ENGAGED extends Topic {
    public static readonly ID = 'ENGAGED';

    public actorId: string;
    public targetId: string;

    public constructor(init?: Readonly<ENGAGED>) {
      super();
      Object.assign(this, init);
    }
  }

  export interface ATTACK_STAGE_CHANGING {
    actorId: string;
    targetId: string;
    instrumentId: string;
    stageChangeTimeMillis: number;
  }

  export class FIGHT_SEQUENCE_NEXT extends Topic {
    public static readonly ID = 'FIGHT_SEQUENCE_NEXT';

    public entityId: string;

    public constructor(init?: Readonly<FIGHT_SEQUENCE_NEXT>) {
      super();
      Object.assign(this, init);
    }
  }

  export class PURSUE_STEP_NEXT extends Topic {
    public static readonly ID = 'PURSUE_STEP_NEXT';

    public entityId: string;

    public constructor(init?: Readonly<PURSUE_STEP_NEXT>) {
      super();
      Object.assign(this, init);
    }
  }

  export class ATTACK_WINDING_UP extends Topic implements ATTACK_STAGE_CHANGING {
    public static readonly ID = 'ATTACK_WINDING_UP';

    public actorId: string;
    public targetId: string;
    public instrumentId: string;
    public stageChangeTimeMillis: number;

    public constructor(init?: Readonly<ATTACK_WINDING_UP>) {
      super();
      Object.assign(this, init);
    }
  }

  export class ATTACK_CANCELLING extends Topic implements ATTACK_STAGE_CHANGING {
    public static readonly ID = 'ATTACK_CANCELLING';

    public actorId: string;
    public targetId: string;
    public instrumentId: string;
    public stageChangeTimeMillis: number;

    public constructor(init?: Readonly<ATTACK_CANCELLING>) {
      super();
      Object.assign(this, init);
    }
  }

  export class ATTACK_LANDING extends Topic implements ATTACK_STAGE_CHANGING {
    public static readonly ID = 'ATTACK_LANDING';

    public actorId: string;
    public targetId: string;
    public instrumentId: string;
    public stageChangeTimeMillis: number;

    public constructor(init?: Readonly<ATTACK_LANDING>) {
      super();
      Object.assign(this, init);
    }
  }

  export class ATTACK_COOLING_DOWN extends Topic implements ATTACK_STAGE_CHANGING {
    public static readonly ID = 'ATTACK_COOLING_DOWN';

    public actorId: string;
    public targetId: string;
    public instrumentId: string;
    public stageChangeTimeMillis: number;

    public constructor(init?: Readonly<ATTACK_COOLING_DOWN>) {
      super();
      Object.assign(this, init);
    }
  }

  export class ATTACK_FINISHING extends Topic implements ATTACK_STAGE_CHANGING {
    public static readonly ID = 'ATTACK_FINISHING';

    public actorId: string;
    public targetId: string;
    public instrumentId: string;
    public stageChangeTimeMillis: number;

    public constructor(init?: Readonly<ATTACK_FINISHING>) {
      super();
      Object.assign(this, init);
    }
  }

  export class ATTACK_WINDUP extends Topic implements ATTACK_STAGE_CHANGING {
    public static readonly ID = 'ATTACK_WINDUP';

    public actorId: string;
    public targetId: string;
    public instrumentId: string;
    public stageChangeTimeMillis: number;

    public constructor(init?: Readonly<ATTACK_WINDUP>) {
      super();
      Object.assign(this, init);
    }
  }

  export class ATTACK_COOLDOWN extends Topic implements ATTACK_STAGE_CHANGING {
    public static readonly ID = 'ATTACK_COOLDOWN';

    public actorId: string;
    public targetId: string;
    public instrumentId: string;
    public stageChangeTimeMillis: number;

    public constructor(init?: Readonly<ATTACK_COOLDOWN>) {
      super();
      Object.assign(this, init);
    }
  }

  export class ATTACK_CANCELLED extends Topic implements ATTACK_STAGE_CHANGING {
    public static readonly ID = 'ATTACK_CANCELLED';

    public actorId: string;
    public targetId: string;
    public instrumentId: string;
    public stageChangeTimeMillis: number;

    public constructor(init?: Readonly<ATTACK_CANCELLED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class ATTACK_FINISHED extends Topic implements ATTACK_STAGE_CHANGING {
    public static readonly ID = 'ATTACK_FINISHED';

    public actorId: string;
    public targetId: string;
    public instrumentId: string;
    public stageChangeTimeMillis: number;

    public constructor(init?: Readonly<ATTACK_FINISHED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class ATTACKED extends Topic {
    public static readonly ID = 'ATTACKED';

    public actorId: string;
    public targetId: string;
    public instrumentId: string;
    public attackRating: number;
    public damage: number;

    public constructor(init?: Readonly<ATTACKED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class STUNNED extends Topic {
    public static readonly ID = 'STUNNED';

    public actorId: string;
    public targetId: string;
    public instrumentId: string;
    public stunDurationMillis: number;

    public constructor(init?: Readonly<STUNNED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class STUN_RECOVERY extends Topic {
    public static readonly ID = 'STUN_RECOVERY';

    public actorId: string;

    public constructor(init?: Readonly<STUN_RECOVERY>) {
      super();
      Object.assign(this, init);
    }
  }

  export class BLOCKING extends Topic {
    public static readonly ID = 'BLOCKING';

    public actorId: string;
    public targetId: string;
    public instrumentId: string;

    public constructor(init?: Readonly<BLOCKING>) {
      super();
      Object.assign(this, init);
    }
  }

  export class BLOCK extends Topic {
    public static readonly ID = 'BLOCK';

    public actorId: string;
    public targetId: string;
    public instrumentId: string;
    public blockWindupMillis: number;

    public constructor(init?: Readonly<BLOCK>) {
      super();
      Object.assign(this, init);
    }
  }

  export class BLOCKED extends Topic {
    public static readonly ID = 'BLOCKED';

    public actorId: string;
    public targetId: string;
    public instrumentId: string;

    public constructor(init?: Readonly<BLOCKED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class DODGING extends Topic {
    public static readonly ID = 'DODGING';

    public actorId: string;
    public targetId: string;

    public constructor(init?: Readonly<DODGING>) {
      super();
      Object.assign(this, init);
    }
  }

  export class DODGE extends Topic {
    public static readonly ID = 'DODGE';

    public actorId: string;
    public targetId: string;
    public dodgeCooldownMillis: number;

    public constructor(init?: Readonly<DODGE>) {
      super();
      Object.assign(this, init);
    }
  }

  export class DODGED extends Topic {
    public static readonly ID = 'DODGED';

    public actorId: string;
    public targetId: string;

    public constructor(init?: Readonly<DODGED>) {
      super();
      Object.assign(this, init);
    }
  }


  export class EVADING extends Topic {
    public static readonly ID = 'EVADING';

    public actorId: string;
    public targetId: string;

    public constructor(init?: Readonly<EVADING>) {
      super();
      Object.assign(this, init);
    }
  }

  export class EVADED extends Topic {
    public static readonly ID = 'EVADED';

    public actorId: string;
    public targetId: string;

    public constructor(init?: Readonly<EVADED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class TOOK_DAMAGE extends Topic {
    public static readonly ID = 'TOOK_DAMAGE';

    public entityId: string;
    public damageSourceId: string;
    public damage: number;

    public constructor(init?: Readonly<TOOK_DAMAGE>) {
      super();
      Object.assign(this, init);
    }
  }

  export class DIED extends Topic {
    public static readonly ID = 'DIED';

    public entityId: string;

    public constructor(init?: Readonly<DIED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class RENDER_SCENE_DESCRIPTION extends Topic {
    public static readonly ID = 'RENDER_SCENE_DESCRIPTION';

    public actorId: string;

    public constructor(init?: Readonly<RENDER_SCENE_DESCRIPTION>) {
      super();
      Object.assign(this, init);
    }
  }

  export class GAME_LOG_UPDATED extends Topic {
    public static readonly ID = 'GAME_LOG_UPDATED';

    public constructor(init?: Readonly<GAME_LOG_UPDATED>) {
      super();
      Object.assign(this, init);
    }
  }

  export class UI_REFRESH extends Topic {
    public static readonly ID = 'UI_REFRESH';

    public constructor(init?: Readonly<UI_REFRESH>) {
      super();
      Object.assign(this, init);
    }
  }
}
