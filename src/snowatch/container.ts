import {StatelessEntity, Entity, HasId} from 'snowatch/entity';
import {Topic, broadcast, payload} from 'snowatch/message';
import {Topics} from 'snowatch/message-topics/all';
import { SpatialPrepositions } from './prepositions';
import * as spatial from 'snowatch/spatial-relations';
import * as attrs from 'snowatch/attributes-ids';

export interface ContainerProperties {
  container: Map<string, ContainerInit>;
}

export interface ContainerInit extends HasId {
}

export interface ContainerItemFilterFn {
  (entity: Entity): boolean;
}

export function getContainerId(entity: StatelessEntity): string | null {
  if( attrs.Attribute.applies(entity, attrs.ContainerId) ) {
    return attrs.Attribute.value<string>(entity, attrs.ContainerId);
  } else {
    return null;
  }
}

export function setContainerId(entity: Entity, value: string | null | undefined) {
  if( attrs.Attribute.applies(entity, attrs.ContainerId) ) {
    return attrs.Attribute.setValue<string>(entity, attrs.ContainerId, value !== undefined ? value : null);
  }
}

export class Container {
  static initializeContainer(target: Entity) {
    target.logger.debug(`${target.id}: initializeContainer()`);
    attrs.Attribute.add(target, attrs.ContainerCapacityUsed, { initialState: {value: 0}} );
    target.game.bus.notifications.subscribe({
      receiver: target,
      filter: (m) => {
        if( m.topicId !== Topic.id(Topics.SPATIAL_RELATION_ADDED) ) {
          return false;
        }
        const messagePayload = payload<Topics.SPATIAL_RELATION_ADDED>(m);
        return messagePayload.indirectId === target.id && messagePayload.typeId === SpatialPrepositions.isIn.id;
      },
      handler: (m) =>  {
        const messagePayload = payload<Topics.SPATIAL_RELATION_ADDED>(m);
        Container.add(target, messagePayload.directId);
      }
    });
    target.game.bus.notifications.subscribe({
      receiver: target,
      filter: (m) =>  {
        if( m.topicId !== Topic.id(Topics.SPATIAL_RELATION_REMOVED) ) {
          return false;
        }
        const messagePayload = payload<Topics.SPATIAL_RELATION_REMOVED>(m);
        return messagePayload.indirectId === target.id && messagePayload.typeId === SpatialPrepositions.isIn.id;
      },
      handler: (m) =>  {
        const messagePayload = payload<Topics.SPATIAL_RELATION_REMOVED>(m);
        Container.remove(target, messagePayload.directId);
      }
    });
    target.game.bus.system.subscribe({
      receiver: target,
      filter: (m) =>  {
        return m.topicId === Topic.id(Topics.INITIALIZE_COMPONENTS);
      },
      handler: (m) =>  {
        Container.updateUsedVolume(target);
      }
    });

    target.state.container = target.state.container ?? new Set<Entity>();
  }

  static getContainer(target: Entity) {
    return target.state.container;
  }

  static add(target: Entity, itemId: string) {
    target.logger.debug(`Adding item to container ${itemId}`);
    target.state.container.add(itemId);
    Container.updateUsedVolume(target);
    target.game.bus.notifications.publish(broadcast(Topics.ENTITY_ADDED_TO_CONTAINER, { containerId: target.id, entityId: itemId }, undefined, target.id));
  }

  static addFromProperties(target: Entity, properties: ContainerProperties) {
    if( properties != null && properties.container != null ) {
      for(let [itemId, item] of properties.container) {
        Container.add(target, itemId);
      }
    }
  }

  static remove(target: Entity, itemId: string) {
    target.logger.debug(`Removing item from container ${itemId}`);
    target.state.container.delete(itemId);
    Container.updateUsedVolume(target);
    target.game.bus.notifications.publish(broadcast(Topics.ENTITY_REMOVED_FROM_CONTAINER, { containerId: target.id, entityId: itemId }, undefined, target.id));
  }

  static removeAll(target: Entity) {
    target.logger.debug(`Clearing container`);
    const items = Array.from<string>(target.state.container);
    for(let itemId of items) {
      Container.remove(target, itemId);
    }
  }

  static contains(target: Entity, itemId: string): boolean {
    if( !attrs.Attribute.applies(target, attrs.IsContainer) || !attrs.Attribute.value<boolean>(target, attrs.IsContainer) ) {
      target.logger.warn(`Trying to check whether [${target.id}] contains [${itemId}], but [${target.id}] is not a container`);

      return false;
    }

    return target.state.container.has(itemId);
  }

  static items(target: Entity, filter?: ContainerItemFilterFn): Array<Entity> {
    if( !attrs.Attribute.applies(target, attrs.IsContainer) || !attrs.Attribute.value<boolean>(target, attrs.IsContainer) ) {
      target.logger.warn(`Trying to get items in [${target.id}] which is not a container`);

      return [];
    }

    const containedItems = new Array<Entity>();

    const items = Array.from<string>(target.state.container);
    for(let itemId of items) {
      const item = target.game.instances.getAnyFromId(itemId);
      if(item != null && (filter == null || filter(item)) ) {
        containedItems.push(item);
      }
    }

    return containedItems;
  }

  private static updateUsedVolume(target: Entity) {
    let currentCapacityUsed = 0;
    for(const entityId of target.state.container.values()) {
      const entity = target.game.instances.getEntityFromId(entityId);
      if(entity != null && (attrs.Attribute.applies(entity, attrs.SpatialSize) || attrs.Attribute.applies(entity, attrs.Volume))) {
        // TODO: at some point we should be able to add stuff to container with liquid including pushing liquid out of the container,
        // in which case we need way more complex rules here
        let size: spatial.Vector3 = spatial.Zero3;
        if( attrs.Attribute.applies(entity, attrs.Volume) ) {
          const volumeDirect = attrs.Attribute.value<number>(entity, attrs.Volume);
          size = spatial.sizeFromVolume(volumeDirect);
        } else if( attrs.Attribute.applies(entity, attrs.SpatialSize) ) {
          size = attrs.Attribute.value<spatial.Vector3>(entity, attrs.SpatialSize);
        }

        let objectCompressibility: spatial.Vector3 = attrs.Attribute.applies(entity, attrs.Compressibility) ? attrs.Attribute.value(entity, attrs.Compressibility) : spatial.Zero3;
        currentCapacityUsed += spatial.Size3.volume({
          x: size.x * (1  - objectCompressibility.x),
          y: size.y * (1  - objectCompressibility.y),
          z: size.z * (1  - objectCompressibility.z),
        });
      }
    }
    attrs.Attribute.setValue<number>(target, attrs.ContainerCapacityUsed, currentCapacityUsed);
  }
}