import { GameData } from 'snowatch/game-data';
import { Attribute } from 'snowatch/attribute';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { DefaultResDict } from 'snowatch/resources/default';
import { Entity } from 'snowatch/entity';
import { Topics } from 'snowatch/message-topics/all';
import { broadcast, Message, payload, Topic } from 'snowatch/message';
import { AttackType, FightCalculator } from 'snowatch/fight-calculator';
import { DefaultFightCalculator } from 'snowatch/calculators/default-fight-calculator';
import { TimeSource } from 'snowatch/time-source';
import { BusRequest, clamp, isArrayNullOrEmpty, RejectFilterBehaviors, tryGetArrayElement } from 'snowatch/utils';
import * as skill from 'snowatch/skill';
import * as result from 'snowatch/result';
import * as attrs from 'snowatch/attributes-ids';
import * as lang from 'snowatch/language-structures';

export class AttackModifiers {
  public static readonly quickly: skill.SkillModifier = { id: 'quickly', modifierResource: DefaultResDict.QUICKLY, details: { attackType: AttackType.fast } };
  public static readonly strongly: skill.SkillModifier = { id: 'strongly', modifierResource: DefaultResDict.STRONGLY, details: {  attackType: AttackType.strong } };

  private static _all: Map<string, skill.SkillModifier>;
  static all(): Map<string, skill.SkillModifier> {
    AttackModifiers._all = AttackModifiers._all ?? new Map<string, skill.SkillModifier>([
      [AttackModifiers.strongly.id, AttackModifiers.strongly],
      [AttackModifiers.quickly.id, AttackModifiers.quickly],
    ]);
    return AttackModifiers._all;
  }
}

export class Attack extends skill.Skill {
  public static readonly ID = 'Attack';

  private _indirects: Array<skill.PrepositionedEntity>;

  private _calc: FightCalculator;

  constructor() {
    super();
  }

  initialize(game: GameData) {
    super.initialize(game);
    this._calc = new DefaultFightCalculator();
    return this;
  }

  get verb() {
    return lang.Verb.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.ATTACK));
  }

  get isRealTime(): boolean {
    return true;
  }

  preview(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn): result.ResultObject {
    if (!Attribute.applies(config.direct, attrs.IsAttackable, config.actor)) {
      return result.make(result.rejected, undefined, `Entity ${config.direct.id} doesn't support attacking at this moment.`);
    }

    if (!Attribute.value<boolean>(config.direct, attrs.IsAttackable, config.actor)) {
      return result.make(result.rejected, undefined, `Entity ${config.direct.id} cannot be attacked at this moment.`);
    }

    if (isArrayNullOrEmpty(config.instruments)) {
      this.logger.debug(`No instrument given`);
      return result.make(result.rejected, `Attack on ${config.direct} was cancelled because no instrument was given.`);
    }

    const { ownStaminaConsumptionAttack, ownStamina } = this.getAttackMetrics(config);

    if( ownStaminaConsumptionAttack > ownStamina.value ) {
      return result.make(result.rejected, undefined, `Entity ${config.direct.id} doesn't have enough stamina (attack needs ${ownStaminaConsumptionAttack} but available is ${ownStamina}).`);
    }

    return super.preview(state, config, filter);
  }

  async execute(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn) {
    this.logger.debug('execute:', state, config);
    const previewResult = this.preview(state, config, filter);
    if (result.isAccepted(previewResult)) {
      const { ownStaminaConsumptionAttack, ownStamina, attackedEntity, instrumentId, windupMillis, cooldownMillis, cancelCooldownMillis, instrument, stunDurationMillis, damage } = this.getAttackMetrics(config);

      // inform about attack stage change
      this.game.bus.notifications.publish(broadcast(
        Topics.ATTACK_WINDING_UP, {
          actorId: config.actor.id,
          instrumentId: instrumentId,
          targetId: attackedEntity.id,
          stageChangeTimeMillis: windupMillis,
        },
        undefined,
        config.actor.id
      ));

      // this is async behavior, where timing plays crucial role
      // we need to initiate attack and send attack telegraph message
      // we do not have resolve path to the windup, just reject by BLOCK, DODGE, EVADE, or the windup times out which means, that the attack is going forward
      const windupRequest = new BusRequest({
        requestBus: this.game.bus.notifications,
        responseBus: this.game.bus.notifications,
        clockTickBus: this.game.bus.system,
        rejectFilter: (m: Message) => {
          switch( m.topicId ) {
            case Topic.id(Topics.BLOCKED): {
              const messagePayload = payload<Topics.BLOCKED>(m);
              // block applies just to this particular attacker
              return messagePayload.targetId === config.actor.id || messagePayload.actorId === config.actor.id;
            }
            case Topic.id(Topics.DODGE): {
              // dodge applies to all attacks targeted at the attackedEntity
              const messagePayload = payload<Topics.DODGE>(m);
              return messagePayload.actorId === attackedEntity.id || messagePayload.actorId === config.actor.id;
            }
            case Topic.id(Topics.EVADED): {
              const messagePayload = payload<Topics.EVADED>(m);
              return messagePayload.targetId === config.actor.id || messagePayload.actorId === config.actor.id;
            }
            case Topic.id(Topics.ATTACKED): {
              const messagePayload = payload<Topics.ATTACKED>(m);
              return messagePayload.targetId === config.actor.id;
            }
            default: {
              return false;
            }
          }
        },
        rejectFilterBehavior: RejectFilterBehaviors.any,
        timeoutWallClockTicks: TimeSource.getWallClockTicksFromNow(this.game, windupMillis),
      });
      let windupResult = await windupRequest.send(broadcast(
        Topics.ATTACK_WINDUP, {
          actorId: config.actor.id,
          targetId: attackedEntity.id,
          instrumentId: instrumentId,
          stageChangeTimeMillis: windupMillis,
        },
        undefined,
        config.actor.id
      ));

      // (in the mean time, consume stamina once we signal windup)
      ownStamina.value = Math.round(clamp(ownStamina.value - ownStaminaConsumptionAttack, ownStamina.minValue, ownStamina.maxValue));
      attrs.Attribute.setValue(config.actor, attrs.Stamina, ownStamina);

      // that gives opponents time to react with block
      const cancelled = result.isRejected(windupResult);
      if (cancelled) {
        // inform about attack stage change
        this.game.bus.notifications.publish(broadcast(
          Topics.ATTACK_CANCELLING, {
            actorId: config.actor.id,
            targetId: attackedEntity.id,
            instrumentId: instrumentId,
            stageChangeTimeMillis: cancelCooldownMillis,
          },
          undefined,
          config.actor.id
        ));

        // if oponent blocks the attack (ie. reacts within the attack wind up time window), the attack is cancelled and the failed attack cooldown is applied
        // there is no way to resolve the request other than timeout, which means that cooldown have to be waited out to the end
        // TODO: implement usage of the faster counter attack, which will cancel cooldown and end up in damage being received by the current actor
        const cancelledRequest = new BusRequest({
          requestBus: this.game.bus.notifications,
          responseBus: this.game.bus.notifications,
          clockTickBus: this.game.bus.system,
          timeoutWallClockTicks: TimeSource.getWallClockTicksFromNow(this.game, cancelCooldownMillis),
        });
        let cancelResult = await cancelledRequest.send(broadcast(
          Topics.ATTACK_CANCELLED, {
            actorId: config.actor.id,
            targetId: attackedEntity.id,
            instrumentId: instrumentId,
            stageChangeTimeMillis: cancelCooldownMillis,
          },
          undefined,
          config.actor.id
        ));


        // inform about attack stage change
        this.game.bus.notifications.publish(broadcast(
          Topics.ATTACK_FINISHING, {
            actorId: config.actor.id,
            targetId: attackedEntity.id,
            instrumentId: instrumentId,
            stageChangeTimeMillis: 0,
          },
          undefined,
          config.actor.id
        ));
        // inform about end of attack
        this.game.bus.notifications.publish(broadcast(
          Topics.ATTACK_FINISHED, {
            actorId: config.actor.id,
            targetId: attackedEntity.id,
            instrumentId: instrumentId,
            stageChangeTimeMillis: 0,
          },
          undefined,
          config.actor.id
        ));

        return result.make(result.rejected, undefined, `Attack on ${attackedEntity.id} was cancelled.`);
      }

      // once attack is issued, it can't be cancelled
      // inform about attack stage change
      this.game.bus.notifications.publish(broadcast(
        Topics.ATTACK_LANDING, {
          actorId: config.actor.id,
          targetId: attackedEntity.id,
          instrumentId: instrumentId,
          stageChangeTimeMillis: 0,
        },
        undefined,
        config.actor.id
      ));
      // attack can cause stun of the target
      if( attrs.Attribute.applies(instrument, attrs.CanStun, config.actor) && attrs.Attribute.value<boolean>(instrument, attrs.CanStun, config.actor)
          && attrs.Attribute.applies(attackedEntity, attrs.CanBeStunned, config.actor) && attrs.Attribute.value<boolean>(attackedEntity, attrs.CanBeStunned, config.actor) ) {
        this.game.bus.notifications.publish(broadcast(
          Topics.STUNNED, {
            actorId: config.actor.id,
            targetId: attackedEntity.id,
            instrumentId: instrumentId,
            stunDurationMillis: stunDurationMillis,
          },
          undefined,
          config.actor.id
        ));
      }
      // if the attack lands, the damage is calculated and it is published in message
      this.game.bus.notifications.publish(broadcast(
        Topics.ATTACKED, {
          actorId: config.actor.id,
          targetId: attackedEntity.id,
          instrumentId: instrumentId,
          attackRating: 1,
          damage: damage,
        },
        undefined,
        config.actor.id
      ));

      // inform about attack stage change
      this.game.bus.notifications.publish(broadcast(
        Topics.ATTACK_COOLING_DOWN, {
          actorId: config.actor.id,
          targetId: attackedEntity.id,
          instrumentId: instrumentId,
          stageChangeTimeMillis: cooldownMillis,
        },
        undefined,
        config.actor.id
      ));

      // then the successful attack cooldown is applied
      // there is no way to resolve the request other than timeout, which means that cooldown have to to be waited out to the end
      // TODO: implement usage of the faster counter attack, which will cancel cooldown and end up in damage being received by the current actor
      const request = new BusRequest({
        requestBus: this.game.bus.notifications,
        responseBus: this.game.bus.notifications,
        clockTickBus: this.game.bus.system,
        timeoutWallClockTicks: TimeSource.getWallClockTicksFromNow(this.game, cooldownMillis),
      });
      let cooldownResult = await request.send(broadcast(
        Topics.ATTACK_COOLDOWN, {
          actorId: config.actor.id,
          targetId: attackedEntity.id,
          instrumentId: instrumentId,
          stageChangeTimeMillis: cooldownMillis,
        },
        undefined,
        config.actor.id
      ));

      // inform about attack stage change
      this.game.bus.notifications.publish(broadcast(
        Topics.ATTACK_FINISHING, {
          actorId: config.actor.id,
          targetId: attackedEntity.id,
          instrumentId: instrumentId,
          stageChangeTimeMillis: 0,
        },
        undefined,
        config.actor.id
      ));
      // inform about end of attack
      this.game.bus.notifications.publish(broadcast(
        Topics.ATTACK_FINISHED, {
          actorId: config.actor.id,
          targetId: attackedEntity.id,
          instrumentId: instrumentId,
          stageChangeTimeMillis: 0,
        },
        undefined,
        config.actor.id
      ));

      return result.make(result.accepted, {
        actorId: config.actor.id,
        targetId: attackedEntity.id,
        attackRating: 1,
      }, `Attack on ${attackedEntity.id} delivered.`);
    }

    return previewResult;
  }

  private getAttackMetrics(config: skill.SkillConfig) {
    const attackedEntity = config.direct as Entity;
    const instrument = config.instruments[0];
    const instrumentId = instrument.id;

    // get attack type
    const attackType = AttackModifiers.all().get(config.modifierId ?? AttackModifiers.strongly.id)?.details?.attackType ?? AttackType.strong;

    // get windup delay
    const windupMillis = this._calc.attackWindUpTime(config.actor, instrument, attackType);
    const cooldownMillis = this._calc.attackCoolDownTime(config.actor, instrument, attackType);
    const cancelCooldownMillis = cooldownMillis / 10;
    const ownStamina = attrs.Attribute.value<attrs.NumericValueAttribute>(config.actor, attrs.Stamina, config.actor);
    const ownStaminaConsumptionAttack = this._calc.attackStaminaConsumption(config.actor, instrument, attackType);
    const damage = this._calc.damage(config.actor, instrument, attackType);
    const stunDurationMillis = this._calc.stunTime(attackedEntity, damage);
    return {
        attackedEntity,
        instrumentId,
        instrument,
        damage,
        windupMillis,
        cooldownMillis,
        cancelCooldownMillis,
        stunDurationMillis,
        ownStamina,
        ownStaminaConsumptionAttack,
      };
  }

  getDirectPrepositionId() {
    return ResourceDictionary.get(this.game, DefaultResDict.ACCUSATIVE_ON);
  }

  getAllModifiers(config?: skill.SkillConfig): Array<skill.SkillModifier> {
    this._allModifiers = this._allModifiers ?? Array.from<skill.SkillModifier>(AttackModifiers.all().values());
    return this._allModifiers;
  }

  getApplicableModifiers(config: skill.SkillConfig): Array<skill.SkillModifier> {
    return this.getAllModifiers().filter(modifier => {
      const { ownStaminaConsumptionAttack, ownStamina } = this.getAttackMetrics(config);
      return ownStaminaConsumptionAttack < ownStamina.value;
    });
  }

  getRequiredIndirectsCount(config: skill.SkillConfig): number {
    return 0;
  }

  getIndirects(config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    this._indirects = this._indirects ?? [];
    return this._indirects;
  }

  getRequiredInstrumentsCount(config: skill.SkillConfig): number {
    return 1;
  }
}
