import { GameData } from 'snowatch/game-data';
import { Actor } from 'snowatch/actor';
import { Inventory } from 'snowatch/inventory';
import { Take } from 'snowatch/skills/take';
import { BusRequest } from 'snowatch/utils';
import { Topic, Message, message, broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { DefaultResDict } from 'snowatch/resources/default';
import * as skill from 'snowatch/skill';
import * as ent from 'snowatch/entity';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';
import * as lang from 'snowatch/language-structures';

export class GiveModifiers {
  public static readonly quickly: skill.SkillModifier = { id: 'quickly', modifierResource: DefaultResDict.QUICKLY, details: {} };
  public static readonly hesitantly: skill.SkillModifier = { id: 'hesitantly', modifierResource: DefaultResDict.HESITANTLY, details: {} };
  public static readonly cautiosly: skill.SkillModifier = { id: 'cautiosly', modifierResource: DefaultResDict.CAUTIOUSLY, details: {} };

  private static _all: Map<string, skill.SkillModifier>;
  static all(): Map<string, skill.SkillModifier> {
    GiveModifiers._all = GiveModifiers._all ?? new Map<string, skill.SkillModifier>([
      [skill.SkillModifiers.neutrally.id, skill.SkillModifiers.neutrally],
      [GiveModifiers.quickly.id, GiveModifiers.quickly],
      [GiveModifiers.hesitantly.id, GiveModifiers.hesitantly],
      [GiveModifiers.cautiosly.id, GiveModifiers.cautiosly],
    ]);
    return GiveModifiers._all;
  }
}

export class Give extends skill.Skill {
  public static readonly ID = 'Give';

  constructor() {
    super();
  }

  initialize(game: GameData) {
    super.initialize(game);
    return this;
  }

  get verb() {
    return lang.Verb.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.GIVE));
  }

  preview(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn): result.ResultObject {
    if (!attrs.Attribute.applies(config.direct, attrs.IsGivable, config.actor)) {
      return result.make(
        result.rejected,
        undefined,
        `Item ${config.direct.id} doesn't support giving at this moment.`
      );
    }

    if (!attrs.Attribute.value<boolean>(config.direct, attrs.IsGivable, config.actor)) {
      return result.make(
        result.rejected,
        undefined,
        `Item ${config.direct.id} cannot be given at this moment.`
      );
    }


    if (ent.getHolderId(config.direct) !== config.actor.id || config.indirects.length === 0 || !config.indirects.some(a => a.id !== config.actor.id)) {
      return result.make(
        result.rejected,
        undefined,
        `Item ${config.direct.id} cannot be given at this moment.`
      );
    }

    return super.preview(state, config, filter);
  }

  async execute(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn) {
    this.logger.debug('execute:', config);
    // preview giving
    const previewResult = this.preview(state, config, filter);
    if (result.isAccepted(previewResult)) {
      // remove from current holder's inventory
      Inventory.removeItem(config.actor, config.direct.id);

      // try invoke taking
      const request = new BusRequest({
        requestBus: this.game.bus.commands,
        responseBus: this.game.bus.notifications,
        resolveFilter: (m: Message) => m.topicId === Topic.id(Topics.SKILL_ACCEPTED),
        rejectFilter: (m: Message) => m.topicId === Topic.id(Topics.SKILL_REJECTED)
      });
      const targetActor = config.indirects[0];
      let skillResult = await request.send(broadcast(
        Topics.TRY_USE_SKILL,
        {
          skillId: Take.ID,
          actorId: targetActor.id,
          directId: config.direct.id,
          modifierId: null,
          indirectsIds: [targetActor.id],
        },
        undefined,
        config.actor.id
      ));
      if (!result.isAccepted(skillResult)) {
        // offer refused, take it back in original holder's inventory
        Inventory.addItem(config.actor, config.direct.id);
      }
      return skillResult;
    }
    return previewResult;
  }

  getAllModifiers(): Array<skill.SkillModifier> {
    this._allModifiers = this._allModifiers ?? Array.from<skill.SkillModifier>(GiveModifiers.all().values());
    return this._allModifiers;
  }

  getAllPrepositions(): Array<skill.EntityPreposition> {
    this._allPrepositions = this._allPrepositions ?? [
      { id: 'to', prepositionResource: DefaultResDict.GIVE_TO }
    ];

    return this._allPrepositions;
  }

  getRequiredIndirectsCount(config: skill.SkillConfig): number {
    return 1;
  }

  getRequiredInstrumentsCount(config: skill.SkillConfig): number {
    return 1;
  }

  getIndirects(config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    const indirects = (config.indirects ?? [])
      .filter(obj => (obj instanceof Actor) && obj !== config.actor)
      .map(obj => this.getPrepositionedEntities(obj, config));
    return [].concat.apply([], indirects);
  }

  getPrepositionedEntities(entity: ent.StatelessEntity, config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    let result: Array<skill.PrepositionedEntity> = [];
    if (skill.Skill.has(entity, Take.ID)) {
      result.push({ preposition: this._prepositionsMap.get('to'), entity: entity });
    }
    return result;
  }
}
