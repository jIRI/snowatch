import { GameData } from 'snowatch/game-data';
import { Actor } from 'snowatch/actor';
import { DialogueLine } from 'snowatch/dialogue-line';
import { BusRequest } from 'snowatch/utils';
import { broadcast, message } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { Vector3 } from 'snowatch/spatial-relations-base-types';
import { Attribute } from 'snowatch/attribute';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { DefaultResDict } from 'snowatch/resources/default';
import * as skill from 'snowatch/skill';
import * as ent from 'snowatch/entity';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';
import * as lang from 'snowatch/language-structures';

export class SayModifiers {
  public static readonly neutrally: skill.SkillModifier = { id: 'neutrally', modifierResource: DefaultResDict.NEUTRALLY, details: { soundLevel: 100 } };
  public static readonly quietly: skill.SkillModifier = { id: 'quietly', modifierResource: DefaultResDict.QUIETLY, details: { soundLevel: 50 } };
  public static readonly whisperingly: skill.SkillModifier = { id: 'whisperingly', modifierResource: DefaultResDict.WHISPERINGLY, details: { soundLevel: 20 } };
  public static readonly loudly: skill.SkillModifier = { id: 'loudly', modifierResource: DefaultResDict.LOUDLY, details: { soundLevel: 200 } };
  public static readonly angrily: skill.SkillModifier = { id: 'angrily', modifierResource: DefaultResDict.ANGRILY, details: { soundLevel: 150 } };
  public static readonly patiently: skill.SkillModifier = { id: 'patiently', modifierResource: DefaultResDict.PATIENTLY, details: { soundLevel: 100 } };

  private static _all: Map<string, skill.SkillModifier>;
  static all(): Map<string, skill.SkillModifier> {
    SayModifiers._all = SayModifiers._all ?? new Map<string, skill.SkillModifier>([
      [SayModifiers.neutrally.id, SayModifiers.neutrally],
      [SayModifiers.quietly.id, SayModifiers.quietly],
      [SayModifiers.whisperingly.id, SayModifiers.whisperingly],
      [SayModifiers.loudly.id, SayModifiers.loudly],
      [SayModifiers.angrily.id, SayModifiers.angrily],
      [SayModifiers.patiently.id, SayModifiers.patiently],
    ]);
    return SayModifiers._all;
  }
}

export class Say extends skill.Skill {
  public static readonly ID = 'Say';

  constructor() {
    super();
  }

  initialize(game: GameData) {
    super.initialize(game);
    return this;
  }

  get verb() {
    return lang.Verb.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.SAY));
  }

  preview(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn): result.ResultObject {
    if (config.actor !== config.direct) {
      if (!Attribute.applies(config.direct, attrs.CanListen, config.actor)) {
        return result.make(result.rejected, undefined, `Object ${config.direct.id} doesn't support listening at this moment.`);
      }

      if (!Attribute.value<boolean>(config.direct, attrs.CanListen, config.actor)) {
        return result.make(result.rejected, undefined, `Object ${config.direct.id} cannot listen at this moment.`);
      }
    }

    return super.preview(state, config, filter);
  }

  async execute(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn) {
    this.logger.debug('execute:', state, config);
    const previewResult = this.preview(state, config, filter);
    if (result.isAccepted(previewResult)) {
      let modifier = SayModifiers.neutrally;
      if (config.modifierId != null) {
        modifier = this._modifiersMap.get(config.modifierId) ?? modifier;
      }
      let soundLevel = SayModifiers.neutrally.details.soundLevel;

      const line = (<DialogueLine>config.indirects[0]);
      // sound goes first to prevent wrong order in speech replies (also: first goes sound, then comprehension)
      const sourceSceneId = Attribute.value<string>(config.actor, attrs.CurrentSceneId, config.actor);
      const sourceLocation = Attribute.value<Vector3>(config.actor, attrs.SpatialLocation, config.actor);
      this.game.bus.notifications.publish(broadcast(Topics.SOUND, {
        entityId: config.actor.id,
        soundLevel: soundLevel,
        duration: line.content.length,
        sourceSceneId: sourceSceneId,
        sourceLocation: sourceLocation,
      }, undefined, config.actor.id));
      this.game.addToGameLog({
        source: config.actor.id,
        action: Say.ID,
        description: `"${line.getContent(config.actor, config.direct, config.details)}"`,
      });
      this.game.bus.notifications.publish(broadcast(Topics.SPEECH, {
        entityId: config.actor.id,
        targetId: config.direct.id,
        dialogueLineId: line.id,
        modifierId: modifier.id,
      }, undefined, config.actor.id));
      line.visit(
        config.actor.state.dialogueLines.get(line.id),
        config.direct.id,
        { modifierId: config.modifierId }
      );
    }
    return previewResult;
  }

  getAllModifiers(): Array<skill.SkillModifier> {
    this._allModifiers = this._allModifiers ?? Array.from<skill.SkillModifier>(SayModifiers.all().values());
    return this._allModifiers;
  }

  getAllPrepositions(): Array<skill.EntityPreposition> {
    this._allPrepositions = this._allPrepositions ?? [];
    return this._allPrepositions;
  }

  getRequiredIndirectsCount(config: skill.SkillConfig): number {
    return 1;
  }

  getIndirects(config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    const indirects = (config.indirects ?? [])
      .filter(dialogueLine => result.isAccepted((<DialogueLine>dialogueLine).isAvailable(config.actor, config.direct, config.details)))
      .map(dialogueLine => this.getPrepositionedEntities(dialogueLine, config));
    return [].concat.apply([], indirects);
  }

  getPrepositionedEntities(entity: ent.StatelessEntity, config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    let result: Array<skill.PrepositionedEntity> = [];
    result.push({ preposition: null, entity: entity });
    return result;
  }
}
