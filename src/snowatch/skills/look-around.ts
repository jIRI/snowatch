import { GameData } from 'snowatch/game-data';
import { Attribute } from 'snowatch/attribute';
import { Knowledge } from 'snowatch/knowledge';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { DefaultResDict } from 'snowatch/resources/default';
import { broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import * as skill from 'snowatch/skill';
import * as ent from 'snowatch/entity';
import * as result from 'snowatch/result';
import * as attrs from 'snowatch/attributes-ids';
import * as lang from 'snowatch/language-structures';

export class LookAround extends skill.Skill {
  public static readonly ID = 'LookAround';

  _indirects: Array<skill.PrepositionedEntity> = [];

  constructor() {
    super();
  }

  initialize(game: GameData) {
    super.initialize(game);
    return this;
  }

  get verb() {
    return lang.Verb.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.LOOK_AROUND));
  }

  preview(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn): result.ResultObject {
    if (!attrs.Attribute.applies(config.direct, attrs.CanLookAround, config.actor)) {
      return result.make(result.rejected, undefined, `Entity ${config.direct.id} doesn't support looking around at this moment.`);
    }

    if (!attrs.Attribute.value<boolean>(config.direct, attrs.CanLookAround, config.actor)) {
      return result.make(result.rejected, undefined, `Item ${config.direct.id} cannot look around at this moment.`);
    }

    return super.preview(state, config, filter);
  }

  async execute(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn) {
    this.logger.debug('execute:', state, config);
    const previewResult = this.preview(state, config, filter);
    if (result.isAccepted(previewResult)) {
      this.game.bus.notifications.publish(broadcast(Topics.RENDER_SCENE_DESCRIPTION, {actorId: config.actor.id}));
    }
    return previewResult;
  }

  getIndirects(config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    this._indirects = this._indirects ?? [];
    return this._indirects;
  }
}
