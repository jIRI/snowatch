import { GameData } from 'snowatch/game-data';
import { Attribute } from 'snowatch/attribute';
import { Knowledge } from 'snowatch/knowledge';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { DefaultResDict } from 'snowatch/resources/default';
import * as skill from 'snowatch/skill';
import * as ent from 'snowatch/entity';
import * as result from 'snowatch/result';
import * as attrs from 'snowatch/attributes-ids';
import * as lang from 'snowatch/language-structures';

export class Learn extends skill.Skill {
  public static readonly ID = 'Learn';

  constructor() {
    super();
  }

  initialize(game: GameData) {
    super.initialize(game);
    return this;
  }

  get verb() {
    return lang.Verb.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.LEARN));
  }

  preview(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn): result.ResultObject {
    if (!Attribute.applies(config.direct, attrs.IsLearnable, config.actor)) {
      return result.make(result.rejected, undefined, `Fact ${config.direct.id} doesn't support learning at this moment.`);
    }

    if (!Attribute.value<boolean>(config.direct, attrs.IsLearnable, config.actor)) {
      return result.make(result.rejected, undefined, `Fact ${config.direct.id} cannot be learnt at this moment.`);
    }

    return super.preview(state, config, filter);
  }

  async execute(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn) {
    this.logger.debug('execute:', state, config);
    const previewResult = this.preview(state, config, filter);
    if (result.isAccepted(previewResult)) {
      Knowledge.add(config.actor, config.direct.id);
    }
    return previewResult;
  }
}
