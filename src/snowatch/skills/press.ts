import { GameData } from 'snowatch/game-data';
import { Attribute } from 'snowatch/attribute';
import { Inventory } from 'snowatch/inventory';
import { SpatialRelations } from 'snowatch/spatial-relations';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { DefaultResDict } from 'snowatch/resources/default';
import * as skill from 'snowatch/skill';
import * as ent from 'snowatch/entity';
import * as result from 'snowatch/result';
import * as attrs from 'snowatch/attributes-ids';
import * as item from 'snowatch/item';
import * as lang from 'snowatch/language-structures';

export class PressModifiers {
  public static readonly strongly: skill.SkillModifier = { id: 'strongly', modifierResource: DefaultResDict.STRONGLY, details: {} };
  public static readonly weakly: skill.SkillModifier = { id: 'weakly', modifierResource: DefaultResDict.WEAKLY, details: {} };

  private static _all: Map<string, skill.SkillModifier>;
  static all(): Map<string, skill.SkillModifier> {
    PressModifiers._all = PressModifiers._all ?? new Map<string, skill.SkillModifier>([
      [PressModifiers.weakly.id, PressModifiers.weakly],
      [PressModifiers.strongly.id, PressModifiers.strongly],
    ]);
    return PressModifiers._all;
  }
}

export class Press extends skill.Skill {
  public static readonly ID = 'Press';

  private _indirects: Array<skill.PrepositionedEntity>;

  constructor() {
    super();
  }

  initialize(game: GameData) {
    super.initialize(game);
    return this;
  }

  get verb() {
    return lang.Verb.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.PRESS));
  }

  preview(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn): result.ResultObject {
    if (!Attribute.applies(config.direct, attrs.IsPressable, config.actor)) {
      return result.make(result.rejected, undefined, `Item ${config.direct.id} doesn't support pressing at this moment.`);
    }

    if (!Attribute.value<boolean>(config.direct, attrs.IsPressable, config.actor)) {
      return result.make(result.rejected, undefined, `Item ${config.direct.id} cannot be pressed at this moment.`);
    }

    return super.preview(state, config, filter);
  }

  async execute(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn) {
    this.logger.debug('execute:', state, config);
    const previewResult = this.preview(state, config, filter);
    // do nothing, pressable item just monitors messages
    // if( result.isAccepted(previewResult) ) {
    // }
    return previewResult;
  }

  getAllModifiers(): Array<skill.SkillModifier> {
    this._allModifiers = this._allModifiers ?? Array.from<skill.SkillModifier>(PressModifiers.all().values());
    return this._allModifiers;
  }

  getIndirects(config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    // no indirects for this command
    this._indirects = this._indirects ?? [];
    return this._indirects;
  }
}
