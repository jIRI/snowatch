import { GameData } from 'snowatch/game-data';
import { Attribute } from 'snowatch/attribute';
import { Inventory } from 'snowatch/inventory';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { DefaultResDict } from 'snowatch/resources/default';
import * as skill from 'snowatch/skill';
import * as ent from 'snowatch/entity';
import * as result from 'snowatch/result';
import * as attrs from 'snowatch/attributes-ids';
import * as lang from 'snowatch/language-structures';

export class Lock extends skill.Skill {
  public static readonly ID = 'Lock';

  private _indirects: Array<skill.PrepositionedEntity>;

  constructor() {
    super();
  }

  initialize(game: GameData) {
    super.initialize(game);
    return this;
  }

  get verb() {
    return lang.Verb.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.LOCK));
  }

  preview(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn): result.ResultObject {
    if (!Attribute.applies(config.direct, attrs.IsLockable, config.actor)) {
      return result.make(result.rejected, undefined, `Entity ${config.direct.id} doesn't support closing at this moment.`);
    }

    if (!Attribute.value<boolean>(config.direct, attrs.IsLockable, config.actor)) {
      return result.make(result.rejected, undefined, `Entity ${config.direct.id} cannot be closed at this moment.`);
    }

    if (Attribute.applies(config.direct, attrs.IsLocked, config.actor) && Attribute.value<boolean>(config.direct, attrs.IsLocked, config.actor)) {
      return result.make(result.rejected, undefined, `Entity ${config.direct.id} cannot be closed at this moment.`);
    }

    return super.preview(state, config, filter);
  }

  async execute(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn) {
    this.logger.debug('execute:', state, config);
    const previewResult = this.preview(state, config, filter);
    if (result.isAccepted(previewResult)) {
      // close
      Attribute.setValue<boolean>(config.direct, attrs.IsLocked, true);
    }
    return previewResult;
  }

  getIndirects(config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    // no indirects for this command
    this._indirects = this._indirects ?? [];
    return this._indirects;
  }
}
