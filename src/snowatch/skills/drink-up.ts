import { GameData } from 'snowatch/game-data';
import { Attribute } from 'snowatch/attribute';
import { Inventory } from 'snowatch/inventory';
import { SpatialRelations } from 'snowatch/spatial-relations';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { DefaultResDict } from 'snowatch/resources/default';
import { Substance } from 'snowatch/substance';
import { broadcast, message } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import * as skill from 'snowatch/skill';
import * as ent from 'snowatch/entity';
import * as result from 'snowatch/result';
import * as attrs from 'snowatch/attributes-ids';
import * as item from 'snowatch/item';
import * as lang from 'snowatch/language-structures';

export class DrinkUpModifiers {
  public static readonly bitOfIt: skill.SkillModifier = { id: 'bit-of-it', modifierResource: DefaultResDict.BIT_OF_IT, details: { amount: 0.1 } };
  public static readonly halfOfIt: skill.SkillModifier = { id: 'half-of-it', modifierResource: DefaultResDict.HALF_OF_IT, details: { amount: 0.5 } };
  public static readonly allOfIt: skill.SkillModifier = { id: 'all-of-it', modifierResource: DefaultResDict.ALL_OF_IT, details: { amount: 1.0 } };

  private static _all: Map<string, skill.SkillModifier>;
  static all(): Map<string, skill.SkillModifier> {
    DrinkUpModifiers._all = DrinkUpModifiers._all ?? new Map<string, skill.SkillModifier>([
      [DrinkUpModifiers.bitOfIt.id, DrinkUpModifiers.bitOfIt],
      [DrinkUpModifiers.halfOfIt.id, DrinkUpModifiers.halfOfIt],
      [DrinkUpModifiers.allOfIt.id, DrinkUpModifiers.allOfIt],
    ]);
    return DrinkUpModifiers._all;
  }
}

export class DrinkUp extends skill.Skill {
  public static readonly ID = 'DrinkUp';

  private _indirects: Array<skill.PrepositionedEntity>;

  constructor() {
    super();
  }

  initialize(game: GameData) {
    super.initialize(game);
    return this;
  }

  get verb() {
    return lang.Verb.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.DRINK_UP));
  }

  preview(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn): result.ResultObject {
    if (!Attribute.applies(config.direct, attrs.IsDrinkable, config.actor)) {
      return result.make(result.rejected, undefined, `Item ${config.direct.id} doesn't support drinking at this moment.`);
    }

    if (!Attribute.value<boolean>(config.direct, attrs.IsDrinkable, config.actor)) {
      return result.make(result.rejected, undefined, `Item ${config.direct.id} cannot be drunk at this moment.`);
    }

    const containerId = Attribute.value<string>(config.direct, attrs.ContainerId, config.actor);
    if ( containerId != null) {
      const container = this.game.instances.getContainerFromId(containerId);
      if( container == null ) {
        return result.make(result.rejected, undefined, `Container ${containerId} cannot be found.`);
      }

      if( !Attribute.value<string>(container, attrs.ContainerSupportsDrinking, config.actor) ) {
        return result.make(result.rejected, undefined, `Container ${containerId} doesn't support drinking.`);
      }
    }

    return super.preview(state, config, filter);
  }

  async execute(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn) {
    this.logger.debug('execute:', state, config);
    const previewResult = this.preview(state, config, filter);
    if (result.isAccepted(previewResult)) {
      let amount = 1.0;
      if (config.modifierId != null) {
        const modifier = DrinkUpModifiers.all().get(config.modifierId);
        if (modifier != null) {
          amount = modifier.details.amount;
        }
      }

      Substance.decreaseVolumeRelative(config.direct, amount);

      config.actor.game.bus.notifications.publish(broadcast(
        Topics.DRUNK,
        { actorId: config.actor.id, substanceId: config.direct.id, amount: amount },
        undefined,
        config.actor.id));
    }
    return previewResult;
  }

  getAllModifiers(): Array<skill.SkillModifier> {
    this._allModifiers = this._allModifiers ?? Array.from<skill.SkillModifier>(DrinkUpModifiers.all().values());
    return this._allModifiers;
  }

  getIndirects(config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    // no indirects for this command
    this._indirects = this._indirects ?? [];
    return this._indirects;
  }

}
