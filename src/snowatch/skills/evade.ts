import { GameData } from 'snowatch/game-data';
import { Attribute } from 'snowatch/attribute';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { DefaultResDict } from 'snowatch/resources/default';
import { broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import * as skill from 'snowatch/skill';
import * as result from 'snowatch/result';
import * as attrs from 'snowatch/attributes-ids';
import * as lang from 'snowatch/language-structures';

export class Evade extends skill.Skill {
  public static readonly ID = 'Evade';

  private _indirects: Array<skill.PrepositionedEntity>;

  constructor() {
    super();
  }

  initialize(game: GameData) {
    super.initialize(game);
    return this;
  }

  get verb() {
    return lang.Verb.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.EVADE));
  }

  get isRealTime(): boolean {
    return true;
  }

  preview(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn): result.ResultObject {
    if (!Attribute.applies(config.direct, attrs.IsEvadeable, config.actor)) {
      return result.make(result.rejected, undefined, `Entity ${config.direct.id} doesn't support evading at this moment.`);
    }

    if (!Attribute.value<boolean>(config.direct, attrs.IsEvadeable, config.actor)) {
      return result.make(result.rejected, undefined, `Entity ${config.direct.id} cannot be evaded at this moment.`);
    }

    return super.preview(state, config, filter);
  }

  async execute(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn) {
    this.logger.debug('execute:', state, config);
    const previewResult = this.preview(state, config, filter);
    if (result.isAccepted(previewResult)) {
      // evading can be performed anytime when oponent is engaged, however in case it happens after the attack wind up, the damage will be taken.
      // also even after the evading the actor can be engaged again by the oponent in case the oponent is agroed and the actor is in attack range
      this.game.bus.notifications.publish(broadcast(Topics.EVADING, {
          actorId: config.actor.id,
          targetId: config.direct.id,
        },
        undefined,
        config.actor.id
      ));
      this.game.bus.notifications.publish(broadcast(Topics.EVADED, {
          actorId: config.actor.id,
          targetId: config.direct.id,
        },
        undefined,
        config.actor.id
      ));
    }
    return previewResult;
  }

  getDirectPrepositionId() {
    return ResourceDictionary.get(this.game, DefaultResDict.EVADE_SOMEONE);
  }

  getIndirects(config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    this._indirects = this._indirects ?? [];
    return this._indirects;
  }
}
