import { GameData } from 'snowatch/game-data';
import { Attribute } from 'snowatch/attribute';
import { Inventory } from 'snowatch/inventory';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { DefaultResDict } from 'snowatch/resources/default';
import * as skill from 'snowatch/skill';
import * as ent from 'snowatch/entity';
import * as result from 'snowatch/result';
import * as attrs from 'snowatch/attributes-ids';
import * as lang from 'snowatch/language-structures';

export class Unlock extends skill.Skill {
  public static readonly ID = 'Unlock';

  private _indirects: Array<skill.PrepositionedEntity>;

  constructor() {
    super();
  }

  initialize(game: GameData) {
    super.initialize(game);
    return this;
  }

  get verb() {
    return lang.Verb.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.UNLOCK));
  }

  preview(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn): result.ResultObject {
    if (!Attribute.applies(config.direct, attrs.IsUnlockable, config.actor)) {
      return result.make(result.rejected, undefined, `Entity ${config.direct.id} doesn't support opening at this moment.`);
    }

    if (!Attribute.value<boolean>(config.direct, attrs.IsUnlockable, config.actor)) {
      return result.make(result.rejected, undefined, `Entity ${config.direct.id} cannot be open at this moment.`);
    }

    if (Attribute.applies(config.direct, attrs.IsUnlocked, config.actor) && Attribute.value<boolean>(config.direct, attrs.IsUnlocked, config.actor)) {
      return result.make(result.rejected, undefined, `Entity ${config.direct.id} cannot be open at this moment.`);
    }

    return super.preview(state, config, filter);
  }

  async execute(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn) {
    this.logger.debug('execute:', state, config);
    const previewResult = this.preview(state, config, filter);
    if (result.isAccepted(previewResult)) {
      Attribute.setValue<boolean>(config.direct, attrs.IsUnlocked, true);
    }
    return previewResult;
  }

  getIndirects(config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    // no indirects for this command
    this._indirects = this._indirects ?? [];
    return this._indirects;
  }
}
