import { GameData } from 'snowatch/game-data';
import { Actor } from 'snowatch/actor';
import { Learn } from 'snowatch/skills/learn';
import { BusRequest } from 'snowatch/utils';
import { Topic, Message, broadcast, message } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { Attribute } from 'snowatch/attribute';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { DefaultResDict } from 'snowatch/resources/default';
import * as skill from 'snowatch/skill';
import * as ent from 'snowatch/entity';
import * as result from 'snowatch/result';
import * as attrs from 'snowatch/attributes-ids';
import * as lang from 'snowatch/language-structures';

export class Teach extends skill.Skill {
  public static readonly ID = 'Teach';

  constructor() {
    super();
  }

  initialize(game: GameData) {
    super.initialize(game);
    return this;
  }

  get verb() {
    return lang.Verb.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.TEACH));
  }

  preview(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn): result.ResultObject {
    if (!Attribute.applies(config.direct, attrs.IsTeachable, config.actor)) {
      return result.make(result.rejected, undefined, `Fact ${config.direct.id} doesn't support teachable at this moment.`);
    }

    if (!Attribute.value<boolean>(config.direct, attrs.IsTeachable, config.actor)) {
      return result.make(result.rejected, undefined, `Fact ${config.direct.id} cannot be taught at this moment.`);
    }

    return super.preview(state, config, filter);
  }

  async execute(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn) {
    this.logger.debug('execute:', state, config);
    const previewResult = this.preview(state, config, filter);
    if (result.isAccepted(previewResult)) {
      // try invoke learning
      const request = new BusRequest({
        requestBus: this.game.bus.commands,
        responseBus: this.game.bus.notifications,
        resolveFilter: (m: Message) => m.topicId === Topic.id(Topics.SKILL_ACCEPTED),
        rejectFilter: (m: Message) => m.topicId === Topic.id(Topics.SKILL_REJECTED)
      });
      const targetActor = config.indirects[0];
      let skillResult = await request.send(broadcast(
        Topics.TRY_USE_SKILL,
        {
          skillId: Learn.ID,
          actorId: targetActor.id,
          directId: config.direct.id,
          modifierId: null,
          indirectsIds: [targetActor.id],
        },
        undefined,
        config.actor.id
      ));
      return skillResult;
    }
    return previewResult;
  }
}
