import { GameData } from 'snowatch/game-data';
import { Attribute } from 'snowatch/attribute';
import { Container, getContainerId } from 'snowatch/container';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { DefaultResDict } from 'snowatch/resources/default';
import { Substance } from 'snowatch/substance';
import { broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { clamp } from 'snowatch/utils';
import { SpatialPrepositions } from 'snowatch/prepositions';
import * as skill from 'snowatch/skill';
import * as ent from 'snowatch/entity';
import * as result from 'snowatch/result';
import * as attrs from 'snowatch/attributes-ids';
import * as spatial from 'snowatch/spatial-relations';
import * as lang from 'snowatch/language-structures';

export class PourModifiers {
  public static readonly bitOfIt: skill.SkillModifier = { id: 'bit-of-it', modifierResource: DefaultResDict.BIT_OF_IT, details: { amount: 0.1 } };
  public static readonly halfOfIt: skill.SkillModifier = { id: 'half-of-it', modifierResource: DefaultResDict.HALF_OF_IT, details: { amount: 0.5 } };
  public static readonly allOfIt: skill.SkillModifier = { id: 'all-of-it', modifierResource: DefaultResDict.ALL_OF_IT, details: { amount: 1.0 } };

  private static _all: Map<string, skill.SkillModifier>;
  static all(): Map<string, skill.SkillModifier> {
    PourModifiers._all = PourModifiers._all ?? new Map<string, skill.SkillModifier>([
      [PourModifiers.bitOfIt.id, PourModifiers.bitOfIt],
      [PourModifiers.halfOfIt.id, PourModifiers.halfOfIt],
      [PourModifiers.allOfIt.id, PourModifiers.allOfIt],
    ]);
    return PourModifiers._all;
  }
}

export class Pour extends skill.Skill {
  public static readonly ID = 'Pour';

  private _indirects: Array<skill.PrepositionedEntity>;

  constructor() {
    super();
  }

  initialize(game: GameData) {
    super.initialize(game);
    return this;
  }

  get verb() {
    return lang.Verb.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.POUR));
  }

  preview(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn): result.ResultObject {
    if (!Attribute.applies(config.direct, attrs.IsPourable, config.actor)) {
      return result.make(result.rejected, undefined, `Item ${config.direct.id} doesn't support pouring at this moment.`);
    }

    if (!Attribute.value<boolean>(config.direct, attrs.IsPourable, config.actor)) {
      return result.make(result.rejected, undefined, `Item ${config.direct.id} cannot be poured at this moment.`);
    }

    const containerId = Attribute.value<string>(config.direct, attrs.ContainerId, config.actor);
    if ( containerId != null) {
      const container = this.game.instances.getContainerFromId(containerId);
      if( container == null ) {
        return result.make(result.rejected, undefined, `Container ${containerId} cannot be found.`);
      }

      if( !Attribute.value<string>(container, attrs.ContainerSupportsPouring, config.actor) ) {
        return result.make(result.rejected, undefined, `Container ${containerId} doesn't support pouring.`);
      }
    }

    return super.preview(state, config, filter);
  }

  async execute(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn) {
    this.logger.debug('execute:', state, config);
    const previewResult = this.preview(state, config, filter);
    if (result.isAccepted(previewResult)) {
      let amount = 1.0;
      if (config.modifierId != null) {
        const modifier = PourModifiers.all().get(config.modifierId);
        if (modifier != null) {
          amount = modifier.details.amount;
        }
      }

      const target = <ent.Entity>config.indirects[0];
      let pouredSubstance: Substance | null = null;
      let spatialRelationTypeId = config.prepositionId;
      if( config.prepositionId === SpatialPrepositions.isIn.id ) {
        const availableVolumeResult = spatial.volumeAvailableForSubstance(target);
        if( result.isRejected(availableVolumeResult) ) {
          return availableVolumeResult;
        }

        const availableVolume = (<spatial.AvailableVolume>(availableVolumeResult.data)).volume;
        pouredSubstance = Substance.split(config.direct, clamp(amount, 0, availableVolume));
        if( pouredSubstance == null ) {
          return result.make(result.rejected);
        }

        Container.add(target, pouredSubstance.id);
        spatial.SpatialRelations.addRelation(this.game, config.prepositionId, pouredSubstance.id, target.id);
      } else if( config.prepositionId != null ) {
        pouredSubstance = Substance.split(config.direct, amount);
        if( pouredSubstance == null ) {
          return result.make(result.rejected);
        }

        spatial.SpatialRelations.addRelation(this.game, config.prepositionId, pouredSubstance.id, target.id);
      }

      if( pouredSubstance != null && spatialRelationTypeId != null) {
        // and now, when we did all that work, we will check whether we should merge/join the new substance with substance which might
        // already exist in the relation with the target
        const targetSpatialRelations = spatial.SpatialRelations.findAll(target.game, {indirectId: target.id, typeId: config.prepositionId});
        for(let relation of targetSpatialRelations) {
          if( relation.directId === pouredSubstance.id ) {
            continue;
          }

          const otherSubstanceInRelationWithTarget = this.game.instances.substances.get(relation.directId);
          if( otherSubstanceInRelationWithTarget != null ) {
            Substance.join(otherSubstanceInRelationWithTarget, pouredSubstance);
            break;
          }
        }

        config.actor.game.bus.notifications.publish(broadcast(
          Topics.POURED,
          { actorId: config.actor.id, substanceId: pouredSubstance.id, targetId: target.id, spatialRelationTypeId: spatialRelationTypeId, amount: amount },
          undefined,
          config.actor.id));
      }
    }
    return previewResult;
  }

  getAllModifiers(): Array<skill.SkillModifier> {
    this._allModifiers = this._allModifiers ?? Array.from<skill.SkillModifier>(PourModifiers.all().values());
    return this._allModifiers;
  }

  getAllPrepositions(): Array<skill.EntityPreposition> {
    this._allPrepositions = this._allPrepositions ?? Array.from(SpatialPrepositions.allById().values(), sr => {
      return { id: sr.id, prepositionResource: sr.prepositionActionResource };
    });
    return this._allPrepositions;
  }

  getRequiredIndirectsCount(config: skill.SkillConfig): number {
    return 1;
  }

  getRequiredInstrumentsCount(config: skill.SkillConfig): number {
    return 1;
  }

  getIndirects(config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    const indirects = super.getIndirects(config);
    const directSpatialSize = spatial.getSpatialSize(config.direct);
    const directVolume = spatial.getVolume(config.direct) ?? 0;
    return [].concat.apply([], indirects)
      .filter((indirect: skill.PrepositionedEntity) => {
        // no preposition means no putting
        if (indirect.preposition == null) {
          return false;
        }

        // can't pour things into container they are in
        if( getContainerId(config.direct) === indirect.entity.id ) {
          return false;
        }

        const availableVolumeResult = spatial.volumeAvailableForSubstance(indirect.entity);
        const availableVolume = <spatial.AvailableVolume>(availableVolumeResult.data);
        // if there is no volume available, reject the indirect
        if( availableVolume.volume <= 0 ) {
          return false;
        }

        // we will fill whatever volume is available by splitting the original volume
        return true;
      });
  }

  getPrepositionedEntities(entity: ent.StatelessEntity, config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    const result: Array<skill.PrepositionedEntity> = [];
    if (attrs.Attribute.applies(entity, attrs.Surfaces)) {
      const surfaces = attrs.Attribute.value<Array<string>>(entity, attrs.Surfaces);
      surfaces.forEach(s => {
        const prep = SpatialPrepositions.allBySurface().get(s);
        if( prep != null) {
          result.push({ preposition: this._prepositionsMap.get(prep.id), entity: entity });
        }
      });
    }

    if (attrs.Attribute.applies(entity, attrs.IsContainer) && attrs.Attribute.value<boolean>(entity, attrs.IsContainer)) {
      result.push({ preposition: this._prepositionsMap.get(SpatialPrepositions.isIn.id), entity: entity });
    }

    return result;
  }
}
