import { GameData } from 'snowatch/game-data';
import { Attribute } from 'snowatch/attribute';
import { Entity } from 'snowatch/entity';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { DefaultResDict } from 'snowatch/resources/default';
import { FightCalculator } from 'snowatch/fight-calculator';
import { Topics } from 'snowatch/message-topics/all';
import { broadcast } from 'snowatch/message';
import { BusRequest, clamp } from 'snowatch/utils';
import { TimeSource } from 'snowatch/time-source';
import { DefaultFightCalculator } from 'snowatch/calculators/default-fight-calculator';
import * as skill from 'snowatch/skill';
import * as result from 'snowatch/result';
import * as attrs from 'snowatch/attributes-ids';
import * as lang from 'snowatch/language-structures';

export class Dodge extends skill.Skill {
  public static readonly ID = 'Dodge';

  private _indirects: Array<skill.PrepositionedEntity>;

  private _calc: FightCalculator;

  constructor() {
    super();
  }

  initialize(game: GameData) {
    super.initialize(game);
    this._calc = new DefaultFightCalculator();
    return this;
  }

  get verb() {
    return lang.Verb.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.DODGE));
  }

  get isRealTime(): boolean {
    return true;
  }

  preview(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn): result.ResultObject {
    if (!Attribute.applies(config.actor, attrs.HasDodgeAvailable, config.actor)) {
      return result.make(result.rejected, undefined, `Entity ${config.actor.id} doesn't support dodging at this moment.`);
    }

    if (!Attribute.value<boolean>(config.actor, attrs.HasDodgeAvailable, config.actor)) {
      return result.make(result.rejected, undefined, `Entity ${config.actor.id} cannot dodge at this moment.`);
    }

    const { ownStaminaConsumptionDodge, ownStamina } = this.getDodgeMetrics(config);

    if( ownStaminaConsumptionDodge > ownStamina.value ) {
      return result.make(result.rejected, undefined, `Entity ${config.direct.id} doesn't have enough stamina (dodge needs ${ownStaminaConsumptionDodge} but available is ${ownStamina}).`);
    }

    return super.preview(state, config, filter);
  }

  async execute(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn) {
    this.logger.debug('execute:', state, config);
    const previewResult = this.preview(state, config, filter);
    if (result.isAccepted(previewResult)) {
      const { attacker, cooldownMillis, ownStaminaConsumptionDodge, ownStamina } = this.getDodgeMetrics(config);

      this.game.bus.notifications.publish(broadcast(
        Topics.DODGING, {
          actorId: config.actor.id,
          targetId: attacker.id,
        },
        undefined,
        config.actor.id
      ));

      // consume stamina
      ownStamina.value = Math.round(clamp(ownStamina.value - ownStaminaConsumptionDodge, ownStamina.minValue, ownStamina.maxValue));
      attrs.Attribute.setValue(config.actor, attrs.Stamina, ownStamina);

      // send dodge request
      const dodgeRequest = new BusRequest({
        requestBus: this.game.bus.notifications,
        responseBus: this.game.bus.notifications,
        clockTickBus: this.game.bus.system,
        timeoutWallClockTicks: TimeSource.getWallClockTicksFromNow(this.game, cooldownMillis),
      });
      await dodgeRequest.send(broadcast(
        Topics.DODGE, {
          actorId: config.actor.id,
          targetId: attacker.id,
          dodgeCooldownMillis: cooldownMillis,
        },
        undefined,
        config.actor.id
      ));

      this.game.bus.notifications.publish(broadcast(
        Topics.DODGED, {
          actorId: config.actor.id,
          targetId: attacker.id,
        },
        undefined,
        config.actor.id
      ));
    }

    return previewResult;
  }

  getRequiredIndirectsCount(config: skill.SkillConfig): number {
    return 0;
  }

  getIndirects(config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    this._indirects = this._indirects ?? [];
    return this._indirects;
  }

  getRequiredInstrumentsCount(config: skill.SkillConfig): number {
    return 0;
  }

  private getDodgeMetrics(config: skill.SkillConfig) {
    const attacker = config.direct as Entity;

    // get windup delay
    const cooldownMillis = this._calc.dodgeCoolDownTime(config.actor);
    const ownStamina = attrs.Attribute.value<attrs.NumericValueAttribute>(config.actor, attrs.Stamina, config.actor);
    const ownStaminaConsumptionDodge = this._calc.dodgeStaminaConsumption(config.actor);
    return {
        attacker,
        cooldownMillis,
        ownStamina,
        ownStaminaConsumptionDodge,
      };
  }
}
