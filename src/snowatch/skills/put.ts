import { GameData } from 'snowatch/game-data';
import { Inventory } from 'snowatch/inventory';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { DefaultResDict } from 'snowatch/resources/default';
import { SpatialRelations, areaSurface } from 'snowatch/spatial-relations';
import { SpatialPrepositions } from 'snowatch/prepositions';
import * as skill from 'snowatch/skill';
import * as ent from 'snowatch/entity';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';
import * as lang from 'snowatch/language-structures';
import * as spatial from 'snowatch/spatial-relations';

export class PutModifiers {
  public static readonly quickly: skill.SkillModifier = { id: 'quickly', modifierResource: DefaultResDict.QUICKLY, details: {} };
  public static readonly hesitantly: skill.SkillModifier = { id: 'hesitantly', modifierResource: DefaultResDict.HESITANTLY, details: {} };
  public static readonly cautiously: skill.SkillModifier = { id: 'cautiously', modifierResource: DefaultResDict.CAUTIOUSLY, details: {} };

  private static _all: Map<string, skill.SkillModifier>;
  static all(): Map<string, skill.SkillModifier> {
    PutModifiers._all = PutModifiers._all ?? new Map<string, skill.SkillModifier>([
      [skill.SkillModifiers.neutrally.id, skill.SkillModifiers.neutrally],
      [PutModifiers.quickly.id, PutModifiers.quickly],
      [PutModifiers.hesitantly.id, PutModifiers.hesitantly],
      [PutModifiers.cautiously.id, PutModifiers.cautiously],
    ]);
    return PutModifiers._all;
  }
}

export class Put extends skill.Skill {
  public static readonly ID = 'Put';

  constructor() {
    super();
  }

  initialize(game: GameData) {
    super.initialize(game);
    return this;
  }

  get verb() {
    return lang.Verb.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.PUT));
  }

  preview(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn): result.ResultObject {
    if (!attrs.Attribute.applies(config.direct, attrs.IsPutable, config.actor)) {
      return result.make(result.rejected, undefined, `Item ${config.direct.id} doesn't support putting at this moment.`);
    }

    if (!attrs.Attribute.value<boolean>(config.direct, attrs.IsPutable, config.actor)) {
      return result.make(result.rejected, undefined, `Item ${config.direct.id} cannot be put at this moment.`);
    }

    if (ent.getHolderId(config.direct) !== config.actor.id) {
      return result.make(result.rejected, undefined, `Item ${config.direct.id} cannot be put at this moment.`);
    }

    return super.preview(state, config, filter);
  }

  async execute(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn) {
    this.logger.debug('execute:', state, config);
    const previewResult = this.preview(state, config, filter);
    if (result.isAccepted(previewResult)) {
      Inventory.removeItem(config.actor, config.direct.id);
      // create spatial relation
      if (config.indirects.length > 0) {
        for (let indirect of config.indirects) {
          if (config.prepositionId != null) {
            spatial.SpatialRelations.addRelation(this.game, config.prepositionId, config.direct.id, indirect.id);
          }
        }
      }
    }
    return previewResult;
  }

  getAllModifiers(): Array<skill.SkillModifier> {
    this._allModifiers = this._allModifiers ?? Array.from<skill.SkillModifier>(PutModifiers.all().values());
    return this._allModifiers;
  }

  getAllPrepositions(): Array<skill.EntityPreposition> {
    this._allPrepositions = this._allPrepositions ?? Array.from(SpatialPrepositions.allById().values(), sr => {
      return { id: sr.id, prepositionResource: sr.prepositionActionResource };
    });
    return this._allPrepositions;
  }

  getIndirects(config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    const indirects = super.getIndirects(config);
    const directSpatialSize = spatial.getSpatialSize(config.direct);
    return [].concat.apply([], indirects)
      .filter((indirect: skill.PrepositionedEntity) => {
        // no preposition means no putting
        if (indirect.preposition == null) {
          return false;
        }

        // things in inventories are excluded
        if (Inventory.hasInAny(config.actor, indirect.entity.id)) {
          return false;
        }

        // things can't be put in container if they do not fit in
        if (indirect.preposition.id === SpatialPrepositions.isIn.id && !result.isAccepted(spatial.doesEntityFitIntoContainer(config.direct, indirect.entity, 0))) {
          return false;
        }

        // no put under objects which direct does not fit under
        if ((indirect.preposition.id === SpatialPrepositions.isUnder.id ?? indirect.preposition.id === SpatialPrepositions.isBelow.id) && !result.isAccepted(spatial.doesEntityFitUnder(config.direct, indirect.entity, 0))) {
          return false;
        }

        // no put on top of objects which are out of reach
        if ( indirect.preposition.id === SpatialPrepositions.isOn.id && !result.isAccepted(spatial.canActorReachTopSurface(config.actor, indirect.entity, 0.35))) {
          return false;
        }

        // things which already has any spatial relation with the other entity are excluded
        if (spatial.SpatialRelations.find(this.game, config.direct.id, indirect.entity.id, indirect.preposition.id)) {
          return false;
        }

        // in case direct has no spatial size, we can use the indirect at this point
        if( directSpatialSize == null ) {
          return true;
        }

        // if the preposition of existing relation is the same as the one we are currently considering, check whether there is enough space available to fit the direct
        if( !attrs.Attribute.hasComp(indirect.entity, attrs.AvailableSpatialCapacity) ) {
          // no spatial capacity? no putting, sir.
          return false;
        }

        const availableCapacity = attrs.Attribute.value<attrs.SpatialCapacityMap>(indirect.entity, attrs.AvailableSpatialCapacity, config.actor);
        const relationDef = SpatialPrepositions.allById().get(indirect.preposition.id);
        if( relationDef == null ) {
          // this should not happen, but we need to be sure...
          return false;
        }

        const capacityForSurface = availableCapacity.get(relationDef.targetSurfaceId);
        if(capacityForSurface == null) {
          // there is no spatial capacity defined, we assume there is all the space we need
          return false;
        }

        if(capacityForSurface.value < areaSurface(relationDef.targetSurfaceId, directSpatialSize)) {
          return false;
        }

        return true;
      });
  }

  getRequiredIndirectsCount(config: skill.SkillConfig): number {
    return 1;
  }

  getRequiredInstrumentsCount(config: skill.SkillConfig): number {
    return 1;
  }

  getPrepositionedEntities(entity: ent.StatelessEntity, config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    const result: Array<skill.PrepositionedEntity> = [];
    if (attrs.Attribute.applies(entity, attrs.Surfaces)) {
      const surfaces = attrs.Attribute.value<Array<string>>(entity, attrs.Surfaces);
      surfaces.forEach(s => {
        const prep = SpatialPrepositions.allBySurface().get(s);
        if( prep != null) {
          result.push({ preposition: this._prepositionsMap.get(prep.id), entity: entity });
        }
      });
    }

    if (attrs.Attribute.applies(entity, attrs.IsContainer) && attrs.Attribute.value<boolean>(entity, attrs.IsContainer)) {
      result.push({ preposition: this._prepositionsMap.get(SpatialPrepositions.isIn.id), entity: entity });
    }

    return result;
  }
}
