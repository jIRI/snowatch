import { GameData } from 'snowatch/game-data';
import { Attribute } from 'snowatch/attribute';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { DefaultResDict } from 'snowatch/resources/default';
import { Topics } from 'snowatch/message-topics/all';
import { broadcast } from 'snowatch/message';
import { Entity } from 'snowatch/entity';
import * as skill from 'snowatch/skill';
import * as result from 'snowatch/result';
import * as attrs from 'snowatch/attributes-ids';
import * as lang from 'snowatch/language-structures';

export class Engage extends skill.Skill {
  public static readonly ID = 'Engage';

  private _indirects: Array<skill.PrepositionedEntity>;

  constructor() {
    super();
  }

  initialize(game: GameData) {
    super.initialize(game);
    return this;
  }

  get verb() {
    return lang.Verb.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.ENGAGE));
  }

  preview(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn): result.ResultObject {
    if (!Attribute.applies(config.direct, attrs.CanEngage, config.actor)) {
      return result.make(result.rejected, undefined, `Entity ${config.actor.id} doesn't support engaging at this moment.`);
    }

    if (!Attribute.value<boolean>(config.direct, attrs.CanEngage, config.actor)) {
      return result.make(result.rejected, undefined, `Entity ${config.direct.id} cannot engage at this moment.`);
    }

    if (config.direct === config.actor) {
      return result.make(result.rejected, undefined, `Entity ${config.direct.id} cannot engage with itself.`);
    }

    return super.preview(state, config, filter);
  }

  async execute(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn) {
    this.logger.debug('execute:', state, config);
    const previewResult = this.preview(state, config, filter);
    if (result.isAccepted(previewResult)) {
      // engaging can be performed at any time if the oponent is in engage range
      // for NPCs there is small cooldown after successful engage which prevents immediate attack which gives human player to get ready to react by either evading or blocking or counter attack
      const engagedEntity = config.direct as Entity;
      this.game.bus.notifications.publish(broadcast(Topics.ENGAGING, {
          actorId: config.actor.id,
          targetId: engagedEntity.id,
        },
        undefined,
        config.actor.id
      ));
      this.game.bus.notifications.publish(broadcast(Topics.ENGAGED, {
          actorId: config.actor.id,
          targetId: engagedEntity.id,
        },
        undefined,
        config.actor.id
      ));
    }
    return previewResult;
  }

  getDirectPrepositionId() {
    return ResourceDictionary.get(this.game, DefaultResDict.ENGAGE_SOMEONE);
  }

  getIndirects(config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    this._indirects = this._indirects ?? []
    return this._indirects;
  }
}
