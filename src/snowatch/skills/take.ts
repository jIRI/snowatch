import { GameData } from 'snowatch/game-data';
import { Attribute } from 'snowatch/attribute';
import { Inventory } from 'snowatch/inventory';
import { getContainerId } from 'snowatch/container';
import { SpatialRelations } from 'snowatch/spatial-relations';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { DefaultResDict } from 'snowatch/resources/default';
import * as skill from 'snowatch/skill';
import * as ent from 'snowatch/entity';
import * as result from 'snowatch/result';
import * as attrs from 'snowatch/attributes-ids';
import * as lang from 'snowatch/language-structures';

export class TakeModifiers {
  public static readonly quickly: skill.SkillModifier = { id: 'quickly', modifierResource: DefaultResDict.QUICKLY, details: {} };
  public static readonly hesitantly: skill.SkillModifier = { id: 'hesitantly', modifierResource: DefaultResDict.HESITANTLY, details: {} };
  public static readonly cautiosly: skill.SkillModifier = { id: 'cautiosly', modifierResource: DefaultResDict.CAUTIOUSLY, details: {} };

  private static _all: Map<string, skill.SkillModifier>;
  static all(): Map<string, skill.SkillModifier> {
    TakeModifiers._all = TakeModifiers._all ?? new Map<string, skill.SkillModifier>([
      [skill.SkillModifiers.neutrally.id, skill.SkillModifiers.neutrally],
      [TakeModifiers.quickly.id, TakeModifiers.quickly],
      [TakeModifiers.hesitantly.id, TakeModifiers.hesitantly],
      [TakeModifiers.cautiosly.id, TakeModifiers.cautiosly],
    ]);
    return TakeModifiers._all;
  }
}

export class Take extends skill.Skill {
  public static readonly ID = 'Take';

  private _indirects: Array<skill.PrepositionedEntity>;

  constructor() {
    super();
  }

  initialize(game: GameData) {
    super.initialize(game);
    return this;
  }

  get verb() {
    return lang.Verb.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.TAKE));
  }

  preview(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn): result.ResultObject {
    if (!Attribute.applies(config.direct, attrs.IsTakeable, config.actor)) {
      return result.make(result.rejected, undefined, `Item ${config.direct.id} doesn't support taking at this moment.`);
    }

    if (!Attribute.value<boolean>(config.direct, attrs.IsTakeable, config.actor)) {
      return result.make(result.rejected, undefined, `Item ${config.direct.id} cannot be taken at this moment.`);
    }

    const holderId = ent.getHolderId(config.direct);
    if (holderId != null) {
      return result.make(result.rejected, undefined, `Item ${config.direct.id} cannot be taken at this moment.`);
    }

    const containerId = getContainerId(config.direct);
    if (containerId != null) {
      const container = this.game.instances.getEntityFromId(containerId);
      if (container == null
        || (attrs.Attribute.applies(container, attrs.IsClosed, config.actor) && attrs.Attribute.value<boolean>(container, attrs.IsClosed, config.actor))
      ) {
        return result.make(result.rejected, undefined, `Item ${config.direct.id} cannot be taken at this moment.`);
      }
    }

    return super.preview(state, config, filter);
  }

  async execute(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn) {
    this.logger.debug('execute:', state, config);
    const previewResult = this.preview(state, config, filter);
    if (result.isAccepted(previewResult)) {
      Inventory.addItem(config.actor, config.direct.id);
      SpatialRelations.removeRelationsOf(this.game, config.direct.id);
    }
    return previewResult;
  }

  getRequiredInstrumentsCount(config: skill.SkillConfig): number {
    return 1;
  }

  getAllModifiers(): Array<skill.SkillModifier> {
    this._allModifiers = this._allModifiers ?? Array.from<skill.SkillModifier>(TakeModifiers.all().values());
    return this._allModifiers;
  }

  getIndirects(config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    // no indirects for this command
    this._indirects = this._indirects ?? [];
    return this._indirects;
  }
}
