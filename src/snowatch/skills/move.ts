import { GameData } from 'snowatch/game-data';
import { Inventory } from 'snowatch/inventory';
import { Surface } from 'snowatch/surface';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { DefaultResDict } from 'snowatch/resources/default';
import { Actor } from 'snowatch/actor';
import { SpatialRelations, areaSurface } from 'snowatch/spatial-relations';
import { SpatialPrepositions } from 'snowatch/prepositions';
import * as skill from 'snowatch/skill';
import * as ent from 'snowatch/entity';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';
import * as lang from 'snowatch/language-structures';
import * as spatial from 'snowatch/spatial-relations';

export class MoveModifiers {
  public static readonly quickly: skill.SkillModifier = { id: 'quickly', modifierResource: DefaultResDict.QUICKLY, details: {} };
  public static readonly hesitantly: skill.SkillModifier = { id: 'slowly', modifierResource: DefaultResDict.SLOWLY, details: {} };

  private static _all: Map<string, skill.SkillModifier>;
  static all(): Map<string, skill.SkillModifier> {
    MoveModifiers._all = MoveModifiers._all ?? new Map<string, skill.SkillModifier>([
      [skill.SkillModifiers.neutrally.id, skill.SkillModifiers.neutrally],
      [MoveModifiers.quickly.id, MoveModifiers.quickly],
      [MoveModifiers.hesitantly.id, MoveModifiers.hesitantly],
    ]);
    return MoveModifiers._all;
  }
}

export class Move extends skill.Skill {
  public static readonly ID = 'Move';

  public static DefaultPreviewFilter(game: GameData, state: any, config: skill.SkillConfig, previewData: skill.PreviewData): result.ResultObject {
    return result.make(result.accepted);
  }

  public static DefaultExecFilter(game: GameData, state: any, config: skill.SkillConfig, previewData: skill.PreviewData): result.ResultObject {
    return result.make(result.accepted);
  }

  constructor() {
    super();
  }

  initialize(game: GameData) {
    super.initialize(game);
    return this;
  }

  get verb() {
    return lang.Verb.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.MOVE));
  }

  preview(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn): result.ResultObject {
    if (!attrs.Attribute.applies(config.direct, attrs.CanMove, config.actor)) {
      return result.make(result.rejected, undefined, `Entity ${config.direct.id} doesn't support moving at this moment.`);
    }

    if (!attrs.Attribute.value<boolean>(config.direct, attrs.CanMove, config.actor)) {
      return result.make(result.rejected, undefined, `Item ${config.direct.id} cannot be moved at this moment.`);
    }

    return super.preview(state, config, filter);
  }

  async execute(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn) {
    this.logger.debug('execute:', state, config);
    const previewResult = this.preview(state, config, filter);
    if (result.isAccepted(previewResult)) {
      // remove previous relations of the actor
      spatial.SpatialRelations.removeRelationsOf(this.game, config.actor.id);
      // create spatial relation
      if (config.indirects.length > 0) {
        for (let indirect of config.indirects) {
          if (config.prepositionId != null) {
            spatial.SpatialRelations.addRelation(this.game, config.prepositionId, config.actor.id, indirect.id);
            spatial.setSpatialLocation(config.actor, Move.getLocation(indirect, config.actor, config.prepositionId));
          }
        }
      }
    }
    return previewResult;
  }

  getAllModifiers(): Array<skill.SkillModifier> {
    this._allModifiers = this._allModifiers ?? Array.from<skill.SkillModifier>(MoveModifiers.all().values());
    return this._allModifiers;
  }

  getAllPrepositions(): Array<skill.EntityPreposition> {
    this._allPrepositions = this._allPrepositions ?? Array.from(SpatialPrepositions.allById().values(), sr => {
      return { id: sr.id, prepositionResource: sr.prepositionActionResource };
    });
    return this._allPrepositions;
  }

  getIndirects(config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    const indirects = super.getIndirects(config);
    const actorSpatialSize = spatial.getSpatialSize(config.actor);
    return [].concat.apply([], indirects)
      .filter((indirect: skill.PrepositionedEntity) => {
        // no preposition means no moving
        if (indirect.preposition == null) {
          return false;
        }

        // things in inventories are excluded
        if (Inventory.hasInAny(config.actor, indirect.entity.id)) {
          return false;
        }

        // things can't be moved in container if they do not fit in
        if (indirect.preposition.id === SpatialPrepositions.isIn.id && !result.isAccepted(spatial.doesEntityFitIntoContainer(config.direct, indirect.entity, 0))) {
          return false;
        }

        // no move under objects which actor does not fit under
        if ((indirect.preposition.id === SpatialPrepositions.isUnder.id || indirect.preposition.id === SpatialPrepositions.isBelow.id) && !result.isAccepted(spatial.doesEntityFitUnder(config.direct, indirect.entity, -0.5))) {
          return false;
        }

        // no move on top of objects which are out of reach
        if ( indirect.preposition.id === SpatialPrepositions.isOn.id && !result.isAccepted(spatial.canActorReachTopSurface(config.actor, indirect.entity, 0.25))) {
          return false;
        }

        // things which already has any spatial relation with the other entity are excluded
        if (spatial.SpatialRelations.find(this.game, config.direct.id, indirect.entity.id, indirect.preposition.id)) {
          return false;
        }

        // in case direct has no spatial size, we can use the indirect at this point
        if( actorSpatialSize == null ) {
          return true;
        }

        // if the preposition of existing relation is the same as the one we are currently considering, check whether there is enough space available to fit the direct
        if( !attrs.Attribute.hasComp(indirect.entity, attrs.AvailableSpatialCapacity) ) {
          // no spatial capacity? no moving, sir.
          return false;
        }

        const availableCapacity = attrs.Attribute.value<attrs.SpatialCapacityMap>(indirect.entity, attrs.AvailableSpatialCapacity, config.actor);
        const relationDef = SpatialPrepositions.allById().get(indirect.preposition.id);
        if( relationDef == null ) {
          // this should not happen, but we need to be sure...
          return false;
        }

        const capacityForSurface = availableCapacity.get(relationDef.targetSurfaceId);
        if(capacityForSurface == null) {
          // there is no spatial capacity defined, we assume there is all the space we need
          return false;
        }

        if(capacityForSurface.value < areaSurface(relationDef.targetSurfaceId, actorSpatialSize)) {
          return false;
        }

        // prevent moving higher than actor can reach


        return true;
      });
  }

  getRequiredIndirectsCount(config: skill.SkillConfig) {
    return 1;
  }

  getPrepositionedEntities(entity: ent.Entity, config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    const result: Array<skill.PrepositionedEntity> = [];
    if (attrs.Attribute.applies(entity, attrs.Surfaces)) {
      const surfaces = attrs.Attribute.value<Array<string>>(entity, attrs.Surfaces);
      surfaces.forEach(s => {
        const prep = SpatialPrepositions.allBySurface().get(s);
        if( prep != null) {
          result.push({ preposition: this._prepositionsMap.get(prep.id), entity: entity });
        }
      });
    }

    if (attrs.Attribute.applies(entity, attrs.IsContainer) && attrs.Attribute.value<boolean>(entity, attrs.IsContainer)) {
      result.push({ preposition: this._prepositionsMap.get(SpatialPrepositions.isIn.id), entity: entity });
    }

    return result;
  }

  static getLocation(obj: ent.StatelessEntity, actor: Actor, prepositionId: string): spatial.Vector3 | null {
    const location = spatial.getSpatialLocation(obj);
    if (location == null) {
      return spatial.getSpatialLocation(actor);
    }

    const size = spatial.getSpatialSize(obj);
    if (size == null) {
      return location;
    }

    let shellThickness = spatial.Zero3;
    if (attrs.Attribute.applies(obj, attrs.ShellThickness, actor)) {
      shellThickness = attrs.Attribute.value(obj, attrs.ShellThickness, actor);
    }

    switch (prepositionId) {
      case Surface.outerTop:
        return spatial.addVect3(location, spatial.mulVect3(size, spatial.BaseZ));
      case Surface.innerTop:
        return spatial.addVect3(location, spatial.addVect3(spatial.mulVect3(size, spatial.BaseZ), spatial.mulVect3(shellThickness, spatial.BaseNegZ)));
      case Surface.outerBottom:
        return spatial.addVect3(location, spatial.mulVect3(size, spatial.BaseNegZ));
      case Surface.innerBottom:
        return spatial.addVect3(location, spatial.addVect3(spatial.mulVect3(size, spatial.BaseNegZ), spatial.mulVect3(shellThickness, spatial.BaseZ)));
      case Surface.outerLeft:
        return spatial.addVect3(location, spatial.mulVect3(size, spatial.BaseNegX));
      case Surface.innerLeft:
        return spatial.addVect3(location, spatial.addVect3(spatial.mulVect3(size, spatial.BaseNegX), spatial.mulVect3(shellThickness, spatial.BaseX)));
      case Surface.outerRight:
        return spatial.addVect3(location, spatial.mulVect3(size, spatial.BaseX));
      case Surface.innerRight:
        return spatial.addVect3(location, spatial.addVect3(spatial.mulVect3(size, spatial.BaseX), spatial.mulVect3(shellThickness, spatial.BaseNegX)));
      case Surface.outerFront:
        return spatial.addVect3(location, spatial.mulVect3(size, spatial.BaseNegY));
      case Surface.innerFront:
        return spatial.addVect3(location, spatial.addVect3(spatial.mulVect3(size, spatial.BaseNegY), spatial.mulVect3(shellThickness, spatial.BaseY)));
      case Surface.outerBack:
        return spatial.addVect3(location, spatial.mulVect3(size, spatial.BaseY));
      case Surface.innerBack:
        return spatial.addVect3(location, spatial.addVect3(spatial.mulVect3(size, spatial.BaseY), spatial.mulVect3(shellThickness, spatial.BaseNegY)));
    }

    return location;
  }
}
