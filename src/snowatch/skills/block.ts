import { GameData } from 'snowatch/game-data';
import { Attribute } from 'snowatch/attribute';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { DefaultResDict } from 'snowatch/resources/default';
import { Entity } from 'snowatch/entity';
import { AccusativePrepositions } from 'snowatch/prepositions';
import { broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { FightCalculator } from 'snowatch/fight-calculator';
import { DefaultFightCalculator } from 'snowatch/calculators/default-fight-calculator';
import { BusRequest, clamp } from 'snowatch/utils';
import * as skill from 'snowatch/skill';
import * as ent from 'snowatch/entity';
import * as result from 'snowatch/result';
import * as attrs from 'snowatch/attributes-ids';
import * as lang from 'snowatch/language-structures';
import { TimeSource } from 'snowatch/time-source';

export class Block extends skill.Skill {
  public static readonly ID = 'Block';

  private _indirects: Array<skill.PrepositionedEntity>;

  private _calc: FightCalculator;

  constructor() {
    super();
  }

  initialize(game: GameData) {
    super.initialize(game);
    this._calc = new DefaultFightCalculator();
    return this;
  }

  get verb() {
    return lang.Verb.get(this.game, ResourceDictionary.get(this.game, DefaultResDict.BLOCK));
  }

  get isRealTime(): boolean {
    return true;
  }

  preview(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn): result.ResultObject {
    if (!Attribute.applies(config.direct, attrs.IsBlockable, config.actor)) {
      return result.make(result.rejected, undefined, `Entity ${config.direct.id} doesn't support blocking at this moment.`);
    }

    if (!Attribute.value<boolean>(config.direct, attrs.IsBlockable, config.actor)) {
      return result.make(result.rejected, undefined, `Entity ${config.direct.id} cannot be blocked at this moment.`);
    }

    const { ownStaminaConsumptionBlock, ownStamina } = this.getBlockMetrics(config);

    if( ownStaminaConsumptionBlock > ownStamina.value ) {
      return result.make(result.rejected, undefined, `Entity ${config.direct.id} doesn't have enough stamina (block needs ${ownStaminaConsumptionBlock} but available is ${ownStamina}).`);
    }

    return super.preview(state, config, filter);
  }

  async execute(state: any, config: skill.SkillConfig, filter: skill.SkillFilterFn) {
    this.logger.debug('execute:', state, config);
    const previewResult = this.preview(state, config, filter);
    if (result.isAccepted(previewResult)) {
      const attackingEntity = config.direct as Entity;

      const { attacker, instrument, windupMillis, ownStamina, ownStaminaConsumptionBlock } = this.getBlockMetrics(config);

      this.game.bus.notifications.publish(broadcast(
        Topics.BLOCKING, {
          actorId: config.actor.id,
          targetId: attacker.id,
          instrumentId: instrument.id,
        },
        undefined,
        config.actor.id
      ));

      // consume stamina
      ownStamina.value = Math.round(clamp(ownStamina.value - ownStaminaConsumptionBlock, ownStamina.minValue, ownStamina.maxValue));
      attrs.Attribute.setValue(config.actor, attrs.Stamina, ownStamina);

      // send dodge request
      const dodgeRequest = new BusRequest({
        requestBus: this.game.bus.notifications,
        responseBus: this.game.bus.notifications,
        clockTickBus: this.game.bus.system,
        timeoutWallClockTicks: TimeSource.getWallClockTicksFromNow(this.game, windupMillis),
      });
      await dodgeRequest.send(broadcast(
        Topics.BLOCK, {
          actorId: config.actor.id,
          targetId: attacker.id,
          instrumentId: instrument.id,
          blockWindupMillis: windupMillis,
        },
        undefined,
        config.actor.id
      ));

      this.game.bus.notifications.publish(broadcast(
        Topics.BLOCKED, {
          actorId: config.actor.id,
          targetId: attacker.id,
          instrumentId: instrument.id,
        },
        undefined,
        config.actor.id
      ));
    }
    return previewResult;
  }

  private getBlockMetrics(config: skill.SkillConfig) {
    const attacker = config.direct as Entity;
    const instrument = config.instruments[0];
    const instrumentId = instrument.id;

    // get windup delay
    const windupMillis = this._calc.blockWindUpTime(config.actor, instrument);
    const ownStamina = attrs.Attribute.value<attrs.NumericValueAttribute>(config.actor, attrs.Stamina, config.actor);
    const ownStaminaConsumptionBlock = this._calc.blockStaminaConsumption(config.actor, instrument);
    return {
        attacker,
        instrument,
        windupMillis,
        ownStamina,
        ownStaminaConsumptionBlock,
      };
  }

  getDirectPrepositionId() {
    return ResourceDictionary.get(this.game, DefaultResDict.ACCUSATIVE_ON);
  }

  getAllPrepositions(): Array<skill.EntityPreposition> {
    this._allPrepositions = this._allPrepositions ?? [];
    return this._allPrepositions;
  }

  getRequiredIndirectsCount(config: skill.SkillConfig): number {
    return 1;
  }

  getRequiredInstrumentsCount(config: skill.SkillConfig): number {
    return 1;
  }

  getIndirects(config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    const indirects = (config.indirects ?? [])
      .filter(obj => Attribute.value<boolean>(obj, attrs.IsBlockable, config.actor) && obj !== config.actor)
      .map(obj => this.getPrepositionedEntities(obj, config));
      return [].concat.apply([], indirects);
  }

  getPrepositionedEntities(entity: ent.StatelessEntity, config: skill.SkillConfig): Array<skill.PrepositionedEntity> {
    let result: Array<skill.PrepositionedEntity> = [];
    if (attrs.Attribute.value<boolean>(entity, attrs.IsBlockable)) {
      result.push({ preposition: this._prepositionsMap.get(AccusativePrepositions.on.id), entity: entity });
    }
    return result;
  }
}
