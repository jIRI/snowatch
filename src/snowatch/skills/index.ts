import { FrameworkConfiguration } from 'aurelia-framework';

import * as skills from 'snowatch/skills/all';

export function configure(aurelia: FrameworkConfiguration) {
  aurelia.singleton(skills.Skill, skills.Take);
  aurelia.singleton(skills.Skill, skills.Give);
  aurelia.singleton(skills.Skill, skills.Put);
  aurelia.singleton(skills.Skill, skills.Learn);
  aurelia.singleton(skills.Skill, skills.Teach);
  aurelia.singleton(skills.Skill, skills.Say);
  aurelia.singleton(skills.Skill, skills.Open);
  aurelia.singleton(skills.Skill, skills.Close);
  aurelia.singleton(skills.Skill, skills.Lock);
  aurelia.singleton(skills.Skill, skills.Unlock);
  aurelia.singleton(skills.Skill, skills.Move);
  aurelia.singleton(skills.Skill, skills.Press);
  aurelia.singleton(skills.Skill, skills.DrinkUp);
  aurelia.singleton(skills.Skill, skills.LookAround);
  aurelia.singleton(skills.Skill, skills.Pour);
  aurelia.singleton(skills.Skill, skills.Engage);
  aurelia.singleton(skills.Skill, skills.Attack);
  aurelia.singleton(skills.Skill, skills.Block);
  aurelia.singleton(skills.Skill, skills.Dodge);
  aurelia.singleton(skills.Skill, skills.Evade);
}
