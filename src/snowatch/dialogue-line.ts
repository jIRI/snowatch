import {GameData} from 'snowatch/game-data';
import {Actor} from 'snowatch/actor';
import {SayModifiers} from 'snowatch/skills/say';
import {Topic, message, broadcast} from 'snowatch/message';
import {Topics} from 'snowatch/message-topics/all';
import * as skills from 'snowatch/skills/all';
import * as attrs from 'snowatch/attributes-ids';
import * as ent from 'snowatch/entity';
import * as utils from 'snowatch/utils';
import * as result from 'snowatch/result';

export interface Intention {
  id: string;
}

export interface LineFilterFn {
  (game: GameData, line: DialogueLine, lineState: any, actor: ent.Entity, target: ent.Entity, details: any): result.ResultObject;
}

export interface LineScoreFn {
  (game: GameData, line: DialogueLine, lineState: any, actor: ent.Entity, target: ent.Entity, details: any): number;
}

export interface LineModifierFn {
  (game: GameData, line: DialogueLine, lineState: any, actor: ent.Entity, target: ent.Entity, details: any): skills.SkillModifier;
}

export interface LineContentFn {
  (game: GameData, line: DialogueLine, lineState: any, actor: ent.Entity, target: ent.Entity, details: any) : any;
}

export interface DialogueLineProperties {
      dialogueLines: Map<string, DialogueLineInit>;
}

export interface DialogueLineInit extends ent.HasId {
      intentions: Array<string>;
      title: string;
      content: LineContentFn;
      knowledge?: Array<string>;
      filter?: LineFilterFn;
      lineScore?: LineScoreFn;
      modifier?: LineModifierFn;
      initialState?: any;
}

export class Intentions {
  public static readonly neutral: Intention = {id: "neutral"};
  public static readonly sarcastic: Intention = {id: "sarcastic"};
  public static readonly ironic: Intention = {id: "ironic"};
  public static readonly pathetic: Intention = {id: "pathetic"};
  public static readonly informal: Intention = {id: "informal"};
  public static readonly formal: Intention = {id: "formal"};
  public static readonly warm: Intention = {id: "warm"};
  public static readonly cold: Intention = {id: "cold"};

  private static _all: Map<string, Intention>;
  static all(): Map<string, Intention> {
    Intentions._all = Intentions._all ?? new Map<string, Intention>([
      [Intentions.neutral.id, Intentions.neutral],
      [Intentions.sarcastic.id, Intentions.sarcastic],
      [Intentions.ironic.id, Intentions.ironic],
      [Intentions.pathetic.id, Intentions.pathetic],
      [Intentions.informal.id, Intentions.informal],
      [Intentions.formal.id, Intentions.formal],
      [Intentions.warm.id, Intentions.warm],
      [Intentions.cold.id, Intentions.cold],
    ]);
    return Intentions._all;
  }
}

export class DialogueLine extends ent.StatelessEntity {
  static ensureIsInstanceOf(obj: DialogueLine | DialogueLineInit) : DialogueLine {
   if( obj instanceof DialogueLine ) {
     return obj;
   }
   return new DialogueLine(obj);
  }

  public static OBJECT_GROUP = 'dialogueLines';

  static registrator(game: GameData, o: DialogueLine | DialogueLineInit) : DialogueLine {
    const obj = DialogueLine.ensureIsInstanceOf(o);
    return obj.initialize(game);
  }

  static initializeDialogueLines(target: ent.Entity) {
    target.logger.debug(`${target.id}: initializeDialogueLine()`);
    target.state.dialogueLines = target.state.dialogueLines ?? new Map<string, any>();
    target.state.dialogueLog = target.state.dialogueLog ?? new Map<string, any>();
    // log what they say to me
    target.game.bus.notifications.subscribe({
      receiver: target,
      filter: (m) =>  {
        return m.topicId === Topic.id(Topics.SPEECH) && m.payload.targetId === target.id;
      },
      handler: (m) =>  {
        // use entityId => one log per character i speak to
        if( !target.state.dialogueLog.has(m.payload.entityId) ) {
          target.state.dialogueLog.set(m.payload.entityId, [m.payload,]);
        } else {
          target.state.dialogueLog.get(m.payload.entityId).push(m.payload);
        }
      }
    });
    // log what i say to them
    target.game.bus.notifications.subscribe({
      receiver: target,
      filter: (m) =>  {
        return m.topicId === Topic.id(Topics.SPEECH) && m.payload.entityId === target.id && m.payload.entityId !== m.payload.targetId;
      },
      handler: (m) =>  {
        // use targetId => one log per character who speaks to me
        if( !target.state.dialogueLog.has(m.payload.targetId) ) {
          target.state.dialogueLog.set(m.payload.targetId, [m.payload,]);
        } else {
          target.state.dialogueLog.get(m.payload.targetId).push(m.payload);
        }
      }
    });
  }

  static getIds(target: ent.Entity): Array<string> {
    return Array.from<string>(target.state.dialogueLines.keys());
  }

  static getLineState(target: ent.Entity, dialogueLineId: string): any {
    return target.state.dialogueLines.get(dialogueLineId);
  }

  static add(target: ent.Entity, dialogueLineId: string, dialogueLine: DialogueLineInit) {
    if( !target.state.dialogueLines.has(dialogueLineId) ) {
      target.logger.debug(`Adding DialogueLine ${dialogueLineId} (${dialogueLine})`);
      target.state.dialogueLines.set(dialogueLineId, dialogueLine.initialState ?? {});
      target.game.bus.notifications.publish(broadcast(Topics.DIALOG_LINE_ADDED, { entityId: target.id, dialogueLineId: dialogueLineId }));
    }
  }

  static addFromProperties(target: ent.Entity, properties: DialogueLineProperties) {
    if( properties != null && properties.dialogueLines != null ) {
      for(let [dialogueLineId, dialogueLine] of properties.dialogueLines) {
        DialogueLine.add(target, dialogueLineId, dialogueLine);
      }
    }
  }

  static remove(target: ent.Entity, lineId: string) {
    target.logger.debug(`Removing DialogueLine ${lineId}`);
    target.state.dialogueLines.delete(lineId);

    target.game.bus.notifications.publish(broadcast(Topics.DIALOG_LINE_REMOVED, { entityId: target.id, dialogueLineId: lineId }, undefined, target.id));
  }

  static removeAll(target: ent.Entity) {
    target.logger.debug(`Clearing all dialogueLines`);
    const dialogueLine = Array.from<string>(target.state.dialogueLines.keys());
    for(let lineId of dialogueLine) {
      DialogueLine.remove(target, lineId);
    }
  }

  init: DialogueLineInit;

  constructor(init: DialogueLineInit | null = null) {
    super(init, DialogueLine.OBJECT_GROUP);
  }

  initialize(game: GameData, init?: DialogueLineInit) {
    super.initialize(game, init);
    const self = this;
    attrs.Attribute.initializeAttributes(this);
    attrs.Attribute.addConst(this, attrs.Title, { constState: { value: self.title} });
    return this;
  }

  get title() {
    return this.init.title;
  }

  get content() {
    return this.init.content;
  }

  get knowledgeIds() {
    return this.init.knowledge ?? [];
  }

  get intentions() {
    return this.init.intentions ?? [];
  }

  visit(state: any, targetId: string, details: any) {
    state.visits = state.visits ?? new Map<string, any>();
    if( !state.visits.has(targetId) ) {
      state.visits.set(targetId, [details, ]);
    } else {
      state.visits.get(targetId).push(details);
    }
  }

  visits(state: any, targetId: string) {
    state.visits = state.visits ?? new Map<string, any>();
    return state.visits.get(targetId) ?? [];
  }

  isAvailable(actor: ent.Entity, target: ent.Entity, details: any) {
    const props = ent.getProperties(actor, DialogueLine.OBJECT_GROUP, this.id);
    if( props.filter != null ) {
      return props.filter(this.game, this, ent.getState(actor, DialogueLine.OBJECT_GROUP, this.id), actor, target, details);
    } else {
      return result.make(result.accepted);
    }
  }

  getContent(actor: ent.Entity, target: ent.Entity, details: any) : string {
    const props = ent.getProperties(actor, DialogueLine.OBJECT_GROUP, this.id);
    const lineState = ent.getState(actor, DialogueLine.OBJECT_GROUP, this.id);
    lineState.contentCallCount = lineState.contentCallCount ?? 0;
    const content = props.content(this.game, this, lineState, actor, target, details);
    let result = "...";
    if( utils.isString(content) ) {
      result = content;
    } else if( Array.isArray(content) && content.length > 0 )  {
      if( lineState.contentCallCount >= content.length ) {
        lineState.contentCallCount = 0;
      }
      result = content[lineState.contentCallCount];
      lineState.contentCallCount++;
    } else {
      if( Array.isArray(content.variants) && content.variants.length > lineState.contentCallCount ) {
        result = content.variants[lineState.contentCallCount];
        lineState.contentCallCount++;
      } else if( content.default != null ) {
        result = content.default;
      }
    }

    return result;
  }

}
