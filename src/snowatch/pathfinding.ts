//
// Based on nice article at http://www.redblobgames.com/pathfinding/a-star/introduction.html
// Basically simplest explanation of A* I've seen so far...
//
import {Entity} from 'snowatch/entity';
import {PriorityQueue} from 'snowatch/collections';

export interface HeuristicFn {
  (actor: Entity, first: GraphNode, second: GraphNode): number;
}

export interface GraphNode {
}

export interface GraphLink {
}

export interface PathElement {
  node: GraphNode;
  link: GraphLink | null;
}

export interface LinkNeighborPair {
  link: GraphLink;
  neighbor: GraphNode;
}

export interface Graph {
  neighbors(actor: Entity, node: GraphNode): Iterable<LinkNeighborPair>;
  cost(actor: Entity, start: GraphNode, target: GraphNode): number;
  validatePath(actor: Entity, path: Array<PathElement>): boolean;
}

export interface NodeScorePair {
  node: GraphNode;
  score: number;
}

export function find(actor: Entity, graph: Graph, start: GraphNode, targetNode: GraphNode, heuristic: HeuristicFn): Array<PathElement> {
  const frontier = new PriorityQueue<NodeScorePair>((f, s) => f.score - s.score);
  frontier.push({node: start, score: 0});
  const cameFrom = new Map<GraphNode, PathElement | null>();
  const costSoFar = new Map<GraphNode, number>();
  cameFrom.set(start, null);
  costSoFar.set(start, 0);

  while( !frontier.isEmpty() ) {
    const pair = frontier.pop();
    if( pair == null ) {
      continue;
    }

    const currentNode = pair.node;
    if( currentNode === targetNode ) {
      break;
    }

    for(let next of graph.neighbors(actor, currentNode) ) {
      const costSoFarForCurrentNode = costSoFar.get(currentNode) ?? 0;
      const new_cost = costSoFarForCurrentNode + graph.cost(actor, currentNode, next.neighbor);

      const costSoFarForNextNode = costSoFar.get(next) ?? 0;
      if( !costSoFar.has(next.neighbor) || new_cost < costSoFarForNextNode ) {
        costSoFar.set(next.neighbor, new_cost);
        const score = new_cost + heuristic(actor, targetNode, next.neighbor);
        frontier.push({ node: next.neighbor, score: score });
        cameFrom.set(next.neighbor, { node: currentNode, link: next.link });
      }
    }
  }

  const path = new Array<PathElement>();
  let currentNode: GraphNode | null | undefined = targetNode;
  path.push({ node: currentNode, link: null });
  while( currentNode !== start ) {
    if( currentNode == null ) {
      // something wrong happend, should never be null
      break;
    }

    const pathElement = cameFrom.get(currentNode);
    if( pathElement != null && pathElement.node != null) {
      // push valid elements only
      path.push(pathElement);
    }

    currentNode = pathElement != null ? pathElement.node : null;
  }

  path.reverse();
  if( !graph.validatePath(actor, path) ) {
    // path is not valid, return path containing just start node
    return [{ node: start, link: null }];
  }

  return path;
}
