import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity, deregisterEntities } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface RemoveWhenVolumeZeroState extends BehaviorState {
}

export class RemoveWhenVolumeZero extends Behavior {
  public static readonly ID = 'RemoveWhenVolumeZero';

  setup(target: Entity, state: RemoveWhenVolumeZeroState) {
  }

  filter(target: Entity, state: RemoveWhenVolumeZeroState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.SUBSTANCE_VOLUME_DECREASED) ) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.SUBSTANCE_VOLUME_DECREASED>(message);
    return (messagePayload.substanceId === target.id)
      ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: RemoveWhenVolumeZeroState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    const messagePayload = payload<Topics.SUBSTANCE_VOLUME_DECREASED>(message);
    if( messagePayload.newVolume <= 0 ) {
      attrs.Attribute.setValue<boolean>(target, attrs.IsVisible, false);
      target.game.addToGarbage(target);
    }
  }
}