import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface SupportsEngagingState extends BehaviorState {
}

export class SupportsEngaging extends Behavior {
  public static readonly ID = 'SupportsEngaging';

  constructor() {
    super();
  }

  setup(target: Entity, state: SupportsEngagingState) {
    if( !attrs.Attribute.has(target, attrs.ActorMode) ) {
      attrs.Attribute.add(target, attrs.ActorMode, { initialState : { value: attrs.ActorModes.Normal } });
    }
    if( !attrs.Attribute.has(target, attrs.AttackStage) ) {
      attrs.Attribute.add(target, attrs.AttackStage, { initialState : { value: attrs.AttackStages.Ready } });
    }
    if( !attrs.Attribute.has(target, attrs.CanEngage) ) {
      attrs.Attribute.add(target, attrs.CanEngage);
    }
    if( !attrs.Attribute.has(target, attrs.CanBeEngaged) ) {
      attrs.Attribute.add(target, attrs.CanBeEngaged);
    }
    if( !attrs.Attribute.has(target, attrs.IsEngageable) ) {
      attrs.Attribute.add(target, attrs.IsEngageable);
    }
  }

  filter(target: Entity, state: SupportsEngagingState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.ENGAGING) ) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.ENGAGING>(message);
    return (messagePayload.targetId === target.id
      || messagePayload.actorId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: SupportsEngagingState | null, message: Message) {
    const targetMode = attrs.Attribute.value<attrs.ActorModes>(target, attrs.ActorMode);
    switch( targetMode ) {
      case attrs.ActorModes.Engaged: {
        this.logger.debug(`Actor ${target.id} is already engaged`);
        return;
      }
      case attrs.ActorModes.Fighting: {
        this.logger.debug(`Actor ${target.id} is already fighting`);
        return;
      }
      default: {
        this.logger.debug(`Actor ${target.id} is switching to engaged mode`);
        break;
      }
    }

    attrs.Attribute.ifAttributesThenSet(
      [{ entity: target, withAttributeId: attrs.ActorMode }],
      [{ entity: target, withAttributeId: attrs.ActorMode, setValueTo: attrs.ActorModes.Engaged }]
    );
    attrs.Attribute.ifAttributesThenSet(
      [{ entity: target, withAttributeId: attrs.CanAttack, valueMustBe: true }],
      [{ entity: target, withAttributeId: attrs.IsAttackable, setValueTo: true }]
    );
    attrs.Attribute.ifAttributesThenSet(
      [{ entity: target, withAttributeId: attrs.CanEvade, valueMustBe: true }],
      [{ entity: target, withAttributeId: attrs.IsEvadeable, setValueTo: true }]
    );
  }
}