import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface SupportsLockingState extends BehaviorState {
  value: boolean;
}

export class SupportsLocking extends Behavior {
  public static readonly ID = 'SupportsLocking';

  setup(target: Entity, state: SupportsLockingState) {
    let initialValue = true;

    // unlocked things are lockable by default
    if( attrs.Attribute.applies(target, attrs.IsUnlocked) ) {
      initialValue = attrs.Attribute.value<boolean>(target, attrs.IsUnlocked);
    }
    // open things can't be locked
    if( attrs.Attribute.applies(target, attrs.IsOpen) ) {
      initialValue = initialValue && !attrs.Attribute.value<boolean>(target, attrs.IsOpen);
    }

    // we can prevent autoinit by setting the attributes explicitly
    if( !attrs.Attribute.has(target, attrs.IsLocked) ) {
      attrs.Attribute.add(target, attrs.IsLocked, { initialState : { value: !initialValue } });
    }
    if( !attrs.Attribute.has(target, attrs.IsLockable) ) {
      attrs.Attribute.add(target, attrs.IsLockable, { initialState : { value: initialValue } });
    }
  }

  filter(target: Entity, state: SupportsLockingState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.ATTRIBUTE_VALUE_CHANGED) ) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.ATTRIBUTE_VALUE_CHANGED>(message);
    return (messagePayload.attributeId === attrs.IsLocked
      || messagePayload.attributeId === attrs.IsUnlocked
      || messagePayload.attributeId === attrs.IsOpen
      || messagePayload.attributeId === attrs.IsClosed)
      ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: SupportsLockingState | null, message: Message) {
    const messagePayload = payload<Topics.ATTRIBUTE_VALUE_CHANGED>(message);
    switch( messagePayload.attributeId ) {
      case attrs.IsOpen:
        if( attrs.Attribute.applies(target, attrs.IsLockable) ) {
          attrs.Attribute.setValue<boolean>(target, attrs.IsLockable, !messagePayload.newValue);
        }
        break;
      case attrs.IsClosed:
        if( attrs.Attribute.applies(target, attrs.IsLockable) ) {
          attrs.Attribute.setValue<boolean>(target, attrs.IsLockable, messagePayload.newValue);
        }
        break;
      case attrs.IsLocked:
        if( attrs.Attribute.applies(target, attrs.IsLockable) ) {
          attrs.Attribute.setValue<boolean>(target, attrs.IsLockable, !messagePayload.newValue);
        }
        break;
      case attrs.IsUnlocked:
        if( attrs.Attribute.applies(target, attrs.IsLocked) ) {
          attrs.Attribute.setValue<boolean>(target, attrs.IsLocked, !messagePayload.newValue);
        }
        break;
    }
  }
}