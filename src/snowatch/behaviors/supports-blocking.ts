import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface SupportsBlockingState extends BehaviorState {
}

export class SupportsBlocking extends Behavior {
  public static readonly ID = 'SupportsBlocking';

  constructor() {
    super();
  }

  setup(target: Entity, state: SupportsBlockingState) {
    if( !attrs.Attribute.has(target, attrs.ActorMode) ) {
      attrs.Attribute.add(target, attrs.ActorMode, { initialState : { value: attrs.ActorModes.Normal } });
    }
    if( !attrs.Attribute.has(target, attrs.AttackStage) ) {
      attrs.Attribute.add(target, attrs.AttackStage, { initialState : { value: attrs.AttackStages.Ready } });
    }
    if( !attrs.Attribute.has(target, attrs.CanBlock) ) {
      attrs.Attribute.add(target, attrs.CanBlock);
    }
    if( !attrs.Attribute.has(target, attrs.CanBeBlocked) ) {
      attrs.Attribute.add(target, attrs.CanBeBlocked);
    }
    if( !attrs.Attribute.has(target, attrs.IsBlockable) ) {
      attrs.Attribute.add(target, attrs.IsBlockable);
    }
  }

  filter(target: Entity, state: SupportsBlockingState | null, message: Message): result.ResultObject {
    switch( message.topicId ) {
      case Topic.id(Topics.ATTACK_WINDING_UP):
      case Topic.id(Topics.ATTACK_COOLING_DOWN):
      case Topic.id(Topics.ATTACK_CANCELLING):
      case Topic.id(Topics.ATTACK_LANDING):
      case Topic.id(Topics.ATTACK_FINISHING): {
        const messagePayload = payload<Topics.ATTACK_STAGE_CHANGING>(message);
        return (messagePayload.targetId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
      }
      case Topic.id(Topics.BLOCKING): {
        const messagePayload = payload<Topics.BLOCKING>(message);
        return (messagePayload.actorId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
      }
      case Topic.id(Topics.BLOCKED): {
        const messagePayload = payload<Topics.BLOCKED>(message);
        return (messagePayload.actorId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
      }
      default:
        return result.make(result.rejected);
    }
  }

  process(target: Entity, state: SupportsBlockingState | null, message: Message) {
    const messagePayload = payload<Topics.ATTACK_STAGE_CHANGING>(message);
    const attacker = target.game.instances.getEntityFromId(messagePayload.actorId);
    if( attacker  == null ) {
      this.logger.debug(`Unable to find attacker entity ${messagePayload.actorId}`);
      return;
    }

    let isApplicableAttackStage = false;
    switch( attrs.Attribute.valueIfApplies<attrs.AttackStages>(target, attrs.AttackStage, attrs.AttackStages.Ready) ) {
      case attrs.AttackStages.Ready:
      case attrs.AttackStages.Windup:
      case attrs.AttackStages.Landed:
        isApplicableAttackStage = true;
        break;
      default:
        break;
    }

    const blockabilityConditions = [
      { entity: target, withAttributeId: attrs.CanBlock, valueMustBe: true },
      { entity: attacker, withAttributeId: attrs.CanBeBlocked, valueMustBe: true }
    ];
    switch( message.topicId ) {
      case Topic.id(Topics.BLOCKING):
        attrs.Attribute.ifAttributesThenSet(
          [{ entity: target, withAttributeId: attrs.ActorMode, valueMustBe: attrs.ActorModes.Fighting }],
          [{ entity: target, withAttributeId: attrs.ActorMode, setValueTo: attrs.ActorModes.Blocking }]
        );
        break;
      case Topic.id(Topics.BLOCKED):
        attrs.Attribute.ifAttributesThenSet(
          [{ entity: target, withAttributeId: attrs.ActorMode, valueMustBe: attrs.ActorModes.Blocking }],
          [{ entity: target, withAttributeId: attrs.ActorMode, setValueTo: attrs.ActorModes.Fighting }]
        );
        break;
      case Topic.id(Topics.ATTACK_WINDING_UP):
        attrs.Attribute.ifAttributesThenSet(blockabilityConditions, [{ entity: attacker, withAttributeId: attrs.IsBlockable, setValueTo: isApplicableAttackStage }]);
        break;
      case Topic.id(Topics.ATTACK_COOLING_DOWN):
        attrs.Attribute.ifAttributesThenSet(blockabilityConditions, [{ entity: attacker, withAttributeId: attrs.IsBlockable, setValueTo: false }]);
        break;
      case Topic.id(Topics.ATTACK_CANCELLING):
        attrs.Attribute.ifAttributesThenSet(blockabilityConditions, [{ entity: attacker, withAttributeId: attrs.IsBlockable, setValueTo: false }]);
        break;
      case Topic.id(Topics.ATTACK_LANDING):
        attrs.Attribute.ifAttributesThenSet(blockabilityConditions, [{ entity: attacker, withAttributeId: attrs.IsBlockable, setValueTo: false }]);
        break;
      case Topic.id(Topics.ATTACK_FINISHING):
        attrs.Attribute.ifAttributesThenSet(blockabilityConditions, [{ entity: attacker, withAttributeId: attrs.IsBlockable, setValueTo: false }]);
        break;
      default:
        this.logger.debug(`Unknown block stage change for actor ${target.id}: ${message.topicId}`);
        return;
    }
  }
}