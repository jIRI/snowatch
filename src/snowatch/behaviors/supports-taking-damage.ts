import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { clamp } from 'snowatch/utils';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface SupportsTakingDamageState extends BehaviorState {
}

export class SupportsTakingDamage extends Behavior {
  public static readonly ID = 'SupportsTakingDamage';

  constructor() {
    super();
  }

  setup(target: Entity, state: SupportsTakingDamageState) {
    if( !attrs.Attribute.has(target, attrs.CanTakeDamage) ) {
      attrs.Attribute.add(target, attrs.CanTakeDamage);
    }
    if( !attrs.Attribute.has(target, attrs.Health) ) {
      attrs.Attribute.add(target, attrs.Health);
    }
  }

  filter(target: Entity, state: SupportsTakingDamageState | null, message: Message): result.ResultObject {
    switch( message.topicId ) {
      case Topic.id(Topics.ATTACKED):
        break;
      default:
        return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.ATTACKED>(message);
    return (messagePayload.targetId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: SupportsTakingDamageState | null, message: Message) {
    if( !(attrs.Attribute.applies(target, attrs.CanTakeDamage) && attrs.Attribute.value<boolean>(target, attrs.CanTakeDamage)) ) {
      this.logger.debug(`Entity ${target.id} cannot take damage at this moment`);
      return;
    }

    const messagePayload = payload<Topics.ATTACKED>(message);
    let health = attrs.Attribute.value<attrs.NumericValueAttribute>(target, attrs.Health);

    health.value = Math.round(clamp(health.value - messagePayload.damage, health.minValue, health.maxValue));
    attrs.Attribute.setValue(target, attrs.Health, health);

    target.game.bus.notifications.publish(
      broadcast(Topics.TOOK_DAMAGE, {
          entityId: target.id,
          damage: messagePayload.damage,
          damageSourceId: messagePayload.actorId
        },
        undefined,
        target.id
      )
    );

  }
}