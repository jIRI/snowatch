import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Skill } from 'snowatch/skill';
import * as messages from 'snowatch/message';
import * as ent from 'snowatch/entity';
import * as result from 'snowatch/result';
import * as attrs from 'snowatch/attributes-ids';


export interface UpdateAttributeDef {
  attributeId: string;
  attrStateInit: { initialState: { value: any}};
}

export interface UpdateBehaviorDef {
  behaviorId: string;
}

export interface UpdateSkillDef {
  skillId: string;
}

export type UpdateElementDef = UpdateAttributeDef | UpdateBehaviorDef | UpdateSkillDef;

export interface UpdatePropertiesOnMessageState extends BehaviorState {
  topicId: string;
  targetId: string;
  payloadTemplate: any;
  add: Array<UpdateElementDef>;
  remove: Array<UpdateElementDef>;
}

export class UpdatePropertiesOnMessage extends Behavior {
  public static readonly ID = 'UpdatePropertiesOnMessage';

  activate(target: ent.Entity, state: UpdatePropertiesOnMessageState) {
    if( !attrs.Attribute.has(target, attrs.TriggeredAutoupdateMessages) ) {
      attrs.Attribute.add(target, attrs.TriggeredAutoupdateMessages, { initialState: {value: new Set<string>()}});
    }

    const triggeredMessages: Set<string> = attrs.Attribute.value(target, attrs.TriggeredAutoupdateMessages);
    if( triggeredMessages.has(state.topicId) ) {
      UpdatePropertiesOnMessage.doRemoval(target, state);
    }
  }

  filter(target: ent.Entity, state: UpdatePropertiesOnMessageState | null, message: messages.Message): result.ResultObject {
    return (state != null && message.topicId === state.topicId
      && (state.targetId == null || state.targetId === message.targetId )
      && messages.checkPayloadTemplate(state.payloadTemplate, message.payload)
      ) ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: ent.Entity, state: UpdatePropertiesOnMessageState | null, message: messages.Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    UpdatePropertiesOnMessage.doRemoval(target, state);
    UpdatePropertiesOnMessage.doAddition(target, state);
    const triggeredMessages: Set<string> = attrs.Attribute.value(target, attrs.TriggeredAutoupdateMessages);
    triggeredMessages.add(state.topicId);
    attrs.Attribute.setValue<Set<string>>(target, attrs.TriggeredAutoupdateMessages, triggeredMessages);
  }

  private static doAddition(target: ent.Entity, state: UpdatePropertiesOnMessageState) {
    if( Array.isArray(state.add) ) {
      state.add.forEach((item: any) => {
        if( item.hasOwnProperty('skillId') ) {
          Skill.add(target, item.skillId, item.skillInit);
        }
        if( item.hasOwnProperty('behaviorId') ) {
          Behavior.add(target, item.behaviorId, item.behaviorInit);
        }
        if( item.hasOwnProperty('attributeId') ) {
          attrs.Attribute.add(target, item.attributeId, item.attrStateInit);
        }
      });
    }
  }

  private static doRemoval(target: ent.Entity, state: UpdatePropertiesOnMessageState) {
    if( Array.isArray(state.remove) ) {
      state.remove.forEach((item: any) => {
        if( item.hasOwnProperty('skillId') ) {
          Skill.remove(target, item.skillId);
        }
        if( item.hasOwnProperty('behaviorId') ) {
          Behavior.remove(target, item.behaviorId);
        }
        if( item.hasOwnProperty('attributeId') ) {
          attrs.Attribute.remove(target, item.attributeId);
        }
      });
    }
  }
}