import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface ChangeAttributeWhenSpatialRelationAddedState extends BehaviorState {
  types: Array<string>;
  changes: Array<{attributeId: string, typeMatch: any, typeNotMatch: any}>;
}

export class ChangeAttributeWhenSpatialRelationAdded extends Behavior {
  public static readonly ID = 'ChangeAttributeWhenSpatialRelationAdded';

  filter(target: Entity, state: ChangeAttributeWhenSpatialRelationAddedState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.SPATIAL_RELATION_ADDED)) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.SPATIAL_RELATION_ADDED>(message);
    return (state != null && messagePayload.directId === target.id)
      ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: ChangeAttributeWhenSpatialRelationAddedState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    const messagePayload = payload<Topics.SPATIAL_RELATION_ADDED>(message);
    if( state.types.includes(messagePayload.typeId) ) {
      for(let change of state.changes) {
        attrs.Attribute.setValue<any>(target, change.attributeId, change.typeMatch);
      }
    } else {
      for(let change of state.changes) {
        attrs.Attribute.setValue<any>(target, change.attributeId, change.typeNotMatch);
      }
    }
  }
}