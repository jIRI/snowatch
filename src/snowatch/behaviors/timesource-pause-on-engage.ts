import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { TimeSource } from 'snowatch/time-source';
import { Engage } from 'snowatch/skills/all';
import * as result from 'snowatch/result';

export interface TimeSourcePauseWhenEngagedState extends BehaviorState {
}

export class TimeSourcePauseWhenEngaged extends Behavior {
  public static readonly ID = 'TimeSourcePauseWhenEngaged';

  filter(target: Entity, state: TimeSourcePauseWhenEngagedState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.TRYING_USE_SKILL) ) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.TRYING_USE_SKILL>(message);
    return (messagePayload.skillId === Engage.ID)
      ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: TimeSourcePauseWhenEngagedState | null, message: Message) {
    const messagePayload = payload<Topics.TRYING_USE_SKILL>(message);
    if (target.game.currentActor.id === messagePayload.directId || target.game.currentActor.id === messagePayload.actorId) {
      this.logger.debug('Pausing time source because current actor is being engaged');
      TimeSource.pause();
    }
  }
}