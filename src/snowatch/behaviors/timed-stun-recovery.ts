import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { TimeSource } from 'snowatch/time-source';
import { Channel } from 'snowatch/bus';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface TimedStunRecoveryState extends BehaviorState {
  recoverAtTicks: number;
  actorModeAfterRecovery?: attrs.ActorModes;
  actorAttackStageAfterRecovery?: attrs.AttackStages;
}

export class TimedStunRecovery extends Behavior {
  public static readonly ID = 'TimedStunRecovery';

  get busChannel(): Channel | null {
    return this.game.bus.system;
  }

  filter(target: Entity, state: TimedStunRecoveryState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.CLOCK_TICK) || target == null ) {
      return result.make(result.rejected);
    }

    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.CLOCK_TICK>(message);
    return (state.recoverAtTicks == null
        || messagePayload.wallClockTicks >= state.recoverAtTicks
      ) ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: TimedStunRecoveryState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    if( state.actorModeAfterRecovery != null ) {
      attrs.Attribute.ifAttributesThenSet([{ entity: target, withAttributeId: attrs.ActorMode }, ], [{ entity: target, withAttributeId: attrs.ActorMode, setValueTo: state.actorModeAfterRecovery }]);
    }
    if( state.actorAttackStageAfterRecovery != null ) {
      attrs.Attribute.ifAttributesThenSet([{ entity: target, withAttributeId: attrs.AttackStage }, ], [{ entity: target, withAttributeId: attrs.AttackStage, setValueTo: state.actorAttackStageAfterRecovery }]);
    }

    attrs.Attribute.ifAttributesThenSet([{ entity: target, withAttributeId: attrs.CanBeStunned }, ], [{ entity: target, withAttributeId: attrs.CanBeStunned, setValueTo: true }]);
    attrs.Attribute.ifAttributesThenSet([{ entity: target, withAttributeId: attrs.IsStunned }, ], [{ entity: target, withAttributeId: attrs.IsStunned, setValueTo: false }]);

    target.game.bus.notifications.publish(broadcast(Topics.STUN_RECOVERY, {
        actorId: target.id
      },
      undefined,
      target.id)
    );

    Behavior.remove(target, TimedStunRecovery.ID);
  }
}