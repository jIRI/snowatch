import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { getSpatialLocation } from 'snowatch/spatial-relations';
import { getCurrentSceneId } from 'snowatch/scene';
import { DefaultResDict } from 'snowatch/resources/default';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface SmellsWhenEvaporatedState extends BehaviorState {
  descriptionResourceId?: string;
  level?: number;
}

export class SmellsWhenEvaporated extends Behavior {
  public static readonly ID = 'SmellsWhenEvaporated';

  filter(target: Entity, state: SmellsWhenEvaporatedState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.EVAPORATED) ) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.EVAPORATED>(message);
    return ( messagePayload.substanceId === target.id)
      ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: SmellsWhenEvaporatedState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    target.game.bus.notifications.publish(broadcast(Topics.SMELL, {
        substanceId: target.id,
        substanceNounId:  attrs.Attribute.value<string>(target, attrs.NounResourceId),
        smellDescriptionResourceId: state.descriptionResourceId ?? DefaultResDict.ACTOR_SMELL_DESCRIPTION,
        smellLevel: state.level ?? 1,
        sourceSceneId: getCurrentSceneId(target),
        sourceLocation: getSpatialLocation(target),
      },
      undefined,
      target.id)
    );
  }
}