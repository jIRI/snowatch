import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface SupportsDyingState extends BehaviorState {
}

export class SupportsDying extends Behavior {
  public static readonly ID = 'SupportsDying';

  constructor() {
    super();
  }

  setup(target: Entity, state: SupportsDyingState) {
    if( !attrs.Attribute.has(target, attrs.Health) ) {
      attrs.Attribute.add(target, attrs.Health);
    }
    if( !attrs.Attribute.has(target, attrs.CanDie) ) {
      attrs.Attribute.add(target, attrs.CanDie);
    }
    if( !attrs.Attribute.has(target, attrs.IsDead) ) {
      attrs.Attribute.add(target, attrs.IsDead);
    }
  }

  filter(target: Entity, state: SupportsDyingState | null, message: Message): result.ResultObject {
    switch( message.topicId ) {
      case Topic.id(Topics.ATTRIBUTE_VALUE_CHANGED):
        break;
      default:
        return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.ATTRIBUTE_VALUE_CHANGED>(message);
    return (messagePayload.entityId === target.id
        && messagePayload.attributeId === attrs.Health)
      ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: SupportsDyingState | null, message: Message) {
    let health = attrs.Attribute.value<attrs.NumericValueAttribute>(target, attrs.Health);

    if( health.value <= 0
      && attrs.Attribute.applies(target, attrs.CanDie) && attrs.Attribute.value<boolean>(target, attrs.CanDie)
    ) {
      attrs.Attribute.setValue<boolean>(target, attrs.IsDead, true);
      attrs.Attribute.ifAttributesThenSet([{ entity: target, withAttributeId: attrs.ActorMode }, ], [{ entity: target, withAttributeId: attrs.ActorMode, setValueTo: attrs.ActorModes.Normal }]);
      attrs.Attribute.ifAttributesThenSet([{ entity: target, withAttributeId: attrs.AttackStage }, ], [{ entity: target, withAttributeId: attrs.ActorMode, setValueTo: attrs.AttackStages.Ready }]);
      target.game.bus.notifications.publish(
        broadcast(Topics.DIED, {
            entityId: target.id,
          },
          undefined,
          target.id
        )
      );
    }
  }
}