import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { TimeSource } from 'snowatch/time-source';
import { TimeSourceDilationRemovalAfterDelay, TimeSourceDilationRemovalAfterDelayState } from 'snowatch/behaviors/timesource-dilation-removal-after-delay';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface TimeSourceDilationWhenAttackedState extends BehaviorState {
  timeSourceDivider?: number;
}

export class TimeSourceDilationWhenAttacked extends Behavior {
  public static readonly ID = 'TimeSourceDilationWhenAttacked';

  filter(target: Entity, state: TimeSourceDilationWhenAttackedState | null, message: Message): result.ResultObject {
    switch( message.topicId ) {
      case Topic.id(Topics.ATTACK_WINDING_UP): {
        const messagePayload = payload<Topics.ATTACK_STAGE_CHANGING>(message);
        return (target.id === messagePayload.targetId)
          ? result.make(result.accepted) : result.make(result.rejected);
      }
      default:
        return result.make(result.rejected);
    }
  }

  process(target: Entity, state: TimeSourceDilationWhenAttackedState | null, message: Message) {
    const messagePayload = payload<Topics.ATTACK_WINDING_UP>(message);
    this.logger.debug('Slowing down time source because actor is being attacked');

    const targetAttackStage = attrs.Attribute.valueIfApplies<attrs.AttackStages>(target, attrs.AttackStage, attrs.AttackStages.Ready)
    if( !TimeSource.isTimeDilated() && Behavior.has(target, TimeSourceDilationRemovalAfterDelay.ID)
      && (targetAttackStage === attrs.AttackStages.Ready || targetAttackStage === attrs.AttackStages.Windup )
    ) {
      // time gets dilated only when receiving end is in ready state, otherwise also counter attacks would be slowed down, which we do not want for now
      const timedRemovalState = Behavior.getState(target, TimeSourceDilationRemovalAfterDelay.ID) as TimeSourceDilationRemovalAfterDelayState;
      timedRemovalState.removeTimeDilationAtTicks = TimeSource.getWallClockTicksFromNow(target.game, 0.3 * messagePayload.stageChangeTimeMillis);
      Behavior.enable(target, TimeSourceDilationRemovalAfterDelay.ID);
      TimeSource.enterTimeDilation(target.game, state?.timeSourceDivider ?? 0.1);
    }
  }
}