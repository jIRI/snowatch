import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { TimeSource } from 'snowatch/time-source';
import { Channel } from 'snowatch/bus';
import * as result from 'snowatch/result';

export interface TimeSourceDilationRemovalOnInputState extends BehaviorState {
}

export class TimeSourceDilationRemovalOnInput extends Behavior {
  public static readonly ID = 'TimeSourceDilationRemovalOnInput';

  get busChannel(): Channel {
    return this.game.bus.input;
  }

  filter(target: Entity, state: TimeSourceDilationRemovalOnInputState | null, message: Message): result.ResultObject {
    return TimeSource.isTimeDilated() === true ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: TimeSourceDilationRemovalOnInputState, message: Message) {
    this.logger.debug('Removing time source divider because of user interaction');
    TimeSource.leaveTimeDilation(target.game);
  }
}