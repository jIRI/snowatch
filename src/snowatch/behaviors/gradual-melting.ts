import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { getCurrentScene } from 'snowatch/scene';
import { Topics } from 'snowatch/message-topics/all';
import { Substance, SubstancePhases, getSubstancePhase } from 'snowatch/substance';
import { SpatialRelations } from 'snowatch/spatial-relations';
import { TimeSource } from 'snowatch/time-source';
import { Channel } from 'snowatch/bus';
import { GradualEvaporation, GradualEvaporationState } from 'snowatch/behaviors/gradual-evaporation';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface GradualMeltingState extends BehaviorState {
  meltsToLiquid?: boolean;
  evaporatesToGas?: boolean;
  dissipationRateTicks?: number;
  evaporationRateTicks?: number;
  meltingRateTicks?: number;
  lastUpdateTicks?: number;
}

export class GradualMelting extends Behavior {
  public static readonly ID = 'GradualMelting';

  get busChannel(): Channel | null {
    return this.game.bus.system;
  }

  filter(target: Entity, state: GradualMeltingState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.CLOCK_TICK) || target == null || getSubstancePhase(target) !== SubstancePhases.solid) {
      return result.make(result.rejected);
    }

    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.CLOCK_TICK>(message);
    state.lastUpdateTicks = state.lastUpdateTicks ?? messagePayload.wallClockTicks;
    state.meltingRateTicks = state.meltingRateTicks ?? 100;
    return (state.meltingRateTicks == null
        || messagePayload.wallClockTicks >= (state.lastUpdateTicks + state.meltingRateTicks)
      ) ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: GradualMeltingState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    const constRateOfMelting = attrs.Attribute.value<number>(target, attrs.ConstantRateOfMelting);
    const volume = attrs.Attribute.value<number>(target, attrs.Volume);

    // we wastly simplify things here by ignoring normal model for evaporation and reduce the volume at constant rate
    if( state.meltsToLiquid === true ) {
      // we turn liquid in to gas
      const liquid = Substance.split(target, constRateOfMelting);
      if( liquid != null ) {
        attrs.Attribute.setValue<SubstancePhases>(liquid, attrs.SubstancePhase, SubstancePhases.liquid);
        Behavior.add(liquid, GradualEvaporation.ID, {
          initialState: <GradualEvaporationState>{
            evaporatesToGas: state.evaporatesToGas,
            evaporationRateTicks: state.evaporationRateTicks,
            dissipationRateTicks: state.dissipationRateTicks,
          }
        });
        const currentScene = getCurrentScene(liquid);
        if( currentScene != null ) {
          // join new gas with existing gas
          const sameLiquid = currentScene.substances
            .filter(substance =>
              substance.id !== liquid.id
              && substance.baseId === liquid.baseId
              && attrs.Attribute.value<SubstancePhases>(substance, attrs.SubstancePhase) === SubstancePhases.liquid
            );
          if( sameLiquid.length >= 1 ) {
            Substance.join(sameLiquid[0], liquid);
          }
        }
      }
    } else {
      Substance.decreaseVolume(target, constRateOfMelting);
    }
    const newVolume = attrs.Attribute.value<number>(target, attrs.Volume);

    const targetSpatialRelations = SpatialRelations.relationsForDirect(target);
    const targetSpatialRelation = targetSpatialRelations != null ? targetSpatialRelations[0] : null;
    target.game.bus.notifications.publish(broadcast(Topics.MELTED, {
        substanceId: target.id,
        substanceNounId: attrs.Attribute.value<string>(target, attrs.NounResourceId),
        spatialRelationTypeId: targetSpatialRelation != null ? targetSpatialRelation.typeId : null,
        indirectId: targetSpatialRelation != null ? targetSpatialRelation.indirectId : null,
        prevVolume: volume,
        newVolume: newVolume
      },
      undefined,
      target.id)
    );

    if( newVolume <= 0 ) {
      // all of the substance is gone, remove the behavior
      Behavior.remove(target, GradualMelting.ID);
    }

    state.lastUpdateTicks = TimeSource.getWallClockTicks(target.game);
  }
}