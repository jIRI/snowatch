import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { getCurrentScene } from 'snowatch/scene';
import { Topics } from 'snowatch/message-topics/all';
import { Substance, SubstancePhases, getSubstancePhase } from 'snowatch/substance';
import { SpatialRelations } from 'snowatch/spatial-relations';
import { TimeSource } from 'snowatch/time-source';
import { GradualDissipation, GradualDissipationState } from './gradual-dissipation';
import { Channel } from 'snowatch/bus';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface GradualEvaporationState extends BehaviorState {
  evaporatesToGas?: boolean;
  dissipationRateTicks?: number;
  evaporationRateTicks?: number;
  lastUpdateTicks?: number;
}

export class GradualEvaporation extends Behavior {
  public static readonly ID = 'GradualEvaporation';

  get busChannel(): Channel | null {
    return this.game.bus.system;
  }

  filter(target: Entity, state: GradualEvaporationState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.CLOCK_TICK)  || target == null || getSubstancePhase(target) !== SubstancePhases.liquid) {
      return result.make(result.rejected);
    }

    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.CLOCK_TICK>(message);
    state.lastUpdateTicks = state.lastUpdateTicks ?? messagePayload.wallClockTicks;
    state.evaporationRateTicks = state.evaporationRateTicks ?? 100;
    return (state.evaporationRateTicks == null
        || messagePayload.wallClockTicks >= (state.lastUpdateTicks + state.evaporationRateTicks)
      ) ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: GradualEvaporationState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    const constRateOfEvaporation = attrs.Attribute.value<number>(target, attrs.ConstantRateOfEvaporation);
    const volume = attrs.Attribute.value<number>(target, attrs.Volume);

    // we wastly simplify things here by ignoring normal model for evaporation and reduce the volume at constant rate
    if( state.evaporatesToGas === true ) {
      // we turn liquid in to gas
      const gas = Substance.split(target, constRateOfEvaporation);
      if( gas != null ) {
        attrs.Attribute.setValue<SubstancePhases>(gas, attrs.SubstancePhase, SubstancePhases.gas);
        Behavior.add(gas, GradualDissipation.ID, {
          initialState: <GradualDissipationState>{
            dissipationRateTicks: state.dissipationRateTicks,
          }
        });
        const currentScene = getCurrentScene(gas);
        if( currentScene != null ) {
          // join new gas with existing gas
          const sameGas = currentScene.substances
            .filter(substance =>
              substance.id !== gas.id
              && substance.baseId === gas.baseId
              && attrs.Attribute.value<SubstancePhases>(substance, attrs.SubstancePhase) === SubstancePhases.gas
            );
          if( sameGas.length >= 1 ) {
            Substance.join(sameGas[0], gas);
          }
        }
      }
    } else {
      Substance.decreaseVolume(target, constRateOfEvaporation);
    }
    const newVolume = attrs.Attribute.value<number>(target, attrs.Volume);

    const targetSpatialRelations = SpatialRelations.relationsForDirect(target);
    const targetSpatialRelation = targetSpatialRelations != null ? targetSpatialRelations[0] : null;
    target.game.bus.notifications.publish(broadcast(Topics.EVAPORATED, {
        substanceId: target.id,
        substanceNounId: attrs.Attribute.value<string>(target, attrs.NounResourceId),
        spatialRelationTypeId: targetSpatialRelation != null ? targetSpatialRelation.typeId : null,
        indirectId: targetSpatialRelation != null ? targetSpatialRelation.indirectId : null,
        prevVolume: volume,
        newVolume: newVolume
      },
      undefined,
      target.id)
    );

    if( newVolume <= 0 ) {
      // all of the substance is gone, remove the behavior
      Behavior.remove(target, GradualEvaporation.ID);
    }

    state.lastUpdateTicks = TimeSource.getWallClockTicks(target.game);
  }
}