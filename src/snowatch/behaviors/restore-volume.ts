import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { Substance } from 'snowatch/substance';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface RestoreVolumeState extends BehaviorState {
  volume: number;
}

export class RestoreVolume extends Behavior {
  public static readonly ID = 'RestoreVolume';

  setup(target: Entity, state: RestoreVolumeState) {
  }

  filter(target: Entity, state: RestoreVolumeState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.SUBSTANCE_VOLUME_DECREASED) ) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.SUBSTANCE_VOLUME_DECREASED>(message);
    return (state != null && messagePayload.substanceId === target.id)
      ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: RestoreVolumeState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    const messagePayload = payload<Topics.SUBSTANCE_VOLUME_DECREASED>(message);
    const oldVolume = messagePayload.newVolume;
    Substance.setVolume(target, state.volume ?? 0);
    const newVolume = attrs.Attribute.value<number>(target, attrs.Volume);

    target.game.bus.notifications.publish(
      broadcast<Topics.SUBSTANCE_VOLUME_RESTORED>(
        Topics.SUBSTANCE_VOLUME_RESTORED,
        {
          substanceId: target.id,
          oldVolume: oldVolume,
          newVolume: newVolume
        },
        undefined,
        target.id
      )
    );
  }
}