import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface SupportsOpeningState extends BehaviorState {
  value: boolean;
}

export class SupportsOpening extends Behavior {
  public static readonly ID = 'SupportsOpening';

  setup(target: Entity, state: SupportsOpeningState) {
    let initialValue = true;

    // closed things are openable by default
    if( attrs.Attribute.applies(target, attrs.IsClosed) ) {
      initialValue = attrs.Attribute.value<boolean>(target, attrs.IsClosed);
    }
    // locked things can't be open
    let locked = false;
    if( attrs.Attribute.applies(target, attrs.IsLocked) ) {
      locked = attrs.Attribute.value<boolean>(target, attrs.IsLocked);
      initialValue = initialValue && !locked;
    }

    // we can prevent autoinit by setting the attributes explicitly
    if( !attrs.Attribute.has(target, attrs.IsOpen) ) {
      attrs.Attribute.add(target, attrs.IsOpen, { initialState : { value: !initialValue && !locked } });
    }
    if( !attrs.Attribute.has(target, attrs.IsOpenable) ) {
      attrs.Attribute.add(target, attrs.IsOpenable, { initialState : { value: initialValue } });
    }
  }

  filter(target: Entity, state: SupportsOpeningState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.ATTRIBUTE_VALUE_CHANGED) ) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.ATTRIBUTE_VALUE_CHANGED>(message);
    return (messagePayload.attributeId === attrs.IsOpen
      || messagePayload.attributeId === attrs.IsClosed
      || messagePayload.attributeId === attrs.IsLocked
      || messagePayload.attributeId === attrs.IsUnlocked)
      ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: SupportsOpeningState | null, message: Message) {
    const messagePayload = payload<Topics.ATTRIBUTE_VALUE_CHANGED>(message);
    switch( messagePayload.attributeId ) {
      case attrs.IsUnlocked:
        if( attrs.Attribute.applies(target, attrs.IsOpenable) ) {
          attrs.Attribute.setValue<boolean>(target, attrs.IsOpenable, messagePayload.newValue);
        }
        break;
      case attrs.IsLocked:
        if( attrs.Attribute.applies(target, attrs.IsOpenable) ) {
          attrs.Attribute.setValue<boolean>(target, attrs.IsOpenable, !messagePayload.newValue);
        }
        break;
      case attrs.IsOpen:
        if( attrs.Attribute.applies(target, attrs.IsOpenable) ) {
          attrs.Attribute.setValue<boolean>(target, attrs.IsOpenable, !messagePayload.newValue);
        }
        break;
      case attrs.IsClosed:
        if( attrs.Attribute.applies(target, attrs.IsOpen) ) {
          attrs.Attribute.setValue<boolean>(target, attrs.IsOpen, !messagePayload.newValue);
        }
        break;
    }
  }
}