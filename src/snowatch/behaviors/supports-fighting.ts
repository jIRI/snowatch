import { Behavior, BehaviorState, BehaviorStatus } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { AttackType, FightCalculator } from 'snowatch/fight-calculator';
import { DefaultFightCalculator } from 'snowatch/calculators/default-fight-calculator';
import { Block } from 'snowatch/skills/block';
import { Attack, AttackModifiers } from 'snowatch/skills/attack';
import { Dodge } from 'snowatch/skills/dodge';
import { TimedStunRecovery, TimedStunRecoveryState } from 'snowatch/behaviors/timed-stun-recovery';
import { TimeSource } from 'snowatch/time-source';
import { TimedMessagePublication, TimedMessagePublicationState } from 'snowatch/behaviors/timed-message-publication';
import { nextElementInCircularBuffer } from 'snowatch/utils';
import { AvailableChannels } from 'snowatch/bus';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface FightSequenceStep {
  delayMillis: number;
  attackModifierId: string;
}

export interface SupportsFightingState extends BehaviorState {
  attackInstrumentId?: string | null;
  attackedEntityId?: string | null;
  attackSequence: Array<FightSequenceStep>;
  blockToStunTimeWindow?: { clockTicksStart: number,  clockTicksEnd: number, };
  currentAttackSequenceId?: number;
}

export class SupportsFighting extends Behavior {
  public static readonly ID = 'SupportsFighting';

  public static readonly DefaultFightSequence = [ { delayMillis: 1000, attackModifierId: AttackModifiers.strongly.id } ];

  private _calc: FightCalculator;

  constructor() {
    super();
    this._calc = new DefaultFightCalculator();
  }

  setup(target: Entity, state: SupportsFightingState) {
    if( !attrs.Attribute.has(target, attrs.ActorMode) ) {
      attrs.Attribute.add(target, attrs.ActorMode, { initialState : { value: attrs.ActorModes.Normal } });
    }
    if( !attrs.Attribute.has(target, attrs.AttackStage) ) {
      attrs.Attribute.add(target, attrs.AttackStage, { initialState : { value: attrs.AttackStages.Ready } });
    }

    state.currentAttackSequenceId = 0;
    state.attackSequence = state.attackSequence ?? SupportsFighting.DefaultFightSequence;
    Behavior.add(target,
      TimedMessagePublication.ID,
      { initialState: {configs: []} as TimedMessagePublicationState }
    );
  }

  filter(target: Entity, state: SupportsFightingState | null, message: Message): result.ResultObject {
    switch( message.topicId ) {
      case Topic.id(Topics.ATTACK_WINDUP):
      case Topic.id(Topics.ATTACK_CANCELLED):
      case Topic.id(Topics.ATTACK_COOLDOWN):
      case Topic.id(Topics.ATTACK_FINISHED): {
        const messagePayload = payload<Topics.ATTACK_STAGE_CHANGING>(message);
        return (messagePayload.targetId === target.id
          || messagePayload.actorId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
      }
      case Topic.id(Topics.ATTACKED): {
        const messagePayload = payload<Topics.ATTACKED>(message);
        return (messagePayload.targetId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
      }
      case Topic.id(Topics.ENGAGED): {
        const messagePayload = payload<Topics.EVADED>(message);
        return (messagePayload.actorId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
      }
      case Topic.id(Topics.EVADED): {
        const messagePayload = payload<Topics.EVADED>(message);
        return (messagePayload.targetId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
      }
      case Topic.id(Topics.BLOCKED): {
        const messagePayload = payload<Topics.BLOCKED>(message);
        return (messagePayload.targetId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
      }
      case Topic.id(Topics.STUNNED): {
        const messagePayload = payload<Topics.STUNNED>(message);
        return (messagePayload.targetId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
      }
      case Topic.id(Topics.STUN_RECOVERY): {
        const messagePayload = payload<Topics.STUN_RECOVERY>(message);
        return (messagePayload.actorId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
      }
      case Topic.id(Topics.DIED): {
        const messagePayload = payload<Topics.DIED>(message);
        return (messagePayload.entityId === target.id
          || messagePayload.entityId === state?.attackedEntityId ) ? result.make(result.accepted) : result.make(result.rejected);
        }
    case Topic.id(Topics.FIGHT_SEQUENCE_NEXT): {
      const messagePayload = payload<Topics.FIGHT_SEQUENCE_NEXT>(message);
      return (messagePayload.entityId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
      }
    default:
      return result.make(result.rejected);
    }
  }

  process(target: Entity, state: SupportsFightingState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    const ownAttackState = attrs.Attribute.value<attrs.AttackStages>(target, attrs.AttackStage, target);

    // handle some general events affecting fight
    switch( message.topicId ) {
      case Topic.id(Topics.DIED): {
        // handle resetting states when any party of the fight dies
        const messagePayload = payload<Topics.DIED>(message);
        if( state.attackedEntityId === messagePayload.entityId ) {
          this.logger.debug(`Actor ${messagePayload.entityId} died. Resetting fighting state of ${target.id}`);
          attrs.Attribute.ifAttributesThenSet([{ entity: target, withAttributeId: attrs.ActorMode }, ], [{ entity: target, withAttributeId: attrs.ActorMode, setValueTo: attrs.ActorModes.Normal }]);
          attrs.Attribute.ifAttributesThenSet([{ entity: target, withAttributeId: attrs.AttackStage }, ], [{ entity: target, withAttributeId: attrs.AttackStage, setValueTo: attrs.AttackStages.Ready }]);
        }

        this.logger.debug(`Actor ${messagePayload.entityId} died. Resetting behavior state of ${this.id}`);

        state.attackInstrumentId = null;
        state.attackedEntityId = null;
        return;
      }

      case Topic.id(Topics.EVADED): {
        const messagePayload = payload<Topics.EVADED>(message);
        if( state.attackedEntityId === messagePayload.actorId ) {
          this.logger.debug(`Actor ${messagePayload.actorId} evaded. Resetting fighting state of ${target.id}`);
          attrs.Attribute.ifAttributesThenSet(
            [
              { entity: target, withAttributeId: attrs.ActorMode, valueMustBe: attrs.ActorModes.Fighting },
              { entity: target, withAttributeId: attrs.ActorMode, valueMustBe: attrs.ActorModes.Engaged },
            ],
            [{ entity: target, withAttributeId: attrs.ActorMode, setValueTo: attrs.ActorModes.Normal }]
          );
          attrs.Attribute.ifAttributesThenSet(
            [
              { entity: target, withAttributeId: attrs.AttackStage },
              { entity: target, withAttributeId: attrs.ActorMode, valueMustBe: attrs.ActorModes.Normal },
            ],
            [{ entity: target, withAttributeId: attrs.AttackStage, setValueTo: attrs.AttackStages.Ready }]
          );
          state.attackInstrumentId = null;
          state.attackedEntityId = null;
        }
        return;
      }

      case Topic.id(Topics.ENGAGED): {
        const messagePayload = payload<Topics.ENGAGED>(message);
        // after little delay initiate attack automatically
        this.scheduleAttack(target, state, messagePayload.targetId);
        return;
      }

      case Topic.id(Topics.BLOCKED): {
        const messagePayload = payload<Topics.BLOCKED>(message);
        if( state.attackedEntityId === messagePayload.actorId ) {
          const now = TimeSource.getWallClockTicks(target.game);
          if( state.blockToStunTimeWindow != null && state.blockToStunTimeWindow.clockTicksStart <= now && state.blockToStunTimeWindow.clockTicksEnd >= now ) {
          if( attrs.Attribute.valueIfApplies(target, attrs.CanBeStunned, false) ) {
              this.game.bus.notifications.publish(broadcast(
                Topics.STUNNED, {
                  actorId: messagePayload.actorId,
                  targetId: target.id,
                  instrumentId: messagePayload.instrumentId,
                  stunDurationMillis: this._calc.stunTime(target, 10),
                },
                undefined,
                target.id
              ));
            }
          }
        }
        return;
      }

      case Topic.id(Topics.DODGED): {
        const messagePayload = payload<Topics.DODGED>(message);
        if( state.attackedEntityId === messagePayload.actorId ) {
          // TODO: implement reaction to dodge
        }
        return;
      }

      case Topic.id(Topics.STUNNED): {
        const messagePayload = payload<Topics.STUNNED>(message);
        this.logger.debug(`Actor ${messagePayload.targetId} is stunned by ${messagePayload.actorId} for ${messagePayload.stunDurationMillis}.`);
        attrs.Attribute.ifAttributesThenSet([{ entity: target, withAttributeId: attrs.ActorMode }, ], [{ entity: target, withAttributeId: attrs.ActorMode, setValueTo: attrs.ActorModes.Stunned }]);
        attrs.Attribute.ifAttributesThenSet([{ entity: target, withAttributeId: attrs.AttackStage }, ], [{ entity: target, withAttributeId: attrs.AttackStage, setValueTo: attrs.AttackStages.Stunned }]);
        attrs.Attribute.ifAttributesThenSet([{ entity: target, withAttributeId: attrs.CanBeStunned }, ], [{ entity: target, withAttributeId: attrs.CanBeStunned, setValueTo: false }]);
        attrs.Attribute.ifAttributesThenSet([{ entity: target, withAttributeId: attrs.IsStunned }, ], [{ entity: target, withAttributeId: attrs.IsStunned, setValueTo: true }]);
        Behavior.add(target, TimedStunRecovery.ID, {
          initialState: <TimedStunRecoveryState>{
            recoverAtTicks: TimeSource.getWallClockTicksFromNow(target.game, messagePayload.stunDurationMillis),
            actorAttackStageAfterRecovery: attrs.AttackStages.Ready,
            actorModeAfterRecovery: attrs.ActorModes.Fighting,
          }
        });
        return;
      }

      default:
        // any other message needs check for stun before we continue with processing
        if( attrs.Attribute.value<boolean>(target, attrs.IsStunned) ) {
          this.logger.debug(`Actor ${target.id} is stunned, ignoring message ${message.topicId}.`);
          return;
        }
        break;
    }

    // this gets invoked only after the other party already started the attack, so we should have always attackedEntity and instrument set
    // keep this here, we need to be checked for stun before doing any of the stuff below...
    if( message.topicId === Topic.id(Topics.FIGHT_SEQUENCE_NEXT) || message.topicId === Topic.id(Topics.STUN_RECOVERY) ) {
      if( state.attackedEntityId != null && state.attackInstrumentId != null && ownAttackState === attrs.AttackStages.Ready ) {
        state.currentAttackSequenceId =  state.currentAttackSequenceId ?? 0;
        target.game.bus.commands.publish(broadcast(
          Topics.TRY_USE_SKILL,
          {
            skillId : Attack.ID,
            actorId : target.id,
            directId : state.attackedEntityId,
            modifierId: state.attackSequence[state.currentAttackSequenceId].attackModifierId,
            instrumentId: state.attackInstrumentId,
            indirectsIds: [],
          },
          message.id,
          target.id
          ));

        const [_, nextIndex] = nextElementInCircularBuffer(state.attackSequence, state.currentAttackSequenceId);
        state.currentAttackSequenceId = nextIndex;
        return;
      } else {
        const targetActorMode = attrs.Attribute.value<attrs.ActorModes>(target, attrs.ActorMode);
        switch( targetActorMode ) {
          case attrs.ActorModes.Fighting:
          case attrs.ActorModes.Stunned:
          case attrs.ActorModes.Blocking:
            // there is something preventing the fight step from execution, reschedule
            this.scheduleNextFightSequenceStep(target, state);
            break;
          default:
            break;
        }
        return;
      }
    }

    const messagePayload = payload<Topics.ATTACK_STAGE_CHANGING>(message);
    const attackedEntity = target.game.instances.getEntityFromId(messagePayload.targetId);
    if( attackedEntity == null ) {
      this.logger.debug(`Unable to find attacked entity ${messagePayload.targetId}`);
      return;
    }

    // pick instrument and use it for the rest of the fight
    let instrument;
    if( message.topicId === Topic.id(Topics.ATTACK_WINDUP)) {
      // pick and store instrument
      instrument = this._calc.pickInstrument(target);
      state.attackInstrumentId = instrument.id;

      // store attacked entity
      state.attackedEntityId = attackedEntity.id;

      // store block to stun time window
      state.blockToStunTimeWindow = {
        clockTicksStart: TimeSource.getWallClockTicksFromNow(target.game, 0.5 * messagePayload.stageChangeTimeMillis),
        clockTicksEnd: TimeSource.getWallClockTicksFromNow(target.game, messagePayload.stageChangeTimeMillis),
      };
    } else {
      if( state.attackInstrumentId == null ) {
        this.logger.debug(`No instrument set in state of ${this.id} for ${target.id}`);
        return;
      }
      if( state.attackedEntityId == null ) {
        this.logger.debug(`No attacked entity set in state of ${this.id} for ${target.id}`);
        return;
      }
      instrument = target.game.instances.getAnyFromId(state.attackInstrumentId);
    }

    let attackType: AttackType;
    let windupTime: number;
    const windupTimeFast = this._calc.attackWindUpTime(target, instrument, AttackType.fast);
    const windupTimeStrong = this._calc.attackWindUpTime(target, instrument, AttackType.strong);
    let coolDownTime: number;
    const coolDownTimeFast = this._calc.attackCoolDownTime(target, instrument, AttackType.fast);
    const coolDownTimeStrong = this._calc.attackCoolDownTime(target, instrument, AttackType.strong);
    let ownStaminaConsumptionAttack: number;
    const ownStaminaConsumptionAttackFast = this._calc.attackStaminaConsumption(target, instrument, AttackType.fast);
    const ownStaminaConsumptionAttackStrong = this._calc.attackStaminaConsumption(target, instrument, AttackType.strong);

    const ownStamina = attrs.Attribute.value<attrs.NumericValueAttribute>(target, attrs.Stamina, target).value;
    if( messagePayload.stageChangeTimeMillis >= windupTimeStrong && ownStamina < ownStaminaConsumptionAttackStrong ) {
      // if we have enough stamina and time for strong attack, use strong attack...
      attackType = AttackType.strong;
      ownStaminaConsumptionAttack = ownStaminaConsumptionAttackStrong;
      windupTime = windupTimeStrong;
      coolDownTime = coolDownTimeStrong;
    } else {
      // ... otherwise try go with the fast attacks
      attackType = AttackType.fast;
      ownStaminaConsumptionAttack = ownStaminaConsumptionAttackFast;
      windupTime = windupTimeFast;
      coolDownTime = coolDownTimeFast;
    }

    const ownStaminaConsumptionBlock = this._calc.blockStaminaConsumption(target, instrument);
    const ownStaminaConsumptionDodge = this._calc.dodgeStaminaConsumption(target);

    if( messagePayload.targetId === target.id ) {
      switch( message.topicId ) {
        case Topic.id(Topics.ATTACK_WINDUP): {
          switch( ownAttackState ) {
            case attrs.AttackStages.Ready: {
              // get the attacker entity windup time and based on our dodge and block time decide whether either dodge or block
              if( messagePayload.stageChangeTimeMillis >= windupTime && ownStamina >= ownStaminaConsumptionAttack ) {
                // we can do counter attack faster than the attack will land, let's do counter attack
                target.game.bus.commands.publish(broadcast(
                  Topics.TRY_USE_SKILL,
                  {
                    skillId : Attack.ID,
                    actorId : target.id,
                    directId : messagePayload.actorId,
                    modifierId: this.getAttackModifierId(attackType),
                    instrumentId: instrument.id,
                    indirectsIds: [],
                  },
                  message.id,
                  target.id
                ));
              } else if( ownStamina >= ownStaminaConsumptionBlock ) {
                // we still have enough stamina for block, lets block
                target.game.bus.commands.publish(broadcast(
                  Topics.TRY_USE_SKILL,
                  {
                    skillId : Block.ID,
                    actorId : target.id,
                    directId : messagePayload.actorId,
                    modifierId: null,
                    instrumentId: instrument.id,
                    indirectsIds: [],
                  },
                  message.id,
                  target.id
                ));
              } else if( ownStamina >= ownStaminaConsumptionDodge ) {
                // we still have enough stamina for dodge, lets dodge
                target.game.bus.commands.publish(broadcast(
                  Topics.TRY_USE_SKILL,
                  {
                    skillId : Dodge.ID,
                    actorId : target.id,
                    directId : messagePayload.actorId,
                    modifierId: null,
                    instrumentId: instrument.id,
                    indirectsIds: [],
                  },
                  message.id,
                  target.id
                ));
              } else {
                this.scheduleNextFightSequenceStep(target, state);
              }
            } break;
          }
        } break;
        case Topic.id(Topics.ATTACK_COOLDOWN): {
          switch( ownAttackState ) {
            case attrs.AttackStages.Ready: {
              // start attack if we are in ready state
              if( messagePayload.stageChangeTimeMillis >= windupTime && ownStamina >= ownStaminaConsumptionAttack ) {
                // we can do counter attack faster than the attack will land, let's do counter attack
                target.game.bus.commands.publish(broadcast(
                  Topics.TRY_USE_SKILL,
                  {
                    skillId : Attack.ID,
                    actorId : target.id,
                    directId : messagePayload.actorId,
                    modifierId: this.getAttackModifierId(attackType),
                    instrumentId: instrument.id,
                    indirectsIds: [],
                  },
                  message.id,
                  target.id
                ));
              } else {
                this.scheduleNextFightSequenceStep(target, state);
              }
            } break;
          }
        } break;
        case Topic.id(Topics.ATTACK_CANCELLED): {
          // attack was cancelled, start our attack
          switch( ownAttackState ) {
            case attrs.AttackStages.Ready: {
              // attack has landed, start attack if we have enough stamina
              if( ownStamina >= ownStaminaConsumptionAttack ) {
                target.game.bus.commands.publish(broadcast(
                  Topics.TRY_USE_SKILL,
                  {
                    skillId : Attack.ID,
                    actorId : target.id,
                    directId : messagePayload.actorId,
                    modifierId: this.getAttackModifierId(attackType),
                    instrumentId: instrument.id,
                    indirectsIds: [],
                  },
                  message.id,
                  target.id
                ));
              } else {
                this.scheduleNextFightSequenceStep(target, state);
              }
            } break;
          }
        } break;
        default:
          this.logger.debug(`Unknown attack stage change for actor ${target.id}: ${message.topicId}`);
          return;
      }
    } else if( messagePayload.actorId === target.id && !attrs.Attribute.value<boolean>(target, attrs.IsCurrentActor) ) {
      switch( message.topicId ) {
        case Topic.id(Topics.ATTACK_FINISHED): {
          // schedule execution of next fighting step
          this.scheduleNextFightSequenceStep(target, state);
        } break;
        default:
          // do nothing
          return;
      }
    }
  }

  private scheduleNextFightSequenceStep(target: Entity, state: SupportsFightingState) {
    state.currentAttackSequenceId =  state.currentAttackSequenceId ?? 0;
    const timedPublicationBehaviorState = Behavior.getState(target, TimedMessagePublication.ID) as TimedMessagePublicationState;
    timedPublicationBehaviorState.configs.push({
      channel: AvailableChannels.notifications,
      messageId: Topic.id(Topics.FIGHT_SEQUENCE_NEXT),
      messagePayload: new Topics.FIGHT_SEQUENCE_NEXT({ entityId: target.id}),
      publishAtTicks: TimeSource.getWallClockTicksFromNow(target.game, state.attackSequence[state.currentAttackSequenceId].delayMillis),
    });

    const [_, nextIndex] = nextElementInCircularBuffer(state.attackSequence, state.currentAttackSequenceId);
    state.currentAttackSequenceId = nextIndex;
  }

  private scheduleAttack(target: Entity, state: SupportsFightingState, attackedEntityId: string) {
    const timedPublicationBehaviorState = Behavior.getState(target, TimedMessagePublication.ID) as TimedMessagePublicationState;
    timedPublicationBehaviorState.configs.push({
      channel: AvailableChannels.commands,
      messageId: Topic.id(Topics.TRY_USE_SKILL),
      messagePayload: new Topics.TRY_USE_SKILL({
        skillId : Attack.ID,
        actorId : target.id,
        directId : attackedEntityId,
        modifierId: this.getAttackModifierId(AttackType.fast),
        instrumentId: this._calc.pickInstrument(target).id,
        indirectsIds: [],
      }),
      publishAtTicks: TimeSource.getWallClockTicksFromNow(target.game, 1500),
    });
  }

  private getAttackModifierId(attackType: AttackType) {
    switch( attackType ) {
      case AttackType.strong:
        return AttackModifiers.strongly.id;
      case AttackType.fast:
      default:
        return AttackModifiers.quickly.id;
    }
  }
}