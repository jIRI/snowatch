import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { setSceneId, getCurrentSceneId, Scene } from 'snowatch/scene';
import { Topics } from 'snowatch/message-topics/all';
import { Substance, SubstancePhases, getSubstancePhase } from 'snowatch/substance';
import { SpatialRelations, SpatialRelation } from 'snowatch/spatial-relations';
import { GradualDissipation, GradualDissipationState } from 'snowatch/behaviors/gradual-dissipation';
import { GradualEvaporation, GradualEvaporationState } from 'snowatch/behaviors/gradual-evaporation';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface ChangeSubstancePhaseState extends BehaviorState {
  substanceIds?: Array<string>;
  substancePhase: SubstancePhases;
  triggerRelationIds: Array<string>;
  transformedSubstances: Array<{
    phase: SubstancePhases,
    sceneId?: string,
    relationId?: string,
    relationTargetId?: string,
    meltingRateTicks?: number;
    evaporationRateTicks?: number;
    dissipationRateTicks?: number;
  }>;
}

export class ChangeSubstancePhase extends Behavior {
  public static readonly ID = 'ChangeSubstancePhase';

  filter(target: Entity, state: ChangeSubstancePhaseState | null, message: Message): result.ResultObject {
    // when substance gets in spatial relation with target, it will change its phase and distribute parts to given scenes and spatial relations
    if( message.topicId !== Topic.id(Topics.SPATIAL_RELATION_ADDED) || state == null) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.SPATIAL_RELATION_ADDED>(message);
    const isSubstanceMatch = state.substanceIds != null ? state.substanceIds.includes(messagePayload.directId) : true;
    const substance = target.game.instances.getSubstanceFromId(messagePayload.directId);
    return ( target.id === messagePayload.indirectId
        && isSubstanceMatch
        && substance != null && getSubstancePhase(substance) === state.substancePhase
        && state.triggerRelationIds.includes(messagePayload.typeId)
      ) ? result.make(result.accepted) : result.make(result.rejected);
    }

  process(target: Entity, state: ChangeSubstancePhaseState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    const messagePayload = payload<Topics.SPATIAL_RELATION_ADDED>(message);
    const substance = target.game.instances.getSubstanceFromId(messagePayload.directId);
    if( substance == null ) {
      this.logger.debug(`No substance ${messagePayload.directId} found`);
      return;
    }

    const substanceVolume = attrs.Attribute.value<number>(substance, attrs.Volume);
    const portionAmount = substanceVolume / state.transformedSubstances.length;
    const currentSceneId = getCurrentSceneId(substance);
    for(let transform of state.transformedSubstances) {
      // split new substance out of the original
      const portion = Substance.split(substance, portionAmount);
      const portionSceneId = transform.sceneId ?? currentSceneId;
      if( portionSceneId == null ) {
        target.logger.warn(`Scene [${portionSceneId}] not found in behavior [${this.id}]`);
        continue;
      }

      if( portion != null ) {
        attrs.Attribute.setValue<SubstancePhases>(portion, attrs.SubstancePhase, transform.phase);
        transform.evaporationRateTicks = transform.evaporationRateTicks ?? 100;
        transform.dissipationRateTicks = transform.dissipationRateTicks ?? 100;
        switch( transform.phase ) {
          case SubstancePhases.solid:
            // TODO: we need to add melting here, along with the behavior and attributes...
            break;
          case SubstancePhases.liquid:
            Behavior.add(portion, GradualEvaporation.ID, {
              initialState: <GradualEvaporationState>{
                evaporationRateTicks: transform.evaporationRateTicks,
              }
            });
            break;
          case SubstancePhases.gas:
            Behavior.add(portion, GradualDissipation.ID, {
              initialState: <GradualDissipationState>{
                dissipationRateTicks: transform.dissipationRateTicks,
              }
            });
            break;
          default:
            target.logger.error(`Unhadled substance phase [${transform.phase}] in behavior [${this.id}]`);
            return;
        }

        const portionScene = target.game.instances.getSceneFromId(portionSceneId);
        if( portionScene != null ) {
          setSceneId(portion, portionScene.id);
          // join new gas with existing gas
          const sameSubstance = portionScene.substances
            .filter(substance =>
              substance.id !== portion.id
              && substance.baseId === portion.baseId
              && attrs.Attribute.value<SubstancePhases>(substance, attrs.SubstancePhase) === transform.phase
            );
          if( sameSubstance.length > 0 ) {
            Substance.join(sameSubstance[0], portion);
          }
        }

      }

      const newVolume = attrs.Attribute.value<number>(substance, attrs.Volume);
      // if target phase if gas, report evaporation
      const targetSpatialRelations = SpatialRelations.relationsForDirect(substance);
      const targetSpatialRelation = targetSpatialRelations != null ? targetSpatialRelations[0] : null;
      switch( transform.phase ) {
        case SubstancePhases.solid:
          target.game.bus.notifications.publish(broadcast(Topics.MELTED, {
            substanceId: substance.id,
            substanceNounId: attrs.Attribute.value<string>(substance, attrs.NounResourceId),
            spatialRelationTypeId: targetSpatialRelation != null ? targetSpatialRelation.typeId : null,
            indirectId: targetSpatialRelation != null ? targetSpatialRelation.indirectId : null,
            prevVolume: substanceVolume,
            newVolume: newVolume
          },
          undefined,
          substance.id)
        );
        break;
        case SubstancePhases.liquid:
          target.game.bus.notifications.publish(broadcast(Topics.EVAPORATED, {
              substanceId: substance.id,
              substanceNounId: attrs.Attribute.value<string>(substance, attrs.NounResourceId),
              spatialRelationTypeId: targetSpatialRelation != null ? targetSpatialRelation.typeId : null,
              indirectId: targetSpatialRelation != null ? targetSpatialRelation.indirectId : null,
              prevVolume: substanceVolume,
              newVolume: newVolume
            },
            undefined,
            substance.id)
          );
          break;
        case SubstancePhases.gas:
          target.game.bus.notifications.publish(broadcast(Topics.DISSIPATED, {
              substanceId: substance.id,
              substanceNounId: attrs.Attribute.value<string>(substance, attrs.NounResourceId),
              prevVolume: substanceVolume,
              newVolume: newVolume
            },
            undefined,
            substance.id)
          );
          break;
        default:
          target.logger.error(`Unhadled substance phase [${transform.phase}] in behavior [${this.id}]`);
          return;
      }
    }
  }
}