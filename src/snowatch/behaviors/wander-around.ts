import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Actor } from 'snowatch/actor';
import { Message, Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { Vector3, setSpatialLocation } from 'snowatch/spatial-relations';
import { TimeSource } from 'snowatch/time-source';
import { Channel } from 'snowatch/bus';
import { ActorTraversabilitySceneGraph, getCurrentScene, Scene, Exit } from 'snowatch/scene';
import { nextElementInCircularBuffer } from 'snowatch/utils';
import * as pathfinding from 'snowatch/pathfinding';
import * as result from 'snowatch/result';
import * as attrs from 'snowatch/attributes-ids';


export interface WanderAroundLocation {
  sceneId: string;
  position?: Vector3;
}

export interface WanderAroundState extends BehaviorState {
  locations: Array<WanderAroundLocation>;
  destinationLocationIndex?: number;
  pathElements?: Array<{ sceneId: string, exitId: string | null }>;
  wanderingRateTicks?: number;
  lastUpdateTicks?: number;
}

export class WanderAround extends Behavior {
  public static readonly ID = 'WanderAround';

  get busChannel(): Channel | null {
    return this.game.bus.system;
  }

  setup(target: Entity, state: WanderAroundState) {
    // set current scene id and spatial location to eithr first or selected index
    state.destinationLocationIndex = state.destinationLocationIndex || 0;
    const location = state.locations[state.destinationLocationIndex];
  }

  filter(target: Entity, state: WanderAroundState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.CLOCK_TICK)
      || target == null
      || attrs.Attribute.value<attrs.ActorModes>(target, attrs.ActorMode) !== attrs.ActorModes.Normal) {
      return result.make(result.rejected);
    }

    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.CLOCK_TICK>(message);
    state.lastUpdateTicks = state.lastUpdateTicks || messagePayload.wallClockTicks;
    state.wanderingRateTicks = state.wanderingRateTicks || 100;
    return (state.wanderingRateTicks == null
        || messagePayload.wallClockTicks >= (state.lastUpdateTicks + state.wanderingRateTicks)
      ) ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: WanderAroundState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    if( attrs.Attribute.value<attrs.ActorModes>(target, attrs.ActorMode) !== attrs.ActorModes.Normal ) {
      this.logger.debug(`Entity ${target.id} is not in normal mode`);
      return;
    }

    let destinationLocation = state.locations[state.destinationLocationIndex || 0];
    if( WanderAround.moveToNextPathNode(target, state, destinationLocation) ) {
      return;
    }

    let nextIndex = state.destinationLocationIndex || 0;
    const startScene = getCurrentScene(target);
    do {
      // pick next target from the list of targets
      [destinationLocation, nextIndex] = nextElementInCircularBuffer(state.locations, nextIndex);

      if( startScene == null ) {
        break;
      }

      // calculate whether it is possible to reach the target
      const destinationScene = target.game.instances.getSceneFromId(destinationLocation.sceneId);
      if( destinationScene != null ) {
        const path = pathfinding.find(target, new ActorTraversabilitySceneGraph(target.game), startScene, destinationScene, (a, f, s) => 0.1);
        if( path[0].node === startScene && path[path.length - 1].node === destinationScene ) {
          state.pathElements = path.map(e => {
            return {
              sceneId: (e.node as Scene).id,
              exitId: e.link != null ? (e.link as Exit).id : null
            };
          });
          break;
        }
      }
      // if not, start over
    } while(state.destinationLocationIndex !== nextIndex);

    // move to the target
    WanderAround.moveToNextPathNode(target, state, destinationLocation);

    // update state
    state.destinationLocationIndex = nextIndex;
    state.lastUpdateTicks = TimeSource.getWallClockTicks(target.game);
  }

  private static moveToNextPathNode(target: Entity, state: WanderAroundState, nextLocation: WanderAroundLocation): boolean {
    if( state.pathElements == null || state.pathElements.length === 0 ) {
      return false;
    }

    const nextPathElement = state.pathElements.shift();
    if( nextPathElement == null || nextPathElement.sceneId == null
      || (nextPathElement.sceneId === nextLocation.sceneId && nextLocation.position == null)) {
      return false;
    }

    const currentScene = getCurrentScene(target);
    if( currentScene == null ) {
      return false;
    }

    if( nextPathElement.exitId != null ) {
      const exit = currentScene.getExit(nextPathElement.exitId);
      if( exit != null ) {
        Scene.tryTakeExit(target.game, target as Actor, exit);
      }
    }

    if (nextPathElement.sceneId === nextLocation.sceneId && nextLocation.position != null) {
      setSpatialLocation(target, nextLocation.position);
    }

    return true;
  }
}
