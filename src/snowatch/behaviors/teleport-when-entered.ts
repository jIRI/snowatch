import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { setSceneId } from 'snowatch/scene';
import { SpatialRelations } from 'snowatch/spatial-relations';
import { Actor } from 'snowatch/actor';
import * as result from 'snowatch/result';

export interface TeleportWhenEnteredState extends BehaviorState {
  entityId: string;
  targetForTheFirstTimeSceneId: string;
  targetSceneId: string;
}

export class TeleportWhenEntered extends Behavior {
  public static readonly ID = 'TeleportWhenEntered';

  filter(target: Entity, state: TeleportWhenEnteredState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.SPATIAL_RELATION_ADDED) ) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.SPATIAL_RELATION_ADDED>(message);
    return (state != null && target.id === messagePayload.indirectId
      && state.entityId === messagePayload.directId)
      ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: TeleportWhenEnteredState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    const messagePayload = payload<Topics.SPATIAL_RELATION_ADDED>(message);
    const actor = target.game.instances.getEntityFromId(messagePayload.directId);
    if( actor != null ) {
      if( actor instanceof Actor ) {
        const targetScene = target.game.instances.getSceneFromId(state.targetSceneId);
        if( targetScene == null ) {
          this.logger.error(`Scene with ID '${state.targetSceneId}' not found.`);
          return;
        }
        let visitCount = targetScene.visitCounts.get(actor.id);
        if( visitCount == null || visitCount === 0 ) {
          actor.tryChangeSceneTo(state.targetForTheFirstTimeSceneId);
        } else {
          actor.tryChangeSceneTo(state.targetSceneId);
        }
      } else {
        setSceneId(actor, state.targetSceneId);
      }

      SpatialRelations.removeRelationsOf(actor.game, actor.id);
    }
  }
}