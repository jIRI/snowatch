import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface ChangeAttributeWhenSubstanceStateChangesState extends BehaviorState {
  stateIds: Array<string>;
  changes: Array<{attributeId: string, stateMatch: any, stateNotMatch: any}>;
}

export class ChangeAttributeWhenSubstanceStateChanges extends Behavior {
  public static readonly ID = 'ChangeAttributeWhenSubstanceStateChanges';

  filter(target: Entity, state: ChangeAttributeWhenSubstanceStateChangesState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.ATTRIBUTE_VALUE_CHANGED)) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.ATTRIBUTE_VALUE_CHANGED>(message);
    return (state != null
        && messagePayload.attributeId === attrs.SubstancePhase
        && messagePayload.entityId === target.id
      ) ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: ChangeAttributeWhenSubstanceStateChangesState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    const messagePayload = payload<Topics.ATTRIBUTE_VALUE_CHANGED>(message);
    if( messagePayload.newValue != null && state.stateIds.includes(messagePayload.newValue) ) {
      for(let change of state.changes) {
        attrs.Attribute.setValue<any>(target, change.attributeId, change.stateMatch);
      }
    } else if ( messagePayload.oldValue != null && state.stateIds.includes(messagePayload.oldValue) ) {
      for(let change of state.changes) {
        attrs.Attribute.setValue<any>(target, change.attributeId, change.stateNotMatch);
      }
    }
  }
}