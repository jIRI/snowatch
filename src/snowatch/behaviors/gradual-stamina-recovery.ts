import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { Channel } from 'snowatch/bus';
import { TimeSource } from 'snowatch/time-source';
import { clamp } from 'snowatch/utils';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface GradualStaminaRecoveryState extends BehaviorState {
  recoveryPeriodMillis: number;
  lastUpdateTicks?: number;
}

export class GradualStaminaRecovery extends Behavior {
  public static readonly ID = 'GradualStaminaRecovery';

  get busChannel(): Channel | null {
    return this.game.bus.system;
  }

  setup(target: Entity, state: GradualStaminaRecoveryState) {
    state.recoveryPeriodMillis = state.recoveryPeriodMillis ?? 2000;
  }

  filter(target: Entity, state: GradualStaminaRecoveryState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.CLOCK_TICK) || target == null) {
      return result.make(result.rejected);
    }

    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.CLOCK_TICK>(message);
    state.lastUpdateTicks = state.lastUpdateTicks ?? messagePayload.wallClockTicks;
    return (messagePayload.wallClockTicks >= (state.lastUpdateTicks + TimeSource.numberOfTicksFromMillis(target.game, state.recoveryPeriodMillis))) ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: GradualStaminaRecoveryState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    if( !attrs.Attribute.applies(target, attrs.Stamina) ) {
      return;
    }

    const stamina = attrs.Attribute.value<attrs.NumericValueAttribute>(target, attrs.Stamina);
    const strength = attrs.Attribute.value<attrs.NumericValueAttribute>(target, attrs.Strength);
    const constitution = attrs.Attribute.value<attrs.NumericValueAttribute>(target, attrs.Constitution);
    const newStaminaValue = stamina.value + 2 * strength.value * constitution.value;
    stamina.value = clamp(newStaminaValue, stamina.minValue, stamina.maxValue);
    attrs.Attribute.setValue<attrs.NumericValueAttribute>(target, attrs.Stamina, stamina);

    state.lastUpdateTicks = TimeSource.getWallClockTicks(target.game);
  }
}