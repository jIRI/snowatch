import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface SupportsEvadingState extends BehaviorState {
}

export class SupportsEvading extends Behavior {
  public static readonly ID = 'SupportsEvading';

  constructor() {
    super();
  }

  setup(target: Entity, state: SupportsEvadingState) {
    if( !attrs.Attribute.has(target, attrs.ActorMode) ) {
      attrs.Attribute.add(target, attrs.ActorMode, { initialState : { value: attrs.ActorModes.Normal } });
    }
    if( !attrs.Attribute.has(target, attrs.AttackStage) ) {
      attrs.Attribute.add(target, attrs.AttackStage, { initialState : { value: attrs.AttackStages.Ready } });
    }
    if( !attrs.Attribute.has(target, attrs.CanEvade) ) {
      attrs.Attribute.add(target, attrs.CanEvade);
    }
    if( !attrs.Attribute.has(target, attrs.CanBeEvaded) ) {
      attrs.Attribute.add(target, attrs.CanBeEvaded);
    }
    if( !attrs.Attribute.has(target, attrs.IsEvadeable) ) {
      attrs.Attribute.add(target, attrs.IsEvadeable);
    }
  }

  filter(target: Entity, state: SupportsEvadingState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.EVADING) ) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.EVADING>(message);
    return (messagePayload.actorId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: SupportsEvadingState | null, message: Message) {
    const messagePayload = payload<Topics.EVADING>(message);
    const attackedEntity = target.game.instances.getEntityFromId(messagePayload.targetId);
    if( attackedEntity == null ) {
      this.logger.debug(`Unable to find attackedEntity ${messagePayload.targetId}`);
      return;
    }

    attrs.Attribute.ifAttributesThenSet(
      [{ entity: target, withAttributeId: attrs.ActorMode }, ],
      [{ entity: target, withAttributeId: attrs.ActorMode, setValueTo: attrs.ActorModes.Normal }]
    );
    attrs.Attribute.ifAttributesThenSet(
      [{ entity: target, withAttributeId: attrs.AttackStage }, ],
      [{ entity: target, withAttributeId: attrs.AttackStage, setValueTo: attrs.AttackStages.Ready }]
    );

    attrs.Attribute.ifAttributesThenSet(
      [{ entity: attackedEntity, withAttributeId: attrs.ActorMode, valueMustBe: attrs.ActorModes.Fighting }, ],
      [{ entity: attackedEntity, withAttributeId: attrs.ActorMode, setValueTo: attrs.ActorModes.Normal }]
    );
    attrs.Attribute.ifAttributesThenSet(
      [{ entity: attackedEntity, withAttributeId: attrs.AttackStage }, { entity: attackedEntity, withAttributeId: attrs.ActorMode, valueMustBe: attrs.ActorModes.Normal }, ],
      [{ entity: attackedEntity, withAttributeId: attrs.AttackStage, setValueTo: attrs.AttackStages.Ready }]
    );
  }
}