import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface SupportsAttackingState extends BehaviorState {
}

export class SupportsAttacking extends Behavior {
  public static readonly ID = 'SupportsAttacking';

  constructor() {
    super();
  }

  setup(target: Entity, state: SupportsAttackingState) {
    if( !attrs.Attribute.has(target, attrs.ActorMode) ) {
      attrs.Attribute.add(target, attrs.ActorMode, { initialState : { value: attrs.ActorModes.Normal } });
    }
    if( !attrs.Attribute.has(target, attrs.AttackStage) ) {
      attrs.Attribute.add(target, attrs.AttackStage, { initialState : { value: attrs.AttackStages.Ready } });
    }
    if( !attrs.Attribute.has(target, attrs.CanAttack) ) {
      attrs.Attribute.add(target, attrs.CanAttack);
    }
    if( !attrs.Attribute.has(target, attrs.CanBeAttacked) ) {
      attrs.Attribute.add(target, attrs.CanBeAttacked);
    }
    if( !attrs.Attribute.has(target, attrs.IsAttackable) ) {
      attrs.Attribute.add(target, attrs.IsAttackable);
    }
  }

  filter(target: Entity, state: SupportsAttackingState | null, message: Message): result.ResultObject {
    switch( message.topicId ) {
      case Topic.id(Topics.ATTACK_WINDING_UP):
      case Topic.id(Topics.ATTACK_COOLING_DOWN):
      case Topic.id(Topics.ATTACK_CANCELLING):
      case Topic.id(Topics.ATTACK_LANDING):
      case Topic.id(Topics.ATTACK_FINISHING):
        break;
      default:
        return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.ATTACK_STAGE_CHANGING>(message);
    return (messagePayload.actorId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: SupportsAttackingState | null, message: Message) {
    const messagePayload = payload<Topics.ATTACK_STAGE_CHANGING>(message);
    const attackedEntity = target.game.instances.getEntityFromId(messagePayload.targetId);
    if( attackedEntity  == null ) {
      this.logger.debug(`Unable to find attacked entity ${messagePayload.targetId}`);
      return;
    }

    const attackabilityConditions = [{ entity: target, withAttributeId: attrs.CanAttack, valueMustBe: true }, { entity: attackedEntity, withAttributeId: attrs.CanBeAttacked, valueMustBe: true }];
    switch( message.topicId ) {
      case Topic.id(Topics.ATTACK_WINDING_UP):
        attrs.Attribute.ifAttributesThenSet(
          [{ entity: target, withAttributeId: attrs.ActorMode, valueMustBe: attrs.ActorModes.Engaged }],
          [{ entity: target, withAttributeId: attrs.ActorMode, setValueTo: attrs.ActorModes.Fighting }]
        );
        attrs.Attribute.ifAttributesThenSet(attackabilityConditions, [{ entity: attackedEntity, withAttributeId: attrs.IsAttackable, setValueTo: false }]);
        attrs.Attribute.setValue(target, attrs.AttackStage, attrs.AttackStages.Windup);
        break;
      case Topic.id(Topics.ATTACK_COOLING_DOWN):
        attrs.Attribute.ifAttributesThenSet(attackabilityConditions, [{ entity: attackedEntity, withAttributeId: attrs.IsAttackable, setValueTo: false }]);
        attrs.Attribute.setValue(target, attrs.AttackStage, attrs.AttackStages.Cooldown);
        break;
      case Topic.id(Topics.ATTACK_CANCELLING):
        attrs.Attribute.ifAttributesThenSet(attackabilityConditions, [{ entity: attackedEntity, withAttributeId: attrs.IsAttackable, setValueTo: false }]);
        attrs.Attribute.setValue(target, attrs.AttackStage, attrs.AttackStages.CancelCooldown);
        break;
      case Topic.id(Topics.ATTACK_LANDING):
        attrs.Attribute.ifAttributesThenSet(attackabilityConditions, [{ entity: attackedEntity, withAttributeId: attrs.IsAttackable, setValueTo: false }]);
        attrs.Attribute.setValue(target, attrs.AttackStage, attrs.AttackStages.Landed);
        break;
      case Topic.id(Topics.ATTACK_FINISHING):
        attrs.Attribute.ifAttributesThenSet(
          [{ entity: target, withAttributeId: attrs.ActorMode, valueMustBe: attrs.ActorModes.Fighting }],
          [{ entity: target, withAttributeId: attrs.ActorMode, setValueTo: attrs.ActorModes.Engaged }]
        );
        attrs.Attribute.ifAttributesThenSet(attackabilityConditions, [{ entity: attackedEntity, withAttributeId: attrs.IsAttackable, setValueTo: true }]);
        attrs.Attribute.setValue(target, attrs.AttackStage, attrs.AttackStages.Ready);
        break;
      default:
        this.logger.debug(`Unknown attack stage change for actor ${target.id}: ${message.topicId}`);
        return;
    }
  }
}