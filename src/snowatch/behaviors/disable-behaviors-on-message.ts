import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface DisableBehaviorsOnMessageState extends BehaviorState {
  topicId: string;
  disableAll?: boolean;
  behaviorIds?: Array<string>;
}

export class DisableBehaviorsOnMessage extends Behavior {
  public static readonly ID = 'DisableBehaviorsOnMessage';

  constructor() {
    super();
  }

  setup(target: Entity, state: DisableBehaviorsOnMessageState) {
  }

  filter(target: Entity, state: DisableBehaviorsOnMessageState | null, message: Message): result.ResultObject {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return result.make(result.rejected);
    }

    return (state.topicId === message.topicId && target.id === message.sourceId) ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: DisableBehaviorsOnMessageState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    if( state?.disableAll ?? false ) {
      Behavior.disableAll(target);
    } else if( state?.behaviorIds != null) {
      state?.behaviorIds.forEach(id => {
        Behavior.disable(target, id);
      });
    }
  }
}