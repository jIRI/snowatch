import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface SupportsTakingState extends BehaviorState {
  value: boolean;
}

export class SupportsTaking extends Behavior {
  public static readonly ID = 'SupportsTaking';

  constructor() {
    super();
  }

  setup(target: Entity, state: SupportsTakingState) {
    // we can prevent autoinit by setting the attributes explicitly
    if( !attrs.Attribute.has(target, attrs.IsTakeable) ) {
      attrs.Attribute.add(target, attrs.IsTakeable, { initialState : { value: true } });
    }
  }

  filter(target: Entity, state: SupportsTakingState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.ENTITY_ADDED_TO_INVENTORY)
      || message.topicId !== Topic.id(Topics.ENTITY_REMOVED_FROM_INVENTORY)
    ) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.ENTITY_ADDED_TO_INVENTORY | Topics.ENTITY_REMOVED_FROM_INVENTORY>(message);
    return (messagePayload.entityId === target.id)
      ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: SupportsTakingState | null, message: Message) {
    if( !attrs.Attribute.applies(target, attrs.IsTakeable) ) {
      return;
    }

    switch( message.topicId ) {
      case Topic.id(Topics.ENTITY_ADDED_TO_INVENTORY):
        attrs.Attribute.setValue<boolean>(target, attrs.IsTakeable, false);
        break;
      case Topic.id(Topics.ENTITY_REMOVED_FROM_INVENTORY):
        attrs.Attribute.setValue<boolean>(target, attrs.IsTakeable, true);
        break;
    }
  }
}