import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity, getProperties } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { Say, SayModifiers } from 'snowatch/skills/say';
import { DialogueLine } from 'snowatch/dialogue-line';
import * as result from 'snowatch/result';

export interface SupportsReplyingState extends BehaviorState {
}

export class SupportsReplying extends Behavior {
  public static readonly ID = 'SupportsReplying';

  static getNpcResponse(actor: Entity, targetId: string, inResponseToLineId: string, inResponseToModifierId: string) {
    const game = actor.game;
    const inResponseToLine = game.instances.dialogueLines.get(inResponseToLineId);
    const target = game.instances.actors.get(targetId);
    const inResponseToModifier = SayModifiers.all().get(inResponseToModifierId);
    const scores = new Array<any>();
    for(let lineId of DialogueLine.getIds(actor)) {
      const line =  game.instances.dialogueLines.get(lineId);
      const lineState = DialogueLine.getLineState(actor, lineId);
      const lineProps = getProperties(actor, DialogueLine.OBJECT_GROUP, lineId);
      const details = {
        responseTo: inResponseToLine,
        responseToModifier: inResponseToModifier,
      };
      const score = lineProps.lineScore != null ? lineProps.lineScore(game, line, lineState, actor, target, details) : 0;
      if( score > 0 ) {
        const modifier = lineProps.modifier != null ? lineProps.modifier(game, line, lineState, actor, target, details) : SayModifiers.neutrally;
        scores.push({ lineId, score, modifier });
      }
    }
    scores.sort((a, b) => a.score - b.score);
    return scores;
  }

  filter(target: Entity, state: SupportsReplyingState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.SPEECH) ) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.SPEECH>(message);
    return (messagePayload.targetId === target.id)
      ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: SupportsReplyingState | null, message: Message) {
    const messagePayload = payload<Topics.SPEECH>(message);
    const scores = SupportsReplying.getNpcResponse(target, messagePayload.entityId, messagePayload.dialogueLineId, messagePayload.modifierId);
    for(let item of scores ) {
      // prevent infinite loops of reacting to the same line
      if( item.lineId !== messagePayload.dialogueLineId ) {
        this.game.bus.commands.publish(broadcast(
          Topics.TRY_USE_SKILL,
          {
            skillId : Say.ID,
            actorId : target.id,
            directId : messagePayload.entityId,
            modifierId: scores[0].modifier.id,
            details: {
              responseTo: messagePayload.dialogueLineId,
              responseToModifier: messagePayload.modifierId,
            },
            indirectsIds: [scores[0].lineId, ],
          },
          undefined,
          target.id
        ));
        return;
      }
    }
  }
}