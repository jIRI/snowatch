import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, PayloadType, broadcastId } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { TimeSource } from 'snowatch/time-source';
import { AvailableChannels, Channel } from 'snowatch/bus';
import * as result from 'snowatch/result';


export type MessagePublicationConfig = {
  publishAtTicks: number;
  channel?: AvailableChannels;
  messageId: string;
  messagePayload: PayloadType;
  isHandled?: boolean;
};

export interface TimedMessagePublicationState extends BehaviorState {
  configs: Array<MessagePublicationConfig>;
}

export class TimedMessagePublication extends Behavior {
  public static readonly ID = 'TimedMessagePublication';

  get busChannel(): Channel | null {
    return this.game.bus.system;
  }

  filter(target: Entity, state: TimedMessagePublicationState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.CLOCK_TICK) || target == null ) {
      return result.make(result.rejected);
    }

    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.CLOCK_TICK>(message);
    return (state.configs.filter(c => messagePayload.wallClockTicks >= c.publishAtTicks).length > 0) ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: TimedMessagePublicationState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    const messagePayload = payload<Topics.CLOCK_TICK>(message);
    state.configs
      .forEach((c, index) => {
        if( messagePayload.wallClockTicks >= c.publishAtTicks ) {
          target.game.bus.fromType(c.channel ?? AvailableChannels.notifications).publish(broadcastId(c.messageId, c.messagePayload, undefined, target.id));
          c.isHandled = true;
        }
      });

    state.configs = state.configs.filter(c => c.isHandled !== true);
  }
}