import { autoinject } from 'aurelia-framework';
import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Topic, Message, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { DefaultResDict } from 'snowatch/resources/default';
import { SpatialPrepositions } from 'snowatch/prepositions';
import * as lang from 'snowatch/language-structures';
import * as scene from 'snowatch/scene';
import * as pathfinding from 'snowatch/pathfinding';
import * as spatial from 'snowatch/spatial-relations';
import * as ent from 'snowatch/entity';
import * as result from 'snowatch/result';
import * as attrs from 'snowatch/attributes-ids';

export interface SupportsSeeingState extends BehaviorState {
  value: boolean;
}

@autoinject()
export class SupportsSeeing extends Behavior {
  public static readonly ID = 'SupportsSeeing';

  private _spatialRelationsRenderer: spatial.SpatialRelationsRenderer;

  constructor(spatialRelationsRenderer: spatial.SpatialRelationsRenderer) {
    super();
    this._spatialRelationsRenderer = spatialRelationsRenderer;
  }

  get supportsAsync(): boolean {
    return false;
  }

  setup(target: ent.Entity, state: SupportsSeeingState) {
    // we can prevent autoinit by setting the attributes explicitly
    if (!attrs.Attribute.has(target, attrs.CanSee)) {
      attrs.Attribute.add(target, attrs.CanSee, { initialState: { value: true } });
    }
    this._spatialRelationsRenderer.initialize(this.game);
  }

  filter(target: ent.Entity, state: SupportsSeeingState | null, message: Message): result.ResultObject {
    if (target !== target.game.currentActor) {
      return result.make(result.rejected);
    }

    switch (message.topicId) {
      case Topic.id(Topics.RENDER_SCENE_DESCRIPTION): {
        const messagePayload = payload<Topics.RENDER_SCENE_DESCRIPTION>(message);
        return (messagePayload.actorId === target.id && attrs.Attribute.applies(target, attrs.CanSee, target) && attrs.Attribute.value<boolean>(target, attrs.CanSee, target))
          ? result.make(result.accepted)
          : result.make(result.rejected);
      }
      case Topic.id(Topics.ENTITY_SCENE_CHANGED): {
        const messagePayload = payload<Topics.ENTITY_SCENE_CHANGED>(message);
        return (messagePayload.entityId === target.id && attrs.Attribute.applies(target, attrs.CanSee, target) && attrs.Attribute.value<boolean>(target, attrs.CanSee, target))
          ? result.make(result.accepted)
          : result.make(result.rejected);
      }
      case Topic.id(Topics.TRYING_USE_SKILL):
      case Topic.id(Topics.SCENE_ENTERED):
      case Topic.id(Topics.SCENE_LEFT):
      case Topic.id(Topics.SPATIAL_RELATION_ADDED):
      case Topic.id(Topics.SPATIAL_RELATION_REMOVED):
      case Topic.id(Topics.APPEARANCE_CHANGE):
      case Topic.id(Topics.TOOK_DAMAGE):
      case Topic.id(Topics.DIED):
      case Topic.id(Topics.EVAPORATED): {
        return (attrs.Attribute.applies(target, attrs.CanSee, target) && attrs.Attribute.value<boolean>(target, attrs.CanSee, target))
          ? result.make(result.accepted)
          : result.make(result.rejected);
      }
      default:
        return result.make(result.rejected);
    }
  }

  process(target: ent.Entity, state: SupportsSeeingState | null, message: Message) {
    if (state == null) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    switch (message.topicId) {
      case Topic.id(Topics.TRYING_USE_SKILL): {
        this.handleTryUseSkill(target, state, message);
      } break;

      case Topic.id(Topics.RENDER_SCENE_DESCRIPTION): {
        this.renderSceneDescription(target);
      } break;

      case Topic.id(Topics.ENTITY_SCENE_CHANGED): {
        this.handleActorSceneChanged(target, state, message);
      } break;

      case Topic.id(Topics.APPEARANCE_CHANGE): {
        this.handleAppearanceChange(target, state, message);
      } break;

      case Topic.id(Topics.SCENE_ENTERED): {
        this.handleSceneEntered(target, state, message);
      } break;

      case Topic.id(Topics.SCENE_LEFT): {
        this.handleSceneLeft(target, state, message);
      } break;

      case Topic.id(Topics.SPATIAL_RELATION_ADDED): {
        SupportsSeeing.logSpatialRelationChange<Topics.SPATIAL_RELATION_ADDED>(target, message, ResourceDictionary.get(target.game, DefaultResDict.SPATIAL_RELATION_ADDED_DESC));
      } break;

      case Topic.id(Topics.SPATIAL_RELATION_REMOVED): {
        SupportsSeeing.logSpatialRelationChange<Topics.SPATIAL_RELATION_REMOVED>(target, message, ResourceDictionary.get(target.game, DefaultResDict.SPATIAL_RELATION_REMOVED_DESC));
      } break;

      case Topic.id(Topics.EVAPORATED): {
        this.logSubstanceEvaporated(target, state, message);
      } break;

      case Topic.id(Topics.TOOK_DAMAGE): {
        this.logTookDamage(target, state, message);
      } break;

      case Topic.id(Topics.DIED): {
        this.logDied(target, state, message);
      } break;
    }
  }

  private static logSpatialRelationChange<T extends Topics.SPATIAL_RELATION_REMOVED | Topics.SPATIAL_RELATION_ADDED>(target: ent.Entity, message: Message, resource: any) {
    const messagePayload = payload<Topics.SPATIAL_RELATION_REMOVED>(message);
    const sourceEntity = target.game.instances.getEntityFromId(messagePayload.directId);
    const targetEntity = target.game.instances.getEntityFromId(messagePayload.indirectId);
    const relation = SpatialPrepositions.allById().get(messagePayload.typeId);
    if (sourceEntity != null && ent.getIsVisible(sourceEntity, target)
      && targetEntity != null && (ent.getIsVisible(targetEntity, target)
        || (targetEntity.instanceId === scene.Scene.GROUND && sourceEntity.id !== target.id)
      )
      && relation != null
    ) {
      const sourceSceneId = scene.getCurrentScene(sourceEntity);
      const behaviorTargetSceneId = scene.getCurrentScene(target);
      if (sourceSceneId != null && sourceSceneId != null && sourceSceneId === behaviorTargetSceneId) {
        const prepositionId = ResourceDictionary.get(target.game, relation.prepositionDescriptionResource);
        const prepositionNounCase = lang.getPrepositionNounCase(
          target.game,
          prepositionId,
          ResourceDictionary.get(target.game, attrs.Attribute.value<string>(targetEntity, attrs.AdjectiveResourceId)),
          ResourceDictionary.get(target.game, attrs.Attribute.value<string>(targetEntity, attrs.NounResourceId))
        );
        if (prepositionNounCase.noun != null && prepositionNounCase.preposition != null && prepositionNounCase.nounCase != null) {
          const directTitle = ent.getTitle(sourceEntity);
          const directNoun = directTitle.noun;
          if (directNoun != null) {
            target.game.addToGameLog({
              source: target.id,
              action: SupportsSeeing.ID,
              category: 'game-log-spatial',
              description: lang.capitalFirst(lang.compose(
                resource.template,
                {
                  direct: directTitle.title,
                  verb: lang.Verb.get(target.game, resource.verbId).verb(directNoun.number, lang.Tense.past, lang.Person.third, directNoun.gender, directNoun.genderSubtype),
                  preposition: prepositionNounCase.preposition.preposition(),
                  indirect: prepositionNounCase.nounCase,
                }
              )),
            });
          }
        }
      }
    }
  }

  private handleTryUseSkill(target: ent.Entity, initialState: SupportsSeeingState | null, message: Message) {
    const messagePayload = payload<Topics.TRYING_USE_SKILL>(message);
    const resource = ResourceDictionary.get(target.game, DefaultResDict.TRYING_USE_SKILL);

    const actor = target.game.instances.getEntityFromId(messagePayload.actorId);
    if( actor == null ) {
      this.logger.error(`No actor found when trying to handle seeing skill ${messagePayload.skillId}`);
      return;
    }

    const actorTitle = ent.getTitle(actor, lang.Case.nominative);
    const actorNoun = actorTitle.noun;
    if( actorNoun == null ) {
      this.logger.error(`No actor noun found when trying to handle seeing skill ${messagePayload.skillId} of actor ${messagePayload.actorId}`);
      return;
    }

    const tryVerb = lang.Verb.get(target.game, resource.tryVerbId).verb(actorNoun.number, lang.Tense.past, lang.Person.third, actorNoun.gender, actorNoun.genderSubtype);

    const skill = target.game.instances.skills.get(messagePayload.skillId);
    const skillVerb = skill != null ? skill.verb.verb() : messagePayload.skillId;

    const direct = target.game.instances.getEntityFromId(messagePayload.directId);

    let adjectiveId = null;
    let directNounId = messagePayload.directId;

    if (direct != null) {
      adjectiveId = ResourceDictionary.get(target.game, attrs.Attribute.value<string>(direct, attrs.AdjectiveResourceId, target));
      directNounId = ResourceDictionary.get(target.game, attrs.Attribute.value<string>(direct, attrs.NounResourceId, target));
    }

    const modifierResource = messagePayload.modifierId != null && messagePayload.modifierId !== '' ? ResourceDictionary.get(target.game, messagePayload.modifierId) : '';
    const kind = modifierResource.kind;

    let nounCase: lang.AdverbNounCase | lang.PronounNounCase | lang.PrepositionNounCase;
    const preposition: lang.Preposition | null = messagePayload.directPrepositionId != null ? lang.Preposition.get(this.game, messagePayload.directPrepositionId) : null;
    const directPreposition: string | null = preposition?.preposition() ?? '';
    let adverb: lang.Adverb | null = null;
    let pronounCase: string | null = null;
    switch (kind) {
      case lang.Pronoun.ID:
        nounCase = lang.getPronounNounCase(target.game, modifierResource.id, adjectiveId, directNounId);
        pronounCase = nounCase.pronounCase;
        break;
      case lang.Adverb.ID:
        nounCase = lang.getAdverbNounCase(target.game, modifierResource.id, adjectiveId, directNounId);
        adverb = nounCase.adverb;
        break;
      default:
        nounCase = lang.getPrepositionNounCase(target.game, preposition?.id ?? null, adjectiveId, directNounId);
        break;
      }


    let prepositionedInstrumentNounCase: string | null = null;
    if (messagePayload.instruments != null && messagePayload.instruments.length > 0) {
      const instrument = messagePayload.instruments[0];
      const preposition: any | null = (instrument.preposition != null) ? ResourceDictionary.get(this.game, instrument.preposition.prepositionResource) : null;
      const prepositionId: string | null = preposition != null ? (preposition.id != null ? preposition.id : preposition) : null;
      const nounId = ResourceDictionary.get(target.game, attrs.Attribute.value<string>(instrument.entity, attrs.NounResourceId, target));
      const prepNounCase = lang.getPrepositionNounCase(target.game, prepositionId, adjectiveId, nounId);
      prepositionedInstrumentNounCase = lang.compose(resource.templatePrepositionedInstrument, {
        preposition: prepNounCase.preposition != null ? prepNounCase.preposition.preposition() : '',
        indirect: prepNounCase.nounCase != null ? prepNounCase.nounCase : '',
      });
    }

    if (messagePayload.indirects == null || messagePayload.indirects.length === 0) {
      target.game.addToGameLog({
        source: target.id,
        action: SupportsSeeing.ID,
        category: 'game-log-skill',
        description: lang.compose(resource.templateSimple, {
          actorNoun: actorTitle.title,
          tryVerb: tryVerb,
          skillVerb: skillVerb,
          adjective: nounCase.adjectiveCase ?? '',
          directPreposition: directPreposition ?? '',
          directNoun: direct !== actor ? nounCase.nounCase : '',
          adverbBegin: adverb != null && adverb.position() === lang.AdverbPosition.begin ? adverb.adverb() : '',
          adverbMiddle: adverb != null && adverb.position() === lang.AdverbPosition.middle ? adverb.adverb() : '',
          adverbEnd: adverb != null && (adverb.position() === lang.AdverbPosition.end || adverb.position() == null) ? adverb.adverb() : '',
          pronoun: pronounCase ?? '',
          prepositionedInstrumentNoun: prepositionedInstrumentNounCase ?? '',
        }),
      });
    } else {
      const prepositionedIndirects = messagePayload.indirects.map(indirect => {
        const preposition: any | null = (indirect.preposition != null) ? ResourceDictionary.get(this.game, indirect.preposition.prepositionResource) : null;
        const prepositionId: string | null = preposition != null ? (preposition.id != null ? preposition.id : preposition) : null;
        const adjectiveId = ResourceDictionary.get(target.game, attrs.Attribute.value<string>(indirect.entity, attrs.AdjectiveResourceId, target));
        const nounId = ResourceDictionary.get(target.game, attrs.Attribute.value<string>(indirect.entity, attrs.NounResourceId, target));
        const prepNounCase = lang.getPrepositionNounCase(target.game, prepositionId, adjectiveId, nounId);
        return lang.compose(resource.templatePrepositionedIndirect, {
          preposition: prepNounCase.preposition != null ? prepNounCase.preposition.preposition() : '',
          adjective: prepNounCase.adjectiveCase ?? '',
          indirect: prepNounCase.nounCase != null ? prepNounCase.nounCase : '',
        });
      });

      const indirectsMerged = lang.mergeList(prepositionedIndirects, resource.sourcesSeparator, resource.sourcesConjunction);
      const template = (messagePayload.actorId !== messagePayload.directId) ? resource.templateWithIndirects : resource.templateWithIndirectsActorIsDirect;
      target.game.addToGameLog({
        source: target.id,
        action: SupportsSeeing.ID,
        category: 'game-log-skill',
        description: lang.compose(template, {
          actorNoun: actorTitle.title,
          tryVerb: tryVerb,
          skillVerb: skillVerb,
          adjective: nounCase.adjectiveCase ?? '',
          directNoun: nounCase.nounCase,
          adverbBegin: adverb != null && adverb.position() === lang.AdverbPosition.begin ? adverb.adverb() : '',
          adverbMiddle: adverb != null && adverb.position() === lang.AdverbPosition.middle ? adverb.adverb() : '',
          adverbEnd: adverb != null && (adverb.position() === lang.AdverbPosition.end || adverb.position() == null) ? adverb.adverb() : '',
          pronoun: pronounCase ?? '',
          prepositionedIndirects: indirectsMerged,
          prepositionedInstrumentNoun: prepositionedInstrumentNounCase ?? '',
        }),
      });
    }
  }

  private handleActorSceneChanged(target: ent.Entity, initialState: SupportsSeeingState | null, message: Message) {
    this.renderSceneDescription(target);
  }

  private renderSceneDescription(target: ent.Entity) {
    const currentScene = target.game.instances.scenes.get(attrs.Attribute.value<string>(target, attrs.CurrentSceneId, target));
    if (currentScene != null) {
      target.game.addToGameLog({
        source: target.id,
        action: SupportsSeeing.ID,
        category: 'game-log-separator',
      });
      target.game.addToGameLog({
        source: target.id,
        action: SupportsSeeing.ID,
        title: attrs.Attribute.value<string>(currentScene, attrs.Title, target),
        category: 'game-log-scene-desc',
        description: attrs.Attribute.value<string>(currentScene, attrs.Description, target),
      });
      target.game.addToGameLog({
        source: target.id,
        action: SupportsSeeing.ID,
        category: 'game-log-spatial',
        descriptionLines: this._spatialRelationsRenderer.render(currentScene.id),
      });
    }
  }

  private handleAppearanceChange(target: ent.Entity, initialState: SupportsSeeingState | null, message: Message) {
    const messagePayload = payload<Topics.APPEARANCE_CHANGE>(message);
    let canSee = true;
    // appearance change without scene happens in ambience and can be seen everywhere
    if (messagePayload.sourceSceneId != null) {
      const appearanceChangeScene = target.game.instances.getSceneFromId(messagePayload.sourceSceneId);
      const targetScene = scene.getCurrentScene(target);
      if (appearanceChangeScene != null && targetScene != null && appearanceChangeScene !== targetScene) {
        const path = pathfinding.find(target, new scene.VisibilitySceneGraph(target.game), appearanceChangeScene, targetScene, (a, f, s) => 0.1);
        canSee = path[0].node === appearanceChangeScene && path[path.length - 1].node === targetScene;
      }
    }
    if (canSee) {
      if (messagePayload.appearanceDescriptionResourceId != null) {
        target.game.addToGameLog({
          source: target.id,
          action: SupportsSeeing.ID,
          category: 'game-log-skill',
          description: lang.capitalFirst(
            lang.compose(ResourceDictionary.get(target.game, messagePayload.appearanceDescriptionResourceId).template, messagePayload.appearanceDescriptionResourceTemplateBag)
          ),
        });
      }
      if (messagePayload.appearanceDescription != null) {
        target.game.addToGameLog({
          source: target.id,
          action: SupportsSeeing.ID,
          category: 'game-log-skill',
          description: lang.compose(messagePayload.appearanceDescription),
        });
      }
    }
  }

  private handleSceneEntered(targetEntity: ent.Entity, initialState: SupportsSeeingState | null, message: Message) {
    const messagePayload = payload<Topics.SCENE_ENTERED>(message);
    const entity = targetEntity.game.instances.getAnyFromId(messagePayload.entityId);

    // appearance change without scene happens in ambience and can be seen everywhere
    if (entity != null && entity !== targetEntity && messagePayload.sceneId != null) {
      this.logSceneChange(targetEntity, entity, messagePayload.sceneId, DefaultResDict.SCENE_ENTERED);
    }
  }

  private handleSceneLeft(targetEntity: ent.Entity, initialState: SupportsSeeingState | null, message: Message) {
    const messagePayload = payload<Topics.SCENE_LEFT>(message);
    const entity = targetEntity.game.instances.getAnyFromId(messagePayload.entityId);

    // appearance change without scene happens in ambience and can be seen everywhere
    if (entity != null && entity !== targetEntity && messagePayload.sceneId != null) {
      this.logSceneChange(targetEntity, entity, messagePayload.sceneId, DefaultResDict.SCENE_LEFT);
    }
  }

  private logSceneChange(targetEntity: ent.Entity, entity: ent.Entity, sceneId: string, descriptionResourceId: string) {
    const entityScene = targetEntity.game.instances.getSceneFromId(sceneId);
    const targetScene = scene.getCurrentScene(targetEntity);
    if (entityScene != null && targetScene != null && entityScene === targetScene) {
      if (ent.getIsVisible(entity)) {
        const resource = ResourceDictionary.get(entity.game, descriptionResourceId);
        const entityTitle = ent.getTitle(entity, lang.Case.nominative);
        if (entityTitle.noun != null) {
          const sceneTitle = ent.getTitle(entityScene, resource.nounCase ?? lang.Case.nominative, lang.CapitalizationOptions.keep);

          const verb = lang.Verb.get(targetEntity.game, resource.verbId).verb(entityTitle.noun.number, lang.Tense.past, lang.Person.third, entityTitle.noun.gender, entityTitle.noun.genderSubtype);
          targetEntity.game.addToGameLog({
            source: targetEntity.id,
            action: SupportsSeeing.ID,
            category: 'game-log-skill',
            description: lang.capitalFirst(
              lang.compose(resource.template, {
                noun: entityTitle.title,
                verb: verb,
                sceneNoun: sceneTitle.title,
              })
            ),
          });
        }
      }
    }
  }

  private logSubstanceEvaporated(target: ent.Entity, initialState: SupportsSeeingState | null, message: Message) {
    const messagePayload = payload<Topics.EVAPORATED>(message);
    const resource = messagePayload.newVolume > 0 ? ResourceDictionary.get(target.game, DefaultResDict.EVAPORATED_SOME_DESC) : ResourceDictionary.get(target.game, DefaultResDict.EVAPORATED_DESC);
    const directNoun = lang.Noun.get(target.game, ResourceDictionary.get(target.game, messagePayload.substanceNounId));
    const indirectEntity = messagePayload.indirectId != null ? target.game.instances.getEntityFromId(messagePayload.indirectId) : null;
    const relation = messagePayload.spatialRelationTypeId != null ? SpatialPrepositions.allById().get(messagePayload.spatialRelationTypeId) : null;
    if (indirectEntity != null && relation != null) {
      const prepositionId = ResourceDictionary.get(target.game, relation.prepositionDescriptionResource);
      const prepositionNounCase = lang.getPrepositionNounCase(
        target.game,
        prepositionId,
        ResourceDictionary.get(target.game, attrs.Attribute.value<string>(indirectEntity, attrs.AdjectiveResourceId)),
        ResourceDictionary.get(target.game, attrs.Attribute.value<string>(indirectEntity, attrs.NounResourceId))
      );
      if (prepositionNounCase.noun != null && prepositionNounCase.preposition != null && prepositionNounCase.nounCase != null) {
        target.game.addToGameLog({
          source: target.id,
          action: SupportsSeeing.ID,
          category: 'game-log-async-event',
          description: lang.capitalFirst(lang.compose(
            resource.template,
            {
              direct: directNoun.noun(resource.directCase),
              preposition: prepositionNounCase.preposition.preposition(),
              indirect: prepositionNounCase.nounCase,
              evaporateVerb: lang.Verb.get(target.game, resource.evaporateVerbId).verb(directNoun.number, lang.Tense.past, lang.Person.third, directNoun.gender, directNoun.genderSubtype),
            }
          )),
        });
      }
    }
  }

  private logTookDamage(entity: ent.Entity, initialState: SupportsSeeingState | null, message: Message) {
    const messagePayload = payload<Topics.TOOK_DAMAGE>(message);
    const resource = ResourceDictionary.get(entity.game, DefaultResDict.TOOK_DAMAGE_DESCRIPTION);
    const damagedEntity = entity.game.instances.getAnyFromId(messagePayload.entityId);
    const entityTitle = ent.getTitle(damagedEntity, lang.Case.nominative);
    if (entityTitle.noun != null) {
      const beVerb = lang.Verb.get(entity.game, resource.beVerbId).verb(entityTitle.noun.number, lang.Tense.past, lang.Person.third, entityTitle.noun.gender, entityTitle.noun.genderSubtype);
      const hitVerb = lang.Verb.get(entity.game, resource.hitVerbId).verb(entityTitle.noun.number, lang.Tense.passive, lang.Person.third, entityTitle.noun.gender, entityTitle.noun.genderSubtype);
      entity.game.addToGameLog({
        source: entity.id,
        action: SupportsSeeing.ID,
        category: 'game-log-skill',
        description: lang.capitalFirst(
          lang.compose(resource.template, {
            actorNoun: entityTitle.title,
            beVerb: beVerb,
            hitVerb: hitVerb,
          })
        ),
      });
    }
  }

  private logDied(entity: ent.Entity, initialState: SupportsSeeingState | null, message: Message) {
    const messagePayload = payload<Topics.DIED>(message);
    const resource = ResourceDictionary.get(entity.game, DefaultResDict.DIED_DESCRIPTION);
    const damagedEntity = entity.game.instances.getAnyFromId(messagePayload.entityId);
    const entityTitle = ent.getTitle(damagedEntity, lang.Case.nominative);
    if (entityTitle.noun != null) {
      const dieVerb = lang.Verb.get(entity.game, resource.dieVerbId).verb(entityTitle.noun.number, lang.Tense.past, lang.Person.third, entityTitle.noun.gender, entityTitle.noun.genderSubtype);
      entity.game.addToGameLog({
        source: entity.id,
        action: SupportsSeeing.ID,
        category: 'game-log-skill',
        description: lang.capitalFirst(
          lang.compose(resource.template, {
            actorNoun: entityTitle.title,
            dieVerb: dieVerb,
          })
        ),
      });
    }
  }
}