import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { TimeSource } from 'snowatch/time-source';
import { Channel } from 'snowatch/bus';
import * as result from 'snowatch/result';

export interface TimeSourceUnpauseOnInputState extends BehaviorState {
}

export class TimeSourceUnpauseOnInput extends Behavior {
  public static readonly ID = 'TimeSourceUnpauseOnInput';

  get busChannel(): Channel {
    return this.game.bus.input;
  }

  filter(target: Entity, state: TimeSourceUnpauseOnInputState | null, message: Message): result.ResultObject {
    // only unpause if timesource is currently paused
    if( !TimeSource.isRunning() ) {
      return result.make(result.rejected);
    }

    return result.make(result.accepted);
  }

  process(target: Entity, state: TimeSourceUnpauseOnInputState, message: Message) {
    this.logger.debug('Unpausing time source because of user interaction');
    TimeSource.unpause();
  }
}