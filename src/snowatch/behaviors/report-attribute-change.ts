import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface ReportAttributeChangeResourceGetter {
  (attributeId: string, target: Entity, message: Message): string;
}

export interface ReportAttributeChangeState extends BehaviorState {
  attributesIds: Array<string>;
}

export class ReportAttributeChange extends Behavior {
  public static readonly ID = 'ReportAttributeChange';

  setup(target: Entity, state: any) {
  }

  filter(target: Entity, state: ReportAttributeChangeState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.ATTRIBUTE_VALUE_CHANGED) ) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.ATTRIBUTE_VALUE_CHANGED>(message);
    return (state != null && messagePayload.entityId === target.id
      && state.attributesIds.includes(messagePayload.attributeId))
      ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: ReportAttributeChangeState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    const messagePayload = payload<Topics.ATTRIBUTE_VALUE_CHANGED>(message);

    const logMessage = this.subProcess(target, state, message);
    target.game.addToGameLog({
      action: ReportAttributeChange.ID,
      description: logMessage,
      source: target.id
    });
  }

  static with(target: Entity, message: Message): AttributeChangeDescriptionBuilder {
    return new AttributeChangeDescriptionBuilder(target, message);
  }
}

interface TextEvalEvalFn {
  (entity: Entity): string;
}

class AttributeChangeDescriptionBuilder {
  entity: Entity;
  changeData: Topics.ATTRIBUTE_VALUE_CHANGED;
  hasAttributeMatch: boolean = false;
  expectedType: string;
  resultString: string | null = null;

  constructor(entity: Entity, message: Message) {
    this.entity = entity;
    this.changeData = payload<Topics.ATTRIBUTE_VALUE_CHANGED>(message);
  }

  forAttribute(attributeId: string): AttributeChangeDescriptionBuilder {
    this.hasAttributeMatch = (this.changeData.attributeId === attributeId);
    return this;
  }

  ifGoesUp<T extends attrs.NumericValueAttribute>(func: TextEvalEvalFn) {
    if( this.hasAttributeMatch && (<T>this.changeData.newValue).value > (<T>this.changeData.oldValue).value) {
      this.resultString = func(this.entity);
    }

    return this;
  }

  ifGoesDown<T extends attrs.NumericValueAttribute>(func: TextEvalEvalFn) {
    if( this.hasAttributeMatch  && (<T>this.changeData.newValue).value < (<T>this.changeData.oldValue).value) {
      this.resultString = func(this.entity);
    }

    return this;
  }

  ifIsMin<T extends attrs.NumericValueAttribute>(func: TextEvalEvalFn) {
    if( this.hasAttributeMatch ) {
      const attrValue = attrs.Attribute.value<T>(this.entity, this.changeData.attributeId);
      if( (<T>this.changeData.newValue).value === attrValue.minValue ) {
        this.resultString = func(this.entity);
      }
    }

    return this;
  }

  ifIsMax<T extends attrs.NumericValueAttribute>(func: TextEvalEvalFn) {
    if( this.hasAttributeMatch ) {
      const attrValue = attrs.Attribute.value<T>(this.entity, this.changeData.attributeId);
      if( (<T>this.changeData.newValue).value === attrValue.maxValue ) {
        this.resultString = func(this.entity);
      }
    }

    return this;
  }

  ifIsTrue<T extends boolean>(func: TextEvalEvalFn) {
    if( this.hasAttributeMatch  && this.changeData.newValue === true) {
      this.resultString = func(this.entity);
    }

    return true;
  }

  ifIsFalse<T extends boolean>(func: TextEvalEvalFn) {
    if( this.hasAttributeMatch  && this.changeData.newValue === false) {
      this.resultString = func(this.entity);
    }

    return true;
  }

  getText(): string {
    return this.resultString ?? '';
  }
}