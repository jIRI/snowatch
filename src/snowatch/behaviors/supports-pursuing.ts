import { Behavior, BehaviorState, BehaviorStatus } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { TimedMessagePublication, TimedMessagePublicationState } from 'snowatch/behaviors/timed-message-publication';
import { TimeSource } from 'snowatch/time-source';
import { ActorTraversabilitySceneGraph, Exit, getCurrentScene, Scene } from 'snowatch/scene';
import * as spatial from 'snowatch/spatial-relations';
import * as pathfinding from 'snowatch/pathfinding';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';
import { Move, MoveModifiers } from 'snowatch/skills/move';
import { Engage } from 'snowatch/skills/all';
import { evaluate } from 'snowatch/utils';
import { Actor } from 'snowatch/actor';

export interface SupportsPursuingState extends BehaviorState {
  targetId: string;
  allowedSceneIds: Array<string>;
  delayMillis: number;
}

export class SupportsPursuing extends Behavior {
  public static readonly ID = 'SupportsPursuing';

  constructor() {
    super();
  }

  setup(target: Entity, state: SupportsPursuingState) {
    if( !attrs.Attribute.has(target, attrs.ActorMode) ) {
      attrs.Attribute.add(target, attrs.ActorMode, { initialState : { value: attrs.ActorModes.Normal } });
    }
    if( !attrs.Attribute.has(target, attrs.AttackStage) ) {
      attrs.Attribute.add(target, attrs.AttackStage, { initialState : { value: attrs.AttackStages.Ready } });
    }
    if( !attrs.Attribute.has(target, attrs.CanPursue) ) {
      attrs.Attribute.add(target, attrs.CanPursue);
    }
    if( !attrs.Attribute.has(target, attrs.CanBePursued) ) {
      attrs.Attribute.add(target, attrs.CanBePursued);
    }
    if( !attrs.Attribute.has(target, attrs.IsPursuable) ) {
      attrs.Attribute.add(target, attrs.IsPursuable);
    }

    Behavior.add(target,
      TimedMessagePublication.ID,
      { initialState: {configs: []} as TimedMessagePublicationState }
    );
  }

  filter(target: Entity, state: SupportsPursuingState | null, message: Message): result.ResultObject {
    switch( message.topicId ) {
      case Topic.id(Topics.EVADED): {
        const messagePayload = payload<Topics.EVADED>(message);
        return (messagePayload.targetId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
      }

      case Topic.id(Topics.PURSUE_STEP_NEXT): {
        const messagePayload = payload<Topics.PURSUE_STEP_NEXT>(message);
        return (messagePayload.entityId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
      }
      default:
        return result.make(result.rejected);
    }
  }

  process(target: Entity, state: SupportsPursuingState, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    if( message.topicId === Topic.id(Topics.EVADED) ) {
      const messagePayload = payload<Topics.EVADED>(message);
      const pursuedEntity = target.game.instances.getEntityFromId(messagePayload.actorId);
      if( pursuedEntity  == null ) {
        this.logger.debug(`Unable to find attacked entity ${messagePayload.targetId}`);
        return;
      }

      switch( attrs.Attribute.value<attrs.ActorModes>(target, attrs.ActorMode) ) {
        case attrs.ActorModes.Fighting:
        case attrs.ActorModes.Engaged:
          this.stopPursuing(target);
          return;
        default: {
          this.logger.debug(`Actor ${target.id} is not in pursuing mode`);
          let isPursuing = attrs.Attribute.ifAttributesThenSet(
            [{ entity: target, withAttributeId: attrs.CanPursue, valueMustBe: true }, { entity: pursuedEntity, withAttributeId: attrs.CanBePursued, valueMustBe: true }, ],
            [{ entity: pursuedEntity, withAttributeId: attrs.IsPursuable, setValueTo: true },]
          );
          isPursuing &&= attrs.Attribute.ifAttributesThenSet(
            [{ entity: pursuedEntity, withAttributeId: attrs.IsPursuable, valueMustBe: true },],
            [{ entity: target, withAttributeId: attrs.ActorMode, setValueTo: attrs.ActorModes.Pursuing }]
          );

          if( isPursuing === true ) {
            state.targetId = pursuedEntity.id;
            // theoretically, we should invoke the Pursue skill here, but that would require some major refactoring of the skill to support passing
            // additional params like the map of scenes where we can pursue and delays etc.
            // it is not exactly worth it, because pursue is not supposed to be used as a skill, it is really more of the behavior, so we implement it all here
            // and there is no Pursue skill per se
            this.continuePursuing(target, state);
          }

          return;
        }
      }
    }

    if( message.topicId === Topic.id(Topics.PURSUE_STEP_NEXT) && state.targetId != null ) {
      // if we are already Engaged someone, stop pursuing
      if( attrs.Attribute.value<attrs.ActorModes>(target, attrs.ActorMode) === attrs.ActorModes.Engaged ) {
        this.stopPursuing(target);
        return;
      }

      const pursuedEntity = target.game.instances.getEntityFromId(state.targetId);
      if( pursuedEntity == null ) {
        this.logger.debug(`Unable to find pursued entity ${state.targetId}`);
        // unable to find pursued entity, stop pursuing
        this.stopPursuing(target);
        return;
      }

      if( attrs.Attribute.applies(pursuedEntity, attrs.IsDead) && attrs.Attribute.value<boolean>(pursuedEntity, attrs.IsDead) ) {
        // pursued entity is dead, stop pursuing
        this.stopPursuing(target);
        return;
      }

      const destinationScene = getCurrentScene(pursuedEntity);
      if( destinationScene == null ) {
        // unable to find pursued entity scene, stop pursuing
        this.logger.debug(`Unable to find pursuedEntity ${state.targetId} scene`);
        this.stopPursuing(target);
        return;
      }

      const startScene = getCurrentScene(target);
      if( startScene == null ) {
        // unable to find own entity scene, stop pursuing
        this.logger.debug(`Unable to find scene of entity ${target.id}`);
        this.stopPursuing(target);
        return;
      }

      // if destination and start scene are the same, do not search for path, try to move to the actor
      // if there is spatial relation between actor and anything, try to move to the same spatial relation
      // once there, engage
      // if fails, try again after some delay
      if( startScene.id === destinationScene.id) {
        const ownSpatialRelations = spatial.SpatialRelations.relationsForDirect(target);
        const pursuedEntitySpatialRelations = spatial.SpatialRelations.relationsForDirect(pursuedEntity);
        if( pursuedEntitySpatialRelations.length > 0 ) {
          if( pursuedEntitySpatialRelations[0].indirectId === ownSpatialRelations[0].indirectId
              && pursuedEntitySpatialRelations[0].typeId === ownSpatialRelations[0].typeId
          ) {
            target.game.bus.commands.publish(broadcast(
              Topics.TRY_USE_SKILL,
              {
                skillId : Engage.ID,
                actorId : target.id,
                directId : pursuedEntity.id,
                modifierId: '',
                indirectsIds: [],
              },
              undefined,
              target.id
              ));
              return;
            } else {
              target.game.bus.commands.publish(broadcast(
                Topics.TRY_USE_SKILL,
                {
                skillId : Move.ID,
                actorId : target.id,
                directId : target.id,
                modifierId: MoveModifiers.hesitantly.id,
                directPrepositionId: pursuedEntitySpatialRelations[0].typeId,
                indirectsIds: [pursuedEntitySpatialRelations[0].indirectId],
              },
              undefined,
              target.id
              ));

              this.continuePursuing(target, state);
              return;
          }
        }
      }

      // start and destination scenes are not the same, find path to destination scene
      const path = pathfinding.find(target, new ActorTraversabilitySceneGraph(target.game), startScene, destinationScene, (a, f, s) => 0.1);
      if( path[0].node === startScene && path[path.length - 1].node === destinationScene ) {
        const pathElements = path.map(e => {
          return {
            sceneId: (e.node as Scene).id,
            exitId: e.link != null ? (e.link as Exit).id : null
          };
        });

        const nextPathElement = pathElements.shift();
        if( nextPathElement == null || nextPathElement.sceneId == null || nextPathElement.exitId == null) {
          this.continuePursuing(target, state);
          return;
        }

        // if next scene is outside of accessible scenes, stay in pursuing state, but do nothing
        const exit = startScene.getExit(nextPathElement.exitId);
        if( exit != null ) {
          const nextSceneId = evaluate(exit.scene, exit, target);
          if( state.allowedSceneIds.find(sceneId => sceneId === nextSceneId) == null ) {
            this.continuePursuing(target, state);
            return;
          } else {
            // otherwise try move to the next scene ...
            Scene.tryTakeExit(target.game, target as Actor, exit);
          }
        } else {
          // invalid scene?
          this.logger.debug(`Undefined exit ${nextPathElement.exitId}`);
          return;
        }

        // if we are now in the same scene as pursuedEntity, try engage
        target.game.bus.commands.publish(broadcast(
          Topics.TRY_USE_SKILL,
          {
            skillId : Engage.ID,
            actorId : target.id,
            directId : pursuedEntity.id,
            modifierId: '',
            indirectsIds: [],
          },
          undefined,
          target.id
          ));
      }
    }
  }

  private continuePursuing(target: Entity, state: SupportsPursuingState) {
    const timedPublicationBehaviorState = Behavior.getState(target, TimedMessagePublication.ID) as TimedMessagePublicationState;
    timedPublicationBehaviorState.configs.push({
      messageId: Topic.id(Topics.PURSUE_STEP_NEXT),
      messagePayload: new Topics.PURSUE_STEP_NEXT({ entityId: target.id}),
      publishAtTicks: TimeSource.getWallClockTicksFromNow(target.game, state.delayMillis),
    });
  }

  private stopPursuing(target: Entity) {
    attrs.Attribute.ifAttributesThenSet(
      [{ entity: target, withAttributeId: attrs.ActorMode, valueMustBe: attrs.ActorModes.Pursuing },],
      [{ entity: target, withAttributeId: attrs.ActorMode, setValueTo: attrs.ActorModes.Normal }]
    );
    attrs.Attribute.ifAttributesThenSet(
      [{ entity: target, withAttributeId: attrs.ActorMode, valueMustBe: attrs.ActorModes.Pursuing }, { entity: target, withAttributeId: attrs.AttackStage },],
      [{ entity: target, withAttributeId: attrs.AttackStage, setValueTo: attrs.AttackStages.Ready }]
    );
  }
}