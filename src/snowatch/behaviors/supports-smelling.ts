import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { getCurrentSceneId } from 'snowatch/scene';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { DefaultResDict } from 'snowatch/resources/default';
import * as lang from 'snowatch/language-structures';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface SupportsSmellingState extends BehaviorState {
  value: boolean;
}

export class SupportsSmelling extends Behavior {
  public static readonly ID = 'SupportsSmelling';

  constructor() {
    super();
  }

  setup(target: Entity, state: SupportsSmellingState) {
    // we can prevent autoinit by setting the attributes explicitly
    if (!attrs.Attribute.has(target, attrs.CanSmell)) {
      attrs.Attribute.add(target, attrs.CanSmell, { initialState: { value: true } });
    }
  }

  filter(target: Entity, state: SupportsSmellingState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.SMELL)) {
        return result.make(result.rejected);
    }

    return (attrs.Attribute.applies(target, attrs.CanSmell, target) && attrs.Attribute.value<boolean>(target, attrs.CanSmell, target))
      ? result.make(result.accepted)
      :  result.make(result.rejected);
  }

  process(target: Entity, state: SupportsSmellingState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    this.logSubstanceSmell(target, state, message);
  }

  logSubstanceSmell(target: Entity, state: SupportsSmellingState | null, message: Message): any {
    const messagePayload = payload<Topics.SMELL>(message);
    let canSmell = true;
    // smell without scene happens in ambience and can be smelled everywhere
    if( messagePayload.sourceSceneId != null ) {
      canSmell = messagePayload.sourceSceneId === getCurrentSceneId(target);
    }
    if( canSmell ) {
      const directNoun = lang.Noun.get(target.game, ResourceDictionary.get(target.game, messagePayload.substanceNounId));
      if( messagePayload.smellDescriptionResourceId != null ) {
        const resource = ResourceDictionary.get(target.game, messagePayload.smellDescriptionResourceId);
        const actorNoun = lang.Noun.get(this.game, ResourceDictionary.get(target.game, attrs.Attribute.value<string>(target, attrs.NounResourceId, target)));
        target.game.addToGameLog({
          action: SupportsSmelling.ID,
          description: lang.compose(resource.template, {
            actorNoun: lang.capitalFirst(actorNoun.noun(resource.actorNounCase)),
            smellVerb: lang.Verb.get(this.game, resource.smellVerbId).verb(actorNoun.number, lang.Tense.past, lang.Person.third, actorNoun.gender, actorNoun.genderSubtype),
            direct: directNoun.noun(resource.directCase)
          }),
          source: target.id
        });
      } else {
        const resource = ResourceDictionary.get(target.game, DefaultResDict.SMELL_DESCRIPTION);
        target.game.addToGameLog({
          action: SupportsSmelling.ID,
          description: lang.compose(resource.template, {
            beVerb: lang.Verb.get(target.game, resource.beVerbId).verb(directNoun.number, lang.Tense.past, lang.Person.third, directNoun.gender, directNoun.genderSubtype),
            smellVerb: resource.smellVerbId != null ? lang.Verb.get(target.game, resource.smellVerbId).verb() : null,
            smellNoun: resource.smellNounId != null ?  lang.Noun.get(target.game, resource.smellNounId).noun(resource.smellNounCase) : null,
            // TODO: here we should handle compounds
            direct: directNoun.noun(resource.directCase),
          }),
          source: target.id
        });

      }
    }
  }
}