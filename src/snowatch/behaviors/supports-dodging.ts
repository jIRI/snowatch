import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface SupportsDodgingState extends BehaviorState {
}

export class SupportsDodging extends Behavior {
  public static readonly ID = 'SupportsDodging';

  constructor() {
    super();
  }

  setup(target: Entity, state: SupportsDodgingState) {
    if( !attrs.Attribute.has(target, attrs.ActorMode) ) {
      attrs.Attribute.add(target, attrs.ActorMode, { initialState : { value: attrs.ActorModes.Normal } });
    }
    if( !attrs.Attribute.has(target, attrs.AttackStage) ) {
      attrs.Attribute.add(target, attrs.AttackStage, { initialState : { value: attrs.AttackStages.Ready } });
    }
    if( !attrs.Attribute.has(target, attrs.CanDodge) ) {
      attrs.Attribute.add(target, attrs.CanDodge);
    }
    if( !attrs.Attribute.has(target, attrs.CanBeDodged) ) {
      attrs.Attribute.add(target, attrs.CanBeDodged);
    }
    if( !attrs.Attribute.has(target, attrs.HasDodgeAvailable) ) {
      attrs.Attribute.add(target, attrs.HasDodgeAvailable);
    }
  }

  filter(target: Entity, state: SupportsDodgingState | null, message: Message): result.ResultObject {
    switch( message.topicId ) {
      case Topic.id(Topics.ATTACK_WINDING_UP):
      case Topic.id(Topics.ATTACK_COOLING_DOWN):
      case Topic.id(Topics.ATTACK_CANCELLING):
      case Topic.id(Topics.ATTACK_LANDING):
      case Topic.id(Topics.ATTACK_FINISHING): {
        const messagePayload = payload<Topics.ATTACK_STAGE_CHANGING>(message);
        return (messagePayload.targetId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
      }
      case Topic.id(Topics.DODGING): {
        const messagePayload = payload<Topics.DODGING>(message);
        return (messagePayload.actorId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
      }
      case Topic.id(Topics.DODGED): {
        const messagePayload = payload<Topics.DODGED>(message);
        return (messagePayload.actorId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
      }
      default:
        return result.make(result.rejected);
    }

  }

  process(target: Entity, state: SupportsDodgingState | null, message: Message) {
    const messagePayload = payload<Topics.ATTACK_STAGE_CHANGING>(message);
    const attacker = target.game.instances.getEntityFromId(messagePayload.actorId);
    if( attacker  == null ) {
      this.logger.debug(`Unable to find attacker entity ${messagePayload.actorId}`);
      return;
    }

    let isApplicableAttackStage = false;
    switch( attrs.Attribute.valueIfApplies<attrs.AttackStages>(target, attrs.AttackStage, attrs.AttackStages.Ready) ) {
      case attrs.AttackStages.Ready:
      case attrs.AttackStages.Windup:
      case attrs.AttackStages.Landed:
        isApplicableAttackStage = true;
        break;
      default:
        break;
    }

    const dodgeabilityConditions = [
      { entity: target, withAttributeId: attrs.CanDodge, valueMustBe: true },
      { entity: attacker, withAttributeId: attrs.CanBeDodged, mustBe: true }
    ];
    switch( message.topicId ) {
      case Topic.id(Topics.DODGING):
        attrs.Attribute.ifAttributesThenSet(
          [{ entity: target, withAttributeId: attrs.ActorMode, valueMustBe: attrs.ActorModes.Fighting }],
          [{ entity: target, withAttributeId: attrs.ActorMode, setValueTo: attrs.ActorModes.Dodging }]
        );
        break;
      case Topic.id(Topics.DODGED):
        attrs.Attribute.ifAttributesThenSet(
          [{ entity: target, withAttributeId: attrs.ActorMode, valueMustBe: attrs.ActorModes.Dodging }],
          [{ entity: target, withAttributeId: attrs.ActorMode, setValueTo: attrs.ActorModes.Fighting }]
        );
        break;
      case Topic.id(Topics.ATTACK_WINDING_UP):
        attrs.Attribute.ifAttributesThenSet(dodgeabilityConditions, [{ entity: target, withAttributeId: attrs.HasDodgeAvailable, setValueTo: isApplicableAttackStage }]);
        break;
      case Topic.id(Topics.ATTACK_COOLING_DOWN):
        attrs.Attribute.ifAttributesThenSet(dodgeabilityConditions, [{ entity: target, withAttributeId: attrs.HasDodgeAvailable, setValueTo: false }]);
        break;
      case Topic.id(Topics.ATTACK_CANCELLING):
        attrs.Attribute.ifAttributesThenSet(dodgeabilityConditions, [{ entity: target, withAttributeId: attrs.HasDodgeAvailable, setValueTo: false }]);
        break;
      case Topic.id(Topics.ATTACK_LANDING):
        attrs.Attribute.ifAttributesThenSet(dodgeabilityConditions, [{ entity: target, withAttributeId: attrs.HasDodgeAvailable, setValueTo: false }]);
        break;
      case Topic.id(Topics.ATTACK_FINISHING):
        attrs.Attribute.ifAttributesThenSet(dodgeabilityConditions, [{ entity: target, withAttributeId: attrs.HasDodgeAvailable, setValueTo: false }]);
        break;
      default:
        this.logger.debug(`Unknown dodge stage change for actor ${target.id}: ${message.topicId}`);
        return;
    }
  }
}