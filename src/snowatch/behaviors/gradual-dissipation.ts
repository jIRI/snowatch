import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { Substance, SubstancePhases, getSubstancePhase } from 'snowatch/substance';
import { Channel } from 'snowatch/bus';
import { TimeSource } from 'snowatch/time-source';
import { Attribute } from 'snowatch/attribute';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface GradualDissipationState extends BehaviorState {
  dissipationRateTicks?: number;
  lastUpdateTicks?: number;
}

export class GradualDissipation extends Behavior {
  public static readonly ID = 'GradualDissipation';

  get busChannel(): Channel | null {
    return this.game.bus.system;
  }

  filter(target: Entity, state: GradualDissipationState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.CLOCK_TICK) || target == null || getSubstancePhase(target) !== SubstancePhases.gas) {
      return result.make(result.rejected);
    }

    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.CLOCK_TICK>(message);
    state.lastUpdateTicks = state.lastUpdateTicks ?? messagePayload.wallClockTicks;
    state.dissipationRateTicks = state.dissipationRateTicks ?? 100;
    return (state.dissipationRateTicks == null
        || messagePayload.wallClockTicks >= (state.lastUpdateTicks + state.dissipationRateTicks)
      ) ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: GradualDissipationState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    const constRateOfDissipation = attrs.Attribute.value<number>(target, attrs.ConstantRateOfDissipation);
    const volume = attrs.Attribute.value<number>(target, attrs.Volume);

    // we vastly simplify things here by ignoring normal model for dissipation and reduce the volume at constant rate
    // well, generally speaking, we shouldn't reduce volume but concentration of the gas in the volume of space,
    // but that would complicate things above what we need right now.
    Substance.decreaseVolume(target, constRateOfDissipation);
    const newVolume = attrs.Attribute.value<number>(target, attrs.Volume);

    target.game.bus.notifications.publish(broadcast(Topics.DISSIPATED, {
        substanceId: target.id,
        substanceNounId: Attribute.value<string>(target, attrs.NounResourceId),
        prevVolume: volume,
        newVolume: newVolume
      },
      undefined,
      target.id)
    );

    if( newVolume <= 0 ) {
      // all of the substance is gone, remove the behavior
      Behavior.remove(target, GradualDissipation.ID);
    }

    state.lastUpdateTicks = TimeSource.getWallClockTicks(target.game);
  }
}