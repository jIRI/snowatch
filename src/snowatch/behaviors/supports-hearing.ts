import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { getCurrentScene, AudibilitySceneGraph } from 'snowatch/scene';
import { find } from 'snowatch/pathfinding';
import { ResourceDictionary } from 'snowatch/resource-dictionary';
import { compose } from 'snowatch/language-structures';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface SupportsHearingState extends BehaviorState {
  value: boolean;
}

export class SupportsHearing extends Behavior {
  public static readonly ID = 'SupportsHearing';

  setup(target: Entity, state: SupportsHearingState) {
    // we can prevent autoinit by setting the attributes explicitly
    if( !attrs.Attribute.has(target, attrs.CanHear) ) {
      attrs.Attribute.add(target, attrs.CanHear, { initialState : { value: true } });
    }
  }

  filter(target: Entity, state: SupportsHearingState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.SOUND) ) {
      return result.make(result.rejected);
    }

    return (attrs.Attribute.applies(target, attrs.CanHear) && attrs.Attribute.value<boolean>(target, attrs.CanHear))
      ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: SupportsHearingState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    const messagePayload = payload<Topics.SOUND>(message);
    let canHear = true;
    // sound without scene happens in ambience and can be heared everywhere
    if( messagePayload.sourceSceneId != null ) {
      const soundScene = target.game.instances.getSceneFromId(messagePayload.sourceSceneId);
      const targetScene = getCurrentScene(target);
      if( soundScene != null && targetScene != null && soundScene !== targetScene ) {
        const path = find(target, new AudibilitySceneGraph(target.game), soundScene, targetScene, (a, f, s) => 0.1);
        canHear = path[0].node === soundScene && path[path.length - 1].node === targetScene;
      }
    }
    if( canHear ) {
      if( messagePayload.soundDescriptionResourceId != null ) {
        target.game.addToGameLog({
          action: SupportsHearing.ID,
          description: compose(ResourceDictionary.get(target.game, messagePayload.soundDescriptionResourceId).template),
          source: target.id
        });
      }
    }
  }
}