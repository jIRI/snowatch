import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { GradualEvaporation, GradualEvaporationState } from 'snowatch/behaviors/gradual-evaporation';
import { SpatialPrepositions } from 'snowatch/prepositions';
import * as result from 'snowatch/result';

export interface EvaporateWhenPouredState extends BehaviorState {
  evaporationCreatesGas?: boolean;
  dissipationRateTicks: number;
  evaporatingSpatialRelationTypeIds: Array<string>;
  evaporationRateTicks: number;
}

export class EvaporateWhenPoured extends Behavior {
  public static readonly ID = 'EvaporateWhenPoured';

  filter(target: Entity, state: EvaporateWhenPouredState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.POURED) ) {
      return result.make(result.rejected);
    }

    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.POURED>(message);
    state.evaporatingSpatialRelationTypeIds = state.evaporatingSpatialRelationTypeIds ?? [SpatialPrepositions.isOn.id];
    return ( messagePayload.substanceId === target.id
        && state.evaporatingSpatialRelationTypeIds.includes(messagePayload.spatialRelationTypeId)
      ) ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: EvaporateWhenPouredState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    state.dissipationRateTicks = state.dissipationRateTicks ?? 100;
    state.evaporationRateTicks = state.evaporationRateTicks ?? 100;
    Behavior.add(target, GradualEvaporation.ID, {
        initialState: <GradualEvaporationState>{
          evaporatesToGas: state.evaporationCreatesGas,
          dissipationRateTicks: state.dissipationRateTicks,
          evaporationRateTicks: state.evaporationRateTicks
        }
      });
  }
}