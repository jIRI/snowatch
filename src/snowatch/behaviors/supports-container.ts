import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { setSceneId } from 'snowatch/scene';
import { setSpatialLocation, getSpatialLocation } from 'snowatch/spatial-relations';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface SupportsContainerState extends BehaviorState {
}

export class SupportsContainer extends Behavior {
  public static readonly ID = 'SupportsContainer';

  filter(target: Entity, state: SupportsContainerState | null, message: Message): result.ResultObject {
    switch( message.topicId ) {
      case Topic.id(Topics.ENTITY_SCENE_CHANGED): {
        const messagePayload = payload<Topics.ENTITY_SCENE_CHANGED>(message);
        return (messagePayload.entityId === attrs.Attribute.value<string>(target, attrs.ContainerId)) ? result.make(result.accepted) : result.make(result.rejected);
      }
      case Topic.id(Topics.ENTITY_ADDED_TO_CONTAINER): {
        const messagePayload = payload<Topics.ENTITY_ADDED_TO_CONTAINER>(message);
        return (messagePayload.entityId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
      }
      case Topic.id(Topics.ENTITY_REMOVED_FROM_CONTAINER): {
        const messagePayload = payload<Topics.ENTITY_REMOVED_FROM_CONTAINER>(message);
        return (messagePayload.entityId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
      }
      default:
        return result.make(result.rejected);
    }
  }

  process(target: Entity, state: SupportsContainerState | null, message: Message) {
    switch( message.topicId ) {
      case Topic.id(Topics.ENTITY_SCENE_CHANGED): {
        const messagePayload = payload<Topics.ENTITY_SCENE_CHANGED>(message);
        setSceneId(target, messagePayload.newSceneId);
        const container = target.game.instances.getSpatialFromId(messagePayload.entityId);
        if( container != null ) {
          setSpatialLocation(target, getSpatialLocation(container));
        }
        break;
      }
      case Topic.id(Topics.ENTITY_ADDED_TO_CONTAINER): {
        const messagePayload = payload<Topics.ENTITY_ADDED_TO_CONTAINER>(message);
        attrs.Attribute.setValue<string>(target, attrs.ContainerId, messagePayload.containerId);
        break;
      }
      case Topic.id(Topics.ENTITY_REMOVED_FROM_CONTAINER): {
        const messagePayload = payload<Topics.ENTITY_REMOVED_FROM_CONTAINER>(message);
        attrs.Attribute.setValue<string>(target, attrs.ContainerId, null);
        break;
      }
      default:
        break;
    }
  }
}
