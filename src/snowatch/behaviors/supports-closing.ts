import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { Container } from 'snowatch/container';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface SupportsClosingState extends BehaviorState {
  value: boolean;
}

export class SupportsClosing extends Behavior {
  public static readonly ID = 'SupportsClosing';

  setup(target: Entity, state: SupportsClosingState) {
    let initialValue = true;

    // opened things are closeable by default
    if( attrs.Attribute.applies(target, attrs.IsOpen) ) {
      initialValue = attrs.Attribute.value<boolean>(target, attrs.IsOpen);
    }
    // locked things are closed by default
    if( attrs.Attribute.applies(target, attrs.IsLocked) ) {
      initialValue = initialValue && !attrs.Attribute.value<boolean>(target, attrs.IsLocked);
    }

    // we can prevent autoinit by setting the attributes explicitly
    if( !attrs.Attribute.has(target, attrs.IsClosed) ) {
      attrs.Attribute.add(target, attrs.IsClosed, { initialState : { value: !initialValue } });
    }
    if( !attrs.Attribute.has(target, attrs.IsCloseable) ) {
      attrs.Attribute.add(target, attrs.IsCloseable, { initialState : { value: initialValue } });
    }
  }

  filter(target: Entity, state: SupportsClosingState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.ATTRIBUTE_VALUE_CHANGED) ) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.ATTRIBUTE_VALUE_CHANGED>(message);
    return ( messagePayload.attributeId === attrs.IsOpen || messagePayload.attributeId === attrs.IsClosed)
      ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: SupportsClosingState | null, message: Message) {
    const messagePayload = payload<Topics.ATTRIBUTE_VALUE_CHANGED>(message);
    switch( messagePayload.attributeId ) {
      case attrs.IsClosed:
        if( attrs.Attribute.applies(target, attrs.IsCloseable) ) {
          attrs.Attribute.setValue<boolean>(target, attrs.IsCloseable, !messagePayload.newValue);
        }
        if( attrs.Attribute.applies(target, attrs.IsContainer) && attrs.Attribute.value<boolean>(target, attrs.IsContainer) ) {
          const container = Container.getContainer(target);
          for(const itemId of container) {
            const item = this.game.instances.items.get(itemId);
            if( item != null && attrs.Attribute.applies(item, attrs.IsVisible) ) {
              attrs.Attribute.setValue<boolean>(item, attrs.IsVisible, !messagePayload.newValue);
            }
          }
        }
        break;
      case attrs.IsOpen:
        if( attrs.Attribute.applies(target, attrs.IsClosed) ) {
          attrs.Attribute.setValue<boolean>(target, attrs.IsClosed, !messagePayload.newValue);
        }
        break;
    }
  }
}