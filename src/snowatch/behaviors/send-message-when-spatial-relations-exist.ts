import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, messageId, broadcastId } from 'snowatch/message';
import { SpatialRelation, SpatialRelations } from 'snowatch/spatial-relations';
import { Topics } from 'snowatch/message-topics/all';
import * as result from 'snowatch/result';

export interface SendMessageWhenSpatialRelationsExistState extends BehaviorState {
  filterSourcesIds: Array<string>;
  filterTargetsIds: Array<string>;
  triggerRelations: Array<SpatialRelation>;
  messageTopicId: string;
  messageTragetId: string;
  messagePayload: any;
  broadcastTopicId: string;
  broadcastPayload: any;
}

export class SendMessageWhenSpatialRelationsExist extends Behavior {
  public static readonly ID = 'SendMessageWhenSpatialRelationsExist';

  filter(target: Entity, state: SendMessageWhenSpatialRelationsExistState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.SPATIAL_RELATION_ADDED) ) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.SPATIAL_RELATION_ADDED>(message);
    return (state != null && state.filterSourcesIds.includes(messagePayload.directId)
      && state.filterTargetsIds.includes(messagePayload.indirectId))
      ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: SendMessageWhenSpatialRelationsExistState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    let triggered = true;
    for(let relation of state.triggerRelations) {
      if( SpatialRelations.find(target.game, relation.directId, relation.indirectId, relation.typeId) == null ) {
        triggered = false;
        break;
      }
    }

    if( triggered ) {
      if( state.messageTopicId != null ) {
        target.game.bus.notifications.publish(messageId(state.messageTopicId, state.messageTragetId, state.messagePayload, undefined, target.id));
      }
      if( state.broadcastTopicId != null ) {
        target.game.bus.notifications.publish(broadcastId(state.broadcastTopicId, state.broadcastPayload, undefined, target.id));
      }
    }
  }
}
