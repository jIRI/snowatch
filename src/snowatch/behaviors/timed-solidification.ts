import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { SubstancePhases, getSubstancePhase } from 'snowatch/substance';
import { SpatialRelations } from 'snowatch/spatial-relations';
import { TimeSource } from 'snowatch/time-source';
import { Channel } from 'snowatch/bus';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface TimedSolidifactionState extends BehaviorState {
  solidifyAtTicks?: number;
}

export class TimedSolidification extends Behavior {
  public static readonly ID = 'TimedSolidifaction';

  get busChannel(): Channel | null {
    return this.game.bus.system;
  }

  filter(target: Entity, state: TimedSolidifactionState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.CLOCK_TICK) || target == null || getSubstancePhase(target) !== SubstancePhases.liquid) {
      return result.make(result.rejected);
    }

    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.CLOCK_TICK>(message);
    state.solidifyAtTicks = state.solidifyAtTicks ?? 1000;
    return (state.solidifyAtTicks == null
        || messagePayload.wallClockTicks >= state.solidifyAtTicks
      ) ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: TimedSolidifactionState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    attrs.Attribute.setValue<SubstancePhases>(target, attrs.SubstancePhase, SubstancePhases.solid);

    const targetSpatialRelations = SpatialRelations.relationsForDirect(target);
    const targetSpatialRelation = targetSpatialRelations != null ? targetSpatialRelations[0] : null;
    target.game.bus.notifications.publish(broadcast(Topics.SOLIDIFIED, {
        substanceId: target.id,
        substanceNounId: attrs.Attribute.value<string>(target, attrs.NounResourceId),
        spatialRelationTypeId: targetSpatialRelation != null ? targetSpatialRelation.typeId : null,
        indirectId: targetSpatialRelation != null ? targetSpatialRelation.indirectId : null,
      },
      undefined,
      target.id)
    );

    Behavior.remove(target, TimedSolidification.ID);
  }
}