import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface ChangeAttributeOnMessageState extends BehaviorState {
  topicId: string;
  changes: Array<{attributeId: string, setTo: any}>;
}

export class ChangeAttributeOnMessage extends Behavior {
  public static readonly ID = 'ChangeAttributeOnMessage';

  filter(target: Entity, state: ChangeAttributeOnMessageState | null, message: Message): result.ResultObject {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return result.make(result.rejected);
    }

    return (state.topicId === message.topicId && target.id === message.sourceId) ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: ChangeAttributeOnMessageState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    for(const change of state.changes) {
      attrs.Attribute.setValue<any>(target, change.attributeId, change.setTo);
    }
  }
}