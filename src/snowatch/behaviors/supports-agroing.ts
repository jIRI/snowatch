import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface SupportsAgroingState extends BehaviorState {
}

export class SupportsAgroing extends Behavior {
  public static readonly ID = 'SupportsAgroing';

  constructor() {
    super();
  }

  setup(target: Entity, state: SupportsAgroingState) {
    if( !attrs.Attribute.has(target, attrs.ActorMode) ) {
      attrs.Attribute.add(target, attrs.ActorMode, { initialState : { value: attrs.ActorModes.Normal } });
    }
  }

  filter(target: Entity, state: SupportsAgroingState | null, message: Message): result.ResultObject {
    switch( message.topicId ) {
      case Topic.id(Topics.ATTACK_WINDUP): {
        const messagePayload = payload<Topics.ATTACK_WINDUP>(message);
        return (messagePayload.targetId === target.id) ? result.make(result.accepted) : result.make(result.rejected);
      }
      default:
        return result.make(result.rejected);
    }

  }

  process(target: Entity, state: SupportsAgroingState | null, message: Message) {
    const targetMode = attrs.Attribute.value<attrs.ActorModes>(target, attrs.ActorMode);
    switch( targetMode ) {
      case attrs.ActorModes.Normal: {
        this.logger.debug(`Actor ${target.id} is accepting switch to engaged mode`);
        break;
      }
      default: {
        this.logger.debug(`Actor ${target.id} is not in engagaeable mode`);
        return;
      }
    }

    switch( message.topicId ) {
      case Topic.id(Topics.ATTACK_WINDUP): {
        const messagePayload = payload<Topics.ATTACK_WINDUP>(message);
        const attackie = target.game.instances.getEntityFromId(messagePayload.actorId);
        if( attackie != null ) {
        this.game.bus.notifications.publish(broadcast(Topics.ENGAGED, {
            actorId: target.id,
            targetId: attackie.id,
          },
          undefined,
          target.id
        ));
        }
      }
      break;
      default:
        this.logger.debug(`Unknown agro message for actor ${target.id}: ${message.topicId}`);
        break;
    }
  }
}