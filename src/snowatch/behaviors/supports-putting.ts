import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface SupportsPuttingState extends BehaviorState {
  value: boolean;
}

export class SupportsPutting extends Behavior {
  public static readonly ID = 'SupportsPutting';

  setup(target: Entity, state: SupportsPuttingState) {
    // we can prevent autoinit by setting the attributes explicitly
    if( !attrs.Attribute.has(target, attrs.IsPutable) ) {
      attrs.Attribute.add(target, attrs.IsPutable, { initialState : { value: true } });
    }
  }

  filter(target: Entity, state: SupportsPuttingState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.ENTITY_ADDED_TO_INVENTORY)
      || message.topicId !== Topic.id(Topics.ENTITY_REMOVED_FROM_INVENTORY)
    ) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.ENTITY_ADDED_TO_INVENTORY | Topics.ENTITY_REMOVED_FROM_INVENTORY>(message);
    return (messagePayload.entityId === target.id)
      ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: SupportsPuttingState | null, message: Message) {
    if( !attrs.Attribute.applies(target, attrs.IsPutable) ) {
      return;
    }

    switch( message.topicId ) {
      case Topic.id(Topics.ENTITY_ADDED_TO_INVENTORY):
        attrs.Attribute.setValue<boolean>(target, attrs.IsPutable, true);
        break;
      case Topic.id(Topics.ENTITY_REMOVED_FROM_INVENTORY):
        attrs.Attribute.setValue<boolean>(target, attrs.IsPutable, false);
        break;
    }
  }
}