import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { getSpatialLocation } from 'snowatch/spatial-relations';
import { SpatialPrepositions } from 'snowatch/prepositions';
import { getCurrentSceneId } from 'snowatch/scene';
import * as result from 'snowatch/result';

export interface SoundSourceDef {
  sourceId: string;
  resourceId: string;
  soundLevel: number;
  soundDuration: number;
}

export interface MakeSoundWhenItemEntersContainerState extends BehaviorState {
  sources: Array<SoundSourceDef>;
}

export class MakeSoundWhenItemEntersContainer extends Behavior {
  public static readonly ID = 'MakeSoundWhenItemEntersContainer';

  filter(target: Entity, state: MakeSoundWhenItemEntersContainerState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.SPATIAL_RELATION_ADDED) ) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.SPATIAL_RELATION_ADDED>(message);
    return (state != null && messagePayload.typeId === SpatialPrepositions.isIn.id
      && messagePayload.indirectId === target.id
      && (state.sources == null || state.sources.find((s: any) => s.sourceId === messagePayload.directId)))
      ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: MakeSoundWhenItemEntersContainerState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    const messagePayload = payload<Topics.SPATIAL_RELATION_ADDED>(message);
    const source = (state.sources ?? []).find((r: any) => r.sourceId === messagePayload.directId);
    const sourceEntity = target.game.instances.getEntityFromId(messagePayload.directId);
    if( source == null || sourceEntity == null ) {
      return;
    }

    this.game.bus.notifications.publish(broadcast(
      Topics.SOUND,
      {
        entityId: target.id,
        soundDescriptionResourceId: source.resourceId,
        soundLevel: source.soundLevel ?? 100,
        duration: source.soundDuration ?? 10,
        sourceSceneId: getCurrentSceneId(sourceEntity),
        sourceLocation: getSpatialLocation(sourceEntity),
      }
    ));
  }
}