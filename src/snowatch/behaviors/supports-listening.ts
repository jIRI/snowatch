import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload, broadcast } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { Learn } from 'snowatch/skills/learn';
import { getCurrentSceneId } from 'snowatch/scene';
import { GameData } from 'snowatch/game-data';
import * as result from 'snowatch/result';

export interface SupportsListeningState extends BehaviorState {
}

export class SupportsListening extends Behavior {
  public static readonly ID = 'SupportsListening';

  static canHear(game: GameData, sourceId: string, targetId: string): boolean {
    if( sourceId === targetId ) {
      // filter talking to yourself
      return false;
    }

    const sourceActor = game.instances.actors.get(sourceId);
    if( sourceActor == null) {
      return false;
    }

    const targetActor = game.instances.actors.get(targetId);
    if( targetActor == null) {
      return false;
    }

    // if they are in the same snowatch, they can hear each other
    // TODO: add spatial distance and speech level, if needed
    return getCurrentSceneId(sourceActor) === getCurrentSceneId(targetActor);
  }

  filter(target: Entity, state: SupportsListeningState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.SPEECH) ) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.SPEECH>(message);
    return (SupportsListening.canHear(target.game, messagePayload.targetId, target.id))
      ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: SupportsListeningState | null, message: Message) {
    const messagePayload = payload<Topics.SPEECH>(message);
    const line = target.game.instances.dialogueLines.get(messagePayload.dialogueLineId);
    if( line == null ) {
      return;
    }

    for(let knowledgeId of line.knowledgeIds ) {
      target.game.bus.commands.publish(broadcast(
        Topics.TRY_USE_SKILL,
        {
          skillId : Learn.ID,
          actorId : target.id,
          directId : knowledgeId,
          modifierId: null,
          indirectsIds: [],
        },
        undefined,
        target.id
      ));
    }
  }
}