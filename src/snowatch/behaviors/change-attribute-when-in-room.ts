import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface ChangeAttributeWhenInRoomState extends BehaviorState {
  sceneIds: Array<string>;
  changes: Array<{attributeId: string, valueWhenIn: any, valueWhenOut: any}>;
}

export class ChangeAttributeWhenInRoom extends Behavior {
  public static readonly ID = 'ChangeAttributeWhenInRoom';

  filter(target: Entity, state: ChangeAttributeWhenInRoomState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.ENTITY_SCENE_CHANGED) ) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.ENTITY_SCENE_CHANGED>(message);
    return (state != null && messagePayload.entityId === target.id
      && ((messagePayload.oldSceneId != null && state.sceneIds.includes(messagePayload.oldSceneId)) || state.sceneIds.includes(messagePayload.newSceneId)))
      ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: ChangeAttributeWhenInRoomState | null, message: Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }
    // newSceneId -- we are entering the scene, disable
    // oldSceneId -- we are leaving the scene, enable
    const messagePayload = payload<Topics.ENTITY_SCENE_CHANGED>(message);
    if( messagePayload.newSceneId != null && state.sceneIds.includes(messagePayload.newSceneId) ) {
      for(let change of state.changes) {
        attrs.Attribute.setValue<any>(target, change.attributeId, change.valueWhenIn);
      }
    } else if ( messagePayload.oldSceneId != null && state.sceneIds.includes(messagePayload.oldSceneId) ) {
      for(let change of state.changes) {
        attrs.Attribute.setValue<any>(target, change.attributeId, change.valueWhenOut);
      }
    }
  }
}