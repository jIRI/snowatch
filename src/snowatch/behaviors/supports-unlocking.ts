import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import * as attrs from 'snowatch/attributes-ids';
import * as result from 'snowatch/result';

export interface SupportsUnlockingState extends BehaviorState {
  value: boolean;
}

export class SupportsUnlocking extends Behavior {
  public static readonly ID = 'SupportsUnlocking';

  setup(target: Entity, state: SupportsUnlockingState) {
    let initialValue = true;

    // locked things are unlockable by default
    if( attrs.Attribute.applies(target, attrs.IsLocked) ) {
      initialValue = attrs.Attribute.value<boolean>(target, attrs.IsLocked);
    }

    // we can prevent autoinit by setting the attributes explicitly
    if( !attrs.Attribute.has(target, attrs.IsUnlocked) ) {
      attrs.Attribute.add(target, attrs.IsUnlocked, { initialState : { value: !initialValue } });
    }
    if( !attrs.Attribute.has(target, attrs.IsUnlockable) ) {
      attrs.Attribute.add(target, attrs.IsUnlockable, { initialState : { value: initialValue } });
    }
  }

  filter(target: Entity, state: SupportsUnlockingState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.ATTRIBUTE_VALUE_CHANGED) ) {
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.ATTRIBUTE_VALUE_CHANGED>(message);
    return (messagePayload.attributeId === attrs.IsLocked || messagePayload.attributeId === attrs.IsUnlocked)
      ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: SupportsUnlockingState | null, message: Message) {
    const messagePayload = payload<Topics.ATTRIBUTE_VALUE_CHANGED>(message);
    switch( messagePayload.attributeId ) {
      case attrs.IsUnlocked:
        if( attrs.Attribute.applies(target, attrs.IsUnlockable) ) {
          attrs.Attribute.setValue<boolean>(target, attrs.IsUnlockable, !messagePayload.newValue);
        }
        break;
      case attrs.IsLocked:
        if( attrs.Attribute.applies(target, attrs.IsUnlocked) ) {
          attrs.Attribute.setValue<boolean>(target, attrs.IsUnlocked, !messagePayload.newValue);
        }
        break;
    }
  }
}