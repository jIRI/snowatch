import { Behavior, BehaviorState } from 'snowatch/behavior';
import { Entity } from 'snowatch/entity';
import { Message, Topic, payload } from 'snowatch/message';
import { Topics } from 'snowatch/message-topics/all';
import { TimeSource } from 'snowatch/time-source';
import { Attack } from 'snowatch/skills/all';
import * as result from 'snowatch/result';
import { Channel } from 'snowatch/bus';

export interface TimeSourceDilationRemovalAfterDelayState extends BehaviorState {
  removeTimeDilationAtTicks?: number;
}

export class TimeSourceDilationRemovalAfterDelay extends Behavior {
  public static readonly ID = 'TimeSourceDilationRemovalAfterDelay';

  get busChannel(): Channel | null {
    return this.game.bus.system;
  }

  filter(target: Entity, state: TimeSourceDilationRemovalAfterDelayState | null, message: Message): result.ResultObject {
    if( message.topicId !== Topic.id(Topics.CLOCK_TICK) || target == null || TimeSource.isTimeDilated() === false ) {
      return result.make(result.rejected);
    }

    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return result.make(result.rejected);
    }

    const messagePayload = payload<Topics.CLOCK_TICK>(message);
    return (state.removeTimeDilationAtTicks != null
        && messagePayload.wallClockTicks >= state.removeTimeDilationAtTicks
      ) ? result.make(result.accepted) : result.make(result.rejected);
  }

  process(target: Entity, state: TimeSourceDilationRemovalAfterDelayState | null, message: Message) {
    this.logger.debug('Removing time source divider because dilation duration elapsed');
    TimeSource.leaveTimeDilation(target.game);
  }
}