
export interface Newable<T> {
  new(init?: Readonly<T>): T;
}