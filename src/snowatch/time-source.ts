import {Message, broadcast} from 'snowatch/message';
import {Topics} from 'snowatch/message-topics/all';
import {GameData} from 'snowatch/game-data';
import {PublishOptions} from 'snowatch/bus';
import * as ent from 'snowatch/entity';

export interface GameClock {
  wallClockTicks: number;
  clockScale: number;
}

export class TimeSource {
  private static readonly _baseClockDivider = 100;
  private static _intervalId = -1;
  private static _pauseCount = 0;
  private static _scaleStack = new Array<number>();
  private static _timeDilationStack = new Array<number>();
  private static _subscriptionDisposer: ent.Disposable | null = null;
  private static _bot = { wallClockTicks: 0, clockScale: 1, };
  private static _message: Message = broadcast(Topics.CLOCK_TICK, TimeSource._bot);
  private static _publishOptions: PublishOptions = { allowAsync: false, debugLog: false, freeze: false };

  static initializeGameClock(game: GameData, clockState: GameClock = TimeSource.bot()) {
    game.state.gameClock = clockState;
    TimeSource.registerClockTickHandler(game);
  }

  static bot(clockScale?: number): GameClock {
    if( clockScale == null ) {
      return Object.assign({}, TimeSource._bot);
    }

    const bot = Object.assign({}, TimeSource._bot);
    bot.clockScale = clockScale;
    return bot;
  }

  static getWallClockTicks(game: GameData): number {
    return game.state.gameClock.wallClockTicks;
  }

  static getWallClockTicksFromNow(game: GameData, millis: number): number {
    return game.state.gameClock.wallClockTicks + 1 + millis / TimeSource._baseClockDivider;
  }

  static numberOfTicksFromMillis(game: GameData, millis: number): number {
    return millis / TimeSource.clockRate(game);
  }

  static setTimeScale(game: GameData, scale: number) {
    game.state.gameClock.clockScale = scale;
    TimeSource.registerClockTickHandler(game);
  }

  static getTimeScale(game: GameData): number {
    return game.state.gameClock.clockScale;
  }

  static pushTimeScaleDivider(game: GameData, scale: number) {
    TimeSource._scaleStack.push(scale);
    TimeSource.setTimeScale(game, TimeSource.calculateCurrentTimeScale());
  }

  static popTimeScaleDivider(game: GameData): number {
    TimeSource._scaleStack.pop();
    TimeSource.setTimeScale(game, TimeSource.calculateCurrentTimeScale());
    return TimeSource.getTimeScale(game);
  }

  static calculateCurrentTimeScale() {
    if( TimeSource._scaleStack.length > 0 && TimeSource._timeDilationStack.length > 0) {
      return TimeSource._scaleStack.reduce((previous, current) => previous * current) * TimeSource._timeDilationStack.reduce((previous, current) => previous * current);
    } else if( TimeSource._scaleStack.length > 0 && TimeSource._timeDilationStack.length === 0) {
      return TimeSource._scaleStack.reduce((previous, current) => previous * current);
    } else if( TimeSource._scaleStack.length === 0 && TimeSource._timeDilationStack.length > 0) {
      return TimeSource._timeDilationStack.reduce((previous, current) => previous * current);
    } else {
      return 1;
    }
  }

  static isRunning(): boolean {
    return TimeSource._pauseCount === 0;
  }

  static pause() {
    TimeSource._pauseCount++;
  }

  static unpause() {
    if( TimeSource._pauseCount > 0 ) {
      TimeSource._pauseCount--;
    } else {
      TimeSource._pauseCount = 0;
    }
  }

  static enterTimeDilation(game: GameData, timesourceDivider: number) {
    TimeSource._timeDilationStack.push(timesourceDivider);
    TimeSource.setTimeScale(game, TimeSource.calculateCurrentTimeScale());
  }

  static leaveTimeDilation(game: GameData) {
    if( TimeSource._timeDilationStack.length > 0 ) {
      TimeSource._timeDilationStack.pop();
      TimeSource.setTimeScale(game, TimeSource.calculateCurrentTimeScale());
    }
  }

  static isTimeDilated(): boolean {
    return this._timeDilationStack.length > 0;
  }

  private static registerClockTickHandler(game: GameData) {
    if( TimeSource._subscriptionDisposer != null ) {
      game.removeDisposer(TimeSource._subscriptionDisposer);
      TimeSource._subscriptionDisposer.dispose();
    }

    TimeSource._intervalId = window.setInterval(() => {
        if( !TimeSource.isRunning() ) {
          return;
        }

        game.state.gameClock.wallClockTicks += 1;
        TimeSource._message.payload.wallClockTicks = game.state.gameClock.wallClockTicks;
        TimeSource._message.payload.clockScale = game.state.gameClock.clockScale;
        game.bus.system.publish(TimeSource._message, TimeSource._publishOptions);
      },
      TimeSource.clockRate(game)
    );

    TimeSource._subscriptionDisposer = {
      dispose() {
        if( TimeSource._intervalId !== -1 ) {
          clearInterval(TimeSource._intervalId);
          TimeSource._intervalId = -1;
        }
      }
    };

    game.disposeOnReset(TimeSource._subscriptionDisposer);
    TimeSource.unpause();
  }

  private static clockRate(game: GameData): number {
    return TimeSource._baseClockDivider / game.state.gameClock.clockScale;
  }
}