import { SpatialRelationDef } from "snowatch/spatial-relations-base-types";
import { Surface } from "snowatch/surface";
import { DefaultResDict } from "snowatch/resources/default";

export interface AccusativePrepositionDef {
  id: string;
  prepositionActionResource: string;
  prepositionDescriptionResource: string;
}

export class AccusativePrepositions {
  public static on: AccusativePrepositionDef = {
    id: 'accusative-on',
    prepositionActionResource: DefaultResDict.ACCUSATIVE_ON,
    prepositionDescriptionResource: DefaultResDict.ACCUSATIVE_ON,
  };

  private static _all: Array<AccusativePrepositionDef>;
  public static all(): Array<AccusativePrepositionDef> {
    AccusativePrepositions._all = AccusativePrepositions._all || [
      AccusativePrepositions.on,
    ];
    return AccusativePrepositions._all;
  }

  private static _allById: Map<string, AccusativePrepositionDef>;
  public static allById(): Map<string, AccusativePrepositionDef> {
    AccusativePrepositions._allById = AccusativePrepositions._allById ?? new Map<string, AccusativePrepositionDef>(AccusativePrepositions.all().map<[string, AccusativePrepositionDef]>(p => [p.id, p]));
    return AccusativePrepositions._allById;
  }
}

export class SpatialPrepositions {
  public static isOn: SpatialRelationDef = {
    id: 'is-on',
    targetSurfaceId: Surface.outerTop,
    prepositionActionResource: DefaultResDict.SPATIAL_ACTION_ON,
    prepositionDescriptionResource: DefaultResDict.SPATIAL_DESCRIPTION_ON,
  };
  public static isOnInner: SpatialRelationDef = {
    id: 'is-on-inner',
    targetSurfaceId: Surface.innerBottom,
    prepositionActionResource: DefaultResDict.SPATIAL_ACTION_ON,
    prepositionDescriptionResource: DefaultResDict.SPATIAL_DESCRIPTION_ON,
  };
  public static isUnder: SpatialRelationDef = {
    id: 'is-under',
    targetSurfaceId: Surface.innerTop,
    prepositionActionResource: DefaultResDict.SPATIAL_ACTION_UNDER,
    prepositionDescriptionResource: DefaultResDict.SPATIAL_DESCRIPTION_UNDER,
  };
  public static isBelow: SpatialRelationDef = {
    id: 'is-below',
    targetSurfaceId: Surface.outerBottom,
    prepositionActionResource: DefaultResDict.SPATIAL_ACTION_BELOW,
    prepositionDescriptionResource: DefaultResDict.SPATIAL_DESCRIPTION_BELOW,
  };
  public static isIn: SpatialRelationDef = {
    id: 'is-into',
    targetSurfaceId: Surface.inside,
    prepositionActionResource: DefaultResDict.SPATIAL_ACTION_IN,
    prepositionDescriptionResource: DefaultResDict.SPATIAL_DESCRIPTION_IN,
  };
  public static isInFront: SpatialRelationDef = {
    id: 'is-in-front',
    targetSurfaceId: Surface.outerFront,
    prepositionActionResource: DefaultResDict.SPATIAL_ACTION_FRONT,
    prepositionDescriptionResource: DefaultResDict.SPATIAL_DESCRIPTION_FRONT,
  };
  public static isInFrontInner: SpatialRelationDef = {
    id: 'is-in-front-inner',
    targetSurfaceId: Surface.innerFront,
    prepositionActionResource: DefaultResDict.SPATIAL_ACTION_FRONT,
    prepositionDescriptionResource: DefaultResDict.SPATIAL_DESCRIPTION_FRONT,
  };
  public static isInBack: SpatialRelationDef = {
    id: 'is-in-back',
    targetSurfaceId: Surface.innerBack,
    prepositionActionResource: DefaultResDict.SPATIAL_ACTION_BEHIND,
    prepositionDescriptionResource: DefaultResDict.SPATIAL_DESCRIPTION_BEHIND,
  };
  public static isBehind: SpatialRelationDef = {
    id: 'is-behind',
    targetSurfaceId: Surface.outerBack,
    prepositionActionResource: DefaultResDict.SPATIAL_ACTION_BACK,
    prepositionDescriptionResource: DefaultResDict.SPATIAL_DESCRIPTION_BACK,
  };
  public static isOnLeft: SpatialRelationDef = {
    id: 'is-on-left',
    targetSurfaceId: Surface.outerLeft,
    prepositionActionResource: DefaultResDict.SPATIAL_ACTION_LEFT,
    prepositionDescriptionResource: DefaultResDict.SPATIAL_DESCRIPTION_LEFT,
  };
  public static isOnLeftInner: SpatialRelationDef = {
    id: 'is-on-left-inner',
    targetSurfaceId: Surface.innerLeft,
    prepositionActionResource: DefaultResDict.SPATIAL_ACTION_LEFT,
    prepositionDescriptionResource: DefaultResDict.SPATIAL_DESCRIPTION_LEFT,
  };
  public static isOnRight: SpatialRelationDef = {
    id: 'is-on-right',
    targetSurfaceId: Surface.outerRight,
    prepositionActionResource: DefaultResDict.SPATIAL_ACTION_RIGHT,
    prepositionDescriptionResource: DefaultResDict.SPATIAL_DESCRIPTION_RIGHT,
  };
  public static isOnRightInner: SpatialRelationDef = {
    id: 'is-on-right-inner',
    targetSurfaceId: Surface.innerRight,
    prepositionActionResource: DefaultResDict.SPATIAL_ACTION_RIGHT,
    prepositionDescriptionResource: DefaultResDict.SPATIAL_DESCRIPTION_RIGHT,
  };

  private static _all: Array<SpatialRelationDef>;
  public static all(): Array<SpatialRelationDef> {
    SpatialPrepositions._all = SpatialPrepositions._all ?? [
      SpatialPrepositions.isIn,
      SpatialPrepositions.isOn,
      SpatialPrepositions.isOnInner,
      SpatialPrepositions.isUnder,
      SpatialPrepositions.isBelow,
      SpatialPrepositions.isInFront,
      SpatialPrepositions.isInFrontInner,
      SpatialPrepositions.isInBack,
      SpatialPrepositions.isBehind,
      SpatialPrepositions.isOnLeft,
      SpatialPrepositions.isOnLeftInner,
      SpatialPrepositions.isOnRight,
      SpatialPrepositions.isOnRightInner,
    ];
    return SpatialPrepositions._all;
  }

  private static _allById: Map<string, SpatialRelationDef>;
  public static allById(): Map<string, SpatialRelationDef> {
    SpatialPrepositions._allById = SpatialPrepositions._allById ?? new Map<string, SpatialRelationDef>(SpatialPrepositions.all().map<[string, SpatialRelationDef]>(p => [p.id, p]));
    return SpatialPrepositions._allById;
  }


  private static _allBySurface: Map<string, SpatialRelationDef>;
  public static allBySurface(): Map<string, SpatialRelationDef> {
    SpatialPrepositions._allBySurface = SpatialPrepositions._allBySurface ?? new Map<string, SpatialRelationDef>(SpatialPrepositions.all().map<[string, SpatialRelationDef]>(p => [p.targetSurfaceId, p]));
    return SpatialPrepositions._allBySurface;
   }
}
