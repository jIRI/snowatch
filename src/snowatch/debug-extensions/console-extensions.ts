import {autoinject} from 'aurelia-framework';

import {GameData} from 'snowatch/game-data';
import {broadcastId, messageId} from 'snowatch/message';
import {Attribute} from 'snowatch/attribute';
import {DebugExtension} from 'snowatch/debug-extensions/debug-extension';

@autoinject()
export class ConsoleExtensions extends DebugExtension {
  game: GameData;

  register(game: GameData) {
    this.game = game;

    (<any>window)['getAttr'] = (entityId: string, attrId: string) => this.getAttr(entityId, attrId);
    (<any>window)['setAttr'] = (entityId: string, attrId: string, value: any) => this.setAttr(entityId, attrId, value);
    (<any>window)['broadcast'] = (channel: string, id: string, payload?: any, responseTo?: number) => this.broadcast(channel, id, payload, responseTo);

    // register ids
    const ids = {};
    // entities ids
    for(let kind of Object.getOwnPropertyNames(this.game.instances)) {
      const kindIds = {};
      for(let key of this.game.instances[kind].keys()) {
        (<any>kindIds)[`${key}`] = key;
      }
      (<any>ids)[kind] = kindIds;
    }
    // attributes
    const attrs = {};
    for(let key of Attribute.attrRegistry.keys()) {
      (<any>attrs)[`${key}`] = key;
    }
    (<any>ids)['attrs'] = attrs;
    // register
    (<any>window)['$ids'] = ids;
  }

  getAttr(entityId: string, attrId: string): any {
    const entity = this.game.instances.getAnyFromId(entityId);
    if( entity != null ) {
      console.log(`Getting attribute '${attrId}' of '${entityId}'`);
      return Attribute.value(entity, attrId);
    } else {
      console.log(`No entity found with ID '${entityId}'`);
    }
  }

  setAttr(entityId: string, attrId: string, value: any) {
    const entity = this.game.instances.getEntityFromId(entityId);
    if( entity != null ) {
      console.log(`Setting attribute '${attrId}' of '${entityId}' to '${value}'`);
      Attribute.setValue(entity, attrId, value);
    } else {
      console.log(`No entity found with ID '${entityId}'`);
    }
  }

  broadcast(channel: string, id: string, payload?: any, responseTo?: number) {
    (<any>this.game.bus)[channel].publish(broadcastId(id, payload, responseTo));
  }
}