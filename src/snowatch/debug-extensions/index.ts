import {FrameworkConfiguration} from 'aurelia-framework';

import {DebugExtension} from 'snowatch/debug-extensions/debug-extension';
import {ConsoleExtensions} from 'snowatch/debug-extensions/console-extensions';

export function configure(aurelia: FrameworkConfiguration) {
  aurelia.singleton(DebugExtension, ConsoleExtensions);
}