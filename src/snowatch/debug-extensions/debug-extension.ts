import {autoinject} from 'aurelia-framework';

import {GameData} from 'snowatch/game-data';
import {Attribute} from 'snowatch/attribute';

@autoinject()
export abstract class DebugExtension {
  abstract register(game: GameData): void;
}