export * from 'snowatch/attribute';
import {Attribute} from 'snowatch/attribute';
import {Vector3, Zero3} from 'snowatch/spatial-relations-base-types';
import {Surface} from 'snowatch/surface';
import {SubstancePhases} from 'snowatch/substance-base-types';

export const UndefinedStringAttrValue = '<undefined>';

function makeValue<T>(value: T) {
  return { value: value };
}

export const HasSpatialRelations = 'HasSpatialRelations';
Attribute.register<boolean>(HasSpatialRelations, makeValue(true));

export const CanLookAround = 'CanLookAround';
Attribute.register<boolean>(CanLookAround, makeValue(true));
export const CanListen = 'CanListen';
Attribute.register<boolean>(CanListen, makeValue(true));
export const CanHear = 'CanHear';
Attribute.register<boolean>(CanHear, makeValue(true));
export const CanSee = 'CanSee';
Attribute.register<boolean>(CanSee, makeValue(true));
export const CanSmell = 'CanSmell';
Attribute.register<boolean>(CanSmell, makeValue(true));
export const CanBeIndirect = 'CanBeIndirect';
Attribute.register<boolean>(CanBeIndirect, makeValue(true));
export const CanBeInstrument = 'CanBeInstrument';
Attribute.register<boolean>(CanBeInstrument, makeValue(true));
export const CanMove = 'CanMove';
Attribute.register<boolean>(CanMove, makeValue(true));
export const CanUseSkills = 'CanUseSkills';
Attribute.register<boolean>(CanUseSkills, makeValue(true));
export const CanSeeInventory = 'CanSeeInventory';
Attribute.register<boolean>(CanSeeInventory, makeValue(true));
export const CanHoldLiquid = 'CanHoldLiquid';
Attribute.register<boolean>(CanHoldLiquid, makeValue(true));

export const CanEngage = 'CanEngage';
Attribute.register<boolean>(CanEngage, makeValue(true));
export const CanAttack = 'CanAttack';
Attribute.register<boolean>(CanAttack, makeValue(true));
export const CanBlock = 'CanBlock';
Attribute.register<boolean>(CanBlock, makeValue(true));
export const CanDodge = 'CanDodge';
Attribute.register<boolean>(CanDodge, makeValue(true));
export const CanEvade = 'CanEvade';
Attribute.register<boolean>(CanEvade, makeValue(true));
export const CanPursue = 'CanPursue';
Attribute.register<boolean>(CanPursue, makeValue(true));

export const CanTakeDamage = 'CanTakeDamage';
Attribute.register<boolean>(CanTakeDamage, makeValue(true));
export const CanDie = 'CanDie';
Attribute.register<boolean>(CanDie, makeValue(true));
export const IsDead = 'IsDead';
Attribute.register<boolean>(IsDead, makeValue(false));

export const CanStun = 'CanStun';
Attribute.register<boolean>(CanStun, makeValue(true));
export const CanBeStunned = 'CanBeStunned';
Attribute.register<boolean>(CanBeStunned, makeValue(true));
export const IsStunned = 'IsStunned';
Attribute.register<boolean>(IsStunned, makeValue(false));

export const IsInfected = 'IsInfected';
Attribute.register<boolean>(IsInfected, makeValue(true));
export const IsUnconscious = 'IsUnconscious';
Attribute.register<boolean>(IsUnconscious, makeValue(true));

export const CanBeEngaged = 'CanBeEngaged';
Attribute.register<boolean>(CanBeEngaged, makeValue(true));
export const CanBeAttacked = 'CanBeAttacked';
Attribute.register<boolean>(CanBeAttacked, makeValue(true));
export const CanBeBlocked = 'CanBeBlocked';
Attribute.register<boolean>(CanBeBlocked, makeValue(true));
export const CanBeDodged = 'CanBeDodged';
Attribute.register<boolean>(CanBeDodged, makeValue(true));
export const CanBeEvaded = 'CanBeEvaded';
Attribute.register<boolean>(CanBeEvaded, makeValue(true));
export const CanBePursued = 'CanBePursued';
Attribute.register<boolean>(CanBePursued, makeValue(true));

export const IsEngageable = 'IsEngageable';
Attribute.register<boolean>(IsEngageable, makeValue(true));
export const IsAttackable = 'IsAttackable';
Attribute.register<boolean>(IsAttackable, makeValue(true));
export const IsBlockable = 'IsBlockable';
Attribute.register<boolean>(IsBlockable, makeValue(false));
export const IsEvadeable = 'IsEvadeable';
Attribute.register<boolean>(IsEvadeable, makeValue(true));
export const IsPursuable = 'IsPursuable';
Attribute.register<boolean>(IsPursuable, makeValue(true));

export const HasDodgeAvailable = 'HasDodgeAvailable';
Attribute.register<boolean>(HasDodgeAvailable, makeValue(false));

export const IsRealTime = 'IsRealTime';
Attribute.register<boolean>(IsRealTime, makeValue(true));
export const IsContainer = 'IsContainer';
Attribute.register<boolean>(IsContainer, makeValue(true));
export const IsVisible = 'IsVisible';
Attribute.register<boolean>(IsVisible, makeValue(true));
export const IsSmellable = 'IsSmellable';
Attribute.register<boolean>(IsSmellable, makeValue(true));
export const IsOpenable = 'IsOpenable';
Attribute.register<boolean>(IsOpenable, makeValue(true));
export const IsCloseable = 'IsCloseable';
Attribute.register<boolean>(IsCloseable, makeValue(true));
export const IsOpen = 'IsOpen';
Attribute.register<boolean>(IsOpen, makeValue(true));
export const IsClosed = 'IsClosed';
Attribute.register<boolean>(IsClosed, makeValue(true));
export const IsLockable = 'IsLockable';
Attribute.register<boolean>(IsLockable, makeValue(true));
export const IsUnlockable = 'IsUnlockable';
Attribute.register<boolean>(IsUnlockable, makeValue(true));
export const IsLocked = 'IsLocked';
Attribute.register<boolean>(IsLocked, makeValue(true));
export const IsUnlocked = 'IsUnlocked';
Attribute.register<boolean>(IsUnlocked, makeValue(true));
export const IsSplittable = 'IsSplittable';
Attribute.register<boolean>(IsSplittable, makeValue(true));
export const IsJoinable = 'IsJoinable';
Attribute.register<boolean>(IsJoinable, makeValue(true));
export const HasInfiniteAmount = 'HasInfiniteAmount';
Attribute.register<boolean>(HasInfiniteAmount, makeValue(true));

export const IsGivable = 'IsGivable';
Attribute.register<boolean>(IsGivable, makeValue(true));
export const IsLearnable = 'IsLearnable';
Attribute.register<boolean>(IsLearnable, makeValue(true));
export const IsPutable = 'IsPutable';
Attribute.register<boolean>(IsPutable, makeValue(true));
export const IsTakeable = 'IsTakeable';
Attribute.register<boolean>(IsTakeable, makeValue(true));
export const IsTeachable = 'IsTeachable';
Attribute.register<boolean>(IsTeachable, makeValue(true));
export const IsPressable = 'IsPressable';
Attribute.register<boolean>(IsPressable, makeValue(true));
export const IsDrinkable = 'IsDrinkable';
Attribute.register<boolean>(IsDrinkable, makeValue(true));
export const IsPourable = 'IsPourable';
Attribute.register<boolean>(IsPourable, makeValue(true));

export const Title = 'Title';
Attribute.register<string>(Title, makeValue(''));
export const Description = 'Description';
Attribute.register<string>(Description, makeValue(''));
export const NounResourceId = 'NounId';
Attribute.register<string>(NounResourceId, makeValue(UndefinedStringAttrValue));
export const AdjectiveResourceId = 'AdjectiveId';
Attribute.register<string>(AdjectiveResourceId, makeValue(UndefinedStringAttrValue));
export const SpatialSize = 'SpatialSize';
Attribute.register<Vector3>(SpatialSize, makeValue(Zero3));
export const SpatialLocation = 'SpatialLocation';
Attribute.register<Vector3>(SpatialLocation, makeValue(Zero3));
export const ShellThickness = 'ShellThickness';
Attribute.register<Vector3>(ShellThickness, makeValue({x: 0.01, y: 0.01, z: 0.01}));
export const Surfaces = 'Surfaces';
Attribute.register<Array<string>>(Surfaces, makeValue(Surface.none));

export const TriggeredAutoupdateMessages = 'TriggeredUpdateOnMessageBehaviors';
Attribute.register<Set<string>>(TriggeredAutoupdateMessages, makeValue(new Set<string>()));

export const IsStartActor = 'IsStartActor';
Attribute.register<boolean>(IsStartActor, makeValue(true));
export const IsCurrentActor = 'IsCurrentActor';
Attribute.register<boolean>(IsCurrentActor, makeValue(false));
export const StartSceneId = 'StartSceneId';
Attribute.register<string | null>(StartSceneId, makeValue(null));
export const CurrentSceneId = 'CurrentSceneId';
Attribute.register<string | null>(CurrentSceneId, makeValue(null));
export const SceneDefaultSpot = 'SceneDefaultSpot';
Attribute.register<Vector3 | null>(SceneDefaultSpot, makeValue(null));
export const HolderId = 'HolderId';
Attribute.register<string | null>(HolderId, makeValue(null));
export const OwnerId = 'OwnerId';
Attribute.register<string | null>(OwnerId, makeValue(null));
export const ContainerId = 'ContainerId';
Attribute.register<string | null>(ContainerId, makeValue(null));
export const ContainerSupportsDrinking = 'ContainerSupportsDrinking';
Attribute.register<boolean>(ContainerSupportsDrinking, makeValue(false));
export const ContainerSupportsPouring = 'ContainerSupportsPouring';
Attribute.register<boolean>(ContainerSupportsPouring, makeValue(false));
export const SubstanceCompounds = 'SubstanceCompounds';
Attribute.register<Set<string>>(SubstanceCompounds, makeValue(new Set<string>()));

export interface NumericValueAttribute {
  value: number;
  initialValue?: number;
  minValue?: number;
  maxValue?: number;
}
export const DefaultNumericValueAttributeValue = {
  value: 0,
  initialValue: 0,
  minValue: 0,
  maxValue: 1,
};
export const DefaultNumericValueAttributeValueWithoutUpperBound = {
  value: 0,
  initialValue: 0,
  minValue: 0,
};
export enum Potency {
  Singlepotent = "Singlepotent",
  Multipotent = "Multipotent",
}
export const Weight = 'Weight';
Attribute.register<NumericValueAttribute>(Weight, DefaultNumericValueAttributeValue);
export const Strength = 'Strength';
Attribute.register<NumericValueAttribute>(Strength, DefaultNumericValueAttributeValue);
export const Perception = 'Perception';
Attribute.register<NumericValueAttribute>(Perception, DefaultNumericValueAttributeValue);
export const Intelligence = 'Intelligence';
Attribute.register<NumericValueAttribute>(Intelligence, DefaultNumericValueAttributeValue);
export const Constitution = 'Constitution';
Attribute.register<NumericValueAttribute>(Constitution, DefaultNumericValueAttributeValue);
export const Health = 'Health';
Attribute.register<NumericValueAttribute>(Health, { value: 100, initialValue: 100, minValue: 0, });
export const Stamina = 'Stamina';
Attribute.register<NumericValueAttribute>(Stamina, DefaultNumericValueAttributeValue);
export const Agility = 'Agility';
Attribute.register<NumericValueAttribute>(Agility, DefaultNumericValueAttributeValue);
export const Dexterity = 'Dexterity';
Attribute.register<NumericValueAttribute>(Dexterity, DefaultNumericValueAttributeValue);
export const Damage = 'Damage';
Attribute.register<NumericValueAttribute>(Damage, DefaultNumericValueAttributeValue);

export const SubstancePhase = 'SubstancePhase';
Attribute.register<SubstancePhases>(SubstancePhase, SubstancePhases.solid);
export const Volume = 'Volume';
Attribute.register<NumericValueAttribute>(Volume, DefaultNumericValueAttributeValueWithoutUpperBound);
export const ConstantRateOfMelting = 'ConstantRateOfMelting';
Attribute.register<NumericValueAttribute>(ConstantRateOfMelting, { value: 0.1, initialValue: 0.1, minValue: 0, });
export const ConstantRateOfEvaporation = 'ConstantRateOfEvaporation';
Attribute.register<NumericValueAttribute>(ConstantRateOfEvaporation, { value: 0.1, initialValue: 0.1, minValue: 0, });
export const ConstantRateOfDissipation = 'ConstantRateOfDissipation';
Attribute.register<NumericValueAttribute>(ConstantRateOfDissipation, { value: 0.05, initialValue: 0.05, minValue: 0, });
export const Viscosity = 'Viscosity';
Attribute.register<NumericValueAttribute>(Viscosity, DefaultNumericValueAttributeValueWithoutUpperBound);
export const Elasticity = 'Elasticity';
Attribute.register<Vector3>(Elasticity, makeValue(Zero3));
export const Expandability = 'Expandability';
Attribute.register<Vector3>(Expandability, makeValue(Zero3));
export const Compressibility = 'Compressibility';
Attribute.register<Vector3>(Compressibility, makeValue(Zero3));
export const ContainerCapacityUsed = 'ContainerCapacityUsed';
Attribute.register<NumericValueAttribute>(ContainerCapacityUsed, DefaultNumericValueAttributeValueWithoutUpperBound);
export const AvailableSpatialCapacity = 'SpatialCapacityAvailable';
export type SpatialCapacityMap = Map<Surface, NumericValueAttribute>;
export const NewSpatialCapacityValue = (initializer?: Iterable<[Surface, NumericValueAttribute]>) => new Map<Surface, NumericValueAttribute>(initializer ?? []);
Attribute.register<SpatialCapacityMap>(AvailableSpatialCapacity, NewSpatialCapacityValue());

// TODO: should ColorId stuff be moved to game?
export enum ColorIdValues {
  None = 'None',
  Transparent = 'Transparent',
  White = 'White',
  Black = 'Black',
  Red = 'Red',
  Green = 'Green',
  Blue = 'Blue',
  Yellow = 'Yellow',
}
export const ColorId = 'ColorId';
Attribute.register<ColorIdValues>(ColorId, ColorIdValues.None);

export enum GenderIdValues {
  Male = 'Male',
  Female = 'Female',
  Neutral = 'Neutral',
}
export const GenderId = 'GenderId';
Attribute.register<GenderIdValues>(GenderId, GenderIdValues.Male);

export enum ActorModes {
  Normal = 'Normal',
  Engaged = 'Engaged',
  Fighting = 'Fighting',
  Pursuing = 'Pursuing',
  Stunned = 'Stunned',
  Dodging = 'Dodging',
  Blocking = 'Blocking',
}
export const ActorMode = 'ActorMode';
Attribute.register<ActorModes>(ActorMode, ActorModes.Normal);

export enum AttackStages {
  Ready = 'Ready',
  Windup = 'Windup',
  Landed = 'Landed',
  CancelCooldown = 'CancelCooldown',
  Cooldown = 'Cooldown',
  Stunned = 'Stunned',
}
export const AttackStage = 'AttackStage';
Attribute.register<AttackStages>(AttackStage, AttackStages.Ready);
