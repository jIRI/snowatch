import { GameData } from 'snowatch/game-data';
import { Message } from 'snowatch/message';
import { Behavior } from 'snowatch/behavior';
import { Attribute } from 'snowatch/attribute';
import { Surface } from 'snowatch/surface';
import * as behaviors from 'snowatch/behaviors/all';
import * as ent from 'snowatch/entity';
import * as spatial from 'snowatch/spatial-relations';
import * as lang from 'snowatch/language-structures';
import * as attrs from 'snowatch/attributes-ids';
import * as scene from 'snowatch/scene';

export interface ItemInit extends ent.EntityInit {
  title?: string;
  description?: string;
  nounId?: string;
  startSceneId?: string;
  isVisible?: boolean;
  surfaces?: Array<string>;
  spatialLocation?: spatial.Vector3;
  spatialSize?: spatial.Vector3;
  containerId?: string;
}

export class Item extends ent.Entity {
  static ensureIsInstanceOf(obj: Item | ItemInit): Item {
   if( obj instanceof Item ) {
     return obj;
   }
   return new Item(obj);
  }

  public static OBJECT_GROUP = 'items';
  static registrator(game: GameData, o: Item | ItemInit): Item {
    const obj = Item.ensureIsInstanceOf(o);
    return obj.initialize(game);
  }

  init: ItemInit;

  constructor(init: ItemInit | null = null) {
    super(init, Item.OBJECT_GROUP);
  }

  initialize(game: GameData, init?: ItemInit): ent.Entity {
    super.initialize(game, init);

    this.registerInitializer(() => {
      Attribute.initializeAttributes(this);
      Attribute.addFromProperties(this, this.properties);
      Attribute.addConstFromProperties(this, this.properties);
      Attribute.addCompFromProperties(this, this.properties);
      // add default attrs (is ignored if already defined in properties as state, const or computed attr)
      Attribute.add(this, attrs.NounResourceId, { initialState: {value: this.init.nounId ?? this.init.id}});
      Attribute.addComp(this, attrs.Title, { compState: {
        value: (entity, attributeId, actor?: ent.Entity) => {
          return lang.capitalFirst(ent.getTitle(this).title);
        }
      }});
      Attribute.add(this, attrs.Description, { initialState: {value: this.init.description ?? "..."}});
      Attribute.add(this, attrs.IsVisible, { initialState: {value: this.init.isVisible != null ? this.init.isVisible : true}});
      Attribute.add(this, attrs.HolderId, { initialState: {value: this.init.holderId ?? null}});
      Attribute.add(this, attrs.OwnerId, { initialState: {value: this.init.ownerId ?? null}});
      Attribute.add(this, attrs.ContainerId, { initialState: {value: this.init.containerId ?? null}});
      Attribute.add(this, attrs.StartSceneId, { initialState: {value: this.init.startSceneId ?? null}});
      Attribute.add(this, attrs.CurrentSceneId, { initialState: {value: null}});
      Attribute.add(this, attrs.SpatialSize, { initialState: {value: this.init.spatialSize ?? {x: 1, y: 1, z: 1}}});
      Attribute.add(this, attrs.SpatialLocation, { initialState: {value: this.init.spatialLocation ?? {x: 0, y: 0, z: 0}}});
      Attribute.add(this, attrs.Surfaces, { initialState: {value: this.init.surfaces ?? Surface.none}});
      spatial.SpatialRelations.addFromProperties(this, this.properties);
    });

    this.registerInitializer(() => {
      Behavior.initializeBehaviors(this);

      Behavior.add(this, behaviors.SupportsContainer.ID, { initialState: behaviors.createEmptyBehaviorState() });
      Behavior.add(this, behaviors.SupportsInventory.ID, { initialState: behaviors.createEmptyBehaviorState() });

      if( this.properties.behaviors != null ) {
        ent.registerEntities(this.game, Behavior.OBJECT_GROUP, Array.from(this.properties.behaviors.values()), Behavior.registrator);
        Behavior.addFromProperties(this, this.properties);

        for(let [behaviorId, behavior] of this.properties.behaviors) {
          Behavior.registerForActivation(this, behaviorId, behavior.initialState);
        }
      }
    });

    return this;
  }

  onStart(message: Message) {
    super.onStart(message);
    const startSceneId = scene.getStartSceneId(this);
    // we check here for the case that current scene was changed along the way during initialization phase
    if( startSceneId != null && scene.getCurrentSceneId(this) == null ) {
      scene.setSceneId(this, startSceneId);
    }
  }
}
