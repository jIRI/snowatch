import {FrameworkConfiguration} from 'aurelia-framework';

import {Entity} from 'snowatch/snowatch';
import {Topic} from 'snowatch/message-topic';
import {GameSettings, GlobalGameState} from 'snowatch/game-data';

import {CurrentGameSettings} from 'game/current-game/settings';

import * as $current from 'game/current-game/all';

export function configure(aurelia: FrameworkConfiguration) {
  aurelia.singleton(GameSettings, CurrentGameSettings);
  aurelia.singleton(GlobalGameState, $current.CurrentGlobalGameState);
  aurelia.singleton(Entity, $current.GlobalBehaviorsHolder);

  aurelia.singleton(Topic, $current.CustomTopics.GO_SLEEP);
  aurelia.singleton(Topic, $current.CustomTopics.ACTIVATE_BEAMS);
}
