import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

import * as $scenes from 'game/scenes/all';
import * as $behaviors from 'game/behaviors/all';
import * as $items from 'game/items/all';
import * as $gameres from 'game/resources/game-res';

export function getGameSettings(game: snowatch.GameData): CurrentGameSettings {
  return <CurrentGameSettings>game.state.settings;
}

export class CurrentGameSettings extends snowatch.GameSettings {
  currentCulture = 'cz';
}

