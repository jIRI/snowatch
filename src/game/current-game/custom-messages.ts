import {Topic} from 'snowatch/message-topic';

export namespace CustomTopics {
  export class GO_SLEEP extends Topic {
      public static readonly ID = 'GO_SLEEP';

      public constructor(init?: Partial<GO_SLEEP>) {
        super();
        Object.assign(this, init);
      }
  }

  export class ACTIVATE_BEAMS extends Topic {
      public static readonly ID = 'ACTIVATE_BEAMS';

      public constructor(init?: Partial<ACTIVATE_BEAMS>) {
        super();
        Object.assign(this, init);
      }
  }
}
