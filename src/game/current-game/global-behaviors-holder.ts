import * as snowatch from 'snowatch/snowatch';
import * as $behaviors from 'game/behaviors/all';

export class GlobalBehaviorsHolder extends snowatch.Entity {
  public static readonly ID = 'GlobalBehaviorsHolder';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Entity {
    super.initialize(game, {
      properties: {
        attributes: [
        ],
        behaviors: [
          { id: $behaviors.TimeSourceUnpauseOnInput.ID },
        ]
      }
    });

    this.registerInitializer(() => {
      snowatch.Behavior.initializeBehaviors(this);
      if( this.properties.behaviors != null ) {
        snowatch.registerEntities(this.game, snowatch.Behavior.OBJECT_GROUP, Array.from(this.properties.behaviors.values()), snowatch.Behavior.registrator);
        snowatch.Behavior.addFromProperties(this, this.properties);
        for(let [behaviorId, behavior] of this.properties.behaviors) {
          snowatch.Behavior.registerForActivation(this, behaviorId, behavior.initialState);
        }
      }
    });
    return this;
  }
}