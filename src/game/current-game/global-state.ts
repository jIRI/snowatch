import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

import * as $scenes from 'game/scenes/all';
import * as $behaviors from 'game/behaviors/all';
import * as $items from 'game/items/all';
import * as $gameres from 'game/resources/game-res';

export function global(game: snowatch.GameData): CurrentGlobalGameState {
  return <CurrentGlobalGameState>game.state.global;
}

export class CurrentGlobalGameState extends snowatch.GlobalGameState {
  hubDescriptionLevel = 0;
}
