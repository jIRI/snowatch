import {Attribute} from 'snowatch/attribute';
import {UndefinedStringAttrValue, Potency} from 'snowatch/attributes-ids';

export const IsBeamActive = 'IsBeamActive';
Attribute.register<boolean>(IsBeamActive, false);

export const IsSubstanceActive = 'IsSubstanceActive';
Attribute.register<boolean>(IsSubstanceActive, false);

export const TeleportTargetSceneId = 'TeleportTargetSceneId';
Attribute.register<string>(TeleportTargetSceneId, UndefinedStringAttrValue);

export const TeleportTargetForTheFirstTimeSceneId = 'TeleportTargetForTheFirstTimeSceneId';
Attribute.register<string>(TeleportTargetForTheFirstTimeSceneId, UndefinedStringAttrValue);

export const EssenceLiquidId = 'EssenceLiquidId';
Attribute.register<string>(EssenceLiquidId, UndefinedStringAttrValue);

export const RemoveCurrentlyActiveEssenceLiquidEffect = 'RemoveCurrentlyActiveEssenceLiquidEffect';
Attribute.register<boolean>(RemoveCurrentlyActiveEssenceLiquidEffect, true);

export const AttributeToUpdateOnLiquidDrinking = 'AttributeToUpdateOnLiquidDrinking';
export interface AttributeUpdateOnLiquidDrinkingValueType {
  id: string;
  amount: number;
  potency: Potency;
}
Attribute.register<AttributeUpdateOnLiquidDrinkingValueType | null>(AttributeToUpdateOnLiquidDrinking, null);

export const CurrentlyActiveEssenceLiquid = 'CurrentlyActiveEssenceLiquid';
export interface CurrentlyActiveEssenceLiquidValueType {
  updateAttribId: string;
  substanceId: string;
  amount: number;
}
Attribute.register<CurrentlyActiveEssenceLiquidValueType | null>(CurrentlyActiveEssenceLiquid, null);
