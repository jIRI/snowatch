import * as snowatch from 'snowatch/snowatch';
import * as behaviors from 'snowatch/behaviors/all';
import * as attrs from 'snowatch/attributes/all';

import * as $scenes from 'game/scenes/all';
import * as $items from 'game/items/all';
import * as $behaviors from 'game/behaviors/all';
import * as $gameres from 'game/resources/game-res';

export class Sword extends snowatch.Item {
  public static readonly ID = 'Sword';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData) {
    super.initialize(
      game,
      {
        title: 'Sword',
        startSceneId: $scenes.HubChancel.ID,
        holderId: null,
        ownerId: null,
        properties: {
          attributes: [
          ],
          compAttributes: [
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const resource = snowatch.ResourceDictionary.get(this.game, $gameres.EntityResDict.SWORD_DESC);
                return snowatch.compose(resource.template);
              }
            }},
          ],
          constAttributes: [
            { id: attrs.NounResourceId, constState: { value: $gameres.EntityResDict.SWORD_NOUN } },
            { id: attrs.Weight, constState: { value: { value: 5 } as attrs.NumericValueAttribute } },
            { id: attrs.Agility, constState: { value: { value: 2 } as attrs.NumericValueAttribute } },
            { id: attrs.Damage, constState: { value: { value: 30 } as attrs.NumericValueAttribute } },
            { id: attrs.HasSpatialRelations },
            { id: attrs.SpatialSize, constState: { value: {x: 0.1, y: 0.05, z: 1.5}} },
            { id: attrs.CanBeInstrument },
            { id: attrs.CanStun },
          ],
          behaviors: [
            { id: behaviors.SupportsTaking.ID },
            { id: behaviors.SupportsPutting.ID },
            { id: behaviors.SupportsGiving.ID },
            { id: $behaviors.ResetItemOnSleep.ID },
          ],
          spatialRelations: [
            { typeId: snowatch.SpatialPrepositions.isOn.id, targetId: $items.Altar.ID },
          ],
        },
      }
    );
    return this;
  }
}
