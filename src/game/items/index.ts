import {FrameworkConfiguration} from 'aurelia-framework';

import * as items from 'game/items/all';

export function configure(aurelia: FrameworkConfiguration) {
  aurelia.singleton(items.Item, items.Crystal);
  aurelia.singleton(items.Item, items.Sword);
  aurelia.singleton(items.Item, items.Altar);
  aurelia.singleton(items.Item, items.BowlShelf);

  aurelia.singleton(items.Item, items.ClearEssenceBowl);
  aurelia.singleton(items.Item, items.WhiteEssenceBowl);
  aurelia.singleton(items.Item, items.RedEssenceBowl);
  aurelia.singleton(items.Item, items.BlueEssenceBowl);
  aurelia.singleton(items.Item, items.GreenEssenceBowl);
  aurelia.singleton(items.Item, items.YellowEssenceBowl);
  aurelia.singleton(items.Item, items.BlackEssenceBowl);

  aurelia.singleton(items.Item, items.ClearBowlPillar);
  aurelia.singleton(items.Item, items.WhiteBowlPillar);
  aurelia.singleton(items.Item, items.RedBowlPillar);
  aurelia.singleton(items.Item, items.BlueBowlPillar);
  aurelia.singleton(items.Item, items.GreenBowlPillar);
  aurelia.singleton(items.Item, items.YellowBowlPillar);
  aurelia.singleton(items.Item, items.BlackBowlPillar);

  aurelia.singleton(items.Item, items.WhiteBeamOfLight);
  aurelia.singleton(items.Item, items.RedBeamOfLight);
  aurelia.singleton(items.Item, items.BlueBeamOfLight);
  aurelia.singleton(items.Item, items.GreenBeamOfLight);
  aurelia.singleton(items.Item, items.YellowBeamOfLight);
  aurelia.singleton(items.Item, items.BlackBeamOfLight);

  // white
  aurelia.singleton(items.Item, items.AerosolDispenserLeft);
  aurelia.singleton(items.Item, items.AerosolDispenserRight);
  aurelia.singleton(items.Item, items.Bottle);
}
