import * as snowatch from 'snowatch/snowatch';
import * as behaviors from 'snowatch/behaviors/all';
import * as attrs from 'snowatch/attributes/all';

import * as $current from 'game/current-game/all';
import * as $scenes from 'game/scenes/all';
import * as $gameres from 'game/resources/game-res';

const CommonPillarConstAttributes = [
  { id: attrs.HasSpatialRelations },
  { id: attrs.CanBeIndirect },
  { id: attrs.Surfaces, constState: { value: [snowatch.Surface.outerTop]}, },
  { id: attrs.SpatialSize, constState: { value: {x: 0.3, y: 0.3, z: 1}} },
];

const CommonPillarCompAttributes = [
  { id: attrs.AvailableSpatialCapacity, compState: attrs.SpatialCapacityComputedAttribute.default },
];

const CommonPillarBehaviors = [
  { id: behaviors.SupportsPutting.ID },
];

export class ClearBowlPillar extends snowatch.Item {
  public static readonly ID = 'ClearBowlPillar';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Entity {
    super.initialize(
      game,
      {
        title: "Clear Bowl Pillar",
        startSceneId: $scenes.HubChancel.ID,
        properties: {
          constAttributes: [
            { id: attrs.HasSpatialRelations },
            { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.HubChancel.SpatialLocation, {x: 0, y: -3, z: 0})} },
            { id: attrs.SpatialSize, constState: { value: {x: 0.3, y: 0.3, z: 1}} },
          ],
          attributes: [
          ],
          compAttributes: [
            ...CommonPillarCompAttributes,
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.CLEAR_BOWL_PILLAR_NOUN;
                },
            }},
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => getPillarDescription(this.game, $gameres.EntityResDict.CLEAR_BOWL_PILLAR_DESC),
            }},
          ],
          behaviors: [
          ],
        },
      }
    );

    return this;
  }
}

export class WhiteBowlPillar extends snowatch.Item {
  public static readonly ID = 'WhiteBowlPillar';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Entity {
    super.initialize(
      game,
      {
        title: "White Bowl Pillar",
        startSceneId: $scenes.HubChancel.ID,
        properties: {
          constAttributes: [
            ...CommonPillarConstAttributes,
            { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.HubChancel.SpatialLocation, {x: -1.5, y: -1.5, z: 0})} },
          ],
          attributes: [
          ],
          compAttributes: [
            ...CommonPillarCompAttributes,
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.WHITE_BOWL_PILLAR_NOUN;
                },
            }},
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) =>  getPillarDescription(this.game, $gameres.EntityResDict.WHITE_BOWL_PILLAR_DESC),
            }},
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonPillarBehaviors),
          ],
          spatialRelations: [
          ],
        },
      }
    );

    return this;
  }
}

export class RedBowlPillar extends snowatch.Item {
  public static readonly ID = 'RedBowlPillar';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Entity {
    super.initialize(
      game,
      {
        title: "Red Bowl Pillar",
        startSceneId: $scenes.HubChancel.ID,
        properties: {
          constAttributes: [
            ...CommonPillarConstAttributes,
            {id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.HubChancel.SpatialLocation, {x: 1.5, y: 0, z: 0})}},
          ],
          compAttributes: [
            ...CommonPillarCompAttributes,
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.RED_BOWL_PILLAR_NOUN;
                },
            }},
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => getPillarDescription(this.game, $gameres.EntityResDict.RED_BOWL_PILLAR_DESC),
            }},
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonPillarBehaviors),
          ],
          spatialRelations: [
          ],
        },
      }
    );

    return this;
  }
}

export class BlueBowlPillar extends snowatch.Item {
  public static readonly ID = 'BlueBowlPillar';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Entity {
    super.initialize(
      game,
      {
        title: "Blue Bowl Pillar",
        startSceneId: $scenes.HubChancel.ID,
        properties: {
          constAttributes: [
            ...CommonPillarConstAttributes,
            {id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.HubChancel.SpatialLocation, {x: 1.5, y: 1.5, z: 0})}},
          ],
          compAttributes: [
            ...CommonPillarCompAttributes,
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.BLUE_BOWL_PILLAR_NOUN;
                },
            }},
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => getPillarDescription(this.game, $gameres.EntityResDict.BLUE_BOWL_PILLAR_DESC),
            }},
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonPillarBehaviors),
          ],
          spatialRelations: [
          ],
        },
      }
    );

    return this;
  }
}

export class GreenBowlPillar extends snowatch.Item {
  public static readonly ID = 'GreenBowlPillar';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Entity {
    super.initialize(
      game,
      {
        title: "Green Bowl Pillar",
        startSceneId: $scenes.HubChancel.ID,
        properties: {
          constAttributes: [
            ...CommonPillarConstAttributes,
            {id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.HubChancel.SpatialLocation, {x: -1.5, y: 0, z: 0})}},
          ],
          compAttributes: [
            ...CommonPillarCompAttributes,
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.GREEN_BOWL_PILLAR_NOUN;
                },
            }},
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => getPillarDescription(this.game, $gameres.EntityResDict.GREEN_BOWL_PILLAR_DESC),
            }},
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonPillarBehaviors),
          ],
          spatialRelations: [
          ],
        },
      }
    );

    return this;
  }
}

export class YellowBowlPillar extends snowatch.Item {
  public static readonly ID = 'YellowBowlPillar';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Entity {
    super.initialize(
      game,
      {
        title: "Yellow Bowl Pillar",
        startSceneId: $scenes.HubChancel.ID,
        properties: {
          constAttributes: [
            ...CommonPillarConstAttributes,
            {id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.HubChancel.SpatialLocation, {x: -1.5, y: 1.5, z: 0})}},
          ],
          compAttributes: [
            ...CommonPillarCompAttributes,
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.YELLOW_BOWL_PILLAR_NOUN;
                },
            }},
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => getPillarDescription(this.game, $gameres.EntityResDict.YELLOW_BOWL_PILLAR_DESC),
            }},
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonPillarBehaviors),
          ],
          spatialRelations: [
          ],
        },
      }
    );

    return this;
  }
}

export class BlackBowlPillar extends snowatch.Item {
  public static readonly ID = 'BlackBowlPillar';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Entity {
    super.initialize(
      game,
      {
        title: "Black Bowl Pillar",
        startSceneId: $scenes.HubChancel.ID,
        properties: {
          constAttributes: [
            ...CommonPillarConstAttributes,
            {id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.HubChancel.SpatialLocation, {x: 1.5, y: 1.5, z: 0})}},
          ],
          compAttributes: [
            ...CommonPillarCompAttributes,
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.BLACK_BOWL_PILLAR_NOUN;
                },
            }},
            { id: attrs.Description,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => getPillarDescription(this.game, $gameres.EntityResDict.BLACK_BOWL_PILLAR_DESC),
              }},
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonPillarBehaviors),
          ],
          spatialRelations: [
          ],
        },
      }
    );

    return this;
  }
}

function getPillarDescription(game: snowatch.GameData, resourceId: string) {
  const resource = snowatch.ResourceDictionary.get(game, resourceId);
  return snowatch.compose(resource.template[$current.global(game).hubDescriptionLevel]);
}

