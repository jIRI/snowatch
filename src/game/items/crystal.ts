import * as snowatch from 'snowatch/snowatch';
import * as skills from 'snowatch/skills/all';
import * as attrs from 'snowatch/attributes/all';

import * as $current from 'game/current-game/all';
import * as $behaviors from 'game/behaviors/all';
import * as $actors from 'game/actors/all';
import * as $scenes from 'game/scenes/all';
import * as $items from 'game/items/all';
import * as $gameres from 'game/resources/game-res';

export class Crystal extends snowatch.Item {
  public static readonly ID = 'Crystal';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData) {
    const self = this;
    super.initialize(
      game,
      {
        title: 'Crystal',
        holderId: $actors.LastOne.ID,
        ownerId: $actors.LastOne.ID,
        properties: {
          attributes: [
            { id: attrs.ColorId, initialState: { value: attrs.ColorIdValues.None } },
          ],
          compAttributes: [
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                if( actor == null ) {
                  return null;
                }

                const resource = snowatch.ResourceDictionary.get(this.game, $gameres.EntityResDict.CRYSTAL_DESC);
                const colorId = attrs.Attribute.value<attrs.ColorIdValues>(entity, attrs.ColorId, actor);
                const actorNoun = snowatch.Noun.get(this.game, $gameres.ResourceDictionary.get(this.game, attrs.Attribute.value<string>(actor, attrs.NounResourceId, actor)));
                return snowatch.compose(resource.template, {
                  actorNoun: actorNoun.noun(snowatch.Case.dative),
                  coloring: Crystal.pickColoring(colorId, resource),
                });
              }
            }},
          ],
          constAttributes: [
            { id: attrs.NounResourceId, constState: { value: $gameres.EntityResDict.CRYSTAL_NOUN } },
            { id: attrs.Weight },
            { id: attrs.HasSpatialRelations },
            { id: attrs.IsPressable },
          ],
          behaviors: [
            { id: $behaviors.ColorOnDrinkingLiquid.ID },
          ],
        },
      }
    );
    return this;
  }

  static pickColoring(coloring: attrs.ColorIdValues, resource: any): string {
    switch( coloring ) {
      case attrs.ColorIdValues.White:
        return resource.coloringWhite;
      case attrs.ColorIdValues.Red:
        return resource.coloringRed;
      case attrs.ColorIdValues.Green:
        return resource.coloringGreen;
      case attrs.ColorIdValues.Yellow:
        return resource.coloringYellow;
      case attrs.ColorIdValues.Blue:
        return resource.coloringBlue;
      case attrs.ColorIdValues.Black:
        return resource.coloringBlack;
      default:
        return resource.coloringClear;
    }
  }
}
