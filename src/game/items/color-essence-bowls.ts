import * as snowatch from 'snowatch/snowatch';
import * as behaviors from 'snowatch/behaviors/all';
import * as attrs from 'snowatch/attributes/all';

import * as $current from 'game/current-game/all';
import * as $attrs from 'game/current-game/attributes';
import * as $actors from 'game/actors/all';
import * as $items from 'game/items/all';
import * as $scenes from 'game/scenes/all';
import * as $behaviors from 'game/behaviors/all';
import * as $gameres from 'game/resources/game-res';

const CommonMoveableBowlAttributes = [
  { id: attrs.Surfaces, initialState: { value: [snowatch.Surface.innerBottom, snowatch.Surface.outerBottom]}, },
];

const CommonBowlConstAttributes = [
  { id: attrs.IsContainer },
  { id: attrs.HasSpatialRelations },
  { id: attrs.CanBeIndirect },
  { id: attrs.ContainerSupportsDrinking, constState: { value: true } },
  { id: attrs.CanBeIndirect },
  { id: attrs.SpatialSize, constState: { value: {x: 0.3, y: 0.3, z: 0.3}} },
];

const CommonBowlBehaviors = [
  { id: behaviors.SupportsPutting.ID },
  { id: behaviors.SupportsTaking.ID },
  // once light beams are activated by putting all bowls on proper pillars, remove takeability and puttability of bowls
  { id: behaviors.UpdatePropertiesOnMessage.ID,
    initialState: <behaviors.UpdatePropertiesOnMessageState>{
      topicId: snowatch.Topic.id($current.CustomTopics.ACTIVATE_BEAMS),
      remove: [
        { attributeId: attrs.IsPutable },
        { attributeId: attrs.IsTakeable },
        { attributeId: attrs.Surfaces },
      ],
      add: [
        // attributes are first removed, then added, so we can update the surfaces here...
        { attributeId: attrs.Surfaces, attrStateInit: { initialState: { value: [snowatch.Surface.innerBottom]}}, },
      ],
    }
  },
  { id: behaviors.DisableBehaviorsOnMessage.ID, initialState: {
    topicId: snowatch.Topic.id($current.CustomTopics.ACTIVATE_BEAMS),
    behaviorIds: [
      behaviors.SupportsPutting.ID, behaviors.SupportsTaking.ID
    ]} as behaviors.DisableBehaviorsOnMessageState
  },
];


export class ClearEssenceBowl extends snowatch.Item {
  public static readonly ID = 'ClearEssenceBowl';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Entity {
    super.initialize(
      game,
      {
        title: "Clear Essence Bowl",
        startSceneId: $scenes.HubChancel.ID,
        properties: {
          constAttributes: [
            { id: attrs.Surfaces, constState: { value: [snowatch.Surface.innerBottom, ] }, },
          ],
          attributes: [
            ...CommonMoveableBowlAttributes,
          ],
          compAttributes: [
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.CLEAR_ESSENCE_BOWL_NOUN;
                },
            }},
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const resource = snowatch.ResourceDictionary.get(this.game, $gameres.EntityResDict.CLEAR_ESSENCE_BOWL_DESC);
                return snowatch.compose(resource.template);
              }
            }},
          ],
          behaviors: [
          ],
          spatialRelations: [
            { typeId: snowatch.SpatialPrepositions.isOn.id, targetId: $items.ClearBowlPillar.ID },
          ],
        },
      }
    );

    // initialization
    this.registerInitializer(() => {
      snowatch.Container.initializeContainer(this);
    });
    return this;
  }
}


export class WhiteEssenceBowl extends snowatch.Item {
  public static readonly ID = 'WhiteEssenceBowl';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Entity {
    super.initialize(
      game,
      {
        title: "White Essence Bowl",
        startSceneId: $scenes.HubChancel.ID,
        properties: {
          constAttributes: [
            ...CommonBowlConstAttributes,
          ],
          attributes: [
            ...CommonMoveableBowlAttributes,
          ],
          compAttributes: [
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.WHITE_ESSENCE_BOWL_NOUN;
                },
            }},
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const resource = snowatch.ResourceDictionary.get(this.game, $gameres.EntityResDict.WHITE_ESSENCE_BOWL_DESC);
                return snowatch.compose(resource.template);
              }
            }},
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBowlBehaviors),
          ],
          spatialRelations: [
            { typeId: snowatch.SpatialPrepositions.isOn.id, targetId: $items.BowlShelf.ID },
          ],
        },
      }
    );

    // initialization
    this.registerInitializer(() => {
      snowatch.Container.initializeContainer(this);
    });
    return this;
  }
}

export class RedEssenceBowl extends snowatch.Item {
  public static readonly ID = 'RedEssenceBowl';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Entity {
    super.initialize(
      game,
      {
        title: "Red Essence Bowl",
        startSceneId: $scenes.HubChancel.ID,
        properties: {
          constAttributes: [
            ...CommonBowlConstAttributes,
          ],
          attributes: [
            ...CommonMoveableBowlAttributes,
          ],
          compAttributes: [
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.RED_ESSENCE_BOWL_NOUN;
                },
            }},
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const resource = snowatch.ResourceDictionary.get(this.game, $gameres.EntityResDict.RED_ESSENCE_BOWL_DESC);
                return snowatch.compose(resource.template);
              }
            }},
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBowlBehaviors),
          ],
          spatialRelations: [
            { typeId: snowatch.SpatialPrepositions.isOn.id, targetId: $items.BowlShelf.ID },
          ],
        },
      }
    );

    // initialization
    this.registerInitializer(() => {
      snowatch.Container.initializeContainer(this);
    });
    return this;
  }
}

export class BlueEssenceBowl extends snowatch.Item {
  public static readonly ID = 'BlueEssenceBowl';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Entity {
    super.initialize(
      game,
      {
        title: "Blue Essence Bowl",
        startSceneId: $scenes.HubChancel.ID,
        properties: {
          constAttributes: [
            ...CommonBowlConstAttributes,
          ],
          attributes: [
            ...CommonMoveableBowlAttributes,
          ],
          compAttributes: [
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.BLUE_ESSENCE_BOWL_NOUN;
                },
            }},
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const resource = snowatch.ResourceDictionary.get(this.game, $gameres.EntityResDict.BLUE_ESSENCE_BOWL_DESC);
                return snowatch.compose(resource.template);
              }
            }},
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBowlBehaviors),
          ],
          spatialRelations: [
            { typeId: snowatch.SpatialPrepositions.isOn.id, targetId: $items.BowlShelf.ID },
          ],
        },
      }
    );

    // initialization
    this.registerInitializer(() => {
      snowatch.Container.initializeContainer(this);
    });
    return this;
  }
}

export class GreenEssenceBowl extends snowatch.Item {
  public static readonly ID = 'GreenEssenceBowl';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Entity {
    super.initialize(
      game,
      {
        title: "Green Essence Bowl",
        startSceneId: $scenes.HubChancel.ID,
        properties: {
          constAttributes: [
            ...CommonBowlConstAttributes,
          ],
          attributes: [
            ...CommonMoveableBowlAttributes,
          ],
          compAttributes: [
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.GREEN_ESSENCE_BOWL_NOUN;
                },
            }},
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const resource = snowatch.ResourceDictionary.get(this.game, $gameres.EntityResDict.GREEN_ESSENCE_BOWL_DESC);
                return snowatch.compose(resource.template);
              }
            }},
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBowlBehaviors),
          ],
          spatialRelations: [
            { typeId: snowatch.SpatialPrepositions.isOn.id, targetId: $items.BowlShelf.ID },
          ],
        },
      }
    );

    // initialization
    this.registerInitializer(() => {
      snowatch.Container.initializeContainer(this);
    });
    return this;
  }
}

export class YellowEssenceBowl extends snowatch.Item {
  public static readonly ID = 'YellowEssenceBowl';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Entity {
    super.initialize(
      game,
      {
        title: "Yellow Essence Bowl",
        startSceneId: $scenes.HubChancel.ID,
        properties: {
          constAttributes: [
            ...CommonBowlConstAttributes,
          ],
          attributes: [
            ...CommonMoveableBowlAttributes,
          ],
          compAttributes: [
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.YELLOW_ESSENCE_BOWL_NOUN;
                },
            }},
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const resource = snowatch.ResourceDictionary.get(this.game, $gameres.EntityResDict.YELLOW_ESSENCE_BOWL_DESC);
                return snowatch.compose(resource.template);
              }
            }},
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBowlBehaviors),
          ],
          spatialRelations: [
            { typeId: snowatch.SpatialPrepositions.isOn.id, targetId: $items.BowlShelf.ID },
          ],
        },
      }
    );

    // initialization
    this.registerInitializer(() => {
      snowatch.Container.initializeContainer(this);
    });
    return this;
  }
}

export class BlackEssenceBowl extends snowatch.Item {
  public static readonly ID = 'BlackEssenceBowl';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Entity {
    super.initialize(
      game,
      {
        title: "Black Essence Bowl",
        startSceneId: $scenes.HubChancel.ID,
        properties: {
          constAttributes: [
            ...CommonBowlConstAttributes,
          ],
          attributes: [
            ...CommonMoveableBowlAttributes,
          ],
          compAttributes: [
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.BLACK_ESSENCE_BOWL_NOUN;
                },
            }},
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const resource = snowatch.ResourceDictionary.get(this.game, $gameres.EntityResDict.BLACK_ESSENCE_BOWL_DESC);
                return snowatch.compose(resource.template);
              }
            }},
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBowlBehaviors),
          ],
          spatialRelations: [
            { typeId: snowatch.SpatialPrepositions.isOn.id, targetId: $items.BowlShelf.ID },
          ],
        },
      }
    );

    // initialization
    this.registerInitializer(() => {
      snowatch.Container.initializeContainer(this);
    });
    return this;
  }
}
