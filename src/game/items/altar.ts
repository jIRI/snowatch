import * as snowatch from 'snowatch/snowatch';
import * as behaviors from 'snowatch/behaviors/all';
import * as attrs from 'snowatch/attributes/all';

import * as $current from 'game/current-game/all';
import * as $scenes from 'game/scenes/all';
import * as $items from 'game/items/all';
import * as $gameres from 'game/resources/game-res';

export class Altar extends snowatch.Item {
  public static readonly ID = 'Altar';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData) {
    super.initialize(
      game,
      {
        title: 'Stone Altar',
        startSceneId: $scenes.HubChancel.ID,
        holderId: null,
        ownerId: null,
        properties: {
          compAttributes: [
            { id: attrs.AvailableSpatialCapacity, compState: attrs.SpatialCapacityComputedAttribute.default },
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const resource = snowatch.ResourceDictionary.get(this.game, $gameres.EntityResDict.ALTAR_DESC);
                return snowatch.compose(resource.template);
              }
            }},
          ],
          constAttributes: [
            { id: attrs.NounResourceId, constState: { value: $gameres.EntityResDict.ALTAR_NOUN }},
            { id: attrs.CanBeIndirect },
            { id: attrs.Surfaces, constState: { value: [snowatch.Surface.outerTop, ] }, },
            { id: attrs.SpatialLocation, constState: { value: $scenes.HubChancel.SpatialLocation} },
            { id: attrs.SpatialSize, constState: { value: {x: 1, y: 2, z: 1.25}} },
            { id: attrs.HasSpatialRelations },
          ],
          spatialRelations: [
          ],
        }
      }
    );
    return this;
  }
}
