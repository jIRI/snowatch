import * as snowatch from 'snowatch/snowatch';
import * as behaviors from 'snowatch/behaviors/all';
import * as attrs from 'snowatch/attributes/all';

import * as $current from 'game/current-game/all';
import * as $attrs from 'game/current-game/attributes';
import * as $substances from 'game/substances/all';
import * as $actors from 'game/actors/all';
import * as $scenes from 'game/scenes/all';
import * as $behaviors from 'game/behaviors/all';
import * as $gameres from 'game/resources/game-res';

const CommonBeamConstAttributes = [
  { id: attrs.HasSpatialRelations },
  { id: attrs.CanBeIndirect },
  { id: attrs.IsContainer },
  { id: attrs.SpatialSize, constState: { value: {x: 1.5, y: 1.5, z: 10}} },
  { id: attrs.Surfaces, constState: { value: []} },
];

const CommonBeamComputedAttributes = [
  { id: attrs.AvailableSpatialCapacity, compState: attrs.SpatialCapacityComputedAttribute.default },
];

const CommonBeamAttributes = [
  { id: $attrs.IsBeamActive, initialState: { value: false } },
];

const ActivateTheBeamBehaviorId = 'activate-the-beam';
const CommonBehaviors: Array<snowatch.BehaviorInit> = [
  { id: ActivateTheBeamBehaviorId,
    activate: (entity: snowatch.Entity, ...args: any[]) => {
      if( attrs.Attribute.value<boolean>(entity, $attrs.IsBeamActive) ) {
        // if beam is already active, the behavior is supposed to be removed.
        // however system adds behaviors from init object automatically, so we need to handle that.
        behaviors.Behavior.remove(entity, behaviors.MakeSoundWhenItemEntersContainer.ID);
        behaviors.Behavior.remove(entity, ActivateTheBeamBehaviorId);
      // beam now can teleport to target realm
      behaviors.Behavior.add(entity,
        behaviors.TeleportWhenEntered.ID,
        { initialState: <behaviors.TeleportWhenEnteredState>{
            entityId: $actors.LastOne.ID,
            targetForTheFirstTimeSceneId: attrs.Attribute.value<string>(entity, $attrs.TeleportTargetForTheFirstTimeSceneId),
            targetSceneId: attrs.Attribute.value<string>(entity, $attrs.TeleportTargetSceneId),
          }
        }
      );
      }
    },
    filter: (target: snowatch.Entity, initialState: any, message: snowatch.Message): snowatch.ResultObject => {
      return (message.topicId === snowatch.Topic.id($current.CustomTopics.ACTIVATE_BEAMS)) ? snowatch.make(snowatch.accepted) : snowatch.make(snowatch.rejected);
    },
    process: (target: snowatch.Entity, initialState: any, message: snowatch.Message) => {
      // set attribute
      attrs.Attribute.setValue<boolean>(target, $attrs.IsBeamActive, true);
      // beam now can teleport to target realm
      behaviors.Behavior.add(target,
        behaviors.TeleportWhenEntered.ID,
        { initialState: <behaviors.TeleportWhenEnteredState>{
            entityId: $actors.LastOne.ID,
            targetForTheFirstTimeSceneId: attrs.Attribute.value<string>(target, $attrs.TeleportTargetForTheFirstTimeSceneId),
            targetSceneId: attrs.Attribute.value<string>(target, $attrs.TeleportTargetSceneId),
          }
        }
      );
      // there is appearance change of the beam
      target.game.bus.notifications.publish(snowatch.broadcast<snowatch.Topics.APPEARANCE_CHANGE>(
        snowatch.Topics.APPEARANCE_CHANGE,
        {
          entityId: target.id,
          appearanceDescriptionResourceId: $gameres.ActionResDict.BEAM_ACTIVATED_APPEARANCE_CHANGE_DESC,
          appearanceDescriptionResourceTemplateBag: {
            beamNoun: snowatch.Noun.get(
                target.game,
                snowatch.ResourceDictionary.get(target.game, attrs.Attribute.value<string>(target, attrs.NounResourceId))
              )
              .noun(snowatch.Case.nominative),
          },
          sourceSceneId: snowatch.getCurrentSceneId(target),
          sourceLocation: snowatch.getSpatialLocation(target),
          }
      ));
      // remove some behaviors, we do not need anymore
      behaviors.Behavior.remove(target, behaviors.MakeSoundWhenItemEntersContainer.ID);
      behaviors.Behavior.remove(target, ActivateTheBeamBehaviorId);
    }
  },
];


export class WhiteBeamOfLight extends snowatch.Item {
  public static readonly ID = 'WhiteBeamOfLight';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Entity {
    super.initialize(
      game,
      {
        title: "White Beam Of Light",
        startSceneId: $scenes.HubNave.ID,
        properties: {
          constAttributes: [
            ...CommonBeamConstAttributes,
            { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.HubNave.SpatialLocation, {x: -2.5, y: 5, z: 0})} },
            { id: $attrs.TeleportTargetForTheFirstTimeSceneId, constState: { value: $scenes.WhiteEnteringForTheFirstTime.ID} },
            { id: $attrs.TeleportTargetSceneId, constState: { value: $scenes.WhiteLobby.ID} },
            { id: $attrs.EssenceLiquidId, constState: { value: $substances.WhiteEssenceLiquid.ID} },
          ],
          attributes: [
            ...CommonBeamAttributes,
          ],
          compAttributes: [
            ...CommonBeamComputedAttributes,
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.WHITE_BEAM_OF_LIGHT_NOUN;
                },
            }},
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const resource = snowatch.ResourceDictionary.get(this.game, $gameres.EntityResDict.WHITE_BEAM_OF_LIGHT_DESC);
                const active: boolean = attrs.Attribute.value<boolean>(this, $attrs.IsBeamActive);
                return snowatch.compose(active ? resource.templateActive : resource.templateInactive);
              }
            }},
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBehaviors),
            { id: behaviors.MakeSoundWhenItemEntersContainer.ID, initialState: {
              sources: [
                { sourceId: $actors.LastOne.ID, resourceId: $gameres.EntityResDict.WHITE_BEAM_OF_LIGHT_SOUND_DESC, soundLevel: 100, soundDuration: 10 },
              ],
            } as behaviors.MakeSoundWhenItemEntersContainerState },
          ],
          spatialRelations: [
          ],
        },
      }
    );

    // initialization
    this.registerInitializer(() => {
      snowatch.Container.initializeContainer(this);
    });
    return this;
  }
}

export class RedBeamOfLight extends snowatch.Item {
  public static readonly ID = 'RedBeamOfLight';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Entity {
    super.initialize(
      game,
      {
        title: "Red Beam Of Light",
        startSceneId: $scenes.HubNave.ID,
        properties: {
          constAttributes: [
            ...CommonBeamConstAttributes,
            { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.HubNave.SpatialLocation, {x: 2.5, y: 0, z: 0})}},
            { id: $attrs.TeleportTargetForTheFirstTimeSceneId, constState: { value: $scenes.RedEntry.ID} },
            { id: $attrs.TeleportTargetSceneId, constState: { value: $scenes.RedEntry.ID} },
            { id: $attrs.EssenceLiquidId, constState: { value: $substances.RedEssenceLiquid.ID} },
          ],
          attributes: [
            ...CommonBeamAttributes,
          ],
          compAttributes: [
            ...CommonBeamComputedAttributes,
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.RED_BEAM_OF_LIGHT_NOUN;
                },
            }},
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const resource = snowatch.ResourceDictionary.get(this.game, $gameres.EntityResDict.RED_BEAM_OF_LIGHT_DESC);
                const active: boolean = attrs.Attribute.value<boolean>(this, $attrs.IsBeamActive);
                return snowatch.compose(active ? resource.templateActive : resource.templateInactive);
              }
            }},
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBehaviors),
            { id: behaviors.MakeSoundWhenItemEntersContainer.ID, initialState: {
              sources: [
                {sourceId: $actors.LastOne.ID, resourceId: $gameres.EntityResDict.RED_BEAM_OF_LIGHT_SOUND_DESC, soundLevel: 100, soundDuration: 10},
              ],
            } as behaviors.MakeSoundWhenItemEntersContainerState },
          ],
          spatialRelations: [
          ],
        },
      }
    );

    // initialization
    this.registerInitializer(() => {
      snowatch.Container.initializeContainer(this);
    });
    return this;
  }
}

export class BlueBeamOfLight extends snowatch.Item {
  public static readonly ID = 'BlueBeamOfLight';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Entity {
    super.initialize(
      game,
      {
        title: "Blue Beam Of Light",
        startSceneId: $scenes.HubNave.ID,
        properties: {
          constAttributes: [
            ...CommonBeamConstAttributes,
            { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.HubNave.SpatialLocation, {x: 2.5, y: -5, z: 0})}},
            { id: $attrs.TeleportTargetForTheFirstTimeSceneId, constState: { value: $scenes.BlueEntry.ID} },
            { id: $attrs.TeleportTargetSceneId, constState: { value: $scenes.BlueEntry.ID} },
            { id: $attrs.EssenceLiquidId, constState: { value: $substances.BlueEssenceLiquid.ID} },
          ],
          attributes: [
            ...CommonBeamAttributes,
          ],
          compAttributes: [
            ...CommonBeamComputedAttributes,
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.BLUE_BEAM_OF_LIGHT_NOUN;
                },
            }},
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const resource = snowatch.ResourceDictionary.get(this.game, $gameres.EntityResDict.BLUE_BEAM_OF_LIGHT_DESC);
                const active: boolean = attrs.Attribute.value<boolean>(this, $attrs.IsBeamActive);
                return snowatch.compose(active ? resource.templateActive : resource.templateInactive);
              }
            }},
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBehaviors),
            { id: behaviors.MakeSoundWhenItemEntersContainer.ID, initialState: {
              sources: [
                {sourceId: $actors.LastOne.ID, resourceId: $gameres.EntityResDict.BLUE_BEAM_OF_LIGHT_SOUND_DESC, soundLevel: 100, soundDuration: 10},
              ],
            } as behaviors.MakeSoundWhenItemEntersContainerState },
          ],
          spatialRelations: [
          ],
        },
      }
    );

    // initialization
    this.registerInitializer(() => {
      snowatch.Container.initializeContainer(this);
    });
    return this;
  }
}

export class GreenBeamOfLight extends snowatch.Item {
  public static readonly ID = 'GreenBeamOfLight';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Entity {
    super.initialize(
      game,
      {
        title: "Green Beam Of Light",
        startSceneId: $scenes.HubNave.ID,
        properties: {
          constAttributes: [
            ...CommonBeamConstAttributes,
            { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.HubNave.SpatialLocation, {x: -2.5, y: 0, z: 0})}},
            { id: $attrs.TeleportTargetForTheFirstTimeSceneId, constState: { value: $scenes.GreenEntry.ID} },
            { id: $attrs.TeleportTargetSceneId, constState: { value: $scenes.GreenEntry.ID} },
            { id: $attrs.EssenceLiquidId, constState: { value: $substances.GreenEssenceLiquid.ID} },
          ],
          attributes: [
            ...CommonBeamAttributes,
          ],
          compAttributes: [
            ...CommonBeamComputedAttributes,
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.GREEN_BEAM_OF_LIGHT_NOUN;
                },
            }},
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const resource = snowatch.ResourceDictionary.get(this.game, $gameres.EntityResDict.GREEN_BEAM_OF_LIGHT_DESC);
                const active: boolean = attrs.Attribute.value<boolean>(this, $attrs.IsBeamActive);
                return snowatch.compose(active ? resource.templateActive : resource.templateInactive);
              }
            }},
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBehaviors),
            { id: behaviors.MakeSoundWhenItemEntersContainer.ID, initialState: {
              sources: [
                {sourceId: $actors.LastOne.ID, resourceId: $gameres.EntityResDict.GREEN_BEAM_OF_LIGHT_SOUND_DESC, soundLevel: 100, soundDuration: 10},
              ],
            } as behaviors.MakeSoundWhenItemEntersContainerState },
          ],
          spatialRelations: [
          ],
        },
      }
    );

    // initialization
    this.registerInitializer(() => {
      snowatch.Container.initializeContainer(this);
    });
    return this;
  }
}

export class YellowBeamOfLight extends snowatch.Item {
  public static readonly ID = 'YellowBeamOfLight';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Entity {
    super.initialize(
      game,
      {
        title: "Yellow Beam Of Light",
        startSceneId: $scenes.HubNave.ID,
        properties: {
          constAttributes: [
            ...CommonBeamConstAttributes,
            { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.HubNave.SpatialLocation, {x: -2.5, y: -5, z: 0})}},
            { id: $attrs.TeleportTargetForTheFirstTimeSceneId, constState: { value: $scenes.YellowEntry.ID} },
            { id: $attrs.TeleportTargetSceneId, constState: { value: $scenes.YellowEntry.ID} },
            { id: $attrs.EssenceLiquidId, constState: { value: $substances.YellowEssenceLiquid.ID} },
          ],
          attributes: [
            ...CommonBeamAttributes,
          ],
          compAttributes: [
            ...CommonBeamComputedAttributes,
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.YELLOW_BEAM_OF_LIGHT_NOUN;
                },
            }},
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const resource = snowatch.ResourceDictionary.get(this.game, $gameres.EntityResDict.YELLOW_BEAM_OF_LIGHT_DESC);
                const active: boolean = attrs.Attribute.value<boolean>(this, $attrs.IsBeamActive);
                return snowatch.compose(active ? resource.templateActive : resource.templateInactive);
              }
            }},
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBehaviors),
            { id: behaviors.MakeSoundWhenItemEntersContainer.ID, initialState: {
              sources: [
                {sourceId: $actors.LastOne.ID, resourceId: $gameres.EntityResDict.YELLOW_BEAM_OF_LIGHT_SOUND_DESC, soundLevel: 100, soundDuration: 10},
              ],
            } as behaviors.MakeSoundWhenItemEntersContainerState },
          ],
          spatialRelations: [
          ],
        },
      }
    );

    // initialization
    this.registerInitializer(() => {
      snowatch.Container.initializeContainer(this);
    });
    return this;
  }
}

export class BlackBeamOfLight extends snowatch.Item {
  public static readonly ID = 'BlackBeamOfLight';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Entity {
    super.initialize(
      game,
      {
        title: "Black Beam Of Light",
        startSceneId: $scenes.HubNave.ID,
        properties: {
          constAttributes: [
            ...CommonBeamConstAttributes,
            { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.HubNave.SpatialLocation, {x: 2.5, y: 5, z: 0})}},
            { id: $attrs.TeleportTargetForTheFirstTimeSceneId, constState: { value: $scenes.BlackEntry.ID} },
            { id: $attrs.TeleportTargetSceneId, constState: { value: $scenes.BlackEntry.ID} },
            { id: $attrs.EssenceLiquidId, constState: { value: $substances.BlackEssenceLiquid.ID} },
          ],
          attributes: [
            ...CommonBeamAttributes,
          ],
          compAttributes: [
            ...CommonBeamComputedAttributes,
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.BLACK_BEAM_OF_LIGHT_NOUN;
                },
            }},
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const resource = snowatch.ResourceDictionary.get(this.game, $gameres.EntityResDict.BLACK_BEAM_OF_LIGHT_DESC);
                const active: boolean = attrs.Attribute.value<boolean>(this, $attrs.IsBeamActive);
                return snowatch.compose(active ? resource.templateActive : resource.templateInactive);
              }
            }},
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBehaviors),
            { id: behaviors.MakeSoundWhenItemEntersContainer.ID, initialState: {
              sources: [
                {sourceId: $actors.LastOne.ID, resourceId: $gameres.EntityResDict.BLACK_BEAM_OF_LIGHT_SOUND_DESC, soundLevel: 100, soundDuration: 10},
              ],
            } as behaviors.MakeSoundWhenItemEntersContainerState },
          ],
          spatialRelations: [
          ],
        },
      }
    );

    // initialization
    this.registerInitializer(() => {
      snowatch.Container.initializeContainer(this);
    });
    return this;
  }
}

