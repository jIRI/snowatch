import * as snowatch from 'snowatch/snowatch';
import * as behaviors from 'snowatch/behaviors/all';
import * as attrs from 'snowatch/attributes/all';

import * as $current from 'game/current-game/all';
import * as $scenes from 'game/scenes/all';
import * as $items from 'game/items/all';
import * as $gameres from 'game/resources/game-res';

const CommonConstAerosolDispenserAttributes = [
  { id: attrs.IsContainer },
  { id: attrs.CanBeIndirect },
  { id: attrs.SpatialSize, constState: { value: {x: 1, y: 2, z: 1.25}} },
  { id: attrs.HasSpatialRelations },
];

export class AerosolDispenserRight extends snowatch.Item {
  public static readonly ID = 'AerosolDispenserRight';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData) {
    super.initialize(
      game,
      {
        title: 'Aerosol Dispenser',
        startSceneId: $scenes.WhiteRightServiceRoom.ID,
        holderId: null,
        ownerId: null,
        properties: {
          compAttributes: [
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const resource = snowatch.ResourceDictionary.get(this.game, $gameres.EntityResDict.AEROSOL_DISPENSER_DESC);
                return snowatch.compose(resource.template, {
                  sideAdjective: snowatch.Adjective.get(this.game, resource.rightAdjectiveId).adjective(snowatch.Number.singular, snowatch.Case.genitive, snowatch.Gender.feminine)
                });
              }
            }},
          ],
          constAttributes: [
            { id: attrs.NounResourceId, constState: { value: $gameres.EntityResDict.AEROSOL_DISPENSER_NOUN }},
            { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.WhiteRightServiceRoom.SpatialLocation, {x: 3, y: 0, z: 1.625})} },
            ...CommonConstAerosolDispenserAttributes
          ],
          behaviors: [
            { id: behaviors.ChangeSubstancePhase.ID,
              initialState: <behaviors.ChangeSubstancePhaseState>{
                substancePhase: snowatch.SubstancePhases.liquid,
                triggerRelationIds: [ snowatch.SpatialPrepositions.isIn.id ],
                transformedSubstances: [
                  { phase: snowatch.SubstancePhases.gas,
                    sceneId: $scenes.WhiteLeftReception.ID,
                  },
                  { phase: snowatch.SubstancePhases.gas,
                    sceneId: $scenes.WhiteLeftHallway.ID,
                  },
                  { phase: snowatch.SubstancePhases.gas,
                    sceneId: $scenes.WhiteLeftPatientRoomNorth.ID,
                  },
                  { phase: snowatch.SubstancePhases.gas,
                    sceneId: $scenes.WhiteLeftPatientRoomSouth.ID,
                  },
                ]
            }},
          ],
        }
      }
    );

    // initialization
    this.registerInitializer(() => {
      snowatch.Container.initializeContainer(this);
    });
    return this;
  }
}

export class AerosolDispenserLeft extends snowatch.Item {
  public static readonly ID = 'AerosolDispenserLeft';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData) {
    super.initialize(
      game,
      {
        title: 'Aerosol Dispenser',
        startSceneId: $scenes.WhiteLeftServiceRoom.ID,
        holderId: null,
        ownerId: null,
        properties: {
          compAttributes: [
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const resource = snowatch.ResourceDictionary.get(this.game, $gameres.EntityResDict.AEROSOL_DISPENSER_DESC);
                return snowatch.compose(resource.template, {
                  sideAdjective: snowatch.Adjective.get(this.game, resource.leftAdjectiveId).adjective(snowatch.Number.singular, snowatch.Case.genitive, snowatch.Gender.feminine)
                });
              }
            }},
          ],
          constAttributes: [
            { id: attrs.NounResourceId, constState: { value: $gameres.EntityResDict.AEROSOL_DISPENSER_NOUN }},
            { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.WhiteLeftServiceRoom.SpatialLocation, {x: -3, y: 0, z: 1.625})} },
            ...CommonConstAerosolDispenserAttributes
          ],
          behaviors: [
            { id: behaviors.ChangeSubstancePhase.ID,
              initialState: <behaviors.ChangeSubstancePhaseState>{
                substancePhase: snowatch.SubstancePhases.liquid,
                triggerRelationIds: [ snowatch.SpatialPrepositions.isIn.id ],
                transformedSubstances: [
                  { phase: snowatch.SubstancePhases.gas,
                    sceneId: $scenes.WhiteLeftReception.ID,
                    dissipationRateTicks: 50,
                  },
                  { phase: snowatch.SubstancePhases.gas,
                    sceneId: $scenes.WhiteLeftHallway.ID,
                    dissipationRateTicks: 50,
                  },
                  { phase: snowatch.SubstancePhases.gas,
                    sceneId: $scenes.WhiteLeftPatientRoomNorth.ID,
                    dissipationRateTicks: 50,
                  },
                  { phase: snowatch.SubstancePhases.gas,
                    sceneId: $scenes.WhiteLeftPatientRoomSouth.ID,
                    dissipationRateTicks: 50,
                  },
                ]
            }},
          ],
       }
      }
    );

    // initialization
    this.registerInitializer(() => {
      snowatch.Container.initializeContainer(this);
    });

    return this;
  }
}