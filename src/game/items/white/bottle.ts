import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';
import * as behaviors from 'snowatch/behaviors/all';

import * as $scenes from 'game/scenes/all';
import * as $gameres from 'game/resources/game-res';

export class Bottle extends snowatch.Item {
  public static readonly ID = 'Bottle';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData) {
    super.initialize(
      game,
      {
        title: 'Bottle',
        startSceneId: $scenes.WhiteLeftServiceRoom.ID,
        holderId: null,
        ownerId: null,
        properties: {
          attributes: [
            { id: attrs.SpatialLocation, initialState: { value: snowatch.addVect3($scenes.WhiteLeftServiceRoom.SpatialLocation, {x: 0, y: 0, z: 0})} },
          ],
          compAttributes: [
            { id: attrs.AvailableSpatialCapacity, compState: attrs.SpatialCapacityComputedAttribute.default },
            { id: attrs.Volume, compState: attrs.VolumeComputedAttribute.default },
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const resource = snowatch.ResourceDictionary.get(entity.game, $gameres.EntityResDict.BOTTLE_DESC);
                const capacity = snowatch.Size3.volume(snowatch.getSpatialSize(entity) ?? snowatch.Zero3);
                const capacityUsed = attrs.Attribute.value<number>(entity, attrs.ContainerCapacityUsed, actor);
                return snowatch.compose(
                  resource.template,
                  {
                    contentsDescription: $gameres.ResourceDictionary.getContentsDescription(entity.game, $gameres.EntityResDict.BOTTLE_DESC, capacityUsed / capacity),
                  }
                );
              }
            }},
          ],
          constAttributes: [
            { id: attrs.NounResourceId, constState: { value: $gameres.EntityResDict.BOTTLE_NOUN }},
            { id: attrs.Weight },
            { id: attrs.IsContainer },
            { id: attrs.CanBeIndirect },
            { id: attrs.HasSpatialRelations },
            { id: attrs.Surfaces, constState: { value: [snowatch.Surface.inside, snowatch.Surface.outerBottom, ] }, },
            { id: attrs.SpatialSize, constState: { value: {x: 0.075, y: 0.075, z: 0.2}} },
            { id: attrs.ContainerSupportsDrinking, constState: { value: true } },
            { id: attrs.ContainerSupportsPouring, constState: { value: true } },
          ],
          behaviors: [
            { id: behaviors.SupportsTaking.ID },
            { id: behaviors.SupportsPutting.ID },
            { id: behaviors.SupportsGiving.ID },
          ],
          spatialRelations: [
          ],
        }
      }
    );

    // initialization
    this.registerInitializer(() => {
      snowatch.Container.initializeContainer(this);
    });

    return this;
  }
}
