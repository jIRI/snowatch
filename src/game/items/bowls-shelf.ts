import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

import * as $scenes from 'game/scenes/all';
import * as $behaviors from 'game/behaviors/all';
import * as $items from 'game/items/all';
import * as $gameres from 'game/resources/game-res';

export class BowlShelf extends snowatch.Item {
  public static readonly ID = 'BowlShelf';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData) {
    super.initialize(
      game,
      {
        title: 'Bowls Shelf',
        startSceneId: $scenes.HubChancel.ID,
        holderId: null,
        ownerId: null,
        properties: {
          attributes: [
          ],
          compAttributes: [
            { id: attrs.AvailableSpatialCapacity, compState: attrs.SpatialCapacityComputedAttribute.default },
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const resource = snowatch.ResourceDictionary.get(this.game, $gameres.EntityResDict.BOWL_SHELF_DESC);
                return snowatch.compose(resource.template);
              }
            }},
          ],
          constAttributes: [
            { id: attrs.NounResourceId, constState: { value: $gameres.EntityResDict.BOWL_SHELF_NOUN }},
            { id: attrs.CanBeIndirect },
            { id: attrs.Surfaces, constState: { value: [snowatch.Surface.outerTop, snowatch.Surface.outerBottom, ] }, },
            { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.HubSacristy.SpatialLocation, {x: 0, y: 2, z: 0.75})} },
            { id: attrs.SpatialSize, constState: { value: {x: 6 * 0.3, y: 0.3, z: 0.1}} },
            { id: attrs.HasSpatialRelations },
          ],
          spatialRelations: [
          ],
        }
      }
    );
    return this;
  }
}
