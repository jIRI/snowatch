import {FrameworkConfiguration} from 'aurelia-framework';

import * as $realms from 'game/realms/all';

export function configure(aurelia: FrameworkConfiguration) {
  aurelia.singleton($realms.Realm, $realms.HubRealm);
  aurelia.singleton($realms.Realm, $realms.WhiteRealm);
}
