import * as snowatch from 'snowatch/snowatch';
import * as behaviors from 'snowatch/behaviors/all';
import * as attrs from 'snowatch/attributes/all';

import * as $current from 'game/current-game/all';
import * as $scenes from 'game/scenes/all';
import * as $items from 'game/items/all';
import * as $gameres from 'game/resources/game-res';

export class HubRealm extends snowatch.Realm {
  public static readonly ID = 'HubRealm';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData) {
    super.initialize(
      game,
      {
        sceneIds: [
          $scenes.HubChancel.ID,
          $scenes.HubNave.ID,
          $scenes.HubSacristy.ID,
          $scenes.HubSacristyFirstTimeLeaving.ID,
          $scenes.HubTowerBase.ID,
        ],
        properties: {
          compAttributes: [
          ],
          constAttributes: [
            { id: attrs.IsVisible, constState: { value: false } },
          ],
          behaviors: [
            // when all bowls are on proper pillars, activate light beams
            { id: behaviors.SendMessageWhenSpatialRelationsExist.ID, initialState: <behaviors.SendMessageWhenSpatialRelationsExistState>{
                filterSourcesIds: [ $items.WhiteEssenceBowl.ID, $items.GreenEssenceBowl.ID, $items.RedEssenceBowl.ID, $items.YellowEssenceBowl.ID, $items.BlueEssenceBowl.ID, $items.BlackEssenceBowl.ID, ],
                filterTargetsIds: [ $items.WhiteBowlPillar.ID, $items.GreenBowlPillar.ID, $items.RedBowlPillar.ID, $items.YellowBowlPillar.ID, $items.BlueBowlPillar.ID, $items.BlackBowlPillar.ID, ],
                triggerRelations: [
                  { typeId: snowatch.SpatialPrepositions.isOn.id, directId: $items.WhiteEssenceBowl.ID, indirectId: $items.WhiteBowlPillar.ID},
                  { typeId: snowatch.SpatialPrepositions.isOn.id, directId: $items.GreenEssenceBowl.ID, indirectId: $items.GreenBowlPillar.ID},
                  { typeId: snowatch.SpatialPrepositions.isOn.id, directId: $items.RedEssenceBowl.ID, indirectId: $items.RedBowlPillar.ID},
                  { typeId: snowatch.SpatialPrepositions.isOn.id, directId: $items.YellowEssenceBowl.ID, indirectId: $items.YellowBowlPillar.ID},
                  { typeId: snowatch.SpatialPrepositions.isOn.id, directId: $items.BlueEssenceBowl.ID, indirectId: $items.BlueBowlPillar.ID},
                  { typeId: snowatch.SpatialPrepositions.isOn.id, directId: $items.BlackEssenceBowl.ID, indirectId: $items.BlackBowlPillar.ID},
                ],
                broadcastTopicId: snowatch.Topic.id($current.CustomTopics.ACTIVATE_BEAMS),
                broadcastPayload: new $current.CustomTopics.ACTIVATE_BEAMS({}),
            }},
            { id: behaviors.DisableBehaviorsOnMessage.ID, initialState: {
              topicId: snowatch.Topic.id($current.CustomTopics.ACTIVATE_BEAMS),
              behaviorIds: [
                behaviors.SendMessageWhenSpatialRelationsExist.ID,
              ]} as behaviors.DisableBehaviorsOnMessageState
            },
            // when beams are activated, make a sound so player is aware there was some change in nave and beams
            { id: 'make-sound-when-beams-are-activated',
              filter: (target: snowatch.Entity, initialState: any, message: snowatch.Message) => {
                return (message.topicId === snowatch.Topic.id($current.CustomTopics.ACTIVATE_BEAMS)) ? snowatch.make(snowatch.accepted) : snowatch.make(snowatch.rejected);
              },
              process: (target: snowatch.Entity, initialState: any, message: snowatch.Message) => {
                const messagePayload = snowatch.payload<snowatch.Topics.SPATIAL_RELATION_ADDED>(message);
                const sourceScene = target.game.instances.getSceneFromId($scenes.HubNave.ID);
                if( sourceScene == null ) {
                  return;
                }
                this.game.bus.notifications.publish(snowatch.broadcast(
                  snowatch.Topics.SOUND,
                  {
                    entityId: target.id,
                    soundDescriptionResourceId: $gameres.ActionResDict.BEAMS_ACTIVATED_SOUND_DESC,
                    soundLevel: 200,
                    duration: 5,
                    sourceSceneId: sourceScene.id,
                    sourceLocation: snowatch.getSpatialLocation(sourceScene),
                  },
                  undefined,
                  target.id
                ));
              }
            },
          ],
        }
      }
    );

    return this;
  }
}
