import * as snowatch from 'snowatch/snowatch';
import * as behaviors from 'snowatch/behaviors/all';
import * as attrs from 'snowatch/attributes/all';

import * as $current from 'game/current-game/all';
import * as $scenes from 'game/scenes/all';
import * as $items from 'game/items/all';
import * as $gameres from 'game/resources/game-res';

export class WhiteRealm extends snowatch.Realm {
  public static readonly ID = 'WhiteRealm';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData) {
    super.initialize(
      game,
      {
        sceneIds: [
          $scenes.WhiteEnteringForTheFirstTime.ID,
          $scenes.WhiteLobby.ID,
          $scenes.WhiteLeftReception.ID,
          $scenes.WhiteLeftUpstairs.ID,
          $scenes.WhiteLeftLab.ID,
          $scenes.WhiteLeftHallway.ID,
          $scenes.WhiteLeftPatientRoomSouth.ID,
          $scenes.WhiteLeftPatientRoomNorth.ID,
          $scenes.WhiteLeftMessHall.ID,
          $scenes.WhiteRightReception.ID,
          $scenes.WhiteRightUpstairs.ID,
          $scenes.WhiteRightLab.ID,
          $scenes.WhiteRightHallway.ID,
          $scenes.WhiteRightPatientRoomSouth.ID,
          $scenes.WhiteRightPatientRoomNorth.ID,
          $scenes.WhiteRightMessHall.ID,
          $scenes.WhiteTerraceEntrance.ID,
        ],
        properties: {
          compAttributes: [
          ],
          constAttributes: [
            { id: attrs.IsVisible, constState: { value: false } },
          ],
          behaviors: [
          ],
        }
      }
    );
    return this;
  }
}
