export * from 'game/dictionary/verbs';
export * from 'game/dictionary/verbs-wiki';
export * from 'game/dictionary/nouns';
export * from 'game/dictionary/pronouns';
export * from 'game/dictionary/adjectives';
export * from 'game/dictionary/adjectives-wiki';
export * from 'game/dictionary/conjunctions';
export * from 'game/dictionary/adverbs';
export * from 'game/dictionary/prepositions';

