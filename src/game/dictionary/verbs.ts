import { FrameworkConfiguration } from 'aurelia-framework';
import * as lang from 'snowatch/language-structures';

import { configureVerbsWiki } from 'game/dictionary/verbs-wiki';

export * from 'game/dictionary/verbs-wiki';

export function configureVerbs(aurelia: FrameworkConfiguration) {
  configureVerbsWiki(aurelia);
  aurelia.singleton(lang.LanguageStructure, ProbuditSe);
  aurelia.singleton(lang.LanguageStructure, Rozhlédnout);
  aurelia.singleton(lang.LanguageStructure, RozhlédnoutSe);
  aurelia.singleton(lang.LanguageStructure, Stisknout);
  aurelia.singleton(lang.LanguageStructure, Vypít);
  aurelia.singleton(lang.LanguageStructure, Ocitnout);
  aurelia.singleton(lang.LanguageStructure, Nalít);
  aurelia.singleton(lang.LanguageStructure, Vypařit);
  aurelia.singleton(lang.LanguageStructure, Ucítit);
}

// ----------------------------------------------------------------------------------------

export class ProbuditSe extends lang.Verb {
  static ID = `${lang.Verb.ID}.probudit-se`;
  constructor() {
    super({
      verb: 'probudit se',
      infinitive: 'probudit se',
      singular: {
        present: {
          first: 'probouzím se',
          second: 'probouzíš se',
          third: 'probouzí se',
        },
        future: {
          first: 'probudím se',
          second: 'probudíš se',
          third: 'probudí se',
        },
        imperative: 'probuď se',
        past: {
          first: { masculine: 'probudil se', feminine: 'probudila se', neutral: 'probudilo se', },
          second: { masculine: 'probudil se', feminine: 'probudila se', neutral: 'probudilo se', },
          third: { masculine: 'probudil se', feminine: 'probudila se', neutral: 'probudilo se', },
        },
        participle: {
          masculine: 'probudiv se',
          feminine: 'probudivši se',
          neutral: 'probudivši se',
        },
      },
      plural: {
        present: {
          first: 'probouzíme se',
          second: 'probouzíte se',
          third: 'probouzí se',
        },
        future: {
          first: 'probudíme se',
          second: 'probudíte se',
          third: 'probudí se',
        },
        imperative: {
          second: 'probuďme se',
          third: 'probuďte se',
        },
        past: {
          first: { masculine: 'probudili se', feminine: 'probudily se', neutral: 'probudila se', },
          second: { masculine: 'probudili se', feminine: 'probudily se', neutral: 'probudila se', },
          third: { masculine: 'probudili se', feminine: 'probudily se', neutral: 'probudila se', },
        },
        passive: {
          first: { masculine: 'probuzeni se', feminine: 'probuzeny se', neutral: 'probuzena se', },
          second: { masculine: 'probuzeni se', feminine: 'probuzeny se', neutral: 'probuzena se', },
          third: { masculine: 'probuzeni se', feminine: 'probuzeny se', neutral: 'probuzena se', },
        },
        participle: 'probudivše se',
      },
    });
  }
}

export class RozhlédnoutSe extends lang.Verb {
  static ID = `${lang.Verb.ID}.rozhlédnout-se`;
  constructor() {
    super({
      verb: 'rozhlédnout se',
      infinitive: 'rozhlédnout se',
      singular: {
        present: {
          first: 'rozhlížím se',
          second: 'rozhlížíš se',
          third: 'rozhlíží se',
        },
        future: {
          first: 'reozhlédnu se',
          second: 'rozhlédneš se',
          third: 'rozhlédne se',
        },
        imperative: 'rozhlédni se',
        past: {
          first: { masculine: 'rozhlédl se', feminine: 'rozhlédla se', neutral: 'rozhlédlo se', },
          second: { masculine: 'rozhlédl se', feminine: 'rozhlédla se', neutral: 'rozhlédlo se', },
          third: { masculine: 'rozhlédl se', feminine: 'rozhlédla se', neutral: 'rozhlédlo se', },
        },
        participle: {
          masculine: 'rozhlédnuv se',
          feminine: 'rozhlédnuvši se',
          neutral: 'rozhlédnuvši se',
        },
      },
      plural: {
        present: {
          first: 'rozhlížíme se',
          second: 'rozhlížíte se',
          third: 'rozhlíží se',
        },
        future: {
          first: 'rozhlédneme se',
          second: 'rozhlédnete se',
          third: 'rozhlédnou se',
        },
        imperative: {
          second: 'rozhlédněme se',
          third: 'rozhlédněte se',
        },
        past: {
          first: { masculine: 'rozhlédli se', feminine: 'rozhlédly se', neutral: 'rozhlédla se', },
          second: { masculine: 'rozhlédli se', feminine: 'rozhlédly se', neutral: 'rozhlédla se', },
          third: { masculine: 'rozhlédli se', feminine: 'rozhlédly se', neutral: 'rozhlédla se', },
        },
        participle: 'rozhlédnuvše se',
      },
    });
  }
}

export class Rozhlédnout extends lang.Verb {
  static ID = `${lang.Verb.ID}.rozhlédnout`;
  constructor() {
    super({
      verb: 'rozhlédnout',
      infinitive: 'rozhlédnout',
      singular: {
        present: {
          first: 'rozhlížím',
          second: 'rozhlížíš',
          third: 'rozhlíží',
        },
        future: {
          first: 'reozhlédnu',
          second: 'rozhlédneš',
          third: 'rozhlédne',
        },
        imperative: 'rozhlédni',
        past: {
          first: { masculine: 'rozhlédl', feminine: 'rozhlédla', neutral: 'rozhlédlo', },
          second: { masculine: 'rozhlédl', feminine: 'rozhlédla', neutral: 'rozhlédlo', },
          third: { masculine: 'rozhlédl', feminine: 'rozhlédla', neutral: 'rozhlédlo', },
        },
        participle: {
          masculine: 'rozhlédnuv',
          feminine: 'rozhlédnuvši',
          neutral: 'rozhlédnuvši',
        },
      },
      plural: {
        present: {
          first: 'rozhlížíme',
          second: 'rozhlížíte',
          third: 'rozhlíží',
        },
        future: {
          first: 'rozhlédneme',
          second: 'rozhlédnete',
          third: 'rozhlédnou',
        },
        imperative: {
          second: 'rozhlédněme',
          third: 'rozhlédněte',
        },
        past: {
          first: { masculine: 'rozhlédli', feminine: 'rozhlédly', neutral: 'rozhlédla', },
          second: { masculine: 'rozhlédli', feminine: 'rozhlédly', neutral: 'rozhlédla', },
          third: { masculine: 'rozhlédli', feminine: 'rozhlédly', neutral: 'rozhlédla', },
        },
        passive: {
          first: { masculine: 'rozhlédnuti', feminine: 'rozhlédnuty', neutral: 'rozhlédnuta', },
          second: { masculine: 'rozhlédnuti', feminine: 'rozhlédnuty', neutral: 'rozhlédnuta', },
          third: { masculine: 'rozhlédnuti', feminine: 'rozhlédnuty', neutral: 'rozhlédnuta', },
        },
        participle: 'probudivše',
      },
    });
  }
}

export class Stisknout extends lang.Verb {
  static ID = `${lang.Verb.ID}.stisknout`;
  constructor() {
    super({
      verb: 'stisknout',
      infinitive: 'stisknout',
      singular: {
        present: {
          first: 'stiskám',
          second: 'stiskáš',
          third: 'stiská',
        },
        future: {
          first: 'stisknu',
          second: 'stiskneš',
          third: 'stiskne',
        },
        imperative: 'stiskni',
        past: {
          first: { masculine: 'stiskl', feminine: 'stiskla', neutral: 'stisklo', },
          second: { masculine: 'stiskl', feminine: 'stiskla', neutral: 'stisklo', },
          third: { masculine: 'stiskl', feminine: 'stiskla', neutral: 'stisklo', },
        },
        participle: {
          masculine: 'stisknuv',
          feminine: 'stisknuvši',
          neutral: 'stisknuvši',
        },
      },
      plural: {
        present: {
          first: 'stiskáme',
          second: 'stiskáte',
          third: 'stiskají',
        },
        future: {
          first: 'stiskneme',
          second: 'stisknete',
          third: 'stisknou',
        },
        imperative: {
          second: 'stiskněme',
          third: 'stiskněte',
        },
        past: {
          first: { masculine: 'stiskli', feminine: 'stiskly', neutral: 'stiskla', },
          second: { masculine: 'stiskli', feminine: 'stiskly', neutral: 'stiskla', },
          third: { masculine: 'stiskli', feminine: 'stiskly', neutral: 'stiskla', },
        },
        passive: {
          first: { masculine: 'stisknuti', feminine: 'stisknuty', neutral: 'stisknuta', },
          second: { masculine: 'stisknuti', feminine: 'stisknuty', neutral: 'stisknuta', },
          third: { masculine: 'stisknuti', feminine: 'stisknuty', neutral: 'stisknuta', },
        },
        participle: 'stisknuvše',
      },
    });
  }
}

export class Vypít extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.vypít`;
  constructor() {
    super({
      verb: 'vypít',
      infinitive: 'vypít',
      singular: {
        present: {
          first: 'vypiji',
          second: 'vypiješ',
          third: 'vypije',
        },
        future: {
          first: 'vypiji',
          second: 'vypiješ',
          third: 'vypije',
        },
        imperative: 'vypij',
        past: {
          first: { masculine: 'vypil', feminine: 'vypila', neutral: 'vypilo', },
          second: { masculine: 'vypil', feminine: 'vypila', neutral: 'vypilo', },
          third: { masculine: 'vypil', feminine: 'vypila', neutral: 'vypilo', },
        },
        passive: {
          first: { masculine: 'vypit', feminine: 'vypita', neutral: 'vypito', },
          second: { masculine: 'vypit', feminine: 'vypita', neutral: 'vypito', },
          third: { masculine: 'vypit', feminine: 'vypita', neutral: 'vypito', },
        },
        participle: {
          masculine: 'vypiv',
          feminine: 'vypivši',
          neutral: 'vypivše',
        },
      },
      plural: {
        present: {
          first: 'vypijeme',
          second: 'vypijete',
          third: 'vypijí',
        },
        future: {
          first: 'vypiji',
          second: 'vypijete',
          third: 'vypijí',
        },
        imperative: {
          second: 'vypijme',
          third: 'vypijte',
        },
        past: {
          first: { masculine: 'vypili', feminine: 'vypily', neutral: 'vypila', },
          second: { masculine: 'vypili', feminine: 'vypily', neutral: 'vypila', },
          third: { masculine: 'vypili', feminine: 'vypily', neutral: 'vypila', },
        },
        passive: {
          first: { masculine: 'vypiti', feminine: 'vypity', neutral: 'vypita', },
          second: { masculine: 'vypiti', feminine: 'vypity', neutral: 'vypita', },
          third: { masculine: 'vypiti', feminine: 'vypity', neutral: 'vypita', },
        },
        participle: 'vypivše',
      },
    });
  }
}

export class Ocitnout extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.ocitnout`;
  constructor() {
    super({
      verb: 'ocitnout',
      infinitive: 'ocitnout',
      singular: {
        present: {
          first: 'ocitnu',
          second: 'ocitneš',
          third: 'ocitne',
        },
        future: {
          first: 'ocitnu',
          second: 'ocitneš',
          third: 'ocitne',
        },
        imperative: 'ocitni',
        past: {
          first: { masculine: 'ocitl', feminine: 'ocitla', neutral: 'ocitlo', },
          second: { masculine: 'ocitl', feminine: 'ocitla', neutral: 'ocitlo', },
          third: { masculine: 'ocitl', feminine: 'ocitla', neutral: 'ocitlo', },
        },
        participle: {
          masculine: 'ocitnuv',
          feminine: 'ocitnuvši',
          neutral: 'ocitnuvši',
        },
      },
      plural: {
        present: {
          first: 'ocitneme',
          second: 'ocitnete',
          third: 'ocitnou',
        },
        future: {
          first: 'ocitneme',
          second: 'ocitnete',
          third: 'ocitnou',
        },
        imperative: {
          second: 'ocitněme',
          third: 'ocitněte',
        },
        past: {
          first: { masculine: 'ocitli', feminine: 'ocitly', neutral: 'ocitla', },
          second: { masculine: 'ocitli', feminine: 'ocitly', neutral: 'ocitla', },
          third: { masculine: 'ocitli', feminine: 'ocitly', neutral: 'ocitla', },
        },
        participle: 'ocitnuvše',
      },
    });
  }
}

export class Nalít extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.nalít`;
  constructor() {
    super({
      verb: 'nalít',
      infinitive: 'nalít',
      singular: {
        present: {
          first: 'naleji',
          second: 'naleješ',
          third: 'naleje',
        },
        future: {
          first: 'nabudu lít',
          second: 'nabudeš lít',
          third: 'nabude lít',
        },
        imperative: 'nalej',
        past: {
          first: { masculine: 'nalil', feminine: 'nalila', neutral: 'nalilo', },
          second: { masculine: 'nalil', feminine: 'nalila', neutral: 'nalilo', },
          third: { masculine: 'nalil', feminine: 'nalila', neutral: 'nalilo', },
        },
        passive: {
          first: { masculine: 'nalit', feminine: 'nalita', neutral: 'nalito', },
          second: { masculine: 'nalit', feminine: 'nalita', neutral: 'nalito', },
          third: { masculine: 'nalit', feminine: 'nalita', neutral: 'nalito', },
        },
        participle: {
          masculine: 'naleje',
          feminine: 'nalejíc',
          neutral: 'nalejíc',
        },
      },
      plural: {
        present: {
          first: 'nalejeme',
          second: 'nalejete',
          third: 'nalejí',
        },
        future: {
          first: 'nabudeme lít',
          second: 'nabudete lít',
          third: 'nabudou lít',
        },
        imperative: {
          second: 'nalejme',
          third: 'nalejte',
        },
        past: {
          first: { masculine: 'nalili', feminine: 'nalily', neutral: 'nalila', },
          second: { masculine: 'nalili', feminine: 'nalily', neutral: 'nalila', },
          third: { masculine: 'nalili', feminine: 'nalily', neutral: 'nalila', },
        },
        passive: {
          first: { masculine: 'naliti', feminine: 'nality', neutral: 'nalita', },
          second: { masculine: 'naliti', feminine: 'nality', neutral: 'nalita', },
          third: { masculine: 'naliti', feminine: 'nality', neutral: 'nalita', },
        },
        participle: 'nalejíce',
      },
    });
  }
}

export class Vypařit extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.vypařit`;
  constructor() {
    super({
      verb: 'vypařit',
      infinitive: 'vypařit',
      singular: {
        present: {
          first: 'vypařím',
          second: 'vypaříš',
          third: 'vypaří',
        },
        future: {
          first: 'vypařím',
          second: 'vypaříš',
          third: 'vypaří',
        },
        imperative: 'vypař',
        past: {
          first: { masculine: 'vypařil', feminine: 'vypařila', neutral: 'vypařilo', },
          second: { masculine: 'vypařil', feminine: 'vypařila', neutral: 'vypařilo', },
          third: { masculine: 'vypařil', feminine: 'vypařila', neutral: 'vypařilo', },
        },
      },
      plural: {
        present: {
          first: 'vypaříme',
          second: 'vypaříte',
          third: 'vypaří',
        },
        future: {
          first: 'vypaříme',
          second: 'vypaříte',
          third: 'vypaří',
        },
        imperative: {
          second: 'vypařme',
          third: 'vypařte',
        },
        past: {
          first: { masculine: 'vypařili', feminine: 'vypařily', neutral: 'vypařilo', },
          second: { masculine: 'vypařili', feminine: 'vypařily', neutral: 'vypařilo', },
          third: { masculine: 'vypařili', feminine: 'vypařily', neutral: 'vypařilo', },
        },
      },
    });
  }
}

export class Ucítit extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.ucítit`;
  constructor() {
    super({
      verb: 'ucítit',
      infinitive: 'ucítit',
      singular: {
        present: {
          first: 'ucítím',
          second: 'ucítíš',
          third: 'ucítí',
        },
        future: {
          first: 'budu ucítit',
          second: 'budeš ucítit',
          third: 'bude ucítit',
        },
        imperative: 'ciť',
        past: {
          first: { masculine: 'ucítil', feminine: 'ucítila', neutral: 'ucítilo', },
          second: { masculine: 'ucítil', feminine: 'ucítila', neutral: 'ucítilo', },
          third: { masculine: 'ucítil', feminine: 'ucítila', neutral: 'ucítilo', },
        },
        passive: {
          first: { masculine: 'ucítěn', feminine: 'ucítěna', neutral: 'ucítěno', },
          second: { masculine: 'ucítěn', feminine: 'ucítěna', neutral: 'ucítěno', },
          third: { masculine: 'ucítěn', feminine: 'ucítěna', neutral: 'ucítěno', },
        },
        participle: {
          masculine: 'ucítě',
          feminine: 'ucítíc',
          neutral: 'ucítíc',
        },
      },
      plural: {
        present: {
          first: 'ucítíme',
          second: 'ucítíte',
          third: 'ucítí',
        },
        future: {
          first: 'budeme ucítit',
          second: 'budete ucítit',
          third: 'budou ucítit',
        },
        imperative: {
          second: 'ciťme',
          third: 'ciťte',
        },
        past: {
          first: { masculine: 'ucítili', feminine: 'ucítily', neutral: 'ucítila', },
          second: { masculine: 'ucítili', feminine: 'ucítily', neutral: 'ucítila', },
          third: { masculine: 'ucítili', feminine: 'ucítily', neutral: 'ucítila', },
        },
        passive: {
          first: { masculine: 'ucítěni', feminine: 'ucítěny', neutral: 'ucítěna', },
          second: { masculine: 'ucítěni', feminine: 'ucítěny', neutral: 'ucítěna', },
          third: { masculine: 'ucítěni', feminine: 'ucítěny', neutral: 'ucítěna', },
        },
        participle: 'ucítíce',
      },
    });
  }
}
