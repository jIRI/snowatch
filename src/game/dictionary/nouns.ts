import {FrameworkConfiguration} from 'aurelia-framework';

import * as lang from 'snowatch/language-structures';

export function configureNouns(aurelia: FrameworkConfiguration) {
  aurelia.singleton(lang.LanguageStructure, ZemSingularCz);
  aurelia.singleton(lang.LanguageStructure, ZemPluralCz);
  aurelia.singleton(lang.LanguageStructure, PosledníŽena);
  aurelia.singleton(lang.LanguageStructure, PosledníMuž);
  aurelia.singleton(lang.LanguageStructure, Oltář);
  aurelia.singleton(lang.LanguageStructure, Police);
  aurelia.singleton(lang.LanguageStructure, Krystal);
  aurelia.singleton(lang.LanguageStructure, Meč);
  aurelia.singleton(lang.LanguageStructure, Zbroj);
  aurelia.singleton(lang.LanguageStructure, Prsten);
  aurelia.singleton(lang.LanguageStructure, Odpařovač);
  aurelia.singleton(lang.LanguageStructure, Láhev);
  aurelia.singleton(lang.LanguageStructure, TekutáDezinfekce);
  aurelia.singleton(lang.LanguageStructure, Pacient);
  aurelia.singleton(lang.LanguageStructure, Pacientka);
  aurelia.singleton(lang.LanguageStructure, Ruka);
  aurelia.singleton(lang.LanguageStructure, Ruce);

  aurelia.singleton(lang.LanguageStructure, Nádoba);
  aurelia.singleton(lang.LanguageStructure, ČiráNádoba);
  aurelia.singleton(lang.LanguageStructure, BíláNádoba);
  aurelia.singleton(lang.LanguageStructure, ČervenáNádoba);
  aurelia.singleton(lang.LanguageStructure, ModráNádoba);
  aurelia.singleton(lang.LanguageStructure, ZelenáNádoba);
  aurelia.singleton(lang.LanguageStructure, ŽlutáNádoba);
  aurelia.singleton(lang.LanguageStructure, ČernáNádoba);

  aurelia.singleton(lang.LanguageStructure, Podstavec);
  aurelia.singleton(lang.LanguageStructure, ČirýSloup);
  aurelia.singleton(lang.LanguageStructure, BílýSloup);
  aurelia.singleton(lang.LanguageStructure, ČervenýSloup);
  aurelia.singleton(lang.LanguageStructure, ModrýSloup);
  aurelia.singleton(lang.LanguageStructure, ZelenýSloup);
  aurelia.singleton(lang.LanguageStructure, ŽlutýSloup);
  aurelia.singleton(lang.LanguageStructure, ČernýSloup);

  aurelia.singleton(lang.LanguageStructure, PaprsekBíléhoSvětla);
  aurelia.singleton(lang.LanguageStructure, PaprsekČervenéhoSvětla);
  aurelia.singleton(lang.LanguageStructure, PaprsekModréhoSvětla);
  aurelia.singleton(lang.LanguageStructure, PaprsekZelenéhoSvětla);
  aurelia.singleton(lang.LanguageStructure, PaprsekŽlutéhoSvětla);
  aurelia.singleton(lang.LanguageStructure, PaprsekČernéhoSvětla);

  aurelia.singleton(lang.LanguageStructure, ČiráEsence);
  aurelia.singleton(lang.LanguageStructure, BíláEsence);
  aurelia.singleton(lang.LanguageStructure, ČervenáEsence);
  aurelia.singleton(lang.LanguageStructure, ModráEsence);
  aurelia.singleton(lang.LanguageStructure, ZelenáEsence);
  aurelia.singleton(lang.LanguageStructure, ŽlutáEsence);
  aurelia.singleton(lang.LanguageStructure, ČernáEsence);

  aurelia.singleton(lang.LanguageStructure, ServisníMístnost);
  aurelia.singleton(lang.LanguageStructure, Schodiště);
  aurelia.singleton(lang.LanguageStructure, Recepce);
  aurelia.singleton(lang.LanguageStructure, Laboratoř);
  aurelia.singleton(lang.LanguageStructure, Chodba);
  aurelia.singleton(lang.LanguageStructure, Balkón);
  aurelia.singleton(lang.LanguageStructure, Pokoj);
  aurelia.singleton(lang.LanguageStructure, Jídelna);
}

export class ZemSingularCz extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.zem-singular.cz`;
  constructor() {
    super({
      noun: 'ground.cz',
      gender: lang.Gender.feminine,
      genderSubtype: lang.GenderSubtype.animate,
      number: lang.Number.singular,
      nominative: 'zem',
      genitive: 'země',
      dative: 'zemi',
      accusative: 'zem',
      vocative: 'zemi',
      locative: 'zemi',
      instrumental: 'zemí',
    });
  }
}

export class ZemPluralCz extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.zem-plural.cz`;
  constructor() {
    super({
      noun: 'ground.cz',
      gender: lang.Gender.feminine,
      genderSubtype: lang.GenderSubtype.animate,
      number: lang.Number.plural,
      nominative: 'země',
      genitive: 'zemí',
      dative: 'zemím',
      accusative: 'země',
      vocative: 'země',
      locative: 'zemích',
      instrumental: 'zeměmi',
    });
  }
}

export class PosledníMuž extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.poslední-muž`;
  constructor() {
    super({
      noun: 'poslední-muž',
      gender: lang.Gender.masculine,
      genderSubtype: lang.GenderSubtype.animate,
      number: lang.Number.singular,
      nominative: 'Poslední',
      genitive: 'Posledního',
      dative: 'Poslednímu',
      accusative: 'Posledního',
      vocative: 'Poslední',
      locative: 'Posledním',
      instrumental: 'Posledním',
    });
  }
}

export class PosledníŽena extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.poslední-žena`;
  constructor() {
    super({
      noun: 'poslední-žena',
      gender: lang.Gender.feminine,
      number: lang.Number.singular,
      nominative: 'Poslední',
      genitive: 'Poslední',
      dative: 'Poslední',
      accusative: 'Poslední',
      vocative: 'Poslední',
      locative: 'Poslední',
      instrumental: 'Poslední',
    });
  }
}

export class Pacient extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.pacient`;
  constructor() {
    super({
      noun: 'pacient',
      gender: lang.Gender.masculine,
      genderSubtype: lang.GenderSubtype.animate,
      number: lang.Number.singular,
      nominative: 'pacient',
      genitive: 'pacienta',
      dative: 'pacientovi',
      accusative: 'pacienta',
      vocative: 'paciente',
      locative: 'pacientovi',
      instrumental: 'pacientem',
    });
  }
}

export class Pacientka extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.pacientka`;
  constructor() {
    super({
      noun: 'pacientka',
      gender: lang.Gender.feminine,
      genderSubtype: lang.GenderSubtype.animate,
      number: lang.Number.singular,
      nominative: 'pacientka',
      genitive: 'pacientky',
      dative: 'pacientce',
      accusative: 'pacientku',
      vocative: 'pacientko',
      locative: 'pacientce',
      instrumental: 'pacientkou',
    });
  }
}

export class Oltář extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.oltář`;
  constructor() {
    super({
      noun: 'oltář',
      gender: lang.Gender.masculine,
      genderSubtype: lang.GenderSubtype.inanimate,
      number: lang.Number.singular,
      nominative: 'oltář',
      genitive: 'oltáře',
      dative: 'oltáři',
      accusative: 'oltář',
      vocative: 'oltáři',
      locative: 'oltáři',
      instrumental: 'oltářem',
    });
  }
}

export class Police extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.police`;
  constructor() {
    super({
      noun: 'police',
      gender: lang.Gender.feminine,
      number: lang.Number.singular,
      nominative: 'police',
      genitive: 'police',
      dative: 'polici',
      accusative: 'polici',
      vocative: 'police',
      locative: 'polici',
      instrumental: 'policí',
    });
  }
}

export class Krystal extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.krystal`;
  constructor() {
    super({
      noun: 'krystal',
      gender: lang.Gender.masculine,
      genderSubtype: lang.GenderSubtype.inanimate,
      number: lang.Number.singular,
      nominative: 'krystal',
      genitive: 'krystalu',
      dative: 'krystalu',
      accusative: 'krystal',
      vocative: 'krystale',
      locative: 'krystalu',
      instrumental: 'krystalem',
    });
  }
}

export class Meč extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.meč`;
  constructor() {
    super({
      noun: 'meč',
      gender: lang.Gender.masculine,
      genderSubtype: lang.GenderSubtype.inanimate,
      number: lang.Number.singular,
      nominative: 'meč',
      genitive: 'meče',
      dative: 'meči',
      accusative: 'meč',
      vocative: 'meči',
      locative: 'meči',
      instrumental: 'mečem',
    });
  }
}

export class Zbroj extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.zbroj`;
  constructor() {
    super({
      noun: 'zbroj',
      gender: lang.Gender.masculine,
      genderSubtype: lang.GenderSubtype.inanimate,
      number: lang.Number.singular,
      nominative: 'zbroj',
      genitive: 'zbroje',
      dative: 'zbroji',
      accusative: 'zbroj',
      vocative: 'zbroji',
      locative: 'zbroji',
      instrumental: 'zbrojí',
    });
  }
}

export class Prsten extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.prsten`;
  constructor() {
    super({
      noun: 'prsten',
      gender: lang.Gender.masculine,
      genderSubtype: lang.GenderSubtype.inanimate,
      number: lang.Number.singular,
      nominative: 'prsten',
      genitive: 'prstenu',
      dative: 'prstenu',
      accusative: 'prsten',
      vocative: 'prstene',
      locative: 'prstenu',
      instrumental: 'prstenem',
    });
  }
}

export class Odpařovač extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.odpařovač`;
  constructor() {
    super({
      noun: 'odpařovač',
      gender: lang.Gender.masculine,
      genderSubtype: lang.GenderSubtype.inanimate,
      number: lang.Number.singular,
      nominative: 'odpařovač',
      genitive: 'odpařovače',
      dative: 'odpařovači',
      accusative: 'odpačovači',
      vocative: 'odpařovači',
      locative: 'odpařovači',
      instrumental: 'odpařovačem',
    });
  }
}

export class Nádoba extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.nádoba`;
  constructor() {
    super({
      noun: 'nádoba',
      gender: lang.Gender.feminine,
      number: lang.Number.singular,
      nominative: 'nádoba',
      genitive: 'nádoby',
      dative: 'nádobě',
      accusative: 'nádobu',
      vocative: 'nádobo',
      locative: 'nádobě',
      instrumental: 'nádobou',
    });
  }
}

export class ČiráNádoba extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.čirá-nádoba`;
  constructor() {
    super({
      noun: 'platinová nádoba',
      gender: lang.Gender.feminine,
      number: lang.Number.singular,
      nominative: 'platinová nádoba',
      genitive: 'platinové nádoby',
      dative: 'platinové nádobě',
      accusative: 'platinovou nádobu',
      vocative: 'platinová nádobo',
      locative: 'platinové nádobě',
      instrumental: 'platinovou nádobou',
    });
  }
}

export class BíláNádoba extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.bílá-nádoba`;
  constructor() {
    super({
      noun: 'zlatá nádoba',
      gender: lang.Gender.feminine,
      number: lang.Number.singular,
      nominative: 'zlatá nádoba',
      genitive: 'zlaté nádoby',
      dative: 'zlaté nádobě',
      accusative: 'zlatou nádobu',
      vocative: 'zlatá nádobo',
      locative: 'zlaté nádobě',
      instrumental: 'zlatou nádobou',
    });
  }
}

export class ČervenáNádoba extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.červená-nádoba`;
  constructor() {
    super({
      noun: 'železná nádoba',
      gender: lang.Gender.feminine,
      number: lang.Number.singular,
      nominative: 'železná nádoba',
      genitive: 'železné nádoby',
      dative: 'železné nádobě',
      accusative: 'železnou nádobu',
      vocative: 'červená nádobo',
      locative: 'železné nádobě',
      instrumental: 'železnou nádobou',
    });
  }
}

export class ModráNádoba extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.modrá-nádoba`;
  constructor() {
    super({
      noun: 'stříbrná nádoba',
      gender: lang.Gender.feminine,
      number: lang.Number.singular,
      nominative: 'stříbrná nádoba',
      genitive: 'stříbrné nádoby',
      dative: 'stříbrné nádobě',
      accusative: 'stříbrnou nádobu',
      vocative: 'stříbrná nádobo',
      locative: 'stříbrné nádobě',
      instrumental: 'stříbrnou nádobou',
    });
  }
}

export class ZelenáNádoba extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.zelená-nádoba`;
  constructor() {
    super({
      noun: 'cínová nádoba',
      gender: lang.Gender.feminine,
      number: lang.Number.singular,
      nominative: 'cínová nádoba',
      genitive: 'cínové nádoby',
      dative: 'cínové nádobě',
      accusative: 'cínovou nádobu',
      vocative: 'cínová nádobo',
      locative: 'cínové nádobě',
      instrumental: 'cínovou nádobou',
    });
  }
}

export class ŽlutáNádoba extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.žlutá-nádoba`;
  constructor() {
    super({
      noun: 'olověná nádoba',
      gender: lang.Gender.feminine,
      number: lang.Number.singular,
      nominative: 'olověná nádoba',
      genitive: 'olověné nádoby',
      dative: 'olověné nádobě',
      accusative: 'olověnou nádobu',
      vocative: 'olověná nádobo',
      locative: 'olověné nádobě',
      instrumental: 'olověnou nádobou',
    });
  }
}

export class ČernáNádoba extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.černá-nádoba`;
  constructor() {
    super({
      noun: 'měděná nádoba',
      gender: lang.Gender.feminine,
      number: lang.Number.singular,
      nominative: 'měděná nádoba',
      genitive: 'měděné nádoby',
      dative: 'měděné nádobě',
      accusative: 'měděnou nádobu',
      vocative: 'měděná nádobo',
      locative: 'měděné nádobě',
      instrumental: 'měděnou nádobou',
    });
  }
}

export class Podstavec extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.podstavec`;
  constructor() {
    super({
      noun: 'podstavec',
      gender: lang.Gender.masculine,
      number: lang.Number.singular,
      nominative: 'podstavec',
      genitive: 'podstavce',
      dative: 'podstavci',
      accusative: 'podstavec',
      vocative: 'podstavče',
      locative: 'podstavci',
      instrumental: 'podstavcem',
    });
  }
}

export class ČirýSloup extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.čirý-sloup`;
  constructor() {
    super({
      noun: 'křišťálový sloup',
      gender: lang.Gender.masculine,
      number: lang.Number.singular,
      nominative: 'křišťálový podstavec',
      genitive: 'křišťálového podstavce',
      dative: 'křišťálovému podstavci',
      accusative: 'křišťálový podstavec',
      vocative: 'křišťálový podstavče',
      locative: 'křišťálovém podstavci',
      instrumental: 'křišťálovým podstavcem',
    });
  }
}

export class BílýSloup extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.bílý-sloup`;
  constructor() {
    super({
      noun: 'bílý sloup',
      gender: lang.Gender.masculine,
      number: lang.Number.singular,
      nominative: 'bílý podstavec',
      genitive: 'bílého podstavce',
      dative: 'bílému podstavci',
      accusative: 'bílý podstavec',
      vocative: 'bílý podstavče',
      locative: 'bílém podstavci',
      instrumental: 'bílým podstavcem',
    });
  }
}

export class ČervenýSloup extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.červený-sloup`;
  constructor() {
    super({
      noun: 'červený sloup',
      gender: lang.Gender.masculine,
      number: lang.Number.singular,
      nominative: 'červený podstavec',
      genitive: 'červeného podstavce',
      dative: 'červenému podstavci',
      accusative: 'červený podstavec',
      vocative: 'červený podstavče',
      locative: 'červeném podstavci',
      instrumental: 'červeným podstavcem',
    });
  }
}

export class ModrýSloup extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.modrý-sloup`;
  constructor() {
    super({
      noun: 'modrý sloup',
      gender: lang.Gender.masculine,
      number: lang.Number.singular,
      nominative: 'modrý podstavec',
      genitive: 'modrého podstavce',
      dative: 'modrému podstavci',
      accusative: 'modrý podstavec',
      vocative: 'modrý podstavče',
      locative: 'modrém podstavci',
      instrumental: 'modrým podstavcem',
    });
  }
}

export class ZelenýSloup extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.zelený-sloup`;
  constructor() {
    super({
      noun: 'zelený sloup',
      gender: lang.Gender.masculine,
      number: lang.Number.singular,
      nominative: 'zelený podstavec',
      genitive: 'zeleného podstavce',
      dative: 'zelenému podstavci',
      accusative: 'zelený podstavec',
      vocative: 'zelený podstavče',
      locative: 'zeleném podstavci',
      instrumental: 'zeleným podstavcem',
    });
  }
}

export class ŽlutýSloup extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.žlutý-sloup`;
  constructor() {
    super({
      noun: 'žlutý sloup',
      gender: lang.Gender.masculine,
      number: lang.Number.singular,
      nominative: 'žlutý podstavec',
      genitive: 'žlutého podstavce',
      dative: 'žlutému podstavci',
      accusative: 'žlutý podstavec',
      vocative: 'žlutý podstavče',
      locative: 'žlutém podstavci',
      instrumental: 'žlutým podstavcem',
    });
  }
}

export class ČernýSloup extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.černý-sloup`;
  constructor() {
    super({
      noun: 'černý sloup',
      gender: lang.Gender.masculine,
      number: lang.Number.singular,
      nominative: 'černý podstavec',
      genitive: 'černého podstavce',
      dative: 'černému podstavci',
      accusative: 'černý podstavec',
      vocative: 'černý podstavče',
      locative: 'černém podstavci',
      instrumental: 'černým podstavcem',
    });
  }
}

export class PaprsekBíléhoSvětla extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.paprsek-bílého-světla`;
  constructor() {
    super({
      noun: 'paprsek bílého světla',
      gender: lang.Gender.masculine,
      number: lang.Number.singular,
      nominative: 'paprsek bílého světla',
      genitive: 'paprsku bílého světla',
      dative: 'paprsku bílého světla',
      accusative: 'paprsek bílý světla',
      vocative: 'paprsku bílého světla',
      locative: 'paprsku bílého světla',
      instrumental: 'paprskem bílého světla',
    });
  }
}

export class PaprsekČervenéhoSvětla extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.paprsek-červeného-světla`;
  constructor() {
    super({
      noun: 'paprsek červeného světla',
      gender: lang.Gender.masculine,
      number: lang.Number.singular,
      nominative: 'paprsek červeného světla',
      genitive: 'paprsku červeného světla',
      dative: 'paprsku červeného světla',
      accusative: 'paprsek červený světla',
      vocative: 'paprsku červeného světla',
      locative: 'paprsku červeného světla',
      instrumental: 'paprskem červeného světla',
    });
  }
}

export class PaprsekModréhoSvětla extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.paprsek-modrého-světla`;
  constructor() {
    super({
      noun: 'paprsek modrého světla',
      gender: lang.Gender.masculine,
      number: lang.Number.singular,
      nominative: 'paprsek modrého světla',
      genitive: 'paprsku modrého světla',
      dative: 'paprsku modrého světla',
      accusative: 'paprsek modrý světla',
      vocative: 'paprsku modrého světla',
      locative: 'paprsku modrého světla',
      instrumental: 'paprskem modrého světla',
    });
  }
}

export class PaprsekZelenéhoSvětla extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.paprsek-zeleného-světla`;
  constructor() {
    super({
      noun: 'paprsek zeleného světla',
      gender: lang.Gender.masculine,
      number: lang.Number.singular,
      nominative: 'paprsek zeleného světla',
      genitive: 'paprsku zeleného světla',
      dative: 'paprsku zeleného světla',
      accusative: 'paprsek zelený světla',
      vocative: 'paprsku zeleného světla',
      locative: 'paprsku zeleného světla',
      instrumental: 'paprskem zeleného světla',
    });
  }
}

export class PaprsekŽlutéhoSvětla extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.paprsek-žlutého-světla`;
  constructor() {
    super({
      noun: 'paprsek žlutého světla',
      gender: lang.Gender.masculine,
      number: lang.Number.singular,
      nominative: 'paprsek žlutého světla',
      genitive: 'paprsku žlutého světla',
      dative: 'paprsku žlutého světla',
      accusative: 'paprsek žlutý světla',
      vocative: 'paprsku žlutého světla',
      locative: 'paprsku žlutého světla',
      instrumental: 'paprskem žlutého světla',
    });
  }
}

export class PaprsekČernéhoSvětla extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.paprsek-černého-světla`;
  constructor() {
    super({
      noun: 'paprsek temnoty',
      gender: lang.Gender.masculine,
      number: lang.Number.singular,
      nominative: 'paprsek temnoty',
      genitive: 'paprsku temnoty',
      dative: 'paprsku temnoty',
      accusative: 'paprsek temnoty',
      vocative: 'paprsku temnoty',
      locative: 'paprsku temnoty',
      instrumental: 'paprskem temnoty',
    });
  }
}

export class ČiráEsence extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.čirá-essence`;
  constructor() {
    super({
      noun: 'čirá tekutina',
      gender: lang.Gender.feminine,
      number: lang.Number.singular,
      nominative: 'čirá tekutina',
      genitive: 'čiré tekutiny',
      dative: 'čiré tekutině',
      accusative: 'čirou tekutinu',
      vocative: 'čirá tekutino',
      locative: 'čiré tekutině',
      instrumental: 'čirou tekutinou',
    });
  }
}

export class BíláEsence extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.bílá-essence`;
  constructor() {
    super({
      noun: 'bílá tekutina',
      gender: lang.Gender.feminine,
      number: lang.Number.singular,
      nominative: 'bílá tekutina',
      genitive: 'bílé tekutiny',
      dative: 'bílé tekutině',
      accusative: 'bílou tekutinu',
      vocative: 'bílá tekutino',
      locative: 'bílé tekutině',
      instrumental: 'bílou tekutinou',
    });
  }
}

export class ČervenáEsence extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.červená-essence`;
  constructor() {
    super({
      noun: 'červená tekutina',
      gender: lang.Gender.feminine,
      number: lang.Number.singular,
      nominative: 'červená tekutina',
      genitive: 'červené tekutiny',
      dative: 'červené tekutině',
      accusative: 'červenou tekutinu',
      vocative: 'červená tekutino',
      locative: 'červené tekutině',
      instrumental: 'červenou tekutinou',
    });
  }
}

export class ModráEsence extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.modrá-essence`;
  constructor() {
    super({
      noun: 'modrá tekutina',
      gender: lang.Gender.feminine,
      number: lang.Number.singular,
      nominative: 'modrá tekutina',
      genitive: 'modré tekutiny',
      dative: 'modré tekutině',
      accusative: 'modrou tekutinu',
      vocative: 'modrá tekutino',
      locative: 'modré tekutině',
      instrumental: 'modrou tekutinou',
    });
  }
}

export class ZelenáEsence extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.zelená-essence`;
  constructor() {
    super({
      noun: 'zelená tekutina',
      gender: lang.Gender.feminine,
      number: lang.Number.singular,
      nominative: 'zelená tekutina',
      genitive: 'zelené tekutiny',
      dative: 'zelené tekutině',
      accusative: 'zelenou tekutinu',
      vocative: 'zelená tekutino',
      locative: 'zelené tekutině',
      instrumental: 'zelenou tekutinou',
    });
  }
}

export class ŽlutáEsence extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.žlutá-essence`;
  constructor() {
    super({
      noun: 'žlutá tekutina',
      gender: lang.Gender.feminine,
      number: lang.Number.singular,
      nominative: 'žlutá tekutina',
      genitive: 'žluté tekutiny',
      dative: 'žluté tekutině',
      accusative: 'žlutou tekutinu',
      vocative: 'žlutá tekutino',
      locative: 'žluté tekutině',
      instrumental: 'žlutou tekutinou',
    });
  }
}

export class ČernáEsence extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.černá-essence`;
  constructor() {
    super({
      noun: 'černá tekutina',
      gender: lang.Gender.feminine,
      number: lang.Number.singular,
      nominative: 'černá tekutina',
      genitive: 'černé tekutiny',
      dative: 'černé tekutině',
      accusative: 'černou tekutinu',
      vocative: 'černá tekutino',
      locative: 'černé tekutině',
      instrumental: 'černou tekutinou',
    });
  }
}

export class Láhev extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.lahev`;
  constructor() {
    super({
      noun: 'lahev',
      gender: lang.Gender.feminine,
      genderSubtype: lang.GenderSubtype.animate,
      number: lang.Number.singular,
      nominative: 'lahev',
      genitive: 'lahve',
      dative: 'lahvi',
      accusative: 'lahev',
      vocative: 'lahvi',
      locative: 'lahvi',
      instrumental: 'lahví',
    });
  }
}

export class TekutáDezinfekce extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.tekutá-dezinfekce`;
  constructor() {
    super({
      noun: 'tekutá dezinfekce',
      gender: lang.Gender.feminine,
      genderSubtype: lang.GenderSubtype.animate,
      number: lang.Number.singular,
      nominative: 'tekutá dezinfekce',
      genitive: 'tekuté dezinfekce',
      dative: 'tekuté dezinfekci',
      accusative: 'tekutou dezinfekci',
      vocative: 'tekutá dezinfekce',
      locative: 'tekuté dezinfekci',
      instrumental: 'tekutou dezinfekcí',
    });
  }
}

export class ServisníMístnost extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.servisní-místnost`;
  constructor() {
    super({
      noun: 'servisní místnost',
      gender: lang.Gender.feminine,
      genderSubtype: lang.GenderSubtype.animate,
      number: lang.Number.singular,
      nominative: 'servisní místnost',
      genitive: 'servisní místnosti',
      dative: 'servisní místnosti',
      accusative: 'servisní místnost',
      vocative: 'servisní místnosti',
      locative: 'servisní místnosti',
      instrumental: 'servisní místností',
    });
  }
}

// Source file: 011-165.html
export class Chodba extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.chodba`;
  constructor() {
    super({
      noun: 'chodba',
      gender: lang.Gender.feminine,
      genderSubtype: lang.GenderSubtype.animate,
      number: lang.Number.singular,
      nominative: 'chodba',
      genitive: 'chodby',
      dative: 'chodbě',
      accusative: 'chodbu',
      vocative: 'chodbo',
      locative: 'chodbě',
      instrumental: 'chodbou',
    });
  }
}

// Source file: 063-053.html
export class Schodiště extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.schodiště`;
  constructor() {
    super({
      noun: 'schodiště',
      gender: lang.Gender.neutral,
      genderSubtype: lang.GenderSubtype.animate,
      number: lang.Number.singular,
      nominative: 'schodiště',
      genitive: 'schodiště',
      dative: 'schodišti',
      accusative: 'schodiště',
      vocative: 'schodiště',
      locative: 'schodišti',
      instrumental: 'schodištěm',
    });
  }
}

// Source file: 059-168.html
export class Recepce extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.recepce`;
  constructor() {
    super({
      noun: 'recepce',
      gender: lang.Gender.feminine,
      genderSubtype: lang.GenderSubtype.animate,
      number: lang.Number.singular,
      nominative: 'recepce',
      genitive: 'recepce',
      dative: 'recepci',
      accusative: 'recepci',
      vocative: 'recepce',
      locative: 'recepci',
      instrumental: 'recepcí',
    });
  }
}

// Source file: 036-035.html
export class Laboratoř extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.laboratoř`;
  constructor() {
    super({
      noun: 'laboratoř',
      gender: lang.Gender.feminine,
      genderSubtype: lang.GenderSubtype.animate,
      number: lang.Number.singular,
      nominative: 'laboratoř',
      genitive: 'laboratoře',
      dative: 'laboratoři',
      accusative: 'laboratoř',
      vocative: 'laboratoři',
      locative: 'laboratoři',
      instrumental: 'laboratoří',
    });
  }
}

// Source file: 005-063.html
export class Balkón extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.balkón`;
  constructor() {
    super({
      noun: 'balkón',
      gender: lang.Gender.masculine,
      genderSubtype: lang.GenderSubtype.inanimate,
      number: lang.Number.singular,
      nominative: 'balkón',
      genitive: 'balkónu',
      dative: 'balkónu',
      accusative: 'balkón',
      vocative: 'balkóne',
      locative: 'balkónu',
      instrumental: 'balkónem',
    });
  }
}

// Source file: 054-055.html
export class Pokoj extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.pokoj`;
  constructor() {
    super({
      noun: 'pokoj',
      gender: lang.Gender.masculine,
      genderSubtype: lang.GenderSubtype.inanimate,
      number: lang.Number.singular,
      nominative: 'pokoj',
      genitive: 'pokoje',
      dative: 'pokoji',
      accusative: 'pokoj',
      vocative: 'pokoji',
      locative: 'pokoji',
      instrumental: 'pokojem',
    });
  }
}

export class Jídelna extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.jídelna`;
  constructor() {
    super({
      noun: 'jídelna',
      gender: lang.Gender.feminine,
      number: lang.Number.singular,
      nominative: 'jídelna',
      genitive: 'jídelny',
      dative: 'jídelně',
      accusative: 'jídelno',
      vocative: 'jídelno',
      locative: 'jídelně',
      instrumental: 'jídelnou',
    });
  }
}

export class Ruka extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.ruka`;
  constructor() {
    super({
      noun: 'ruka',
      gender: lang.Gender.feminine,
      genderSubtype: lang.GenderSubtype.animate,
      number: lang.Number.singular,
      nominative: 'ruka',
      genitive: 'ruky',
      dative: 'ruce',
      accusative: 'ruku',
      vocative: 'ruko',
      locative: 'ruce',
      instrumental: 'rukou',
    });
  }
}

export class Ruce extends lang.Noun {
  public static readonly ID = `${lang.Noun.ID}.ruce`;
  constructor() {
    super({
      noun: 'ruce',
      gender: lang.Gender.feminine,
      genderSubtype: lang.GenderSubtype.animate,
      number: lang.Number.plural,
      nominative: 'ruce',
      genitive: 'ruk',
      dative: 'rukám',
      accusative: 'ruce',
      vocative: 'ruce',
      locative: 'rukách',
      instrumental: 'rukama',
    });
  }
}

