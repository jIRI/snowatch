import {FrameworkConfiguration} from 'aurelia-framework';

import * as lang from 'snowatch/language-structures';

export function configurePronouns(aurelia: FrameworkConfiguration) {
  aurelia.singleton(lang.LanguageStructure, Personal);
  aurelia.singleton(lang.LanguageStructure, Všechen);
}

export class Personal extends lang.Pronoun {
  public static readonly ID = `${lang.Pronoun.ID}.personal`;
  constructor() {
    super({
      pronoun: 'personal',
      singular: {
        nominative: { first: "já", second: "ty", third: { masculine: "on", feminine: "ona", neutral: "ono", }, },
        genitive: { first: "mě", second: "tě", third: { masculine: '', feminine: '', neutral: '', }, },
        dative: { first: '', second: '', third: { masculine: '', feminine: '', neutral: '', }, },
        accusative: { first: '', second: '', third: { masculine:  {animate: '', inanimate: ''}, feminine: '', neutral: '', }, },
        vocative: { first: '', second: '', third: { masculine: '', feminine: '', neutral: '', }, },
        locative: { first: '', second: '', third: { masculine: '', feminine: '', neutral: '', }, },
        instrumental: { first: '', second: '', third: { masculine: '', feminine: '', neutral: '', }, },
      },
      plural: {
        nominative: { first: '', second: '', third: { masculine: {animate: '', inanimate: ''}, feminine: '', neutral: '', }, },
        genitive: { first: '', second: '', third: { masculine: '', feminine: '', neutral: '', }, },
        dative: { first: '', second: '', third: { masculine: '', feminine: '', neutral: '', }, },
        accusative: { first: '', second: '', third: { masculine: '', feminine: '', neutral: '', }, },
        vocative: { first: '', second: '', third: { masculine: '', feminine: '', neutral: '', }, },
        locative: { first: '', second: '', third: { masculine: '', feminine: '', neutral: '', }, },
        instrumental: { first: '', second: '', third: { masculine: '', feminine: '', neutral: '', }, },
      }
    });
  }
}

export class Všechen extends lang.Pronoun {
  public static readonly ID = `${lang.Pronoun.ID}.všechen`;
  constructor() {
    super({
      pronoun: 'všechno',
      singular: {
        nominative: { masculine: "všechen", feminine: "všechna", neutral: "všechno", },
        genitive: { masculine: "všeho", feminine: "vší", neutral: "všeho", },
        dative: { masculine: "všemu", feminine: "vší", neutral: "všemu", },
        accusative: { masculine: { animate: "všeho", inanimate: "všechen" }, feminine: "všechnu", neutral: "všechno", },
        vocative: { masculine: "všechen", feminine: "všechna", neutral: "všechno", },
        locative: { masculine: "všem", feminine: "vší", neutral: "všem", },
        instrumental: { masculine: "vším", feminine: "vší", neutral: "vším", },
      },
      plural: {
        nominative: { masculine: { animate: "všichni", inanimate: "všechny" }, feminine: "všechny", neutral: "všechna", },
        genitive: { masculine: "všech", feminine: "všech", neutral: "všech", },
        dative: { masculine: "všem", feminine: "všem", neutral: "všem", },
        accusative: { masculine: "všechny", feminine: "všechny", neutral: "všechna", },
        vocative: { masculine: { animate: "všichni", inanimate: "všechny" }, feminine: "všechny", neutral: "všechna", },
        locative: { masculine: "všech", feminine: "všech", neutral: "všech", },
        instrumental: { masculine: "všemi", feminine: "všemi", neutral: "všemi", },
      }
    });
  }
}
