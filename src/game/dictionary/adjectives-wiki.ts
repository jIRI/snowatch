import {FrameworkConfiguration} from 'aurelia-framework';

import * as lang from 'snowatch/language-structures';

export function configureAdjectivesWiki(aurelia: FrameworkConfiguration) {
  aurelia.singleton(lang.LanguageStructure, Vysoký);
  aurelia.singleton(lang.LanguageStructure, Hubený);
  aurelia.singleton(lang.LanguageStructure, Malý);
}

// Source file: 025-095.html
export class Vysoký extends lang.Adjective {
  public static readonly ID = `${lang.Adjective.ID}.vysoký`;
  constructor() {
    super({
      adjective: 'vysoký',
      singular: {
        masculine: {
          nominative: 'vysoký',
          genitive: 'vysokého',
          dative: 'vysokému',
          accusative: { animate: 'vysokého', inanimate: 'vysoký', },
          vocative: 'vysoký',
          locative: 'vysokém',
          instrumental: 'vysokým',
        },
        feminine: {
          nominative: 'vysoká',
          genitive: 'vysoké',
          dative: 'vysoké',
          accusative: 'vysokou',
          vocative: 'vysoká',
          locative: 'vysoké',
          instrumental: 'vysokou',
        },
        neutral: {
          nominative: 'vysoké',
          genitive: 'vysokého',
          dative: 'vysokému',
          accusative: 'vysoké',
          vocative: 'vysoké',
          locative: 'vysokém',
          instrumental: 'vysokým',
        },
      },
      plural: {
        masculine: {
          nominative: { animate: 'vysocí', inanimate: 'vysoké', },
          genitive: 'vysokých',
          dative: 'vysokým',
          accusative: 'vysoké',
          vocative: { animate: 'vysocí', inanimate: 'vysoké', },
          locative: 'vysokých',
          instrumental: 'vysokými',
        },
        feminine: {
          nominative: 'vysoké',
          genitive: 'vysokých',
          dative: 'vysokým',
          accusative: 'vysoké',
          vocative: 'vysoké',
          locative: 'vysokých',
          instrumental: 'vysokými',
        },
        neutral: {
          nominative: 'vysoká',
          genitive: 'vysokých',
          dative: 'vysokým',
          accusative: 'vysoká',
          vocative: 'vysoká',
          locative: 'vysokých',
          instrumental: 'vysokými',
        },
      },
    });
  }
}

// Source file: 008-067.html
export class Hubený extends lang.Adjective {
  public static readonly ID = `${lang.Adjective.ID}.hubený`;
  constructor() {
    super({
      adjective: 'hubený',
      singular: {
        masculine: {
          nominative: 'hubený',
          genitive: 'hubeného',
          dative: 'hubenému',
          accusative: { animate: 'hubeného', inanimate: 'hubený', },
          vocative: 'hubený',
          locative: 'hubeném',
          instrumental: 'hubeným',
        },
        feminine: {
          nominative: 'hubená',
          genitive: 'hubené',
          dative: 'hubené',
          accusative: 'hubenou',
          vocative: 'hubená',
          locative: 'hubené',
          instrumental: 'hubenou',
        },
        neutral: {
          nominative: 'hubené',
          genitive: 'hubeného',
          dative: 'hubenému',
          accusative: 'hubené',
          vocative: 'hubené',
          locative: 'hubeném',
          instrumental: 'hubeným',
        },
      },
      plural: {
        masculine: {
          nominative: { animate: 'hubení', inanimate: 'hubené', },
          genitive: 'hubených',
          dative: 'hubeným',
          accusative: 'hubené',
          vocative: { animate: 'hubení', inanimate: 'hubené', },
          locative: 'hubených',
          instrumental: 'hubenými',
        },
        feminine: {
          nominative: 'hubené',
          genitive: 'hubených',
          dative: 'hubeným',
          accusative: 'hubené',
          vocative: 'hubené',
          locative: 'hubených',
          instrumental: 'hubenými',
        },
        neutral: {
          nominative: 'hubená',
          genitive: 'hubených',
          dative: 'hubeným',
          accusative: 'hubená',
          vocative: 'hubená',
          locative: 'hubených',
          instrumental: 'hubenými',
        },
      },
    });
  }
}

export class Malý extends lang.Adjective {
  public static readonly ID = `${lang.Adjective.ID}.malý`;
  constructor() {
    super({
      adjective: 'malý',
      singular: {
        masculine: {
          nominative: 'malý',
          genitive: 'malého',
          dative: 'malému',
          accusative: { animate: 'malého', inanimate: 'malý', },
          vocative: 'malý',
          locative: 'malém',
          instrumental: 'malým',
        },
        feminine: {
          nominative: 'malá',
          genitive: 'malé',
          dative: 'malé',
          accusative: 'malou',
          vocative: 'malá',
          locative: 'malé',
          instrumental: 'malou',
        },
        neutral: {
          nominative: 'malé',
          genitive: 'malého',
          dative: 'malému',
          accusative: 'malé',
          vocative: 'malé',
          locative: 'malém',
          instrumental: 'malým',
        },
      },
      plural: {
        masculine: {
          nominative: { animate: 'malí', inanimate: 'malé', },
          genitive: 'malých',
          dative: 'malým',
          accusative: 'malé',
          vocative: { animate: 'malí', inanimate: 'malé', },
          locative: 'malých',
          instrumental: 'malými',
        },
        feminine: {
          nominative: 'malé',
          genitive: 'malých',
          dative: 'malým',
          accusative: 'malé',
          vocative: 'malé',
          locative: 'malých',
          instrumental: 'malými',
        },
        neutral: {
          nominative: 'malá',
          genitive: 'malých',
          dative: 'malým',
          accusative: 'malá',
          vocative: 'malá',
          locative: 'malých',
          instrumental: 'malými',
        },
      },
    });
  }
}