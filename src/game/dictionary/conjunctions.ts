import {FrameworkConfiguration} from 'aurelia-framework';

import * as lang from 'snowatch/language-structures';

export function configureConjunctions(aurelia: FrameworkConfiguration) {
}


export class And extends lang.Conjunction {
  public static readonly ID = `${lang.Conjunction.ID}.and`;
  constructor() {
    super({
      conjunction: 'and',
    });
  }
}

