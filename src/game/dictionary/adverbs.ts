import {FrameworkConfiguration} from 'aurelia-framework';

import * as lang from 'snowatch/language-structures';

export function configureAdverbs(aurelia: FrameworkConfiguration) {
  aurelia.singleton(lang.LanguageStructure, Neutrálně);
  aurelia.singleton(lang.LanguageStructure, Silně);
  aurelia.singleton(lang.LanguageStructure, Slabě);
  aurelia.singleton(lang.LanguageStructure, Rychle);
  aurelia.singleton(lang.LanguageStructure, Tiše);
  aurelia.singleton(lang.LanguageStructure, Šeptavě);
  aurelia.singleton(lang.LanguageStructure, Pomalu);
  aurelia.singleton(lang.LanguageStructure, Váhavě);
  aurelia.singleton(lang.LanguageStructure, Opatrně);
  aurelia.singleton(lang.LanguageStructure, Hlasitě);
  aurelia.singleton(lang.LanguageStructure, Zlostně);
  aurelia.singleton(lang.LanguageStructure, Trpělivě);
  aurelia.singleton(lang.LanguageStructure, Trochu);
  aurelia.singleton(lang.LanguageStructure, Půlku);
  aurelia.singleton(lang.LanguageStructure, Všechno);
}


export class Neutrálně extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.neutrálně`;
  constructor() {
    super({
      adverb: '',
    });
  }
}

export class Silně extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.silně`;
  constructor() {
    super({
      adverb: 'silně',
      position: lang.AdverbPosition.middle,
    });
  }
}

export class Slabě extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.slabě`;
  constructor() {
    super({
      adverb: 'slabě',
      position: lang.AdverbPosition.middle,
    });
  }
}

export class Rychle extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.rychle`;
  constructor() {
    super({
      adverb: 'rychle',
      position: lang.AdverbPosition.middle,
    });
  }
}

export class Tiše extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.tiše`;
  constructor() {
    super({
      adverb: 'tiše',
      position: lang.AdverbPosition.middle,
    });
  }
}

export class Šeptavě extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.šeptavě`;
  constructor() {
    super({
      adverb: 'šeptavě',
      position: lang.AdverbPosition.middle,
    });
  }
}

export class Pomalu extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.pomalu`;
  constructor() {
    super({
      adverb: 'pomalu',
      position: lang.AdverbPosition.middle,
    });
  }
}

export class Váhavě extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.váhavě`;
  constructor() {
    super({
      adverb: 'váhavě',
      position: lang.AdverbPosition.middle,
    });
  }
}

export class Opatrně extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.opatrně`;
  constructor() {
    super({
      adverb: 'opatrně',
      position: lang.AdverbPosition.middle,
    });
  }
}

export class Hlasitě extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.hlasitě`;
  constructor() {
    super({
      adverb: 'hlasitě',
      position: lang.AdverbPosition.middle,
    });
  }
}

export class Zlostně extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.zlostně`;
  constructor() {
    super({
      adverb: 'zlostně',
      position: lang.AdverbPosition.middle,
    });
  }
}

export class Trpělivě extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.trpělivě`;
  constructor() {
    super({
      adverb: 'trpělivě',
      position: lang.AdverbPosition.middle,
    });
  }
}

export class Trochu extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.trochu`;
  constructor() {
    super({
      adverb: 'trochu',
      requiredCase: lang.Case.genitive,
      position: lang.AdverbPosition.end,
    });
  }
}

export class Půlku extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.půlku`;
  constructor() {
    super({
      adverb: 'půlku',
      requiredCase: lang.Case.genitive,
      position: lang.AdverbPosition.end,
    });
  }
}

export class Všechno extends lang.Adverb {
  public static readonly ID = `${lang.Adverb.ID}.všechno`;
  constructor() {
    super({
      adverb: 'všechno',
      requiredCase: lang.Case.genitive,
      position: lang.AdverbPosition.middle,
    });
  }
}

