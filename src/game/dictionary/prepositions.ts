import {FrameworkConfiguration} from 'aurelia-framework';

import * as lang from 'snowatch/language-structures';

export function configurePrepositions(aurelia: FrameworkConfiguration) {
  aurelia.singleton(lang.LanguageStructure, DátNěkomu);
  aurelia.singleton(lang.LanguageStructure, DoActionWith);
  aurelia.singleton(lang.LanguageStructure, AccusativeNa);
  aurelia.singleton(lang.LanguageStructure, VěnovatSeNěkomu);
  aurelia.singleton(lang.LanguageStructure, UtéctNěkomu);
  aurelia.singleton(lang.LanguageStructure, SpatialActionNa);
  aurelia.singleton(lang.LanguageStructure, SpatialActionPod);
  aurelia.singleton(lang.LanguageStructure, SpatialActionDo);
  aurelia.singleton(lang.LanguageStructure, SpatialActionPřed);
  aurelia.singleton(lang.LanguageStructure, SpatialActionZa);
  aurelia.singleton(lang.LanguageStructure, SpatialActionDozadu);
  aurelia.singleton(lang.LanguageStructure, SpatialActionDopředu);
  aurelia.singleton(lang.LanguageStructure, SpatialActionNalevo);
  aurelia.singleton(lang.LanguageStructure, SpatialActionNapravo);
  aurelia.singleton(lang.LanguageStructure, SpatialDescriptionNa);
  aurelia.singleton(lang.LanguageStructure, SpatialDescriptionPod);
  aurelia.singleton(lang.LanguageStructure, SpatialDescriptionV);
  aurelia.singleton(lang.LanguageStructure, SpatialDescriptionDo);
  aurelia.singleton(lang.LanguageStructure, SpatialDescriptionPřed);
  aurelia.singleton(lang.LanguageStructure, SpatialDescriptionZa);
  aurelia.singleton(lang.LanguageStructure, SpatialDescriptionDozadu);
  aurelia.singleton(lang.LanguageStructure, SpatialDescriptionNalevo);
  aurelia.singleton(lang.LanguageStructure, SpatialDescriptionNapravo);

  aurelia.singleton(lang.LanguageStructure, InstrumentS);
}

export class DátNěkomu extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.give-to-cz`;
  constructor() {
    super({
      preposition: '',
    });
  }
}

export class VěnovatSeNěkomu extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.engage-someone-cz`;
  constructor() {
    super({
      preposition: '',
      requiredCase: lang.Case.dative,
    });
  }
}

export class UtéctNěkomu extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.evade-someone-cz`;
  constructor() {
    super({
      preposition: '',
      requiredCase: lang.Case.dative,
    });
  }
}

export class DoActionWith extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.do-action-with-cz`;
  constructor() {
    super({
      preposition: '',
      requiredCase: lang.Case.instrumental,
    });
  }
}

export class AccusativeNa extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.accusative-na`;
  constructor() {
    super({
      preposition: 'na',
      requiredCase: lang.Case.accusative,
    });
  }
}

export class SpatialActionNa extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-action-na`;
  constructor() {
    super({
      preposition: 'na',
      requiredCase: lang.Case.accusative,
    });
  }
}

export class SpatialActionPod extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-action-pod`;
  constructor() {
    super({
      preposition: 'pod',
      requiredCase: lang.Case.accusative,
    });
  }
}

export class SpatialActionDo extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-action-do`;
  constructor() {
    super({
      preposition: 'do',
      requiredCase: lang.Case.genitive,
    });
  }
}

export class SpatialActionPřed extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-action-před`;
  constructor() {
    super({
      preposition: 'před',
      requiredCase: lang.Case.accusative,
    });
  }
}

export class SpatialActionZa extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-action-za`;
  constructor() {
    super({
      preposition: 'za',
      requiredCase: lang.Case.genitive,
    });
  }
}

export class SpatialActionDozadu extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-action-dozadu`;
  constructor() {
    super({
      preposition: 'k zadní straně',
      requiredCase: lang.Case.genitive,
    });
  }
}

export class SpatialActionDopředu extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-action-dopředu`;
  constructor() {
    super({
      preposition: 'k přední straně',
      requiredCase: lang.Case.genitive,
    });
  }
}

export class SpatialActionNalevo extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-action-nalevo`;
  constructor() {
    super({
      preposition: 'k levé straně',
      requiredCase: lang.Case.genitive,
    });
  }
}

export class SpatialActionNapravo extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-action-vpravo`;
  constructor() {
    super({
      preposition: 'k pravé straně',
      requiredCase: lang.Case.genitive,
    });
  }
}

export class SpatialDescriptionNa extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-description-na`;
  constructor() {
    super({
      preposition: 'na',
      requiredCase: lang.Case.locative,
    });
  }
}

export class SpatialDescriptionPod extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-description-pod`;
  constructor() {
    super({
      preposition: 'pod',
      requiredCase: lang.Case.instrumental,
    });
  }
}

export class SpatialDescriptionV extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-description-v`;
  constructor() {
    super({
      preposition: 'v',
      requiredCase: lang.Case.locative,
    });
  }
}

export class SpatialDescriptionDo extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-description-do`;
  constructor() {
    super({
      preposition: 'do',
      requiredCase: lang.Case.genitive,
    });
  }
}

export class SpatialDescriptionPřed extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-description-před`;
  constructor() {
    super({
      preposition: 'před',
      requiredCase: lang.Case.instrumental,
    });
  }
}

export class SpatialDescriptionZa extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-description-za`;
  constructor() {
    super({
      preposition: 'za',
      requiredCase: lang.Case.accusative,
    });
  }
}

export class SpatialDescriptionDozadu extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-description-dozadu`;
  constructor() {
    super({
      preposition: 'u zadní strany',
      requiredCase: lang.Case.accusative,
    });
  }
}

export class SpatialDescriptionNalevo extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-description-nalevo`;
  constructor() {
    super({
      preposition: 'nalevo od',
      requiredCase: lang.Case.genitive,
    });
  }
}

export class SpatialDescriptionNapravo extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.spatial-description-vpravo`;
  constructor() {
    super({
      preposition: 'napravo od',
      requiredCase: lang.Case.genitive,
    });
  }
}

export class InstrumentS extends lang.Preposition {
  public static readonly ID = `${lang.Preposition.ID}.instrument-with-cz`;
  constructor() {
    super({
      preposition: '',
      requiredCase: lang.Case.instrumental,
    });
  }
}
