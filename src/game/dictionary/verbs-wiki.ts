import {FrameworkConfiguration} from 'aurelia-framework';
import * as lang from 'snowatch/language-structures';

// generated from wikipedia list of czech verbs
// extraction tool repo: https://bitbucket.org/jIRI/wikiscraper


export function configureVerbsWiki(aurelia: FrameworkConfiguration) {
  aurelia.singleton(lang.LanguageStructure, Padat);
  aurelia.singleton(lang.LanguageStructure, Cítit);
  aurelia.singleton(lang.LanguageStructure, Dopadnout);
  aurelia.singleton(lang.LanguageStructure, Být);
  aurelia.singleton(lang.LanguageStructure, Nebýt);
  aurelia.singleton(lang.LanguageStructure, Vzít);
  aurelia.singleton(lang.LanguageStructure, Dát);
  aurelia.singleton(lang.LanguageStructure, Říct);
  aurelia.singleton(lang.LanguageStructure, Otevřít);
  aurelia.singleton(lang.LanguageStructure, Zavřít);
  aurelia.singleton(lang.LanguageStructure, Přejít);
  aurelia.singleton(lang.LanguageStructure, Zamknout);
  aurelia.singleton(lang.LanguageStructure, Odemknout);
  aurelia.singleton(lang.LanguageStructure, NaučitSe);
  aurelia.singleton(lang.LanguageStructure, Naučit);
  aurelia.singleton(lang.LanguageStructure, Projít);
  aurelia.singleton(lang.LanguageStructure, Usnout);
  aurelia.singleton(lang.LanguageStructure, Zkusit);
  aurelia.singleton(lang.LanguageStructure, Pít);
  aurelia.singleton(lang.LanguageStructure, OcitnoutSe);
  aurelia.singleton(lang.LanguageStructure, OctnoutSe);
  aurelia.singleton(lang.LanguageStructure, Lít);
  aurelia.singleton(lang.LanguageStructure, Vstoupit);
  aurelia.singleton(lang.LanguageStructure, Opustit);
  aurelia.singleton(lang.LanguageStructure, Zaútočit);
  aurelia.singleton(lang.LanguageStructure, Odrazit);
  aurelia.singleton(lang.LanguageStructure, Uhýbat);
  aurelia.singleton(lang.LanguageStructure, Utéct);
  aurelia.singleton(lang.LanguageStructure, VěnovatSe);
  aurelia.singleton(lang.LanguageStructure, Zasáhnout);
  aurelia.singleton(lang.LanguageStructure, Zemřít);
}

// ----------------------------------------------------------------------------------------

// Source file: 003-146.html
export class Padat extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.padat`;
  constructor() {
    super({
      verb: 'padat',
      infinitive: 'padat',
      singular: {
        present: {
          first: 'padám',
          second: 'padáš',
          third: 'padá',
        },
        future: {
          first: 'budu padat',
          second: 'budeš padat',
          third: 'bude padat',
        },
        imperative: 'padej',
        past: {
          first: { masculine: 'padal', feminine: 'padala', neutral: 'padalo', },
          second: { masculine: 'padal', feminine: 'padala', neutral: 'padalo', },
          third: { masculine: 'padal', feminine: 'padala', neutral: 'padalo', },
        },
        participle: {
          masculine: 'padaje',
          feminine: 'padajíc',
          neutral: 'padajíc',
        },
      },
      plural: {
        present: {
          first: 'padáme',
          second: 'padáte',
          third: 'padají',
        },
        future: {
          first: 'budeme padat',
          second: 'budete padat',
          third: 'budou padat',
        },
        imperative: {
          second: 'padejme',
          third: 'padejte',
        },
        past: {
          first: { masculine: 'padali', feminine: 'padaly', neutral: 'padala', },
          second: { masculine: 'padali', feminine: 'padaly', neutral: 'padala', },
          third: { masculine: 'padali', feminine: 'padaly', neutral: 'padala', },
        },
        participle: 'padajíce',
      },
    });
  }
}

// Source file: 000-125.html
export class Cítit extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.cítit`;
  constructor() {
    super({
      verb: 'cítit',
      infinitive: 'cítit',
      singular: {
        present: {
          first: 'cítím',
          second: 'cítíš',
          third: 'cítí',
        },
        future: {
          first: 'budu cítit',
          second: 'budeš cítit',
          third: 'bude cítit',
        },
        imperative: 'ciť',
        past: {
          first: { masculine: 'cítil', feminine: 'cítila', neutral: 'cítilo', },
          second: { masculine: 'cítil', feminine: 'cítila', neutral: 'cítilo', },
          third: { masculine: 'cítil', feminine: 'cítila', neutral: 'cítilo', },
        },
        passive: {
          first: { masculine: 'cítěn', feminine: 'cítěna', neutral: 'cítěno', },
          second: { masculine: 'cítěn', feminine: 'cítěna', neutral: 'cítěno', },
          third: { masculine: 'cítěn', feminine: 'cítěna', neutral: 'cítěno', },
        },
        participle: {
          masculine: 'cítě',
          feminine: 'cítíc',
          neutral: 'cítíc',
        },
      },
      plural: {
        present: {
          first: 'cítíme',
          second: 'cítíte',
          third: 'cítí',
        },
        future: {
          first: 'budeme cítit',
          second: 'budete cítit',
          third: 'budou cítit',
        },
        imperative: {
          second: 'ciťme',
          third: 'ciťte',
        },
        past: {
          first: { masculine: 'cítili', feminine: 'cítily', neutral: 'cítila', },
          second: { masculine: 'cítili', feminine: 'cítily', neutral: 'cítila', },
          third: { masculine: 'cítili', feminine: 'cítily', neutral: 'cítila', },
        },
        passive: {
          first: { masculine: 'cítěni', feminine: 'cítěny', neutral: 'cítěna', },
          second: { masculine: 'cítěni', feminine: 'cítěny', neutral: 'cítěna', },
          third: { masculine: 'cítěni', feminine: 'cítěny', neutral: 'cítěna', },
        },
        participle: 'cítíce',
      },
    });
  }
}

// Source file: 000-197.html
export class Dopadnout extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.dopadnout`;
  constructor() {
    super({
      verb: 'dopadnout',
      infinitive: 'dopadnout',
      singular: {
        present: {
          first: 'dopadnu',
          second: 'dopadneš',
          third: 'dopadne',
        },
        future: {
          first: 'dopadnu',
          second: 'dopadneš',
          third: 'dopadne',
        },
        imperative: 'dopadni',
        past: {
          first: { masculine: 'dopadl', feminine: 'dopadla', neutral: 'dopadlo', },
          second: { masculine: 'dopadl', feminine: 'dopadla', neutral: 'dopadlo', },
          third: { masculine: 'dopadl', feminine: 'dopadla', neutral: 'dopadlo', },
        },
        passive: {
          first: { masculine: 'dopaden', feminine: 'dopadena', neutral: 'dopadena', },
          second: { masculine: 'dopaden', feminine: 'dopadena', neutral: 'dopadena', },
          third: { masculine: 'dopaden', feminine: 'dopadena', neutral: 'dopadena', },
        },
        participle: {
          masculine: 'dopadnuv',
          feminine: 'dopadnuvši',
          neutral: 'dopadnuvši',
        },
      },
      plural: {
        present: {
          first: 'dopadneme',
          second: 'dopadnete',
          third: 'dopadnou',
        },
        future: {
          first: 'dopadneme',
          second: 'dopadnete',
          third: 'dopadnou',
        },
        imperative: {
          second: 'dopadněme',
          third: 'dopadněte',
        },
        past: {
          first: { masculine: 'dopadli', feminine: 'dopadly', neutral: 'dopadla', },
          second: { masculine: 'dopadli', feminine: 'dopadly', neutral: 'dopadla', },
          third: { masculine: 'dopadli', feminine: 'dopadly', neutral: 'dopadla', },
        },
        passive: {
          first: { masculine: 'dopadeni', feminine: 'dopadeny', neutral: 'dopadena', },
          second: { masculine: 'dopadeni', feminine: 'dopadeny', neutral: 'dopadena', },
          third: { masculine: 'dopadeni', feminine: 'dopadeny', neutral: 'dopadena', },
        },
        participle: 'dopadnuvše',
      },
    });
  }
}

// Source file: 000-070.html
export class Být extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.být`;
  constructor() {
    super({
      verb: 'být',
      infinitive: 'být',
      singular: {
        present: {
          first: 'jsem',
          second: 'jsi',
          third: 'je',
        },
        future: {
          first: 'budu',
          second: 'budeš',
          third: 'bude',
        },
        imperative: 'buď',
        past: {
          first: { masculine: 'byl', feminine: 'byla', neutral: 'bylo', },
          second: { masculine: 'byl', feminine: 'byla', neutral: 'bylo', },
          third: { masculine: 'byl', feminine: 'byla', neutral: 'bylo', },
        },
        participle: {
          masculine: 'jsa',
          feminine: 'jsouc',
          neutral: 'jsouc',
        },
      },
      plural: {
        present: {
          first: 'jsme',
          second: 'jste',
          third: 'jsou',
        },
        future: {
          first: 'budeme',
          second: 'budete',
          third: 'budou',
        },
        imperative: {
          second: 'buďme',
          third: 'buďte',
        },
        past: {
          first: { masculine: {animate: 'byli', inanimate: 'byly'}, feminine: 'byly', neutral: 'byla', },
          second: { masculine: {animate: 'byli', inanimate: 'byly'}, feminine: 'byly', neutral: 'byla', },
          third: { masculine: {animate: 'byli', inanimate: 'byly'}, feminine: 'byly', neutral: 'byla', },
        },
        participle: 'jsouce',
      },
    });
  }
}

export class Nebýt extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.nebýt`;
  constructor() {
    super({
      verb: 'nebýt',
      infinitive: 'nebýt',
      singular: {
        present: {
          first: 'nejsem',
          second: 'nejsi',
          third: 'není',
        },
        future: {
          first: 'nebudu',
          second: 'nebudeš',
          third: 'nebude',
        },
        imperative: 'nebuď',
        past: {
          first: { masculine: 'nebyl', feminine: 'nebyla', neutral: 'nebylo', },
          second: { masculine: 'nebyl', feminine: 'nebyla', neutral: 'nebylo', },
          third: { masculine: 'nebyl', feminine: 'nebyla', neutral: 'nebylo', },
        },
        participle: {
          masculine: 'nejsa',
          feminine: 'nejsouc',
          neutral: 'nejsouc',
        },
      },
      plural: {
        present: {
          first: 'nejsme',
          second: 'nejste',
          third: 'nejsou',
        },
        future: {
          first: 'nebudeme',
          second: 'nebudete',
          third: 'nebudou',
        },
        imperative: {
          second: 'nebuďme',
          third: 'nebuďte',
        },
        past: {
          first: { masculine: 'nebyli', feminine: 'nebyly', neutral: 'nebyla', },
          second: { masculine: 'nebyli', feminine: 'nebyly', neutral: 'nebyla', },
          third: { masculine: 'nebyli', feminine: 'nebyly', neutral: 'nebyla', },
        },
        participle: 'nejsouce',
      },
    });
  }
}

// Source file: 008-048.html
export class Vzít extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.vzít`;
  constructor() {
    super({
      verb: 'vzít',
      infinitive: 'vzít',
      singular: {
        future: {
          first: 'vezmu',
          second: 'vezmeš',
          third: 'vezme',
        },
        imperative: 'vezmi',
        past: {
          first: { masculine: 'vzal', feminine: 'vzala', neutral: 'vzalo', },
          second: { masculine: 'vzal', feminine: 'vzala', neutral: 'vzalo', },
          third: { masculine: 'vzal', feminine: 'vzala', neutral: 'vzalo', },
        },
        passive: {
          first: { masculine: 'vzat', feminine: 'vzata', neutral: 'vzato', },
          second: { masculine: 'vzat', feminine: 'vzata', neutral: 'vzato', },
          third: { masculine: 'vzat', feminine: 'vzata', neutral: 'vzato', },
        },
        participle: {
          masculine: 'vezma',
          feminine: 'vezmouc',
          neutral: 'vezmouc',
        },
      },
      plural: {
        future: {
          first: 'vezmeme',
          second: 'vezmete',
          third: 'vezmou',
        },
        imperative: {
          second: 'vezměme',
          third: 'vezměte',
        },
        past: {
          first: { masculine: 'vzali', feminine: 'vzaly', neutral: 'vzala', },
          second: { masculine: 'vzali', feminine: 'vzaly', neutral: 'vzala', },
          third: { masculine: 'vzali', feminine: 'vzaly', neutral: 'vzala', },
        },
        passive: {
          first: { masculine: 'vzati', feminine: 'vzaty', neutral: 'vzata', },
          second: { masculine: 'vzati', feminine: 'vzaty', neutral: 'vzata', },
          third: { masculine: 'vzati', feminine: 'vzaty', neutral: 'vzata', },
        },
        participle: 'vezmouce',
      },
    });
  }
}

// Source file: 000-149.html
export class Dát extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.dát`;
  constructor() {
    super({
      verb: 'dát',
      infinitive: 'dát',
      singular: {
        future: {
          first: 'dám',
          second: 'dáš',
          third: 'dá',
        },
        imperative: 'dej',
        past: {
          first: { masculine: 'dal', feminine: 'dala', neutral: 'dalo', },
          second: { masculine: 'dal', feminine: 'dala', neutral: 'dalo', },
          third: { masculine: 'dal', feminine: 'dala', neutral: 'dalo', },
        },
        passive: {
          first: { masculine: 'dán', feminine: 'dána', neutral: 'dáno', },
          second: { masculine: 'dán', feminine: 'dána', neutral: 'dáno', },
          third: { masculine: 'dán', feminine: 'dána', neutral: 'dáno', },
        },
        participle: {
          masculine: 'dada',
          feminine: 'dadouc',
          neutral: 'dadouc',
        },
      },
      plural: {
        future: {
          first: 'dáme',
          second: 'dáte',
          third: 'dají',
        },
        imperative: {
          second: 'dejme',
          third: 'dejte',
        },
        past: {
          first: { masculine: 'dali', feminine: 'daly', neutral: 'dala', },
          second: { masculine: 'dali', feminine: 'daly', neutral: 'dala', },
          third: { masculine: 'dali', feminine: 'daly', neutral: 'dala', },
        },
        passive: {
          first: { masculine: 'dáni', feminine: 'dány', neutral: 'dána', },
          second: { masculine: 'dáni', feminine: 'dány', neutral: 'dána', },
          third: { masculine: 'dáni', feminine: 'dány', neutral: 'dána', },
        },
        participle: 'dadouce',
      },
    });
  }
}

// Source file: 005-088.html
export class Říct extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.říct`;
  constructor() {
    super({
      verb: 'říct',
      infinitive: 'říct',
      singular: {
        future: {
          first: 'řeknu',
          second: 'řekneš',
          third: 'řekne',
        },
        imperative: 'řekni',
        past: {
          first: { masculine: 'řekl', feminine: 'řekla', neutral: 'řeklo', },
          second: { masculine: 'řekl', feminine: 'řekla', neutral: 'řeklo', },
          third: { masculine: 'řekl', feminine: 'řekla', neutral: 'řeklo', },
        },
        passive: {
          first: { masculine: 'řečen', feminine: 'řečena', neutral: 'řečeno', },
          second: { masculine: 'řečen', feminine: 'řečena', neutral: 'řečeno', },
          third: { masculine: 'řečen', feminine: 'řečena', neutral: 'řečeno', },
        },
        participle: {
          masculine: 'řka',
          feminine: 'řkouc',
          neutral: 'řkouc',
        },
      },
      plural: {
        future: {
          first: 'řekneme',
          second: 'řeknete',
          third: 'řeknou',
        },
        imperative: {
          second: 'řekněme',
          third: 'řekněte',
        },
        past: {
          first: { masculine: 'řekli', feminine: 'řekly', neutral: 'řekla', },
          second: { masculine: 'řekli', feminine: 'řekly', neutral: 'řekla', },
          third: { masculine: 'řekli', feminine: 'řekly', neutral: 'řekla', },
        },
        passive: {
          first: { masculine: 'řečeni', feminine: 'řečeny', neutral: 'řečena', },
          second: { masculine: 'řečeni', feminine: 'řečeny', neutral: 'řečena', },
          third: { masculine: 'řečeni', feminine: 'řečeny', neutral: 'řečena', },
        },
        participle: 'řkouce',
      },
    });
  }
}

// Source file: 003-125.html
export class Otevřít extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.otevřít`;
  constructor() {
    super({
      verb: 'otevřít',
      infinitive: 'otevřít',
      singular: {
        future: {
          first: 'otevřu',
          second: 'otevřeš',
          third: 'otevře',
        },
        imperative: 'otevři',
        past: {
          first: { masculine: 'otevřel', feminine: 'otevřela', neutral: 'otevřelo', },
          second: { masculine: 'otevřel', feminine: 'otevřela', neutral: 'otevřelo', },
          third: { masculine: 'otevřel', feminine: 'otevřela', neutral: 'otevřelo', },
        },
        passive: {
          first: { masculine: 'otevřen', feminine: 'otevřena', neutral: 'otevřeno', },
          second: { masculine: 'otevřen', feminine: 'otevřena', neutral: 'otevřeno', },
          third: { masculine: 'otevřen', feminine: 'otevřena', neutral: 'otevřeno', },
        },
        participle: {
          masculine: 'otevřev',
          feminine: 'otevřevši',
          neutral: 'otevřevši',
        },
      },
      plural: {
        future: {
          first: 'otevřeme',
          second: 'otevřete',
          third: 'otevřou',
        },
        imperative: {
          second: 'otevřeme',
          third: 'otevřete',
        },
        past: {
          first: { masculine: 'otevřeli', feminine: 'otevřely', neutral: 'otevřela', },
          second: { masculine: 'otevřeli', feminine: 'otevřely', neutral: 'otevřela', },
          third: { masculine: 'otevřeli', feminine: 'otevřely', neutral: 'otevřela', },
        },
        passive: {
          first: { masculine: 'otevřeni', feminine: 'otevřeny', neutral: 'otevřena', },
          second: { masculine: 'otevřeni', feminine: 'otevřeny', neutral: 'otevřena', },
          third: { masculine: 'otevřeni', feminine: 'otevřeny', neutral: 'otevřena', },
        },
        participle: 'otevřevše',
      },
    });
  }
}

// Source file: 008-147.html
export class Zavřít extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.zavřít`;
  constructor() {
    super({
      verb: 'zavřít',
      infinitive: 'zavřít',
      singular: {
        future: {
          first: 'zavřu',
          second: 'zavřeš',
          third: 'zavře',
        },
        imperative: 'zavři',
        past: {
          first: { masculine: 'zavřel', feminine: 'zavřela', neutral: 'zavřelo', },
          second: { masculine: 'zavřel', feminine: 'zavřela', neutral: 'zavřelo', },
          third: { masculine: 'zavřel', feminine: 'zavřela', neutral: 'zavřelo', },
        },
        passive: {
          first: { masculine: 'zavřen', feminine: 'zavřena', neutral: 'zavřeno', },
          second: { masculine: 'zavřen', feminine: 'zavřena', neutral: 'zavřeno', },
          third: { masculine: 'zavřen', feminine: 'zavřena', neutral: 'zavřeno', },
        },
        participle: {
          masculine: 'zavřev',
          feminine: 'zavřevši',
          neutral: 'zavřevši',
        },
      },
      plural: {
        future: {
          first: 'zavřeme',
          second: 'zavřete',
          third: 'zavřou',
        },
        imperative: {
          second: 'zavřeme',
          third: 'zavřete',
        },
        past: {
          first: { masculine: 'zavřeli', feminine: 'zavřely', neutral: 'zavřela', },
          second: { masculine: 'zavřeli', feminine: 'zavřely', neutral: 'zavřela', },
          third: { masculine: 'zavřeli', feminine: 'zavřely', neutral: 'zavřela', },
        },
        passive: {
          first: { masculine: 'zavřeni', feminine: 'zavřeny', neutral: 'zavřena', },
          second: { masculine: 'zavřeni', feminine: 'zavřeny', neutral: 'zavřena', },
          third: { masculine: 'zavřeni', feminine: 'zavřeny', neutral: 'zavřena', },
        },
        participle: 'zavřevše',
      },
    });
  }
}

// Source file: 004-138.html
export class Přejít extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.přejít`;
  constructor() {
    super({
      verb: 'přejít',
      infinitive: 'přejít',
      singular: {
        future: {
          first: 'přejdu',
          second: 'přejdeš',
          third: 'přejde',
        },
        imperative: 'přejdi',
        past: {
          first: { masculine: 'přešel', feminine: 'přešla', neutral: 'přešlo', },
          second: { masculine: 'přešel', feminine: 'přešla', neutral: 'přešlo', },
          third: { masculine: 'přešel', feminine: 'přešla', neutral: 'přešlo', },
        },
        participle: {
          masculine: 'přešed',
          feminine: 'přešedši',
          neutral: 'přešedši',
        },
      },
      plural: {
        future: {
          first: 'přejdeme',
          second: 'přejdete',
          third: 'přejdou',
        },
        imperative: {
          second: 'přejděme',
          third: 'přejděte',
        },
        past: {
          first: { masculine: 'přešli', feminine: 'přešly', neutral: 'přešla', },
          second: { masculine: 'přešli', feminine: 'přešly', neutral: 'přešla', },
          third: { masculine: 'přešli', feminine: 'přešly', neutral: 'přešla', },
        },
        participle: 'přešedše',
      },
    });
  }
}

// Source file: 008-106.html
export class Zamknout extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.zamknout`;
  constructor() {
    super({
      verb: 'zamknout',
      infinitive: 'zamknout',
      singular: {
        present: {
          first: 'zamykám',
          second: 'zamykáš',
          third: 'zamyká',
        },
        future: {
          first: 'zamknu',
          second: 'zamkneš',
          third: 'zamkne',
        },
        imperative: 'zamkni',
        past: {
          first: { masculine: 'zamkl', feminine: 'zamkla', neutral: 'zamklo', },
          second: { masculine: 'zamkl', feminine: 'zamkla', neutral: 'zamklo',  },
          third: { masculine: 'zamkl', feminine: 'zamkla', neutral: 'zamklo',  },
        },
        passive: {
          first: { masculine: 'zamykán', feminine: 'zamykána', neutral: 'zamykáno', },
          second: { masculine: 'zamykán', feminine: 'zamykána', neutral: 'zamykáno', },
          third: { masculine: 'zamykán', feminine: 'zamykána', neutral: 'zamykáno', },
        },
        participle: {
          masculine: 'zamykaje',
          feminine: 'zamykajíc',
          neutral: 'zamykajíc',
        },
      },
      plural: {
        present: {
          first: 'zamykáme',
          second: 'zamykáte',
          third: 'zamykají',
        },
        future: {
          first: 'zamykneme',
          second: 'zamknete',
          third: 'zamknou',
        },
        imperative: {
          second: 'zamkněte',
          third: 'zamkněte',
        },
        past: {
          first: { masculine: 'zamkli', feminine: 'zamkly', neutral: 'zamkla', },
          second: { masculine: 'zamkli', feminine: 'zamkly', neutral: 'zamkla', },
          third: { masculine: 'zamkli', feminine: 'zamkly', neutral: 'zamkla', },
        },
        passive: {
          first: { masculine: 'zamykáni', feminine: 'zamykány', neutral: 'zamykána', },
          second: { masculine: 'zamykáni', feminine: 'zamykány', neutral: 'zamykána', },
          third: { masculine: 'zamykáni', feminine: 'zamykány', neutral: 'zamykána', },
        },
        participle: 'zamykajíce',
      },
    });
  }
}

// Source file: 008-106.html
export class Odemknout extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.odemknout`;
  constructor() {
    super({
      verb: 'odemknout',
      infinitive: 'odemknout',
      singular: {
        present: {
          first: 'odemykám',
          second: 'odemykáš',
          third: 'odemyká',
        },
        future: {
          first: 'odemknu',
          second: 'odemkneš',
          third: 'odemkne',
        },
        imperative: 'odemkni',
        past: {
          first: { masculine: 'odemkl', feminine: 'odemkla', neutral: 'odemklo', },
          second: { masculine: 'odemkl', feminine: 'odemkla', neutral: 'odemklo',  },
          third: { masculine: 'odemkl', feminine: 'odemkla', neutral: 'odemklo',  },
        },
        passive: {
          first: { masculine: 'odemykán', feminine: 'odemykána', neutral: 'odemykáno', },
          second: { masculine: 'odemykán', feminine: 'odemykána', neutral: 'odemykáno', },
          third: { masculine: 'odemykán', feminine: 'odemykána', neutral: 'odemykáno', },
        },
        participle: {
          masculine: 'odemykaje',
          feminine: 'odemykajíc',
          neutral: 'odemykajíc',
        },
      },
      plural: {
        present: {
          first: 'odemykáme',
          second: 'odemykáte',
          third: 'odemykají',
        },
        future: {
          first: 'odemykneme',
          second: 'odemknete',
          third: 'odemknou',
        },
        imperative: {
          second: 'odemkněte',
          third: 'odemkněte',
        },
        past: {
          first: { masculine: 'odemkli', feminine: 'odemkly', neutral: 'odemkla', },
          second: { masculine: 'odemkli', feminine: 'odemkly', neutral: 'odemkla', },
          third: { masculine: 'odemkli', feminine: 'odemkly', neutral: 'odemkla', },
        },
        passive: {
          first: { masculine: 'odemykáni', feminine: 'odemykány', neutral: 'odemykána', },
          second: { masculine: 'odemykáni', feminine: 'odemykány', neutral: 'odemykána', },
          third: { masculine: 'odemykáni', feminine: 'odemykány', neutral: 'odemykána', },
        },
        participle: 'odemykajíce',
      },
    });
  }
}

// Source file: 002-196.html
export class NaučitSe extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.naučit-se`;
  constructor() {
    super({
      verb: 'naučit se',
      infinitive: 'naučit se',
      singular: {
        future: {
          first: 'naučím se',
          second: 'naučíš se',
          third: 'naučí se',
        },
        imperative: 'nauč se',
        past: {
          first: { masculine: 'naučil se', feminine: 'naučila se', neutral: 'naučilo se', },
          second: { masculine: 'naučil se', feminine: 'naučila se', neutral: 'naučilo se', },
          third: { masculine: 'naučil se', feminine: 'naučila se', neutral: 'naučilo se', },
        },
        passive: {
          first: { masculine: 'naučen', feminine: 'naučena', neutral: 'naučeno', },
          second: { masculine: 'naučen', feminine: 'naučena', neutral: 'naučeno', },
          third: { masculine: 'naučen', feminine: 'naučena', neutral: 'naučeno', },
        },
        participle: {
          masculine: 'naučiv se',
          feminine: 'naučivši se',
          neutral: 'naučivši se',
        },
      },
      plural: {
        future: {
          first: 'naučíme se',
          second: 'naučíte se',
          third: 'naučí se',
        },
        imperative: {
          second: 'naučme se',
          third: 'naučte se',
        },
        past: {
          first: { masculine: 'naučili se', feminine: 'naučily se', neutral: 'naučila se', },
          second: { masculine: 'naučili se', feminine: 'naučily se', neutral: 'naučila se', },
          third: { masculine: 'naučili se', feminine: 'naučily se', neutral: 'naučila se', },
        },
        passive: {
          first: { masculine: 'naučeni', feminine: 'naučeny', neutral: 'naučena', },
          second: { masculine: 'naučeni', feminine: 'naučeny', neutral: 'naučena', },
          third: { masculine: 'naučeni', feminine: 'naučeny', neutral: 'naučena', },
        },
        participle: 'naučivše se',
      },
    });
  }
}

// Source file: 002-196.html
export class Naučit extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.naučit`;
  constructor() {
    super({
      verb: 'naučit',
      infinitive: 'naučit',
      singular: {
        future: {
          first: 'naučím',
          second: 'naučíš',
          third: 'naučí',
        },
        imperative: 'nauč',
        past: {
          first: { masculine: 'naučil', feminine: 'naučila', neutral: 'naučilo', },
          second: { masculine: 'naučil', feminine: 'naučila', neutral: 'naučilo', },
          third: { masculine: 'naučil', feminine: 'naučila', neutral: 'naučilo', },
        },
        passive: {
          first: { masculine: 'naučen', feminine: 'naučena', neutral: 'naučeno', },
          second: { masculine: 'naučen', feminine: 'naučena', neutral: 'naučeno', },
          third: { masculine: 'naučen', feminine: 'naučena', neutral: 'naučeno', },
        },
        participle: {
          masculine: 'naučiv',
          feminine: 'naučivši',
          neutral: 'naučivši',
        },
      },
      plural: {
        future: {
          first: 'naučíme',
          second: 'naučíte',
          third: 'naučí',
        },
        imperative: {
          second: 'naučme',
          third: 'naučte',
        },
        past: {
          first: { masculine: 'naučili', feminine: 'naučily', neutral: 'naučila', },
          second: { masculine: 'naučili', feminine: 'naučily', neutral: 'naučila', },
          third: { masculine: 'naučili', feminine: 'naučily', neutral: 'naučila', },
        },
        passive: {
          first: { masculine: 'naučeni', feminine: 'naučeny', neutral: 'naučena', },
          second: { masculine: 'naučeni', feminine: 'naučeny', neutral: 'naučena', },
          third: { masculine: 'naučeni', feminine: 'naučeny', neutral: 'naučena', },
        },
        participle: 'naučivše',
      },
    });
  }
}

export class Projít extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.projít`;
  constructor() {
    super({
      verb: 'projít',
      infinitive: 'projít',
      singular: {
        future: {
          first: 'projdu',
          second: 'projdeš',
          third: 'projde',
        },
        imperative: 'projdi',
        past: {
          first: { masculine: 'prošel', feminine: 'prošla', neutral: 'prošlo', },
          second: { masculine: 'prošel', feminine: 'prošla', neutral: 'prošlo', },
          third: { masculine: 'prošel', feminine: 'prošla', neutral: 'prošlo', },
        },
        participle: {
          masculine: 'prošed',
          feminine: 'prošedši',
          neutral: 'prošedši',
        },
      },
      plural: {
        future: {
          first: 'projdeme',
          second: 'projdete',
          third: 'projdou',
        },
        imperative: {
          second: 'projděme',
          third: 'projděte',
        },
        past: {
          first: { masculine: 'prošli', feminine: 'prošly', neutral: 'prošla', },
          second: { masculine: 'prošli', feminine: 'prošly', neutral: 'prošla', },
          third: { masculine: 'prošli', feminine: 'prošly', neutral: 'prošla', },
        },
        participle: 'prošedše',
      },
    });
  }
}

// Source file: 007-042.html
export class Usnout extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.usnout`;
  constructor() {
    super({
      verb: 'usnout',
      infinitive: 'usnout',
      singular: {
        future: {
          first: 'usnu',
          second: 'usneš',
          third: 'usne',
        },
        imperative: 'usni',
        past: {
          first: { masculine: 'usnul', feminine: 'usnula', neutral: 'usnulo', },
          second: { masculine: 'usnul', feminine: 'usnula', neutral: 'usnulo', },
          third: { masculine: 'usnul', feminine: 'usnula', neutral: 'usnulo', },
        },
        passive: {
          first: { masculine: 'usnut', feminine: 'usnuta', neutral: 'usnuto', },
          second: { masculine: 'usnut', feminine: 'usnuta', neutral: 'usnuto', },
          third: { masculine: 'usnut', feminine: 'usnuta', neutral: 'usnuto', },
        },
        participle: {
          masculine: 'usnuv',
          feminine: 'usnuvši',
          neutral: 'usnuvši',
        },
      },
      plural: {
        future: {
          first: 'usneme',
          second: 'usnete',
          third: 'usnou',
        },
        imperative: {
          second: 'usněme',
          third: 'usněte',
        },
        past: {
          first: { masculine: 'usnuli', feminine: 'usnuly', neutral: 'usnula', },
          second: { masculine: 'usnuli', feminine: 'usnuly', neutral: 'usnula', },
          third: { masculine: 'usnuli', feminine: 'usnuly', neutral: 'usnula', },
        },
        passive: {
          first: { masculine: 'usnuti', feminine: 'usnuty', neutral: 'usnuta', },
          second: { masculine: 'usnuti', feminine: 'usnuty', neutral: 'usnuta', },
          third: { masculine: 'usnuti', feminine: 'usnuty', neutral: 'usnuta', },
        },
        participle: 'usnuvše',
      },
    });
  }
}

// Source file: 009-070.html
export class Zkusit extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.zkusit`;
  constructor() {
    super({
      verb: 'zkusit',
      infinitive: 'zkusit',
      singular: {
        future: {
          first: 'zkusím',
          second: 'zkusíš',
          third: 'zkusí',
        },
        imperative: 'zkus',
        past: {
          first: { masculine: 'zkusil', feminine: 'zkusila', neutral: 'zkusilo', },
          second: { masculine: 'zkusil', feminine: 'zkusila', neutral: 'zkusilo', },
          third: { masculine: 'zkusil', feminine: 'zkusila', neutral: 'zkusilo', },
        },
        participle: {
          masculine: 'zkusiv',
          feminine: 'zkusivši',
          neutral: 'zkusivši',
        },
      },
      plural: {
        future: {
          first: 'zkusíme',
          second: 'zkusíte',
          third: 'zkusí',
        },
        imperative: {
          second: 'zkusme',
          third: 'zkuste',
        },
        past: {
          first: { masculine: 'zkusili', feminine: 'zkusily', neutral: 'zkusila', },
          second: { masculine: 'zkusili', feminine: 'zkusily', neutral: 'zkusila', },
          third: { masculine: 'zkusili', feminine: 'zkusily', neutral: 'zkusila', },
        },
        participle: 'zkusivše',
      },
    });
  }
}

// Source file: 003-177.html
export class Pít extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.pít`;
  constructor() {
    super({
      verb: 'pít',
      infinitive: 'pít',
      singular: {
        present: {
          first: 'piju',
          second: 'piješ',
          third: 'pije',
        },
        future: {
          first: 'budu pít',
          second: 'budeš pít',
          third: 'bude pít',
        },
        imperative: 'pij',
        past: {
          first: { masculine: 'pil', feminine: 'pila', neutral: 'pilo', },
          second: { masculine: 'pil', feminine: 'pila', neutral: 'pilo', },
          third: { masculine: 'pil', feminine: 'pila', neutral: 'pilo', },
        },
        passive: {
          first: { masculine: 'pit', feminine: 'pita', neutral: 'pito', },
          second: { masculine: 'pit', feminine: 'pita', neutral: 'pito', },
          third: { masculine: 'pit', feminine: 'pita', neutral: 'pito', },
        },
        participle: {
          masculine: 'pije',
          feminine: 'pijíc',
          neutral: 'pijíc',
        },
      },
      plural: {
        present: {
          first: 'pijeme',
          second: 'pijete',
          third: 'pijou',
        },
        future: {
          first: 'budeme pít',
          second: 'budete pít',
          third: 'budou pít',
        },
        imperative: {
          second: 'pijme',
          third: 'pijte',
        },
        past: {
          first: { masculine: 'pili', feminine: 'pily', neutral: 'pila', },
          second: { masculine: 'pili', feminine: 'pily', neutral: 'pila', },
          third: { masculine: 'pili', feminine: 'pily', neutral: 'pila', },
        },
        passive: {
          first: { masculine: 'piti', feminine: 'pity', neutral: 'pita', },
          second: { masculine: 'piti', feminine: 'pity', neutral: 'pita', },
          third: { masculine: 'piti', feminine: 'pity', neutral: 'pita', },
        },
        participle: 'pijíce',
      },
    });
  }
}

// Source file: 003-035.html
export class OcitnoutSe extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.ocitnout-se`;
  constructor() {
    super({
      verb: 'ocitnout se',
      infinitive: 'ocitnout se',
      singular: {
        present: {
          first: 'ocitnu se',
          second: 'ocitneš se',
          third: 'ocitne se',
        },
        future: {
          first: 'ocitnu se',
          second: 'ocitneš se',
          third: 'ocitne se',
        },
        imperative: 'ocitni se',
        past: {
          first: { masculine: 'ocitl se', feminine: 'ocitla se', neutral: 'ocitlo se', },
          second: { masculine: 'ocitl se', feminine: 'ocitla se', neutral: 'ocitlo se', },
          third: { masculine: 'ocitl se', feminine: 'ocitla se', neutral: 'ocitlo se', },
        },
        participle: {
          masculine: 'ocitnuv se',
          feminine: 'ocitnuvši se',
          neutral: 'ocitnuvši se',
        },
      },
      plural: {
        present: {
          first: 'ocitneme se',
          second: 'ocitnete se',
          third: 'ocitnou se',
        },
        future: {
          first: 'ocitneme se',
          second: 'ocitnete se',
          third: 'ocitnou se',
        },
        imperative: {
          second: 'ocitněme se',
          third: 'ocitněte se',
        },
        past: {
          first: { masculine: 'ocitli se', feminine: 'ocitly se', neutral: 'ocitla se', },
          second: { masculine: 'ocitli se', feminine: 'ocitly se', neutral: 'ocitla se', },
          third: { masculine: 'ocitli se', feminine: 'ocitly se', neutral: 'ocitla se', },
        },
        participle: 'ocitnuvše se',
      },
    });
  }
}

// Source file: 003-036.html
export class OctnoutSe extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.octnout-se`;
  constructor() {
    super({
      verb: 'octnout se',
      infinitive: 'octnout se',
      singular: {
        present: {
          first: 'octnu se',
          second: 'octneš se',
          third: 'octne se',
        },
        future: {
          first: 'octnu se',
          second: 'octneš se',
          third: 'octne se',
        },
        imperative: 'octni se',
        past: {
          first: { masculine: 'octl se', feminine: 'octla se', neutral: 'octlo se', },
          second: { masculine: 'octl se', feminine: 'octla se', neutral: 'octlo se', },
          third: { masculine: 'octl se', feminine: 'octla se', neutral: 'octlo se', },
        },
        participle: {
          masculine: 'octnuv se',
          feminine: 'octnuvši se',
          neutral: 'octnuvši se',
        },
      },
      plural: {
        present: {
          first: 'octneme se',
          second: 'octnete se',
          third: 'octnou se',
        },
        future: {
          first: 'octneme se',
          second: 'octnete se',
          third: 'octnou se',
        },
        imperative: {
          second: 'octněme se',
          third: 'octněte se',
        },
        past: {
          first: { masculine: 'octli se', feminine: 'octly se', neutral: 'octla se', },
          second: { masculine: 'octli se', feminine: 'octly se', neutral: 'octla se', },
          third: { masculine: 'octli se', feminine: 'octly se', neutral: 'octla se', },
        },
        participle: 'octnuvše se',
      },
    });
  }
}

// Source file: 002-065.html
export class Lít extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.lít`;
  constructor() {
    super({
      verb: 'lít',
      infinitive: 'lít',
      singular: {
        present: {
          first: 'leji',
          second: 'leješ',
          third: 'leje',
        },
        future: {
          first: 'budu lít',
          second: 'budeš lít',
          third: 'bude lít',
        },
        imperative: 'lej',
        past: {
          first: { masculine: 'lil', feminine: 'lila', neutral: 'lilo', },
          second: { masculine: 'lil', feminine: 'lila', neutral: 'lilo', },
          third: { masculine: 'lil', feminine: 'lila', neutral: 'lilo', },
        },
        passive: {
          first: { masculine: 'lit', feminine: 'lita', neutral: 'lito', },
          second: { masculine: 'lit', feminine: 'lita', neutral: 'lito', },
          third: { masculine: 'lit', feminine: 'lita', neutral: 'lito', },
        },
        participle: {
          masculine: 'leje',
          feminine: 'lejíc',
          neutral: 'lejíc',
        },
      },
      plural: {
        present: {
          first: 'lejeme',
          second: 'lejete',
          third: 'lejí',
        },
        future: {
          first: 'budeme lít',
          second: 'budete lít',
          third: 'budou lít',
        },
        imperative: {
          second: 'lejme',
          third: 'lejte',
        },
        past: {
          first: { masculine: 'lili', feminine: 'lily', neutral: 'lila', },
          second: { masculine: 'lili', feminine: 'lily', neutral: 'lila', },
          third: { masculine: 'lili', feminine: 'lily', neutral: 'lila', },
        },
        passive: {
          first: { masculine: 'liti', feminine: 'lity', neutral: 'lita', },
          second: { masculine: 'liti', feminine: 'lity', neutral: 'lita', },
          third: { masculine: 'liti', feminine: 'lity', neutral: 'lita', },
        },
        participle: 'lejíce',
      },
    });
  }
}

// Source file: 008-133.html
export class Vstoupit extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.vstoupit`;
  constructor() {
    super({
      verb: 'vstoupit',
      infinitive: 'vstoupit',
      singular: {
        present: {
          first: 'vstupuji',
          second: 'vstupujěš',
          third: 'vstupuje',
        },
        future: {
          first: 'vstoupím',
          second: 'vstoupíš',
          third: 'vstoupí',
        },
        past: {
          first: { masculine: 'vstoupil', feminine: 'vstoupila', neutral: 'vstoupilo', },
          second: { masculine: 'vstoupil', feminine: 'vstoupila', neutral: 'vstoupilo',  },
          third: { masculine: 'vstoupil', feminine: 'vstoupila', neutral: 'vstoupilo',  },
        },
        passive: {
            first: { masculine: 'vstoupil', feminine: 'vstoupila', neutral: 'vstoupilo', },
            second: { masculine: 'vstoupil', feminine: 'vstoupila', neutral: 'vstoupilo',  },
            third: { masculine: 'vstoupil', feminine: 'vstoupila', neutral: 'vstoupilo',  },
        },
        imperative: 'vstup',
        participle: {
          masculine: 'vstoupiv',
          feminine: 'vstoupivši',
          neutral: 'vstoupivši',
        },
      },
      plural: {
        present: {
          first: 'vstupujeme',
          second: 'vstupujete',
          third: 'vstupují',
        },
        future: {
          first: 'vstoupíme',
          second: 'vstoupíte',
          third: 'vstoupí',
        },
        past: {
          first: { masculine: 'vstoupili', feminine: 'vstoupily', neutral: 'vstoupila', },
          second: { masculine: 'vstoupili', feminine: 'vstoupily', neutral: 'vstoupila', },
          third: { masculine: 'vstoupili', feminine: 'vstoupily', neutral: 'vstoupila', },
        },
        passive: {
            first: { masculine: 'vstoupili', feminine: 'vstoupily', neutral: 'vstoupila',},
            second: { masculine: 'vstoupili', feminine: 'vstoupily', neutral: 'vstoupila', },
            third: { masculine: 'vstoupili', feminine: 'vstoupily', neutral: 'vstoupila', },
        },
        imperative: {
          second: 'vstupme',
          third: 'vstupte',
        },
        participle: 'vstoupivše',
      },
    });
  }
}

// Source file: 003-198.html
export class Opustit extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.opustit`;
  constructor() {
    super({
      verb: 'opustit',
      infinitive: 'opustit',
      singular: {
        present: {
          first: 'opouštím',
          second: 'opouštíš',
          third: 'opouští',
        },
        future: {
          first: 'opustím',
          second: 'opustíš',
          third: 'opustí',
        },
        imperative: 'opusť',
        past: {
          first: { masculine: 'opustil', feminine: 'opustila', neutral: 'opustilo', },
          second: { masculine: 'opustil', feminine: 'opustila', neutral: 'opustilo',  },
          third: { masculine: 'opustil', feminine: 'opustila', neutral: 'opustilo',  },
        },
        passive: {
          first: { masculine: 'opuštěn', feminine: 'opuštěna', neutral: 'opuštěno', },
          second: { masculine: 'opuštěn', feminine: 'opuštěna', neutral: 'opuštěno', },
          third: { masculine: 'opuštěn', feminine: 'opuštěna', neutral: 'opuštěno', },
        },
        participle: {
          masculine: 'opustiv',
          feminine: 'opustivši',
          neutral: 'opustivši',
        },
      },
      plural: {
        present: {
          first: 'opouštíme',
          second: 'opouštíte',
          third: 'opouští',
        },
        future: {
          first: 'opustíme',
          second: 'opustíte',
          third: 'opustí',
        },
        imperative: {
          second: 'opusťme',
          third: 'opusťte',
        },
        past: {
          first: { masculine: 'opustili', feminine: 'opustily', neutral: 'opustila', },
          second: { masculine: 'opustili', feminine: 'opustily', neutral: 'opustila', },
          third: { masculine: 'opustili', feminine: 'opustily', neutral: 'opustila', },
        },
        passive: {
          first: { masculine: 'opuštěni', feminine: 'opuštěny', neutral: 'opuštěna', },
          second: { masculine: 'opuštěni', feminine: 'opuštěny', neutral: 'opuštěna', },
          third: { masculine: 'opuštěni', feminine: 'opuštěny', neutral: 'opuštěna', },
        },
        participle: 'opustivše',
      },
    });
  }
}

export class Zaútočit extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.zaútočit`;
  constructor() {
    super({
      verb: 'zaútočit',
      infinitive: 'zaútočit',
      singular: {
        present: {
          first: 'útočím',
          second: 'útočíš',
          third: 'útočí',
        },
        future: {
          first: 'zaútočím',
          second: 'zaútočíš',
          third: 'zaútočí',
        },
        imperative: 'zaútoč',
        passive: {
          first: { masculine: 'zaútočen', feminine: 'zaútočena', neutral: 'zaútočeno', },
          second: { masculine: 'zaútočen', feminine: 'zaútočena', neutral: 'zaútočeno', },
          third: { masculine: 'zaútočen', feminine: 'zaútočena', neutral: 'zaútočeno', },
        },
        participle: {
          masculine: 'zaútoče',
          feminine: 'zaútočíc',
          neutral: 'zaútočíc',
        },
      },
      plural: {
        present: {
          first: 'útočíme',
          second: 'útočíte',
          third: 'útočí',
        },
        future: {
          first: 'zaútočíme',
          second: 'zaútočíte',
          third: 'zaútočíte',
        },
        imperative: {
          second: 'zaútočme',
          third: 'zaútočte',
        },
        passive: {
          first: { masculine: 'zaútočeni', feminine: 'zaútočeny', neutral: 'zaútočena', },
          second: { masculine: 'zaútočeni', feminine: 'zaútočeny', neutral: 'zaútočena', },
          third: { masculine: 'zaútočeni', feminine: 'zaútočeny', neutral: 'zaútočena', },
        },
        participle: 'zaútočíce',
      },
    });
  }
}

export class Odrazit extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.odrazit`;
  constructor() {
    super({
      verb: 'odrazit',
      infinitive: 'odrazit',
      singular: {
        present: {
          first: 'odrážím',
          second: 'odrážíš',
          third: 'odráží',
        },
        future: {
          first: 'budu odrážet',
          second: 'budeš odrážet',
          third: 'bude odrážet',
        },
        imperative: 'odrážej',
        passive: {
          first: { masculine: 'odrážen', feminine: 'odrážena', neutral: 'odráženo', },
          second: { masculine: 'odrážen', feminine: 'odrážena', neutral: 'odráženo', },
          third: { masculine: 'odrážen', feminine: 'odrážena', neutral: 'odráženo', },
        },
        participle: {
          masculine: 'odrážeje',
          feminine: 'odrážejíc',
          neutral: 'odrážejíc',
        },
      },
      plural: {
        present: {
          first: 'odrážíme',
          second: 'odrážíte',
          third: 'odráží',
        },
        future: {
          first: 'budeme odrážet',
          second: 'budete odrážet',
          third: 'budou odrážet',
        },
        imperative: {
          second: 'odrážejme',
          third: 'odrážejte',
        },
        passive: {
          first: { masculine: 'odráženi', feminine: 'odráženy', neutral: 'odrážena', },
          second: { masculine: 'odráženi', feminine: 'odráženy', neutral: 'odrážena', },
          third: { masculine: 'odráženi', feminine: 'odráženy', neutral: 'odrážena', },
        },
        participle: 'odrážejíce',
      },
    });
  }
}

export class Uhýbat extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.Uhýbat`;
  constructor() {
    super({
      verb: 'uhýbat',
      infinitive: 'uhnout',
      singular: {
        present: {
          first: 'uhýbám',
          second: 'uhýbáš',
          third: 'uhýbá',
        },
        future: {
          first: 'budu uhýbat',
          second: 'budeš uhýbat',
          third: 'bude uhýbat',
        },
        imperative: 'uhýbej',
        passive: {
          first: { masculine: 'uhýbán', feminine: 'uhýbána', neutral: 'uhýbáno', },
          second: { masculine: 'uhýbán', feminine: 'uhýbána', neutral: 'uhýbáno', },
          third: { masculine: 'uhýbán', feminine: 'uhýbána', neutral: 'uhýbáno', },
        },
        participle: {
          masculine: 'uhýbaje',
          feminine: 'uhýbajíc',
          neutral: 'uhýbajíc',
        },
      },
      plural: {
        present: {
          first: 'uhýbáme',
          second: 'uhýbáte',
          third: 'uhýbá',
        },
        future: {
          first: 'budeme uhýbat',
          second: 'budete uhýbat',
          third: 'budou uhýbat',
        },
        imperative: {
          second: 'uhýbejme',
          third: 'uhýbejte',
        },
        passive: {
          first: { masculine: 'uhýbáni', feminine: 'uhýbány', neutral: 'uhýbána', },
          second: { masculine: 'uhýbáni', feminine: 'uhýbány', neutral: 'uhýbána', },
          third: { masculine: 'uhýbáni', feminine: 'uhýbány', neutral: 'uhýbána', },
        },
        participle: 'uhýbajíce',
      },
    });
  }
}

// Source file: 008-037.html
export class Utéct extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.utéct`;
  constructor() {
    super({
      verb: 'utéct',
      infinitive: 'utéct',
      singular: {
        present: {
          first: 'uteču',
          second: 'utečeš',
          third: 'uteče',
        },
        future: {
          first: 'budu utéct',
          second: 'budeš utéct',
          third: 'bude utéct',
        },
        imperative: 'uteč',
        participle: {
          masculine: 'utek',
          feminine: 'utekši',
          neutral: 'utekši',
        },
      },
      plural: {
        present: {
          first: 'utečeme',
          second: 'utečete',
          third: 'utečou',
        },
        future: {
          first: 'budeme utéct',
          second: 'budete utéct',
          third: 'budou utéct',
        },
        imperative: {
          second: 'utečme',
          third: 'utečte',
        },
        participle: 'utekše',
      },
    });
  }
}

// Source file: 008-079.html
export class VěnovatSe extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.věnovat-se`;
  constructor() {
    super({
      verb: 'věnovat se',
      infinitive: 'věnovat se',
      singular: {
        present: {
          first: 'věnuji se',
          second: 'věnuješ se',
          third: 'věnuje se',
        },
        future: {
          first: 'budu se věnovat',
          second: 'budeš se věnovat',
          third: 'bude se věnovat',
        },
        imperative: 'věnuj se',
        participle: {
          masculine: 'věnuje se',
          feminine: 'věnujíc se',
          neutral: 'věnujíc se',
        },
      },
      plural: {
        present: {
          first: 'věnujeme se',
          second: 'věnujete se',
          third: 'věnují se',
        },
        future: {
          first: 'budeme se věnovat',
          second: 'budete se věnovat',
          third: 'budou se věnovat',
        },
        imperative: {
          second: 'věnujme se',
          third: 'věnujte se',
        },
        participle: 'věnujíce se',
      },
    });
  }
}

// Source file: 011-104.html
export class Zasáhnout extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.zasáhnout`;
  constructor() {
    super({
      verb: 'zasáhnout',
      infinitive: 'zasáhnout',
      singular: {
        future: {
          first: 'zasáhnu',
          second: 'zasáhneš',
          third: 'zasáhne',
        },
        imperative: 'zasáhni',
        past: {
          first: { masculine: 'zasáhl', feminine: 'zasáhla', neutral: 'zasáhlo', },
          second: { masculine: 'zasáhl', feminine: 'zasáhla', neutral: 'zasáhlo', },
          third: { masculine: 'zasáhl', feminine: 'zasáhla', neutral: 'zasáhlo', },
        },
        passive: {
          first: { masculine: 'zasažen', feminine: 'zasažena', neutral: 'zasaženo', },
          second: { masculine: 'zasažen', feminine: 'zasažena', neutral: 'zasaženo', },
          third: { masculine: 'zasažen', feminine: 'zasažena', neutral: 'zasaženo', },
        },
        participle: {
          masculine: 'zasáhnuv',
          feminine: 'zasáhnuvši',
          neutral: 'zasáhnuvši',
        },
      },
      plural: {
        future: {
          first: 'zasáhneme',
          second: 'zasáhnete',
          third: 'zasáhnou',
        },
        imperative: {
          second: 'zasáhněme',
          third: 'zasáhněte',
        },
        past: {
          first: { masculine: 'zasáhli', feminine: 'zasáhly', neutral: 'zasáhla', },
          second: { masculine: 'zasáhli', feminine: 'zasáhly', neutral: 'zasáhla', },
          third: { masculine: 'zasáhli', feminine: 'zasáhly', neutral: 'zasáhla', },
        },
        passive: {
          first: { masculine: 'zasaženi', feminine: 'zasaženy', neutral: 'zasažena', },
          second: { masculine: 'zasaženi', feminine: 'zasaženy', neutral: 'zasažena', },
          third: { masculine: 'zasaženi', feminine: 'zasaženy', neutral: 'zasažena', },
        },
        participle: 'zasáhnuvše',
      },
    });
  }
}

// Source file: 010-050.html
export class Zemřít extends lang.Verb {
  public static readonly ID = `${lang.Verb.ID}.zemřít`;
  constructor() {
    super({
      verb: 'zemřít',
      infinitive: 'zemřít',
      singular: {
        future: {
          first: 'zemřu',
          second: 'zemřeš',
          third: 'zemře',
        },
        past: {
          first: 'zemřel',
          second: 'zemřela',
          third: { masculine: 'zemřel', feminine: 'zemřela', neutral: 'zemřelo', },
        },
        imperative: 'zemři',
        participle: {
          masculine: 'zemřev',
          feminine: 'zemřevši',
          neutral: 'zemřevši',
        },
      },
      plural: {
        future: {
          first: 'zemřeme',
          second: 'zemřete',
          third: 'zemřou',
        },
        past: {
          first: 'zemřeli',
          second: 'zemřely',
          third: { masculine: 'zemřeli', feminine: 'zemřely', neutral: 'zemřela', },
        },
        imperative: {
          second: 'zemřeme',
          third: 'zemřete',
        },
        participle: 'zemřevše',
      },
    });
  }
}

