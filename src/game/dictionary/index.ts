import {FrameworkConfiguration} from 'aurelia-framework';
import * as lang from 'snowatch/language-structures';

import {configureVerbs} from 'game/dictionary/verbs';
import {configureNouns} from 'game/dictionary/nouns';
import {configureConjunctions} from 'game/dictionary/conjunctions';
import {configureAdjectives} from 'game/dictionary/adjectives';
import {configurePronouns} from 'game/dictionary/pronouns';
import {configurePrepositions} from 'game/dictionary/prepositions';
import {configureAdverbs} from 'game/dictionary/adverbs';

export function configure(aurelia: FrameworkConfiguration) {
  configureVerbs(aurelia);
  configureNouns(aurelia);
  configureConjunctions(aurelia);
  configureAdjectives(aurelia);
  configurePronouns(aurelia);
  configurePrepositions(aurelia);
  configureAdverbs(aurelia);
}
