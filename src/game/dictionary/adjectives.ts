import {FrameworkConfiguration} from 'aurelia-framework';

import {configureAdjectivesWiki} from 'game/dictionary/adjectives-wiki';
export * from 'game/dictionary/adjectives-wiki';

import * as lang from 'snowatch/language-structures';

export function configureAdjectives(aurelia: FrameworkConfiguration) {
  configureAdjectivesWiki(aurelia);
  aurelia.singleton(lang.LanguageStructure, Bílý);
  aurelia.singleton(lang.LanguageStructure, Levý);
  aurelia.singleton(lang.LanguageStructure, Pravý);
  aurelia.singleton(lang.LanguageStructure, Omráčený);
}

export class Bílý  extends lang.Adjective {
  public static readonly ID = `${lang.Adjective.ID}.bílý`;

  constructor() {
    super(
      {
        adjective: 'bílý',
        singular: {
          masculine: {
            nominative: 'bílý',
            genitive: 'bílého',
            dative: 'bílému',
            accusative: { animate: 'bílého', inanimate: 'bílý' },
            vocative: 'bílý',
            locative: 'bílém',
            instrumental: 'bílým',
          },
          feminine: {
            nominative: 'bílá',
            genitive: 'bílé',
            dative: 'bílé',
            accusative: 'bílou',
            vocative: 'bílá',
            locative: 'bílé',
            instrumental: 'bílou',
          },
          neutral: {
            nominative: 'bílé',
            genitive: 'bílého',
            dative: 'bílému',
            accusative: 'bílé',
            vocative: 'bílé',
            locative: 'bílém',
            instrumental: 'bílým',
          },
        },
        plural: {
          masculine: {
            nominative: 'bílí',
            genitive: 'bílých',
            dative: 'bílým',
            accusative: 'bílé',
            vocative: 'bílí',
            locative: 'bílých',
            instrumental: 'bílými',
          },
          feminine: {
            nominative: 'bílé',
            genitive: 'bílých',
            dative: 'bílým',
            accusative: 'bílé',
            vocative: 'bílé',
            locative: 'bílých',
            instrumental: 'bílými',
          },
          neutral: {
            nominative: 'bílá',
            genitive: 'bílých',
            dative: 'bílým',
            accusative: 'bílá',
            vocative: 'bílá',
            locative: 'bílých',
            instrumental: 'bílými',
          },
        }
      }
    );
  }
}

export class Levý  extends lang.Adjective {
  public static readonly ID = `${lang.Adjective.ID}.levý`;

  constructor() {
    super(
      {
        adjective: 'levý',
        singular: {
          masculine: {
            nominative: 'levý',
            genitive: 'levého',
            dative: 'levému',
            accusative: { animate: 'levého', inanimate: 'levý' },
            vocative: 'levý',
            locative: 'levém',
            instrumental: 'levým',
          },
          feminine: {
            nominative: 'levá',
            genitive: 'levé',
            dative: 'levé',
            accusative: 'levou',
            vocative: 'levá',
            locative: 'levé',
            instrumental: 'levou',
          },
          neutral: {
            nominative: 'levé',
            genitive: 'levého',
            dative: 'levému',
            accusative: 'levé',
            vocative: 'levé',
            locative: 'levém',
            instrumental: 'levým',
          },
        },
        plural: {
          masculine: {
            nominative: 'leví',
            genitive: 'levých',
            dative: 'levým',
            accusative: 'levé',
            vocative: {
              animate: 'leví',
              inanimate: 'levé',
            },
            locative: 'levých',
            instrumental: 'levými',
          },
          feminine: {
            nominative: 'levé',
            genitive: 'levých',
            dative: 'levým',
            accusative: 'levé',
            vocative: 'levé',
            locative: 'levých',
            instrumental: 'levými',
          },
          neutral: {
            nominative: 'levá',
            genitive: 'levých',
            dative: 'levým',
            accusative: 'levá',
            vocative: 'levá',
            locative: 'levých',
            instrumental: 'levými',
          },
        }
      }
    );
  }
}

export class Pravý  extends lang.Adjective {
  public static readonly ID = `${lang.Adjective.ID}.pravý`;

  constructor() {
    super(
      {
        adjective: 'pravý',
        singular: {
          masculine: {
            nominative: 'pravý',
            genitive: 'pravého',
            dative: 'pravému',
            accusative: { animate: 'pravého', inanimate: 'pravý' },
            vocative: 'pravý',
            locative: 'pravém',
            instrumental: 'pravým',
          },
          feminine: {
            nominative: 'pravá',
            genitive: 'pravé',
            dative: 'pravé',
            accusative: 'pravou',
            vocative: 'pravá',
            locative: 'pravé',
            instrumental: 'pravou',
          },
          neutral: {
            nominative: 'pravé',
            genitive: 'pravého',
            dative: 'pravému',
            accusative: 'pravé',
            vocative: 'pravé',
            locative: 'pravém',
            instrumental: 'pravým',
          },
        },
        plural: {
          masculine: {
            nominative: 'praví',
            genitive: 'pravých',
            dative: 'pravým',
            accusative: 'pravé',
            vocative: {
              animate: 'praví',
              inanimate: 'pravé',
            },
            locative: 'pravých',
            instrumental: 'pravými',
          },
          feminine: {
            nominative: 'pravé',
            genitive: 'pravých',
            dative: 'pravým',
            accusative: 'pravé',
            vocative: 'pravé',
            locative: 'pravých',
            instrumental: 'pravými',
          },
          neutral: {
            nominative: 'pravá',
            genitive: 'pravých',
            dative: 'pravým',
            accusative: 'pravá',
            vocative: 'pravá',
            locative: 'pravých',
            instrumental: 'pravými',
          },
        }
      }
    );
  }
}

export class Omráčený extends lang.Adjective {
  public static readonly ID = `${lang.Adjective.ID}.omráčený`;

  constructor() {
    super(
      {
        adjective: 'omráčený',
        singular: {
          masculine: {
            nominative: 'omráčený',
            genitive: 'omráčeného',
            dative: 'omráčenému',
            accusative: { animate: 'omráčeného', inanimate: 'omráčený' },
            vocative: 'omráčený',
            locative: 'omráčeném',
            instrumental: 'omráčeným',
          },
          feminine: {
            nominative: 'omráčená',
            genitive: 'omráčené',
            dative: 'omráčené',
            accusative: 'omráčenou',
            vocative: 'omráčená',
            locative: 'omráčené',
            instrumental: 'omráčenou',
          },
          neutral: {
            nominative: 'omráčené',
            genitive: 'omráčeného',
            dative: 'omráčenému',
            accusative: 'omráčené',
            vocative: 'omráčené',
            locative: 'omráčeném',
            instrumental: 'omráčeným',
          },
        },
        plural: {
          masculine: {
            nominative: 'omráčení',
            genitive: 'omráčených',
            dative: 'omráčeným',
            accusative: 'omráčené',
            vocative: {
              animate: 'omráčení',
              inanimate: 'omráčené',
            },
            locative: 'omráčených',
            instrumental: 'omráčenými',
          },
          feminine: {
            nominative: 'omráčené',
            genitive: 'omráčených',
            dative: 'omráčeným',
            accusative: 'omráčené',
            vocative: 'omráčené',
            locative: 'omráčených',
            instrumental: 'omráčenými',
          },
          neutral: {
            nominative: 'omráčená',
            genitive: 'omráčených',
            dative: 'omráčeným',
            accusative: 'omráčená',
            vocative: 'omráčená',
            locative: 'omráčených',
            instrumental: 'omráčenými',
          },
        }
      }
    );
  }
}
