// import {DialogueLine, Intentions, DialogueLineInit} from 'snowatch/dialogue-line';
// import {GameData} from 'snowatch/game-data';
// import {Entity, updateInstanceIds} from 'snowatch/entity';
// import {LastOne} from 'actors/last-one';
// import {White} from 'actors/white';
// import {Modifier} from 'snowatch/skill';
// import {Inventory} from 'snowatch/inventory';
// import * as result from 'snowatch/result';

// export class PlayerDialogueLinesIds {
//   static greetingsInformal = 'player-greetings-informal';
//   static greetingsFormal = 'player-greetings-formal';
//   static speakingToSelf = 'player-speaking-to-self';
// }

// export class WandererDialogueLinesIds {
//   static greetingsInformal = 'wanderer-greetings-informal';
//   static askAboutDuckie = 'wanderer-ask-about-duckie';
//   static gotTheDuckie = 'wanderer-got-the-duckie';
//   static sayTheCode = 'wanderer-tell-the-code';
//   static mumble = 'wanderer-mumble';
//   static duckieIsBack = 'wanderer-duckie-is-back';
// }

// export function getPlayerDialogueLines(): Array<DialogueLineInit> {
//   return [
//     {
//       id: PlayerDialogueLinesIds.greetingsInformal,
//       intentions: [Intentions.informal.id,],
//       title: "Hi",
//       content(game: GameData, line: DialogueLine, lineState: any, actor: Entity, target: Entity, details: any) {
//         return "Hi!";
//       },
//       filter : filterPlayerCharacterLines,
//     },
//     {
//       id: PlayerDialogueLinesIds.greetingsFormal,
//       intentions: [Intentions.formal.id,],
//       title: "Good morning",
//       content(game: GameData, line: DialogueLine, lineState: any, actor: Entity, target: Entity, details: any) {
//         return "Good morning, sir!";
//       },
//       filter : filterPlayerCharacterLines,
//     },
//     {
//       id: PlayerDialogueLinesIds.speakingToSelf,
//       intentions: [Intentions.informal.id,],
//       title: "Speaking to myself",
//       content(game: GameData, line: DialogueLine, lineState: any, actor: Entity, target: Entity, details: any) {
//         return {
//           variants: ["So now I'm speaking to myself? Great...",],
//           default: "Oh, shut up, me!",
//         }
//       },
//       filter : onlyWhenReplyingToSelf,
//       lineScore: getPlayerDialogueLineScore,
//     },
//   ];
// }

// export function getWandererDialogueLines(): Array<DialogueLineInit> {
//   return [
//     {
//       id: WandererDialogueLinesIds.greetingsInformal,
//       intentions: [Intentions.informal.id,],
//       title: "Opener",
//       content(game: GameData, line: DialogueLine, lineState: any, actor: Entity, target: Entity, details: any) {
//         this._content = this._content ?? [
//           "Hey, buddy, listen...",
//           "Come on, man, talk to me...",
//           "Don't be an asshole man...",
//           "Hey, sir, please...",
//         ];
//         return this._content;
//       },
//       filter : notToSelf,
//       lineScore: getWanderDialogueLineScore,
//     },
//     {
//       id: WandererDialogueLinesIds.askAboutDuckie,
//       intentions: [Intentions.informal.id,],
//       title: "Duckie?",
//       content(game: GameData, line: DialogueLine, lineState: any, actor: Entity, target: Entity, details: any) {
//         this._content = this._content ?? [
//           "Hey, have you seen my lovely duckie?",
//           "I'm looking for my little duckie.",
//           "By any chance, have you seen my little lovely duck?",
//           "I've lost my little duck. I can't find it. Please, help me!",
//         ];
//         return this._content;
//       },
//       filter : notToSelf,
//       lineScore: getWanderDialogueLineScore,
//     },
//     {
//       id: WandererDialogueLinesIds.mumble,
//       intentions: [Intentions.informal.id,],
//       title: "Mumbling",
//       content(game: GameData, line: DialogueLine, lineState: any, actor: Entity, target: Entity, details: any) {
//         this._content = this._content ?? "[Mumbles...]";
//         return this._content;
//       },
//       filter : notToSelf,
//       lineScore: getWanderDialogueLineScore,
//     },
//     {
//       id: WandererDialogueLinesIds.gotTheDuckie,
//       intentions: [Intentions.informal.id,],
//       title: "Got duckie",
//       content(game: GameData, line: DialogueLine, lineState: any, actor: Entity, target: Entity, details: any) {
//         this._content = this._content ?? "This is my duckie!";
//         return this._content;
//       },
//       filter : notToSelf,
//       lineScore: getWanderDialogueLineScore,
//     },
//     {
//       id: WandererDialogueLinesIds.sayTheCode,
//       intentions: [Intentions.informal.id,],
//       knowledge: [DoorSecureCode.ID,],
//       title: "The code",
//       content(game: GameData, line: DialogueLine, lineState: any, actor: Entity, target: Entity, details: any) {
//         this._content = this._content ?? "I remember this... 1234... That's the number...";
//         return this._content;
//       },
//       filter : notToSelf,
//       lineScore: getWanderDialogueLineScore,
//     },
//     {
//       id: WandererDialogueLinesIds.duckieIsBack,
//       intentions: [Intentions.informal.id,],
//       title: "Thanks",
//       content(game: GameData, line: DialogueLine, lineState: any, actor: Entity, target: Entity, details: any) {
//         this._content = this._content ?? {
//           variants: [
//             "Now she is safe. You know, my lovely little duckie.",
//             "Things are fine again. I've got her back.",
//           ],
//           default: "Thanks, man. My duckie is safe again.",
//         };
//         return this._content;
//       },
//       filter : notToSelf,
//       lineScore: getWanderDialogueLineScore,
//     }
//   ];
// }

// function always(game: GameData, line: DialogueLine, lineState: any, actor: Entity, target: Entity, details: any) : result.ResultObject {
//   return result.make(result.accepted);
// }

// function notToSelf(game: GameData, line: DialogueLine, lineState: any, actor: Entity, target: Entity, details: any) : result.ResultObject {
//   return (actor.id === target.id && details != null && details.responseTo != null ) ? result.make(result.rejected) : result.make(result.accepted);
// }

// function onlyWhenReplyingToSelf(game: GameData, line: DialogueLine, lineState: any, actor: Entity, target: Entity, details: any) : result.ResultObject {
//   if( line.id === PlayerDialogueLinesIds.speakingToSelf && details != null && details.responseTo != null ) {
//     return result.make(result.accepted);
//   } else {
//     return result.make(result.rejected);
//   }
// }

// function filterPlayerCharacterLines(game: GameData, line: DialogueLine, lineState: any, actor: Entity, target: Entity, details: any) : result.ResultObject {
//   return result.make(result.accepted);

//   // if( line.visits(lineState, target.id).length === 0) {
//   //   return result.make(result.accepted);
//   // } else {
//   //   return result.make(result.rejected);
//   // }
// }

// function getPlayerDialogueLineScore(game: GameData, line: DialogueLine, lineState: any, actor: Entity, target: Entity, details: any) : number {
//   return  actor.id === target.id ? 10 : 0;
// }

// function getWanderDialogueLineScore(game: GameData, line: DialogueLine, lineState: any, actor: Entity, target: Entity, details: any) : number {
//   // if wanderer doesn't have a duck...
//   if( !Inventory.hasInAny(actor, RubberDuck.ID) ) {
//     // ...ask about it
//     if( details != null && details.responseTo != null ) {
//       return line.id === WandererDialogueLinesIds.askAboutDuckie ?  1 : 0;
//     } else {
//       return line.id === WandererDialogueLinesIds.mumble ?  1 : 0;
//     }
//   }

//   switch( line.id ) {
//     case WandererDialogueLinesIds.duckieIsBack:
//       return Math.max(0.5, 0);
//   }
//   return 0;
// }