import {ResourceDictionary} from 'snowatch/resource-dictionary';

import * as $dict from 'game/dictionary/dict';

export class ActionResDict extends ResourceDictionary {
  public static readonly CRYSTAL_PRESSED_WEAKLY_DESC = 'crystal-presses-weakly-desc-id';
  public static readonly FEELING_WHEN_STRENGTH_GOES_UP_DESC = 'feeling-when-strength-goes-up-desc-id';
  public static readonly FEELING_WHEN_STRENGTH_GOES_DOWN_DESC = 'feeling-when-strength-goes-down-desc-id';
  public static readonly FEELING_WHEN_HEALTH_GOES_UP_DESC = 'feeling-when-health-goes-up-desc-id';
  public static readonly FEELING_WHEN_HEALTH_GOES_DOWN_DESC = 'feeling-when-health-goes-down-desc-id';
  public static readonly FEELING_WHEN_AGILITY_GOES_UP_DESC = 'feeling-when-agility-goes-up-desc-id';
  public static readonly FEELING_WHEN_AGILITY_GOES_DOWN_DESC = 'feeling-when-agility-goes-down-desc-id';
  public static readonly FEELING_WHEN_STAMINA_GOES_UP_DESC = 'feeling-when-stamina-goes-up-desc-id';
  public static readonly FEELING_WHEN_STAMINA_GOES_DOWN_DESC = 'feeling-when-stamina-goes-down-desc-id';
  public static readonly FEELING_WHEN_INTELLIGENCE_GOES_UP_DESC = 'feeling-when-intelligence-goes-up-desc-id';
  public static readonly FEELING_WHEN_INTELLIGENCE_GOES_DOWN_DESC = 'feeling-when-intelligence-goes-down-desc-id';
  public static readonly FEELING_WHEN_DEXTERITY_GOES_UP_DESC = 'feeling-when-dexterity-goes-up-desc-id';
  public static readonly FEELING_WHEN_DEXTERITY_GOES_DOWN_DESC = 'feeling-when-dexterity-goes-down-desc-id';

  public static readonly BEAMS_ACTIVATED_SOUND_DESC = 'beams-activated-sound-desc';
  public static readonly BEAM_ACTIVATED_APPEARANCE_CHANGE_DESC = 'beams-activated-appearance-change-desc';


  public static readonly DISINFECTANT_LIQUID_SMELL_DESC = 'disinfectant-liquid-smell-desc';

  constructor() {
    super({
      culture: 'cz',
      resources: [
        { id: ActionResDict.CRYSTAL_PRESSED_WEAKLY_DESC, value: {
          template: 'Jak ${actorNoun} ${pressVerb} krystal, ${feelVerb} těžkou, otupující únavu a téměř neodolatelnou touhu prostě jen zavřít oči a spát.',
          feelVerbId: $dict.Cítit.ID,
          pressVerbId: $dict.Stisknout.ID
        }},


        { id: ActionResDict.FEELING_WHEN_STRENGTH_GOES_UP_DESC, value: {
          template: '${actorNoun} se ${feelVerb} silnější.',
          feelVerbId: $dict.Cítit.ID,
        }},
        { id: ActionResDict.FEELING_WHEN_STRENGTH_GOES_DOWN_DESC, value: {
          template: '${actorNoun} se ${feelVerb} slabší.',
          feelVerbId: $dict.Cítit.ID,
        }},
        { id: ActionResDict.FEELING_WHEN_HEALTH_GOES_UP_DESC, value: {
          template: '${actorNoun} ${feelVerb} odolnější.',
          feelVerbId: $dict.Cítit.ID,
        }},
        { id: ActionResDict.FEELING_WHEN_HEALTH_GOES_DOWN_DESC, value: {
          template: '${actorNoun} se ${feelVerb} méně odolně.',
          feelVerbId: $dict.Cítit.ID,
        }},
        { id: ActionResDict.FEELING_WHEN_AGILITY_GOES_UP_DESC, value: {
          template: '${actorNoun} se ${feelVerb} hbitější.',
          feelVerbId: $dict.Cítit.ID,
        }},
        { id: ActionResDict.FEELING_WHEN_AGILITY_GOES_DOWN_DESC, value: {
          template: '${actorNoun} se ${feelVerb} méně hbitě.',
          feelVerbId: $dict.Cítit.ID,
        }},
        { id: ActionResDict.FEELING_WHEN_STAMINA_GOES_UP_DESC, value: {
          template: '${actorNoun} se ${feelVerb} vytrvalejší.',
          feelVerbId: $dict.Cítit.ID,
        }},
        { id: ActionResDict.FEELING_WHEN_STAMINA_GOES_DOWN_DESC, value: {
          template: '${actorNoun} se ${feelVerb} méně vytrvale.',
          feelVerbId: $dict.Cítit.ID,
        }},
        { id: ActionResDict.FEELING_WHEN_INTELLIGENCE_GOES_UP_DESC, value: {
          template: '${actorNoun} se ${feelVerb} vnímavější.',
          feelVerbId: $dict.Cítit.ID,
        }},
        { id: ActionResDict.FEELING_WHEN_INTELLIGENCE_GOES_DOWN_DESC, value: {
          template: '${actorNoun} se ${feelVerb} méně vnímavě.',
          feelVerbId: $dict.Cítit.ID,
        }},
        { id: ActionResDict.FEELING_WHEN_DEXTERITY_GOES_UP_DESC, value: {
          template: '${actorNoun} se ${feelVerb} zručnější.',
          feelVerbId: $dict.Cítit.ID,
        }},
        { id: ActionResDict.FEELING_WHEN_DEXTERITY_GOES_DOWN_DESC, value: {
          template: '${actorNoun} se ${feelVerb} méně zručně.',
          feelVerbId: $dict.Cítit.ID,
        }},

        { id: ActionResDict.BEAMS_ACTIVATED_SOUND_DESC, value: {
          template: 'Z lodi kostela se ozval hluboký, dunivý zvuk.',
        }},
        { id: ActionResDict.BEAM_ACTIVATED_APPEARANCE_CHANGE_DESC, value: {
          template: '${beamNoun} v lodi kostela se jasně rozzářil.',
        }},
      ],
    });
  }
}
