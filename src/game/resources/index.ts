import {FrameworkConfiguration} from 'aurelia-framework';

import {ResourceDictionary} from 'snowatch/resource-dictionary';
import {CzechSystemResDict} from 'game/resources/cz-system-resource-dictionary';
import * as $gameres from 'game/resources/game-res';

export function configure(aurelia: FrameworkConfiguration) {
  aurelia.singleton(ResourceDictionary, CzechSystemResDict);
  aurelia.singleton(ResourceDictionary, $gameres.EntityResDict);
  aurelia.singleton(ResourceDictionary, $gameres.SceneResDict);
  aurelia.singleton(ResourceDictionary, $gameres.ActionResDict);
}
