import {ResourceDictionary} from 'snowatch/resource-dictionary';

import * as $dict from 'game/dictionary/dict';

export class SceneResDict extends ResourceDictionary {
  public static readonly SCENE_DESC_INTRO_WAKEUP = 'scene-desc-intro-wakeup';
  public static readonly SCENE_DESC_INTRO_FALL = 'scene-desc-intro-fall';
  public static readonly SCENE_DESC_INTRO_GO_SLEEP = 'scene-desc-go-sleep';

  public static readonly SCENE_TITLE_HUB_CHANCEL = 'scene-title-hub-chancel';
  public static readonly SCENE_DESC_HUB_CHANCEL = 'scene-desc-hub-chancel';
  public static readonly SCENE_TITLE_HUB_NAVE = 'scene-title-hub-nave';
  public static readonly SCENE_DESC_HUB_NAVE = 'scene-desc-hub-nave';
  public static readonly SCENE_TITLE_HUB_SACRISTY = 'scene-title-hub-sacristy';
  public static readonly SCENE_DESC_HUB_SACRISTY = 'scene-desc-hub-sacristy';
  public static readonly SCENE_DESC_HUB_SACRISTY_FIRST_TIME_LEAVING = 'scene-desc-hub-sacristy-first-time-leaving';
  public static readonly SCENE_TITLE_HUB_TOWER_BASE = 'scene-title-hub-tower-base';
  public static readonly SCENE_DESC_HUB_TOWER_BASE = 'scene-desc-hub-tower-base';
  public static readonly SCENE_TITLE_WHITE_LOBBY = 'scene-title-white-lobby';
  public static readonly SCENE_DESC_WHITE_LOBBY = 'scene-desc-white-lobby';
  public static readonly SCENE_TITLE_WHITE_ENTERING_FOR_THE_FIRST_TIME = 'scene-title-white-for-the-first-time';
  public static readonly SCENE_DESC_WHITE_ENTERING_FOR_THE_FIRST_TIME = 'scene-desc-white-for-the-first-time';

  public static readonly SCENE_TITLE_NOUN_HALLWAY = 'scene-title-noun-hallway';
  public static readonly SCENE_TITLE_WHITE_LEFT_HALLWAY = 'scene-title-white-left-hallway';
  public static readonly SCENE_DESC_WHITE_LEFT_HALLWAY = 'scene-desc-white-left-hallway';
  public static readonly SCENE_TITLE_WHITE_RIGHT_HALLWAY = 'scene-title-white-right-hallway';
  public static readonly SCENE_DESC_WHITE_RIGHT_HALLWAY = 'scene-desc-white-right-hallway';

  public static readonly SCENE_TITLE_NOUN_SERVICE_ROOM = 'scene-title-noun-service-room';
  public static readonly SCENE_TITLE_WHITE_LEFT_SERVICE_ROOM = 'scene-title-white-left-service-room';
  public static readonly SCENE_DESC_WHITE_LEFT_SERVICE_ROOM = 'scene-desc-white-left-service-room';
  public static readonly SCENE_TITLE_WHITE_RIGHT_SERVICE_ROOM = 'scene-title-white-right-service-room';
  public static readonly SCENE_DESC_WHITE_RIGHT_SERVICE_ROOM = 'scene-desc-white-right-service-room';

  public static readonly SCENE_TITLE_NOUN_UPSTAIRS = 'scene-title-noun-upstairs';
  public static readonly SCENE_TITLE_WHITE_LEFT_UPSTAIRS = 'scene-title-white-left-upstairs';
  public static readonly SCENE_DESC_WHITE_LEFT_UPSTAIRS = 'scene-desc-white-left-upstairs';
  public static readonly SCENE_TITLE_WHITE_RIGHT_UPSTAIRS = 'scene-title-white-right-upstairs';
  public static readonly SCENE_DESC_WHITE_RIGHT_UPSTAIRS = 'scene-desc-white-right-upstairs';

  public static readonly SCENE_TITLE_NOUN_RECEPTION = 'scene-title-noun-reception';
  public static readonly SCENE_TITLE_WHITE_LEFT_RECEPTION = 'scene-title-white-left-reception';
  public static readonly SCENE_DESC_WHITE_LEFT_RECEPTION = 'scene-desc-white-left-reception';
  public static readonly SCENE_TITLE_WHITE_RIGHT_RECEPTION = 'scene-title-white-right-reception';
  public static readonly SCENE_DESC_WHITE_RIGHT_RECEPTION = 'scene-desc-white-right-reception';

  public static readonly SCENE_TITLE_NOUN_LAB = 'scene-title-noun-lab';
  public static readonly SCENE_TITLE_WHITE_LEFT_LAB = 'scene-title-white-left-lab';
  public static readonly SCENE_DESC_WHITE_LEFT_LAB = 'scene-desc-white-left-lab';
  public static readonly SCENE_TITLE_WHITE_RIGHT_LAB = 'scene-title-white-right-lab';
  public static readonly SCENE_DESC_WHITE_RIGHT_LAB = 'scene-desc-white-right-lab';

  public static readonly SCENE_TITLE_NOUN_PATIENT_ROOM_SOUTH = 'scene-title-noun-patient-room-south';
  public static readonly SCENE_TITLE_WHITE_LEFT_PATIENT_ROOM_SOUTH = 'scene-title-white-left-patient-room-south';
  public static readonly SCENE_DESC_WHITE_LEFT_PATIENT_ROOM_SOUTH = 'scene-desc-white-left-patient-room-south';
  public static readonly SCENE_TITLE_WHITE_RIGHT_PATIENT_ROOM_SOUTH = 'scene-title-white-right-patient-room-south';
  public static readonly SCENE_DESC_WHITE_RIGHT_PATIENT_ROOM_SOUTH = 'scene-desc-white-right-patient-room-south';

  public static readonly SCENE_TITLE_NOUN_PATIENT_ROOM_NORTH = 'scene-title-noun-patient-room-north';
  public static readonly SCENE_TITLE_WHITE_LEFT_PATIENT_ROOM_NORTH = 'scene-title-white-left-patient-room-north';
  public static readonly SCENE_DESC_WHITE_LEFT_PATIENT_ROOM_NORTH = 'scene-desc-white-left-patient-room-north';
  public static readonly SCENE_TITLE_WHITE_RIGHT_PATIENT_ROOM_NORTH = 'scene-title-white-right-patient-room-north';
  public static readonly SCENE_DESC_WHITE_RIGHT_PATIENT_ROOM_NORTH = 'scene-desc-white-right-patient-room-north';

  public static readonly SCENE_TITLE_NOUN_BALCONY = 'scene-title-noun-balcony';
  public static readonly SCENE_TITLE_WHITE_LEFT_BALCONY = 'scene-title-white-left-balcony';
  public static readonly SCENE_DESC_WHITE_LEFT_BALCONY = 'scene-desc-white-left-balcony';
  public static readonly SCENE_TITLE_WHITE_RIGHT_BALCONY = 'scene-title-white-right-balcony';
  public static readonly SCENE_DESC_WHITE_RIGHT_BALCONY = 'scene-desc-white-right-balcony';

  public static readonly SCENE_TITLE_NOUN_MESS_HALL = 'scene-title-noun-mess-hall';
  public static readonly SCENE_TITLE_WHITE_LEFT_MESS_HALL = 'scene-title-white-left-mess-hall';
  public static readonly SCENE_DESC_WHITE_LEFT_MESS_HALL = 'scene-desc-white-left-mess-hall';
  public static readonly SCENE_TITLE_WHITE_RIGHT_MESS_HALL = 'scene-title-white-right-mess-hall';
  public static readonly SCENE_DESC_WHITE_RIGHT_MESS_HALL = 'scene-desc-white-right-mess-hall';

  public static readonly SCENE_TITLE_WHITE_TERRACE_ENTRANCE = 'scene-title-white-terrace-entrance';
  public static readonly SCENE_DESC_WHITE_TERRACE_ENTRANCE = 'scene-desc-white-terrace-entrance';

  public static readonly SCENE_TITLE_REALM_RED_ENTRY = 'scene-title-red-entry';
  public static readonly SCENE_DESC_RED_ENTRY = 'scene-desc-red-entry';
  public static readonly SCENE_TITLE_BLUE_ENTRY = 'scene-title-blue-entry';
  public static readonly SCENE_DESC_BLUE_ENTRY = 'scene-desc-blue-entry';
  public static readonly SCENE_TITLE_BLACK_ENTRY = 'scene-title-black-entry';
  public static readonly SCENE_DESC_BLACK_ENTRY = 'scene-desc-black-entry';
  public static readonly SCENE_TITLE_YELLOW_ENTRY = 'scene-title-yellow-entry';
  public static readonly SCENE_DESC_YELLOW_ENTRY = 'scene-desc-yellow-entry';
  public static readonly SCENE_TITLE_GREEN_ENTRY = 'scene-title-green-entry';
  public static readonly SCENE_DESC_GREEN_ENTRY = 'scene-desc-green-entry';

  public static readonly COMMAND_LABEL_WAKE_UP = 'command-label-wake-up';
  public static readonly COMMAND_LABEL_LOOK_AROUND = 'command-label-look-around';
  public static readonly COMMAND_LABEL_GO_OUT_OF_THE_ROOM = 'command-label-go-out-of-the-room';
  public static readonly COMMAND_LABEL_CONTINUE = 'command-label-continue';

  constructor() {
    super({
      culture: 'cz',
      resources: [
        { id: SceneResDict.SCENE_DESC_INTRO_WAKEUP, value: {
          template: 'Temnota byla nekonečná.\nPak se na jejích okrajích objevily nepatrné zářící tečky, které pozvolna pluly směrem ke středu zorného pole.',
        }},
        { id: SceneResDict.SCENE_DESC_INTRO_FALL, value: {
          template: '${fall}.\n' +
                  '${fall} temnotou, ale ta se postupně rozjasňovala, jak se z jiskřiček světla stávaly světlé šmouhy a jejich jas postupně sílil.\n' +
                  'Pak ${pronoun} na chvíli oslnilo jasné světlo a ${feel}, že ${fallDown}.',
          fallVerbId: $dict.Padat.ID,
          pronounMasculine: "ho",
          pronounFeminine: "ji",
          feelVerbId: $dict.Cítit.ID,
          fallDownVerbId: $dict.Dopadnout.ID,
        }},
        { id: SceneResDict.SCENE_DESC_INTRO_GO_SLEEP, value: {
          template: '${actorNoun} ${pressVerb} krystal vší silou. ${closeVerb} oči a ${sleepVerb}.',
          closeVerbId: $dict.Zavřít.ID,
          sleepVerbId: $dict.Usnout.ID,
          pressVerbId: $dict.Stisknout.ID
        }},
        { id: SceneResDict.SCENE_TITLE_HUB_CHANCEL, value: {
          template: 'Presbytář',
        }},
        { id: SceneResDict.SCENE_DESC_HUB_CHANCEL, value: {
          template: [
            `Půlkruh kněžiště tvořily napůl zhroucené zdi ze zčernalého pískovce, kusy kamene vydrolené a povalující se na podlaze. Prázdný prostor, kde kdysi nejspíše bývala kupole, vyplňovalo cosi, co se líně čeřilo jako hustá černá tekutina. Stejná substance vyplňovala i spáry obvodové zdi, pomalu pulzující, snažící se s neodvratnou trpělivostí protačit dovnitř a vyplnit všechen zbývající prostor.`,
            `Zdi kolem presbytáře se zdály dosahovat o něco výše, spáry mezi kvádry jako by byly menší. Ať černou hmotu vyplňující prostor venku zadržovalo cokoliv, bylo to teď silnější. Kupole stále chyběla, ale prostor nahoře měl skoro správný tvar.`,
            `Zárodek kupole nad presbytářem byl teď jasně zřetelný, ale k tomu, aby se řady kvádrů sešly v jejím vrcholu, stále kus chyběl. Černá kapalina byla teď vytlačena ze všech spár a dokonce o kus výš nad předpokládaný vrchol kupole, ale její ústup v sobě měl cosi dočasného, jako by bylo nevynetelné, že nad odpudivou silou, která ji udržovala venku nakonec zvítězí.`,
            `Zdi kolem presbytáře teď dosahovaly skoro až k vrchlíku kupole. Spáry, jakkoliv stále jasně viditelné, byly vyplněny maltou, kámen působil opotřebovaně, ale stále byl v solidním stavu. Ze zdí vycházelo světlo, přes které už nebylo vidět temnou hmotu venku.`,
            `Kupole nad presbytářem se uzavřela a všechno zdivo bylo v dobrém stavu. Tu a tam byl pískovec trochu zašlý, místy se lehce drolil, ale celý prostor teď působil jasně a vzdušně.`,
            `Prostorný presbytář v čele nevelikého kostela byl jasně osvětlen světlem vycházejícím ze zdí. Nahoře prostor uzavírala polokoule kupole.`,
            `Prostorný presbytář byl jasně osvětlen světlem vycházejícím ze zdí. Pískovec zářil novotou, všechny hrany ostré, spáry sotva znatelné, kupole dokonale symetrická.`,
          ]
        }},
        { id: SceneResDict.SCENE_TITLE_HUB_NAVE, value: {
          template: 'Hlavní loď',
        }},
        { id: SceneResDict.SCENE_DESC_HUB_NAVE, value: {
          template: [
            'Stejně jako presbytář, i loď kostela byla v posledním stádiu rozkladu. Zdivo bylo vydrolené, spárami v obvodových zdech a chybějící klenbou se dovnitř tlačila černá hmota zvenku. Po každé straně lodi byly v pravidelných rozestupech tři mihotající se paprsky světla, každý půldruhého metru v průměru. Světlo zářilo nejjasněji u podlahy, s rostoucí výškou se postupně vytrácelo.',
            'Zdi lodi se tyčily dostatečně vysoko, aby bylo patrné, že ve zdech v místech, kde byly paprsky světla, kdysi byla okna.',
            'Obvodové zdi lodi teď dosahovaly skoro dost vysoko na to, aby byla zřejmá klenba střechy. Okna byla prázdná, ale jejich lomené oblouky byly kompletní. Paprsky světla dosahovaly do výš nějakých patnácti metrů, než se docela vytratily.',
            'Klenba nad lodí byla zcela uzavřena. Lomené oblouhky, které ji nesly stále nevypadaly úplně stabilně, ale černá hmota venku byla bezpečně vytlačena ven. V oknech byly pozůstatky vitráží, barvou kdysi zřejmě odpovídající sloupům světla před nimy.',
            'Loď kostela byla plná jasného světla vycházejícího z jasně žlutého pískovce. Symetrická struktura lomeného klenboví byla bez zřejmých stop poškození, vitráže v oknech byly úplné a přesto, že prostor venku by nepochybně stále plný číhající temné hmoty, zářily komplikovanými strukturami, jako by do nich ze všech stran svítilo jasné ranní slunce.',
            'Přesto, že kostel nebyl veliký, působila loď téměř majestátním dojmem. Lomené oblouky stropu se setkávaly v pravidelných vzorech, okna byla plná světla a zdálo se, jako by se barevné struktury v nich pozvolna přelévaly a měnily.',
          ]
        }},
        { id: SceneResDict.SCENE_TITLE_HUB_SACRISTY, value: {
          template: 'Sakristie',
        }},
        { id: SceneResDict.SCENE_DESC_HUB_SACRISTY, value: {
          template: [
            'Malá místnost byla tmavá a prázdná. Strop nebylo vidět, ale prostor nahoře byl vyplněný něčím temným a trpělivě čekajícím na svou příležitost. Otvor ve zdi, kde nejspíš kdysi bývaly dveře, vedl do další, mnohem větší místnosti.',
            'Sakristie byla tmavá a prázdná. Strop nad místností působil vetchým dojmem. Těžce poškozeným lomeným obloukem se dalo projít do presbytáře.',
            'Sakristie vypadala téměř obyvatelně. Pod lomeným obloukem se dalo projít do kněžiště kostela.',
            'Sakristie byla čistá a téměř úhledná. Malý prostor byl propojen s presbytářem.',
            'Ačkoliv byla sakristie prázdná, nepůsobila stísněně. Průchodem přicházelo dovnitř jasné světlo z presbytáře.',
            'Sakristie působila příjemným, útulným dojmem. Záře vycházející ze stěn místnosti byla tlumená, ale průchodem do presbytáře dovnitř přicházelo jasné, teplé světlo.'
          ],
        }},
        { id: SceneResDict.SCENE_DESC_HUB_SACRISTY_FIRST_TIME_LEAVING, value: {
          template: '${comeThroughVerb} zbytky dveří do rozlehlejšího prostoru, který vypadal jako trosky nepříliš velkého kostela. Stavba mohla mít od zhroucených pozůstatků kněžiště k rozvalinám věže na druhém konci tak čtyřicet metrů. Hlavní loď mezi tím mohla být přibližně deset metrů široká.',
          comeThroughVerbId: $dict.Projít.ID
        }},
        { id: SceneResDict.SCENE_TITLE_HUB_TOWER_BASE, value: {
          template: 'Věž',
        }},
        { id: SceneResDict.SCENE_DESC_HUB_TOWER_BASE, value: {
          template: [
            'Čtvercová vstupní věž byla ve stejně zoufalém stavu jako zbytek kostela. Z konstrukce toho nezbývalo mnoho a olejnatá černá hmota venku hrozila, že se každým okamžikem přelije dovnitř, zaplaví celý prostor a neodvolatelně všechno pohltí.',
            'Obvodové zdi věže byly na stejné úrovni jako zdi lodi a začalo být zřejmé, že celý kostel je jeden uzavřený prostor, do kterého zvenčí nevedou žádné dveře.',
            'Věž se teď tyčila dost vysoko na to, aby bylo jasné, že sahá nad vrchol klenby lodi. Na zdi protilehlé presbytáři byly viditelné struktury odpovídající vstupnímu portálu.',
            'Ačkoliv klenba hlavní lodi se už uzavřela, věž byla shora stále otevřená. Chybějícím stropem bylo stále vidět zvolna se převalující hustou temnotu. Pod vstupním portálem nebyly dveře ale pevná zeď.',
            'Vrchol věže byl uzavřený ve výšce téměř dvojnásobné ve srovnání s lodí kostela. Pod abstraktním reliéfem zdobeným portálem byla pevná zeď.',
            'Čtvercová vež se vypínala do dvojnásobné výše než hlavní loď. Ústupkový portál končil v pevné, jednolité zdi.',
          ]
        }},


        { id: SceneResDict.COMMAND_LABEL_WAKE_UP, value: {
          template: '${verb}',
          verbId: $dict.ProbuditSe.ID,
        }},
        { id: SceneResDict.COMMAND_LABEL_LOOK_AROUND, value: {
          template: '${verb}',
          verbId: $dict.RozhlédnoutSe.ID,
        }},
        { id: SceneResDict.COMMAND_LABEL_GO_OUT_OF_THE_ROOM, value: {
          template: 'Ven z místnosti',
        }},
        { id: SceneResDict.COMMAND_LABEL_CONTINUE, value: {
          template: 'Pokračovat...',
        }},

        { id: SceneResDict.SCENE_TITLE_WHITE_LOBBY, value: {
          template: 'Vstupní hala',
        }},
        { id: SceneResDict.SCENE_DESC_WHITE_LOBBY, value: {
          template: 'Vstupní hala.',
        }},
        { id: SceneResDict.SCENE_TITLE_WHITE_ENTERING_FOR_THE_FIRST_TIME, value: {
          template: 'Nemocnice',
        }},
        { id: SceneResDict.SCENE_DESC_WHITE_ENTERING_FOR_THE_FIRST_TIME, value: {
          template: 'Oslepující bílá záře a ${actorNoun} se ${transportVerb} uprostřed rozlehlého nemocničního vestibulu.',
          transportVerbId: $dict.Ocitnout.ID,
        }},

        { id: SceneResDict.SCENE_TITLE_NOUN_SERVICE_ROOM, value: $dict.ServisníMístnost.ID},
        { id: SceneResDict.SCENE_TITLE_WHITE_LEFT_SERVICE_ROOM, value: {
          template: 'Levá servisní místnost',
        }},
        { id: SceneResDict.SCENE_DESC_WHITE_LEFT_SERVICE_ROOM, value: {
          template: 'Malá místnost byla téměř zcela zaplněna klimatizační jednotkou, která dodávala vzduch do levého křídla nemocnice. Vedle řídícího panelu s mnoha ovládacími prvky, které nastavovaly teplotu a průtok vzduchu, byla malá průhledná nádoba odpařovače, který do systému umožňoval vypouštět dezinfekční prostředky. Místnost byla prosycena kombinací vůně kafru a dezinfekce.',
        }},
        { id: SceneResDict.SCENE_TITLE_WHITE_RIGHT_SERVICE_ROOM, value: {
          template: 'Pravá servisní místnost',
        }},
        { id: SceneResDict.SCENE_DESC_WHITE_RIGHT_SERVICE_ROOM, value: {
          template: 'Stísněná místnost byla téměř zcela zaplněna klimatizační jednotkou, která dodávala vzduch do pravého křídla nemocnice. Místnost byla prosycena kombinací vůně kafru a dezinfekce. Na řídícím panelu líně poblikávaly zelené kontrolky, po jeho pravé straně byla malá průhledná nádoba odpařovače, který do systému umožňoval vypouštět dezinfekční prostředky.',
        }},

        { id: SceneResDict.SCENE_TITLE_NOUN_UPSTAIRS, value: $dict.Schodiště.ID},
        { id: SceneResDict.SCENE_TITLE_WHITE_LEFT_UPSTAIRS, value: {
          template: 'Levé schodiště',
        }},
        { id: SceneResDict.SCENE_DESC_WHITE_LEFT_UPSTAIRS, value: {
          template: 'Levé schodiště.',
        }},
        { id: SceneResDict.SCENE_TITLE_WHITE_RIGHT_UPSTAIRS, value: {
          template: 'Pravé schodiště',
        }},
        { id: SceneResDict.SCENE_DESC_WHITE_RIGHT_UPSTAIRS, value: {
          template: 'Pravé schodiště.',
        }},

        { id: SceneResDict.SCENE_TITLE_NOUN_RECEPTION, value: $dict.Recepce.ID},
        { id: SceneResDict.SCENE_TITLE_WHITE_LEFT_RECEPTION, value: {
          template: 'Levá recepce',
        }},
        { id: SceneResDict.SCENE_DESC_WHITE_LEFT_RECEPTION, value: {
          template: 'Levá recepce.',
        }},
        { id: SceneResDict.SCENE_TITLE_WHITE_RIGHT_RECEPTION, value: {
          template: 'Pravá recepce',
        }},
        { id: SceneResDict.SCENE_DESC_WHITE_RIGHT_RECEPTION, value: {
          template: 'Pravá recepce.',
        }},

        { id: SceneResDict.SCENE_TITLE_NOUN_LAB, value: $dict.Laboratoř.ID},
        { id: SceneResDict.SCENE_TITLE_WHITE_LEFT_LAB, value: {
          template: 'Levá laboratoř',
        }},
        { id: SceneResDict.SCENE_DESC_WHITE_LEFT_LAB, value: {
          template: 'Levá laboratoř.',
        }},
        { id: SceneResDict.SCENE_TITLE_WHITE_RIGHT_LAB, value: {
          template: 'Pravá laboratoř',
        }},
        { id: SceneResDict.SCENE_DESC_WHITE_RIGHT_LAB, value: {
          template: 'Pravá laboratoř.',
        }},

        { id: SceneResDict.SCENE_TITLE_NOUN_HALLWAY, value: $dict.Chodba.ID},
        { id: SceneResDict.SCENE_TITLE_WHITE_LEFT_HALLWAY, value: {
          template: 'Levá chodba',
        }},
        { id: SceneResDict.SCENE_DESC_WHITE_LEFT_HALLWAY, value: {
          template: 'Levá chodba.',
        }},
        { id: SceneResDict.SCENE_TITLE_WHITE_RIGHT_HALLWAY, value: {
          template: 'Pravá chodba',
        }},
        { id: SceneResDict.SCENE_DESC_WHITE_RIGHT_HALLWAY, value: {
          template: 'Pravá chodba.',
        }},

        { id: SceneResDict.SCENE_TITLE_NOUN_BALCONY, value: $dict.Balkón.ID},
        { id: SceneResDict.SCENE_TITLE_WHITE_LEFT_BALCONY, value: {
          template: 'Levý balkón',
        }},
        { id: SceneResDict.SCENE_DESC_WHITE_LEFT_BALCONY, value: {
          template: 'Levý balkón.',
        }},
        { id: SceneResDict.SCENE_TITLE_WHITE_RIGHT_BALCONY, value: {
          template: 'Pravý balkón',
        }},
        { id: SceneResDict.SCENE_DESC_WHITE_RIGHT_BALCONY, value: {
          template: 'Pravý balkón.',
        }},

        { id: SceneResDict.SCENE_TITLE_NOUN_PATIENT_ROOM_SOUTH, value: $dict.Pokoj.ID},
        { id: SceneResDict.SCENE_TITLE_WHITE_LEFT_PATIENT_ROOM_SOUTH, value: {
          template: 'Levá místnost pro pacinety -- jih',
        }},
        { id: SceneResDict.SCENE_DESC_WHITE_LEFT_PATIENT_ROOM_SOUTH, value: {
          template: 'Levá místnost pro pacinety -- jih.',
        }},
        { id: SceneResDict.SCENE_TITLE_WHITE_RIGHT_PATIENT_ROOM_SOUTH, value: {
          template: 'Pravá místnost pro pacinety -- jih',
        }},
        { id: SceneResDict.SCENE_DESC_WHITE_RIGHT_PATIENT_ROOM_SOUTH, value: {
          template: 'Pravá místnost pro pacinety -- jih.',
        }},

        { id: SceneResDict.SCENE_TITLE_NOUN_PATIENT_ROOM_NORTH, value: $dict.Pokoj.ID},
        { id: SceneResDict.SCENE_TITLE_WHITE_LEFT_PATIENT_ROOM_NORTH, value: {
          template: 'Levá místnost pro pacinety -- sever',
        }},
        { id: SceneResDict.SCENE_DESC_WHITE_LEFT_PATIENT_ROOM_NORTH, value: {
          template: 'Levá místnost pro pacinety -- sever.',
        }},
        { id: SceneResDict.SCENE_TITLE_WHITE_RIGHT_PATIENT_ROOM_NORTH, value: {
          template: 'Pravá místnost pro pacinety -- sever',
        }},
        { id: SceneResDict.SCENE_DESC_WHITE_RIGHT_PATIENT_ROOM_NORTH, value: {
          template: 'Pravá místnost pro pacinety -- sever.',
        }},

        { id: SceneResDict.SCENE_TITLE_NOUN_MESS_HALL, value: $dict.Jídelna.ID},
        { id: SceneResDict.SCENE_TITLE_WHITE_LEFT_MESS_HALL, value: {
          template: 'Levá jídelna',
        }},
        { id: SceneResDict.SCENE_DESC_WHITE_LEFT_MESS_HALL, value: {
          template: 'Levá jídelna.',
        }},
        { id: SceneResDict.SCENE_TITLE_WHITE_RIGHT_MESS_HALL, value: {
          template: 'Pravá jídelna',
        }},
        { id: SceneResDict.SCENE_DESC_WHITE_RIGHT_MESS_HALL, value: {
          template: 'Pravá jídelna.',
        }},


        { id: SceneResDict.SCENE_TITLE_WHITE_TERRACE_ENTRANCE, value: {
          template: 'Vstup na terasu',
        }},
        { id: SceneResDict.SCENE_DESC_WHITE_TERRACE_ENTRANCE, value: {
          template: 'Vstup na terasu.',
        }},

        { id: SceneResDict.SCENE_TITLE_REALM_RED_ENTRY, value: {
          template: 'Červená místnost.',
        }},
        { id: SceneResDict.SCENE_DESC_RED_ENTRY, value: {
          template: 'Červená místnost.',
        }},

        { id: SceneResDict.SCENE_TITLE_BLACK_ENTRY, value: {
          template: 'Černá místnost.',
        }},
        { id: SceneResDict.SCENE_DESC_BLACK_ENTRY, value: {
          template: 'Černá místnost.',
        }},

        { id: SceneResDict.SCENE_TITLE_GREEN_ENTRY, value: {
          template: 'Zelená místnost.',
        }},
        { id: SceneResDict.SCENE_DESC_GREEN_ENTRY, value: {
          template: 'Zelená místnost.',
        }},

        { id: SceneResDict.SCENE_TITLE_BLUE_ENTRY, value: {
          template: 'Modrá místnost.',
        }},
        { id: SceneResDict.SCENE_DESC_BLUE_ENTRY, value: {
          template: 'Modrá místnost.',
        }},

        { id: SceneResDict.SCENE_TITLE_YELLOW_ENTRY, value: {
          template: 'Žlutá místnost.',
        }},
        { id: SceneResDict.SCENE_DESC_YELLOW_ENTRY, value: {
          template: 'Žlutá místnost.',
        }},
      ]
    });
  }
}
