import {ResourceDictionary} from 'snowatch/resource-dictionary';
import * as lang from 'snowatch/language-structures';
import {DefaultResDict} from 'snowatch/resources/default';

import * as $dict from 'game/dictionary/dict';

export class CzechSystemResDict extends ResourceDictionary {
  constructor() {
    super({
      culture: 'cz',
      resources: [
        { id: DefaultResDict.GROUND_NOUN, value: $dict.ZemSingularCz.ID },

        { id: DefaultResDict.SPATIAL_ACTION_ON, value: $dict.SpatialActionNa.ID },
        { id: DefaultResDict.SPATIAL_ACTION_UNDER, value: $dict.SpatialActionPod.ID },
        { id: DefaultResDict.SPATIAL_ACTION_BELOW, value: $dict.SpatialActionPod.ID },
        { id: DefaultResDict.SPATIAL_ACTION_IN, value: $dict.SpatialActionDo.ID },
        { id: DefaultResDict.SPATIAL_ACTION_FRONT, value: $dict.SpatialActionPřed.ID },
        { id: DefaultResDict.SPATIAL_ACTION_BEHIND, value: $dict.SpatialActionZa.ID },
        { id: DefaultResDict.SPATIAL_ACTION_BACK, value: $dict.SpatialActionDozadu.ID },
        { id: DefaultResDict.SPATIAL_ACTION_LEFT, value: $dict.SpatialActionNalevo.ID },
        { id: DefaultResDict.SPATIAL_ACTION_RIGHT, value: $dict.SpatialActionNapravo.ID },

        { id: DefaultResDict.SPATIAL_DESCRIPTION_ON, value: $dict.SpatialDescriptionNa.ID },
        { id: DefaultResDict.SPATIAL_DESCRIPTION_UNDER, value: $dict.SpatialDescriptionPod.ID },
        { id: DefaultResDict.SPATIAL_DESCRIPTION_BELOW, value: $dict.SpatialDescriptionPod.ID },
        { id: DefaultResDict.SPATIAL_DESCRIPTION_IN, value: $dict.SpatialDescriptionV.ID },
        { id: DefaultResDict.SPATIAL_DESCRIPTION_FRONT, value: $dict.SpatialDescriptionPřed.ID },
        { id: DefaultResDict.SPATIAL_DESCRIPTION_BEHIND, value: $dict.SpatialDescriptionZa.ID },
        { id: DefaultResDict.SPATIAL_DESCRIPTION_BACK, value: $dict.SpatialDescriptionDozadu.ID },
        { id: DefaultResDict.SPATIAL_DESCRIPTION_LEFT, value: $dict.SpatialDescriptionNalevo.ID },
        { id: DefaultResDict.SPATIAL_DESCRIPTION_RIGHT, value: $dict.SpatialDescriptionNapravo.ID },

        { id: DefaultResDict.INSTRUMENT_WITH_PREPOSITION, value: $dict.InstrumentS.ID },
        { id: DefaultResDict.HANDS_NOUN, value: $dict.Ruce.ID },

        { id: DefaultResDict.NEUTRALLY, value: { id: $dict.Neutrálně.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.STRONGLY, value: { id: $dict.Silně.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.WEAKLY, value: { id: $dict.Slabě.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.QUICKLY, value: { id: $dict.Rychle.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.QUIETLY, value: { id: $dict.Tiše.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.WHISPERINGLY, value: { id: $dict.Šeptavě.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.SLOWLY, value: { id: $dict.Pomalu.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.HESITANTLY, value: { id: $dict.Váhavě.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.CAUTIOUSLY, value: { id: $dict.Opatrně.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.LOUDLY, value: { id: $dict.Hlasitě.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.ANGRILY, value: { id: $dict.Zlostně.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.PATIENTLY, value: { id: $dict.Trpělivě.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.GIVE_TO, value: { id: $dict.DátNěkomu.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.DO_ACTION_WITH, value: { id: $dict.DoActionWith.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.ACCUSATIVE_ON, value: $dict.AccusativeNa.ID },
        { id: DefaultResDict.ENGAGE_SOMEONE, value: $dict.VěnovatSeNěkomu.ID },
        { id: DefaultResDict.EVADE_SOMEONE, value: $dict.UtéctNěkomu.ID },

        { id: DefaultResDict.STUNNED, value: $dict.Omráčený.ID },

        { id: DefaultResDict.BIT_OF_IT, value: { id: $dict.Trochu.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.HALF_OF_IT, value: { id: $dict.Půlku.ID, kind: lang.Adverb.ID } },
        { id: DefaultResDict.ALL_OF_IT, value: { id: $dict.Všechen.ID, kind: lang.Pronoun.ID } },

        { id: DefaultResDict.GIVE, value: $dict.Dát.ID },
        { id: DefaultResDict.TAKE, value: $dict.Vzít.ID },
        { id: DefaultResDict.PUT, value: $dict.Dát.ID },
        { id: DefaultResDict.MOVE, value: $dict.Přejít.ID },
        { id: DefaultResDict.CLOSE, value: $dict.Zavřít.ID },
        { id: DefaultResDict.OPEN, value: $dict.Otevřít.ID },
        { id: DefaultResDict.LOCK, value: $dict.Zamknout.ID },
        { id: DefaultResDict.SAY, value: $dict.Říct.ID },
        { id: DefaultResDict.LEARN, value: $dict.NaučitSe.ID },
        { id: DefaultResDict.TEACH, value: $dict.Naučit.ID },
        { id: DefaultResDict.UNLOCK, value: $dict.Odemknout.ID },
        { id: DefaultResDict.PRESS, value: $dict.Stisknout.ID },
        { id: DefaultResDict.DRINK_UP, value: $dict.Vypít.ID },
        { id: DefaultResDict.LOOK_AROUND, value: $dict.RozhlédnoutSe.ID },
        { id: DefaultResDict.POUR, value: $dict.Nalít.ID },
        { id: DefaultResDict.ENGAGE, value: $dict.VěnovatSe.ID },
        { id: DefaultResDict.ATTACK, value: $dict.Zaútočit.ID },
        { id: DefaultResDict.BLOCK, value: $dict.Odrazit.ID },
        { id: DefaultResDict.DODGE, value: $dict.Uhýbat.ID },
        { id: DefaultResDict.EVADE, value: $dict.Utéct.ID },

        { id: DefaultResDict.SPATIAL_RELATION_DESCRIPTION, value: {
          template: '${sources} ${be} ${preposition} ${target}.',
          beId: $dict.Být.ID,
          sourcesSeparator: ', ',
          sourcesConjunction: ' a ',
        }},

        { id: DefaultResDict.SPATIAL_RELATION_ADDED_DESC, value: {
          template: '${direct} ${verb} ${preposition} ${indirect}.',
          verbId: $dict.Být.ID,
        }},

        { id: DefaultResDict.SPATIAL_RELATION_REMOVED_DESC, value: {
          template: '${direct} už ${verb} ${preposition} ${indirect}.',
          verbId: $dict.Nebýt.ID,
        }},


        { id: DefaultResDict.EVAPORATED_SOME_DESC, value: {
          template: 'Část ${direct} ${preposition} ${indirect} se ${evaporateVerb}.',
          evaporateVerbId: $dict.Vypařit.ID,
          directCase: lang.Case.genitive,
        }},

        { id: DefaultResDict.EVAPORATED_DESC, value: {
          template: '${direct} ${preposition} ${indirect} se ${evaporateVerb}.',
          evaporateVerbId: $dict.Vypařit.ID,
          directCase: lang.Case.nominative,
        }},

        { id: DefaultResDict.SMELL_DESCRIPTION, value: {
          template: 'Ve vzduchu ${beVerb} ${smellVerb} ${direct}.',
          beVerbId: $dict.Být.ID,
          smellVerbId: $dict.Cítit.ID,
          smellVerbNounId: null,
          directCase: lang.Case.nominative,
        }},
        { id: DefaultResDict.SMELLS_DESCRIPTION, value: {
          template: 'Ve vzduchu ${beVerb} ${smellVerb} ${substances}.',
          beVerbId: $dict.Být.ID,
          smellVerbId: $dict.Cítit.ID,
          smellNounId: null,
          smellNounCase: null,
          smellCase: lang.Case.nominative,
          substancesSeparator: ', ',
          substancesConjunction: ' a ',
        }},
        { id: DefaultResDict.ACTOR_SMELL_DESCRIPTION, value: {
          template: '${actorNoun} ${smellVerb} ${direct}.',
          smellVerbId: $dict.Ucítit.ID,
          actorNounCase: lang.Case.nominative,
          directCase: lang.Case.accusative,
        }},

        { id: DefaultResDict.TOOK_DAMAGE_DESCRIPTION, value: {
          template: '${actorNoun} ${beVerb} ${hitVerb}.',
          beVerbId: $dict.Být.ID,
          hitVerbId: $dict.Zasáhnout.ID,
          actorNounCase: lang.Case.nominative,
        }},
        { id: DefaultResDict.DIED_DESCRIPTION, value: {
          template: '${actorNoun} ${dieVerb}.',
          dieVerbId: $dict.Zemřít.ID,
          actorNounCase: lang.Case.nominative,
        }},


        { id: DefaultResDict.TRYING_USE_SKILL, value: {
          templateSimple: '${adverbBegin} ${actorNoun} ${tryVerb} ${adverbMiddle} ${skillVerb} ${prepositionedInstrumentNoun} ${adverbEnd} ${pronoun} ${directPreposition} ${adjective} ${directNoun}.',
          templateWithIndirects: '${adverbBegin} ${actorNoun} ${tryVerb} ${adverbMiddle} ${skillVerb} ${prepositionedInstrumentNoun} ${adverbEnd} ${pronoun} ${directPreposition} ${adjective} ${directNoun} ${prepositionedIndirects}.',
          templateWithIndirectsActorIsDirect: '${adverbBegin} ${actorNoun} ${tryVerb} ${adverbMiddle} ${adverbEnd} ${skillVerb} ${prepositionedInstrumentNoun} ${pronoun} ${prepositionedIndirects}.',
          templatePrepositionedIndirect: '${preposition} ${adjective} ${indirect}',
          templatePrepositionedInstrument: '${preposition} ${indirect}',
          indirectsSeparator: ', ',
          indirectsConjunction: ' a ',
          tryVerbId: $dict.Zkusit.ID,
          pronounCase: lang.Case.accusative,
        }},
        { id: DefaultResDict.SKILL_ACCEPTED, value: {
          templateSimple: '${adverbBegin} ${actorNoun} ${adverbMiddle} ${skillVerb} ${prepositionedInstrumentNoun} ${adverbEnd} ${pronoun${directPreposition} } ${adjective ${directNoun}.',
          templateWithIndirects: '${adverbBegin} ${actorNoun} ${adverbMiddle} ${skillVerb} ${prepositionedInstrumentNoun} ${adverbEnd} ${pronoun${directPreposition} } ${adjective ${directNoun} ${prepositionedIndirects}.',
          templateWithIndirectsActorIsDirect: '${adverbBegin} ${actorNoun} ${adverbMiddle} ${skillVerb} ${prepositionedInstrumentNoun} ${adverbEnd} ${pronoun} ${prepositionedIndirects}.',
          templatePrepositionedIndirect: '${preposition} ${adjective} ${indirect}',
          templatePrepositionedInstrument: '${preposition} ${indirect}',
          indirectsSeparator: ', ',
          indirectsConjunction: ' a ',
          pronounCase: lang.Case.accusative,
        }},
        { id: DefaultResDict.SCENE_ENTERED, value: {
          template: '${noun} ${verb} do ${sceneNoun}.',
          verbId: $dict.Vstoupit.ID,
          nounCase: lang.Case.genitive,
        }},
        { id: DefaultResDict.SCENE_LEFT, value: {
          template: '${noun} ${verb} ${sceneNoun}.',
          verbId: $dict.Opustit.ID,
          nounCase: lang.Case.accusative,
        }},
      ],
    });
  }
}
