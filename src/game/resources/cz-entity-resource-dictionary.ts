import {ResourceDictionary, HasContentsDescription} from 'snowatch/resource-dictionary';
import {Case} from 'snowatch/language-structures';

import * as $dict from 'game/dictionary/dict';

export class EntityResDict extends ResourceDictionary {
  // items
  public static readonly ALTAR_NOUN = 'altar-noun-id';
  public static readonly ALTAR_DESC = 'altar-desc-id';
  public static readonly BOWL_SHELF_NOUN = 'bowl-shelf-noun-id';
  public static readonly BOWL_SHELF_DESC = 'bowl-shelf-desc-id';
  public static readonly SWORD_NOUN = 'sword-noun-id';
  public static readonly SWORD_DESC = 'sword-desc-id';
  public static readonly CRYSTAL_NOUN = 'crystal-noun-id';
  public static readonly CRYSTAL_DESC = 'crystal-desc-id';
  public static readonly AEROSOL_DISPENSER_NOUN = 'aerosol-dispenser-noun-id';
  public static readonly AEROSOL_DISPENSER_DESC = 'aerosol-dispenser-desc-id';
  public static readonly LAST_ONE_MASCULINE_NOUN = 'last-one-masculine-noun-id';
  public static readonly LAST_ONE_FEMININE_NOUN = 'last-one-feminine-noun-id';
  public static readonly BOTTLE_NOUN = 'bottle-noun-id';
  public static readonly BOTTLE_DESC = 'bottle-desc-id';
  public static readonly DISINFECTANT_LIQUID_NOUN = 'disinfectant-liquid-noun-id';
  public static readonly DISINFECTANT_LIQUID_DESC = 'disinfectant-liquid-desc-id';
  public static readonly PATIENT_MASCULINE_NOUN = 'patient-masculine-noun-id';
  public static readonly PATIENT_FEMININE_NOUN = 'patient-feminine-noun-id';

  // verbs
  public static readonly LOOK_AROUND_VERB = 'look-around-verb-id';

  // adjectives
  public static readonly TALL_ADJECTIVE = 'tall-adjective-id';
  public static readonly SHORT_ADJECTIVE = 'short-adjective-id';
  public static readonly SKINNY_ADJECTIVE = 'skinny-adjective-id';

  // bowls, liquids, beams and pillars -- lots of kind of samey things
  // nouns
  public static readonly ESSENCE_BOWL_NOUN = 'essence-bowl-noun-id';
  public static readonly CLEAR_ESSENCE_BOWL_NOUN = 'clear-essence-bowl-noun-id';
  public static readonly WHITE_ESSENCE_BOWL_NOUN = 'white-essence-bowl-noun-id';
  public static readonly RED_ESSENCE_BOWL_NOUN = 'red-essence-bowl-noun-id';
  public static readonly BLUE_ESSENCE_BOWL_NOUN = 'blue-essence-bowl-noun-id';
  public static readonly GREEN_ESSENCE_BOWL_NOUN = 'green-essence-bowl-noun-id';
  public static readonly YELLOW_ESSENCE_BOWL_NOUN = 'yellow-essence-bowl-noun-id';
  public static readonly BLACK_ESSENCE_BOWL_NOUN = 'black-essence-bowl-noun-id';

  public static readonly BOWL_PILLAR_NOUN = 'bowl-pillar-noun-id';
  public static readonly CLEAR_BOWL_PILLAR_NOUN = 'clear-bowl-pillar-noun-id';
  public static readonly WHITE_BOWL_PILLAR_NOUN = 'white-bowl-pillar-noun-id';
  public static readonly RED_BOWL_PILLAR_NOUN = 'red-bowl-pillar-noun-id';
  public static readonly BLUE_BOWL_PILLAR_NOUN = 'blue-bowl-pillar-noun-id';
  public static readonly GREEN_BOWL_PILLAR_NOUN = 'green-bowl-pillar-noun-id';
  public static readonly YELLOW_BOWL_PILLAR_NOUN = 'yellow-bowl-pillar-noun-id';
  public static readonly BLACK_BOWL_PILLAR_NOUN = 'black-bowl-pillar-noun-id';

  public static readonly CLEAR_BEAM_OF_LIGHT_NOUN = 'clear-beam-of-light-noun-id';
  public static readonly WHITE_BEAM_OF_LIGHT_NOUN = 'white-beam-of-light-noun-id';
  public static readonly RED_BEAM_OF_LIGHT_NOUN = 'red-beam-of-light-noun-id';
  public static readonly BLUE_BEAM_OF_LIGHT_NOUN = 'blue-beam-of-light-noun-id';
  public static readonly GREEN_BEAM_OF_LIGHT_NOUN = 'green-beam-of-light-noun-id';
  public static readonly YELLOW_BEAM_OF_LIGHT_NOUN = 'yellow-beam-of-light-noun-id';
  public static readonly BLACK_BEAM_OF_LIGHT_NOUN = 'black-beam-of-light-noun-id';

  public static readonly CLEAR_ESSENCE_LIQUID_NOUN = 'clear-essence-liquid-noun-id';
  public static readonly WHITE_ESSENCE_LIQUID_NOUN = 'white-essence-liquid-noun-id';
  public static readonly RED_ESSENCE_LIQUID_NOUN = 'red-beessence-liquid-noun-id';
  public static readonly BLUE_ESSENCE_LIQUID_NOUN = 'blue-bessence-liquid-noun-id';
  public static readonly GREEN_ESSENCE_LIQUID_NOUN = 'green-essence-liquid-noun-id';
  public static readonly YELLOW_ESSENCE_LIQUID_NOUN = 'yellowessence-liquidlight-noun-id';
  public static readonly BLACK_ESSENCE_LIQUID_NOUN = 'black-essence-liquid-noun-id';

  // descriptions
  public static readonly CLEAR_ESSENCE_BOWL_DESC = 'clear-essence-bowl-desc-id';
  public static readonly WHITE_ESSENCE_BOWL_DESC = 'white-essence-bowl-desc-id';
  public static readonly RED_ESSENCE_BOWL_DESC = 'red-essence-bowl-desc-id';
  public static readonly BLUE_ESSENCE_BOWL_DESC = 'blue-essence-bowl-desc-id';
  public static readonly GREEN_ESSENCE_BOWL_DESC = 'green-essence-bowl-desc-id';
  public static readonly YELLOW_ESSENCE_BOWL_DESC = 'yellow-essence-bowl-desc-id';
  public static readonly BLACK_ESSENCE_BOWL_DESC = 'black-essence-bowl-desc-id';

  public static readonly CLEAR_BOWL_PILLAR_DESC = 'clear-bowl-pillar-desc-id';
  public static readonly WHITE_BOWL_PILLAR_DESC = 'white-bowl-pillar-desc-id';
  public static readonly RED_BOWL_PILLAR_DESC = 'red-bowl-pillar-desc-id';
  public static readonly BLUE_BOWL_PILLAR_DESC = 'blue-bowl-pillar-desc-id';
  public static readonly GREEN_BOWL_PILLAR_DESC = 'green-bowl-pillar-desc-id';
  public static readonly YELLOW_BOWL_PILLAR_DESC = 'yellow-bowl-pillar-desc-id';
  public static readonly BLACK_BOWL_PILLAR_DESC = 'black-bowl-pillar-desc-id';

  public static readonly CLEAR_BEAM_OF_LIGHT_DESC = 'clear-beam-of-light-desc-id';
  public static readonly WHITE_BEAM_OF_LIGHT_DESC = 'white-beam-of-light-desc-id';
  public static readonly RED_BEAM_OF_LIGHT_DESC = 'red-beam-of-light-desc-id';
  public static readonly BLUE_BEAM_OF_LIGHT_DESC = 'blue-beam-of-light-desc-id';
  public static readonly GREEN_BEAM_OF_LIGHT_DESC = 'green-beam-of-light-desc-id';
  public static readonly YELLOW_BEAM_OF_LIGHT_DESC = 'yellow-beam-of-light-desc-id';
  public static readonly BLACK_BEAM_OF_LIGHT_DESC = 'black-beam-of-light-desc-id';

  public static readonly WHITE_BEAM_OF_LIGHT_SOUND_DESC = 'white-beam-of-light-sound-desc-id';
  public static readonly RED_BEAM_OF_LIGHT_SOUND_DESC = 'red-beam-of-light-sound-desc-id';
  public static readonly BLUE_BEAM_OF_LIGHT_SOUND_DESC = 'blue-beam-of-light-sound-desc-id';
  public static readonly GREEN_BEAM_OF_LIGHT_SOUND_DESC = 'green-beam-of-light-sound-desc-id';
  public static readonly YELLOW_BEAM_OF_LIGHT_SOUND_DESC = 'yellow-beam-of-light-sound-desc-id';
  public static readonly BLACK_BEAM_OF_LIGHT_SOUND_DESC = 'black-beam-of-light-sound-desc-id';

  public static readonly ESSENCE_LIQUID_INACTIVE_DESC = 'essence-liquid-inactive-desc-id';
  public static readonly ESSENCE_LIQUID_ACTIVE_DESC = 'essence-liquid-active-desc-id';
  public static readonly ESSENCE_LIQUID_REFILLS_DESC = 'essence-liquid-refills-desc-id';

  constructor() {
    super({
      culture: 'cz',
      resources: [
        // nouns
        { id: EntityResDict.ALTAR_NOUN, value: $dict.Oltář.ID },
        { id: EntityResDict.ALTAR_DESC, value: {
          template: [
            'Uprostřed presbytáře stál velký kvádr z bezchybně hladkého obsidiánu orientovaný delší stranou podél osy kostela. Kolem něj stálo sedm podstavců z různých druhů kamene -- jeden v čele, po třech na každé straně.',
          ],
        }},
        { id: EntityResDict.BOWL_SHELF_NOUN, value: $dict.Police.ID },
        { id: EntityResDict.BOWL_SHELF_DESC, value: {
          template: [
            'Hrubě opracovaná dubová police byla připevněna ve výšce prsou ke zdi za hlavou oltáře.',
          ],
        }},
        { id: EntityResDict.CRYSTAL_NOUN, value: $dict.Krystal.ID },
        { id: EntityResDict.CRYSTAL_DESC, value: {
          template: 'Kus křišťálu na stříbrném řetízku visel ${actorNoun} na krku. ${coloring}',
          coloringClear: 'Krystal byl průzračně čistý.',
          coloringWhite: 'Uvnitř krystalu slabě žhnulo bílé světlo.',
          coloringRed: 'Uvnitř krystalu slabě žhnulo rudé světlo.',
          coloringGreen: 'Uvnitř krystalu slabě žhnulo sytě zelené světlo.',
          coloringYellow: 'Uvnitř krystalu slabě žhnulo jasné žluté světlo.',
          coloringBlue: 'Uvnitř krystalu slabě žhnulo akvamarínové světlo.',
          coloringBlack: 'Uvnitř krystalu byla podivná černá substance, která jako by pohlcovala světlo.',
        }},
        { id: EntityResDict.SWORD_NOUN, value: $dict.Meč.ID },
        { id: EntityResDict.SWORD_DESC, value: {
          template: 'Jeden a půlruční meč měl dokonale hladkou, necelý metr dlouhou čepel. Jílec byl ovinutý černou kůží, po obvodu hlavice bylo pravidelně umístěno sedm malých drahých kamenů.',
        }},
        { id: EntityResDict.AEROSOL_DISPENSER_NOUN, value: $dict.Odpařovač.ID },
        { id: EntityResDict.AEROSOL_DISPENSER_DESC, value: {
          template: 'Zařízení odpařovalo kapalinu z malé průhledné nádobky do vzduchových rozvodů za klimatizační jednotkou, odkud se aerosol šířil do ${sideAdjective} části nemocnice.',
          leftAdjectiveId: $dict.Levý.ID,
          rightAdjectiveId: $dict.Pravý.ID,
        }},
        { id: EntityResDict.BOTTLE_NOUN, value: $dict.Láhev.ID },
        { id: EntityResDict.BOTTLE_DESC, value: <HasContentsDescription>{
          template: 'Čirá láhev. ${contentsDescription}',
          contentsDescriptions: [
            {from: 0.00, to: 0.00, description: 'Láhev byla prázdná.'},
            {from: 0.00, to: 0.25, description: 'Láhev byla skoro prázdná.'},
            {from: 0.25, to: 0.75, description: 'Láhev byla asi z poloviny plná.'},
            {from: 0.75, to: 0.99, description: 'Láhev byla skoro plná.'},
            {from: 1.00, to: 1.00, description: 'Láhev byla plná.'},
          ],
        }},
        { id: EntityResDict.DISINFECTANT_LIQUID_NOUN, value: $dict.TekutáDezinfekce.ID },
        { id: EntityResDict.DISINFECTANT_LIQUID_DESC, value: {
          template: 'Tekutý dezinfekční prostředek.',
        }},

        // verbs
        { id: EntityResDict.LOOK_AROUND_VERB, value: $dict.RozhlédnoutSe.ID },

        // adjectives
        { id: EntityResDict.TALL_ADJECTIVE, value: $dict.Vysoký.ID },
        { id: EntityResDict.SHORT_ADJECTIVE, value: $dict.Malý.ID },
        { id: EntityResDict.SKINNY_ADJECTIVE, value: $dict.Hubený.ID },

        // various nouns
        { id: EntityResDict.ESSENCE_BOWL_NOUN, value: $dict.Nádoba.ID },
        { id: EntityResDict.CLEAR_ESSENCE_BOWL_NOUN, value: $dict.ČiráNádoba.ID },
        { id: EntityResDict.WHITE_ESSENCE_BOWL_NOUN, value: $dict.BíláNádoba.ID },
        { id: EntityResDict.RED_ESSENCE_BOWL_NOUN, value: $dict.ČervenáNádoba.ID },
        { id: EntityResDict.BLUE_ESSENCE_BOWL_NOUN, value: $dict.ModráNádoba.ID },
        { id: EntityResDict.GREEN_ESSENCE_BOWL_NOUN, value: $dict.ZelenáNádoba.ID },
        { id: EntityResDict.YELLOW_ESSENCE_BOWL_NOUN, value: $dict.ŽlutáNádoba.ID },
        { id: EntityResDict.BLACK_ESSENCE_BOWL_NOUN, value: $dict.ČernáNádoba.ID },

        { id: EntityResDict.BOWL_PILLAR_NOUN, value: $dict.Podstavec.ID },
        { id: EntityResDict.CLEAR_BOWL_PILLAR_NOUN, value: $dict.ČirýSloup.ID },
        { id: EntityResDict.WHITE_BOWL_PILLAR_NOUN, value: $dict.BílýSloup.ID },
        { id: EntityResDict.RED_BOWL_PILLAR_NOUN, value: $dict.ČervenýSloup.ID },
        { id: EntityResDict.BLUE_BOWL_PILLAR_NOUN, value: $dict.ModrýSloup.ID },
        { id: EntityResDict.GREEN_BOWL_PILLAR_NOUN, value: $dict.ZelenýSloup.ID },
        { id: EntityResDict.YELLOW_BOWL_PILLAR_NOUN, value: $dict.ŽlutýSloup.ID },
        { id: EntityResDict.BLACK_BOWL_PILLAR_NOUN, value: $dict.ČernýSloup.ID },

        { id: EntityResDict.WHITE_BEAM_OF_LIGHT_NOUN, value: $dict.PaprsekBíléhoSvětla.ID },
        { id: EntityResDict.RED_BEAM_OF_LIGHT_NOUN, value: $dict.PaprsekČervenéhoSvětla.ID },
        { id: EntityResDict.BLUE_BEAM_OF_LIGHT_NOUN, value: $dict.PaprsekModréhoSvětla.ID },
        { id: EntityResDict.GREEN_BEAM_OF_LIGHT_NOUN, value: $dict.PaprsekZelenéhoSvětla.ID },
        { id: EntityResDict.YELLOW_BEAM_OF_LIGHT_NOUN, value: $dict.PaprsekŽlutéhoSvětla.ID },
        { id: EntityResDict.BLACK_BEAM_OF_LIGHT_NOUN, value: $dict.PaprsekČernéhoSvětla.ID },

        { id: EntityResDict.WHITE_BEAM_OF_LIGHT_NOUN, value: $dict.PaprsekBíléhoSvětla.ID },
        { id: EntityResDict.RED_BEAM_OF_LIGHT_NOUN, value: $dict.PaprsekČervenéhoSvětla.ID },
        { id: EntityResDict.BLUE_BEAM_OF_LIGHT_NOUN, value: $dict.PaprsekModréhoSvětla.ID },
        { id: EntityResDict.GREEN_BEAM_OF_LIGHT_NOUN, value: $dict.PaprsekZelenéhoSvětla.ID },
        { id: EntityResDict.YELLOW_BEAM_OF_LIGHT_NOUN, value: $dict.PaprsekŽlutéhoSvětla.ID },
        { id: EntityResDict.BLACK_BEAM_OF_LIGHT_NOUN, value: $dict.PaprsekČernéhoSvětla.ID },

        { id: EntityResDict.CLEAR_ESSENCE_LIQUID_NOUN, value: $dict.ČiráEsence.ID },
        { id: EntityResDict.WHITE_ESSENCE_LIQUID_NOUN, value: $dict.BíláEsence.ID },
        { id: EntityResDict.RED_ESSENCE_LIQUID_NOUN, value: $dict.ČervenáEsence.ID },
        { id: EntityResDict.BLUE_ESSENCE_LIQUID_NOUN, value: $dict.ModráEsence.ID },
        { id: EntityResDict.GREEN_ESSENCE_LIQUID_NOUN, value: $dict.ZelenáEsence.ID },
        { id: EntityResDict.YELLOW_ESSENCE_LIQUID_NOUN, value: $dict.ŽlutáEsence.ID },
        { id: EntityResDict.BLACK_ESSENCE_LIQUID_NOUN, value: $dict.ČernáEsence.ID },

        { id: EntityResDict.LAST_ONE_MASCULINE_NOUN, value: $dict.PosledníMuž.ID },
        { id: EntityResDict.LAST_ONE_FEMININE_NOUN, value: $dict.PosledníŽena.ID },

        { id: EntityResDict.PATIENT_MASCULINE_NOUN, value: $dict.Pacient.ID },
        { id: EntityResDict.PATIENT_FEMININE_NOUN, value: $dict.Pacientka.ID },

        // descriptions
        { id: EntityResDict.CLEAR_ESSENCE_BOWL_DESC, value : {
          template: 'Platinová nádoba byla jakoby vrostlá do vrcholu křišťálového podstavce.',
        }},
        { id: EntityResDict.WHITE_ESSENCE_BOWL_DESC, value : {
          template: 'Zlatá nádoba. Kolem jejího obvodu byly vyryty tři linky se vzrůstající šířkou.',
        }},
        { id: EntityResDict.RED_ESSENCE_BOWL_DESC, value : {
          template: 'Železná nádoba. Kolem jejího okraje byla vyryta široká linka.',
        }},
        { id: EntityResDict.BLUE_ESSENCE_BOWL_DESC, value : {
          template: 'Stříbrná nádoba. Kolem jejího okraje byla vyryta tenká linka.',
        }},
        { id: EntityResDict.GREEN_ESSENCE_BOWL_DESC, value : {
          template: 'Cínová nádoba. Kolem jejího okraje byla vyryta linka.',
        }},
        { id: EntityResDict.YELLOW_ESSENCE_BOWL_DESC, value : {
          template: 'Olověná nádoba. Kolem jejího okraje byla vyryta tenká a o něco širší linka.',
        }},
        { id: EntityResDict.BLACK_ESSENCE_BOWL_DESC, value : {
          template: 'Měděná nádoba bez ozdob.',
        }},

        { id: EntityResDict.CLEAR_BOWL_PILLAR_DESC, value : {
          template: [
            'Rozpadající se podstavec z čirého křišťálu byl asi metr vysoký. Na jeho vrcholu ležela nádoba z čisté platiny.',
            'Těžce poškozený podstavec z křišťálu. Na jeho vrcholu ležela nádoba z čisté platiny.',
            'Z křišťálového podstavce byly vytlučené velké kusy materiálu. Na jeho vrcholu ležela nádoba z čisté platiny.',
            'Křišťálový podstavec byl popraskaný a znatelně opotřebovaný. Na jeho vrcholu ležela nádoba z čisté platiny.',
            'Na hladkém povrchu křišťálového podstavce vystupovaly tu a tam jemné praskliny. Na jeho vrcholu ležela nádoba z čisté platiny.',
            'Křišťálový podstavec byl bezchybně hladký. Na jeho vrcholu ležela nádoba z čisté platiny.',
          ],
        }},
        { id: EntityResDict.WHITE_BOWL_PILLAR_DESC, value : {
          template: [
            'Rozpadající se podstavec z bílého kamene. Na horní straně podstavce byly sotva znatelé stopy po třech soustředných kružnicích s rostoucí šířkou. Sloup byl přibližně jeden metr vysoký.',
            'Těžce poškozený podstavec z bílého kamene.',
            'Z bílého podstavce byly vydrolené kusy materiálu.',
            'Bílý podstavec byl popraskaný a znatelně opotřebovaný.',
            'Na hladkém povrchu bílého podstavce vystupovaly tu a tam jemné praskliny.',
            'Bílý púodstavec byl bezchybně hladký.',
          ],
        }},
        { id: EntityResDict.RED_BOWL_PILLAR_DESC, value : {
          template: [
            'Rozpadající se podstavec z červeného kamene. Sloup byl vysoký asi metr.',
            'Těžce poškozený podstavec z červeneného kamene.',
            'Z červeného podstavce byly vydrolené kusy materiálu.',
            'Červený podstavec byl popraskaný a znatelně opotřebovaný.',
            'Na hladkém povrchu červeného podstavce vystupovaly tu a tam jemné praskliny.',
            'Červený podstavec byl bezchybně hladký.',
          ],
        }},
        { id: EntityResDict.BLUE_BOWL_PILLAR_DESC, value : {
          template: [
            'Rozpadající se podstavec z modrého kamene. Na horní straně podstavce byly sotva znatelé stopy po tesání jediné tenké kružnice. Sloup byl vysoký asi metr.',
            'Těžce poškozený podstavec z modrého kamene.',
            'Z modrého podstavce byly vydrolené kusy materiálu.',
            'Modrý podstavec byl popraskaný a znatelně opotřebovaný.',
            'Na hladkém povrchu modrého podstavce vystupovaly tu a tam jemné praskliny.',
            'Modrý podstavec byl bezchybně hladký.',
          ],
        }},
        { id: EntityResDict.GREEN_BOWL_PILLAR_DESC, value : {
          template: [
            'Rozpadající se podstavec ze zeleného kamene byl asi metr vysoký. Na horní straně podstavce byly sotva znatelé stopy po tesání neširoké kružnice. ',
            'Těžce poškozený podstavec ze zeleného kamene.',
            'Ze zeleného podstavce byly vydrolené kusy materiálu.',
            'Zelený podstavec byl popraskaný a znatelně opotřebovaný.',
            'Na hladkém povrchu zeleného podstavce vystupovaly tu a tam jemné praskliny.',
            'Zelený podstavec byl bezchybně hladký.',
          ],
        }},
        { id: EntityResDict.YELLOW_BOWL_PILLAR_DESC, value : {
          template: [
            'Přibližně metr vysoký rozpadající se podstavec ze žlutého kamene. Na horní straně podstavce byly sotva znatelé stopy po tesání dvou kružnic: tenké a o něco širší.',
            'Těžce poškozený podstavec ze žlutého kamene.',
            'Ze žlutého podstavce byly vydrolené kusy materiálu.',
            'Žlutý podstavec byl popraskaný a znatelně opotřebovaný.',
            'Na hladkém povrchu žlutého podstavce vystupovaly tu a tam jemné praskliny.',
            'Žlutý podstavec byl bezchybně hladký.',
          ],
        }},
        { id: EntityResDict.BLACK_BOWL_PILLAR_DESC, value : {
          template: [
            'Rozpadající se podstavec z černého kamene měl výšku odhadem metr. Na horní straně podstavce byly sotva znatelé stopy po tesání široké kružnice.',
            'Těžce poškozený podstavec z černého kamene.',
            'Z černého podstavce byly vydrolené kusy materiálu.',
            'Černý podstavec byl popraskaný a znatelně opotřebovaný.',
            'Na hladkém povrchu černého podstavce vystupovaly tu a tam jemné praskliny.',
            'Černý podstavec byl bezchybně hladký.',
          ],
        }},

        { id: EntityResDict.WHITE_BEAM_OF_LIGHT_DESC, value : {
          templateInactive: 'Paprsek slabě zářil bílým světlem.',
          templateActive: 'Paprsek jasně zářil bílým světlem.',
        }},
        { id: EntityResDict.RED_BEAM_OF_LIGHT_DESC, value : {
          templateInactive: 'Paprsek slabě zářil červeným světlem.',
          templateActive: 'Paprsek jasně zářil červeným světlem.',
        }},
        { id: EntityResDict.BLUE_BEAM_OF_LIGHT_DESC, value : {
          templateInactive: 'Paprsek slabě zářil modrým světlem.',
          templateActive: 'Paprsek jasně zářil modrým světlem.',
        }},
        { id: EntityResDict.GREEN_BEAM_OF_LIGHT_DESC, value : {
          templateInactive: 'Paprsek slabě zářil zeleným světlem.',
          templateActive: 'Paprsek jasně zářil zeleným světlem.',
        }},
        { id: EntityResDict.YELLOW_BEAM_OF_LIGHT_DESC, value : {
          templateInactive: 'Paprsek slabě zářil žlutým světlem.',
          templateActive: 'Paprsek jasně zářil žlutým světlem.',
        }},
        { id: EntityResDict.BLACK_BEAM_OF_LIGHT_DESC, value : {
          templateInactive: 'Paprsek jako by pohlcoval část světla.',
          templateActive: 'Paprsek úplně pohlcoval světlo ve svém okolí.',
        }},

        { id: EntityResDict.WHITE_BEAM_OF_LIGHT_SOUND_DESC, value : {
          template: 'Ozval se disonantní zvuk složený ze tří tónů: vysokého, nízkého a kdesi uprostřed.',
        }},
        { id: EntityResDict.RED_BEAM_OF_LIGHT_SOUND_DESC, value : {
          template: 'Ozval se hluboký, dunivý tón.',
        }},
        { id: EntityResDict.BLUE_BEAM_OF_LIGHT_SOUND_DESC, value : {
          template: 'Ozval se vysoký jasný tón.',
        }},
        { id: EntityResDict.GREEN_BEAM_OF_LIGHT_SOUND_DESC, value : {
          template: 'Ozval se jediný sřední tón.',
        }},
        { id: EntityResDict.YELLOW_BEAM_OF_LIGHT_SOUND_DESC, value : {
          template: 'Ozval se zvuk složený z hlubokého a středního tónu.',
        }},
        { id: EntityResDict.BLACK_BEAM_OF_LIGHT_SOUND_DESC, value : {
          template: 'Všechen zvuk jako by byl na okamžik pohlcen a nebylo slyšet nic. ',
        }},

        { id: EntityResDict.ESSENCE_LIQUID_INACTIVE_DESC, value : {
          template: '${noun} slabě zářila.',
          requiredNounCase: Case.nominative,
        }},
        { id: EntityResDict.ESSENCE_LIQUID_ACTIVE_DESC, value : {
          template: '${noun} jasně zářila.',
          requiredNounCase: Case.nominative,
        }},
        { id: EntityResDict.ESSENCE_LIQUID_REFILLS_DESC, value : {
          template: '${noun} se doplnila.',
          requiredNounCase: Case.nominative,
        }},
      ],
    });
  }
}
