import {FrameworkConfiguration} from 'aurelia-framework';

import * as $actors from 'game/actors/all';

export function configure(aurelia: FrameworkConfiguration) {
  aurelia.singleton($actors.Actor, $actors.LastOne);
  aurelia.singleton($actors.Actor, $actors.White);
  aurelia.singleton($actors.Actor, $actors.PatientFemaleShort);
  aurelia.singleton($actors.Actor, $actors.PatientFemaleTall);
  aurelia.singleton($actors.Actor, $actors.PatientFemaleSkinny);
  aurelia.singleton($actors.Actor, $actors.PatientMaleShort);
  aurelia.singleton($actors.Actor, $actors.PatientMaleTall);
  aurelia.singleton($actors.Actor, $actors.PatientMaleSkinny);
}
