import * as snowatch from 'snowatch/snowatch';
import * as behaviors from 'snowatch/behaviors/all';
import * as attrs from 'snowatch/attributes/all';
import * as skills from 'snowatch/skills/all';

import * as $behaviors from 'game/behaviors/all';
import * as $scenes from 'game/scenes/all';
import * as $items from 'game/items/all';
import * as $gameres from 'game/resources/game-res';

export class White extends snowatch.Actor {
  public static readonly ID = "White";

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData) {
    super.initialize(
      game,
      {
        startSceneId: 'WhiteRoom',
        name: 'White',
        properties: {
          attributes: [
            { id: attrs.CanListen },
            { id: attrs.CanBeIndirect },
            { id: attrs.HasSpatialRelations },
            { id: attrs.SpatialLocation, initialState: { value: {x: 2, y: 2, z: 0}}},
            { id: attrs.SpatialSize},
          ],
          skills: [
            { id: skills.Give.ID, initialState: {} },
            { id: skills.Put.ID },
            { id: skills.Say.ID },
          ],
          knowledge: [
          ],
          behaviors: [
            { id: behaviors.SupportsReplying.ID },
          ],
        }
      }
    );

    return this;
  }
}
