import * as snowatch from 'snowatch/snowatch';
import * as entities from 'snowatch/entities/all';
import * as behaviors from 'snowatch/behaviors/all';
import * as skills from 'snowatch/skills/all';
import * as attrs from 'snowatch/attributes/all';

import * as $scenes from 'game/scenes/all';
import * as $gameres from 'game/resources/game-res';

const CommonAttributes: Array<attrs.AttributeInit> = [
  { id: attrs.CanListen },
  { id: attrs.CanEngage },
  { id: attrs.CanBeEngaged },
  { id: attrs.CanBlock },
  { id: attrs.CanBeBlocked },
  { id: attrs.CanDodge },
  { id: attrs.CanBeDodged },
  { id: attrs.CanEvade, initialState: { value: true }  },
  { id: attrs.CanAttack},
  { id: attrs.CanBeAttacked},
  { id: attrs.CanTakeDamage},
  { id: attrs.IsEngageable, initialState: { value: true } },
  { id: attrs.IsAttackable },
  { id: attrs.HasDodgeAvailable },
  { id: attrs.IsBlockable },
  { id: attrs.IsEvadeable },
  { id: attrs.IsStunned },
  { id: attrs.CanBeIndirect },
  { id: attrs.CanBeStunned },
  { id: attrs.CanPursue },
  { id: attrs.CanDie },
  { id: attrs.CanUseSkills, initialState: {value: false}},
  { id: attrs.Weight, initialState: {value: <attrs.NumericValueAttribute>{ value: 80, initialValue: 80, minValue: 80, maxValue: 80 }}},
  { id: attrs.Agility, initialState: {value: <attrs.NumericValueAttribute>{ value: 0.07, initialValue: 0.07, minValue: 0, maxValue: 1 }}},
  { id: attrs.Dexterity, initialState: {value: <attrs.NumericValueAttribute>{ value: 0.4, initialValue: 0.4, minValue: 0, maxValue: 1 }}},
  { id: attrs.Constitution, initialState: {value: <attrs.NumericValueAttribute>{ value: 0.25, initialValue: 0.25, minValue: 0, maxValue: 1 }}},
  { id: attrs.Perception, initialState: {value: <attrs.NumericValueAttribute>{ value: 0.5, initialValue: 0.5, minValue: 0, maxValue: 1 }}},
  { id: attrs.Stamina, initialState: { value: { value: 10, initialValue: 10, minValue: 0, maxValue: 10 } as attrs.NumericValueAttribute}},
  { id: attrs.Strength, initialState: {value: <attrs.NumericValueAttribute>{ value: 0.45, initialValue: 0.45, minValue: 0, maxValue: 1 }}},
];

const CommonMaleConstAttributes: Array<attrs.ConstAttributeInit> = [
  { id: attrs.NounResourceId, constState: { value: $gameres.EntityResDict.PATIENT_MASCULINE_NOUN } },
  { id: attrs.GenderId, constState: { value: attrs.GenderIdValues.Male }},
];

const CommonFemaleConstAttributes: Array<attrs.ConstAttributeInit> = [
  { id: attrs.NounResourceId, constState: { value: $gameres.EntityResDict.PATIENT_FEMININE_NOUN } },
  { id: attrs.GenderId, constState: { value: attrs.GenderIdValues.Female }},
];

const CommonSkills: Array<skills.SkillInit> = [
  { id: skills.Move.ID },
  { id: skills.Engage.ID },
  { id: skills.Attack.ID },
  { id: skills.Block.ID },
  { id: skills.Evade.ID },
];

const PatientsLeftWanderingPath: Array<behaviors.WanderAroundLocation> = [
  {sceneId: $scenes.WhiteLeftHallway.ID, position: snowatch.addVect3($scenes.WhiteLeftHallway.SpatialLocation, {x: 0, y: 0, z: 0})},
  {sceneId: $scenes.WhiteLeftPatientRoomSouth.ID},
  {sceneId: $scenes.WhiteLeftHallway.ID},
  {sceneId: $scenes.WhiteLeftPatientRoomNorth.ID},
];

const PatientsRightWanderingPath: Array<behaviors.WanderAroundLocation> = [
  {sceneId: $scenes.WhiteRightHallway.ID, position: snowatch.addVect3($scenes.WhiteRightHallway.SpatialLocation, {x: 0, y: 0, z: 0})},
  {sceneId: $scenes.WhiteRightPatientRoomSouth.ID},
  {sceneId: $scenes.WhiteRightHallway.ID},
  {sceneId: $scenes.WhiteRightPatientRoomNorth.ID},
];

const CommonBehaviors: Array<behaviors.BehaviorInit> = [
  { id: behaviors.SupportsHearing.ID },
  { id: behaviors.SupportsSeeing.ID },
  { id: behaviors.SupportsSmelling.ID },
  { id: behaviors.SupportsAgroing.ID },
  { id: behaviors.SupportsAttacking.ID },
  { id: behaviors.SupportsEngaging.ID },
  { id: behaviors.SupportsEvading.ID },
  { id: behaviors.SupportsBlocking.ID },
  { id: behaviors.SupportsDodging.ID },
  { id: behaviors.SupportsFighting.ID, initialState: {
    attackSequence: [
      { delayMillis: 1500, attackModifierId: skills.AttackModifiers.quickly.id},
      { delayMillis: 2000, attackModifierId: skills.AttackModifiers.strongly.id},
      { delayMillis: 1000, attackModifierId: skills.AttackModifiers.quickly.id},
    ]
  } as behaviors.SupportsFightingState},
  { id: behaviors.SupportsTakingDamage.ID },
  { id: behaviors.SupportsDying.ID },
  { id: behaviors.GradualStaminaRecovery.ID },
  { id: behaviors.DisableBehaviorsOnMessage.ID,
    initialState: {
      topicId: snowatch.Topic.id(snowatch.Topics.DIED),
      disableAll: true,
    } as behaviors.DisableBehaviorsOnMessageState
  },
  { id: behaviors.ChangeAttributeOnMessage.ID,
    initialState: {
      topicId: snowatch.Topic.id(snowatch.Topics.DIED),
      changes: [
        { attributeId: attrs.CanListen, setTo: false },
        { attributeId: attrs.CanEngage, setTo: false },
        { attributeId: attrs.CanBeEngaged, setTo: false },
        { attributeId: attrs.CanBlock, setTo: false },
        { attributeId: attrs.CanBeBlocked, setTo: false },
        { attributeId: attrs.CanDodge, setTo: false },
        { attributeId: attrs.CanBeDodged, setTo: false },
        { attributeId: attrs.CanEvade, setTo: false },
        { attributeId: attrs.CanAttack, setTo: false },
        { attributeId: attrs.CanBeAttacked, setTo: false },
        { attributeId: attrs.CanTakeDamage, setTo: false },
        { attributeId: attrs.CanBeIndirect, setTo: false },
        { attributeId: attrs.CanDie, setTo: false },
        { attributeId: attrs.CanUseSkills, setTo: false },
      ],
    } as behaviors.ChangeAttributeOnMessageState
  },
];

export class PatientMaleTall extends snowatch.Actor {
  public static readonly ID = 'PatientMaleTall';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData) {
    super.initialize(
      game,
      {
        startSceneId: $scenes.WhiteLeftPatientRoomSouth.ID,
        name: "Patient Male Tall",
        properties: {
          constAttributes: [
            ...snowatch.arrayDeepClone(CommonMaleConstAttributes),
            { id: attrs.AdjectiveResourceId, constState: { value: $gameres.EntityResDict.TALL_ADJECTIVE } },
          ],
          attributes: [
            ...snowatch.arrayDeepClone(CommonAttributes),
            { id: attrs.SpatialLocation, initialState: { value: snowatch.addVect3($scenes.WhiteLeftPatientRoomSouth.SpatialLocation, {x: 0, y: 0, z: 0 })}},
            { id: attrs.SpatialSize, initialState: { value: {x: 0.3, y: 0.3, z: 1.95 }}},
            { id: attrs.Health, initialState: { value: { value: 80, initialValue: 80, minValue: 0, maxValue: 80 } as attrs.NumericValueAttribute}},
          ],
          skills: [
            ...snowatch.arrayDeepClone(CommonSkills),
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBehaviors),
            { id: behaviors.WanderAround.ID, initialState: <behaviors.WanderAroundState> {
              locations: PatientsLeftWanderingPath,
              wanderingRateTicks: 100,
            }},
            { id: behaviors.SupportsPursuing.ID, initialState: {
              delayMillis: 2000,
              allowedSceneIds: PatientsLeftWanderingPath.map(p => p.sceneId)
            } as behaviors.SupportsPursuingState },
          ],
        }
      }
    );

    this.registerInitializer(() => entities.createHands(this, $scenes.WhiteLeftPatientRoomSouth.ID, PatientMaleTall.ID));

    return this;
  }
}

export class PatientMaleShort extends snowatch.Actor {
  public static readonly ID = 'PatientMaleShort';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData) {
    super.initialize(
      game,
      {
        startSceneId: $scenes.WhiteLeftPatientRoomNorth.ID,
        name: "Patient Male Short",
        properties: {
          constAttributes: [
            ...snowatch.arrayDeepClone(CommonMaleConstAttributes),
            { id: attrs.AdjectiveResourceId, constState: { value: $gameres.EntityResDict.SHORT_ADJECTIVE } },
          ],
          attributes: [
            ...snowatch.arrayDeepClone(CommonAttributes),
            { id: attrs.SpatialLocation, initialState: { value: snowatch.addVect3($scenes.WhiteLeftPatientRoomNorth.SpatialLocation, {x: 0, y: 0, z: 0 })}},
            { id: attrs.SpatialSize, initialState: { value: {x: 0.3, y: 0.3, z: 1.7 }}},
            { id: attrs.Health, initialState: { value: { value: 100, initialValue: 100, minValue: 0, maxValue: 100 } as attrs.NumericValueAttribute}},
          ],
          skills: [
            ...snowatch.arrayDeepClone(CommonSkills),
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBehaviors),
            { id: behaviors.WanderAround.ID, initialState: <behaviors.WanderAroundState> {
              locations: PatientsLeftWanderingPath,
              wanderingRateTicks: 150,
            }},
            { id: behaviors.SupportsPursuing.ID, initialState: {
              delayMillis: 1500,
              allowedSceneIds: PatientsLeftWanderingPath.map(p => p.sceneId)
            } as behaviors.SupportsPursuingState },
          ],
        }
      }
    );

    this.registerInitializer(() => entities.createHands(this, $scenes.WhiteLeftPatientRoomNorth.ID, PatientMaleShort.ID));

    return this;
  }
}

export class PatientMaleSkinny extends snowatch.Actor {
  public static readonly ID = 'PatientMaleSkinny';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData) {
    super.initialize(
      game,
      {
        startSceneId: $scenes.WhiteLeftHallway.ID,
        name: "Patient Male Skinny",
        properties: {
          constAttributes: [
            ...snowatch.arrayDeepClone(CommonMaleConstAttributes),
            { id: attrs.AdjectiveResourceId, constState: { value: $gameres.EntityResDict.SKINNY_ADJECTIVE } },
          ],
          attributes: [
            ...snowatch.arrayDeepClone(CommonAttributes),
            { id: attrs.SpatialLocation, initialState: { value: snowatch.addVect3($scenes.WhiteLeftHallway.SpatialLocation, {x: 0, y: 0, z: 0 })}},
            { id: attrs.SpatialSize, initialState: { value: {x: 0.3, y: 0.2, z: 1.85 }}},
            { id: attrs.Health, initialState: { value: { value: 60, initialValue: 60, minValue: 0, maxValue: 60 } as attrs.NumericValueAttribute}},
          ],
          skills: [
            ...snowatch.arrayDeepClone(CommonSkills),
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBehaviors),
            { id: behaviors.WanderAround.ID, initialState: <behaviors.WanderAroundState> {
              locations: PatientsLeftWanderingPath,
              wanderingRateTicks: 200,
            }},
            { id: behaviors.SupportsPursuing.ID, initialState: {
              delayMillis: 3000,
              allowedSceneIds: PatientsLeftWanderingPath.map(p => p.sceneId)
            } as behaviors.SupportsPursuingState },
          ],
        }
      }
    );

    this.registerInitializer(() => entities.createHands(this, $scenes.WhiteLeftHallway.ID, PatientMaleSkinny.ID));

    return this;
  }
}

export class PatientFemaleTall extends snowatch.Actor {
  public static readonly ID = 'PatientFemaleTall';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData) {
    super.initialize(
      game,
      {
        startSceneId: $scenes.WhiteRightPatientRoomNorth.ID,
        name: "Patient Female Tall",
        properties: {
          constAttributes: [
            ...snowatch.arrayDeepClone(CommonFemaleConstAttributes),
            { id: attrs.AdjectiveResourceId, constState: { value: $gameres.EntityResDict.TALL_ADJECTIVE } },
          ],
          attributes: [
            ...snowatch.arrayDeepClone(CommonAttributes),
            { id: attrs.SpatialLocation, initialState: { value: snowatch.addVect3($scenes.WhiteRightPatientRoomNorth.SpatialLocation, {x: 0, y: 0, z: 0 })}},
            { id: attrs.SpatialSize, initialState: { value: {x: 0.3, y: 0.3, z: 1.70 }}},
            { id: attrs.Health, initialState: { value: { value: 60, initialValue: 60, minValue: 0, maxValue: 60 } as attrs.NumericValueAttribute}},
          ],
          skills: [
            ...snowatch.arrayDeepClone(CommonSkills),
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBehaviors),
            { id: behaviors.WanderAround.ID, initialState: <behaviors.WanderAroundState> {
              locations: PatientsRightWanderingPath,
              wanderingRateTicks: 100,
            }},
            { id: behaviors.SupportsPursuing.ID, initialState: {
              delayMillis: 2000,
              allowedSceneIds: PatientsRightWanderingPath.map(p => p.sceneId)
            } as behaviors.SupportsPursuingState },
          ],
        }
      }
    );

    this.registerInitializer(() => entities.createHands(this, $scenes.WhiteRightPatientRoomNorth.ID, PatientFemaleTall.ID));

    return this;
  }
}

export class PatientFemaleShort extends snowatch.Actor {
  public static readonly ID = 'PatientFemaleShort';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData) {
    super.initialize(
      game,
      {
        startSceneId: $scenes.WhiteRightHallway.ID,
        name: "Patient Female Short",
        properties: {
          constAttributes: [
            ...snowatch.arrayDeepClone(CommonFemaleConstAttributes),
            { id: attrs.AdjectiveResourceId, constState: { value: $gameres.EntityResDict.SHORT_ADJECTIVE } },
          ],
          attributes: [
            ...snowatch.arrayDeepClone(CommonAttributes),
            { id: attrs.SpatialLocation, initialState: { value: snowatch.addVect3($scenes.WhiteRightHallway.SpatialLocation, {x: 0, y: 0, z: 0 })}},
            { id: attrs.SpatialSize, initialState: { value: {x: 0.3, y: 0.3, z: 1.70 }}},
            { id: attrs.Health, initialState: { value: { value: 80, initialValue: 80, minValue: 0, maxValue: 80 } as attrs.NumericValueAttribute}},
          ],
          skills: [
            ...snowatch.arrayDeepClone(CommonSkills),
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBehaviors),
            { id: behaviors.WanderAround.ID, initialState: <behaviors.WanderAroundState> {
              locations: PatientsRightWanderingPath,
              wanderingRateTicks: 150,
            }},
            { id: behaviors.SupportsPursuing.ID, initialState: {
              delayMillis: 1500,
              allowedSceneIds: PatientsRightWanderingPath.map(p => p.sceneId)
            } as behaviors.SupportsPursuingState },
          ],
        }
      }
    );

    this.registerInitializer(() => entities.createHands(this, $scenes.WhiteRightHallway.ID, PatientFemaleShort.ID));

    return this;
  }
}
export class PatientFemaleSkinny extends snowatch.Actor {
  public static readonly ID = 'PatientFemaleSkinny';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData) {
    super.initialize(
      game,
      {
        startSceneId: $scenes.WhiteRightPatientRoomSouth.ID,
        name: "Patient Female Skinny",
        properties: {
          constAttributes: [
            ...snowatch.arrayDeepClone(CommonFemaleConstAttributes),
            { id: attrs.AdjectiveResourceId, constState: { value: $gameres.EntityResDict.SKINNY_ADJECTIVE } },
          ],
          attributes: [
            ...snowatch.arrayDeepClone(CommonAttributes),
            { id: attrs.SpatialLocation, initialState: { value: snowatch.addVect3($scenes.WhiteRightPatientRoomSouth.SpatialLocation, {x: 0, y: 0, z: 0 })}},
            { id: attrs.SpatialSize, initialState: { value: {x: 0.3, y: 0.2, z: 1.70 }}},
            { id: attrs.Health, initialState: { value: { value: 40, initialValue: 40, minValue: 0, maxValue: 40 } as attrs.NumericValueAttribute}},
          ],
          skills: [
            ...snowatch.arrayDeepClone(CommonSkills),
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBehaviors),
            { id: behaviors.WanderAround.ID, initialState: <behaviors.WanderAroundState> {
              locations: PatientsRightWanderingPath,
              wanderingRateTicks: 200,
            }},
            { id: behaviors.SupportsPursuing.ID, initialState: {
              delayMillis: 3000,
              allowedSceneIds: PatientsRightWanderingPath.map(p => p.sceneId)
            } as behaviors.SupportsPursuingState },
          ],
        }
      }
    );

    this.registerInitializer(() => entities.createHands(this, $scenes.WhiteRightPatientRoomSouth.ID, PatientFemaleSkinny.ID));

    return this;
  }
}