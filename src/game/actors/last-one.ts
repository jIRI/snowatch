// import { createHandsInstance } from 'snowatch/containers';
import { DefaultResDict } from 'snowatch/resources/default';

import * as snowatch from 'snowatch/snowatch';
import * as behaviors from 'snowatch/behaviors/all';
import * as attrs from 'snowatch/attributes/all';
import * as skills from 'snowatch/skills/all';
import * as entities from 'snowatch/entities/all';

import * as $current from 'game/current-game/all';
import * as $attrs from 'game/current-game/attributes';
import * as $entities from 'game/entities/all';
import * as $scenes from 'game/scenes/all';
import * as $items from 'game/items/all';
import * as $behaviors from 'game/behaviors/all';
import * as $gameres from 'game/resources/game-res';

export class LastOne extends snowatch.Actor {
  public static readonly ID = 'LastOne';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData) {
    super.initialize(
      game,
      {
        startSceneId: $scenes.IntroWakeUp.ID,
        name: "Last One",
        properties: {
          inventory: [
            { id: $items.Crystal.ID },
          ],
          constAttributes: [
            { id: attrs.IsStartActor },
          ],
          attributes: [
            { id: attrs.NounResourceId, initialState: { value: $gameres.EntityResDict.LAST_ONE_MASCULINE_NOUN } },
            { id: attrs.CanLookAround },
            { id: attrs.CanListen },
            { id: attrs.SpatialLocation, initialState: { value: snowatch.cloneIfObject($scenes.HubSacristy.SpatialLocation)} },
            { id: attrs.SpatialSize, initialState: { value: {x: 0.3, y: 0.3, z: 1.85 }} },
            { id: attrs.GenderId, initialState: { value: attrs.GenderIdValues.Male } },
            { id: attrs.CanUseSkills, initialState: {value: false} },
            { id: attrs.CanSeeInventory, initialState: {value: false} },
            { id: attrs.CanEngage, initialState: {value: true} },
            { id: attrs.CanBlock },
            { id: attrs.CanBeBlocked },
            { id: attrs.IsBlockable },
            { id: attrs.CanDodge },
            { id: attrs.CanBeDodged },
            { id: attrs.HasDodgeAvailable },
            { id: attrs.CanEvade, initialState: {value: true} },
            { id: attrs.CanBeEvaded },
            { id: attrs.IsEvadeable },
            { id: attrs.IsAttackable },
            { id: attrs.CanAttack },
            { id: attrs.CanDie },
            { id: attrs.IsDead },
            { id: attrs.CanBeStunned },
            { id: attrs.IsPursuable },
            { id: attrs.CanBePursued },
            { id: attrs.IsStunned },
            { id: attrs.Weight, initialState: { value: { value: 80, initialValue: 80, minValue: 80, maxValue: 80 } as attrs.NumericValueAttribute} },
            { id: attrs.Agility, initialState: { value: { value: 0.7, initialValue: 0.7, minValue: 0, maxValue: 1 } as attrs.NumericValueAttribute} },
            { id: attrs.Dexterity, initialState: { value: { value: 0.6, initialValue: 0.6, minValue: 0, maxValue: 1 } as attrs.NumericValueAttribute} },
            { id: attrs.Constitution, initialState: { value: { value: 0.5, initialValue: 0.5, minValue: 0, maxValue: 1 } as attrs.NumericValueAttribute} },
            { id: attrs.Perception, initialState: { value: { value: 0.5, initialValue: 0.5, minValue: 0, maxValue: 1 } as attrs.NumericValueAttribute }},
            { id: attrs.Stamina, initialState: { value: { value: 20, initialValue: 20, minValue: 0, maxValue: 20 } as attrs.NumericValueAttribute} },
            { id: attrs.Strength, initialState: { value: { value: 0.65, initialValue: 0.65, minValue: 0, maxValue: 1 } as attrs.NumericValueAttribute} },
            { id: attrs.Health, initialState: { value: { value: 80, initialValue: 80, minValue: 0, maxValue: 80 } as attrs.NumericValueAttribute} },
            { id: $attrs.CurrentlyActiveEssenceLiquid, initialState: { value: null } },
          ],
          skills: [
            { id: skills.Give.ID },
            { id: skills.Take.ID },
            { id: skills.Put.ID },
            { id: skills.Learn.ID },
            { id: skills.Teach.ID },
            { id: skills.Say.ID },
            { id: skills.Open.ID },
            { id: skills.Close.ID },
            { id: skills.Lock.ID },
            { id: skills.Unlock.ID },
            { id: skills.Move.ID },
            { id: skills.Press.ID },
            { id: skills.DrinkUp.ID },
            { id: skills.LookAround.ID },
            { id: skills.Pour.ID },
            { id: skills.Engage.ID },
            { id: skills.Attack.ID },
            { id: skills.Block.ID },
            { id: skills.Dodge.ID },
            { id: skills.Evade.ID },
          ],
          behaviors: [
            { id: behaviors.SupportsReplying.ID },
            { id: behaviors.SupportsListening.ID },
            { id: behaviors.SupportsHearing.ID },
            { id: behaviors.SupportsSeeing.ID },
            { id: behaviors.SupportsSmelling.ID },
            { id: behaviors.SupportsAttacking.ID },
            { id: behaviors.SupportsEvading.ID },
            { id: behaviors.SupportsEngaging.ID },
            { id: behaviors.SupportsBlocking.ID },
            { id: behaviors.SupportsDodging.ID },
            { id: behaviors.SupportsTakingDamage.ID },
            { id: behaviors.SupportsDying.ID },
            { id: behaviors.GradualHealthRecovery.ID },
            { id: behaviors.GradualStaminaRecovery.ID },
            { id: behaviors.TimeSourceDilationWhenAttacked.ID },
            { id: behaviors.TimeSourceDilationRemovalAfterDelay.ID },
            { id: behaviors.TimeSourceDilationRemovalOnInput.ID },
            { id: $behaviors.CrystalPressed.ID, initialState: <$behaviors.CrystalPressedState>{ actorId: LastOne.ID } },
            { id: $behaviors.UpdateNumericAttributeOnDrinkingLiquid.ID },
            { id: $behaviors.ReportAttributeChange.ID,
              initialState: <$behaviors.ReportAttributeChangeState>{
                attributesIds: [ attrs.Strength, attrs.Agility, attrs.Perception, attrs.Dexterity, attrs.Stamina, attrs.Constitution],
              },
              subProcess: (target: snowatch.Entity, state: behaviors.BehaviorState | null, message: snowatch.Message): string => {
                return $behaviors.ReportAttributeChange
                  .with(target, message)
                  .forAttribute(attrs.Strength)
                    .ifGoesUp<attrs.NumericValueAttribute>((e) => this.composeAttrChangeText(e, $gameres.ActionResDict.FEELING_WHEN_STRENGTH_GOES_UP_DESC, this))
                    .ifGoesDown<attrs.NumericValueAttribute>((e) => this.composeAttrChangeText(e, $gameres.ActionResDict.FEELING_WHEN_STRENGTH_GOES_DOWN_DESC, this))
                  .forAttribute(attrs.Agility)
                    .ifGoesUp<attrs.NumericValueAttribute>((e) => this.composeAttrChangeText(e, $gameres.ActionResDict.FEELING_WHEN_AGILITY_GOES_UP_DESC, this))
                    .ifGoesDown<attrs.NumericValueAttribute>((e) => this.composeAttrChangeText(e, $gameres.ActionResDict.FEELING_WHEN_AGILITY_GOES_DOWN_DESC, this))
                  .forAttribute(attrs.Perception)
                    .ifGoesUp<attrs.NumericValueAttribute>((e) => this.composeAttrChangeText(e, $gameres.ActionResDict.FEELING_WHEN_INTELLIGENCE_GOES_UP_DESC, this))
                    .ifGoesDown<attrs.NumericValueAttribute>((e) => this.composeAttrChangeText(e, $gameres.ActionResDict.FEELING_WHEN_INTELLIGENCE_GOES_DOWN_DESC, this))
                  .forAttribute(attrs.Dexterity)
                    .ifGoesUp<attrs.NumericValueAttribute>((e) => this.composeAttrChangeText(e, $gameres.ActionResDict.FEELING_WHEN_DEXTERITY_GOES_UP_DESC, this))
                    .ifGoesDown<attrs.NumericValueAttribute>((e) => this.composeAttrChangeText(e, $gameres.ActionResDict.FEELING_WHEN_DEXTERITY_GOES_DOWN_DESC, this))
                  .forAttribute(attrs.Constitution)
                    .ifGoesUp<attrs.NumericValueAttribute>((e) => this.composeAttrChangeText(e, $gameres.ActionResDict.FEELING_WHEN_HEALTH_GOES_UP_DESC, this))
                    .ifGoesDown<attrs.NumericValueAttribute>((e) => this.composeAttrChangeText(e, $gameres.ActionResDict.FEELING_WHEN_HEALTH_GOES_DOWN_DESC, this))
                  .getText();
              },
            },
            { id: 'last-one-went-sleeping',
              filter: (target: snowatch.Entity, initialState: any, message: snowatch.Message) => {
                return (message.topicId === snowatch.Topic.id($current.CustomTopics.GO_SLEEP)) ? snowatch.make(snowatch.accepted) : snowatch.make(snowatch.rejected);
              },
              process: (target: snowatch.Entity, initialState: any, message: snowatch.Message) => {
                this.tryChangeSceneTo($scenes.IntroGoSleep.ID);
              }
            },
            { id: 'last-one-died',
              filter: (target: snowatch.Entity, initialState: any, message: snowatch.Message) => {
                switch( message.topicId ) {
                  case snowatch.Topic.id(snowatch.Topics.DIED):
                    break;
                  default:
                    return snowatch.make(snowatch.rejected);
                }

                const messagePayload = snowatch.payload<snowatch.Topics.DIED>(message);
                return (messagePayload.entityId === target.id) ? snowatch.make(snowatch.accepted) : snowatch.make(snowatch.rejected);
              },
              process: (target: snowatch.Entity, initialState: any, message: snowatch.Message) => {
                snowatch.Attribute.setValue(target, attrs.IsDead, false);
                this.tryChangeSceneTo($scenes.IntroWakeUp.ID);
              }
            },
          ],
          // dialogueLines: dialogs.getLastOneDialogueLines(),
        }
      }
    );

    this.registerInitializer(() => entities.createHands(this, $scenes.IntroWakeUp.ID, LastOne.ID));

    return this;
  }

  private composeAttrChangeText(entity: snowatch.Entity, resourceId: string, actor: snowatch.Actor): string {
    const resource = snowatch.ResourceDictionary.get(entity.game, resourceId);
    const actorNoun = snowatch.Noun.get(entity.game, $gameres.ResourceDictionary.get(entity.game, attrs.Attribute.value<string>(entity, attrs.NounResourceId, actor)));
    return snowatch.compose(resource.template, {
      actorNoun: actorNoun.noun(snowatch.Case.nominative),
      feelVerb: snowatch.Verb.get(entity.game, resource.feelVerbId).verb(actorNoun.number, snowatch.Tense.past, snowatch.Person.third, actorNoun.gender, actorNoun.genderSubtype),
    });
  }
}
