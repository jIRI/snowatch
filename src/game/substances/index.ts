import {FrameworkConfiguration} from 'aurelia-framework';

import * as $substances from 'game/substances/all';

export function configure(aurelia: FrameworkConfiguration) {
  aurelia.singleton($substances.Substance, $substances.ClearEssenceLiquid);
  aurelia.singleton($substances.Substance, $substances.WhiteEssenceLiquid);
  aurelia.singleton($substances.Substance, $substances.RedEssenceLiquid);
  aurelia.singleton($substances.Substance, $substances.BlueEssenceLiquid);
  aurelia.singleton($substances.Substance, $substances.GreenEssenceLiquid);
  aurelia.singleton($substances.Substance, $substances.YellowEssenceLiquid);
  aurelia.singleton($substances.Substance, $substances.BlackEssenceLiquid);
  aurelia.singleton($substances.Substance, $substances.DisinfectantLiquid);
}
