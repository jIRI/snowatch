import * as snowatch from 'snowatch/snowatch';
import * as behaviors from 'snowatch/behaviors/all';
import * as attrs from 'snowatch/attributes/all';
import * as $items from 'game/items/all';
import * as $scenes from 'game/scenes/all';
import * as $gameres from 'game/resources/game-res';

export class DisinfectantLiquid extends snowatch.Substance {
  public static readonly ID = 'DisinfectantLiquid';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Substance {
    super.initialize(
      game,
      {
        title: 'Disinfectant Liquid',
        startSceneId: $scenes.WhiteLeftServiceRoom.ID,
        substancePhase: snowatch.SubstancePhases.liquid,
        properties: {
          constAttributes: [
            { id: attrs.NounResourceId, constState: { value: $gameres.EntityResDict.DISINFECTANT_LIQUID_NOUN} },
            { id: attrs.HasSpatialRelations },
          ],
          compAttributes: [
            { id: attrs.Description, compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const resource = snowatch.ResourceDictionary.get(this.game, $gameres.EntityResDict.DISINFECTANT_LIQUID_DESC);
                return snowatch.compose(resource.template);
              }
            }},
          ],
          attributes: [
            { id: attrs.ContainerId, initialState: { value: $items.Bottle.ID } },
            { id: attrs.Volume, initialState: { value: 1} },
            { id: attrs.CanBeIndirect },
            { id: attrs.IsDrinkable },
            { id: attrs.IsPourable },
            { id: attrs.IsSmellable },
            { id: attrs.SubstanceCompounds, initialState: { value: new Set([ DisinfectantLiquid.ID ]) }},
          ],
          behaviors: [
            {id: behaviors.EvaporateWhenPoured.ID, initialState: <behaviors.EvaporateWhenPouredState>{
              evaporationCreatesGas: true,
              evaporatingSpatialRelationTypeIds: [snowatch.SpatialPrepositions.isOn.id],
            }},
            { id: behaviors.SmellsWhenEvaporated.ID },
            { id: behaviors.ChangeAttributeWhenSpatialRelationAdded.ID,
              initialState: <behaviors.ChangeAttributeWhenSpatialRelationAddedState>{
                types: [snowatch.SpatialPrepositions.isIn.id],
                changes: [
                  { attributeId: attrs.IsDrinkable, typeNotMatch: false },
                  { attributeId: attrs.IsPourable, typeNotMatch: false },
                ],
            }},
            { id: behaviors.ChangeAttributeWhenSubstanceStateChanges.ID,
              initialState: <behaviors.ChangeAttributeWhenSubstanceStateChangesState>{
                stateIds: [snowatch.SubstancePhases.gas],
                changes: [ { attributeId: attrs.IsVisible, stateMatch: false } ],
            }},
            {id: behaviors.RemoveWhenVolumeZero.ID },
          ],
          spatialRelations: [
            { typeId: snowatch.SpatialPrepositions.isIn.id, targetId: $items.Bottle.ID },
          ],
        },
      }
    );

    return this;
  }
}

