import * as snowatch from 'snowatch/snowatch';
import * as behaviors from 'snowatch/behaviors/all';
import * as attrs from 'snowatch/attributes/all';

import * as $current from 'game/current-game/all';
import * as $attrs from 'game/current-game/attributes';
import * as $items from 'game/items/all';
import * as $scenes from 'game/scenes/all';
import * as $gameres from 'game/resources/game-res';

const CommonLiquidConstAttributes = [
  { id: attrs.HasSpatialRelations },
  { id: attrs.CanBeIndirect },
  { id: attrs.IsDrinkable },
  { id: attrs.SpatialSize, constState: { value: {x: 0.3, y: 0.3, z: 0.3}} },
];

const CommonLiquidComputedAttributes = [
  { id: attrs.Description,
    compState: {
      value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity): any => getLiquidDescription(
        entity,
        $gameres.EntityResDict.ESSENCE_LIQUID_ACTIVE_DESC,
        $gameres.EntityResDict.ESSENCE_LIQUID_INACTIVE_DESC
      ),
  }},
];

const HandleBeamActicationBehaviorId = 'handle-beam-actication-behavior';
const LogLiquidRestorationBehaviorId = 'log-liquid-restoration-behavior';
const CommonBehaviors: Array<snowatch.BehaviorInit> = [
  { id: HandleBeamActicationBehaviorId,
    activate: (entity: snowatch.Entity, ...args: any[]) => {
      if( attrs.Attribute.value<boolean>(entity, attrs.IsVisible) ) {
        // if beam is already active, the behavior is supposed to be removed.
        // however system adds behaviors from init object automatically, so we need to handle that.
        behaviors.Behavior.remove(entity, HandleBeamActicationBehaviorId);
      }
    },
    filter: (target: snowatch.Entity, initialState: any, message: snowatch.Message): snowatch.ResultObject => {
      return (message.topicId === snowatch.Topic.id($current.CustomTopics.ACTIVATE_BEAMS)) ? snowatch.make(snowatch.accepted) : snowatch.make(snowatch.rejected);
    },
    process: (target: snowatch.Entity, initialState: any, message: snowatch.Message) => {
      // set attribute
      attrs.Attribute.setValue<boolean>(target, attrs.IsVisible, true);
      behaviors.Behavior.remove(target, HandleBeamActicationBehaviorId);
    }
  },
  { id: behaviors.RestoreVolume.ID, initialState: { volume: 1 } as behaviors.RestoreVolumeState },
  { id: LogLiquidRestorationBehaviorId,
    filter: (target: snowatch.Entity, initialState: any, message: snowatch.Message): snowatch.ResultObject => {
      if( message.topicId !== snowatch.Topic.id(snowatch.Topics.SUBSTANCE_VOLUME_RESTORED) ) {
        return snowatch.make(snowatch.rejected);
      }

      const messagePayload = snowatch.payload<snowatch.Topics.SUBSTANCE_VOLUME_RESTORED>(message);
      return (target.id === messagePayload.substanceId) ? snowatch.make(snowatch.accepted) : snowatch.make(snowatch.rejected);
    },
    process: (target: snowatch.Entity, initialState: any, message: snowatch.Message) => {
      const resource = snowatch.ResourceDictionary.get(target.game, $gameres.EntityResDict.ESSENCE_LIQUID_REFILLS_DESC);
      const nounId = snowatch.ResourceDictionary.get(target.game, snowatch.Attribute.value<string>(target, attrs.NounResourceId));
      const noun = snowatch.Noun.get(target.game, nounId).noun(resource.requiredNounCase);
        target.game.addToGameLog({
        source: target.id,
        action: behaviors.RestoreVolume.ID,
        description: snowatch.capitalFirst(snowatch.compose(resource.template, { noun })),
      });
    }
  },
];

export class ClearEssenceLiquid extends snowatch.Substance {
  public static readonly ID = 'ClearEssenceLiquid';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Substance {
    super.initialize(
      game,
      {
        title: 'Clear Essence Liquid',
        startSceneId: $scenes.HubChancel.ID,
        isVisible: false,
        properties: {
          constAttributes: [
            ...CommonLiquidConstAttributes,
            { id: $attrs.RemoveCurrentlyActiveEssenceLiquidEffect },
            { id: attrs.ColorId, constState: { value: attrs.ColorIdValues.None} },
          ],
          compAttributes: [
            ...CommonLiquidComputedAttributes,
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.CLEAR_ESSENCE_LIQUID_NOUN;
                },
            }},
          ],
          attributes: [
            { id: $attrs.IsSubstanceActive, initialState: { value: true} },
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBehaviors),
          ],
          spatialRelations: [
            { typeId: snowatch.SpatialPrepositions.isIn.id, targetId: $items.ClearEssenceBowl.ID },
          ],
        },
      }
    );

    return this;
  }
}

export class WhiteEssenceLiquid extends snowatch.Substance {
  public static readonly ID = 'WhiteEssenceLiquid';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Substance {
    super.initialize(
      game,
      {
        title: 'White Essence Liquid',
        startSceneId: $scenes.HubChancel.ID,
        isVisible: false,
        properties: {
          constAttributes: [
            ...CommonLiquidConstAttributes,
            { id: $attrs.AttributeToUpdateOnLiquidDrinking, constState: { value: { id: attrs.Constitution, amount: 0.1, potency: attrs.Potency.Singlepotent } } },
            { id: attrs.ColorId, constState: { value: attrs.ColorIdValues.White} },
          ],
          compAttributes: [
            ...CommonLiquidComputedAttributes,
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.WHITE_ESSENCE_LIQUID_NOUN;
                },
            }},
          ],
          attributes: [
            { id: $attrs.IsSubstanceActive, initialState: { value: false} },
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBehaviors),
          ],
          spatialRelations: [
            { typeId: snowatch.SpatialPrepositions.isIn.id, targetId: $items.WhiteEssenceBowl.ID },
          ],
        },
      }
    );

    return this;
  }
}

export class RedEssenceLiquid extends snowatch.Substance {
  public static readonly ID = 'RedEssenceLiquid';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Substance {
    super.initialize(
      game,
      {
        title: 'Red Essence Liquid',
        startSceneId: $scenes.HubChancel.ID,
        isVisible: false,
        properties: {
          constAttributes: [
            ...CommonLiquidConstAttributes,
            { id: $attrs.AttributeToUpdateOnLiquidDrinking, constState: { value: { id: attrs.Strength, amount: 0.1, potency: attrs.Potency.Singlepotent } } },
            { id: attrs.ColorId, constState: { value: attrs.ColorIdValues.Red} },
          ],
          compAttributes: [
            ...CommonLiquidComputedAttributes,
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.RED_ESSENCE_LIQUID_NOUN;
                },
            }},
          ],
          attributes: [
            { id: $attrs.IsSubstanceActive, initialState: { value: false} },
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBehaviors),
          ],
          spatialRelations: [
            { typeId: snowatch.SpatialPrepositions.isIn.id, targetId: $items.RedEssenceBowl.ID },
          ],
        },
      }
    );

    return this;
  }
}

export class BlueEssenceLiquid extends snowatch.Substance {
  public static readonly ID = 'BlueEssenceLiquid';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Substance {
    super.initialize(
      game,
      {
        title: 'Blue Essence Liquid',
        startSceneId: $scenes.HubChancel.ID,
        isVisible: false,
        properties: {
          constAttributes: [
            ...CommonLiquidConstAttributes,
            { id: $attrs.AttributeToUpdateOnLiquidDrinking, constState: { value: { id: attrs.Dexterity, amount: 0.1, potency: attrs.Potency.Singlepotent } } },
            { id: attrs.ColorId, constState: { value: attrs.ColorIdValues.Blue} },
          ],
          compAttributes: [
            ...CommonLiquidComputedAttributes,
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.BLUE_ESSENCE_LIQUID_NOUN;
                },
            }},
          ],
          attributes: [
            { id: $attrs.IsSubstanceActive, initialState: { value: false} },
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBehaviors),
          ],
          spatialRelations: [
            { typeId: snowatch.SpatialPrepositions.isIn.id, targetId: $items.BlueEssenceBowl.ID },
          ],
        },
      }
    );

    return this;
  }
}

export class GreenEssenceLiquid extends snowatch.Substance {
  public static readonly ID = 'GreenEssenceLiquid';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Substance {
    super.initialize(
      game,
      {
        title: 'Green Essence Liquid',
        startSceneId: $scenes.HubChancel.ID,
        isVisible: false,
        properties: {
          constAttributes: [
            ...CommonLiquidConstAttributes,
            { id: $attrs.AttributeToUpdateOnLiquidDrinking, constState: { value: { id: attrs.Stamina, amount: 0.1, potency: attrs.Potency.Singlepotent } } },
            { id: attrs.ColorId, constState: { value: attrs.ColorIdValues.Green} },
          ],
          compAttributes: [
            ...CommonLiquidComputedAttributes,
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.GREEN_ESSENCE_LIQUID_NOUN;
                },
            }},
          ],
          attributes: [
            { id: $attrs.IsSubstanceActive, initialState: { value: false} },
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBehaviors),
          ],
          spatialRelations: [
            { typeId: snowatch.SpatialPrepositions.isIn.id, targetId: $items.GreenEssenceBowl.ID },
          ],
        },
      }
    );

    return this;
  }
}

export class YellowEssenceLiquid extends snowatch.Substance {
  public static readonly ID = 'YellowEssenceLiquid';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Substance {
    super.initialize(
      game,
      {
        title: 'Yellow Essence Liquid',
        startSceneId: $scenes.HubChancel.ID,
        isVisible: false,
        properties: {
          constAttributes: [
            ...CommonLiquidConstAttributes,
            { id: $attrs.AttributeToUpdateOnLiquidDrinking, constState: { value: { id: attrs.Agility, amount: 0.1, potency: attrs.Potency.Singlepotent } } },
            { id: attrs.ColorId, constState: { value: attrs.ColorIdValues.Yellow}},
          ],
          compAttributes: [
            ...CommonLiquidComputedAttributes,
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  return $gameres.EntityResDict.YELLOW_ESSENCE_LIQUID_NOUN;
                },
            }},
          ],
          attributes: [
            { id: $attrs.IsSubstanceActive, initialState: { value: false} },
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBehaviors),
          ],
          spatialRelations: [
            { typeId: snowatch.SpatialPrepositions.isIn.id, targetId: $items.YellowEssenceBowl.ID },
          ],
        },
      }
    );

    return this;
  }
}

export class BlackEssenceLiquid extends snowatch.Substance {
  public static readonly ID = 'BlackEssenceLiquid';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData): snowatch.Substance {
    super.initialize(
      game,
      {
        title: 'Black Essence Liquid',
        startSceneId: $scenes.HubChancel.ID,
        isVisible: false,
        properties: {
          constAttributes: [
            ...CommonLiquidConstAttributes,
            { id: $attrs.AttributeToUpdateOnLiquidDrinking, constState: { value: { id: attrs.Perception, amount: 0.1, potency: attrs.Potency.Singlepotent } } },
            { id: attrs.ColorId, constState: { value: attrs.ColorIdValues.Black} },
          ],
          compAttributes: [
            ...CommonLiquidComputedAttributes,
            { id: attrs.NounResourceId,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity): any => {
                  return $gameres.EntityResDict.BLACK_ESSENCE_LIQUID_NOUN;
                },
            }},
          ],
          attributes: [
            { id: $attrs.IsSubstanceActive, initialState: { value: false} },
          ],
          behaviors: [
            ...snowatch.arrayDeepClone(CommonBehaviors),
          ],
          spatialRelations: [
            { typeId: snowatch.SpatialPrepositions.isIn.id, targetId: $items.BlackEssenceBowl.ID },
          ],
        },
      }
    );

    return this;
  }
}

function getLiquidDescription(liquid: snowatch.StatelessEntity, activeResourceId: string, inactiveResourceId: string): string {
  const resourceId = snowatch.Attribute.value<boolean>(liquid, $attrs.IsSubstanceActive) === true ? activeResourceId : inactiveResourceId;
  const resource = snowatch.ResourceDictionary.get(liquid.game, resourceId);
  const nounId = snowatch.ResourceDictionary.get(liquid.game, snowatch.Attribute.value<string>(liquid, attrs.NounResourceId));
  const noun = snowatch.Noun.get(liquid.game, nounId).noun(resource.requiredNounCase);
  return snowatch.capitalFirst(snowatch.compose(resource.template, { noun }));
}
