import {FrameworkConfiguration} from 'aurelia-framework';

import * as $scenes from 'game/scenes/all';

export function configure(aurelia: FrameworkConfiguration) {
  aurelia.singleton($scenes.Scene, $scenes.IntroGoSleep);
  aurelia.singleton($scenes.Scene, $scenes.IntroFall);
  aurelia.singleton($scenes.Scene, $scenes.IntroWakeUp);
  aurelia.singleton($scenes.Scene, $scenes.HubChancel);
  aurelia.singleton($scenes.Scene, $scenes.HubNave);
  aurelia.singleton($scenes.Scene, $scenes.HubSacristy);
  aurelia.singleton($scenes.Scene, $scenes.HubSacristyFirstTimeLeaving);
  aurelia.singleton($scenes.Scene, $scenes.HubTowerBase);
  aurelia.singleton($scenes.Scene, $scenes.WhiteEnteringForTheFirstTime);
  aurelia.singleton($scenes.Scene, $scenes.WhiteLobby);
  aurelia.singleton($scenes.Scene, $scenes.WhiteLeftServiceRoom);
  aurelia.singleton($scenes.Scene, $scenes.WhiteLeftUpstairs);
  aurelia.singleton($scenes.Scene, $scenes.WhiteLeftReception);
  aurelia.singleton($scenes.Scene, $scenes.WhiteLeftLab);
  aurelia.singleton($scenes.Scene, $scenes.WhiteLeftHallway);
  aurelia.singleton($scenes.Scene, $scenes.WhiteLeftPatientRoomSouth);
  aurelia.singleton($scenes.Scene, $scenes.WhiteLeftPatientRoomNorth);
  aurelia.singleton($scenes.Scene, $scenes.WhiteLeftMessHall);
  aurelia.singleton($scenes.Scene, $scenes.WhiteLeftBalcony);
  aurelia.singleton($scenes.Scene, $scenes.WhiteRightServiceRoom);
  aurelia.singleton($scenes.Scene, $scenes.WhiteRightUpstairs);
  aurelia.singleton($scenes.Scene, $scenes.WhiteRightReception);
  aurelia.singleton($scenes.Scene, $scenes.WhiteRightLab);
  aurelia.singleton($scenes.Scene, $scenes.WhiteRightHallway);
  aurelia.singleton($scenes.Scene, $scenes.WhiteRightPatientRoomSouth);
  aurelia.singleton($scenes.Scene, $scenes.WhiteRightPatientRoomNorth);
  aurelia.singleton($scenes.Scene, $scenes.WhiteRightMessHall);
  aurelia.singleton($scenes.Scene, $scenes.WhiteRightBalcony);
  aurelia.singleton($scenes.Scene, $scenes.WhiteTerraceEntrance);
  aurelia.singleton($scenes.Scene, $scenes.BlackEntry);
  aurelia.singleton($scenes.Scene, $scenes.RedEntry);
  aurelia.singleton($scenes.Scene, $scenes.GreenEntry);
  aurelia.singleton($scenes.Scene, $scenes.YellowEntry);
  aurelia.singleton($scenes.Scene, $scenes.BlueEntry);
}
