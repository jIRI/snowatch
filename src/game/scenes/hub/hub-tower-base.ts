import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

import * as $current from 'game/current-game/all';
import * as $behaviors from 'game/behaviors/all';
import * as $scenes from 'game/scenes/all';
import * as $items from 'game/items/all';
import * as $actors from 'game/actors/all';
import * as $gameres from 'game/resources/game-res';

export class HubTowerBase extends snowatch.Scene {
  public static readonly ID = 'HubTowerBase';
  public static readonly SpatialLocation: snowatch.Vector3 = {x: 0, y: 30, z: 0};
  public static readonly SpatialSize: snowatch.Vector3 = {x: 13, y: 10, z: 40};
  public static readonly SceneDefaultSpot: snowatch.Vector3 = {x: 0, y: 0, z: 1};

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData, init?: snowatch.SceneInit) {
    super.initialize(game, {
      properties: {
        compAttributes: [
          { id: attrs.Title,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_HUB_TOWER_BASE);
                return snowatch.compose(sceneTitleResource.template, {});
              }
            }
          },
          { id: attrs.Description,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneDescResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_DESC_HUB_TOWER_BASE);
                return snowatch.compose(sceneDescResource.template[$current.global(this.game).hubDescriptionLevel], {});
              }
            }
          },
        ],
        constAttributes: [
          { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.HubRealmSpatialLocation, HubTowerBase.SpatialLocation)}},
          { id: attrs.SpatialSize, constState: { value: snowatch.addVect3($scenes.HubRealmSpatialSize, HubTowerBase.SpatialSize)}},
          { id: attrs.SceneDefaultSpot, constState: { value: HubTowerBase.SceneDefaultSpot}},
        ],
      },
      exits: [
          // exit 1
          { id: 'to-hub-nave',
            title: (exit, actor: snowatch.Entity) => {
              const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_HUB_NAVE);
              return snowatch.compose(sceneTitleResource.template, {});
            },
            scene: $scenes.HubNave.ID,
          },
        ],
    });
    return this;
  }
}
