import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

import * as $current from 'game/current-game/all';
import * as $behaviors from 'game/behaviors/all';
import * as $scenes from 'game/scenes/all';
import * as $items from 'game/items/all';
import * as $actors from 'game/actors/all';
import * as $gameres from 'game/resources/game-res';

export class HubSacristy extends snowatch.Scene {
  public static readonly ID = 'HubSacristy';
  public static readonly SpatialLocation: snowatch.Vector3 = {x: -6, y: -1, z: 1.625};
  public static readonly SpatialSize: snowatch.Vector3 = {x: 4, y: 3, z: 2.5};
  public static readonly SceneDefaultSpot: snowatch.Vector3 = {x: -6, y: -1, z: 1};

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData, init?: snowatch.SceneInit) {
    super.initialize(game, {
      disablesInteractions: true,
      properties: {
        compAttributes: [
          { id: attrs.Title,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_HUB_SACRISTY);
                return snowatch.compose(sceneTitleResource.template, {});
              }
            }
          },
          { id: attrs.Description,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneDescResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_DESC_HUB_SACRISTY);
                return snowatch.compose(sceneDescResource.template[$current.global(this.game).hubDescriptionLevel], {});
              }
            }
          },
        ],
        constAttributes: [
          { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.HubRealmSpatialLocation, HubSacristy.SpatialLocation)}},
          { id: attrs.SpatialSize, constState: { value: snowatch.addVect3($scenes.HubRealmSpatialSize, HubSacristy.SpatialSize)}},
          { id: attrs.SceneDefaultSpot, constState: { value: HubSacristy.SceneDefaultSpot}},
        ],
      },
      exits: [
        { id: 'to-hub-chancel',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game,
              (actor.id === $actors.LastOne.ID && this.visitCounts.get(actor.id) === 1) ? $gameres.SceneResDict.COMMAND_LABEL_GO_OUT_OF_THE_ROOM : $gameres.SceneResDict.SCENE_TITLE_HUB_CHANCEL
            );
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: (exit, entity) => {
            return (entity.id === $actors.LastOne.ID && this.visitCounts.get(entity.id) === 1) ? $scenes.HubSacristyFirstTimeLeaving.ID : $scenes.HubChancel.ID;
          },
        },
      ],
    });
    return this;
  }
}
