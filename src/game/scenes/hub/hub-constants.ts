import { Vector3, addVect3 } from "snowatch/spatial-relations";

export const HubRealmAnchorLocation: Vector3 = {x: 0, y: 16, z: 0};
export const HubRealmSpatialLocation: Vector3 = addVect3(HubRealmAnchorLocation, {x: 0, y: 0, z: 0});
export const HubRealmSpatialSize: Vector3 = {x: 13, y: 40, z: 40};