import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

import * as $current from 'game/current-game/all';
import * as $behaviors from 'game/behaviors/all';
import * as $scenes from 'game/scenes/all';
import * as $items from 'game/items/all';
import * as $actors from 'game/actors/all';
import * as $gameres from 'game/resources/game-res';

export class HubNave extends snowatch.Scene {
  public static readonly ID = 'HubNave';
  public static readonly SpatialLocation: snowatch.Vector3 = {x: 0, y: -16, z: 0};
  public static readonly SpatialSize: snowatch.Vector3 = {x: 9, y: 25, z: 20};
  public static readonly SceneDefaultSpot: snowatch.Vector3 = {x: 0, y: -16, z: 0};

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData, init?: snowatch.SceneInit) {
    super.initialize(game, {
        properties: {
          compAttributes: [
            { id: attrs.Title,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_HUB_NAVE);
                  return snowatch.compose(sceneTitleResource.template, {});
                }
              }
            },
            { id: attrs.Description,
              compState: {
                value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                  const sceneDescResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_DESC_HUB_NAVE);
                  return snowatch.compose(sceneDescResource.template[$current.global(this.game).hubDescriptionLevel], {});
                }
              }
            },
          ],
          constAttributes: [
            { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.HubRealmSpatialLocation, HubNave.SpatialLocation)}},
            { id: attrs.SpatialSize, constState: { value: snowatch.addVect3($scenes.HubRealmSpatialSize, HubNave.SpatialSize)}},
            { id: attrs.SceneDefaultSpot, constState: { value: HubNave.SceneDefaultSpot}},
          ],
        },
      exits: [
          // exit 1
          { id: 'to-hub-chancel',
            title: (exit, actor: snowatch.Entity) => {
              const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_HUB_CHANCEL);
              return snowatch.compose(sceneTitleResource.template, {});
            },
            scene: $scenes.HubChancel.ID,
          },
          // exit 2
          { id: 'to-hub-tower-base',
            title: (exit, actor: snowatch.Entity) => {
              const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_HUB_TOWER_BASE);
              return snowatch.compose(sceneTitleResource.template, {});
            },
            scene: $scenes.HubTowerBase.ID,
          },
        ],
    });
    return this;
  }
}
