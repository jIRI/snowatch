import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

import * as $current from 'game/current-game/all';
import * as $behaviors from 'game/behaviors/all';
import * as $scenes from 'game/scenes/all';
import * as $items from 'game/items/all';
import * as $actors from 'game/actors/all';
import * as $gameres from 'game/resources/game-res';

export class HubSacristyFirstTimeLeaving extends snowatch.Scene {
  public static readonly ID = 'HubSacristyFirstTimeLeaving';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData, init?: snowatch.SceneInit) {
    super.initialize(game, {
      hasGround: false,
      disablesInteractions: true,
      properties: {
        compAttributes: [
          { id: attrs.Description,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
              if( actor == null ) {
                return null;
              }

              const sceneDescResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_DESC_HUB_SACRISTY_FIRST_TIME_LEAVING);
                const actorNoun = snowatch.Noun.get(this.game, $gameres.ResourceDictionary.get(this.game, attrs.Attribute.value<string>(actor, attrs.NounResourceId, actor)));
                return snowatch.compose(sceneDescResource.template, {
                  comeThroughVerb: snowatch.capitalFirst(snowatch.Verb.get(this.game, sceneDescResource.comeThroughVerbId).verb(actorNoun.number, snowatch.Tense.past, snowatch.Person.third, actorNoun.gender, actorNoun.genderSubtype)),
                });
              }
            }
          },
        ],
      },
      exits: [
        // exit
        { id: 'to-hub-chancel',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.COMMAND_LABEL_CONTINUE);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.HubChancel.ID,
        },
      ],
    });
    return this;
  }
}
