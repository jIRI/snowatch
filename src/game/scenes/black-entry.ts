import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

// import * as current from 'current-game/all';
// import * as behaviors from 'behaviors/all';
// import * as scenes from 'scenes/all';
// import * as items from 'items/all';
// import * as actors from 'actors/all';
import * as $gameres from 'game/resources/game-res';

export class BlackEntry extends snowatch.Scene {
  public static readonly ID = 'BlackEntry';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData, init?: snowatch.SceneInit) {
    super.initialize(game, {
      properties: {
        compAttributes: [
          { id: attrs.Title,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_BLACK_ENTRY);
                return snowatch.compose(sceneTitleResource.template, {});
              }
            }
          },
          { id: attrs.Description,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneDescResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_DESC_BLACK_ENTRY);
                return snowatch.compose(sceneDescResource.template, {});
              }
            }
          },
        ],
        attributes: [
          { id: attrs.SpatialLocation, initialState: { value: {x: 200, y: 200, z: 100}}},
          { id: attrs.SpatialSize, initialState: { value: {x: 10, y: 10, z: 10}}},
        ],
      },
      exits: [
      ],
    });
    return this;
  }
}
