import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

import * as $scenes from 'game/scenes/all';
import * as $actors from 'game/actors/all';
import * as $gameres from 'game/resources/game-res';

export class IntroWakeUp extends snowatch.Scene {
  public static readonly ID = 'IntroWakeUp';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData, init?: snowatch.SceneInit) {
    super.initialize(game, {
      hasGround: false,
      disablesInteractions: true,
      properties: {
        compAttributes: [
          { id: attrs.Description,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => snowatch.compose($gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_DESC_INTRO_WAKEUP).template)
          }
        },
        ],
      },
      exits: [
        { id: 'to-fall-or-sacristy-masculine',
          title: (exit, entity) => {
            const wakeupResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.COMMAND_LABEL_WAKE_UP);
            return snowatch.compose(wakeupResource.template, {
              verb: snowatch.capitalFirst(snowatch.Verb.get(this.game, wakeupResource.verbId).verb(snowatch.Number.singular, snowatch.Tense.past, snowatch.Person.third, snowatch.Gender.masculine))
            });
          },
          scene: (exit, entity) => {
            if( entity != null ) {
              // we need to set the gender at this point, not at success, beceuse we already are in next scene where we depend on gender being set properly when handling success
              attrs.Attribute.setValue<attrs.GenderIdValues>(entity, attrs.GenderId, attrs.GenderIdValues.Male);
              attrs.Attribute.setValue<string>(entity, attrs.NounResourceId, $gameres.EntityResDict.LAST_ONE_MASCULINE_NOUN);
            }
            return this.visitCounts.get(entity.id) === 1 ? $scenes.IntroFall.ID : $scenes.HubSacristy.ID;
          },
        },
        { id: 'to-fall-or-sacristy-feminine',
          title: (exit, entity) => {
            const wakeupResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.COMMAND_LABEL_WAKE_UP);
            return snowatch.compose(wakeupResource.template, {
              verb: snowatch.capitalFirst(snowatch.Verb.get(this.game, wakeupResource.verbId).verb(snowatch.Number.singular, snowatch.Tense.past, snowatch.Person.third, snowatch.Gender.feminine))
            });
          },
          scene: (exit, entity) => {
            if( entity != null ) {
              // we need to set the gender at this point, not at success, beceuse we already are in next scene where we depend on gender being set properly when handling success
              attrs.Attribute.setValue<attrs.GenderIdValues>(entity, attrs.GenderId, attrs.GenderIdValues.Female);
              attrs.Attribute.setValue<string>(entity, attrs.NounResourceId, $gameres.EntityResDict.LAST_ONE_FEMININE_NOUN);
            }
            return this.visitCounts.get(entity.id) === 1 ? $scenes.IntroFall.ID : $scenes.HubSacristy.ID;
          },
        },
      ],
    });
    return this;
  }
}
