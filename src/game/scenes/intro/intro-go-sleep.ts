import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

import * as $current from 'game/current-game/all';
import * as $behaviors from 'game/behaviors/all';
import * as $scenes from 'game/scenes/all';
import * as $items from 'game/items/all';
import * as $actors from 'game/actors/all';
import * as $gameres from 'game/resources/game-res';


export class IntroGoSleep extends snowatch.Scene {
  public static readonly ID = 'IntroGoSleep';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData, init?: snowatch.SceneInit) {
    let self = this;
    super.initialize(game, {
      hasGround: false,
      disablesInteractions: true,
      properties: {
        compAttributes: [
          { id: attrs.Description,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                if( actor == null ) {
                  return null;
                }

                const resource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_DESC_INTRO_GO_SLEEP);
                const actorNoun = snowatch.Noun.get(this.game, $gameres.ResourceDictionary.get(this.game, attrs.Attribute.value<string>(actor, attrs.NounResourceId, actor)));
                return snowatch.compose(resource.template, {
                    actorNoun: actorNoun.noun(snowatch.Case.nominative),
                    pressVerb: snowatch.Verb.get(this.game, resource.pressVerbId).verb(actorNoun.number, snowatch.Tense.past, snowatch.Person.third, actorNoun.gender, actorNoun.genderSubtype),
                    closeVerb: snowatch.capitalFirst(snowatch.Verb.get(this.game, resource.closeVerbId).verb(actorNoun.number, snowatch.Tense.past, snowatch.Person.third, actorNoun.gender, actorNoun.genderSubtype)),
                    sleepVerb: snowatch.Verb.get(this.game, resource.sleepVerbId).verb(actorNoun.number, snowatch.Tense.past, snowatch.Person.third, actorNoun.gender, actorNoun.genderSubtype),
                  }
                );
              }
            }},
        ],
      },
      exits: [
        { id: 'to-intro-wake-up',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.COMMAND_LABEL_CONTINUE);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.IntroWakeUp.ID,
        },
      ],
    });
    return this;
  }
}
