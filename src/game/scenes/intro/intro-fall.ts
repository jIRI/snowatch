import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

import * as $current from 'game/current-game/all';
import * as $behaviors from 'game/behaviors/all';
import * as $scenes from 'game/scenes/all';
import * as $items from 'game/items/all';
import * as $actors from 'game/actors/all';
import * as $gameres from 'game/resources/game-res';


export class IntroFall extends snowatch.Scene {
  public static readonly ID = 'IntroFall';

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData, init?: snowatch.SceneInit) {
    let self = this;
    super.initialize(game, {
      hasGround: false,
      disablesInteractions: true,
      properties: {
        compAttributes: [
          { id: attrs.Description,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                if( actor == null ) {
                  return null;
                }

                const sceneDescResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_DESC_INTRO_FALL);
                const actorNoun = snowatch.Noun.get(this.game, $gameres.ResourceDictionary.get(this.game, attrs.Attribute.value<string>(actor, attrs.NounResourceId, actor)));
                return snowatch.compose(sceneDescResource.template, {
                    fall: snowatch.capitalFirst(snowatch.Verb.get(this.game, sceneDescResource.fallVerbId).verb(actorNoun.number, snowatch.Tense.past, snowatch.Person.third, actorNoun.gender, actorNoun.genderSubtype)),
                    pronoun: snowatch.pickByGender(actor, sceneDescResource.pronounMasculine, sceneDescResource.pronounFeminine),
                    feel: snowatch.Verb.get(this.game, sceneDescResource.feelVerbId).verb(actorNoun.number, snowatch.Tense.past, snowatch.Person.third, actorNoun.gender, actorNoun.genderSubtype),
                    fallDown: snowatch.Verb.get(this.game, sceneDescResource.fallDownVerbId).verb(actorNoun.number, snowatch.Tense.past, snowatch.Person.third, actorNoun.gender, actorNoun.genderSubtype),
                  }
                );
              }
            }},
        ],
      },
      exits: [
        { id: 'to-hub-sacristy',
          title: (exit, actor) => {
            if( actor == null ) {
              return '<undefined actor>';
            }
            const lookaroundResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.COMMAND_LABEL_LOOK_AROUND);
            const actorNoun = snowatch.Noun.get(this.game, $gameres.ResourceDictionary.get(this.game, attrs.Attribute.value<string>(actor, attrs.NounResourceId, actor)));
            return snowatch.compose(lookaroundResource.template, {
              verb: snowatch.capitalFirst(snowatch.Verb.get(this.game, lookaroundResource.verbId).verb(actorNoun.number, snowatch.Tense.past, snowatch.Person.third, actorNoun.gender, actorNoun.genderSubtype))
            });
          },
          scene: $scenes.HubSacristy.ID,
        },
      ],
    });
    return this;
  }
}
