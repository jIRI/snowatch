import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

import * as $scenes from 'game/scenes/all';
import * as $gameres from 'game/resources/game-res';

export class WhiteLobby extends snowatch.Scene {
  public static readonly ID = 'WhiteLobby';
  public static readonly SpatialLocation: snowatch.Vector3 = {x: 0, y: 0, z: 0};
  public static readonly SpatialSize: snowatch.Vector3 = {x: 7, y: 7, z: $scenes.WhiteRealmSpatialSize.z};

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData, init?: snowatch.SceneInit) {
    super.initialize(game, {
      properties: {
        compAttributes: [
          { id: attrs.Title,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_LOBBY);
                return snowatch.compose(sceneTitleResource.template, {});
              }
            }
          },
          { id: attrs.Description,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneDescResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_DESC_WHITE_LOBBY);
                return snowatch.compose(sceneDescResource.template, {});
              }
            }
          },
        ],
        constAttributes: [
          { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.WhiteRealmSpatialLocation, WhiteLobby.SpatialLocation)}},
          { id: attrs.SpatialSize, constState: { value: WhiteLobby.SpatialSize}},
        ],
      },
      exits: [
        { id: 'to-white-left-upstairs',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_LEFT_UPSTAIRS);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteLeftUpstairs.ID,
        },
        { id: 'to-white-left-service-room',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_LEFT_SERVICE_ROOM);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteLeftServiceRoom.ID,
        },
        { id: 'to-white-right-upstairs',
        title: (exit, actor: snowatch.Entity) => {
          const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_RIGHT_UPSTAIRS);
          return snowatch.compose(sceneTitleResource.template, {});
        },
        scene: $scenes.WhiteRightUpstairs.ID,
      },
      { id: 'to-white-right-service-room',
        title: (exit, actor: snowatch.Entity) => {
          const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_RIGHT_SERVICE_ROOM);
          return snowatch.compose(sceneTitleResource.template, {});
        },
        scene: $scenes.WhiteRightServiceRoom.ID,
      },
      ],
    });
    return this;
  }
}
