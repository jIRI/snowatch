import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

import * as $scenes from 'game/scenes/all';
import * as $gameres from 'game/resources/game-res';

export class WhiteRightHallway extends snowatch.Scene {
  public static readonly ID = 'WhiteRightHallway';
  public static readonly SpatialLocation: snowatch.Vector3 = {x: 14.5, y: 13, z: 6};
  public static readonly SpatialSize: snowatch.Vector3 = {x: 1, y: 12, z: $scenes.WhiteRealmSpatialSize.z - 6};

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData, init?: snowatch.SceneInit) {
    super.initialize(game, {
      properties: {
        compAttributes: [
          { id: attrs.Title,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_RIGHT_HALLWAY);
                return snowatch.compose(sceneTitleResource.template, {});
              }
            }
          },
          { id: attrs.Description,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneDescResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_DESC_WHITE_RIGHT_HALLWAY);
                return snowatch.compose(sceneDescResource.template, {});
              }
            }
          },
        ],
        constAttributes: [
          { id: attrs.NounResourceId, constState: { value: $gameres.SceneResDict.SCENE_TITLE_NOUN_HALLWAY }},
          { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.WhiteRealmSpatialLocation, WhiteRightHallway.SpatialLocation)}},
          { id: attrs.SpatialSize, constState: { value: WhiteRightHallway.SpatialSize}},
        ],
      },
      exits: [
        { id: 'to-white-right-reception-from-right-hallway',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_RIGHT_RECEPTION);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteRightReception.ID,
        },
        { id: 'to-white-right-patient-room-south-from-right-hallway',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_RIGHT_PATIENT_ROOM_SOUTH);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteRightPatientRoomSouth.ID,
        },
        { id: 'to-white-right-balcony-from-right-hallway',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_RIGHT_BALCONY);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteRightBalcony.ID,
        },
        { id: 'to-white-right-patient-room-north-from-right-hallway',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_RIGHT_PATIENT_ROOM_NORTH);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteRightPatientRoomNorth.ID,
        },
        { id: 'to-white-right-mess-hall-from-right-hallway',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_RIGHT_MESS_HALL);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteRightMessHall.ID,
        },
      ],
    });
    return this;
  }
}
