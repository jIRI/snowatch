import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

import * as $scenes from 'game/scenes/all';
import * as $gameres from 'game/resources/game-res';

export class WhiteRightMessHall extends snowatch.Scene {
  public static readonly ID = 'WhiteRightMessHall';
  public static readonly SpatialLocation: snowatch.Vector3 = {x: 11, y: 20, z: 6};
  public static readonly SpatialSize: snowatch.Vector3 = {x: 8, y: 7, z: $scenes.WhiteRealmSpatialSize.z - 6};

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData, init?: snowatch.SceneInit) {
    super.initialize(game, {
      properties: {
        compAttributes: [
          { id: attrs.Title,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_RIGHT_MESS_HALL);
                return snowatch.compose(sceneTitleResource.template, {});
              }
            }
          },
          { id: attrs.Description,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneDescResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_DESC_WHITE_RIGHT_MESS_HALL);
                return snowatch.compose(sceneDescResource.template, {});
              }
            }
          },
        ],
        constAttributes: [
          { id: attrs.NounResourceId, constState: { value: $gameres.SceneResDict.SCENE_TITLE_NOUN_MESS_HALL }},
          { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.WhiteRealmSpatialLocation, WhiteRightMessHall.SpatialLocation)}},
          { id: attrs.SpatialSize, constState: { value: WhiteRightMessHall.SpatialSize}},
        ],
      },
      exits: [
        { id: 'to-white-right-hallway-from-right-mess-hall',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_RIGHT_HALLWAY);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteRightHallway.ID,
        },
        { id: 'to-white-right-terrace-entrance-from-right-mess-hall',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_TERRACE_ENTRANCE);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteTerraceEntrance.ID,
        },
      ],
    });
    return this;
  }
}
