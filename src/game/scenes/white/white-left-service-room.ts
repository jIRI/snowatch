import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

import * as $scenes from 'game/scenes/all';
import * as $gameres from 'game/resources/game-res';

export class WhiteLeftServiceRoom extends snowatch.Scene {
  public static readonly ID = 'WhiteLeftServiceRoom';
  public static readonly SpatialLocation: snowatch.Vector3 = {x: -11, y: 0.5, z: 0};
  public static readonly SpatialSize: snowatch.Vector3 = {x: 8, y: 3, z: 4};

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData, init?: snowatch.SceneInit) {
    super.initialize(game, {
      properties: {
        compAttributes: [
          { id: attrs.Title,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_LEFT_SERVICE_ROOM);
                return snowatch.compose(sceneTitleResource.template, {});
              }
            }
          },
          { id: attrs.Description,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneDescResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_DESC_WHITE_LEFT_SERVICE_ROOM);
                return snowatch.compose(sceneDescResource.template, {});
              }
            }
          },
        ],
        constAttributes: [
          { id: attrs.NounResourceId, constState: { value: $gameres.SceneResDict.SCENE_TITLE_NOUN_SERVICE_ROOM }},
          { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.WhiteRealmSpatialLocation, WhiteLeftServiceRoom.SpatialLocation)}},
          { id: attrs.SpatialSize, constState: { value: WhiteLeftServiceRoom.SpatialSize}},
        ],
      },
      exits: [
        { id: 'to-white-lobby-from-left-service-room',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_LOBBY);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteLobby.ID,
        },
      ],
    });
    return this;
  }
}
