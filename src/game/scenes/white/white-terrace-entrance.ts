import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

import * as $scenes from 'game/scenes/all';
import * as $gameres from 'game/resources/game-res';

export class WhiteTerraceEntrance extends snowatch.Scene {
  public static readonly ID = 'WhiteTerraceEntrance';
  public static readonly SpatialLocation: snowatch.Vector3 = {x: -12, y: 5, z: 6};
  public static readonly SpatialSize: snowatch.Vector3 = {x: 6, y: 4, z: $scenes.WhiteRealmSpatialSize.z - 6};

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData, init?: snowatch.SceneInit) {
    super.initialize(game, {
      properties: {
        compAttributes: [
          { id: attrs.Title,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_TERRACE_ENTRANCE);
                return snowatch.compose(sceneTitleResource.template, {});
              }
            }
          },
          { id: attrs.Description,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneDescResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_DESC_WHITE_TERRACE_ENTRANCE);
                return snowatch.compose(sceneDescResource.template, {});
              }
            }
          },
        ],
        attributes: [
          { id: attrs.SpatialLocation, initialState: { value: snowatch.addVect3($scenes.WhiteRealmSpatialLocation, WhiteTerraceEntrance.SpatialLocation)}},
          { id: attrs.SpatialSize, initialState: { value: WhiteTerraceEntrance.SpatialSize}},
        ],
      },
      exits: [
        { id: 'to-white-left-mess-hall-from-terrace-entrance',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_LEFT_MESS_HALL);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteLeftMessHall.ID,
        },
        { id: 'to-white-right-mess-hall-from-terrace-entrance',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_RIGHT_MESS_HALL);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteRightMessHall.ID,
        },
      ],
    });
    return this;
  }
}
