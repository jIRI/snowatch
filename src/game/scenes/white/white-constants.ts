import { Vector3, addVect3 } from "snowatch/spatial-relations";

export const WhiteRealmAnchorLocation: Vector3 = {x: 0, y: -10, z: 0};
export const WhiteRealmSpatialLocation: Vector3 = addVect3(WhiteRealmAnchorLocation, {x: 1000, y: 1000, z: 0});
export const WhiteRealmSpatialSize: Vector3 = {x: 2 * 18, y: 22, z: 20};