import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

import * as $scenes from 'game/scenes/all';
import * as $gameres from 'game/resources/game-res';

export class WhiteLeftPatientRoomNorth extends snowatch.Scene {
  public static readonly ID = 'WhiteLeftPatientRoomNorth';
  public static readonly SpatialLocation: snowatch.Vector3 = {x: -12.5, y: 15, z: 6};
  public static readonly SpatialSize: snowatch.Vector3 = {x: 3, y: 6, z: $scenes.WhiteRealmSpatialSize.z - 6};

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData, init?: snowatch.SceneInit) {
    super.initialize(game, {
      properties: {
        compAttributes: [
          { id: attrs.Title,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_LEFT_PATIENT_ROOM_NORTH);
                return snowatch.compose(sceneTitleResource.template, {});
              }
            }
          },
          { id: attrs.Description,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneDescResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_DESC_WHITE_LEFT_PATIENT_ROOM_NORTH);
                return snowatch.compose(sceneDescResource.template, {});
              }
            }
          },
        ],
        constAttributes: [
          { id: attrs.NounResourceId, constState: { value: $gameres.SceneResDict.SCENE_TITLE_NOUN_PATIENT_ROOM_NORTH }},
          { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.WhiteRealmSpatialLocation, WhiteLeftPatientRoomNorth.SpatialLocation)}},
          { id: attrs.SpatialSize, constState: { value: WhiteLeftPatientRoomNorth.SpatialSize}},
        ],
      },
      exits: [
        { id: 'to-white-left-hallway-from-left-patient-room-north',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_LEFT_HALLWAY);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteLeftHallway.ID,
        },
      ],
    });
    return this;
  }
}
