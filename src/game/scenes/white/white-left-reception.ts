import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

import * as $scenes from 'game/scenes/all';
import * as $gameres from 'game/resources/game-res';

export class WhiteLeftReception extends snowatch.Scene {
  public static readonly ID = 'WhiteLeftReception';
  public static readonly SpatialLocation: snowatch.Vector3 = {x: -12, y: 5, z: 6};
  public static readonly SpatialSize: snowatch.Vector3 = {x: 6, y: 4, z: $scenes.WhiteRealmSpatialSize.z - 6};

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData, init?: snowatch.SceneInit) {
    super.initialize(game, {
      properties: {
        compAttributes: [
          { id: attrs.Title,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_LEFT_RECEPTION);
                return snowatch.compose(sceneTitleResource.template, {});
              }
            }
          },
          { id: attrs.Description,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneDescResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_DESC_WHITE_LEFT_RECEPTION);
                return snowatch.compose(sceneDescResource.template, {});
              }
            }
          },
        ],
        constAttributes: [
          { id: attrs.NounResourceId, constState: { value: $gameres.SceneResDict.SCENE_TITLE_NOUN_RECEPTION }},
          { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.WhiteRealmSpatialLocation, WhiteLeftReception.SpatialSize)}},
          { id: attrs.SpatialSize, constState: { value: WhiteLeftReception.SpatialSize}},
        ],
      },
      exits: [
        { id: 'to-white-left-upstairs-from-left-reception',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_LEFT_UPSTAIRS);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteLeftUpstairs.ID,
        },
        { id: 'to-white-left-lab-from-left-reception',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_LEFT_LAB);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteLeftLab.ID,
        },
        { id: 'to-white-left-hallway-from-left-reception',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_LEFT_HALLWAY);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteLeftHallway.ID,
        },
      ],
    });
    return this;
  }
}
