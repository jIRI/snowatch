import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

import * as $scenes from 'game/scenes/all';
import * as $gameres from 'game/resources/game-res';
import { WhiteRealmSpatialLocation } from './white-constants';

export class WhiteEnteringForTheFirstTime extends snowatch.Scene {
  public static readonly ID = 'WhiteEnteringForTheFirstTime';
  public static readonly SpatialLocation: snowatch.Vector3 = {x: 0, y: 0, z: 0};
  public static readonly SpatialSize: snowatch.Vector3 = {x: 7, y: 7, z: $scenes.WhiteRealmSpatialSize.z};

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData, init?: snowatch.SceneInit) {
    super.initialize(game, {
      disablesInteractions: true,
      properties: {
        compAttributes: [
          { id: attrs.Title,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_ENTERING_FOR_THE_FIRST_TIME);
                return snowatch.compose(sceneTitleResource.template, {});
              }
            }
          },
          { id: attrs.Description,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                if( actor == null ) {
                  return null;
                }

                const sceneDescResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_DESC_WHITE_ENTERING_FOR_THE_FIRST_TIME);
                const actorNoun = snowatch.Noun.get(this.game, $gameres.ResourceDictionary.get(this.game, attrs.Attribute.value<string>(actor, attrs.NounResourceId, actor)));
                return snowatch.compose(sceneDescResource.template, {
                  actorNoun: actorNoun.noun(snowatch.Case.nominative),
                  transportVerb: snowatch.Verb.get(this.game, sceneDescResource.transportVerbId).verb(actorNoun.number, snowatch.Tense.past, snowatch.Person.third, actorNoun.gender, actorNoun.genderSubtype),
                });
              }
            }
          },
        ],
        constAttributes: [
          { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.WhiteRealmSpatialLocation, WhiteEnteringForTheFirstTime.SpatialLocation)}},
          { id: attrs.SpatialSize, constState: { value: WhiteEnteringForTheFirstTime.SpatialSize}},
        ],
      },
      exits: [
        { id: 'to-white-lobby',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.COMMAND_LABEL_CONTINUE);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteLobby.ID,
        },
      ],
    });
    return this;
  }
}
