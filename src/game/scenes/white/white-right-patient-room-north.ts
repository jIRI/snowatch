import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

import * as $scenes from 'game/scenes/all';
import * as $gameres from 'game/resources/game-res';

export class WhiteRightPatientRoomNorth extends snowatch.Scene {
  public static readonly ID = 'WhiteRightPatientRoomNorth';
  public static readonly SpatialLocation: snowatch.Vector3 = {x: 12.5, y: 15, z: 6};
  public static readonly SpatialSize: snowatch.Vector3 = {x: 3, y: 6, z: $scenes.WhiteRealmSpatialSize.z - 6};

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData, init?: snowatch.SceneInit) {
    super.initialize(game, {
      properties: {
        compAttributes: [
          { id: attrs.Title,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_RIGHT_PATIENT_ROOM_NORTH);
                return snowatch.compose(sceneTitleResource.template, {});
              }
            }
          },
          { id: attrs.Description,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneDescResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_DESC_WHITE_RIGHT_PATIENT_ROOM_NORTH);
                return snowatch.compose(sceneDescResource.template, {});
              }
            }
          },
        ],
        constAttributes: [
          { id: attrs.NounResourceId, constState: { value: $gameres.SceneResDict.SCENE_TITLE_NOUN_PATIENT_ROOM_NORTH }},
          { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.WhiteRealmSpatialLocation, WhiteRightPatientRoomNorth.SpatialLocation)}},
          { id: attrs.SpatialSize, constState: { value: WhiteRightPatientRoomNorth.SpatialSize}},
        ],
      },
      exits: [
        { id: 'to-white-right-hallway-from-right-patient-room-north',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_RIGHT_HALLWAY);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteRightHallway.ID,
        },
      ],
    });
    return this;
  }
}
