import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

import * as $scenes from 'game/scenes/all';
import * as $gameres from 'game/resources/game-res';

export class WhiteRightUpstairs extends snowatch.Scene {
  public static readonly ID = 'WhiteRightUpstairs';
  public static readonly SpatialLocation: snowatch.Vector3 = {x: 8, y: 5, z: 6};
  public static readonly SpatialSize: snowatch.Vector3 = {x: 2, y: 2, z: $scenes.WhiteRealmSpatialSize.z - 6};

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData, init?: snowatch.SceneInit) {
    super.initialize(game, {
      properties: {
        compAttributes: [
          { id: attrs.Title,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_RIGHT_UPSTAIRS);
                return snowatch.compose(sceneTitleResource.template, {});
              }
            }
          },
          { id: attrs.Description,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneDescResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_DESC_WHITE_RIGHT_UPSTAIRS);
                return snowatch.compose(sceneDescResource.template, {});
              }
            }
          },
        ],
        constAttributes: [
          { id: attrs.NounResourceId, constState: { value: $gameres.SceneResDict.SCENE_TITLE_NOUN_UPSTAIRS }},
          { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.WhiteRealmSpatialLocation, WhiteRightUpstairs.SpatialLocation)}},
          { id: attrs.SpatialSize, constState: { value: WhiteRightUpstairs.SpatialSize}},
        ],
      },
      exits: [
        { id: 'to-white-lobby-from-right-upstairs',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_LOBBY);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteLobby.ID,
        },
        { id: 'to-white-right-reception',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_RIGHT_RECEPTION);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteRightReception.ID,
        },
        { id: 'to-white-left-upstairs-from-right-upstairs',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_LEFT_UPSTAIRS);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteLeftUpstairs.ID,
        },
      ],
    });
    return this;
  }
}
