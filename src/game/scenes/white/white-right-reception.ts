import * as snowatch from 'snowatch/snowatch';
import * as attrs from 'snowatch/attributes/all';

import * as $scenes from 'game/scenes/all';
import * as $gameres from 'game/resources/game-res';

export class WhiteRightReception extends snowatch.Scene {
  public static readonly ID = 'WhiteRightReception';
  public static readonly SpatialLocation: snowatch.Vector3 = {x: 12, y: 5, z: 6};
  public static readonly SpatialSize: snowatch.Vector3 = {x: 6, y: 4, z: $scenes.WhiteRealmSpatialSize.z - 6};

  constructor() {
    super();
  }

  initialize(game: snowatch.GameData, init?: snowatch.SceneInit) {
    super.initialize(game, {
      properties: {
        compAttributes: [
          { id: attrs.Title,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_RIGHT_RECEPTION);
                return snowatch.compose(sceneTitleResource.template, {});
              }
            }
          },
          { id: attrs.Description,
            compState: {
              value: (entity: snowatch.StatelessEntity, attributeId: string, actor?: snowatch.Entity) => {
                const sceneDescResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_DESC_WHITE_RIGHT_RECEPTION);
                return snowatch.compose(sceneDescResource.template, {});
              }
            }
          },
        ],
        constAttributes: [
          { id: attrs.NounResourceId, constState: { value: $gameres.SceneResDict.SCENE_TITLE_NOUN_RECEPTION }},
          { id: attrs.SpatialLocation, constState: { value: snowatch.addVect3($scenes.WhiteRealmSpatialLocation, WhiteRightReception.SpatialLocation)}},
          { id: attrs.SpatialSize, constState: { value: WhiteRightReception.SpatialSize}},
        ],
      },
      exits: [
        { id: 'to-white-right-upstairs-from-right-reception',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_RIGHT_UPSTAIRS);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteRightUpstairs.ID,
        },
        { id: 'to-white-right-lab-from-right-reception',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_RIGHT_LAB);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteRightLab.ID,
        },
        { id: 'to-white-right-hallway-from-right-reception',
          title: (exit, actor: snowatch.Entity) => {
            const sceneTitleResource = $gameres.ResourceDictionary.get(this.game, $gameres.SceneResDict.SCENE_TITLE_WHITE_RIGHT_HALLWAY);
            return snowatch.compose(sceneTitleResource.template, {});
          },
          scene: $scenes.WhiteRightHallway.ID,
        },
      ],
    });
    return this;
  }
}
