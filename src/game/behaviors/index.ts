import {FrameworkConfiguration} from 'aurelia-framework';

import {Behavior} from 'snowatch/behavior';

import * as $behaviors from 'game/behaviors/all';

export function configure(aurelia: FrameworkConfiguration) {
  aurelia.singleton(Behavior, $behaviors.ResetItemOnSleep);
  aurelia.singleton(Behavior, $behaviors.CrystalPressed);
  aurelia.singleton(Behavior, $behaviors.UpdateNumericAttributeOnDrinkingLiquid);
  aurelia.singleton(Behavior, $behaviors.ColorOnDrinkingLiquid);
}