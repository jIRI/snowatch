import * as snowatch from 'snowatch/snowatch';

import {clamp} from 'snowatch/utils';
import * as attrs from 'snowatch/attributes/all';
import * as $attrs from 'game/current-game/attributes';
import { BehaviorState } from 'snowatch/snowatch';

export interface ColorOnDrinkingLiquidState extends BehaviorState {
}


export class ColorOnDrinkingLiquid extends snowatch.Behavior {
  public static readonly ID = 'ColorOnDrinkingLiquid';

  filter(target: snowatch.Entity, state: ColorOnDrinkingLiquidState | null, message: snowatch.Message): snowatch.ResultObject {
    if( message.topicId !== snowatch.Topic.id(snowatch.Topics.ATTRIBUTE_VALUE_CHANGED) ) {
      return snowatch.make(snowatch.rejected);
    }

    const messagePayload = snowatch.payload<snowatch.Topics.ATTRIBUTE_VALUE_CHANGED>(message);
    return (messagePayload.attributeId === $attrs.CurrentlyActiveEssenceLiquid)
      ? snowatch.make(snowatch.accepted) : snowatch.make(snowatch.rejected);
  }

  process(target: snowatch.Entity, state: ColorOnDrinkingLiquidState | null, message: snowatch.Message) {
    const messagePayload = snowatch.payload<snowatch.Topics.ATTRIBUTE_VALUE_CHANGED>(message);
    const actor = target.game.instances.getEntityFromId(messagePayload.entityId);
    if( actor == null ) {
      return;
    }

    const attrValue = snowatch.Attribute.value<$attrs.CurrentlyActiveEssenceLiquidValueType>(actor, $attrs.CurrentlyActiveEssenceLiquid, actor);
    if( attrValue == null ) {
      snowatch.Attribute.setValue<attrs.ColorIdValues>(target, attrs.ColorId, attrs.ColorIdValues.None);
      return;
    }

    const substance = target.game.instances.getEntityFromId(attrValue.substanceId);
    if( substance == null ) {
      return;
    }

    if( snowatch.Attribute.applies(target, attrs.ColorId) ) {
      const colorId = snowatch.Attribute.value<attrs.ColorIdValues>(substance, attrs.ColorId, actor);
      snowatch.Attribute.setValue<attrs.ColorIdValues>(target, attrs.ColorId, colorId);
    }
  }
}