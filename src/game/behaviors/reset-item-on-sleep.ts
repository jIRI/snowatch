import * as snowatch from 'snowatch/snowatch';

import * as $current from 'game/current-game/all';
import { BehaviorState } from 'snowatch/snowatch';

export interface ResetItemOnSleepState extends BehaviorState {
}

export class ResetItemOnSleep extends snowatch.Behavior {
  public static readonly ID = 'ResetItemOnSleep';

  filter(target: snowatch.Entity, state: ResetItemOnSleepState | null, message: snowatch.Message): snowatch.ResultObject {
    return (message.topicId === snowatch.Topic.id($current.CustomTopics.GO_SLEEP)) ? snowatch.make(snowatch.accepted) : snowatch.make(snowatch.rejected);
  }

  process(target: snowatch.Entity, state: ResetItemOnSleepState | null, message: snowatch.Message) {
    // reset all state here
    const startSceneId = snowatch.getStartSceneId(target);
    if( startSceneId != null ) {
      snowatch.setSceneId(target, startSceneId);
    }

    const itemInit = <snowatch.ItemInit>target.init;
    // reset owners etc.
    // remove from all inventories
    snowatch.setOwnerId(target, itemInit.holderId);
    const holderId = snowatch.getHolderId(target);
    const holder = holderId != null ? target.game.instances.getEntityFromId(holderId) : null;
    if( holder != null && snowatch.Inventory.hasInAny(holder, target.id) ) {
      snowatch.Inventory.removeItem(holder, target.id);
    }
    snowatch.setHolderId(target, itemInit.holderId);
    const containerId = snowatch.getContainerId(target);
    if( containerId != null ) {
      const container = target.game.instances.getEntityFromId(containerId);
      if( container != null ) {
        snowatch.Container.remove(container, target.id);
      }
    }

    // remove and recreate spatial relations
    snowatch.SpatialRelations.removeRelationsOf(target.game, target.id);
    snowatch.SpatialRelations.addFromProperties(target, target.properties);

    // iterate over all attributes and reset them (beware: this ignores attribs added later, so all of the need to be initialized in entity initializer)
    for(let [attrId, attrInit] of target.properties.attributes) {
      snowatch.Attribute.setValue<any>(target, attrId, attrInit);
    }
  }
}