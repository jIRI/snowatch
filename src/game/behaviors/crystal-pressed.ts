import * as snowatch from 'snowatch/snowatch';
import * as skills from 'snowatch/skills/all';
import * as attrs from 'snowatch/attributes/all';

import * as $current from 'game/current-game/all';
import * as $items from 'game/items/all';
import * as $gameres from 'game/resources/game-res';
import { BehaviorState } from 'snowatch/snowatch';

export interface CrystalPressedState extends BehaviorState {
  actorId: string;
}


export class CrystalPressed extends snowatch.Behavior {
  public static readonly ID = 'CrystalPressed';

  filter(target: snowatch.Entity, state: CrystalPressedState | null, message: snowatch.Message): snowatch.ResultObject {
    if( message.topicId !== snowatch.Topic.id(snowatch.Topics.SKILL_ACCEPTED) ) {
      return snowatch.make(snowatch.rejected);
    }

    const messagePayload = snowatch.payload<snowatch.Topics.SKILL_ACCEPTED>(message);
    return (messagePayload.skillId === skills.Press.ID
      && messagePayload.directId === $items.Crystal.ID)
      ? snowatch.make(snowatch.accepted) : snowatch.make(snowatch.rejected);
  }

  process(target: snowatch.Entity, state: CrystalPressedState | null, message: snowatch.Message) {
    if( state == null ) {
      this.logger.debug(`No state for behavior ${this.id} of entity ${target.id}`);
      return;
    }

    const messagePayload = snowatch.payload<snowatch.Topics.SKILL_ACCEPTED>(message);
    const actor = <snowatch.Actor>target.game.instances.getEntityFromId(state.actorId);
    switch(messagePayload.modifierId) {
      case skills.PressModifiers.weakly.id:
        const resource = snowatch.ResourceDictionary.get(target.game, $gameres.ActionResDict.CRYSTAL_PRESSED_WEAKLY_DESC);
        const actorNoun = snowatch.Noun.get(target.game, $gameres.ResourceDictionary.get(target.game, attrs.Attribute.value<string>(actor, attrs.NounResourceId, actor)));
        const text = snowatch.compose(resource.template, {
          actorNoun: actorNoun.noun(snowatch.Case.nominative),
          pressVerb: snowatch.Verb.get(target.game, resource.pressVerbId).verb(actorNoun.number, snowatch.Tense.past, snowatch.Person.third, actorNoun.gender, actorNoun.genderSubtype),
          feelVerb: snowatch.Verb.get(target.game, resource.feelVerbId).verb(actorNoun.number, snowatch.Tense.past, snowatch.Person.third, actorNoun.gender, actorNoun.genderSubtype),
        });
        target.game.addToGameLog({
          source: actor.id,
          action: messagePayload.skillId,
          description: text,
        });
        break;
      case skills.PressModifiers.strongly.id:
        target.game.bus.notifications.publish(snowatch.broadcast($current.CustomTopics.GO_SLEEP));
        break;
    }
  }
}