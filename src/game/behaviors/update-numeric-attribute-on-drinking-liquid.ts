import * as snowatch from 'snowatch/snowatch';

import {clamp} from 'snowatch/utils';
import * as attrs from 'snowatch/attributes/all';
import * as $attrs from 'game/current-game/attributes';
import { BehaviorState } from 'snowatch/snowatch';

export interface UpdateNumericAttributeOnDrinkingLiquidState extends BehaviorState {
}

export class UpdateNumericAttributeOnDrinkingLiquid extends snowatch.Behavior {
  public static readonly ID = 'UpdateNumericAttributeOnDrinkingLiquid';

  filter(target: snowatch.Entity, state: UpdateNumericAttributeOnDrinkingLiquidState | null, message: snowatch.Message): snowatch.ResultObject {
    if( message.topicId !== snowatch.Topic.id(snowatch.Topics.DRUNK) ) {
      return snowatch.make(snowatch.rejected);
    }

    const messagePayload = snowatch.payload<snowatch.Topics.DRUNK>(message);
    return (messagePayload.actorId === target.id)
      ? snowatch.make(snowatch.accepted) : snowatch.make(snowatch.rejected);
  }

  process(target: snowatch.Entity, state: UpdateNumericAttributeOnDrinkingLiquidState | null, message: snowatch.Message) {
    const messagePayload = snowatch.payload<snowatch.Topics.DRUNK>(message);
    const substance = target.game.instances.getEntityFromId(messagePayload.substanceId);
    if( substance == null ) {
      return;
    }
    const actor = target.game.instances.getEntityFromId(messagePayload.actorId);

    // apply modifiers only if substance is active
    const isLiquidActive = snowatch.Attribute.value<boolean>(substance, $attrs.IsSubstanceActive, actor);
    if( !isLiquidActive ) {
      return;
    }

    if( snowatch.Attribute.applies(substance, $attrs.AttributeToUpdateOnLiquidDrinking, actor) ) {
      const attrToUpdate = snowatch.Attribute.value<$attrs.AttributeUpdateOnLiquidDrinkingValueType>(substance, $attrs.AttributeToUpdateOnLiquidDrinking, actor);
      const currentlyAppliedEssenceModifier = snowatch.Attribute.value<$attrs.CurrentlyActiveEssenceLiquidValueType>(target, $attrs.CurrentlyActiveEssenceLiquid, actor);
      if( snowatch.Attribute.applies(target, attrToUpdate.id, actor) ) {
        let valueToAdd: any = null;
        let currentLiquid: any = null;
        if( currentlyAppliedEssenceModifier == null ) {
          valueToAdd = attrToUpdate;
        } else if( currentlyAppliedEssenceModifier.updateAttribId !== attrToUpdate.id ) {
          currentLiquid = currentlyAppliedEssenceModifier;
          valueToAdd = attrToUpdate;
        } else if( attrToUpdate.potency === attrs.Potency.Multipotent ) {
          valueToAdd = attrToUpdate;
        }

        if( currentLiquid != null ) {
          const attrValue = snowatch.Attribute.value<attrs.NumericValueAttribute>(target, currentLiquid.updateAttribId, actor);
          attrValue.value = clamp(attrValue.value - currentLiquid.amount, attrValue.minValue, attrValue.maxValue);
          snowatch.Attribute.setValue<attrs.NumericValueAttribute>(target, currentLiquid.updateAttribId, attrValue);
        }

        if( valueToAdd != null ) {
          const attrValue = snowatch.Attribute.value<attrs.NumericValueAttribute>(target, valueToAdd.id, actor);
          attrValue.value = clamp(attrValue.value + valueToAdd.amount, attrValue.minValue, attrValue.maxValue);
          snowatch.Attribute.setValue<attrs.NumericValueAttribute>(target, valueToAdd.id, attrValue);
          snowatch.Attribute.setValue<$attrs.CurrentlyActiveEssenceLiquidValueType>(
            target,
            $attrs.CurrentlyActiveEssenceLiquid,
            { updateAttribId: valueToAdd.id, substanceId: messagePayload.substanceId, amount: valueToAdd.amount}
          );
        }
      }
    } else if( snowatch.Attribute.applies(substance, $attrs.RemoveCurrentlyActiveEssenceLiquidEffect, actor) ) {
      const currentlyAppliedEssenceModifier = snowatch.Attribute.value<$attrs.CurrentlyActiveEssenceLiquidValueType>(target, $attrs.CurrentlyActiveEssenceLiquid, actor);
      if( currentlyAppliedEssenceModifier != null ) {
        const attrValue = snowatch.Attribute.value<attrs.NumericValueAttribute>(target, currentlyAppliedEssenceModifier.updateAttribId, actor);
        attrValue.value = clamp(attrValue.value - currentlyAppliedEssenceModifier.amount, attrValue.minValue, attrValue.maxValue);
        snowatch.Attribute.setValue<attrs.NumericValueAttribute>(target, currentlyAppliedEssenceModifier.updateAttribId, attrValue);
        snowatch.Attribute.setValue<$attrs.CurrentlyActiveEssenceLiquidValueType>(target, $attrs.CurrentlyActiveEssenceLiquid, null);
      }
    }
  }
}