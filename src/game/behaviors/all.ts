export * from 'snowatch/behaviors/all';

export * from 'game/behaviors/reset-item-on-sleep';
export * from 'game/behaviors/crystal-pressed';
export * from 'game/behaviors/update-numeric-attribute-on-drinking-liquid';
export * from 'game/behaviors/color-on-drinking-liquid';
