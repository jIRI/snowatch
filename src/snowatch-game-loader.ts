/// <reference types="aurelia-loader-webpack/src/webpack-hot-interface"/>
// we want font-awesome to load as soon as possible to show the fa-spinner
import './static/styles.css';
import './static/bootstrap-darkly.min.css';
import 'snowatch/styles/snowatch.scss';
import { Aurelia } from 'aurelia-framework';
import { PLATFORM } from 'aurelia-pal';

import {LogManager} from 'aurelia-framework';
import {ConsoleAppender} from 'aurelia-logging-console';
import { DialogConfiguration } from 'aurelia-dialog';
import { dilaogCssText } from 'snowatch/views/dialogs/dialog-styles';

LogManager.addAppender(new ConsoleAppender());
LogManager.setLevel(LogManager.logLevel.debug);
const logger = LogManager.getLogger('snowatch-game-loader');


export async function configure(aurelia: Aurelia) {
  logger.info('Starting story...');
  aurelia.use
    .standardConfiguration()
    .plugin(PLATFORM.moduleName('aurelia-dialog'), (cfg: DialogConfiguration) => {
      cfg.useDefaults().useCSS(dilaogCssText);
    })
    .feature(PLATFORM.moduleName('snowatch/resources/index'))
    .feature(PLATFORM.moduleName('snowatch/attributes/index'))
    .feature(PLATFORM.moduleName('snowatch/message-topics/index'))
    .feature(PLATFORM.moduleName('snowatch/skills/index'))
    .feature(PLATFORM.moduleName('snowatch/behaviors/index'))
    .feature(PLATFORM.moduleName('snowatch/items/index'))
    .feature(PLATFORM.moduleName('snowatch/views/index'))
    .feature(PLATFORM.moduleName('snowatch/calculators/index'))
    .feature(PLATFORM.moduleName('game/current-game/index'))
    .feature(PLATFORM.moduleName('game/dictionary/index'))
    .feature(PLATFORM.moduleName('game/resources/index'))
    .feature(PLATFORM.moduleName('game/behaviors/index'))
    .feature(PLATFORM.moduleName('game/realms/index'))
    .feature(PLATFORM.moduleName('game/scenes/index'))
    .feature(PLATFORM.moduleName('game/actors/index'))
    .feature(PLATFORM.moduleName('game/items/index'))
    .feature(PLATFORM.moduleName('game/entities/index'))
    .feature(PLATFORM.moduleName('game/substances/index'))
    .feature(PLATFORM.moduleName('game/knowledge/index'))
    .feature(PLATFORM.moduleName('snowatch/debug-extensions/index'));

  await aurelia.start();
  await aurelia.setRoot(PLATFORM.moduleName('app'));
}
