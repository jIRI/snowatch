# Decision log

Note: I've started this almost 3 years in development, so many things are kind of lost, but still: better late than never.

So, I guess, this is file where I keep my design notes and stuff.

## Basic design choices

## Building and development

## Bus and channels

Messages can be frozen on publish. This is controlled by *PublishOptions.freeze* field. By default all messages are frozen prior publishing, to prevent freezing the message, set the *PublishOptions.freeze* field to false.

## Time source

For performance reasons and to reduce stress on garbage collector, time source message **Topics.CLOCK_TICK** is singleton and it is being reused for every clock tick. This means that it can't be frozen when publishing, and should be passed further, at least shallow copy needs to be created using *Object.assign()*.

## Game controller

## Entities

There are two base types of entities:

### StatelessEntity

This entity doesn't have any stored internal state outside of limited number of system related attributes like *instanceId*. Behavior needs to be completely stateless.

### Entity

This kind of entity derives from *StatelessEntity* and stores its internal state in *state* attribute which is mapped to the game data object and is persisted when game state is saved.

## Realms

As a way to reduce traffic on the bus and to speed up the message processing, we divide game in several realms, which ensure that messages published inside of one realm are delivered mainly to entities in that realm and not elsewhere.

This applies to broadcast messages, as messages with explicit targets needs to be delivered in all cases.

Whether entity is belonging to one or the other realm is decided on the basis of CurrentSceneId attribute, from which we are able to get realm into which each scene belongs.

As scenes might belong to multiple realms, the relation between scene and realm is established in realm object.

In case there is no relation for the scene, it belongs to all realms. Same for entities without CurrentSceneId defined.

Channel takes the information about the realm from GameData object and applies it as RxJs filter on particular observable.

## Scenes

## Attributes

## Skills

## Behaviors

### Damage and health

Instead of health and damage control on the items, we have just one attribute *Health*, which indicates the overal state of the item/actor. There is *Damageable* behavior which allows to deduct the hit points. Other behavior like *Healable* or *Repairable* can be implemented later, if needed. The entity which is assigned the behavior and attribute will decide what to do with the value changes by either using other system or custom behaviors.

## Knowledge

## Dialogs

## Inventory

## Scenes

Availability of skills and inventory is controlled by the init property *SceneInit.disablesInteractions* -- when set to *true*, scene will disable all skills and inventory interactions fo actors in *Scene.onEnter()* method and enable it again in *Scene.onLeave()* method.

## Items

## Substances

Substances have three available states defined in *SubstanceStates* enum:
 * *SubstanceStates.solid*
 * *SubstanceStates.liquid*
 * *SubstanceStates.gas*

Substances support splitting and joining through functions *Substance.split()* and *Substance.join()*.

Substances can evaporate (behavior *Evaporate*) when poured on something using skill *Pour* (behavior *EvaporateWhenPoured*). Pouring same substance on the same surface will result in joining the substance instances into one, when the already existing instance is preserved.

Evaporation is signalled vie message **Topics.EVAPORATED**. Evaporated substance can be turned into the gas based on setting in *EvaporateState* behavior initializer. Gas produce by evaporation is joined to existing gas instance.

Substances in gas state will be merged into existing instance. Gas can dissipate when behavior *Dissipate* is attached to it. Dissipation of the gas is signalled vie message **Topics.DISSIPATED**

Both evaporation and dissipation are controlled by the wold clock, there is simplified model for both which decreases volume of substance by constant rate every sample period. This constant rate of evaporation and dissipation is controlled by attribute

As the substance might disappear because of evaporation or dissipation, we need to pass substance noun Id to the appropriate message, to prevent referencing instance which is already destroyed.

## Actors

### Actor modes

There are following actor modes available:
 * *Normal* -- normal actor state
 * *Engaged* -- Actor is either engaging or engaged
 * *Fighting* -- Actor is either attacking of being attacked
 * *Pursuing* -- Actor is pursuing other actor, usually after

## UI

## Spatial relations

## Language structures

## Resources and localization

## State saving

## Debugging


